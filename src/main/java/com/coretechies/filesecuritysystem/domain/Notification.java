/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author CoreTechies M8
 */
@Entity
@Table(name = "notification")
@Access(AccessType.FIELD)
public class Notification implements Serializable {

    @Id
    @GeneratedValue(generator = "notification")
    @GenericGenerator(name = "notification", strategy = "increment")
    private Integer notificationId;

    @Column
    private String notificationMessage;

    @Column
    private Integer notificationType;

    @Column
    private String seenby;

    @Column
    private Integer createdBy;

    @Column
    private Integer userId;

    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createOn;

    public Integer getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Integer notificationId) {
        this.notificationId = notificationId;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    public Integer getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(Integer notificationType) {
        this.notificationType = notificationType;
    }

    public String getSeenby() {
        return seenby;
    }

    public void setSeenby(String seenby) {
        this.seenby = seenby;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreateOn() {
        return createOn;
    }

    public void setCreateOn(Date createOn) {
        this.createOn = createOn;
    }

    @Override
    public String toString() {
        return "Notification{" + "notificationId=" + notificationId + ", notificationMessage=" + notificationMessage + ", notificationType=" + notificationType + ", seenby=" + seenby + ", createdBy=" + createdBy + ", userId=" + userId + ", createOn=" + createOn + '}';
    }

}
