/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.security;

import com.coretechies.filesecuritysystem.domain.AdminLicense;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CoreTechies M8
 */
public final class LicenseInformation {

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private final AES aes = new AES();

    private String userId;
    private String userName;
    private String organization;
    private String systemId;
    private String issueDate;
    private String expiryDate;
    private Integer carryForward = 0;
    private String licenseLimit;
    private String securityKey;
    private String identity = "FOLC";
    private String validity;
    private AdminLicense adminLicense;
    private String licenseInfo;

    public LicenseInformation(String license) throws InvalidInformationException {
        if (license.isEmpty()) {
            throw new InvalidInformationException("Please enter License.");
        } else {
            try {
                String decrypt = aes.decrypt(license, "0123456789abcdef");
                String[] split = decrypt.split("#");
                if (split[6].trim().equals(identity)) {
                    String user = split[0].trim();
                    String[] userSplit = user.split(":");
                    this.userId = userSplit[0].trim();
                    System.out.println("keyyyy=="+split[5].trim());
                    this.userName = userSplit[1].trim();
                    this.organization = userSplit[2].trim();
                    this.systemId = split[1].trim();
                    if (split[3].trim().contains("null")) {
                        //lbDETLICEnd.setText("LifeTime");
                        this.validity = "LifeTime";
                    } else {
                        this.validity = split[2].trim() + " to " + split[3].trim();
//                        Calendar startCalendar = Calendar.getInstance();
//                        startCalendar.setTime(simpleDateFormat.parse(split[2].trim()));
//                        Calendar endCalendar = Calendar.getInstance();
//                        startCalendar.setTime(simpleDateFormat.parse(split[3].trim()));
//                        if (startCalendar.get(Calendar.DATE) == endCalendar.get(Calendar.DATE) && startCalendar.get(Calendar.MONTH) == endCalendar.get(Calendar.MONTH)) {
//                            this.validity = String.valueOf(endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR));
//                        } else {
//                            this.validity = "No validity";
//                        }
                    }
                    if (split[4].trim().substring(2).equals("-1")) {
                        this.licenseLimit = "Unlimited";
                    } else {
                        this.licenseLimit = split[4].trim().substring(2);
                    }
                } else {
                    throw new InvalidInformationException("Invalid Licence");
                }
                this.licenseInfo = "success#{\"userId\":\"" + userId + "\",\"userName\":\"" + userName + "\",\"organization\":\"" + organization + "\","
                        + "\"validity\":\"" + validity + "\",\"licenseLimit\":\"" + licenseLimit + "\",\"systemId\":\"" + systemId + "\"}";
            } catch (Exception ex) {
                Logger.getLogger(LicenseInformation.class.getName()).log(Level.SEVERE, null, ex);
                throw new InvalidInformationException("Invalid Licence");
            }
        }
    }

    public LicenseInformation() {
        System.out.println("License Information constructor.");
    }

    public String getSystemId() {
        return systemId;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getOrganization() {
        return organization;
    }

    public String getLicenseLimit() {
        return licenseLimit;
    }

    public String getValidity() {
        return validity;
    }

    public String getLicenseInfo() {
        return licenseInfo;
    }

    public AdminLicense getAdminLicense() {
        return adminLicense;
    }

    public LicenseInformation(Integer clientuserid, String userName, String organization,
            Integer validity, Date startDate, Date endDate, String systemId, Boolean webunlimited,
            Boolean desktopunlimited, Integer weblicenseLimit,Integer dekstoplicenselimit,
            Boolean unlimitedcourse, Integer courselimit, Integer courseSetting,
            Integer smsTransaction, Integer smsPromotion, Integer carryForward,
            String securityKey) throws InvalidInformationException {
        adminLicense = new AdminLicense();
        adminLicense.setCreateOn(new Date());
        adminLicense.setIssueDate(startDate);
        adminLicense.setSystemInfoId(systemId);
        adminLicense.setUpdateOn(new Date());
        adminLicense.setUserId(clientuserid);
        adminLicense.setCourseSetting(courseSetting);
        adminLicense.setSmsTransactional(smsTransaction);
        adminLicense.setSmsPromotional(smsPromotion);
        adminLicense.setValidity(validity);
        adminLicense.setTransactionalCounter(0);
        adminLicense.setPromotionalCounter(0);

        if(unlimitedcourse){
            adminLicense.setCourseLimit(-1);
        }else{
            adminLicense.setCourseLimit(courselimit);
        }
        if (webunlimited) {
            adminLicense.setWeblicenseLimit(-1);
        } else {
            adminLicense.setWeblicenseLimit(weblicenseLimit);
        }
        if (desktopunlimited) {
            adminLicense.setDesktopLicenseLimit(-1);
        } else {
            adminLicense.setDesktopLicenseLimit(dekstoplicenselimit);
        }

        if(validity != null){
            if (validity == 20) {
                adminLicense.setExpiryDate(null);
            } else {
                adminLicense.setExpiryDate(endDate);
            }
        }else{
            adminLicense.setExpiryDate(endDate);
        }
        
        this.userName = userName;
        this.organization = organization;
        this.issueDate = simpleDateFormat.format(startDate);
        this.expiryDate = adminLicense.getExpiryDate() == null ? null : simpleDateFormat.format(adminLicense.getExpiryDate());
//        this.carryForward = carryForward;
        System.out.println("desktop limit==="+adminLicense.getDesktopLicenseLimit());
        this.licenseLimit = String.valueOf(adminLicense.getDesktopLicenseLimit());
        this.securityKey = securityKey;

        setUserId(clientuserid);
        setSystemId(systemId);
        adminLicense.setDesktopLicense(generateLicense());
        System.out.println("desktop license generated");
        this.licenseLimit = String.valueOf(adminLicense.getWeblicenseLimit());
        System.out.println("web limit===="+licenseLimit);
        adminLicense.setLicense(generateLicense());
    }

    public LicenseInformation(UserInfo userInfo, Boolean lifetime, Integer validity, Date startDate, Date endDate, Boolean webunlimited, Boolean desktopunlimited, Integer weblicenseLimit, Integer desktoplicenseLimit, Integer carryForward, String securityKey, AdminLicense adminLicense) throws InvalidInformationException {
        System.out.println("Reached.......");
        this.adminLicense = adminLicense;
        this.adminLicense.setIssueDate(startDate);
        this.adminLicense.setUpdateOn(new Date());
        this.adminLicense.setValidity(validity);

        if (webunlimited) {
            this.adminLicense.setWeblicenseLimit(-1);
        } else {
            this.adminLicense.setWeblicenseLimit(weblicenseLimit); 
        }
        
        if(desktopunlimited){
            this.adminLicense.setDesktopLicenseLimit(-1);
        } else{
            this.adminLicense.setDesktopLicenseLimit(desktoplicenseLimit);
        }

        if(validity != null){
            if (validity == 20) {
                this.adminLicense.setExpiryDate(null);
            } else {
                this.adminLicense.setExpiryDate(endDate);
            }
        } else{
            this.adminLicense.setExpiryDate(endDate);
        }
        

        this.userName = userInfo.getUserName();
        this.organization = userInfo.getOrganization();
        this.issueDate = simpleDateFormat.format(startDate);
        this.expiryDate = adminLicense.getExpiryDate() == null ? null : simpleDateFormat.format(adminLicense.getExpiryDate());
//        this.carryForward = carryForward;
        this.licenseLimit = String.valueOf(adminLicense.getDesktopLicenseLimit());
        this.securityKey = securityKey;
        this.carryForward = carryForward;

        setUserId(userInfo.getUserInfoId());
        setSystemId(adminLicense.getSystemInfoId());
        adminLicense.setDesktopLicense(generateLicense());
        System.out.println("desktop license renwed");
        this.licenseLimit = String.valueOf(adminLicense.getWeblicenseLimit());
        System.out.println("web limit===="+this.licenseLimit);
        adminLicense.setLicense(generateLicense());
        System.out.println("weblicense renwed");
    }
    
    public LicenseInformation(AdminLicense adminLicense, String systemId, Boolean unlimitedcourse, Integer courselimit, Integer courseSetting,
            Integer smsTransaction, Integer smsPromotion, UserInfo userInfo, String securityKey) throws InvalidInformationException, ParseException{
        System.out.println("Edit License");
        this.adminLicense = adminLicense;
        this.adminLicense.setIssueDate(adminLicense.getIssueDate());
        String startDate = new SimpleDateFormat("dd-MM-yyyy").format(adminLicense.getIssueDate());
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        this.issueDate = simpleDateFormat.format(format.parse(startDate));
        this.adminLicense.setUpdateOn(new Date());
        this.adminLicense.setValidity(adminLicense.getValidity());
        this.adminLicense.setWeblicenseLimit(adminLicense.getWeblicenseLimit()); 
        this.adminLicense.setDesktopLicenseLimit(adminLicense.getDesktopLicenseLimit());
        this.adminLicense.setExpiryDate(adminLicense.getExpiryDate());
        if(unlimitedcourse){
            this.adminLicense.setCourseLimit(-1);
        } else{
            this.adminLicense.setCourseLimit(courselimit); 
        }
        this.adminLicense.setCourseSetting(courseSetting);
        this.adminLicense.setSmsTransactional(smsTransaction);
        this.adminLicense.setSmsPromotional(smsPromotion);
        String enddate = new SimpleDateFormat("dd-MM-yyyy").format(adminLicense.getExpiryDate());
        this.expiryDate = simpleDateFormat.format(format.parse(enddate));
        this.userName = userInfo.getUserName();
        this.organization = userInfo.getOrganization();
        this.securityKey = securityKey;
        this.adminLicense.setSystemInfoId(systemId);
        setUserId(userInfo.getUserInfoId());
        setSystemId(systemId);
        this.licenseLimit = adminLicense.getDesktopLicenseLimit().toString();
        adminLicense.setDesktopLicense(generateLicense());
        System.out.println("desktop license generated");
        this.licenseLimit = adminLicense.getWeblicenseLimit().toString();
        adminLicense.setLicense(generateLicense());
        System.out.println("web license generated");
    }
    private String generateLicense() {
        String rawText
                = userId + ":" + userName + ":" + organization + "#"
                + systemId + "#"
                + issueDate + "#"
                + expiryDate + "#"
                + carryForward + "$" + licenseLimit + "#"
                + securityKey + "#" + identity;
        return aes.generate(rawText);
    }

    private void setUserId(Integer clientuserid) {
        clientuserid = 1000 + clientuserid;
        this.userId = "SA" + clientuserid;
    }

    public void setSystemId(String systemId) throws InvalidInformationException {
        try {
            this.systemId = systemId;
            String decrypt = aes.decrypt(systemId, "0123456789abcdef");
            System.out.println("system id=="+decrypt);
            String[] code = decrypt.split("#");
            if (!code[1].trim().equals("FCSO")) {
                throw new InvalidInformationException("Invalid System Id.");
            }
        } catch (Exception ex) {
            Logger.getLogger(LicenseInformation.class.getName()).log(Level.SEVERE, null, ex);
            throw new InvalidInformationException("Invalid System Id.");
        }
    }
    
    public String checkSystemId(String systemId) throws InvalidInformationException{
        try {
            String decrypt = aes.decrypt(systemId, "0123456789abcdef");
            System.out.println("system id=="+decrypt);
            String[] code = decrypt.split("#");
            if (!code[1].trim().equals("FCSO")) {
                throw new InvalidInformationException("Invalid System Id.");
            } else{
                return "success";
            }
        } catch (Exception ex) {
            Logger.getLogger(LicenseInformation.class.getName()).log(Level.SEVERE, null, ex);
            throw new InvalidInformationException("Invalid System Id.");
        }
    }
}

//    public LicenseInformation(Integer clientuserid, String userName, String organization, Integer validity, Date startDate, Date endDate, String systemId, Boolean unlimited, Integer licenseLimit, Integer carryForward, String securityKey) throws InvalidInformationException {
//        clientuserid = 1000 + clientuserid;
//        this.userId = "SA" + clientuserid;
//        this.userName = userName;
//        this.organization = organization;
//        this.systemId = systemId;
//        this.issueDate = simpleDateFormat.format(startDate);
//        this.expiryDate = endDate == null ? null : simpleDateFormat.format(endDate);
//        this.carryForward = carryForward;
//        this.licenseLimit = String.valueOf(licenseLimit);
//        this.securityKey = securityKey;
//        try {
//            String decrypt = aes.decrypt(systemId.trim(), "0123456789abcdef");
//            String[] code = decrypt.split("#");
//            if (!code[1].trim().equals("FCSO")) {
//                throw new InvalidInformationException("Invalid System Id.");
//            }
//
//            AdminLicense adminLicense = new AdminLicense();
//            adminLicense.setCreateOn(new Date());
//            adminLicense.setIssueDate(startDate);
//            adminLicense.setSystemInfoId(systemId);
//            adminLicense.setUpdateOn(new Date());
//            adminLicense.setUserId(clientuserid);
//            adminLicense.setValidity(validity);
//
//            if (unlimited) {
//                adminLicense.setLicenseLimit(-1);
//            } else {
//                adminLicense.setLicenseLimit(licenseLimit);
//            }
//
//            String userID = "SA";
//            switch ((clientuserid + "").length()) {
//                case 1:
//                    userID = userID + "100" + clientuserid;
//                    break;
//                case 2:
//                    userID = userID + "10" + clientuserid;
//                    break;
//                case 3:
//                    userID = userID + "1" + clientuserid;
//                    break;
//                case 4:
//                    userID = userID + "" + clientuserid;
//                    break;
//            }
//            if (validity == 20) {
//                adminLicense.setExpiryDate(null);
//            } else {
//                adminLicense.setExpiryDate(endDate);
//            }
//            String rawText = userID + ":" + userName + ":" + organization + "#"
//                    + systemId.trim() + "#"
//                    + simpleDateFormat.format(adminLicense.getIssueDate()) + "#"
//                    + (adminLicense.getExpiryDate() == null ? null : simpleDateFormat.format(adminLicense.getExpiryDate())) + "#"
//                    + carryForward + "$" + adminLicense.getLicenseLimit() + "#"
//                    + securityKey + "#" + identity;
//            adminLicense.setLicense(aes.generate(rawText));
//        } catch (Exception ex) {
//            throw new InvalidInformationException("Invalid System Id.");
//        }
//    }
