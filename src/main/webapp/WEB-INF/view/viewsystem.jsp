<%-- 
    Document   : changepassword
    Created on : 9 Aug, 2016, 2:24:32 PM
    Author     : CoreTechies M8
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>FSS | View System</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
 <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<style>
    
    .bs-example{
                width: 500px;
		float: right;
	}
</style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top"> 
 <!-- BEGIN HEADER INNER -->
 <jsp:include page="topbar.jsp" />
 <!-- END HEADER INNER --> 
</div>
<!-- END HEADER --> 
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER --> 
<!-- BEGIN CONTAINER -->
<div class="page-container"> 
 <!-- BEGIN SIDEBAR -->
 <div class="page-sidebar-wrapper"> 
  <!-- BEGIN SIDEBAR --> 
  <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
  <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
  <div class="page-sidebar navbar-collapse collapse"> 
   <!-- BEGIN SIDEBAR MENU --> 
   <jsp:include page="sidebar.jsp" />
<!--   <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
     DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
    <li class="sidebar-toggler-wrapper hide"> 
      BEGIN SIDEBAR TOGGLER BUTTON 
     <div class="sidebar-toggler"> <span></span> </div>
      END SIDEBAR TOGGLER BUTTON  
    </li>
    <li class="nav-item  "> <a href="index.html" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
    <li class="nav-item  "> <a href="users.html" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
     
    </li>
    <li class="nav-item  active"> <a href="licence.html" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">Licence</span>  </a>
     
    </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
     
    </li>
    <li class="nav-item  "> <a href="report.html" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
    <li class="nav-item  "> <a href="watermark.html" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
    <li class="nav-item  "> <a href="settings.html" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
   </ul>-->
   <!-- END SIDEBAR MENU --> 
   <!-- END SIDEBAR MENU --> 
  </div>
  <!-- END SIDEBAR --> 
 </div>
 <!-- END SIDEBAR --> 
 <!-- BEGIN CONTENT -->
 <div class="page-content-wrapper"> 
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content"> 
   <!-- BEGIN PAGE HEADER--> 
   
   <!-- BEGIN PAGE TITLE-->
   <!--<h3 class="page-title"> Licence 
    <!-- <small>blank page layout</small>-- 
   </h3>-->
   <!-- END PAGE TITLE--> 
   <!-- END PAGE HEADER-->
   
<div id="successMessage" class="bs-example" style="display: none;">
    <div  class="alert alert-success fade in">
           <a href="#" class="close" data-dismiss="alert">&times;</a>
               <strong>Success!</strong> ${message}
    </div>
</div>
  <div class="row">
         <div class="col-sm-12">
         	<div class="portlet box red   portlet-datatable  ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-desktop"></i>
                                        System
                                    </div>
                                    <div class="actions">
                                        <c:set var="Uname" value="${user.userName}"/>
                                        <a class="btn "  style="display:none;" data-toggle="tooltip" data-placement="top" title="Delete System" id="deletesystem">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                        <a class="btn "  style="display:none;" data-toggle="tooltip" data-placement="top" title="Edit System" id="editsystem">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a class="btn  " style="display:none;" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Copy" id="copy">
                                            <i class="icon-docs"></i>
                                        </a>
                                         <a class="btn  " href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/addsystem" data-toggle="tooltip" data-placement="top" title="Add System">
                                            <i class="icon-plus"></i> 
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_5">
                                        <thead>
                                            <tr>
                                                <th class="table-checkbox">
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes" />
                                                        <span></span>
                                                    </label>
                                                </th>
                                                <th> User ID </th>
                                                <th> User Name </th>
                                                <th> System Name </th>
                                                <th> System ID </th>
                                            </tr>
                                        </thead>
                                       
                                        <tbody id="tBody">
                                        <c:forEach items="${systems}" var="system" varStatus="status">
                                            <tr class="odd gradeX">
                                                <td>
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" name="systemId" class="checkboxes" value="${system.systemInfoId}" />
                                                        <span></span>
                                                    </label>
                                                </td>
<!--                                                <td>${system.userId}</td>-->
                                                <c:forEach items="${userIdmap}" var="userid">
                                                    <c:if test="${userid.key == system.userId}" >
                                                        <c:set var="id" value="${userid.value + 1000}"/>
                                                        <td>AD<c:out value="${id}" /></td>
                                                    </c:if>
                                                </c:forEach>
                                                <td>${system.userInfo.userName}</td>
                                                <td>${system.systemName}</td>
                                                <td class="clearfix">
                                                    <span class="text-ellips" id="">${system.systemId}</span>
                                                </td>
<!--                                            <td>
                                                    <a href="${pageContext.request.contextPath}/${user.userName}/deletesystem/${system.userId}/${system.systemInfoId}">Delete</a>
                                                </td>-->
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                    <span class="error-message" id="selecterror" ></span>
                                </div>
                            </div>
         </div>               
  </div>
  </div>
  <!-- END CONTENT BODY --> 
 </div>
 <!-- END CONTENT --> 
 <!-- BEGIN QUICK SIDEBAR --> 
 <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 
 
 <!-- END QUICK SIDEBAR --> 
</div>
<!-- END CONTAINER --> 
<!-- BEGIN FOOTER -->
<div class="page-footer">
 <div class="page-footer-inner"> 2016 &copy; Organization </div>
 <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
</div>
<!-- END FOOTER --> 

        <div class="modal fade" id="confirm_delete" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="">Confirm Delete</h4>
                    </div>
                    <div class="modal-body"> 
                        <div class="form-group">
                            <span Style="font-style: normal;">Are you sure you want to delete selected system ?</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <span id="response" style="color: green; display: block; font-size: 14px; float: left;" ></span>
                        <!--<button type="button" class=" btn btn green" data-dismiss="modal">NO</button>-->
<!--                        <button type="button" class="btn green" id="">YES</button>-->
                        <a  id="Confirm" class="btn btn-circle btn-success">YES</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div id="editmodel" class="modal fade" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModal">Edit System Name</h4>
                    </div>
                    <!-- dialog body -->
                    <div class="modal-body clearfix">
                        
                        <input type="hidden" class="form-control input-circle" value="XYZ" id="SystemIddisplay">
                        <label class="col-sm-3 control-label">System Name</label>
                        <div class="col-sm-8 clearfix">
                            <div class="input-group">
                                <span class="input-group-addon input-circle-left">
                                    <i class="fa fa-desktop"></i>
                                </span>
                                <input type="text" class="form-control input-circle-right" id="systemname" />
                            </div>
                            <span class="error-message" id="editerror" ></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-circle btn-danger" data-dismiss="modal">Cancel</button><!--data-dismiss="modal"-->
                        <button type="button" class="btn btn-circle btn-success" id="updatesystemname">Save</button>
                    </div>
                </div>
            </div>
        </div>
<!-- BEGIN CORE PLUGINS --> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
<!-- END CORE PLUGINS --> 
<!-- BEGIN THEME GLOBAL SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
<!-- END THEME GLOBAL SCRIPTS --> 
<!-- BEGIN THEME LAYOUT SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
<!-- END THEME LAYOUT SCRIPTS --> 
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="${pageContext.request.contextPath}/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="${pageContext.request.contextPath}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/clipboard/clipboard.min.js"></script>
        
        <script>
                $(document).ready(function () {
                    $('#side-menu').find('li').removeClass('active');
                    $(document).find(".system").addClass('active');
                    
                    if('${message}'!==''){
                        $('#successMessage').show();
                        setTimeout(function()
                            {
                                $('#successMessage').hide();
                            }, 3000);
                    }
                });
        </script>
        <script>
        $( ".form-control" ).focus(function() {
            $(this).prev('.input-group-addon').removeClass().addClass('input-group-addon-focus');
            $(this).next('.input-group-addon').removeClass().addClass('input-group-addon-focus');
        });

        $( ".form-control" ).focusout(function() {
            $(this).prev('.input-group-addon-focus').removeClass().addClass('input-group-addon');
            $(this).next('.input-group-addon-focus').removeClass().addClass('input-group-addon');
        });
        </script>
        <script>
            var clipboard = new Clipboard('#copy', {
                text: function () {
                    var checkedData = [];
                    $("#tBody").find("input[type='checkbox'][name='systemId']:checked").each(function () {
                        checkedData.push($(this).parent().parent().next().next().next().next().text().trim());
                    });
                    if (checkedData === null || checkedData.length === 0) {
                        $("#selecterror").text("Please select a System.");
                    } else if (checkedData.length > 1) {
                        $("#selecterror").text("More than one System is selected.");
                    } else {
                        return checkedData[0];
                    }
                }
            });
            clipboard.on('success', function (e) {
                console.log(e);
            });
            clipboard.on('error', function (e) {
                console.log(e);
            });
            </script>
            
            <script>
            $(".checkboxes").on("click", function () {
                    var checkedData = [];
                    $("#tBody").find("input[type='checkbox'][name='systemId']:checked").each(function () {
                        checkedData.push($(this).val());   
                    });
                    if(checkedData.length === 1){
                        $("#deletesystem").show();
                        $("#editsystem").show();
                        $("#copy").show();
                    }
                    else if (checkedData.length > 1) {
                            $("#deletesystem").show();
                            $("#editsystem").hide();
                            $("#copy").hide();
                        }
                    else if (checkedData === null || checkedData.length === 0){
                            $("#deletesystem").hide();
                            $("#editsystem").hide();
                            $("#copy").hide();
                        }
                    });
                    
                    $(".group-checkable").on("click", function () {
                        var checkedData = [];
                        var checkedboxes = [];
                        $("#tBody").find("input[type='checkbox'][name='systemId']").each(function () {
                            checkedData.push($(this).val());      
                        });
                        $("#tBody").find("input[type='checkbox'][name='systemId']:checked").each(function () {
                            checkedboxes.push($(this).val());   
                        });
                        console.log($(this).is(":checked"));
                        if($(this).is(":checked")){
                            console.log(checkedboxes);
                            if(checkedData === null || checkedData.length === 0){
                                $("#deletesystem").hide();
                            }else{
                                $("#deletesystem").show();
                                if(checkedboxes !== null || checkedboxes.length !== 0){
                                    $("#editsystem").hide();
                                    $("#copy").hide();
                                }
                            }
                        }else{
                            $("#deletesystem").hide();
                            $("#editsystem").hide();
                            $("#copy").hide();
                        }
                    });
        </script>
        <script>
            $("#editsystem").on("click", function () {
                var checkedData = [];
                $("#tBody").find("input[type='checkbox'][name='systemId']:checked").each(function () {
                    checkedData.push($(this).val());      
                });
                if (checkedData.length === 1) {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getsinglesystem", {systemid: checkedData[0]},
                                    function (data, status) {
                                        if (data.indexOf("success") > -1) {
                                            var jsondata = JSON.parse(data.split("#")[1]);
                                            $("#SystemIddisplay").val(jsondata.systemId);
                                            $("#systemname").val(jsondata.systemName);
                                            $("#editmodel").modal("show");
                                        } else {
                                            $("#selecterror").text(data);
                                        }
                                    });   
                    }
            });
            
            $("#deletesystem").on("click", function () {
                 $("#confirm_delete").modal("show");
            });
            $("#Confirm").on("click", function () {
                var checkedData = [];
                $("#tBody").find("input[type='checkbox'][name='systemId']:checked").each(function () {
                        checkedData.push($(this).val());   
                });
                 if(checkedData === null || checkedData.length === 0){
                    $("#selecterror").text("Please select a System.");
                } else{
                    var Uname = "${user.userName}".replace(" ", "");
                    $.get("${pageContext.request.contextPath}/"+Uname+"/deletesystem",{systemInfoIds: checkedData},
                    function(data, status){
                        console.log(data);
                        if(data.indexOf("success") > -1){
                            var res = data.split("#")[1];
                            $("#response").text(res);
                            setTimeout( function()
                            {
                                $("#confirm_delete").modal("hide");
                                window.location.reload();
                            }, 5000);
                            
                        }else{
                            $("#response").text(data);
                            setTimeout(function()
                                {
                                    $("#confirm_delete").modal("hide");
                                }, 10000);
                        }
                    });
                }
            });
            
            $("#updatesystemname").on("click", function () {
                var name = $("#systemname").val();
                var id = $("#SystemIddisplay").val(); 
                var flag = true;
                if(name === "" || name === null){
                    $("#editerror").text("Please Enter System Name");
                    flag = false;
                }
                if(flag){
                    var Uname = "${user.userName}".replace(" ", "");
                    window.location.href = "${pageContext.request.contextPath}/"+Uname+"/editsystem/"+id+"/"+name; 
                } 
            });
            
            $("#systemname").on("keyup", function(){
                var name = $("#systemname").val();
                if(name === "" || name === null){
                    $("#editerror").text("Please Enter System Name");
                } else{
                    $("#editerror").text("");
                }
            });
        </script>
        <script>
            $(function () {
                 $('[data-toggle="tooltip"]').tooltip();
            });
	</script>
            
</body>
</html>