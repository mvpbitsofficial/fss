/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.controller;

import com.coretechies.filesecuritysystem.algorithm.UserType;
import com.coretechies.filesecuritysystem.dao.NotificationDao;
import com.coretechies.filesecuritysystem.dao.SystemInfoDao;
import com.coretechies.filesecuritysystem.dao.UserInfoDao;
import com.coretechies.filesecuritysystem.dao.UserLicenseDao;
import com.coretechies.filesecuritysystem.domain.SystemInfo;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.domain.UserLicense;
import com.coretechies.filesecuritysystem.security.InvalidInformationException;
import com.coretechies.filesecuritysystem.service.SystemInfoService;
import com.coretechies.filesecuritysystem.service.UserInfoService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @Controller annotation represents that this is a java controller class.
 * The SystemController class implements an application that handles all the requests related to 
 * system, like add a new system, view system, delete system.
 * 
 * @SessionAttributes annotation represents a user in session.
 * "user" is a object of userInfo class and have a complete information
 * of that user who is present in the session at that time.
 * 
 * @author Jaishree
 * @version 1.0
 * @since 2016-09-02
 */
@Controller
@SessionAttributes({"user"})
public class SystemController {

    /** The @Autowired annotation is auto wire the bean by matching data type.
     *  systemInfoService, userInfoDao and mailController are objects to use methods of these classes in our controller. 
     */
    @Autowired
    private SystemInfoService systemInfoService;

    @Autowired
    private UserInfoDao userInfoDao;

    @Autowired
    private SystemInfoDao systemInfoDao;
    
    @Autowired
    private MailController mailController;

    @Autowired
    private UserInfoService userInfoService;
    
    @Autowired
    private UserLicenseDao userLicenseDao;
    
    @Autowired
    private NotificationDao notificationDao;
    
    /**
     * This method will take the user to the page viewsystem. 
     * 
     * @param userName
     * @param modelMap
     * @return on page viewsystem
     */
    @GetMapping(value = "/{userName}/viewsystem")
    public ModelAndView viewSystem(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("systems", systemInfoService.getSystemInfoByCreator(userInfo.getUserInfoId()));
                modelMap.addAttribute("userIdmap", userInfoService.showUserIdThroughCount());
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                return new ModelAndView("viewsystem");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This class will take the user at the page addsystem 
     * with a list of all users created by that particular client who is im session right now. 
     * 
     * @param userName
     * @param modelMap
     * @return on page addsystem
     */
    @GetMapping(value = "/{userName}/addsystem")
    public ModelAndView getAddSystem(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("users", userInfoDao.getUserInfoByCreatedBy(userInfo.getUserInfoId()));
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                return new ModelAndView("addsystem");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will add a new system in a database for user and return a success message,
     * If the required value is not provided by client then this method will return an appropriate error message.
     * 
     * @param userName
     * @param userId
     * @param systemName
     * @param systemId
     * @param modelMap
     * @return String
     */
    @PostMapping(value = "/{userName}/addsystem")
    @ResponseBody
    @Transactional(propagation = Propagation.REQUIRED)
    public String addSystem(@PathVariable String userName, @RequestParam Integer userId, @RequestParam String systemName, @RequestParam String systemId, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                if (systemName == null || systemName.trim().isEmpty()) {
                    return "Please enter system name.";
                }
                if (systemId == null || systemId.trim().isEmpty()) {
                    return "Please enter system id.";
                }
                if (systemInfoService.getSystemInfoBySystemName(userId, systemName) != null) {
                    return "This system name is already added.";
                }
                if (systemInfoService.getSystemInfoBySystemId(userId, systemId) != null) {
                    return "This system id is already added.";
                }
                try {
                    if (systemInfoService.saveSystemInfo(systemName, systemId, userId, userInfo.getUserInfoId()) != null) {
                        return "success";
                    } else {
                        return "Unable to add data";
                    }
                } catch (InvalidInformationException e) {
                    return e.getMessage();
                }
            } else {
                return "Please login again";
            }
        } else {
            return "Please login again";
        }
    }

    /**
     * This method will delete a system information from the database and send a mail for confirmation.
     * 
     * @param userName
     * @param systemInfoIds
     * @param redirectAttributes
     * @param request
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/deletesystem")
    @ResponseBody
    public String deleteSystem(@PathVariable String userName, @RequestParam(value = "systemInfoIds[]") Integer[] systemInfoIds, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                String message = "";
                String message1 = "";
                System.out.println("Ids====="+Arrays.toString(systemInfoIds));
                for (Integer systemInfoId : systemInfoIds) {
                    SystemInfo systemInfo = systemInfoDao.getSystemById(systemInfoId);
                    List<UserLicense> userLicense = userLicenseDao.getUserLicenseBySystemId(systemInfoId);
                    if(userLicense == null || userLicense.isEmpty()){
                        if (systemInfo != null)  {
                            if (systemInfoService.deleteSystemId(systemInfoId)) {
                                redirectAttributes.addFlashAttribute("message", "System has been deleted successfully.");
                                message += ","+systemInfo.getSystemName();
                            } else {
                                redirectAttributes.addFlashAttribute("message", "Unable to delete system.");
                            }
                        } else {
                            redirectAttributes.addFlashAttribute("message", "This system is unavailable.");
                        }
                    }else{
                        message1 += ","+systemInfo.getSystemName();
                        System.out.println(message1);
                        System.out.println("Can not delete system id is in use");
                    }
                }
                
                if(!message.isEmpty()){
                    message = "\""+message.substring(1)+"\""+" System has been deleted successfully";
                }
                if(!message1.isEmpty()){
                    message1 = "\""+message1.substring(1)+"\""+" System can not delete, Id is in use.";
                }
                System.out.println("message1=="+message+" message2=="+message1);
                if(!message.isEmpty() && !message1.isEmpty()){
                    return "success#"+message+" and "+message1;
                }else{
                    return "success#"+message+""+message1;
                }
            } else {
                return "Unauthorised Access";
            }
        } else {
            return "Login Again";
        }
    }

    @GetMapping(value = "/{userName}/editsystem/{systemId}/{systemName}")
    public ModelAndView editSystem(@PathVariable String userName, @PathVariable Integer systemId, @PathVariable String systemName ,RedirectAttributes redirectAttributes, ModelMap modelMap){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
       if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                SystemInfo systemInfo = systemInfoDao.getSystemById(systemId);
                System.out.println(systemName);
                systemInfo.setSystemName(systemName);
                systemInfoService.editSystemName(systemInfo);
                redirectAttributes.addFlashAttribute("message", "System Name has been updated successfully.");
                return new ModelAndView("redirect:/" + userName + "/viewsystem");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }
    
    /**
     * This method will return a string containing the system information of requested user.
     * 
     * @param userName
     * @param userId
     * @param modelMap
     * @return 
     */
    @PostMapping(value = "/{userName}/getusersystem")
    @ResponseBody
    public String getUserSystem(@PathVariable String userName, @RequestParam Integer userId, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                if (userId == null || userId == 0) {
                    return "Please select a user.";
                }
                return systemInfoService.getSystemInfoByUser(userId);
            } else {
                return "Please login again";
            }
        } else {
            return "Please login again";
        }
    }

    /**
     * 
     * @param userName
     * @param type
     * @param modelMap
     * @return List<>
     */
    @GetMapping(value = "/{userName}/getusers")
    @ResponseBody
    public String getUsersByChoice(@PathVariable String userName, @RequestParam Integer type, ModelMap modelMap){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                List<UserInfo> userInfoByCreatedBy = userInfoDao.getUserInfoByCreatedBy(userInfo.getUserInfoId());
                List<String> username = new ArrayList<>();
                if(type==1){
                    for (UserInfo userInfo1 : userInfoByCreatedBy) {
                        username.add(userInfo1.getUserName());
                    }
                }
                if(type==2){
                    for (UserInfo userInfo1 : userInfoByCreatedBy) {
                        username.add(userInfo1.getEmailId());
                    }
                }
                if(type==3){
                    for (UserInfo userInfo1 : userInfoByCreatedBy) {
                        username.add(userInfo1.getMobileNumber());
                    }
                }
                System.out.println(username);
                String value = "success";
                for(int i=0; i < username.size(); i++ ){
                    value += "#"+username.get(i);
                }
                System.out.println(value);
                return value;
            } else {
                return "fail to fetch";
            }
        } else {
            return "login again";
        }
    }
    /**
     * 
     * @param userName
     * @param systemid
     * @param modelMap
     * @return 
     */
    @PostMapping(value = "/{userName}/getsinglesystem")
    @ResponseBody
    public String getsysteminfo(@PathVariable String userName, @RequestParam Integer systemid, ModelMap modelMap){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                SystemInfo system = systemInfoDao.getSystemById(systemid);
                if (system != null) {
                    return "success#{\"systemId\":\""+system.getSystemInfoId()+"\","
                            +"\"systemName\":\""+system.getSystemName()+ "\"}";
                }
                return "Unauthorized Access";
            } else {
                return "Unauthorized Access";
            }
        }else{
            return "Please Login Again";
        }
    }
    
    @PostMapping(value = "/{userName}/getsystemid")
    @ResponseBody
    public String getSystemId(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer systemInfoId){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                SystemInfo system = systemInfoDao.getSystemById(systemInfoId);
                if (system != null) {
                    return "success#"+system.getSystemId();
                }
                return "Unauthorized Access";
            } else {
                return "Unauthorized Access";
            }
        }else{
            return "Please Login Again";
        }
    }
//    @PostMapping(value = "/{userName}/getsysteminfo")
//    @ResponseBody
//    public String getSysteminfo(@PathVariable String userName, ModelMap modelMap, @PathVariable String systemId){
//        UserInfo userInfo = (UserInfo) modelMap.get("user");
//        if (userInfo != null && userInfo.getUserName().equals(userName)) {
//            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
//                try {
//                    return new SystemInformation(systemId.trim()).getSystemInfo();
//                } catch (InvalidInformationException ex) {
//                    //Logger.getLogger(SystemController.class.getName()).log(Level.SEVERE, null, ex);
//                    return ex.getMessage();
//                }
//            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
//                try {
//                    return new SystemInformation(systemId.trim()).getSystemInfo();
//                } catch (InvalidInformationException ex) {
//                    //Logger.getLogger(SystemController.class.getName()).log(Level.SEVERE, null, ex);
//                    return ex.getMessage();
//                }
//            } else {
//                return "Please login again.";
//            }
//        }else{
//            return "login again";
//        }
//    }
}
