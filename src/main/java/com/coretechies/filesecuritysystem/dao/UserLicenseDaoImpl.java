/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.SystemInfo;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.domain.UserLicense;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("UserLicenseDao")
public class UserLicenseDaoImpl implements UserLicenseDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = false)
    public UserLicense saveUserLicense(UserLicense userLicense) {
        try {
            entityManager.persist(userLicense);
            return userLicense;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = false)
    public UserLicense updateUserLicense(UserLicense userLicense) {
        try {
            entityManager.merge(userLicense);
            return userLicense;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserLicense> getUserLicenseByUser(Integer userId) {
        try {
            TypedQuery<UserLicense> createQuery = entityManager.createQuery("SELECT UL FROM UserLicense AS UL WHERE UL.userId=:userId AND UL.status=true", UserLicense.class);
            createQuery.setParameter("userId", userId);
            return createQuery.getResultList();
        } catch (Exception e) {
            return null;
        }
    }
//
//    @Override
//    @Transactional(readOnly = true)
//    public List<UserLicense> getUserLicenseByUser(Integer userId, Date todayDate) {
//        try {
//            TypedQuery<UserLicense> createQuery = entityManager.createQuery("SELECT UL FROM UserLicense AS UL WHERE UL.userId=:userId AND UL.status=true AND UL.issueDate>=:date AND UL.expiryDate<=:date", UserLicense.class);
//            createQuery.setParameter("userId", userId);
//            createQuery.setParameter("date", todayDate);
//            return createQuery.getResultList();
//        } catch (Exception e) {
//            return null;
//        }
//    }

    @Override
    @Transactional(readOnly = true)
    public UserLicense getUserLicense(Integer licenseId) {
        try {
            TypedQuery<UserLicense> createQuery = entityManager.createQuery("SELECT UL FROM UserLicense AS UL WHERE UL.userLicenseId=:licenseId AND UL.status=true", UserLicense.class);
            createQuery.setParameter("licenseId", licenseId);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
//
//    @Override
//    @Transactional(readOnly = false)
//    public Boolean deleteUserLicense(Integer licenseId) {
//        try {
//            entityManager.remove(entityManager.find(UserLicense.class, licenseId));
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//    }

    @Override
    @Transactional(readOnly = true)
    public List<UserLicense> getLicenseExpireAlert(Date date) {
        try {
            TypedQuery<UserLicense> createQuery = entityManager.createQuery("SELECT UL FROM UserLicense AS UL WHERE UL.expiryDate IS NOT NULL AND UL.expiryDate<=:date AND UL.status=true", UserLicense.class);
            createQuery.setParameter("date", date);
            System.out.println("date============"+date);
            List<UserLicense> userLicenses = createQuery.getResultList();
            for (UserLicense adminLicense : userLicenses) {
                Hibernate.initialize(adminLicense.getUserInfo());
            }
            return userLicenses;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserLicense> getUserLicenseByCreator(Integer createdBy) {
        try {
            TypedQuery<UserLicense> createQuery = entityManager.createQuery("SELECT UL FROM UserLicense AS UL WHERE UL.createdBy=:createdBy AND UL.status=true", UserLicense.class);
            createQuery.setParameter("createdBy", createdBy);
            List<UserLicense> userLicenses = createQuery.getResultList();
            for (UserLicense userLicense : userLicenses) {
                Hibernate.initialize(userLicense.getUserInfo());
                Hibernate.initialize(userLicense.getCourse());
            }
            return userLicenses;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Set<UserInfo> getAssignUserListOnCourse(Integer courseId) {
        try {
            TypedQuery<UserLicense> createQuery = entityManager.createQuery("SELECT UL FROM UserLicense AS UL WHERE UL.courseId=:courseId AND UL.status=true", UserLicense.class);
            createQuery.setParameter("courseId", courseId);
            List<UserLicense> userLicenses = createQuery.getResultList();
            Set<UserInfo> userInfos = new HashSet<>();
            for (UserLicense userLicense : userLicenses) {
                Hibernate.initialize(userLicense.getUserInfo());
                userInfos.add(userLicense.getUserInfo());
            }
            return userInfos;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Set<SystemInfo> getAssignSystemListOnCourse(Integer courseId) {
        try {
            TypedQuery<UserLicense> createQuery = entityManager.createQuery("SELECT UL FROM UserLicense AS UL WHERE UL.courseId=:courseId AND UL.status=true", UserLicense.class);
            createQuery.setParameter("courseId", courseId);
            List<UserLicense> userLicenses = createQuery.getResultList();
            Set<SystemInfo> systemInfos = new HashSet<>();
            for (UserLicense userLicense : userLicenses) {
                Hibernate.initialize(userLicense.getSystemInfo());
                systemInfos.add(userLicense.getSystemInfo());
            }
            return systemInfos;
        } catch (Exception e) {
            return null;
        }
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<UserLicense> getUserLicenseBySystemId(Integer systemInfoId){
        try {
            TypedQuery<UserLicense> createQuery = entityManager.createQuery("SELECT UL FROM UserLicense AS UL WHERE UL.systemInfoId=:systemInfoId AND UL.status=true", UserLicense.class);
            createQuery.setParameter("systemInfoId", systemInfoId);
            return createQuery.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserLicense> getUserLicenseListByCreateOn(Integer createdBy, Date fromDate, Date toDate) {
        try {
            TypedQuery<UserLicense> createQuery = entityManager.createQuery("SELECT UL FROM UserLicense AS UL WHERE UL.createdBy=:createdBy AND createOn BETWEEN :startDate AND :endDate AND UL.status=true", UserLicense.class);
            createQuery.setParameter("createdBy", createdBy);
            createQuery.setParameter("startDate", fromDate);
            createQuery.setParameter("endDate", toDate);
            List<UserLicense> userLicenses = createQuery.getResultList();
            for (UserLicense userLicense : userLicenses) {
                Hibernate.initialize(userLicense.getUserInfo());
                Hibernate.initialize(userLicense.getCourse());
            }
            return userLicenses;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserLicense> getUserLicenseListByExpiryDate(Integer createdBy, Date fromDate, Date toDate) {
        try {
            TypedQuery<UserLicense> createQuery = entityManager.createQuery("SELECT UL FROM UserLicense AS UL WHERE UL.createdBy=:createdBy AND UL.expiryDate>=:startDate AND UL.expiryDate<=:endDate AND UL.status=true", UserLicense.class);
            createQuery.setParameter("createdBy", createdBy);
            createQuery.setParameter("startDate", fromDate);
            createQuery.setParameter("endDate", toDate);
            List<UserLicense> userLicenses = createQuery.getResultList();
            for (UserLicense userLicense : userLicenses) {
                Hibernate.initialize(userLicense.getUserInfo());
                Hibernate.initialize(userLicense.getCourse());
            }
            return userLicenses;
        } catch (Exception e) {
            return null;
        }
    }
    
    @Override
    public List<UserLicense> getUserLicenseForChart(Integer createdBy, Date date){
        try {
            TypedQuery<UserLicense> createQuery = entityManager.createQuery("SELECT UL FROM UserLicense AS UL WHERE UL.createdBy=:createdBy AND UL.createOn=:startDate AND UL.status=true", UserLicense.class);
            createQuery.setParameter("createdBy", createdBy);
            createQuery.setParameter("startDate", date);
            System.out.println("date====="+date);
            List<UserLicense> userLicenses = createQuery.getResultList();
            for (UserLicense userLicense : userLicenses) {
                Hibernate.initialize(userLicense.getUserInfo());
                Hibernate.initialize(userLicense.getCourse());
            }
            return userLicenses;
        } catch (Exception e) {
            return null;
        }
    }
    
    @Override
    public UserLicense getConfirmLicenseForDisplay(){
        TypedQuery<UserLicense> query = entityManager.createQuery("SELECT UL FROM UserLicense AS UL order by UL.userLicenseId desc", UserLicense.class);
        return query.setMaxResults(1).getSingleResult();
//        UserLicense userLicense = new UserLicense();
//        return userLicense;
    }
    
    @Override
    public List<UserLicense> getTotalUsedCourseCount(Integer courseid){
        try{
            TypedQuery<UserLicense> query = entityManager.createQuery("SELECT UL FROM UserLicense AS UL WHERE UL.courseId=:courseid ", UserLicense.class);
            query.setParameter("courseid", courseid);
            return query.getResultList();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public Integer showLicenseChartData(Date startDate, Date endDate, Integer createdBy){
        try{
            TypedQuery<UserLicense> query = entityManager.createQuery("SELECT UL FROM UserLicense AS UL WHERE createOn BETWEEN :from AND :to AND UL.createdBy=:createdBy", UserLicense.class);
            query.setParameter("from", startDate);
            query.setParameter("to", endDate);
            query.setParameter("createdBy", createdBy);
            List<UserLicense> resultList = query.getResultList();
            System.out.println("userInfo" +resultList);
            if(resultList != null){
                return resultList.size();
            } else{
                return 0;
            }
        } catch(Exception e){
            e.printStackTrace();
            return 0;
        }
    }
    
    @Override
    public List<UserLicense> getLicenseListOfAssignedCourse(Integer courseId){
        try{
            TypedQuery<UserLicense> createQuery = entityManager.createQuery("SELECT UL FROM UserLicense AS UL WHERE UL.courseId=:courseId AND UL.status=true", UserLicense.class);
            createQuery.setParameter("courseId", courseId);
            List<UserLicense> resultList = createQuery.getResultList();
            System.out.println("RESULTLIST"+resultList);
            if(resultList.isEmpty()){
                return null;
            }else{
                return resultList;
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<UserLicense> getUserLicenseByWatermarkId(Integer watermarkId){
        try {
            TypedQuery<UserLicense> createQuery = entityManager.createQuery("SELECT UL FROM UserLicense AS UL WHERE UL.watermarkId=:watermarkId AND UL.status=true", UserLicense.class);
            createQuery.setParameter("watermarkId", watermarkId);
            return createQuery.getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    @Override
    public Integer getAllUserLicenseCount(){
        try{
            TypedQuery<UserLicense> createQuery = entityManager.createQuery("SELECT UL FROM UserLicense AS UL", UserLicense.class);
            return createQuery.getResultList().size();
        } catch(Exception e){
            e.printStackTrace();
            return 0;
        }
    }
}
