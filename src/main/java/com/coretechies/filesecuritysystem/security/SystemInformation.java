/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.security;

/**
 *
 * @author CoreTechies M8
 */
public final class SystemInformation {

    private final String diskSerialNumber;
    private final String macId;
    private final String systemInfo;

    public SystemInformation(String systemId) throws InvalidInformationException {
        if (systemId.isEmpty()) {
            throw new InvalidInformationException("Please enter System Id.");
        } else {
            try {
                AES aes = new AES();
                String decrypt = aes.decrypt(systemId, "0123456789abcdef");
                System.out.println("decrypt"+decrypt);
                String[] code = decrypt.split("#");
                if (code[1].trim().equals("FCSO")) {
                    String uid = code[0].trim();
                    String[] split = uid.split(":");
                    this.diskSerialNumber = split[0].trim();
                    this.macId = split[1].trim();
                    this.systemInfo = "success#{\"serialNumber\":\"" + diskSerialNumber + "\",\"macId\":\"" + macId + "\"}";
                } else {
                    throw new InvalidInformationException("Invalid System Id.");
                }
            } catch (Exception e) {
                throw new InvalidInformationException("Invalid System Id.");
            }
        }
    }

    public String getDiskSerialNumber() {
        return diskSerialNumber;
    }

    public String getMacId() {
        return macId;
    }

    public String getSystemInfo() {
        return systemInfo;
    }

}
