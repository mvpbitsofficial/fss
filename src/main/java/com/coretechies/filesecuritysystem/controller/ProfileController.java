/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.controller;

import com.coretechies.filesecuritysystem.algorithm.NotificationType;
import com.coretechies.filesecuritysystem.algorithm.UserType;
import com.coretechies.filesecuritysystem.dao.AdminLicenseDao;
import com.coretechies.filesecuritysystem.dao.MailSettingDao;
import com.coretechies.filesecuritysystem.dao.NotificationDao;
import com.coretechies.filesecuritysystem.dao.SecurityQuestionAnswerDao;
import com.coretechies.filesecuritysystem.dao.SecurityQuestionDao;
import com.coretechies.filesecuritysystem.dao.UserInfoDao;
import com.coretechies.filesecuritysystem.dao.UserLicenseDao;
import com.coretechies.filesecuritysystem.domain.AdminLicense;
import com.coretechies.filesecuritysystem.domain.MailSetting;
import com.coretechies.filesecuritysystem.domain.Notification;
import com.coretechies.filesecuritysystem.domain.SecurityQuestion;
import com.coretechies.filesecuritysystem.domain.SecurityQuestionAnswer;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.domain.UserLicense;
import com.coretechies.filesecuritysystem.security.InvalidInformationException;
import com.coretechies.filesecuritysystem.security.SystemInformation;
import com.coretechies.filesecuritysystem.service.AdminLicenseService;
import com.coretechies.filesecuritysystem.service.CourseService;
import com.coretechies.filesecuritysystem.service.SystemInfoService;
import com.coretechies.filesecuritysystem.service.UserInfoService;
import com.coretechies.filesecuritysystem.service.UserLicenseService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FilenameUtils;
import com.coretechies.filesecuritysystem.security.AES;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.ocpsoft.prettytime.PrettyTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

/**
 * The ProfileController class implements an application that handles all the requests related to user profile.
 * 
 * @Controller annotation represents that this is a java controller class.
 * 
 * @SessionAttributes annotation represents a user in session.
 * "user" is a object of userInfo class and have a complete information
 * of that user who is present in the session at that time.
 * 
 * @author Jaishree
 * @version 1.0
 * @since 2016-09-02
 */
@Controller
@SessionAttributes({"user"})
public class ProfileController {

    @Autowired
    private UserLicenseService userLicenseService;

    @Autowired
    private UserLicenseDao userLicenseDao;
    
    @Autowired
    private CourseService courseService;

    @Autowired
    private SystemInfoService systemInfoService;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private UserInfoDao userInfoDao;

    @Autowired
    private SecurityQuestionDao securityQuestionDao;

    @Autowired
    private SecurityQuestionAnswerDao securityQuestionAnswerDao;

    @Autowired
    private MailController mailController;
    
    @Autowired
    private AdminLicenseService adminLicenseService;

    @Autowired
    private AdminLicenseDao adminLicenseDao;
    
    @Autowired
    private NotificationDao notificationDao;
    
    @Autowired
    private MailSettingDao mailSettingDao;
    
    private final AES aes = new AES();
    
    static Map<String, String> backuphistory = new HashMap<>();
    /**
     * This method will take the user on dashboard page
     * If the user is admin or owner then it will return on page admindashboard,  
     * And if the user is a client then it will return on page clientdashboard.
     * 
     * @param userName
     * @param modelMap
     * @return on dashboard
     */
    @GetMapping(value = "/{userName}/dashboard")
    public ModelAndView getDashboard(@PathVariable String userName, ModelMap modelMap) throws ParseException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
//        System.out.println("session vala name"+ userInfo.getUserName().replace(" ", ""));
//        System.out.println("url vala name"+userName);
            System.out.println("userInfo ==== "+userInfo);
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
//            if (securityQuestionAnswerDao.getSecurityQuestionAnswers(userInfo.getUserInfoId()).isEmpty()) {
//                modelMap.addAttribute("answers", null);
//                modelMap.addAttribute("questions", securityQuestionDao.getSecurityQuestions());
//            }
//            List<SecurityQuestionAnswer> securityQuestionAnswers = securityQuestionAnswerDao.getSecurityQuestionAnswers(userInfo.getUserInfoId());
//            System.out.println("answers=="+securityQuestionAnswers);
//            if(securityQuestionAnswers.isEmpty()){
//                 modelMap.addAttribute("answers", null);
//            }else{
//                modelMap.addAttribute("answers", securityQuestionAnswers);
//            }
           
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                List<UserInfo> userInfos = userInfoDao.getUserInfoByCreatedByDescendingOrder(userInfo.getUserInfoId());
                System.out.println("getUserInfoByCreatedByDescendingOrder : " + userInfos);
                if (userInfos != null) {
                    modelMap.addAttribute("totalClients", userInfos.size());
                } else {
                    modelMap.addAttribute("totalClients", 0);
                }
                modelMap.addAttribute("users", userInfos);
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
//                modelMap.addAttribute("totalLicenses", adminLicenseService.getAdminLicenseCount());
                modelMap.addAttribute("totalLicenses", userLicenseDao.getAllUserLicenseCount());
                modelMap.addAttribute("totalUsers", userInfoDao.getUserInfoByUserType(3).size());
//                modelMap.addAttribute("totalCourses", courseService.getAllCourseCount());
                modelMap.addAttribute("totalSystems", systemInfoService.getSystemInfoCount());
                List<String> dates = new ArrayList<>();
                List<Integer> weeklyusers = new ArrayList<>();
                List<Integer> weeklylicense = new ArrayList<>();
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date todaysdate =cal.getTime();
                String today = dateFormat.format(cal.getTime());
                cal.add(Calendar.DATE, -6); 
                Date previousday = cal.getTime();
                while (todaysdate.compareTo(previousday) >= 0) {
                    Date date = dateFormat.parse(dateFormat.format(previousday));
                    System.out.println("parse date====="+date);
                    List<UserInfo> chartUserData = userInfoService.chartUserData(userInfo.getUserInfoId(), date);
                    if(chartUserData!=null){
                        weeklyusers.add(chartUserData.size());
                        System.out.println(chartUserData.size());
                    }else{
                        weeklyusers.add(0);
                    }
                    Integer licenseCount = adminLicenseService.getClientLicenseForChart(date); 
                    weeklylicense.add(licenseCount);
                    String format=dateFormat.format(previousday);
                    dates.add(format);
                    cal.add(Calendar.DATE, 1); 
                    Date nextDay =cal.getTime();
                    previousday = nextDay;
                }
                modelMap.addAttribute("dates", dates);
                modelMap.addAttribute("weeklyusers", weeklyusers);
                modelMap.addAttribute("weeklylicense", weeklylicense);
                System.out.println(weeklyusers);
                System.out.println(weeklylicense);
                return new ModelAndView("admindashboard");
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                List<UserInfo> userInfos = userInfoDao.getUserInfoByCreatedByDescendingOrder(userInfo.getUserInfoId());
                if (userInfos != null) {
                    modelMap.addAttribute("totalUsers", userInfos.size());
                } else {
                    modelMap.addAttribute("totalUsers", 0);
                }
                PrettyTime p = new PrettyTime();
                for(UserInfo userInfo1 :userInfos){
                    System.out.println("Chaurasia Time:"+p.format(userInfo1.getCreateOn()));
                    System.out.println(userInfo1);
                }
                modelMap.addAttribute("users", userInfos);
                modelMap.addAttribute("totalLicenses", userLicenseService.getUserLicenseCountByCreator(userInfo.getUserInfoId()));
                modelMap.addAttribute("totalCourses", courseService.getCourseCount(userInfo.getUserInfoId()));
                modelMap.addAttribute("totalSystems", systemInfoService.getSystemInfoCountByCreator(userInfo.getUserInfoId()));
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                List<String> dates = new ArrayList<>();
                List<Integer> weeklyusers = new ArrayList<>();
                List<Integer> weeklylicense = new ArrayList<>();
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date todaysdate =cal.getTime();
                String today = dateFormat.format(cal.getTime());
                cal.add(Calendar.DATE, -6); 
                Date previousday = cal.getTime();
                while (todaysdate.compareTo(previousday) >= 0) {
                    String d = dateFormat.format(previousday);
                    Date date2 = dateFormat.parse(d);
                   
                    List<UserInfo> chartUserData = userInfoService.chartUserData(userInfo.getUserInfoId(), date2);
                    if(chartUserData!=null){
                        weeklyusers.add(chartUserData.size());
                        System.out.println(chartUserData.size());
                    }else{
                        weeklyusers.add(0);
                    }
                    List<UserLicense> chartLicenseData = userLicenseService.getUserLicenseForChart(userInfo.getUserInfoId(), date2);
                    if(chartLicenseData!=null){
                        weeklylicense.add(chartLicenseData.size());
                    }else{
                        weeklylicense.add(0);
                    }
                    String format=dateFormat.format(previousday);
                    dates.add(format);
                    cal.add(Calendar.DATE, 1); 
                    Date nextDay =cal.getTime();
                    previousday = nextDay;
                }
                modelMap.addAttribute("dates", dates);
                modelMap.addAttribute("weeklyusers", weeklyusers);
                modelMap.addAttribute("weeklylicense", weeklylicense);
                System.out.println(weeklyusers);
                System.out.println(weeklylicense);
                return new ModelAndView("clientdashboard");
            } else {
                return new ModelAndView("redirect:/");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will logout the user and invalidate the session of that user.
     * 
     * @param status
     * @param session
     * @param userName
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/logout")
    public ModelAndView logout(SessionStatus status, HttpSession session, @PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            System.out.println("LOGOUT....");
            session.invalidate();
            status.setComplete();
        }
        return new ModelAndView("redirect:/");
    }

    /**
     * This url is called if the user request to change the password,
     * the method will take the user to the changepassword page.
     * 
     * @param userName
     * @param modelMap
     * @return on page changepassword
     */
//    @GetMapping(value = "/{userName}/changepassword")
//    public ModelAndView getChangePassword(@PathVariable String userName, ModelMap modelMap) {
//        UserInfo userInfo = (UserInfo) modelMap.get("user");
//        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
//            return new ModelAndView("changepassword");
//        } else {
//            return new ModelAndView("redirect:/");
//        }
//    }

    /**
     * This method will get the current password and new password as the method parameter,
     * update the database entry and send a confirmation mail to the user.
     * 
     * @param userName
     * @param currentPassword
     * @param newPassword
     * @param request
     * @param model
     * @return String 
     * @throws NoSuchAlgorithmException
     */
    @PostMapping(value = "/{userName}/changepassword")
    @ResponseBody
    public String changePassword(@PathVariable String userName, @RequestParam String currentPassword,
            @RequestParam String newPassword, HttpServletRequest request, ModelMap model) throws NoSuchAlgorithmException {

        UserInfo userInfo = (UserInfo) model.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if(UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())){
                if (userInfoService.authenticate(userInfo.getUserName(), currentPassword, userInfo)) {
                    if ((userInfo = userInfoService.updatePassword(userInfo, newPassword)) != null) {
                        model.addAttribute("user", userInfo);
                        try {
                            mailController.sendEmail("Password Changes", "Your password is successfully changed.\n", userInfo.getEmailId(), request, userInfo);
                            return "success";
                        } catch (Exception e) {
                            e.printStackTrace();
                            return "error";
                        }
                    } else {
                        return "Unexpected error occur, Please try again later or contact to administrator.";
                    }
                } else {
                    return "Incorrect Current Password.";
                }
            }
            else if(UserType.Client.getUserTypeValue().equals(userInfo.getUserType())){
                if (userInfoService.authenticate(userInfo.getUserName(), currentPassword, userInfo)) {
                    if ((userInfo = userInfoService.updatePassword(userInfo, newPassword)) != null) {
                        model.addAttribute("user", userInfo);
                        try {
                            Boolean sendMail = mailController.sendMail("Password Changes", "Your password is successfully changed.\n", userInfo.getEmailId(), request, userInfo.getUserInfoId());
                            if(sendMail){
                              return "success";  
                            }else{
                                return "Password has been changed successfully but mail cannot be sent due to incomplete mail settings.";
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            return "error";
                        }
                    } else {
                        return "Unexpected error occur, Please try again later or contact to administrator.";
                    }
                } else {
                    return "Incorrect Current Password.";
                }
            }else{
                return "Unauthorised Access";
            }
        } else {
            return "Please login again.";
        }
    }

    /**
     * This method will show the profile page of user.
     * If the admin is in session then it return on admin_profile page,
     * And if client is in session then it return on profile page.
     * 
     * @param userName
     * @param modelMap
     * @return page profile
     */
    @GetMapping(value = "/{userName}/profile")
    public ModelAndView getProfile(@PathVariable String userName, ModelMap modelMap) throws InvalidInformationException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                modelMap.addAttribute("totalLicense",  adminLicenseService.getAdminLicenseCount());
                modelMap.addAttribute("totalCourse", courseService.getAllCourseCount());
                modelMap.addAttribute("totalSystem", systemInfoService.getSystemInfoCount());
                modelMap.addAttribute("userInfo", userInfoDao.getUserInfo(1));
                return new ModelAndView("admin_accountsetting");
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                modelMap.addAttribute("courses", courseService.getCourseById(userInfo.getUserInfoId()));
                AdminLicense adminLicenseByClient = adminLicenseService.getAdminLicenseByClient(userInfo.getUserInfoId());
                System.out.println("dekstop limit=="+adminLicenseByClient.getDesktopLicenseLimit());
                String systemInfo = new SystemInformation(adminLicenseByClient.getSystemInfoId()).getSystemInfo();
                
                modelMap.addAttribute("systemid", adminLicenseByClient.getSystemInfoId());
                String[] split = systemInfo.split("#");
                JSONObject jSONObject = new JSONObject(split[1]);
                modelMap.addAttribute("systemserialnumber", jSONObject.getString("serialNumber"));
                modelMap.addAttribute("systemmacid", jSONObject.getString("macId"));
                modelMap.addAttribute("systemid", adminLicenseByClient.getSystemInfoId());
                modelMap.addAttribute("adminlicense", adminLicenseByClient);
                modelMap.addAttribute("totalLicense", userLicenseService.getUserLicenseCountByCreator(userInfo.getUserInfoId()));
                modelMap.addAttribute("totalCourse", courseService.getCourseCount(userInfo.getUserInfoId()));
                modelMap.addAttribute("totalSystem", systemInfoService.getSystemInfoCountByCreator(userInfo.getUserInfoId()));
                Map<Integer, Integer> showUserIdThroughCount = userInfoDao.showClientIdThroughCount();
                Integer value = 0;
                for (Map.Entry<Integer, Integer> entry : showUserIdThroughCount.entrySet()) {
                    System.out.println("key="+entry.getKey()+"id="+userInfo.getUserInfoId());
                    if(Objects.equals(entry.getKey(), userInfo.getUserInfoId())){
                        System.out.println("value="+entry.getValue());
                        value = entry.getValue();
                    }
                }
                modelMap.addAttribute("USERID", 1000+value);
                return new ModelAndView("profile");
            } else {
                return new ModelAndView("redirect:/");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * get method to return in settings page when admin is in session.
     * 
     * @param userName
     * @param modelMap
     * @return on page admin_accountsetting
     */
    @GetMapping(value = "/{userName}/profileaccountsetting")
    public ModelAndView getProfileAccount(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("totalLicense",  adminLicenseService.getAdminLicenseCount());
                modelMap.addAttribute("totalCourse", courseService.getAllCourseCount());
                modelMap.addAttribute("totalSystem", systemInfoService.getSystemInfoCount());
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                return new ModelAndView("admin_accountsetting");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }
    /**
     * This method is to update userName.
     * If the new entered userName is already in use then it will show a error message,
     * Otherwise it will update the userName and send a confirmation mail to the user.
     * 
     * @param userName
     * @param newUserName
     * @param neworganization
     * @param request
     * @param newmobile
     * @param image
     * @param newemail
     * @param model
     * @return String
     * @throws NoSuchAlgorithmException
     */
    @PostMapping(value = "/{userName}/updateprofile")
    @ResponseBody
    public String updateProfile(@PathVariable String userName, @RequestParam String newUserName, @RequestParam String neworganization,
            @RequestParam String newmobile, @RequestParam String newemail, @RequestParam(name = "image", required = false) MultipartFile image,
            HttpServletRequest request, ModelMap model) throws NoSuchAlgorithmException {

        UserInfo userInfo = (UserInfo) model.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
                userInfo.setUserName(newUserName);
                userInfo.setOrganization(neworganization);
                userInfo.setMobileNumber(newmobile);
                userInfo.setEmailId(newemail);
                if ((userInfo = userInfoDao.updateUser(userInfo)) != null) {
                    model.addAttribute("user", userInfo);
                    try {
                        if(userInfo.getUserType() == 1){
                            mailController.sendEmail("Profile Updated", "Your profile is successfully updated.\n" , userInfo.getEmailId(), request, userInfo);
                            return "success";
                        } else{
                            Boolean sendMail = mailController.sendMail("Profile Update", "Your profile is successfully updated.\n", userInfo.getEmailId(), request, userInfo.getUserInfoId());
                            if(sendMail){
                                return "success#" + userInfo.getUserName();
                            } else{
                                return "Information has been updated successfully but Mail cannot be sent due to incomplete mail settings.";
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        return "error";
                    }
                } else {
                    return "Unexpected error occur, Please try again later or contact to administrator.";
                }
            
        } else {
            return "Please login again.";
        }
    }

    /**
     * If the admin is in session method will take the user on add_security_question page,
     * 
     * 
     * @param userName
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/setting")
    public ModelAndView getSecurityQuestion(@PathVariable String userName, ModelMap modelMap) throws Exception {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
//                modelMap.addAttribute("questions", securityQuestionDao.getSecurityQuestions());
                Integer logoutTime = userInfo.getLogoutTime();
                System.out.println("user="+userInfo);
                if(logoutTime == null){
                    modelMap.addAttribute("logouttime", "");
                } else{
                    modelMap.addAttribute("logouttime", userInfo.getLogoutTime());
                }
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
//                List<Notification> backupnotification = notificationDao.getNotificationByType(userInfo.getUserInfoId(), 9);
//                List<Notification> restorenotification = notificationDao.getNotificationByType(userInfo.getUserInfoId(), 10);
//                for (Notification notification : restorenotification) {
//                    backupnotification.add(notification);
//                }
                System.out.println("history map = "+backuphistory);
                modelMap.addAttribute("backuphistory", backuphistory);
                return new ModelAndView("add_security_question");
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                List<SecurityQuestion> securityQuestions = securityQuestionDao.getSecurityQuestions();
                List<SecurityQuestionAnswer> securityQuestionAnswers = securityQuestionAnswerDao.getSecurityQuestionAnswers(userInfo.getUserInfoId());
                Integer questionnumbers = 0;
                if(securityQuestions != null){
                    questionnumbers = securityQuestions.size();
                }
                modelMap.addAttribute("questions", securityQuestionAnswers);
                modelMap.addAttribute("answers", securityQuestionAnswers);
                AdminLicense adminLicenseByClient = adminLicenseService.getAdminLicenseByClient(userInfo.getUserInfoId());
                modelMap.addAttribute("licenseInfo", adminLicenseByClient);
                System.out.println("userinfo"+userInfo);
                modelMap.addAttribute("userinfo", userInfoDao.getUserInfo(userInfo.getUserInfoId()));
                MailSetting mailSettings = mailSettingDao.getMailSettingsById(userInfo.getUserInfoId());
//                String decryptedpassword = aes.decrypt(mailSettings.getSmtpPassword(), "0123456789abcdef");
//                mailSettings.setSmtpPassword(decryptedpassword.trim());
                System.out.println(mailSettings);
                modelMap.addAttribute("mailsetting", mailSettings);
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                return new ModelAndView("clientsettings");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This Method will add the security questions by admin. 
     * 
     * @param userName
     * @param question
     * @param request
     * @param model
     * @return String
     * @throws NoSuchAlgorithmException
     */
    @PostMapping(value = "/{userName}/addsecurityquestion")
    @ResponseBody
    public String addSecurityQuestion(@PathVariable String userName, @RequestParam String question,
            HttpServletRequest request, ModelMap model) throws NoSuchAlgorithmException {

        UserInfo userInfo = (UserInfo) model.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (question.isEmpty()) {
                return "Please enter question.";
            } else if (securityQuestionDao.getSecurityQuestions(question) == null) {

                SecurityQuestion securityQuestion = new SecurityQuestion();
                securityQuestion.setQuestion(question);
                if (securityQuestionDao.saveSecurityQuestions(securityQuestion) != null) {
                    return "success";
                } else {
                    return "Unexpected error occur, Please try again later or contact to administrator.";
                }
            } else {
                return "This question already added, Please enter another question.";
            }
        } else {
            return "Please login again.";
        }
    }

    /**
     * This method will add the list of questions at attribute and return on page securityquestionanswer
     * 
     * @param userName
     * @param modelMap
     * @return on page securityquestionanswer
     */
    @GetMapping(value = "/{userName}/securityquestionanswer")
    public ModelAndView getSecurityQuestionAnswer(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            modelMap.addAttribute("questions", securityQuestionDao.getSecurityQuestions());
            return new ModelAndView("securityquestionanswer");
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * 
     * @param userName
     * @param modelMap
     * @param questionid
     * @return a string of questions and answers.
     */
    @PostMapping(value = "/{userName}/getquestionanswer")
    @ResponseBody
    public String getQuestionAnswer(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer questionid){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                SecurityQuestion securityQuestionById = securityQuestionDao.getSecurityQuestionById(questionid);
                SecurityQuestionAnswer answerByQuestionId = securityQuestionAnswerDao.getAnswerByQuestionId(questionid, userInfo.getUserInfoId());
                System.out.println(securityQuestionById);
                System.out.println(answerByQuestionId);
                if(securityQuestionById != null){
                    if(answerByQuestionId == null){
                        return "success#" + securityQuestionById.getQuestion()+"*"+" ";
                    } else{ 
                        return "success#" + securityQuestionById.getQuestion()+"*"+answerByQuestionId.getAnswer();
                    }
                } else{
                    return "success#" + "No question Available"+"*"+" ";
                }
            }else{
                return "unauthorized access";
            }
        } else {
            return "login again";
        }
    }
    
    /**
     * 
     * @param userName
     * @param model
     * @param questionId
     * @param answer
     * @return response of editing security answer
     */
    @PostMapping(value = "/{userName}/editsecurityanswer")
    @ResponseBody
    public String editSecurityAnswer(@PathVariable String userName,ModelMap model,
            @RequestParam Integer questionId, @RequestParam String answer){
        UserInfo userInfo = (UserInfo) model.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (answer == null || answer.trim().isEmpty()) {
                return "Please Enter Answer";
            }else if(securityQuestionAnswerDao.getAnswerByQuestionId(questionId, userInfo.getUserInfoId())!=null){
                SecurityQuestionAnswer securityQuestionAnswer = securityQuestionAnswerDao.getAnswerByQuestionId(questionId, userInfo.getUserInfoId());
                securityQuestionAnswer.setAnswer(answer);
                securityQuestionAnswerDao.editAnswer(securityQuestionAnswer);
                return "success";
            }else{
                return "Unexpected error";
            }
        }else{
            return "Please login again.";
        }
    }
    /**
     * This method will store the questions and answers in database, 
     * these question answers will useful in case, when user forgot his password. 
     * 
     * @param userName
     * @param question1
     * @param answer1
     * @param question2
     * @param answer2
     * @param request
     * @param model
     * @return
     * @throws NoSuchAlgorithmException
     */
    @PostMapping(value = "/{userName}/addsecurityquestionanswer")
    @ResponseBody
    public String addSecurityQuestionAnswer(@PathVariable String userName,
            @RequestParam Integer question1, @RequestParam String answer1,
            @RequestParam Integer question2, @RequestParam String answer2,
            HttpServletRequest request, ModelMap model) throws NoSuchAlgorithmException {

        UserInfo userInfo = (UserInfo) model.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (question1 == null || question1 == 0) {
                return "Please select first question.";
            } else if (answer1 == null || answer1.trim().isEmpty()) {
                return "Please enter answer for first question.";
            } else if (question2 == null || question2 == 0) {
                return "Please select second question.";
            } else if (answer2 == null || answer2.trim().isEmpty()) {
                return "Please enter answer for second question.";
            } else if (Objects.equals(question1, question2)) {
                return "Both Questions are same please select different question.";
            }
            if (securityQuestionAnswerDao.getSecurityQuestionAnswers(userInfo.getUserInfoId()).isEmpty()) {
                SecurityQuestionAnswer securityQuestionAnswer = new SecurityQuestionAnswer();
                securityQuestionAnswer.setUserId(userInfo.getUserInfoId());
                securityQuestionAnswer.setSecurityQuestionId(question1);
                securityQuestionAnswer.setAnswer(answer1);
                if (securityQuestionAnswerDao.saveSecurityQuestionAnswers(securityQuestionAnswer) != null) {
                    SecurityQuestionAnswer securityQuestionAnswer1 = new SecurityQuestionAnswer();
                    securityQuestionAnswer1.setUserId(userInfo.getUserInfoId());
                    securityQuestionAnswer1.setSecurityQuestionId(question2);
                    securityQuestionAnswer1.setAnswer(answer2);
                    if (securityQuestionAnswerDao.saveSecurityQuestionAnswers(securityQuestionAnswer1) != null) {
                        return "success";
                    }
                }
                return "Unexpected error occur, Please try again later or contact to administrator.";
            } else {
                return "All question and answers are submitted.";
            }
        } else {
            return "Please login again.";
        }
    }

    /**
     * This method will upload the profile photo for user.
     * 
     * @param request
     * @param userName
     * @param model
     * @return String
     * @throws IOException
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @RequestMapping(value = "/{userName}/uploadprofilepic", method = RequestMethod.POST)
    public @ResponseBody
    String uploadProfilePic(MultipartHttpServletRequest request, @PathVariable String userName, ModelMap model) throws IOException {

        UserInfo userInfo = (UserInfo) model.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            Iterator<String> itr = request.getFileNames();

            if (itr.hasNext()) {
                MultipartFile image = request.getFile(itr.next());

                ServletContext servletContext = request.getSession().getServletContext();
                String profilePicturePath = servletContext.getRealPath("/");
                System.out.println("first path:::" + profilePicturePath);
                profilePicturePath = profilePicturePath.replace("\\", "/");
                File profilePictureFile = new File(profilePicturePath, "profilepic");
                if (!image.getContentType().contains("image")) {
                    return "Invalid image file.";
                }
                if (!profilePictureFile.exists()) {
                    profilePictureFile.mkdir();
                }
                System.out.println("second path:::" + profilePicturePath);

                byte[] fileData = image.getBytes();
                String extension = FilenameUtils.getExtension(image.getOriginalFilename());

                if (fileData.length != 0) {
                    String profilePic = image.getName() + "_" + new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(new Date()) + "_" + userName + "." + extension;
                    File profilePicFile = new File(profilePictureFile, profilePic);
                    try (FileOutputStream fileOutputStream = new FileOutputStream(profilePicFile)) {
                        fileOutputStream.write(fileData);
                        String imagePath = profilePicFile.getAbsolutePath().replace("\\", "/");
                        userInfo.setImagePath(imagePath.substring(imagePath.indexOf("/profilepic")));
                        UserInfo updateUser = userInfoDao.updateUser(userInfo);
                        model.addAttribute("user", updateUser);
                        return "success";
                    }
                } else {
                    return "File is empty";
                }
            } else {
                return "No file available";
            }
        } else {
            return "Please login again.";
        }
    }
    
    /**
     * Every client have to set mail settings to send mail to users registered by the client itself.
     * this  method will save the entries in database and return a appropriate response.
     * 
     * @param userName
     * @param modelMap
     * @param SMTPServer
     * @param SMTPUser
     * @param Password
     * @param Mailport
     * @param mailform
     * @param subject
     * @param text
     * @return response to save mail settings
     * @throws Exception
     */
    @PostMapping(value = "/{userName}/savemailsettings")
    @ResponseBody
    public String saveMailSettings(@PathVariable String userName, ModelMap modelMap, @RequestParam String SMTPServer, @RequestParam String SMTPUser,
            @RequestParam String Password, @RequestParam Integer Mailport, @RequestParam String mailform, @RequestParam String subject, @RequestParam String text) throws Exception{
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            MailSetting mailSettingsById = mailSettingDao.getMailSettingsById(userInfo.getUserInfoId());
            if(mailSettingsById == null){
                if (SMTPServer == null || SMTPServer.trim().isEmpty()) {
                    return "Please Enter SMTP Server.";
                } else if (SMTPUser == null || SMTPUser.trim().isEmpty()) {
                    return "Please Enter SMTP user.";
                } else if (Mailport == null || Mailport == 0) {
                    return "Please Enter Mail Port.";
                } else if (Password == null || Password.trim().isEmpty()) {
                    return "Please Enter Password.";
                } else if (mailform == null || mailform.trim().isEmpty()) {
                    return "Please Enter Mail Form.";
                } else {
                    MailSetting mailSetting = new MailSetting();
                    mailSetting.setSmtpServer(SMTPServer);
                    mailSetting.setSmtpUser(SMTPUser);
                    String generatedpassword = aes.generate(Password);
                    mailSetting.setSmtpPassword(generatedpassword);
                    mailSetting.setMailPort(Mailport);
                    mailSetting.setMailFrom(mailform);
                    mailSetting.setSubject(subject);
                    mailSetting.setText(text);
                    mailSetting.setUserId(userInfo.getUserInfoId());
                    mailSettingDao.saveCourse(mailSetting);
                    return "success";
                }
            } else{
                if (SMTPServer == null || SMTPServer.trim().isEmpty()) {
                    return "Please Enter SMTP Server.";
                } else if (SMTPUser == null || SMTPUser.trim().isEmpty()) {
                    return "Please Enter SMTP user.";
                } else if (Mailport == null || Mailport == 0) {
                    return "Please Enter Mail Port.";
                } else if (Password == null || Password.trim().isEmpty()) {
                    return "Please Enter Password.";
                } else if (mailform == null || mailform.trim().isEmpty()) {
                    return "Please Enter Mail Form.";
                } else {
                    mailSettingsById.setSmtpServer(SMTPServer);
                    mailSettingsById.setSmtpUser(SMTPUser);
                    System.out.println("PASSWORD"+Password);
                    String generatedpassword = aes.generate(Password);
                    mailSettingsById.setSmtpPassword(generatedpassword);
                    mailSettingsById.setMailPort(Mailport);
                    mailSettingsById.setMailFrom(mailform);
                    mailSettingsById.setSubject(subject);
                    mailSettingsById.setText(text);
                    mailSettingDao.updateCourse(mailSettingsById);
                    return "success";
                } 
            }
        }else{
            return "login again";
        }
    }
    
    /**
     * Method save the security key into database,
     * security key is related to FSS desktop application
     * 
     * @param userName
     * @param modelMap
     * @param securitykey
     * @return response
     */
    @PostMapping(value="/{userName}/addsecuritykey")
    @ResponseBody
    public String addSecurityKey(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer securitykey){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            UserInfo userInfo1 = userInfoDao.getUserInfo(userInfo.getUserInfoId());
            userInfo1.setSecurityKey(securitykey);
            if(userInfoDao.updateUser(userInfo1) != null){
                return "success";
            } else{
                return "fail";
            }
        }else{
            return "Please login again.";
        }
    }
    
    /**
     * This method will save a logout time for a particular client,
     * The session will destroy automatically after the specified time in minutes
     * 
     * @param userName
     * @param modelMap
     * @param logouttime
     * @return response
     */
    @PostMapping(value = "/{userName}/savelogouttime")
    @ResponseBody
    public String saveLogoutTime(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer logouttime){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            UserInfo userInfo1 = userInfoDao.getUserInfo(userInfo.getUserInfoId());
            userInfo1.setLogoutTime(logouttime);
            UserInfo updateUser = userInfoDao.updateUser(userInfo1);
            if(updateUser != null){
                modelMap.addAttribute("user", updateUser);
                return "success";
            } else{
                return "fail";
            }
        }else{
            return "Please login again.";
        }
    }
    
    /**
     * This method will manage and return the data of users to display in chart.
     * 
     * @param userName
     * @param modelMap
     * @param type
     * @return JSON
     * @throws ParseException
     */
    @PostMapping(value = "/{userName}/getuserDataforchart")
    @ResponseBody
    public String getUserDataForChart(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer type) throws ParseException{
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if(UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())){
                ArrayList<Integer> yaxis = new ArrayList<>();
                ArrayList<String> xaxis = new ArrayList<>();
                String title = "";
                if(type == 5){
                    title = "\"Weekly Clients\"";
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date todaysdate =cal.getTime();
                    String today = dateFormat.format(cal.getTime());
                    cal.add(Calendar.DATE, -6); 
                    Date previousday = cal.getTime();
                    while (todaysdate.compareTo(previousday) >= 0) {
                        Date date = dateFormat.parse(dateFormat.format(previousday));
                        System.out.println("parse date====="+date);
                        List<UserInfo> chartUserData = userInfoService.chartUserData(userInfo.getUserInfoId(), date);
                        if(chartUserData!=null){
                            yaxis.add(chartUserData.size());
                            System.out.println(chartUserData.size());
                        }else{
                            yaxis.add(0);
                        }
                        String format=dateFormat.format(previousday);
                        xaxis.add(format);
                        cal.add(Calendar.DATE, 1); 
                        Date nextDay =cal.getTime();
                        previousday = nextDay;
                    }
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                } else if(type == 4){
                    title = "\"Monthly Clients\"";
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat dfyy = new SimpleDateFormat("yyyy");
                    SimpleDateFormat dfmm = new SimpleDateFormat("MMM");
                    String format = dateFormat.format(cal.getTime());
                    Date first = dateFormat.parse(format);
                    cal.set(Calendar.DAY_OF_MONTH, 1);
                    String format1 = dateFormat.format(cal.getTime());
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    System.out.println(cal.getTime());
                    Date last = dateFormat.parse(format1);
                    System.out.println("fetching data from "+last+"to "+first);
                    Integer firstdata = userInfoDao.showUserChartData(last, first, 2, userInfo.getUserInfoId());
                    yaxis.add(firstdata);
                    
                    cal.add(Calendar.DAY_OF_WEEK, -1);
                    first = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    last = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+last+"to "+first);
                    Integer seconddata = userInfoDao.showUserChartData(last, first,2, userInfo.getUserInfoId());
                    yaxis.add(seconddata);
                    
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    first = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+first+"to "+last);
                    Integer thirddata = userInfoDao.showUserChartData(first, last,2, userInfo.getUserInfoId());
                    yaxis.add(thirddata);
                    
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    last = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+last+"to "+first);
                    Integer fourthdata = userInfoDao.showUserChartData(last, first,2, userInfo.getUserInfoId());
                    yaxis.add(fourthdata);
                    
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    first = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+first+"to "+last);
                    Integer fifthdata = userInfoDao.showUserChartData(first, last,2, userInfo.getUserInfoId());
                    yaxis.add(fifthdata);
                    
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    last = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+last+"to "+first);
                    Integer sixthdata = userInfoDao.showUserChartData(last, first,2, userInfo.getUserInfoId());
                    yaxis.add(sixthdata);
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                } else if(type == 3){
                    title = "\"Quarterly Clients\"";
                    Calendar cal = Calendar.getInstance();
                    Calendar cal1 = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat dfYY = new SimpleDateFormat("yy");
                    Date Today = dateFormat.parse(dateFormat.format(cal.getTime()));
                    cal1.set(Calendar.DAY_OF_YEAR, 1);
                    Date startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                    System.out.println(startdate);
                    while(Today.after(startdate)){
                        cal1.add(Calendar.MONTH, 3);
                        cal1.add(Calendar.DAY_OF_WEEK, -1);
                        Date nextdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                        if(Today.before(nextdate)){
                            Integer firstdata = userInfoDao.showUserChartData(startdate, Today, 2, userInfo.getUserInfoId());
                            yaxis.add(firstdata);
                            System.out.println("ifblock="+firstdata);
                            int month = cal.get(Calendar.MONTH) + 1;
                            int quarter = month % 3 == 0 ? (month / 3) : (month / 3) + 1;
                            xaxis.add("Q" + quarter + "-" + dfYY.format(cal.getTime()));
                            break;
                        }else{
                            System.out.println("fetching data from "+startdate+" to "+nextdate );
                            Integer firstdata = userInfoDao.showUserChartData(startdate,nextdate, 2, userInfo.getUserInfoId());
                            yaxis.add(firstdata);
                            System.out.println("elseblock="+firstdata);
                            int month = cal1.get(Calendar.MONTH) + 1;
                            int quarter = month % 3 == 0 ? (month / 3) : (month / 3) + 1;
                            xaxis.add("Q" + quarter + "-" + dfYY.format(cal.getTime()));
                            cal1.add(Calendar.DAY_OF_WEEK, 1);
                            startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                            System.out.println(startdate);
                        }
                    }
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                } else if(type == 2){
                    title = "\"Half-yearly Clients\"";
                    Calendar cal = Calendar.getInstance();
                    Calendar cal1 = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat dfYY = new SimpleDateFormat("yy");
                    Date today = dateFormat.parse(dateFormat.format(cal.getTime()));
                    cal1.set(Calendar.DAY_OF_YEAR, 1);
                    System.out.println("cal.get()"+cal1.getTime());
                    Date startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                    System.out.println("startdate"+startdate);
                    while(today.after(startdate)){
                        cal1.add(Calendar.MONTH, 6);
                        cal1.add(Calendar.DAY_OF_WEEK, -1);
                        System.out.println("Testing...."+cal1.getTime());
                        Date nextdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                        if(today.before(nextdate)){
                            System.out.println("fetching data from"+startdate+" to "+today);
                            Integer firstdata = userInfoDao.showUserChartData(startdate, today, 2, userInfo.getUserInfoId());
                            yaxis.add(firstdata);
                            int month = cal.get(Calendar.MONTH) + 1;
                            int halfyear = month % 6 == 0 ? (month / 6) : (month / 6) + 1;
                            xaxis.add("H" + halfyear + "-" + dfYY.format(cal.getTime()));
                            break;
                        } else{
                            System.out.println("fetching data from "+startdate+" to "+nextdate);
                            Integer firstdata = userInfoDao.showUserChartData(startdate, nextdate, 2, userInfo.getUserInfoId());
                            yaxis.add(firstdata);
                            int month = cal1.get(Calendar.MONTH) + 1;
                            int halfyear = month % 6 == 0 ? (month / 6) : (month / 6) + 1;
                            xaxis.add("H" + halfyear + "-" + dfYY.format(cal.getTime()));
                            cal1.add(Calendar.DAY_OF_WEEK, 1);
                            startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                        }
                    }
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                } else if(type == 1){
                    title = "\"Yearly Clients\"";
                    Calendar cal = Calendar.getInstance();
                    Calendar cal1 = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat dFYY = new SimpleDateFormat("yyyy");
                    Date today = dateFormat.parse(dateFormat.format(cal.getTime()));
                    cal1.set(Calendar.DAY_OF_YEAR, 1);
                    Date startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                    Integer firstdata = userInfoDao.showUserChartData(startdate, today, 2, userInfo.getUserInfoId());
                    yaxis.add(firstdata);
                    int year = cal.get(Calendar.YEAR);
                    System.out.println("YEAR=="+year);
                    xaxis.add("Y-"+year);
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                }
                String str = "";
                for(String value : xaxis){
                    str +="\""+value+"\",";
                }
                
                str = str.isEmpty()?"":str.substring(0,str.length()-1);
                return "{\"series\":[{\"name\":\"user\",\"data\":"+yaxis+"}],\"category\":["+str+"],\"title\":["+title+"]}";
            }
            else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                ArrayList<Integer> yaxis = new ArrayList<>();
                ArrayList<String> xaxis = new ArrayList<>();
                String title = "";
                if(type == 5){
                    title = "\"Weekly users\"";
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date todaysdate =cal.getTime();
                    String today = dateFormat.format(cal.getTime());
                    cal.add(Calendar.DATE, -6); 
                    Date previousday = cal.getTime();
                    while (todaysdate.compareTo(previousday) >= 0) {
                        String d = dateFormat.format(previousday);
                        Date date2 = dateFormat.parse(d);
                       
                        List<UserInfo> chartUserData = userInfoService.chartUserData(userInfo.getUserInfoId(), date2);
                        if(chartUserData!=null){
                            yaxis.add(chartUserData.size());
                            System.out.println(chartUserData.size());
                        }else{
                            yaxis.add(0);
                        }
                        String format=dateFormat.format(previousday);
                        xaxis.add(format);
                        cal.add(Calendar.DATE, 1); 
                        Date nextDay =cal.getTime();
                        previousday = nextDay;
                    }
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                }else if(type == 4){
                    title = "\"Monthly users\"";
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat dfyy = new SimpleDateFormat("yyyy");
                    SimpleDateFormat dfmm = new SimpleDateFormat("MMM");
                    String format = dateFormat.format(cal.getTime());
                    Date first = dateFormat.parse(format);
                    cal.set(Calendar.DAY_OF_MONTH, 1);
                    String format1 = dateFormat.format(cal.getTime());
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    System.out.println(cal.getTime());
                    Date last = dateFormat.parse(format1);
                    System.out.println("fetching data from "+last+"to "+first);
                    Integer firstdata = userInfoDao.showUserChartData(last, first, 3, userInfo.getUserInfoId());
                    yaxis.add(firstdata);
                    
                    cal.add(Calendar.DAY_OF_WEEK, -1);
                    first = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    last = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+last+"to "+first);
                    Integer seconddata = userInfoDao.showUserChartData(last, first,3, userInfo.getUserInfoId());
                    yaxis.add(seconddata);
                    
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    first = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+first+"to "+last);
                    Integer thirddata = userInfoDao.showUserChartData(first, last,3, userInfo.getUserInfoId());
                    yaxis.add(thirddata);
                    
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    last = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+last+"to "+first);
                    Integer fourthdata = userInfoDao.showUserChartData(last, first,3, userInfo.getUserInfoId());
                    yaxis.add(fourthdata);
                    
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    first = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+first+"to "+last);
                    Integer fifthdata = userInfoDao.showUserChartData(first, last,3, userInfo.getUserInfoId());
                    yaxis.add(fifthdata);
                    
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    last = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+last+"to "+first);
                    Integer sixthdata = userInfoDao.showUserChartData(last, first,3, userInfo.getUserInfoId());
                    yaxis.add(sixthdata);
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                } else if(type == 3){
                    title = "\"Quarterly users\"";
                    Calendar cal = Calendar.getInstance();
                    Calendar cal1 = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat dfYY = new SimpleDateFormat("yy");
                    Date Today = dateFormat.parse(dateFormat.format(cal.getTime()));
                    cal1.set(Calendar.DAY_OF_YEAR, 1);
                    Date startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                    System.out.println(startdate);
                    while(Today.after(startdate)){
                        cal1.add(Calendar.MONTH, 3);
                        cal1.add(Calendar.DAY_OF_WEEK, -1);
                        Date nextdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                        if(Today.before(nextdate)){
                            Integer firstdata = userInfoDao.showUserChartData(startdate, Today,3, userInfo.getUserInfoId());
                            yaxis.add(firstdata);
                            System.out.println("ifblock="+firstdata);
                            int month = cal.get(Calendar.MONTH) + 1;
                            int quarter = month % 3 == 0 ? (month / 3) : (month / 3) + 1;
                            xaxis.add("Q" + quarter + "-" + dfYY.format(cal.getTime()));
                            break;
                        }else{
                            System.out.println("fetching data from "+startdate+" to "+nextdate );
                            Integer firstdata = userInfoDao.showUserChartData(startdate,nextdate,3, userInfo.getUserInfoId());
                            yaxis.add(firstdata);
                            System.out.println("elseblock="+firstdata);
                            int month = cal1.get(Calendar.MONTH) + 1;
                            int quarter = month % 3 == 0 ? (month / 3) : (month / 3) + 1;
                            xaxis.add("Q" + quarter + "-" + dfYY.format(cal.getTime()));
                            cal1.add(Calendar.DAY_OF_WEEK, 1);
                            startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                            System.out.println(startdate);
                        }
                    }
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                } else if(type == 2){
                    title = "\"Half-Yearly users\"";
                    Calendar cal = Calendar.getInstance();
                    Calendar cal1 = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat dfYY = new SimpleDateFormat("yy");
                    Date today = dateFormat.parse(dateFormat.format(cal.getTime()));
                    cal1.set(Calendar.DAY_OF_YEAR, 1);
                    System.out.println("cal.get()"+cal1.getTime());
                    Date startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                    System.out.println("startdate"+startdate);
                    while(today.after(startdate)){
                        cal1.add(Calendar.MONTH, 6);
                        cal1.add(Calendar.DAY_OF_WEEK, -1);
                        System.out.println("Testing...."+cal1.getTime());
                        Date nextdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                        if(today.before(nextdate)){
                            System.out.println("fetching data from"+startdate+" to "+today);
                            Integer firstdata = userInfoDao.showUserChartData(startdate, today, 3, userInfo.getUserInfoId());
                            yaxis.add(firstdata);
                            int month = cal.get(Calendar.MONTH) + 1;
                            int halfyear = month % 6 == 0 ? (month / 6) : (month / 6) + 1;
                            xaxis.add("H" + halfyear + "-" + dfYY.format(cal.getTime()));
                            break;
                        } else{
                            System.out.println("fetching data from "+startdate+" to "+nextdate);
                            Integer firstdata = userInfoDao.showUserChartData(startdate, nextdate, 3, userInfo.getUserInfoId());
                            yaxis.add(firstdata);
                            int month = cal1.get(Calendar.MONTH) + 1;
                            int halfyear = month % 6 == 0 ? (month / 6) : (month / 6) + 1;
                            xaxis.add("H" + halfyear + "-" + dfYY.format(cal.getTime()));
                            cal1.add(Calendar.DAY_OF_WEEK, 1);
                            startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                        }
                    }
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                } else if(type == 1){
                    title = "\"Yearly users\"";
                    Calendar cal = Calendar.getInstance();
                    Calendar cal1 = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat dFYY = new SimpleDateFormat("yyyy");
                    Date today = dateFormat.parse(dateFormat.format(cal.getTime()));
                    cal1.set(Calendar.DAY_OF_YEAR, 1);
                    Date startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                    Integer firstdata = userInfoDao.showUserChartData(startdate, today, 3, userInfo.getUserInfoId());
                    yaxis.add(firstdata);
                    int year = cal.get(Calendar.YEAR);
                    System.out.println("YEAR=="+year);
                    xaxis.add("Y-"+year);
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                }
                String str = "";
                for(String value : xaxis){
                    str +="\""+value+"\",";
                }
                
                str = str.isEmpty()?"":str.substring(0,str.length()-1);
                return "{\"series\":[{\"name\":\"user\",\"data\":"+yaxis+"}],\"category\":["+str+"],\"title\":["+title+"]}";
            } else{
                return "unauthorised Access";
            }
        }else{
            return "Please login again.";
        }
    }
    
    /**
     * This method will manage and return the data of license to display in chart.
     * 
     * @param userName
     * @param modelMap
     * @param type
     * @return
     * @throws ParseException
     */
    @PostMapping(value = "/{userName}/getlicenseDataforchart")
    @ResponseBody
    public String getLicenseDataForChart(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer type) throws ParseException{
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if(UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())){
                ArrayList<Integer> yaxis = new ArrayList<>();
                ArrayList<String> xaxis = new ArrayList<>();
                String title = "";
                if(type == 5){
                    title = "\"Weekly License\"";
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date todaysdate =cal.getTime();
                    String today = dateFormat.format(cal.getTime());
                    cal.add(Calendar.DATE, -6); 
                    Date previousday = cal.getTime();
                    while (todaysdate.compareTo(previousday) >= 0) {
                        Date date2 = dateFormat.parse(dateFormat.format(previousday));
                        System.out.println("parse date====="+date2);
                        List<UserLicense> chartLicenseData = userLicenseService.getUserLicenseForChart(userInfo.getUserInfoId(), date2);
                        if(chartLicenseData!=null){
                            yaxis.add(chartLicenseData.size());
                        }else{
                            yaxis.add(0);
                        }
                        String format=dateFormat.format(previousday);
                        xaxis.add(format);
                        cal.add(Calendar.DATE, 1); 
                        Date nextDay =cal.getTime();
                        previousday = nextDay;
                    }
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                } else if(type == 4){
                    title = "\"Monthly License\"";
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat dfyy = new SimpleDateFormat("yyyy");
                    SimpleDateFormat dfmm = new SimpleDateFormat("MMM");
                    String format = dateFormat.format(cal.getTime());
                    Date first = dateFormat.parse(format);
                    cal.set(Calendar.DAY_OF_MONTH, 1);
                    String format1 = dateFormat.format(cal.getTime());
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    System.out.println(cal.getTime());
                    Date last = dateFormat.parse(format1);
                    System.out.println("fetching data from "+last+"to "+first);
                    List<AdminLicense> adminLicenseList = adminLicenseDao.getAdminLicenseListByCreateOn(last, first); 
                    if(adminLicenseList != null){
                        yaxis.add(adminLicenseList.size());
                    } else{
                        yaxis.add(0);
                    }
                    
                    cal.add(Calendar.DAY_OF_WEEK, -1);
                    first = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    last = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+last+"to "+first);
                    List<AdminLicense> adminLicenseList1 = adminLicenseDao.getAdminLicenseListByCreateOn(last, first); 
                    if(adminLicenseList1 != null){
                        yaxis.add(adminLicenseList1.size());
                    } else{
                        yaxis.add(0);
                    }
                    
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    first = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+first+"to "+last);
                    List<AdminLicense> adminLicenseList3 = adminLicenseDao.getAdminLicenseListByCreateOn(first, last); 
                    if(adminLicenseList3 != null){
                        yaxis.add(adminLicenseList3.size());
                    } else{
                        yaxis.add(0);
                    }
                    
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    last = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+last+"to "+first);
                    List<AdminLicense> adminLicenseList4 = adminLicenseDao.getAdminLicenseListByCreateOn(last, first); 
                    if(adminLicenseList4 != null){
                        yaxis.add(adminLicenseList4.size());
                    } else{
                        yaxis.add(0);
                    }
                    
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    first = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+first+"to "+last);
                    List<AdminLicense> adminLicenseList5 = adminLicenseDao.getAdminLicenseListByCreateOn(first, last); 
                    if(adminLicenseList5 != null){
                        yaxis.add(adminLicenseList5.size());
                    } else{
                        yaxis.add(0);
                    }
                    
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    last = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+last+"to "+first);
                    List<AdminLicense> adminLicenseList6 = adminLicenseDao.getAdminLicenseListByCreateOn(last, first); 
                    if(adminLicenseList6 != null){
                        yaxis.add(adminLicenseList6.size());
                    } else{
                        yaxis.add(0);
                    }
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                } else if(type == 3){
                    title = "\"Quarterly License\"";
                    Calendar cal = Calendar.getInstance();
                    Calendar cal1 = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat dfYY = new SimpleDateFormat("yy");
                    Date Today = dateFormat.parse(dateFormat.format(cal.getTime()));
                    cal1.set(Calendar.DAY_OF_YEAR, 1);
                    Date startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                    System.out.println(startdate);
                    while(Today.after(startdate)){
                        cal1.add(Calendar.MONTH, 3);
                        cal1.add(Calendar.DAY_OF_WEEK, -1);
                        Date nextdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                        if(Today.before(nextdate)){
                            List<AdminLicense> adminLicenseList = adminLicenseDao.getAdminLicenseListByCreateOn(startdate, Today);
                            if(adminLicenseList != null){
                                yaxis.add(adminLicenseList.size());
                            } else{
                                yaxis.add(0);
                            }
                            System.out.println("ifblock=");
                            int month = cal.get(Calendar.MONTH) + 1;
                            int quarter = month % 3 == 0 ? (month / 3) : (month / 3) + 1;
                            xaxis.add("Q" + quarter + "-" + dfYY.format(cal.getTime()));
                            break;
                        }else{
                            System.out.println("fetching data from "+startdate+" to "+nextdate );
                            List<AdminLicense> adminLicenseList = adminLicenseDao.getAdminLicenseListByCreateOn(startdate, nextdate); 
                            if(adminLicenseList != null){
                                yaxis.add(adminLicenseList.size());
                            } else{
                               yaxis.add(0);
                            }
                            System.out.println("elseblock=");
                            int month = cal1.get(Calendar.MONTH) + 1;
                            int quarter = month % 3 == 0 ? (month / 3) : (month / 3) + 1;
                            xaxis.add("Q" + quarter + "-" + dfYY.format(cal.getTime()));
                            cal1.add(Calendar.DAY_OF_WEEK, 1);
                            startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                            System.out.println(startdate);
                        }
                    }
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                } else if(type == 2){
                    title = "\"Half-Yearly License\"";
                    Calendar cal = Calendar.getInstance();
                    Calendar cal1 = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat dfYY = new SimpleDateFormat("yy");
                    Date today = dateFormat.parse(dateFormat.format(cal.getTime()));
                    cal1.set(Calendar.DAY_OF_YEAR, 1);
                    System.out.println("cal.get()"+cal1.getTime());
                    Date startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                    System.out.println("startdate"+startdate);
                    while(today.after(startdate)){
                        cal1.add(Calendar.MONTH, 6);
                        cal1.add(Calendar.DAY_OF_WEEK, -1);
                        System.out.println("Testing...."+cal1.getTime());
                        Date nextdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                        if(today.before(nextdate)){
                            System.out.println("fetching data from"+startdate+" to "+today);
                            List<AdminLicense> adminLicenseList = adminLicenseDao.getAdminLicenseListByCreateOn(startdate, today);
                            if(adminLicenseList != null){
                                yaxis.add(adminLicenseList.size());
                            } else{
                                yaxis.add(0);
                            }
                            int month = cal.get(Calendar.MONTH) + 1;
                            int halfyear = month % 6 == 0 ? (month / 6) : (month / 6) + 1;
                            xaxis.add("H" + halfyear + "-" + dfYY.format(cal.getTime()));
                            break;
                        } else{
                            System.out.println("fetching data from "+startdate+" to "+nextdate);
                            List<AdminLicense> adminLicenseList = adminLicenseDao.getAdminLicenseListByCreateOn(startdate, nextdate); 
                            if(adminLicenseList != null){
                                yaxis.add(adminLicenseList.size());
                            } else{
                               yaxis.add(0);
                            }
                            int month = cal1.get(Calendar.MONTH) + 1;
                            int halfyear = month % 6 == 0 ? (month / 6) : (month / 6) + 1;
                            xaxis.add("H" + halfyear + "-" + dfYY.format(cal.getTime()));
                            cal1.add(Calendar.DAY_OF_WEEK, 1);
                            startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                        }
                    }
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                } else if(type == 1){
                    title = "\"Yearly License\"";
                    Calendar cal = Calendar.getInstance();
                    Calendar cal1 = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat dFYY = new SimpleDateFormat("yyyy");
                    Date today = dateFormat.parse(dateFormat.format(cal.getTime()));
                    cal1.set(Calendar.DAY_OF_YEAR, 1);
                    Date startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                    List<AdminLicense> adminLicenseList = adminLicenseDao.getAdminLicenseListByCreateOn(startdate, today);
                    if(adminLicenseList != null){
                        yaxis.add(adminLicenseList.size());
                    } else{
                        yaxis.add(0);
                    }
                    int year = cal.get(Calendar.YEAR);
                    System.out.println("YEAR=="+year);
                    xaxis.add("Y-"+year);
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                }
                String str = "";
                for(String value : xaxis){
                    str +="\""+value+"\",";
                }
                str = str.isEmpty()?"":str.substring(0,str.length()-1);
                return "{\"series\":[{\"name\":\"license\",\"data\":"+yaxis+"}],\"category\":["+str+"],\"title\":["+title+"]}";
            }else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                ArrayList<Integer> yaxis = new ArrayList<>();
                ArrayList<String> xaxis = new ArrayList<>();
                String title = "";
                if(type == 5){
                    title = "\"Weekly License\"";
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date todaysdate =cal.getTime();
                    String today = dateFormat.format(cal.getTime());
                    cal.add(Calendar.DATE, -6); 
                    Date previousday = cal.getTime();
                    while (todaysdate.compareTo(previousday) >= 0) {
                        Date date2 = dateFormat.parse(dateFormat.format(previousday));
                        System.out.println("parse date====="+date2);
                        List<UserLicense> chartLicenseData = userLicenseService.getUserLicenseForChart(userInfo.getUserInfoId(), date2);
                        if(chartLicenseData!=null){
                            yaxis.add(chartLicenseData.size());
                        }else{
                            yaxis.add(0);
                        }
                        String format=dateFormat.format(previousday);
                        xaxis.add(format);
                        cal.add(Calendar.DATE, 1); 
                        Date nextDay =cal.getTime();
                        previousday = nextDay;
                    }
                } else if(type == 4){
                    title = "\"Monthly License\"";
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat dfyy = new SimpleDateFormat("yyyy");
                    SimpleDateFormat dfmm = new SimpleDateFormat("MMM");
                    String format = dateFormat.format(cal.getTime());
                    Date first = dateFormat.parse(format);
                    cal.set(Calendar.DAY_OF_MONTH, 1);
                    String format1 = dateFormat.format(cal.getTime());
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    System.out.println(cal.getTime());
                    Date last = dateFormat.parse(format1);
                    System.out.println("fetching data from "+last+"to "+first);
                    Integer firstdata = userLicenseDao.showLicenseChartData(last, first, userInfo.getUserInfoId());
                    yaxis.add(firstdata);
                    
                    cal.add(Calendar.DAY_OF_WEEK, -1);
                    first = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    last = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+last+"to "+first);
                    Integer seconddata = userLicenseDao.showLicenseChartData(last, first, userInfo.getUserInfoId());
                    yaxis.add(seconddata);
                    
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    first = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+first+"to "+last);
                    Integer thirddata = userLicenseDao.showLicenseChartData(first, last, userInfo.getUserInfoId());
                    yaxis.add(thirddata);
                    
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    last = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+last+"to "+first);
                    Integer fourthdata = userLicenseDao.showLicenseChartData(last, first, userInfo.getUserInfoId());
                    yaxis.add(fourthdata);
                    
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    first = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+first+"to "+last);
                    Integer fifthdata = userLicenseDao.showLicenseChartData(first, last, userInfo.getUserInfoId());
                    yaxis.add(fifthdata);
                    
                    System.out.println((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    xaxis.add((cal.get(Calendar.MONTH)+1)+"-"+dfyy.format(cal.getTime()));
                    cal.add(Calendar.MONTH, -1);
                    last = dateFormat.parse(dateFormat.format(cal.getTime()));
                    System.out.println("fetching data from "+last+"to "+first);
                    Integer sixthdata = userLicenseDao.showLicenseChartData(last, first, userInfo.getUserInfoId());
                    yaxis.add(sixthdata);
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                } else if(type == 3){
                    title = "\"Quarterly License\"";
                    Calendar cal = Calendar.getInstance();
                    Calendar cal1 = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat dfYY = new SimpleDateFormat("yy");
                    Date Today = dateFormat.parse(dateFormat.format(cal.getTime()));
                    cal1.set(Calendar.DAY_OF_YEAR, 1);
                    Date startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                    System.out.println(startdate);
                    while(Today.after(startdate)){
                        cal1.add(Calendar.MONTH, 3);
                        cal1.add(Calendar.DAY_OF_WEEK, -1);
                        Date nextdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                        if(Today.before(nextdate)){
                            Integer firstdata = userLicenseDao.showLicenseChartData(startdate, Today, userInfo.getUserInfoId());
                            yaxis.add(firstdata);
                            System.out.println("ifblock="+firstdata);
                            int month = cal.get(Calendar.MONTH) + 1;
                            int quarter = month % 3 == 0 ? (month / 3) : (month / 3) + 1;
                            xaxis.add("Q" + quarter + "-" + dfYY.format(cal.getTime()));
                            break;
                        }else{
                            System.out.println("fetching data from "+startdate+" to "+nextdate );
                            Integer firstdata = userLicenseDao.showLicenseChartData(startdate,nextdate, userInfo.getUserInfoId());
                            yaxis.add(firstdata);
                            System.out.println("elseblock="+firstdata);
                            int month = cal1.get(Calendar.MONTH) + 1;
                            int quarter = month % 3 == 0 ? (month / 3) : (month / 3) + 1;
                            xaxis.add("Q" + quarter + "-" + dfYY.format(cal.getTime()));
                            cal1.add(Calendar.DAY_OF_WEEK, 1);
                            startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                            System.out.println(startdate);
                        }
                    }
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                } else if(type == 2){
                    title = "\"Half-Yearly License\"";
                    Calendar cal = Calendar.getInstance();
                    Calendar cal1 = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat dfYY = new SimpleDateFormat("yy");
                    Date today = dateFormat.parse(dateFormat.format(cal.getTime()));
                    cal1.set(Calendar.DAY_OF_YEAR, 1);
                    System.out.println("cal.get()"+cal1.getTime());
                    Date startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                    System.out.println("startdate"+startdate);
                    while(today.after(startdate)){
                        cal1.add(Calendar.MONTH, 6);
                        cal1.add(Calendar.DAY_OF_WEEK, -1);
                        System.out.println("Testing...."+cal1.getTime());
                        Date nextdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                        if(today.before(nextdate)){
                            System.out.println("fetching data from"+startdate+" to "+today);
                            Integer firstdata = userLicenseDao.showLicenseChartData(startdate, today, userInfo.getUserInfoId());
                            yaxis.add(firstdata);
                            int month = cal.get(Calendar.MONTH) + 1;
                            int halfyear = month % 6 == 0 ? (month / 6) : (month / 6) + 1;
                            xaxis.add("H" + halfyear + "-" + dfYY.format(cal.getTime()));
                            break;
                        } else{
                            System.out.println("fetching data from "+startdate+" to "+nextdate);
                            Integer firstdata = userLicenseDao.showLicenseChartData(startdate, nextdate, userInfo.getUserInfoId());
                            yaxis.add(firstdata);
                            int month = cal1.get(Calendar.MONTH) + 1;
                            int halfyear = month % 6 == 0 ? (month / 6) : (month / 6) + 1;
                            xaxis.add("H" + halfyear + "-" + dfYY.format(cal.getTime()));
                            cal1.add(Calendar.DAY_OF_WEEK, 1);
                            startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                        }
                    }
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                } else if(type == 1){
                    title = "\"Yearly License\"";
                    Calendar cal = Calendar.getInstance();
                    Calendar cal1 = Calendar.getInstance();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat dFYY = new SimpleDateFormat("yyyy");
                    Date today = dateFormat.parse(dateFormat.format(cal.getTime()));
                    cal1.set(Calendar.DAY_OF_YEAR, 1);
                    Date startdate = dateFormat.parse(dateFormat.format(cal1.getTime()));
                    Integer firstdata = userLicenseDao.showLicenseChartData(startdate, today, userInfo.getUserInfoId());
                    yaxis.add(firstdata);
                    int year = cal.get(Calendar.YEAR);
                    System.out.println("YEAR=="+year);
                    xaxis.add("Y-"+year);
                    System.out.println(yaxis);
                    System.out.println(xaxis);
                }
                String str = "";
                for(String value : xaxis){
                    str +="\""+value+"\",";
                }
                str = str.isEmpty()?"":str.substring(0,str.length()-1);
                return "{\"series\":[{\"name\":\"license\",\"data\":"+yaxis+"}],\"category\":["+str+"],\"title\":["+title+"]}";
            } else{
                return "unauthorised Access";
            }
        }else{
            return "Please login again.";
        }
    }
    
    /**
     * this method is to delete security question on admin's choice.
     * 
     * @param userName
     * @param questionId
     * @param modelMap
     * @return response
     */
    @PostMapping(value = "/{userName}/deletesecurityquestion")
    @ResponseBody
    public String deleteSecurityQuestion(@PathVariable String userName, @RequestParam Integer questionId, ModelMap modelMap){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                System.out.println("Question = "+questionId);
                List<SecurityQuestionAnswer> securityQuestionAnswers = securityQuestionAnswerDao.getSecurityQuestionAnswersByQuestionId(questionId);
                if(securityQuestionAnswers == null || securityQuestionAnswers.isEmpty()){
                    if(securityQuestionDao.deleteSecurityQuestions(questionId)){
                        return "success";
                    } else{
                        return "Unable to delete System.";
                    }
                }else{
                    return "This security Question can not delete, id is in use.";
                }
            }else{
                return "Unauthorised Access";
            }
        }else{
            return "Please login again.";
        }
    }
    
    /**
     * This method is to take backup of current database.
     * 
     * @param userName
     * @param modelMap
     * @param request
     * @param response
     * @throws URISyntaxException
     */
    @GetMapping(value = "/{userName}/takedbbackup")
    public void takeBackUpOfDB(@PathVariable String userName, ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException{
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            Process p;
            try {
                ServletContext servletContext = request.getSession().getServletContext();
                String appPath = servletContext.getRealPath("/");
                
//                CodeSource codeSource = ProfileController.class.getProtectionDomain().getCodeSource();
//                File jarFile = new File(codeSource.getLocation().toURI().getPath());
//                String appPath = jarFile.getParentFile().getPath();
                String folderPath = appPath + "\\backup";
                File f1 = new File(folderPath);
                f1.mkdir();
//                folderPath = folderPath + "\\backup";
//                File f2 = new File(folderPath);
//                f2.mkdir();
                String savePath =  appPath + "\\backup\\" + "backup.sql";
                System.out.println("path="+savePath);
                Runtime runtime = Runtime.getRuntime();
                String executeCmd = "mysqldump -u root -padmin fss -r "+savePath;
//                p= runtime.exec(new String[] { "cmd.exe", "/c", executeCmd });
                p = runtime.exec(executeCmd);
                //change the dbpass and dbname with your dbpass and dbname
                int processComplete = p.waitFor();
                System.out.println("p===="+p);
                if (processComplete == 0) {
                    System.out.println("Backup created successfully!");
                } else {
                    System.out.println("Could not create the backup");
                }
                String mimeType = servletContext.getMimeType(savePath);
                if (mimeType == null) {
                    mimeType = "application/octet-stream";
                }            
                 
                // set content properties and header attributes for the response
//                
                savePath = savePath.replace("\\\\", "\\");
                System.out.println(savePath.replace("\"", ""));
                FileInputStream fins = new FileInputStream(savePath);
                response.getOutputStream();
                response.setContentType(mimeType);
                String headerKey = "Content-Disposition";
                    String headerValue = String.format("attachment; filename=\"%s\"",
                          "Database back up file.sql" );
                      response.setHeader(headerKey, headerValue);
                    IOUtils.copy(fins, response.getOutputStream());
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    System.out.println("new date = "+new Date() +"format = "+dateFormat.format(new Date())); 
                    backuphistory.put(dateFormat.format(new Date()), "Backup Created");
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * This method is to restore a selected sql file into our database.
     * 
     * @param userName
     * @param modelMap
     * @param servletrequest
     * @param request
     * @param response
     * @return
     * @throws URISyntaxException
     */
    @RequestMapping(value = "/{userName}/restoredb",method = RequestMethod.POST )
    @ResponseBody
    public String restoreDB(@PathVariable String userName, ModelMap modelMap, HttpServletRequest servletrequest, MultipartHttpServletRequest request, HttpServletResponse response) throws URISyntaxException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            Process p;
            try {
                Iterator<String> itr = request.getFileNames();
                MultipartFile dbfile = request.getFile(itr.next());
                System.out.println(dbfile);
                if (dbfile != null && !dbfile.isEmpty()) {
                    if (dbfile.getOriginalFilename().substring(dbfile.getOriginalFilename().lastIndexOf(".") + 1).equals("sql")) {
                        System.out.println("restoring...");
                        ServletContext servletContext = servletrequest.getSession().getServletContext();
                        String appPath = servletContext.getRealPath("/");
                        System.out.println("dsadsa"+appPath);
                        
                        String folderPath = appPath + "//restore";
                        File f1 = new File(folderPath);
                        if (!f1.exists()) {
                            f1.mkdir();
                        }
                        System.out.println("folderPath"+folderPath);
                        
                        String savePath;
                        String host = "localhost";
                        String port = "8084";
                        String user = "root";
                        String password = "admin";
                        String dbname = "fss";
                        savePath = "backupfile.sql";
                        Path filePath = Paths.get(f1.getAbsolutePath(), savePath);
                        System.out.println("f1.getAbsolutePath()"+f1.getAbsolutePath());
                        Files.deleteIfExists(filePath);
                        Files.copy(dbfile.getInputStream(), filePath);
                        savePath = appPath + "//restore//" + "backupfile.sql";
                        // String command = "mysql --host=" + host + " --port=" + port + " --user=" + user + " --password=" + password + " " + dbname + " " + table + " < " + folder + table + ".sql";
                        System.out.println("dsadsa"+appPath);
                        System.out.println(savePath.replace("//", "/"));

                        return restore(user, password, dbname, savePath);

                    } else {
                        return "Please select a sql file.";
                    }
                } else {
                    return "";
                }
            } catch (IOException e) {
                e.printStackTrace();
                return "error";
            }
        } else {
            return "login again";
        }
    }
    
    
    private String restore(String username, String password, String dbName, String filename) {
        try {
            final String MYSQL_UTIL = "mysql";

            String[] executeCmd = new String[]{MYSQL_UTIL, "--user=" + username, "--password=" + password, dbName,"-e",
                    " source "+filename};

            Runtime runtime = Runtime.getRuntime();
            System.out.println("executeCmd"+executeCmd.toString());
            Process restoreProcess = runtime.exec(executeCmd);
            int processComplete = restoreProcess.waitFor();

            if (processComplete == 0) {
                return "Restored successfully!";
            } else {
                
                return "Could not restore the backup!";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return "Could not restore the backup!";
        }
    }
}
