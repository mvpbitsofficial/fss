
<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>FSS | User Profile</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top"> 
            <!-- BEGIN HEADER INNER -->
            <jsp:include page="topbar.jsp" />
            <!-- END HEADER INNER --> 
        </div>
        <!-- END HEADER --> 
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER --> 
        <!-- BEGIN CONTAINER -->
        <div class="page-container"> 
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper"> 
                <!-- BEGIN SIDEBAR --> 
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse"> 
                    <!-- BEGIN SIDEBAR MENU --> 
                    <jsp:include page="sidebar.jsp" />
                    <!--                    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                                             DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
                                            <li class="sidebar-toggler-wrapper hide"> 
                                                 BEGIN SIDEBAR TOGGLER BUTTON 
                                                <div class="sidebar-toggler"> <span></span> </div>
                                                 END SIDEBAR TOGGLER BUTTON  
                                            </li>
                                            <li class="nav-item start"> <a href="dashboard" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
                                            <li class="nav-item  "> <a href="viewuser" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
                    
                                            </li>
                                            <li class="nav-item  "> <a href="viewlicense" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">License</span>  </a>
                    
                                            </li>
                                            <li class="nav-item  "> <a href="viewcourse" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
                    
                                            </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
                                            <li class="nav-item  "> <a href="licensehistory" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
                                            <li class="nav-item  "> <a href="viewwatermark" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
                                        </ul>-->
                    <!-- END SIDEBAR MENU --> 
                    <!-- END SIDEBAR MENU --> 
                </div>
                <!-- END SIDEBAR --> 
            </div>
            <!-- END SIDEBAR --> 
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper"> 
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content"> 
                    <!-- BEGIN PAGE HEADER--> 

                    <!-- BEGIN PAGE TITLE-->
                    <h3 class="page-title"> User Profile 
                        <!-- <small>blank page layout</small>--> 
                    </h3>
                    <!-- END PAGE TITLE--> 
                    <!-- END PAGE HEADER-->


                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PROFILE SIDEBAR -->
                            <div class="profile-sidebar">
                                <!-- PORTLET MAIN -->
                                <div class="portlet light profile-sidebar-portlet bordered box-shadow">
                                    <!-- SIDEBAR USERPIC -->
                                    <div class="profile-userpic">
                                        <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/teambg1.jpg" class="img-responsive" alt=""> </div>
                                    <!-- END SIDEBAR USERPIC -->
                                    <!-- SIDEBAR USER TITLE -->
                                    <div class="profile-usertitle">
                                        <div class="profile-usertitle-name"> ${user.userName} </div>
                                        <div class="profile-usertitle-job" style="text-transform:none;"> <a href="#">${user.emailId}</a> </div>
                                    </div>
                                    <!-- END SIDEBAR USER TITLE -->
                                    <!-- SIDEBAR BUTTONS -->
                                    <!--<div class="profile-userbuttons">
                                        <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                                        <button type="button" class="btn btn-circle red btn-sm">Message</button>
                                    </div>-->
                                    <!-- END SIDEBAR BUTTONS -->
                                    <!-- SIDEBAR MENU -->
                                    <div class="profile-usermenu">
                                        <ul class="nav">
                                            <li class="active">
                                                <a href="${pageContext.request.contextPath}/${user.userName}/profile">
                                                    <i class="icon-home"></i> Overview </a>
                                            </li>
                                            <li>
                                                <a href="${pageContext.request.contextPath}/${user.userName}/profileaccountsetting">
                                                    <i class="icon-settings"></i> Account Settings </a>
                                            </li>

                                        </ul>
                                    </div>
                                    <!-- END MENU -->
                                </div>
                                <!-- END PORTLET MAIN -->
                                <!-- PORTLET MAIN -->
                                <div class="portlet light bordered box-shadow">
                                    <!-- STAT -->
                                    <div class="row list-separated ">
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <div class="uppercase profile-stat-title"> ${totalCourse} </div>
                                            <div class="uppercase profile-stat-text"> Course </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <div class="uppercase profile-stat-title"> ${totalLicense} </div>
                                            <div class="uppercase profile-stat-text"> License </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <div class="uppercase profile-stat-title"> ${totalSystem} </div>
                                            <div class="uppercase profile-stat-text"> System </div>
                                        </div>
                                    </div>
                                    <!-- END STAT -->
                                    <!--<div>
                                        <h4 class="profile-desc-title">About Marcus Doe</h4>
                                        <span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span>
                                        <div class="margin-top-20 profile-desc-link">
                                            <i class="fa fa-globe"></i>
                                            <a href="http://www.keenthemes.com">www.keenthemes.com</a>
                                        </div>
                                        <div class="margin-top-20 profile-desc-link">
                                            <i class="fa fa-twitter"></i>
                                            <a href="http://www.twitter.com/keenthemes/">@keenthemes</a>
                                        </div>
                                        <div class="margin-top-20 profile-desc-link">
                                            <i class="fa fa-facebook"></i>
                                            <a href="http://www.facebook.com/keenthemes/">keenthemes</a>
                                        </div>
                                    </div>-->
                                </div>
                                <!-- END PORTLET MAIN -->
                            </div>
                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content">

                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- BEGIN PORTLET -->
                                        <div class="portlet light bordered box-shadow">
                                            <div class="portlet-title">
                                                <div class="caption caption-md">
                                                    <i class="icon-bar-chart theme-font hide"></i>
                                                    <span class="caption-subject font-blue-madison bold uppercase">Customer Support</span>
                                                    <span class="caption-helper">45 pending</span>
                                                </div>
                                                <div class="inputs">
                                                    <div class="portlet-input input-inline input-small ">
                                                        <div class="input-icon right">
                                                            <i class="icon-magnifier"></i>
                                                            <input type="text" class="form-control form-control-solid" placeholder="search..."> </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="scroller" style="height: 422px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                                    <div class="general-item-list">
                                                        <div class="item">
                                                            <div class="item-head">
                                                                <div class="item-details">
                                                                    <img class="item-pic" src="${pageContext.request.contextPath}/assets/pages/media/users/avatar4.jpg">
                                                                    <a href="" class="item-name primary-link">Nick Larson</a>
                                                                    <span class="item-label">3 hrs ago</span>
                                                                </div>
                                                                <span class="item-status">
                                                                    <span class="badge badge-empty badge-success"></span> Open</span>
                                                            </div>
                                                            <div class="item-body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="item-head">
                                                                <div class="item-details">
                                                                    <img class="item-pic" src="${pageContext.request.contextPath}/assets/pages/media/users/avatar3.jpg">
                                                                    <a href="" class="item-name primary-link">Mark</a>
                                                                    <span class="item-label">5 hrs ago</span>
                                                                </div>
                                                                <span class="item-status">
                                                                    <span class="badge badge-empty badge-warning"></span> Pending</span>
                                                            </div>
                                                            <div class="item-body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat tincidunt ut laoreet. </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="item-head">
                                                                <div class="item-details">
                                                                    <img class="item-pic" src="${pageContext.request.contextPath}/assets/pages/media/users/avatar6.jpg">
                                                                    <a href="" class="item-name primary-link">Nick Larson</a>
                                                                    <span class="item-label">8 hrs ago</span>
                                                                </div>
                                                                <span class="item-status">
                                                                    <span class="badge badge-empty badge-primary"></span> Closed</span>
                                                            </div>
                                                            <div class="item-body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh. </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="item-head">
                                                                <div class="item-details">
                                                                    <img class="item-pic" src="${pageContext.request.contextPath}/assets/pages/media/users/avatar7.jpg">
                                                                    <a href="" class="item-name primary-link">Nick Larson</a>
                                                                    <span class="item-label">12 hrs ago</span>
                                                                </div>
                                                                <span class="item-status">
                                                                    <span class="badge badge-empty badge-danger"></span> Pending</span>
                                                            </div>
                                                            <div class="item-body"> Consectetuer adipiscing elit Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="item-head">
                                                                <div class="item-details">
                                                                    <img class="item-pic" src="${pageContext.request.contextPath}/assets/pages/media/users/avatar9.jpg">
                                                                    <a href="" class="item-name primary-link">Richard Stone</a>
                                                                    <span class="item-label">2 days ago</span>
                                                                </div>
                                                                <span class="item-status">
                                                                    <span class="badge badge-empty badge-danger"></span> Open</span>
                                                            </div>
                                                            <div class="item-body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, ut laoreet dolore magna aliquam erat volutpat. </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="item-head">
                                                                <div class="item-details">
                                                                    <img class="item-pic" src="${pageContext.request.contextPath}/assets/pages/media/users/avatar8.jpg">
                                                                    <a href="" class="item-name primary-link">Dan</a>
                                                                    <span class="item-label">3 days ago</span>
                                                                </div>
                                                                <span class="item-status">
                                                                    <span class="badge badge-empty badge-warning"></span> Pending</span>
                                                            </div>
                                                            <div class="item-body"> Lorem ipsum dolor sit amet, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="item-head">
                                                                <div class="item-details">
                                                                    <img class="item-pic" src="${pageContext.request.contextPath}/assets/pages/media/users/avatar2.jpg">
                                                                    <a href="" class="item-name primary-link">Larry</a>
                                                                    <span class="item-label">4 hrs ago</span>
                                                                </div>
                                                                <span class="item-status">
                                                                    <span class="badge badge-empty badge-success"></span> Open</span>
                                                            </div>
                                                            <div class="item-body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END PORTLET -->
                                    </div>

                                </div>

                            </div>
                            <!-- END PROFILE CONTENT -->
                        </div>
                    </div>


                </div>
                <!-- END CONTENT BODY --> 
            </div>
            <!-- END CONTENT --> 
            <!-- BEGIN QUICK SIDEBAR --> 
            <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 

            <!-- END QUICK SIDEBAR --> 
        </div>
        <!-- END CONTAINER --> 
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; Organization </div>
            <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
        </div>
        <!-- END FOOTER --> 

        <!-- BEGIN CORE PLUGINS --> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
        <!-- END CORE PLUGINS --> 
        <!-- BEGIN THEME GLOBAL SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
        <!-- END THEME GLOBAL SCRIPTS --> 
        <!-- BEGIN THEME LAYOUT SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
        <!-- END THEME LAYOUT SCRIPTS --> 

        <script>
            $(document).ready(function () {
                $('#side-menu').find('li').removeClass('active');
            });
        </script>

    </body>
</html>