<%-- 
    Document   : changepassword
    Created on : 9 Aug, 2016, 2:24:32 PM
    Author     : CoreTechies M8
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>FSS | Add System</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
         <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
         <link href="${pageContext.request.contextPath}/assets/global/plugins/TextInputEffects/css/set1.css" rel="stylesheet" type="text/css" />
        
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
 <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top"> 
 <!-- BEGIN HEADER INNER -->
 <jsp:include page="topbar.jsp" />
 <!-- END HEADER INNER --> 
</div>
<!-- END HEADER --> 
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER --> 
<!-- BEGIN CONTAINER -->
<div class="page-container"> 
 <!-- BEGIN SIDEBAR -->
 <div class="page-sidebar-wrapper"> 
  <!-- BEGIN SIDEBAR --> 
  <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
  <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
  <div class="page-sidebar navbar-collapse collapse"> 
   <!-- BEGIN SIDEBAR MENU --> 
   <jsp:include page="sidebar.jsp" />
<!--   <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
     DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
    <li class="sidebar-toggler-wrapper hide"> 
      BEGIN SIDEBAR TOGGLER BUTTON 
     <div class="sidebar-toggler"> <span></span> </div>
      END SIDEBAR TOGGLER BUTTON  
    </li>
    <li class="nav-item  "> <a href="index.html" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
    <li class="nav-item  "> <a href="users.html" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
     
    </li>
    <li class="nav-item  "> <a href="licence.html" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">Licence</span>  </a>
     
    </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
     
    </li>
    <li class="nav-item  "> <a href="report.html" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
    <li class="nav-item  "> <a href="watermark.html" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
    <li class="nav-item  "> <a href="settings.html" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
   </ul>-->
   <!-- END SIDEBAR MENU --> 
   <!-- END SIDEBAR MENU --> 
  </div>
  <!-- END SIDEBAR --> 
 </div>
 <!-- END SIDEBAR --> 
 <!-- BEGIN CONTENT -->
 <div class="page-content-wrapper"> 
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content"> 
   <!-- BEGIN PAGE HEADER--> 
   
   <!-- BEGIN PAGE TITLE-->
   <!--<h3 class="page-title"> Licence 
    <!-- <small>blank page layout</small>-- 
   </h3>-->
   <!-- END PAGE TITLE--> 
   <!-- END PAGE HEADER-->
   
 
  <div class="row">
         <div class="col-sm-12">
         	<div class="portlet red box  portlet-datatable  ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" fa fa-desktop "></i>
                                        <span class="caption-subject sbold uppercase">Add System</span>
                                    </div>
                                    <!--<div class="actions">
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-refresh"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-printer"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-docs"></i>
                                        </a>
                                         
                                    </div>-->
                                </div>
                                <div class="portlet-body clearfix">
                                    <div class="col-sm-6 clearfix" style="float:none; margin:0 auto;">
                                    	<form:form action="#" class="form-horizontal">
                                            <div class="form-group clearfix">
                                                <table class="user-img-table">
                                                    <tr>
                                                        <td>
                                                            <div class="form-group clearfix">
                                                              
                                                            </div>
                                                            <div class="form-group clearfix">
                                                                <label class="col-sm-4 control-label">User Name</label>
                                                                <div class="col-sm-8">
                                                                    <div class="input-group">
                                                                        <div class="input-group-btn">
                                                                          <button type="button" class="btn btn-default dropdown-toggle input-circle-left" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> </button>
                                                                            <div class="dropdown-menu hold-on-click dropdown-checkboxes">
                                                                                <label class="mt-checkbox">
                                                                                    <input type="checkbox" class="" id="getinfobyid" checked/>
                                                                                        User ID <span></span> </label>
                                                                                <label class="mt-checkbox">
                                                                                    <input type="checkbox" class="" id="getinfobyname"/>
                                                                                        User Name <span></span> </label>
                                                                                <label class="mt-checkbox ">
                                                                                    <input type="checkbox" class="" id="getinfobyemail" />
                                                                                        Email <span></span> </label>
                                                                                <label class="mt-checkbox ">
                                                                                    <input type="checkbox" class="" id="getinfobymobile" />
                                                                                        Mobile Number<span></span> </label>
                                                                            </div>
<!--                                                                          <ul class="dropdown-menu radio_dropdown">
                                                                              <li class="radio">
                                                                                  <label>
                                                                                      <input type="checkbox" name="radioname" class="group-checkable" id="nameradio" value="1" onchange="getinfobyname()" > UserName
                                                                                  </label>
                                                                               </li>
                                                                              <li>
                                                                                  <label>
                                                                                      <input type="checkbox" name="radioname" class="group-checkable" id="emailradio" value="2" onchange="getinfobyemail()"> Email
                                                                                  </label>
                                                                              </li>
                                                                              <li>
                                                                                  <label>
                                                                                      <input type="checkbox" name="radioname" class="group-checkable" id="mobileradio" value="3" onchange="getinfobymobile()"> Mobile no.
                                                                                  </label>
                                                                              </li>

                                                                          </ul>-->
                                                                        </div><!-- /btn-group -->
                                                                        <select id="userId" class="form-control input-circle-right selectpicker" data-live-search='true'>
                                                                              <option value="" >Select user</option>
                                                                          </select>
                                                                      </div><!-- /input-group -->
<!--                                                                    <div class="input-group clearfix">
                                                                        <span class="input-group-addon input-circle-left">
                                                                        <select id="selectdisplay" class="input-group-addon input-circle-left" onchange=" return getUser();" >
                                                                            <option value="" disabled selected>Search By</option>
                                                                            <option value="1">UserName</option>
                                                                            <option value="2">Email</option>
                                                                            <option value="3">Mobile number</option>
                                                                        </select>
                                                                        </span>
                                                                            <span class="input-group-addon input-circle-left">
                                                                                <select class="form-control">
                                                                                <option value=""><i class="fa fa-filter"></i></option>
                                                                                </select>
                                                                            </span>
                                                                        <select id="userId" class="form-control input-circle-right">
                                                                            <option value="">Select user</option>
                                                                        </select>  
                                                                    </div>-->
                                                                    <span class="error-message" id="errorMessageUserId"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group clearfix">
                                                                <label class="col-sm-4 control-label">System Name</label>
                                                                <div class="col-sm-8">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                                <i class="fa fa-desktop"></i>
                                                                        </span>
                                                                        <input type="text" id="systemName" class="form-control input-circle-right" placeholder="Enter A name for your System"/>
                                                                    </div>
                                                                    <span class="error-message" id="errorMessageSystemName"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group clearfix">
                                                                <label class="col-sm-4 control-label">System ID</label>
                                                                <div class="col-sm-8 mobile-number-input ">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                                <i class="fa fa-desktop"></i>
                                                                        </span>
                                                                            <input type="text" id="systemId" class="form-control input-circle-right" placeholder="Enter System ID"/>
                                                                    </div>
                                                                    <span class="error-message" id="errorMessageSystemId"></span>
                                                                </div>
                                                            </div>
                                                            <div id="loading" style="display: none;">
                                                                <center>
                                                                    <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/24.gif" />
                                                                </center>
                                                            </div>
                                                            <div class="form-actions">
                                                            <div class="row">
                                                            <div class="col-sm-4"></div>	
                                                                <div class="col-sm-8 text-right">
                                                                    <span class="error-message" id="errorMessageAddSystem"></span>
                                                                    <span style="color: green; display: block; font-size: 14px; float: left;" id="successMessageAddSystem"></span>
                                                                    <br/>
                                                                    <button type="button" id="addSystem" class="btn btn-circle btn-success">Add System</button> 
                                                                </div>
                                                            </div>
                                                            </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                        </form:form>
                                    </div>
                                </div>
                            </div>
         </div>               
  </div>
   
   
  </div>
  <!-- END CONTENT BODY -->  
 </div>
 <!-- END CONTENT --> 
 <!-- BEGIN QUICK SIDEBAR --> 
 <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 
 
 <!-- END QUICK SIDEBAR --> 
</div>
<!-- END CONTAINER --> 
<!-- BEGIN FOOTER -->
<div class="page-footer">
 <div class="page-footer-inner"> 2016 &copy; Organization </div>
 <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
</div>
<!-- END FOOTER --> 


<!--Create User Modal 
<div class="modal fade" id="create_user_modal" tabindex="-1" role="dialog" aria-labelledby="create_user_modalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      
      <div class="modal-body">
	  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <table class="create_user_modal_table">
			<tr>
				<td>Company Name</td>
				<td>XYZ</td>
			</tr>
			<tr>
				<td>Mail ID</td>
				<td>XYZ@demo.com</td>
			</tr>
			<tr>
				<td>Login ID</td>
				<td>ABC@demo.com</td>
			</tr>
			<tr>
				<td>Password</td>
				<td>Z#1571a</td>
			</tr>
		</table>
      </div>
      <div class="modal-footer">
        <div class="pull-left">
			<button type="button" class="btn btn-sm btn-default" title="Copy"><i class="fa fa-copy"></i></button>
			<button type="button" class="btn btn-sm btn-default" title="Send Mail"><i class="icon-envelope-open"></i></button>
		</div>
        <button type="button" class="btn  btn-sm btn-success">Login</button>
      </div>
    </div>
  </div>
</div>-->


<!-- BEGIN CORE PLUGINS --> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
<!-- END CORE PLUGINS --> 
<!-- BEGIN THEME GLOBAL SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
<!-- END THEME GLOBAL SCRIPTS --> 
<!-- BEGIN THEME LAYOUT SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
<!-- END THEME LAYOUT SCRIPTS --> 
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
      
<!-- END PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<script src="${pageContext.request.contextPath}/assets/pages/scripts/profile.min.js" type="text/javascript"></script>
         
<script src="${pageContext.request.contextPath}/assets/global/plugins/TextInputEffects/js/classie.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery-1.10.2.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/global/scripts/bootstrap-select.min.js" type="text/javascript"></script> 

        <script>
            $("#userId").on("change", function(){
                var userId = $("#userId").select().val();
                if (userId === null || userId === "") {
                    $("#errorMessageUserId").html("Please select a user.");
                } else{
                    $("#errorMessageUserId").html("");
                }
            });
            
            $("#systemName").on("change", function(){
                var systemName = $("#systemName").val().trim();
                if (systemName === null || systemName === "") {
                    $("#errorMessageSystemName").html("Please enter system name.");
                } else{
                    $("#errorMessageSystemName").html("");
                }
            });
            
            $("#systemId").on("change", function(){
                var systemId = $("#systemId").val().trim();
                if (systemId === null || systemId === "") {
                    $("#errorMessageSystemId").html("Please enter system Id.");
                } else{
                    $("#errorMessageSystemId").html("");
                }
            });
        </script>

        <script>
            $("#addSystem").on("click", function () {
                var userId = $("#userId").select().val();
                var systemName = $("#systemName").val().trim();
                var systemId = $("#systemId").val().trim();
                $("#errorMessageUserId").html("");
                $("#errorMessageSystemName").html("");
                $("#errorMessageSystemId").html("");
                $("#errorMessageAddSystem").html("");
                var flag = true;
                if (userId === null || userId === "") {
                    $("#errorMessageUserId").html("Please select a user.");
                    flag = false;
                }
                if (systemName === null || systemName === "") {
                    $("#errorMessageSystemName").html("Please enter system name.");
                    flag = false;
                }
                if (systemId === null || systemId === "") {
                    $("#errorMessageSystemId").html("Please enter system Id.");
                    flag = false;
                }
                if (flag) {
                    $("#loading").css('display','block');
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/addsystem", { userId: userId, systemName: systemName, systemId: systemId},
                            function (data, status) {
                                if (data === "success") {
                                    $("#loading").css("display","none");
                                    $("#successMessageAddSystem").text("System has been added successfully.");
                                    setTimeout(function()
                                        {
                                            window.location.href = "${pageContext.request.contextPath}/"+Uname+"/viewsystem";
                                        }, 5000);
                                } else {
                                    $("#loading").css("display","none");
                                    $("#errorMessageAddSystem").html(data);
                                }
                            }
                    );
                }
            });
        </script>
        <script>
            function getUser(){
                var e = document.getElementById("selectdisplay");
                var strUser = e.options[e.selectedIndex].value;
                console.log(strUser);
                var values = [];
                var Uname = "${user.userName}".replace(" ", "");
                $.get("${pageContext.request.contextPath}/"+Uname+"/getusers", { type: strUser},
                        function(data, status) {
                            if(data.indexOf("success") !== -1){
                                values= data.split("#");
                                for(i=1; i < values.length; i++){
                                    $("#"+i).text(values[i]);
                                    console.log(values[i]);
                                }
                            }    
                        });
            }
        </script>
         <!--         <script>
			(function() {
				// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
				if (!String.prototype.trim) {
					(function() {
						// Make sure we trim BOM and NBSP
						var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
						String.prototype.trim = function() {
							return this.replace(rtrim, '');
						};
					})();
				}

				[].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
					// in case the input is already filled..
					if( inputEl.value.trim() !== '' ) {
						classie.add( inputEl.parentNode, 'input--filled' );
					}

					// events:
					inputEl.addEventListener( 'focus', onInputFocus );
					inputEl.addEventListener( 'blur', onInputBlur );
				} );

				function onInputFocus( ev ) {
					classie.add( ev.target.parentNode, 'input--filled' );
				}

				function onInputBlur( ev ) {
					if( ev.target.value.trim() === '' ) {
						classie.remove( ev.target.parentNode, 'input--filled' );
					}
				}
			})();
		</script>-->
         
        <script>
        $( ".form-control" ).focus(function() {
            $(this).prev('.input-group-addon').removeClass().addClass('input-group-addon-focus');
            $(this).next('.input-group-addon').removeClass().addClass('input-group-addon-focus');
        });

        $( ".form-control" ).focusout(function() {
            $(this).prev('.input-group-addon-focus').removeClass().addClass('input-group-addon');
            $(this).next('.input-group-addon-focus').removeClass().addClass('input-group-addon');
        });
        </script>
                <script>
                    $(document).ready(function () {
                        $('#side-menu').find('li').removeClass('active');
                        $(document).find(".system").addClass('active');
                        
                        var Uname = "${user.userName}".replace(" ", "");
                        $.post("${pageContext.request.contextPath}/"+Uname+"/getdropdownuserinfo", {selectBy: 4},
                            function(data, success){
                                console.log(data);
                                $("#displayuserdetails").hide();
                                var dropdownList = "<option selected disabled>Select user By User ID</option>";
                                if (data.indexOf("success") !== -1) {
                                    obj = JSON.parse(data.split("#")[1]);
                                    var shareInfoLen = obj.length;
                                    for(var i = 0; i < shareInfoLen; i++){
            //                            console.log( obj[i].userId +"," + obj[i].userName);
                                        dropdownList+='<option value="' + obj[i].userId + '">' + obj[i].displayId + '</option>';
                                    }
                                    console.log(dropdownList);
                                    $("#userId").html(dropdownList);
                                    $("#userId").selectpicker('refresh');
                                } else {
                                    $("#errorMessageUserId").text(data);
                                }
                            });
                    });
                </script>
            <script>
            $("#getinfobyname").on("click", function(){
                if($("#getinfobyname").is(":checked")){
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getdropdownuserinfo", {selectBy: 1},
                    function(data, success){
                        console.log(data);
                        $("#displayuserdetails").hide();
                        var dropdownList = "<option selected disabled>Select user By User Name</option>";
                        if (data.indexOf("success") !== -1) {
                            obj = JSON.parse(data.split("#")[1]);
                            var shareInfoLen = obj.length;
                            for(var i = 0; i < shareInfoLen; i++){
    //                            console.log( obj[i].userId +"," + obj[i].userName);
                                dropdownList+='<option value="' + obj[i].userId + '">' + obj[i].userName + '</option>';
                            }
                            console.log(dropdownList);
                            $("#userId").html(dropdownList);
                            $("#userId").selectpicker('refresh');
                        } else {
                            $("#errorMessageUserId").text(data);
                        }
                    });
                } else{
                    $("#userId").html("<option value=''>Select user</option>");
                    $("#userId").selectpicker('refresh');
                }
            });
            
            $("#getinfobyid").on("click", function(){
                if($("#getinfobyid").is(":checked")){
                    $("#userid").val("");
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getdropdownuserinfo", {selectBy: 4},
                    function(data, success){
                        console.log(data);
                        var dropdownList = "<option selected disabled>Select user By User ID</option>";
                        if (data.indexOf("success") !== -1) {
                            obj = JSON.parse(data.split("#")[1]);
                            var shareInfoLen = obj.length;
                            for(var i = 0; i < shareInfoLen; i++){
    //                            console.log( obj[i].userId +"," + obj[i].userName);
                                dropdownList+='<option value="' + obj[i].userId + '">' + obj[i].displayId + '</option>';
                            }
                            console.log(dropdownList);
                            $("#userId").html(dropdownList);
                            $("#userId").selectpicker('refresh');
                        } else {
                            $("#userError").text(data);
                        }
                    });
                } else{
                    $("#userId").html("<option selected disabled>--Select user--</option>");
                    $("#userId").selectpicker('refresh');
                }
            });
            
            $("#getinfobyemail").on("click", function(){
                if($("#getinfobyemail").is(":checked")){
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getdropdownuserinfo", {selectBy: 2},
                    function(data, success){
                        console.log(data);
                        $("#displayuserdetails").hide();
                        var dropdownList = "<option value=''>Select user By Email Address</option>";
                        if (data.indexOf("success") !== -1) {
                            obj = JSON.parse(data.split("#")[1]);
                            var shareInfoLen = obj.length;
                            for(var i = 0; i < shareInfoLen; i++){
    //                            console.log( obj[i].userId +"," + obj[i].userName);
                                dropdownList+='<option value="' + obj[i].userId + '">' + obj[i].emailId + '</option>';
                            }
                            console.log(dropdownList);
                            $("#userId").html(dropdownList);
                            $("#userId").selectpicker('refresh');
                        } else {
                            $("#errorMessageUserId").text(data);
                        }
                    });
                } else{
                    $("#userId").html("<option value=''>Select user</option>");
                    $("#userId").selectpicker('refresh');
                }
            }); 
            
            $("#getinfobymobile").on("click", function(){
                if($("#getinfobymobile").is(":checked")){
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}"+Uname+"${user.userName}/getdropdownuserinfo", {selectBy: 3},
                    function(data, success){
                        console.log(data);
                        $("#displayuserdetails").hide();
                        var dropdownList = "<option value=''>Select user By Mobile Number</option>";
                        if (data.indexOf("success") !== -1) {
                            obj = JSON.parse(data.split("#")[1]);
                            var shareInfoLen = obj.length;
                            for(var i = 0; i < shareInfoLen; i++){
    //                            console.log( obj[i].userId +"," + obj[i].userName);
                                dropdownList+='<option value="' + obj[i].userId + '">' + obj[i].mobileNumber + '</option>';
                            }
                            console.log(dropdownList);
                            $("#userId").html(dropdownList);
                            $("#userId").selectpicker('refresh');
                        } else {
                            $("#errorMessageUserId").text(data);
                        }
                    });
                } else{
                    $("#userId").html("<option value=''>Select user</option>");
                    $("#userId").selectpicker('refresh');
                }
            });
            
            $("#getinfobyname").on("click", function(){
                if($("#getinfobyname").is(":checked")){
                    $("#getinfobyemail").attr('checked', false);
                    $("#getinfobymobile").attr('checked', false);
                    $("#getinfobyid").attr('checked', false);
                }
            });
            $("#getinfobyemail").on("click", function(){
                if($("#getinfobyemail").is(":checked")){
                    $("#getinfobyname").attr('checked', false);
                    $("#getinfobymobile").attr('checked', false);
                    $("#getinfobyid").attr('checked', false);
                }
            });
            $("#getinfobymobile").on("click", function(){
                if($("#getinfobymobile").is(":checked")){
                    $("#getinfobyemail").attr('checked', false);
                    $("#getinfobyname").attr('checked', false);
                    $("#getinfobyid").attr('checked', false);
                }
            });
            $("#getinfobyid").on("click", function(){
                if($("#getinfobyid").is(":checked")){
                    $("#getinfobyemail").attr('checked', false);
                    $("#getinfobyname").attr('checked', false);
                    $("#getinfobymobile").attr('checked', false);
                }
            });
        </script>
</body>
</html>