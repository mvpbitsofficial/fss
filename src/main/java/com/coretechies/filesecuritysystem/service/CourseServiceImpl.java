/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.service;

import com.coretechies.filesecuritysystem.algorithm.NotificationType;
import com.coretechies.filesecuritysystem.dao.CourseDao;
import com.coretechies.filesecuritysystem.dao.NotificationDao;
import com.coretechies.filesecuritysystem.domain.Course;
import com.coretechies.filesecuritysystem.domain.Notification;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseDao courseDao;

    @Autowired
    private NotificationDao notificationDao;
     
    @Override
    public Course saveCourse(String courseId, String courseName, String courseDescription, Integer userId) {
        Course course = new Course();
        course.setCourseName(courseName);
        course.setCourseDescription(courseDescription);
        course.setUserId(userId);
        course.setCourseId(courseId);
        if ((course = courseDao.saveCourse(course)) != null) {
//            Integer id = course.getId() + 1000;
//            if(courseName.length() > 7){
//                course.setCourseId("FSS" + courseName.replace(" ", "").substring(0, 7).toUpperCase() + id);
//            } else{
//                course.setCourseId("FSS" + courseName.replace(" ", "").toUpperCase() + id);
//            }
//            course = courseDao.updateCourse(course);
            Notification notification = new Notification();
            notification.setCreateOn(new Date());
            notification.setCreatedBy(userId);
            notification.setNotificationType(NotificationType.Course_History.getNotificationTypeValue());
            notification.setSeenby("0,");
            notification.setNotificationMessage("Course "+course.getCourseName()+" is Uploaded"); 
            notificationDao.saveNotification(notification);
        }
        return course;
    }

    @Override
    public List<Course> getCourseById(Integer userId){
        return courseDao.getCourses(userId);
    }
    
    @Override
    public Integer getCourseCount(Integer userInfoId) {
        List<Course> courses = courseDao.getCourses(userInfoId);
        if (courses != null) {
            return courses.size();
        } else {
            return 0;
        }
    }
    
    @Override
    public Integer getAllCourseCount(){
        List<Course> courses = courseDao.getCourseList();
        if(courses != null){
            return courses.size();
        }else{
            return 0;
        }
    }
    
    @Override
    @Transactional
    public boolean deleteCourse(Integer id){
        Course courseById = courseDao.getCourseById(id);
        boolean deleteCourse = courseDao.deleteCourse(id);
        if(deleteCourse){
            Notification notification = new Notification();
            notification.setNotificationType(NotificationType.Course_Delete_History.getNotificationTypeValue());
            notification.setNotificationMessage("Course "+courseById.getCourseName()+" is Deleted");
            notification.setCreateOn(new Date());
            notification.setCreatedBy(courseById.getUserId());
            notification.setSeenby("0,");
            notificationDao.saveNotification(notification);
            return true;
        } else{
            return false;
        }
    }
}