/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.Notification;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author CoreTechies M8
 */
public interface NotificationDao {
    
    public Notification saveNotification(Notification notification);
    
    public List<Notification> getNotificationForClient(Integer userId);
    
    public List<Notification> getNotificationForAdmin();

    public List<Notification> getNotification(Integer userInfoId, Date fromDate, Date toDate);
    
    public List<Notification> getNotificationByType(Integer userId, Integer type);
    
    public Map<String,Integer> getCourseNotificationHistory(Integer createdBy);
    
    public List<Notification> getNotificationsForAdminByType(Integer type);
}
