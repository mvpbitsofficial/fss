/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.AdminLicense;
import java.util.Date;
import java.util.List;

/**
 *
 * @author CoreTechies M8
 */
public interface AdminLicenseDao {

    public AdminLicense saveAdminLicense(AdminLicense adminLicense);

    public AdminLicense updateAdminLicense(AdminLicense adminLicense);

    public List<AdminLicense> getAdminLicense();

    public AdminLicense getAdminLicense(Integer adminLicenseId);

    public Boolean deleteAdminLicense(Integer adminLicenseId);

    public List<AdminLicense> getLicenseExpireAlert(Date date);

    public AdminLicense getAdminLicenseByClient(Integer clientuserid);

    public List<AdminLicense> getAdminLicenseListByExpiryDate(Date fromDate, Date toDate);

    public List<AdminLicense> getAdminLicenseListByCreateOn(Date fromDate, Date toDate);
    
    public Integer getClientLicenseForChart(Date date);

}
