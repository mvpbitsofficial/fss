/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.controller;

import com.coretechies.filesecuritysystem.algorithm.NotificationType;
import com.coretechies.filesecuritysystem.algorithm.UserType;
import com.coretechies.filesecuritysystem.dao.NotificationDao;
import com.coretechies.filesecuritysystem.dao.UserLicenseDao;
import com.coretechies.filesecuritysystem.dao.WatermarkDao;
import com.coretechies.filesecuritysystem.domain.Notification;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.domain.UserLicense;
import com.coretechies.filesecuritysystem.domain.Watermark;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * The WatermarkController class implements an application that handles all the requests
 * regarding to waterMark like- add, delete and update watermark.
 * 
 * @Controller annotation represents that this is a java controller class.
 * @SessionAttributes annotation represents a user in session.
 * "user" is a object of userInfo class and have a complete information
 * of that user who is present in the session at that time.
 * 
 * @author Jaishree
 * @version 1.0
 * @since 2016-09-03
 */
@Controller
@SessionAttributes({"user"})
public class WatermarkController {

    @Autowired
    private NotificationDao notificationDao;
     
    @Autowired
    private WatermarkDao watermarkDao;

    @Autowired
    private MailController mailController;

    @Autowired
    private UserLicenseDao userLicenseDao;
    /**
     * This method will take the user at page watermark, 
     * with a list of all watermark added by the user who is in session right now.
     * 
     * @param userName
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/viewwatermark")
    public ModelAndView viewWatermark(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("watermarks", watermarkDao.getWatermarkList(userInfo.getUserInfoId()));
                if(watermarkDao.getWatermarkList(userInfo.getUserInfoId())==null){
                    System.out.println("null");
                }else{
                    System.out.println("not null");
                }
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                return new ModelAndView("watermark");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will check if client is in session then it will take the user at page addwatermark.
     * 
     * @param userName
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/addwatermark")
    public ModelAndView getAddWatermark(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                return new ModelAndView("addwatermark");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will add a new watermark according to the specified parameters,
     * And return an appropriate message.
     * 
     * @param userName
     * @param watermarkName
     * @param watermarkMessage
     * @param position
     * @param fontSize
     * @param fontFamily
     * @param fontSize
     * @param modelMap
     * @return String
     */
    @PostMapping(value = "/{userName}/addwatermark")
    @ResponseBody
    @Transactional(propagation = Propagation.REQUIRED)
    public String addWatermark(@PathVariable String userName, @RequestParam String watermarkName, @RequestParam String watermarkMessage,
            @RequestParam String watermarkContent, @RequestParam String property, @RequestParam Integer position, @RequestParam String fontSize, @RequestParam String fontFamily, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                if (watermarkName == null || watermarkName.trim().isEmpty()) {
                    return "Please enter watermark name.";
                }
                if (watermarkMessage == null || watermarkMessage.trim().isEmpty()) {
                    return "Please enter watermark Message.";
                }
                if (watermarkDao.getWatermarkByName(userInfo.getUserInfoId(), watermarkName) != null) {
                    return "This watermark name is already added.";
                }
                if (watermarkDao.getWatermarkByMessage(userInfo.getUserInfoId(), watermarkMessage) != null) {
                    return "This watermark message is already added.";
                }
               
                Watermark createWatermark = new Watermark();
                createWatermark.setWatermarkName(watermarkName);
                createWatermark.setWatermarkMessage(watermarkMessage);
                createWatermark.setWatermarkContent(watermarkContent); 
                createWatermark.setProperty(property); 
                createWatermark.setTextPosition(position);
                createWatermark.setDefaultWatermark(false);
                createWatermark.setUserId(userInfo.getUserInfoId());
                createWatermark.setFontSize(fontSize.replace(";",""));
                createWatermark.setFontFamily(fontFamily.replace(";",""));
                if (watermarkDao.saveWatermark(createWatermark) != null) {
                    Notification notification = new Notification();
                    notification.setCreateOn(new Date());
                    notification.setCreatedBy(userInfo.getUserInfoId());
                    notification.setNotificationMessage("Watermark "+watermarkName+" is Created"); 
                    notification.setNotificationType(NotificationType.Watermark_History.getNotificationTypeValue());
                    notification.setSeenby("0,");
                    notificationDao.saveNotification(notification);
                    return "success";
                } else {
                    return "Unable to add watermark";
                }
            } else {
                return "Please login again";
            }
        } else {
            return "Please login again";
        }
    }

    
    
    
    
    /**
     * This method will take the user to the page updatewatermark,
     * with a watermark object at specified watermark id.
     * 
     * @param userName
     * @param watermarkId
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/updatewatermark/{watermarkId}")
    public ModelAndView getUpdateWatermark(@PathVariable String userName, @PathVariable Integer watermarkId, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("watermark", watermarkDao.getWatermark(userInfo.getUserInfoId(), watermarkId));
                return new ModelAndView("updatewatermark");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will update a watermark object with new entries specified in method parameters, 
     * And return an appropriate message.
     * 
     * @param userName
     * @param watermarkId
     * @param watermarkPosition
     * @param watermarkName
     * @param watermarkMessage
     * @param fontSize
     * @param fontFamily
     * @param redirectAttributes
     * @param modelMap
     * @return String
     */
    @PostMapping(value = "/{userName}/updatewatermark")
    @ResponseBody
    @Transactional(propagation = Propagation.REQUIRED)
    public String updateWatermark(@PathVariable String userName, @RequestParam Integer watermarkId, @RequestParam Integer watermarkPosition, @RequestParam String watermarkName, 
            @RequestParam String watermarkMessage, @RequestParam String watermarkContent, @RequestParam String property, @RequestParam String fontSize, @RequestParam String fontFamily, RedirectAttributes redirectAttributes, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                if (watermarkId == null || watermarkId == 0) {
                    return "Please enter select proper watermark.";
                }
                if (watermarkPosition == null || watermarkPosition == 0) {
                    return "Please enter watermark Position.";
                }
                if (watermarkName == null || watermarkName.trim().isEmpty()) {
                    return "Please enter watermark name.";
                }
                if (watermarkMessage == null || watermarkMessage.trim().isEmpty()) {
                    return "Please enter watermark Message.";
                }
                if (watermarkDao.getWatermarkByName(userInfo.getUserInfoId(), watermarkName, watermarkId) != null) {
                    return "This watermark name is already added.";
                }
                if (watermarkDao.getWatermarkByMessage(userInfo.getUserInfoId(), watermarkMessage, watermarkId) != null) {
                    return "This watermark message is already added.";
                }
                Watermark watermark = watermarkDao.getWatermark(userInfo.getUserInfoId(), watermarkId);
                watermark.setTextPosition(watermarkPosition);
                watermark.setWatermarkName(watermarkName);
                watermark.setWatermarkMessage(watermarkMessage);
                watermark.setWatermarkContent(watermarkContent);
                watermark.setProperty(property); 
                watermark.setFontSize(fontSize.replace(";",""));
                watermark.setFontFamily(fontFamily.replace(";",""));
                if (watermarkDao.updateWatermark(watermark) != null) {
                    redirectAttributes.addFlashAttribute("message", "Watermark has been successfully updated");
                    return "success";
                } else {
                    return "Unable to update watermark";
                }
            } else {
                return "Please login again";
            }
        } else {
            return "Please login again";
        }
    }

    /**
     * This method will delete a watermark from database at specified id,
     * And a confirmation mail is send to the user.
     * 
     * @param userName
     * @param watermarkIds
     * @param redirectAttributes
     * @param request
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/deletewatermark")
    @ResponseBody
    public String deleteWatermark(@PathVariable String userName, @RequestParam(value = "watermarkIds[]") Integer[] watermarkIds, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                String message = "";
                String message1 = "";
                for (Integer watermarkId : watermarkIds) {
                    Watermark watermark = watermarkDao.getWatermark(userInfo.getUserInfoId(), watermarkId);
                    List<UserLicense> userLicenses = userLicenseDao.getUserLicenseByWatermarkId(watermarkId);
                    
                    if(userLicenses == null || userLicenses.isEmpty()){
                        if (watermark != null) {
                            if (watermarkDao.deletewatermark(watermarkId)) {
                                redirectAttributes.addFlashAttribute("message", "Watermark has been successfully deleted.");
                                message += ","+watermark.getWatermarkName();
                                Notification notification = new Notification();
                                notification.setNotificationMessage("Watermark "+ watermark.getWatermarkName()+"is Deleted");
                                notification.setCreateOn(new Date());
                                notification.setCreatedBy(userInfo.getUserInfoId());
                                notification.setNotificationType(NotificationType.Watermark_History.getNotificationTypeValue());
                                notification.setSeenby("0,");
                                notificationDao.saveNotification(notification);
                            } else {
                                redirectAttributes.addFlashAttribute("message", "Unable to delete watermark.");
                            }
                        } else {
                            redirectAttributes.addFlashAttribute("message", "This watermark is unavailable.");
                        }
                    } else{
                        message1 += ","+watermark.getWatermarkName();
                    }                    
                }
                if(!message.isEmpty()){
                    message = "Watermark "+"\""+message.substring(1)+"\""+" has been deleted successfully";
                }
                if(!message1.isEmpty()){
                    message1 = "Watermark "+"\""+message1.substring(1)+"\""+" can not delete, Id is in use.";
                }
                System.out.println("message1=="+message+" message2=="+message1);
                if(!message.isEmpty() && !message1.isEmpty()){
                    return "success#"+message+" and "+message1;
                }else{
                    return "success#"+message+""+message1;
                }
            } else {
                return "Unauthorised Access";
            }
        } else {
            return "Please Login Again";
        }
    }

    /**
     * This method will set a default watermark to the watermark object.  
     * 
     * @param userName
     * @param watermarkId
     * @param redirectAttributes
     * @param request
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/setdefaultwatermark/{watermarkId}")
    public ModelAndView setDefaultWatermark(@PathVariable String userName, @PathVariable Integer watermarkId, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                Watermark watermark = watermarkDao.getDefaultWatermark(userInfo.getUserInfoId());
                System.out.println(watermark);
                if (watermark != null) {
                    watermark.setDefaultWatermark(Boolean.FALSE);
                    if (watermarkDao.updateWatermark(watermark) == null) {
                        redirectAttributes.addFlashAttribute("message", "Unable to access watermark.");
                        return new ModelAndView("redirect:/" + userName + "/viewwatermark");
                    }
                }
                watermark = watermarkDao.getWatermark(userInfo.getUserInfoId(), watermarkId);
                watermark.setDefaultWatermark(true);
                if (watermarkDao.updateWatermark(watermark) != null) {
                    redirectAttributes.addFlashAttribute("message", "This watermark has been set as default.");
                } else {
                    redirectAttributes.addFlashAttribute("message", "Unable to access watermark.");
                }
                return new ModelAndView("redirect:/" + userName + "/viewwatermark");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    @GetMapping(value="/{userName}/showpreview")
    @ResponseBody
    public String getPreviewWatermark(@PathVariable String userName, @RequestParam Integer watermarkId, ModelMap modelMap){
        UserInfo userInfo = (UserInfo)modelMap.get("user");
        if(userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)){
            if(UserType.Client.getUserTypeValue().equals(userInfo.getUserType())){
                System.out.println("watermark ID===="+ watermarkId);
                Watermark watermarkForPreview = watermarkDao.getWatermarkForPreview(watermarkId);
                System.out.println(watermarkForPreview);
                String message = watermarkForPreview.getWatermarkMessage();
                System.out.println("STYLE++="+message.indexOf("style"));
                int indexOf = message.indexOf("font-size", message.indexOf("style"));
                System.out.println("INDEX========="+indexOf);
                return "{\"status\":\"success\",\"watermarkName\":\""+watermarkForPreview.getWatermarkName()+"\","
                            +"\"watermarkMessage\":\""+watermarkForPreview.getWatermarkMessage().replace("\"", "\\\"")+"\","
                            +"\"textPosition\":\""+watermarkForPreview.getTextPosition()+ "\"}";
            }
            else{
                return "can't access";
            }
        } else{
            return "Login again";
        }
    }
}
