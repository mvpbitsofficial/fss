<%-- 
    Document   : getlicenseinfo
    Created on : 20 Aug, 2016, 10:44:00 AM
    Author     : CoreTechies M8
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>FSS | System Info</title>
    </head>
    <body>
        <h1>Get System Info!</h1>
        <form action="#">
            <div>
                <input type="text" id="systemId"/>
            </div>
            <div>
                <button type="button" id="getsysteminfo">Get System Info</button>
            </div>
        </form>
        <div>
            <label id="diskserialnumber"></label>
            <br/>
            <label id="macid"></label>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.10.2.min.js"></script>
        <script>
            $("#getsysteminfo").on("click", function () {
                var systemId = $("#systemId").val();
                if (systemId === null || systemId.trim() === "") {
                    alert("Please enter license id");
                } else {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getsysteminfo", {systemId: systemId},
                            function (data, status) {
                                if (data.indexOf("success") !== -1) {
                                    var jsondata = JSON.parse(data.split("#")[1]);
                                    $("#diskserialnumber").text(jsondata.serialNumber);
                                    $("#macid").text(jsondata.macId);
                                } else {
                                    alert(data);
                                }
                            }
                    );
                }
            });
        </script>
    </body>
</html>
