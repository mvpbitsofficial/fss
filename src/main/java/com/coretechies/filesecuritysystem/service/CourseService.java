/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.service;

import com.coretechies.filesecuritysystem.domain.Course;
import java.util.List;

/**
 *
 * @author CoreTechies M8
 */
public interface CourseService {

    public Course saveCourse(String courseId, String courseName, String courseDescription, Integer userId);

    public Integer getCourseCount(Integer userInfoId);
    
    public Integer getAllCourseCount();
    
    public List<Course> getCourseById(Integer userId);
    
    public boolean deleteCourse(Integer id);
}
