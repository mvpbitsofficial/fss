
<!DOCTYPE html>

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>FSS | Renew License</title>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
 <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<style>
    .input-daterange .input-group-addon{
        padding: 6px 12px;
        width: 1%;
    }
            
    .number-dropdown{ width:200px;} 
    .number-dropdown .btn-group{width:100%;}
    .number-dropdown .btn-group label{ width:calc(100%/5);}

    .number-dropdown .btn-group label.btn-default.active{ background:#e7505a; border-color:#e7505a; color:#fff;}
    .number-dropdown .btn-group .btn+.btn{ margin-left:0px;}
    .form-wizard .steps > li.done > a.step .desc i{display: none !important;}
</style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top"> 
 <!-- BEGIN HEADER INNER -->
 <jsp:include page="topbar.jsp" />
 <!-- END HEADER INNER --> 
</div>
<!-- END HEADER --> 
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER --> 
<!-- BEGIN CONTAINER -->
<div class="page-container"> 
 <!-- BEGIN SIDEBAR -->
 <div class="page-sidebar-wrapper"> 
  <!-- BEGIN SIDEBAR --> 
  <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
  <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
  <div class="page-sidebar navbar-collapse collapse"> 
   <!-- BEGIN SIDEBAR MENU --> 
   <jsp:include page="sidebar.jsp" />
<!--   <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
     DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
    <li class="sidebar-toggler-wrapper hide"> 
      BEGIN SIDEBAR TOGGLER BUTTON 
     <div class="sidebar-toggler"> <span></span> </div>
      END SIDEBAR TOGGLER BUTTON  
    </li>
    <li class="nav-item  "> <a href="index.html" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
    <li class="nav-item  "> <a href="users.html" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
     
    </li>
    <li class="nav-item  "> <a href="licence.html" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">Licence</span>  </a>
     
    </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
     
    </li>
    <li class="nav-item  "> <a href="report.html" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
    <li class="nav-item  "> <a href="watermark.html" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
    <li class="nav-item  "> <a href="settings.html" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
   </ul>-->
   <!-- END SIDEBAR MENU --> 
   <!-- END SIDEBAR MENU --> 
  </div>
  <!-- END SIDEBAR --> 
 </div>
 <!-- END SIDEBAR --> 
 <!-- BEGIN CONTENT -->
 <div class="page-content-wrapper"> 
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content"> 
   <!-- BEGIN PAGE HEADER--> 
   
   <!-- BEGIN PAGE TITLE-->
   <!--<h3 class="page-title"> Licence 
    <!-- <small>blank page layout</small>-- 
   </h3>-->
   <!-- END PAGE TITLE--> 
   <!-- END PAGE HEADER-->
   
 
  <div class="row">
         <div class="col-sm-12">
         	<div class="portlet red box portlet-datatable  ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-key "></i>
                                        <span class="caption-subject  sbold uppercase">Renew  License</span>
                                    </div>
                                    <!--<div class="actions">
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-refresh"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-printer"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-docs"></i>
                                        </a>
                                         
                                    </div>-->
                                </div>
                                <div class="portlet-body clearfix">
                                    <div class="col-sm-8 clearfix" style=" margin:0 auto; float:none;">
                                    	<form>
                                            <div>                
                                                <input type="hidden" id="licenseId" value="${license.userLicenseId}"/>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-sm-3 control-label"> User</label>
                                                <div class="col-sm-8">
							<div class="one input-group">
                                                            <span class="input-group-addon input-circle-left">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                                            <select class="form-control input-circle-right"  id="useruserid">
                                                                <c:forEach items="${users}" var="client">
                                                                    <c:choose>
                                                                        <c:when test="${license.userId==client.userInfoId}">
                                                                            <option value="${client.userInfoId}" selected="selected">${client.userName}</option>
                                                                        </c:when>
                                                                    </c:choose>
                                                                </c:forEach>
								</select>
							</div>	      
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                            <label class="col-sm-3 control-label"> Course</label>
                                                            <div class="col-sm-8">
								<div class="one input-group">
                                                                    <span class="input-group-addon input-circle-left">
                                                                        <i class="fa fa-book"></i>
                                                                    </span>
                                                                    <select class="form-control input-circle-right"  id="courseid">
									<c:forEach items="${courses}" var="course">
                                                                            <c:choose>
                                                                                <c:when test="${license.courseId==course.id}">
                                                                                    <option value="${course.id}" selected="selected">${course.courseName}</option>
                                                                                </c:when>
                                                                            </c:choose>
                                                                        </c:forEach>
                                                                    </select>
								</div>	      
                                                            </div>
                                            </div>
                                            <div class="form-group clearfix">
                                            <label class="col-sm-3 control-label"> Validity</label>
                                             <div class="col-sm-8">
                                                <div class="input-group" id="validity_div">
                                                    <span class="input-group-addon input-circle-left">
                                                        <i class="fa fa-star"></i>
                                                    </span>
                                                    <div class="dropdown">
                                                    <button class="form-control input-circle-right dropdown-toggle" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <span class="status pull-left"></span> <span class="caret pull-right" style="margin-top:5px;"></span> </button>
                                                    <div class="dropdown-menu number-dropdown col-sm-5" aria-labelledby="dLabel" style="width: 250px;">
                                                     <div class="btn-group" data-toggle="buttons" id="radiobuttons">
                                                      <label class="btn btn-default value" id="1">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       1 </label>
                                                      <label class="btn btn-default value" id="2">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       2 </label>
                                                      <label class="btn btn-default value" id="3">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       3 </label>
                                                      <label class="btn btn-default value" id="4">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       4 </label>
                                                      <label class="btn btn-default value" id="5">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       5 </label>
                                                      <label class="btn btn-default value " id="6">
                                                       <input type="radio" name="options" autocomplete="off" >
                                                       6 </label>
                                                      <label class="btn btn-default value" id="7">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       7 </label>
                                                      <label class="btn btn-default value" id="8">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       8 </label>
                                                      <label class="btn btn-default value" id="9">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       9 </label>
                                                      <label class="btn btn-default value" id="10">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       10 </label>
                                                      <label class="btn btn-default value " id="11">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       11 </label>
                                                      <label class="btn btn-default value" id="12">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       12 </label>
                                                      <label class="btn btn-default value" id="13">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       13 </label>
                                                      <label class="btn btn-default value" id="14">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       14 </label>
                                                      <label class="btn btn-default value" id="15">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       15 </label>
                                                      <label class="btn btn-default value " id="16">
                                                       <input type="radio" name="options" autocomplete="off" >
                                                       16 </label>
                                                      <label class="btn btn-default value" id="17">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       17 </label>
                                                      <label class="btn btn-default value" id="18">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       18 </label>
                                                      <label class="btn btn-default value" id="19">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       19 </label>
                                                      <label class="btn btn-default value" id="20">
                                                       <input type="radio" name="options" autocomplete="off">
                                                       20 </label>
                                                    </div>
                                                    <div style="text-align:left; padding-bottom:5px;">
                                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="margin-top: 8px; margin-left: 8px;">
                                                            <input type="hidden" id="validity" />
                                                        <input type="checkbox" class="group-checkable" name="options" id="lifetime" value="0"/>
                                                        <span></span>LifeTime</label>
                                                    </div><!--
                                                        <div class="hint">(Click here for Lifetime)</div>-->
                                                    </div>
                                                   </div>
                                            </div>
                                            <span class="error-message" id="dateError" ></span>
                                            </div>
                                            </div>
                                           
<!--                                                <div class="form-group clearfix">
                                                <div class="one-half">
                                                    <div class="one">
                                                    	<select class="form-control input-circle">
                                                        <option selected disabled>Select Validity</option>
                                                        	<option>Option 1</option>
                                                            <option>Option 2</option> 
                                                            <option>Option 3</option>
                                                        </select>
                                                    </div>	
                                                </div>
                                            </div>
                                            -->
                                            <div class="form-group clearfix">
                                                <label class="col-sm-3 control-label"> Date</label>
                                                <div class="col-sm-8">
                                                    <div class="one-half-50">
							<div class="one input-group">
                                                            <span class="input-group-addon input-circle-left">
                                                                <i class="fa fa-calendar"></i>
                                                            </span>
                                                            <fmt:formatDate value="${license.issueDate}"  
                                                                    type="date" 
                                                                    pattern="dd-MM-yyyy"
                                                                    var="theFormattedIssueDate" />
                                                            <input class="form-control input-circle-right" id="startDate" readonly value="${theFormattedIssueDate}" placeholder=" Issue Date">
							</div>
                                                        <div class="date-picker input-daterange " data-date-format="dd-mm-yyyy">
                                                            <div class="one">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon input-circle-left">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </span>
                                                                    <input class="form-control input-circle-right" id="endDate" value="" placeholder=" Expiry Date" style="text-align: left;">
                                                                </div>
                                                                <span class="error-message" id="enddateerror" ></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                                <!--<span class="help-block"> A block of help text. </span>-->
                                                </div>
                                            </div>
                                            
<!--                                            <div class="form-group clearfix">
                                            	<div class="one-half">
                                                <div class="two" style="float:left">
                                                    	 <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="    margin-top: 8px;">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes" />
                                                        <span></span>
                                                    </label>

                                                    </div>
                                                	<div class="one" style="float:right">
                                                    	<input class="form-control input-circle" placeholder=" Licence Limit">
                                                        <div class="hint text-right">(please click checkbox for unlimited licence)</div>
                                                    </div>
                                                    	
                                                </div>
                                                </div>-->
                                            <div class="form-group clearfix">
                                                            <label class="col-sm-3 control-label"> System Name</label>
                                                            <div class="col-sm-8">
								<div class="one input-group">
                                                                    <span class="input-group-addon input-circle-left">
                                                                        <i class="fa fa-desktop"></i>
                                                                    </span>
									<select class="form-control input-circle-right"  id="systemId">
										<c:forEach items="${systems}" var="system">
                                                                                    <c:choose>
                                                                                        <c:when test="${license.systemInfoId==system.systemInfoId}">
                                                                                            <option value="${system.systemInfoId}" selected>${system.systemName}</option>
                                                                                        </c:when> 
                                                                                    </c:choose>
                                                                                </c:forEach>
									</select>
								</div>
                                                                <span style="font-size: smaller; font-weight:bold;  color: red" id="systemError" ></span>
                                                            </div>
                                            </div>
<!--                                            <div class="form-group clearfix">
                                                            <label class="col-sm-3 control-label">WaterMark</label>
                                                            <div class="col-sm-8">
                                                             <div class="one-half">
								<div class="one">
									<select class="form-control input-circle"  id="watermarkId">
										<c:forEach items="${watermarks}" var="watermark">
                                                                                    <c:choose>
                                                                                        <c:when test="${license.userId==client.userInfoId}">
                                                                                            <option value="${watermark.watermarkId}" selected="selected">${watermark.watermarkName}</option>
                                                                                        </c:when>
                                                                                        <c:otherwise>
                                                                                            <option value="${watermark.watermarkId}">${watermark.watermarkName}</option>
                                                                                        </c:otherwise>
                                                                                    </c:choose>
                                                                                </c:forEach>
									</select>
								</div>
                                                            </div>
                                                                <span class="help-block"> A block of help text. </span>
                                                            </div>
                                            </div>-->
                                                
<!--                                            <div class="form-group clearfix">
                                            	<div class="two-half">
                                                	<div class="one">
                                                    	<input class="form-control input-circle" placeholder=" Licence Key">
                                                    </div>
                                                    <div class="two">
                                                    	<a href="#" class="btn  btn-default btn-circle"><i class="fa fa-copy"></i></a>
                                                    	<a href="#" class="btn  btn-default btn-circle"><i class="fa fa-download"></i></a>
                                                    </div>
                                                </div>
                                                </div>-->
                                                <div class="form-group clearfix">
                                                            <label class="col-sm-3 control-label"> License key</label>
                                                            <div class="col-sm-8">
                                                               <div class="two-half">
                                                                   <div class="one input-group">
                                                                       <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-credit-card"></i>
                                                                        </span>
                                                                       <input class="form-control input-circle-right"  readonly id="licenceKey" placeholder="License key">
                                                                   </div>
                                                                   <div class="two">
                                                                        <a id="copy" class="btn  btn-default btn-circle"><i class="fa fa-copy"></i></a>
                                                                        <a href="javascript:;" id="keydownload" class="btn  btn-default btn-circle"><i class="fa fa-download"></i></a>
                                                                        <!--<a id="mail" class="btn  btn-default btn-circle"><i class="fa fa-envelope"></i></a>-->
                                                                    </div>
								</div>
                                                                <!--<span class="help-block"> A block of help text. </span>-->
                                                            </div>
                                                </div>
                                                <span style="color: red; display: block; margin-left: 170px; font-size: 14px;" id="renewError" ></span>
                                                <span style="color: green; display: block; margin-left: 170px; font-size: 14px;" id="renewSuccess" ></span>
                                                <div id="loading" style="display: none;">
                                                    <center>
                                                        <!--<span style="color: blue;">Processing...</span>-->
                                                        <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/24.gif" />
                                                    </center>
                                                </div>
                                                <div class="form-group">
                                                            <div class="col-sm-3"> </div>
                                                            <div class="col-sm-8 text-right">
                                                                <a href="javascript:;" id="renewLicense" class="btn btn-circle btn-success">Renew License</a>
                                                                <!--<span class="help-block"> A block of help text. </span>-->
                                                            </div>
                                                </div>
                                        </form>
                                    </div>
                                </div>
                </div>
                            </div>
         </div>               
  </div>
   
   
  </div>
  <!-- END CONTENT BODY -->  
 </div>
 <!-- END CONTENT --> 
 <!-- BEGIN QUICK SIDEBAR --> 
 <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 
 
 <!-- END QUICK SIDEBAR --> 
</div>
<!-- END CONTAINER --> 
<!-- BEGIN FOOTER -->
<div class="page-footer">
 <div class="page-footer-inner"> 2016 &copy; Organization </div>
 <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
</div>

<!-- Modal -->
<div class="modal fade " id="otpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm text-center" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Send OTP</h4>
      </div>
      <div class="modal-body">
			<div class="mt-radio-inline">
					<label class="mt-radio">
						<input type="radio" name="sendotp" id="" value="1" checked=""> Email
						<span></span>
					</label>
					<label class="mt-radio">
						<input type="radio" name="sendotp" id="" value="2" > Mobile
						<span></span>
					</label>
					
             </div>
      </div>
      <div class="modal-footer" style="text-align:center;">
        <button type="button" class="btn btn-success btn-circle" data-dismiss="modal" data-toggle="modal" data-target="" id="renew_license"> Send <i class="fa fa-angle-right"></i> </button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade " id="otpModal_text" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm text-center" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirm OTP</h4>
      </div>
      <div class="modal-body">
          <input type="hidden" id="random"/>
	<input type="text" class="form-control input-circle" id="otp"/>
        <span style="color: red; display: block; font-size: 14px;" id="otpError" ></span>
      </div>
      <div class="modal-footer" style="text-align:center;">
        <button type="button" class="btn btn-success btn-circle otp_btn" id="submitotp"> Continue <i class="fa fa-angle-right"></i> </button>
        <button class="btn btn-default btn-circle otp_btn" type="button" id="resendotp">Resend OTP</button>
      </div>
    </div>
  </div>
</div>
<!-- END FOOTER --> 

<!-- BEGIN CORE PLUGINS --> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
<!-- END CORE PLUGINS --> 
<!-- BEGIN THEME GLOBAL SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
<!-- END THEME GLOBAL SCRIPTS --> 
<!-- BEGIN THEME LAYOUT SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
<!-- END THEME LAYOUT SCRIPTS --> 
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="${pageContext.request.contextPath}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/clipboard/clipboard.min.js"></script>
        
        <script>
            $(".form-control").focus(function () {
                $(this).prev('.input-group-addon').removeClass().addClass('input-group-addon-focus');
                $(this).next('.input-group-addon').removeClass().addClass('input-group-addon-focus');
            });

            $(".form-control").focusout(function () {
                $(this).prev('.input-group-addon-focus').removeClass().addClass('input-group-addon');
                $(this).next('.input-group-addon-focus').removeClass().addClass('input-group-addon');
            });
            
            $('#validity_div').on('mouseenter', function () {
                $(this).find('.input-group-addon').removeClass('input-group-addon').addClass('input-group-addon-focus');
                $('.selectpicker ').css('border-color', '#e7505a');
            }).on('mouseleave', function () {
                $(this).find('.input-group-addon-focus ').removeClass('input-group-addon-focus').addClass('input-group-addon');
                $('.selectpicker ').css('border-color', '#c2cad8');
            });
        </script>
        
        <script>
            var endDate = $('#endDate').datepicker({
                format: "dd-mm-yyyy"
            });
            $(document).ready(function () {
//                var useruserId = $("#useruserid").select().val();
//                if (useruserId !== null || useruserId !== "") {
//                    var Uname = "${user.userName}".replace(" ", "");
//                    $.post("${pageContext.request.contextPath}/"+Uname+"/getusersystem",
//                            {userId: useruserId},
//                            function (data, status) {
//                                $("#systemId").html("");
//                                if (data.indexOf("success") !== -1) {
//                                    $("#systemId").append(data.split("#")[1]);
//                                } else {
//                                    $("#systemError").text(data);
//                                }
//                            });
//                } else {
//                    $("#systemId").html("<option value=\"\">Please select a system.</option>");
//                }
                var issuedate = $("#startDate").val();
                console.log(issuedate);
                endDate.datepicker('setStartDate', issuedate);
            });
        </script>
        <script>
            $("#renewLicense").on("click", function (){
                var validity = $("#validity").val();
                var endDate = $("#endDate").val();
                var flag = true;
//                if(validity === null || validity === ""){
//                    $("#dateError").text("Please Select Validity");
//                    flag = false;
//                } 
                if(endDate === null || endDate === ""){
                    $("#enddateerror").text("Please Enter expiry Date");
                    flag = false;
                }
                if(flag){
                    $("#otpModal").modal("show");
                    $("#enddateerror").text("");
                    $("#dateError").text("");
                }
            });
            
//            $("#dLabel").on("change", function(){
//                var validity = $("#validity").val();
//                var endDate = $("#endDate").val();
//                if(validity === null || validity === ""){
//                    $("#dateError").text("Please Select Validity");
//                }else{
//                    $("#dateError").text("");
//                } 
//                if(endDate === null || endDate === ""){
//                    $("#enddateerror").text("Please Enter expiry Date");
//                }else{
//                    $("#enddateerror").text("");
//                }
//            });
            
            $("#renew_license").on("click", function () {
                var licenseId = $("#licenseId").val();
                var useruserId = $("#useruserid").select().val();
                var lifetime = $("#lifetime").is(":checked");
                var courseId = $("#courseid").select().val();
                var validity = $("#validity").val();
                var startDate = $("#startDate").val();
                var endDate = $("#endDate").val();
                var systemId = $("#systemId").select().val();
                var radioValue = $("input[name='sendotp']:checked"). val();
                console.log(useruserId);
                console.log(courseId);
                var Uname = "${user.userName}".replace(" ", "");
                $("#loading").css('display','block');
                $.post("${pageContext.request.contextPath}/"+Uname+"/renewuserlicense",
                        {userId: useruserId,
                            courseId: courseId,
                            lifetime: lifetime,
                            licenseId: licenseId,
                            validity: validity,
                            startDate: startDate,
                            endDate: endDate,
                            systemId: systemId,
                            sendOtpOn: radioValue},
                        function (data, status) {
                            if (data === "Please login again.") {
                                window.location.href = "/";
                            } else if (data.indexOf("success") !== -1) {
                                $("#loading").css("display","none");
//                                alert(data.split("#")[1]);
                                $("#random").val(data.split("#")[1]);
                                $("#otpModal_text").modal("show");
//                                window.location.href = "${pageContext.request.contextPath}/${user.userName}/viewlicense";
                            } else {
                                $("#loading").css("display","none");
                                $("#renewError").text(data);
                            }
                        });
            });
            
            $(document).on("click", "#submitotp", function () {
                var otp = $("#otp").val();
                var random = $("#random").val();
                if (otp === "") {
                    $("#otpError").text("Please enter OTP");
                } else if (random === "") {
                    $("#otpError").text("Login Again");
                } else {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/varifyotp", {otp: otp, random: random},
                            function (data, status) {
                                if (data === "Please login again.") {
                                    window.location.href = "/";
                                } else if (data.indexOf("success") !== -1) {
                                    $("#licenceKey").val(data.split("#")[1]);
                                    $("#otpModal").modal("hide");
                                    $("#otpModal_text").modal("hide");
                                    $("#renewLicense").css("display", "none");
                                    $("#renewSuccess").text("License has been renwed successfully.");
                                } else {
                                    $("#otpError").text(data);
                                }
                            });
                }
            });
            
            $(document).on("click", "#resendotp", function () {
                var radioValue = $("input[name='sendotp']:checked"). val();
                var random = $("#random").val();
                if (random === "") {
                    $("#otpError").text("Login again");
                } else {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/resendotp", {random: random, sendOtpOn: radioValue},
                            function (data, status) {
                                if (data === "Please login again.") {
                                    window.location.href = "/";
                                } else if (data.indexOf("success") !== -1) {
                                    $("#random").val(data.split("#")[1]);
                                } else {
                                    $("#otpError").text(data);
                                }
                            });
                }
            });
            
//            $(document).on("change", "#validity", function(){
//               var valid = $("#validity").val();
//               var startdate = $("#startDate").val();
//               
//               $.get("${pageContext.request.contextPath}/${user.userName}/getdate",
//                        {validity: valid, startdate: startdate}, function (data, status){
//                            if (data.indexOf("success") > -1){
//                                $("#endDate").val(data.split("#")[1]);
//                            }
//                            else{
//                                alert(data);
//                            }
//                        });
//            });
        </script>
        <script>
            $(document).ready(function () {
                $('#side-menu').find('li').removeClass('active');
                $(document).find(".license").addClass('active');
            });
        </script>
        <script>
            $(".number-dropdown label").click(function(){
                var selText = $(this).text();
                $(this).parents().find('.status').html(selText);
            });
            $(".value").on("click", function(){
                $(".status").css("display", "block");
                var startdate = $("#startDate").val();
                var ID = this.id;
                if(ID !== null || ID !== "" ){
                    $("#lifetime").attr('checked', false);
                    $("#startDate").prop( "disabled", false );
                    $("#endDate").prop( "disabled", false );
                    var Uname = "${user.userName}".replace(" ", "");
                    $.get("${pageContext.request.contextPath}/"+Uname+"/getdate",
                        {validity: ID, startdate: startdate}, function (data, status){
                            if (data.indexOf("success") > -1){
                                $("#validity").val(ID);
                                $("#endDate").val(data.split("#")[1]);
                            }
                            else{
                                $("#dateError").text(data);
                            }
                    });
                }
            });

            $("#lifetime").on("click", function(){
                if($("#lifetime").is(":checked")){
                    $("#validity").val("");
                    $("#endDate").val("");
                    $("#radiobuttons").attr("disabled", "disabled").off('click');
                    $("#startDate").prop( "disabled", true );
                    $("#endDate").prop( "disabled", true );
                    $(".btn-default").removeClass("active");
                } else{
                    $("#radiobuttons").prop( "disabled", false );
                    $("#startDate").prop( "disabled", false );
                    $("#endDate").prop( "disabled", false );
                    $(".status").css("display", "none");
                }
            });
        </script>
        <script>
            var clipboard = new Clipboard('#copy', {
                text: function () {
                    var licenseKey = $("#licenceKey").val();
                    if (licenseKey === null || licenseKey === "") {
                        $("#renewError").text("No data available to copy.");
                    } else {
                        $("#renewSuccess").text("License Key has been copied successfully.");
                        return licenseKey;
                    }
                }
            });
            clipboard.on('success', function (e) {
                console.log(e);
            });
            clipboard.on('error', function (e) {
                console.log(e);
            });
            
            $("#keydownload").on("click", function(){
                        var license = $("#licenseId").val();
                        var licenseKey = $("#licenceKey").val();
                        console.log(license);
                        if(license !== null || license !== ""){
                            if(licenseKey === null || licenseKey === ""){
                                $("#renewError").text("No data available to download.");
                            } else{
                                var Uname = "${user.userName}".replace(" ", "");
                                window.location.href = "${pageContext.request.contextPath}/"+Uname+"/downloadlicensekey?licenseId="+license;
                                $("#renewSuccess").text("File Download Successful");
                            }
                        }
                    });
        </script>
</body>
</html>