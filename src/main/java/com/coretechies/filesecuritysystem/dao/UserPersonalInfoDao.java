/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.UserPersonalInfo;
/**
 *
 * @author CoreTechiesLP
 */
public interface UserPersonalInfoDao {
    
    public Boolean saveInfo(UserPersonalInfo userPersonalInfo);
    
}
