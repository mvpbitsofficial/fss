
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>FSS | Settings </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top"> 
            <!-- BEGIN HEADER INNER -->
            <jsp:include page="topbar.jsp" />
            <!-- END HEADER INNER --> 
        </div>
        <!-- END HEADER --> 
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER --> 
        <!-- BEGIN CONTAINER -->
        <div class="page-container"> 
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper"> 
                <!-- BEGIN SIDEBAR --> 
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse"> 
                    <!-- BEGIN SIDEBAR MENU --> 
                    <jsp:include page="sidebar.jsp" />
                    <!--                    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                                             DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
                                            <li class="sidebar-toggler-wrapper hide"> 
                                                 BEGIN SIDEBAR TOGGLER BUTTON 
                                                <div class="sidebar-toggler"> <span></span> </div>
                                                 END SIDEBAR TOGGLER BUTTON  
                                            </li>
                                            <li class="nav-item  "> <a href="index.html" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
                    
                                            </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">License</span>  </a>
                    
                                            </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
                    
                                            </li>
                                            <li class="nav-item  active"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
                                        </ul>-->
                    <!-- END SIDEBAR MENU --> 
                    <!-- END SIDEBAR MENU --> 
                </div>
                <!-- END SIDEBAR --> 
            </div>
            <!-- END SIDEBAR --> 
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper"> 
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content"> 
                    <!-- BEGIN PAGE HEADER--> 

                    <!-- BEGIN PAGE TITLE-->
                    <!--<h3 class="page-title"><i class="icon-docs "></i> Report
                    <!-- <small>blank page layout</small>--
                    </h3>-->
                    <!-- END PAGE TITLE--> 
                    <!-- END PAGE HEADER-->


                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PROFILE SIDEBAR -->

                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered  box-shadow">
                                            <div class="portlet-title tabbable-line">
                                                <div class="caption caption-md" style="width:100%;">
                                                    <i class="icon-globe theme-font hide"></i>
                                                    <span class="caption-subject  bold uppercase"><i class="icon-settings "></i> Settings</span>
                                                </div>
                                                <ul class="nav nav-tabs pull-left">
<!--                                                    <li class="active">
                                                        <a href="#tab_1_1" data-toggle="tab" aria-expanded="true">Security Question</a>
                                                    </li>-->
                                                    <li class="active">
                                                        <a href="#tab_1_2" data-toggle="tab" aria-expanded="false">Change Password</a>
                                                    </li>
                                                    <li class="">
                                                        <a href="#tab_1_3" data-toggle="tab" aria-expanded="false">Other Settings</a>
                                                    </li>
                                                    <li class="">
                                                        <a href="#tab_1_4" data-toggle="tab" aria-expanded="false">Backup and Restore History</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="tab-content">
                                                    <div class="tab-pane" id="tab_1_1">
                                                        <div class="row clearfix">
								<div class="clearfix col-sm-12">
                                                                    <div class="portlet box red   portlet-datatable  ">
									<div class="portlet-title">
                                                                            <div class="caption">
                                                                                Add Security Question
                                                                            </div>
									</div>
                                                                        <div class="portlet-body">
                                                                            <div class="input-group ">
                                                                                <span class="input-group-addon input-circle-left">
                                                                                    <i class="fa fa-question"></i>
                                                                                </span>
                                                                                <input type="text" class="form-control input-circle-right" placeholder="Enter Question" id="question">
                                                                                <span class="input-group-btn input-group-btn-new btn-circle">
                                                                                    <button class="btn btn-danger btn-circle" type="button" id="resetquestion"><i class="fa fa-refresh"></i> Reset</button>
                                                                                    <button class="btn btn-success btn-circle" type="button" id="addSecurityQuestion">Add</button>
                                                                                </span>
                                                                            </div><!-- /input-group -->
                                                                            <span style="color: green; display: block; font-size: 14px; margin-bottom:25px;" id="successMessageQuestion"></span>
                                                                            <span class="error-message" style=" margin-bottom:25px;" id="errorMessageQuestion"></span>
                                                                            <div class="">
                                                                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_5">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="table-checkbox">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </th>
                                                                                        <th style="width:20px;"> Sr </th>
                                                                                        <th> Question </th>
                                                                                        <th> Action </th>

                                                                                    </tr>
                                                                                </thead>

                                                                                <tbody>
                                                                                    <c:forEach items="${questions}" var="question" varStatus="status">
                                                                                    <tr class="odd gradeX">
                                                                                        <td>
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input type="checkbox" class="checkboxes" value="${question.securityQuestionId}" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </td>
                                                                                        <td class="text-center"> ${status.count} </td>

                                                                                        <td>${question.question}</td>

                                                                                        <td class="text-center"> <a href="javascript:;" class="btn btn-sm red " onclick = "deletequestion(${question.securityQuestionId});"><i class="fa fa-trash"></i></a> </td>
                                                                                    </tr>
                                                                                    </c:forEach>
                                                                                </tbody>
                                                                            </table>
                                                                                <span style="color: green; font-size: 14px; display: block;" id="deleteresponse"></span>
                                                                            </div>
                                                                        </div>		
                                                                    </div>
                                                                    </div>	
                                                                </div>
                                                    </div>
                                                   
                                                    <div class="tab-pane active" id="tab_1_2">
                                                        <div class="row clearfix">
                                                            <div class="clearfix col-sm-12" >
								<div class="portlet box red   portlet-datatable" style="margin-bottom:0px;">
                                                                    <div class="portlet-title">
									<div class="caption">
									Profile Settings
									</div>
                                                                    </div>
                                                                    <div class="portlet-body">
									<div class="col-sm-6" style="margin:0 auto; float:none;">
                                                                            <form action="javascript:;" class="form-horizontal">
                                                                                <div class="form-body">
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-4 control-label">User Name</label>
                                                                                            <div class="col-sm-8">
												<div class="input-group">
                                                                                                    <span class="input-group-addon input-circle-left">
													<i class="fa fa-user"></i>
                                                                                                    </span>
                                                                                                    <input type="text" class="form-control input-circle-right" readonly placeholder="" value="${user.userName}">
												</div>
                                                                                                
                                                                                            <!--<span class="help-block"> A block of help text. </span>-->
                                                                                            </div>
                                                                                    </div>
											
                                                                                    <div class="form-group">
                                                                                    <label class="col-sm-4 control-label">Current Password</label>
                                                                                        <div class="col-sm-8">
                                                                                            <div class="input-group">
												<span class="input-group-addon input-circle-left">
                                                                                                    <i class="fa fa-lock"></i>
												</span>
												<input type="password" class="form-control input-circle-right" placeholder="Current Password" value="" id="currentPassword" />
                                                                                            </div>
                                                                                            <span class="error-message" id="errorMessageCurrentPassword"></span>
                                                                                        <!--<span class="help-block"> A block of help text. </span>-->
                                                                                        </div>
                                                                                    </div>
                                                                                                
                                                                                    <div class="form-group">
                                                                                    <label class="col-sm-4 control-label">New Password</label>
                                                                                        <div class="col-sm-8">
                                                                                            <div class="input-group">
												<span class="input-group-addon input-circle-left">
                                                                                                    <i class="fa fa-lock"></i>
												</span>
												<input  type="password" class="form-control input-circle-right" placeholder="New Password" value="" id="newPassword"/>
                                                                                            </div>
                                                                                            <span class="error-message" id="errorMessageNewPassword"></span>
                                                                                        <!--<span class="help-block"> A block of help text. </span>-->
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-4 control-label">Confirm Password</label>
                                                                                            <div class="col-sm-8">
                                                                                                <div class="input-group">
                                                                                                    <span class="input-group-addon input-circle-left">
													<i class="fa fa-lock"></i>
                                                                                                    </span>
                                                                                                    <input type="password" class="form-control input-circle-right" placeholder="Confirm Password" value="" id="confirmPassword">
												</div>
                                                                                                <span class="error-message" id="errorMessageConfirmPassword"></span>
                                                                                            <!--<span class="help-block"> A block of help text. </span>-->
                                                                                            </div>
                                                                                    </div>
                                                      
                                                                                </div>
                                                                                <div class="form-actions">
                                                                                    <div class="row">
                                                                                        <div class="col-md-offset-4 col-md-8 text-right">
                                                                                            <span style="color: green; display: block; font-size: 14px; margin-bottom:25px;" id="changepasswordresponse"></span>
                                                                                            <button type="submit" class="btn btn-circle btn-success" id="changePassword">Update</button>
                                                                                            <!--<button type="button" class="btn btn-circle grey-salsa btn-outline red-hover">Cancel</button>-->
                                                                                            
                                                                                        </div>
                                                                                        <span  style="color: #1e9765; font-size: 14px; display: block; padding-left:180px; " id="errorMessageChangePassword"></span>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
									</div>
                                                                    </div>							
								</div>
                                                            </div>
							</div>
<!--                                                        <div class="col-sm-4">
                                                                <div class="border border-default use-div">
                                                                    <h4>How to Work</h4>
                                                                    <p>
                                                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
                                                                    </p><p> Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.
                                                                    </p>
                                                                </div>
                                                        </div>-->
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_1_3">
                                                        <div class="row clearfix">
								<div class="clearfix col-sm-12">
                                                                    <div class="portlet box red   portlet-datatable  ">
									<div class="portlet-title">
                                                                            <div class="caption">
                                                                                Other Settings
                                                                            </div>
									</div>
                                                                        <div class="portlet-body">
                                                                            <form action="javascript:;" class="form-horizontal">
                                                                                    <div class="form-body">
											<div class="form-group">
                                                                                            <label class="col-sm-4 control-label">Security Key</label>
                                                                                            <div class="col-sm-4">
                                                                                                <div class="input-group">
                                                                                                    <span class="input-group-addon input-circle-left">
                                                                                                            <i class="fa fa-key"></i>
                                                                                                    </span>
                                                                                                    <input id="securitykey" class="form-control input-circle-right" readonly type="password" value="${user.securityKey}">
                                                                                                </div>
                                                                                                <span class="error-message" id="keyerror" ></span>
                                                                                                <span style="color: green; display: block; font-size: 14px;" id="keysuccess" ></span>
                                                                                            </div>
                                                                                            <div class="col-sm-2 text-right">
                                                                                                <a href="javascript:;" class="btn btn-circle btn-success" id="viewsecuritykey">View</a>
                                                                                            </div>
											</div>
                                                                                    </div>
                                                                            </form>
                                                                            <hr>
                                                                                <form action="javascript:;" class="form-horizontal">
                                                                                    <div class="form-body">
											<div class="form-group">
                                                                                            <label class="col-sm-4 control-label">Automatic Logout Time (In Minutes)</label>
                                                                                            <div class="col-sm-4">
												<input id="touchspin_1" type="text" value="${logouttime}" name="demo1"  class="form-control input-circle ">
                                                                                                <span style="color: red; display: block; font-size: 14px;" id="timeerror" ></span>
                                                                                                <span style="color: green; display: block; font-size: 14px;" id="timesuccess" ></span>
                                                                                            </div>
                                                                                            <c:if test="${logouttime == ''}">
                                                                                                <div class="col-sm-2 text-right">
                                                                                                    <a href="javascript:;" class="btn btn-circle btn-success" id="savelogouttime">Add</a>
                                                                                                </div>    
                                                                                            </c:if>
                                                                                            <c:if test="${logouttime != ''}">
                                                                                                <div class="col-sm-2 text-right">
                                                                                                    <a href="javascript:;" class="btn btn-circle btn-success" id="savelogouttime">Update</a>
                                                                                                </div>    
                                                                                            </c:if>
											</div>
                                                                                    </div>
										</form>
                                                                                <hr>
                                                                                <form action="javascript:;" class="form-horizontal">
                                                                                    <div class="form-body" style="text-align: center;">
											<div class="form-group">
                                                                                            <label class="control-label">Back up</label>
                                                                                            <a href="javascript:;" class="btn btn-circle btn-success" id="dbbackup">Backup</a>
											</div>
                                                                                    </div>
										</form>
                                                                                <hr>
                                                                                <form action="javascript:;" class="form-horizontal">
                                                                                    <div class="form-body" style="text-align: center;">
											<div class="form-group">
                                                                                            <label class="control-label">Restore</label>
                                                                                            <span class="btn default btn-file">
                                                                                                        <input type="file" name="dbfile" id="dbfile" /> </span> 
                                                                                            <a href="javascript:;" class="btn btn-circle btn-success" id="dbrestore">Restore</a>
											</div>
                                                                                    </div>
										</form>
                                                                                <div id="loading" style="display: none;">
                                                                                    <center>
                                                                                        <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/24.gif" />
                                                                                    </center>
                                                                                </div>
                                                                                <span style="color: red; display: block; font-size: 14px; margin-left: 350px;" id="responsemessage" ></span>
                                                                                <span style="color: green; display: block; font-size: 14px; margin-left: 350px;" id="successrestore" ></span>
                                                                        </div>		
                                                                    </div>
                                                                </div>	
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_1_4">
                                                        <div class="row clearfix">
								<div class="clearfix col-sm-12">
                                                                    <div class="portlet box red   portlet-datatable  ">
									<div class="portlet-title">
                                                                            <div class="caption">
                                                                                Database History
                                                                            </div>
									</div>
                                                                        <div class="portlet-body clearfix">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable order-column userlist-table datatable" id="sample_6">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="table-checkbox">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </th>
                                                                                        <th> SN. </th>
                                                                                        <th> Notification </th>
                                                                                        <th> Date and Time </th>
                                                                                    </tr>
                                                                                </thead>

                                                                                <tbody id="performactions">
                                                                                    <c:forEach items="${backuphistory}" var="history" varStatus="status">
                                                                                        <tr class="odd gradeX">
                                                                                            <td>
                                                                                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                    <input type="checkbox" name="" class="checkboxes"  />
                                                                                                    <span></span>
                                                                                                </label>
                                                                                            </td>
                                                                                            <td>${status.count}</td>
                                                                                            <td>${history.key}</td>
                                                                                            <td>${history.value}</td>
                                                                                        </tr>
                                                                                    </c:forEach>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>		
                                                                    </div>
                                                                </div>	
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div>
                            <!-- END PROFILE CONTENT -->
                        </div>
                    </div>


                </div>
                <!-- END CONTENT BODY --> 
            </div>
            <!-- END CONTENT --> 
            <!-- BEGIN QUICK SIDEBAR --> 
            <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 

            <!-- END QUICK SIDEBAR --> 
        </div>
        <!-- END CONTAINER --> 
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; Organization </div>
            <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
        </div>
        <!-- END FOOTER --> 
        <!-- Authentication modal -->
        <div class="modal fade" id="authenticate" tabindex="-1" role="authenticate" aria-hidden="true" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Authenticating</h4>
                    </div>
                    <div class="modal-body"> 
                        <div class="form-group clearfix">
                            <label class="col-sm-3 control-label">User Name</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon input-circle-left">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input type="text" placeholder="userName" id="username" class="form-control input-circle-right clearfix" />
                                </div>
                                <span style="color: red; display: block; font-size: 14px;" id="nameerror" ></span>
                            </div>
			</div>
                        <div class="form-group clearfix">
                            <label class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon input-circle-left">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                    <input type="password" placeholder="password" id="psw" class="form-control input-circle-right clearfix" />
                                </div>
                                <span style="color: red; display: block; font-size: 14px;" id="pswerror" ></span>
                            </div>
			</div>
<!--                    <div class="form-group">
                            <input type="text" placeholder="userName" id="username" class="form-control input-circle clearfix" /><br>
                            <span style="color: red; display: block; font-size: 14px;" id="nameerror" ></span>
                            <input type="password" placeholder="password" id="psw" class="form-control input-circle clearfix" />
                            <span style="color: red; display: block; font-size: 14px;" id="pswerror" ></span>
                        </div>-->
                    </div>
                    <div class="modal-footer">
                        <span style="color: red; display: block; font-size: 14px; float: left;" id="authenticationerror" ></span>
                        <button type="button" class=" btn btn-circle btn-danger" data-dismiss="modal">Cancel</button>
<!--                        <button type="button" class="btn green" id="">YES</button>-->
                        <a href="javascript:;" class=" btn btn-circle btn-success" id="checkandviewkey">Continue</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        
        <!-- BEGIN CORE PLUGINS --> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
        <!-- END CORE PLUGINS --> 
        <!-- BEGIN THEME GLOBAL SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
        <!-- END THEME GLOBAL SCRIPTS --> 

        <!-- BEGIN THEME LAYOUT SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
        <!-- END THEME LAYOUT SCRIPTS --> 
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="${pageContext.request.contextPath}/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="${pageContext.request.contextPath}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>

        <script>
            $(document).on("click", "#addSecurityQuestion", function () {
                var question = $("#question").val().trim();
                $("#deleteresponse").text("");
                if (question === null || question === "") {
                    $("#errorMessageQuestion").text("Please Enter Question.");
                } else {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/addsecurityquestion", {question: question},
                            function (data, status) {
                                if (data === "success") {
                                    $("#successMessageQuestion").text("Security question has been added successfully");
                                    setTimeout(function()
                                        {
                                            window.location.reload();
                                        }, 1000);
                                    
                                } else if (data === "Please login again.") {
                                    window.location.href = "${pageContext.request.contextPath}/";
                                } else {
                                    $("#errorMessageQuestion").text(data);
                                }
                            }
                    );
                }
            });

            function deletequestion(questionId){
                
                var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/deletesecurityquestion", {questionId: questionId},
                            function (data, status) {
                                console.log(data);
                                if (data === "success") {
                                    $("#deleteresponse").text("Security question has been deleted successfully");
                                    setTimeout(function()
                                        {
                                            window.location.reload();
                                        }, 1000);
                                    
                                } else if (data === "Please login again.") {
                                    window.location.href = "${pageContext.request.contextPath}/";
                                } else {
                                    $("#deleteresponse").text(data);
                                }
                            }
                    );
            }
            
            $(document).on("keyup", "#question", function () {
                var question = $("#question").val();
                $("#deleteresponse").text("");
                if (question === "") {
                    $("#errorMessageQuestion").text("Please Enter Question.");
                } else {
                    $("#errorMessageQuestion").text("");
                }
            });
            
            $("#resetquestion").on("click", function(){
                $("#question").val("");
                $("#deleteresponse").text("");
            });
            
            $(document).on("click", "#changePassword", function () {
                var currentPassword = $("#currentPassword").val();
                var newPassword = $("#newPassword").val();
                var confirmPassword = $("#confirmPassword").val();
                var flag = true;
                if (currentPassword === null || currentPassword === "") {
                    $("#errorMessageCurrentPassword").text("Please enter current password.");
                    flag = false;
                }
                if (newPassword === null || newPassword === "") {
                    $("#errorMessageNewPassword").text("Please enter new password.");
                    flag = false;
                }
                if (confirmPassword === null || confirmPassword === "") {
                    $("#errorMessageConfirmPassword").text("Please enter confirm password.");
                    flag = false;
                }
                if (newPassword !== confirmPassword) {
                    $("#errorMessageConfirmPassword").text("New password and confirm password are mismatch.");
                    flag = false;
                }
                if (flag) {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/changepassword", {currentPassword: currentPassword, newPassword: newPassword},
                            function (data, status) {
                                if (data === "success") {
                                    $("#changepasswordresponse").text("Password has been changed successfully.");
                                    setTimeout(function()
                                        {
                                            window.location.href = "${pageContext.request.contextPath}/";
                                        }, 1000);
                                    
                                } else if (data === "Please login again.") {
                                    window.location.href = "${pageContext.request.contextPath}/";
                                } else {
                                    $("#errorMessageChangePassword").text(data);
                                }
                            }
                    );
                }
            });

            $(document).on("keyup", "#currentPassword", function () {
                var currentPassword = $("#currentPassword").val();
                if (currentPassword === "") {
                    $("#errorMessageCurrentPassword").text("Please Enter Current Password.");
                } else {
                    $("#errorMessageCurrentPassword").text("");
                }
            });
            $(document).on("keyup", "#newPassword", function () {
                var newPassword = $("#newPassword").val();
                if (newPassword === "") {
                    $("#errorMessageNewPassword").text("Please Enter new Password.");
                } else {
                    $("#errorMessageNewPassword").text("");
                }
            });
            $(document).on("keyup", "#confirmPassword", function () {
                var confirmPassword = $("#confirmPassword").val();
                if (confirmPassword === "") {
                    $("#errorMessageConfirmPassword").text("Please Enter confirm Password.");
                } else {
                    $("#errorMessageConfirmPassword").text("");
                }
            });
            
        </script>
        <script>
            $("#savelogouttime").on("click", function(){
                var logout = $("#touchspin_1").val();
                var numericExp=/^[0-9]+$/;
                var valid = true;
                console.log(logout);
                if(logout ===""){
                    $("#timeerror").text("Please Enter Logout Time");
                    valid = false;
                } else if(logout.match(numericExp)){
                    $("#timeerror").text("");
                } else{
                    $("#timeerror").text("Please Enter Valid Time");
                    valid = false;
                }
                if(valid){
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/savelogouttime", {logouttime: logout},
                            function (data, status) {
                                console.log(data);
                                if (data === "success") {
                                    $("#timesuccess").text("Time Successfully saved");
                                    setTimeout(function()
                                    {
                                        window.location.reload();
                                    }, 1000);
                                } else if (data === "Please login again.") {
                                     $("#timeerror").val("");
                                    window.location.href = "${pageContext.request.contextPath}/";
                                } else {
                                    $("#timeerror").text(data);
                                }
                            }
                    );
                }
                
            });
        </script>
        <script>
            $("#viewsecuritykey").on("click", function(){
                var security = $("#securitykey").val();
                $("#username").val("");
                $("#psw").val("");
                $("#nameerror").text("");
                $("#pswerror").text("");
                $("#authenticationerror").text("");
                $("#authenticate").modal("show");
            });
            
            $("#checkandviewkey").on("click", function(){
                var username = $("#username").val();
                var password = $("#psw").val();
                var flag = true;
                if(username === ""){
                    $("#nameerror").text("Please Enter UserName");
                    flag = false;
                }
                if(password === ""){
                    $("#pswerror").text("Please Enter Password");
                    flag = false;
                }
                if(flag){
                $.post("${pageContext.request.contextPath}/login", {userName: username, password: password},
                            function (data, status) {
                                console.log(data);
                                if (data.indexOf("success") > -1) {
                                    $("#nameerror").text("");
                                    $("#pswerror").text("");
                                    $("#securitykey").prop("type", "text");
                                    setTimeout(function()
                                        {
                                            $("#securitykey").prop("type", "password");
                                        }, 5000);
                                    $("#authenticate").modal("hide");
                                } else {
                                    $("#nameerror").text("");
                                    $("#pswerror").text("");
                                    $("#authenticationerror").text(data);
                                }
                            }
                    );
                }
            });
            
            $("#dbbackup").on("click", function(){
            var Uname = "${user.userName}".replace(" ", "");
                window.location.href = "${pageContext.request.contextPath}/"+Uname+"/takedbbackup";
                setTimeout(function()
                    { 
                        window.location.reload();
                    }, 1000);
            });
            $("#dbrestore").on("click", function(){
                var formdata = new FormData();
                formdata.append("dbfile", dbfile.files[0]);
                if($("#dbfile").val() === ''){
                    $("#responsemessage").text("Please Select a file.");
                }else{
                    var Uname = "${user.userName}".replace(" ", "");
                    $("#loading").css('display','block');
                    $.ajax({
                        url: "${pageContext.request.contextPath}/"+Uname+"/restoredb",
                        type: 'POST',
                        data:  formdata,
                        dataType: 'text',
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            if(data.indexOf("success") > -1){
                                $("#loading").css("display","none");
                                $("#successrestore").text("Database Restored Successfully");
                                setTimeout(function()
                                { 
                                    window.location.reload();
                                }, 1000);
                            } else {
                                $("#loading").css("display","none");
                                $("#responsemessage").text(data);
                            }
                        }
                    });
                }
                
//            var Uname = "${user.userName}".replace(" ", "");
//                window.location.href = "${pageContext.request.contextPath}/"+Uname+"/restoredb";
            });
            
        </script>
        <script>
            $(document).ready(function () {
                $('#side-menu').find('li').removeClass('active');
                $(document).find(".setting").addClass('active');
                
                $("#currentPassword").val("");
            }); 
        </script>
        <script>
        $( ".form-control" ).focus(function() {
            $(this).prev('.input-group-addon').removeClass().addClass('input-group-addon-focus');
            $(this).next('.input-group-addon').removeClass().addClass('input-group-addon-focus');
        });

        $( ".form-control" ).focusout(function() {
            $(this).prev('.input-group-addon-focus').removeClass().addClass('input-group-addon');
            $(this).next('.input-group-addon-focus').removeClass().addClass('input-group-addon');
        });
        </script>
    </body>
</html>