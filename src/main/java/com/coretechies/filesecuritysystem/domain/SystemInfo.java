/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author CoreTechies M8
 */
@Entity
@Table(name = "systeminfo")
@Access(AccessType.FIELD)
public class SystemInfo implements Serializable {

    @Id
    @GeneratedValue(generator = "systeminfo")
    @GenericGenerator(name = "systeminfo", strategy = "increment")
    private Integer systemInfoId;

    @Column
    private String systemName;

    @Column
    private String systemId;

    @Column
    private Integer userId;
    
    @Column
    private Integer createdBy;
    
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date creationDate;
    
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date updationDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId", insertable = false, updatable = false, nullable = false, unique = false)
    private UserInfo userInfo;

    public Integer getSystemInfoId() {
        return systemInfoId;
    }

    public void setSystemInfoId(Integer systemInfoId) {
        this.systemInfoId = systemInfoId;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getUpdationDate() {
        return updationDate;
    }

    public void setUpdationDate(Date updationDate) {
        this.updationDate = updationDate;
    }

    @Override
    public String toString() {
        return "SystemInfo{" + "systemInfoId=" + systemInfoId + ", systemName=" + systemName + ", systemId=" + systemId + ", userId=" + userId + '}';
    }

}
