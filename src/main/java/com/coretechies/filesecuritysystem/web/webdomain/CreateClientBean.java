/*
 * To change this license header; choose License Headers in Project Properties.
 * To change this template file; choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.web.webdomain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author CoreTechies M8
 */
public class CreateClientBean {

    private Integer userId;

    @NotBlank(message = "Please enter user name")
    private String newUserName;

    @NotBlank(message = "Please enter company name")
    private String company;

    @NotBlank(message = "Please enter email id")
    @Email(message = "Please enter valid email Id")
    private String emailId;

    @NotNull(message = "Please select country")
    private Integer country;

//    @NotNull(message = "Please select mobile code")
    private Integer mobileNumberCode;

    @Pattern(regexp = "[0-9]{6,}", message = "Please enter valid mobile number.")
    private String mobileNumber;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getNewUserName() {
        return newUserName;
    }

    public void setNewUserName(String newUserName) {
        this.newUserName = newUserName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public Integer getCountry() {
        return country;
    }

    public void setCountry(Integer country) {
        this.country = country;
    }

    public Integer getMobileNumberCode() {
        return mobileNumberCode;
    }

    public void setMobileNumberCode(Integer mobileNumberCode) {
        this.mobileNumberCode = mobileNumberCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public String toString() {
        return "CreateClientBean{" + "userId=" + userId + ", newUserName=" + newUserName + ", company=" + company + ", emailId=" + emailId + ", country=" + country + ", mobileNumberCode=" + mobileNumberCode + ", mobileNumber=" + mobileNumber + '}';
    }

}
