/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.controller;

import com.coretechies.filesecuritysystem.algorithm.UserType;
import com.coretechies.filesecuritysystem.dao.AdminLicenseDao;
import com.coretechies.filesecuritysystem.dao.CourseDao;
import com.coretechies.filesecuritysystem.dao.NotificationDao;
import com.coretechies.filesecuritysystem.dao.UserInfoDao;
import com.coretechies.filesecuritysystem.dao.UserLicenseDao;
import com.coretechies.filesecuritysystem.dao.WatermarkDao;
import com.coretechies.filesecuritysystem.domain.AdminLicense;
import com.coretechies.filesecuritysystem.domain.Course;
import com.coretechies.filesecuritysystem.domain.Notification;
import com.coretechies.filesecuritysystem.domain.SystemInfo;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.domain.UserLicense;
import com.coretechies.filesecuritysystem.security.InvalidInformationException;
import com.coretechies.filesecuritysystem.security.LicenseInformation;
import com.coretechies.filesecuritysystem.security.RandomPasswordGenerator;
import com.coretechies.filesecuritysystem.security.SHA256;
import com.coretechies.filesecuritysystem.security.SystemInformation;
import com.coretechies.filesecuritysystem.security.UserLicenseInformation;
import com.coretechies.filesecuritysystem.security.UserSystemInformation;
import com.coretechies.filesecuritysystem.service.AdminLicenseService;
import com.coretechies.filesecuritysystem.service.SystemInfoService;
import com.coretechies.filesecuritysystem.service.UserInfoService;
import com.coretechies.filesecuritysystem.service.UserLicenseService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.CodeSource;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * The LicenseController class implements an application that handles all the requests regarding to the license,
 * like create license, delete license, renew license, view license history,
 * generate an OTP and verify it for license generation.
 * 
 * @Controller annotation represents that this is a java controller class.
 * @SessionAttributes annotation represents a user in session.
 * "user" is a object of userInfo class and have a complete information
 * of that user who is present in the session at that time.
 * And "otp" is object[] for license generation.
 * 
 * @author Jaishree
 * @version 1.0
 * @since 2016-09-03
 */
@Controller
@SessionAttributes({"user", "OTP"})
public class LicenseController {

    @Autowired
    private UserInfoDao userInfoDao;

    @Autowired
    private UserInfoService userInfoService;
    
    @Autowired
    private CourseDao courseDao;

    @Autowired
    private SystemInfoService systemInfoService;

    @Autowired
    private AdminLicenseDao adminLicenseDao;

    @Autowired
    private WatermarkDao watermarkDao;

    @Autowired
    private UserLicenseDao userLicenseDao;

    @Autowired
    private NotificationDao notificationDao;

    @Autowired
    private AdminLicenseService adminLicenseService;
    
    @Autowired
    private MailController mailController;

    @Autowired
    private UserLicenseService userLicenseService;

    @Autowired
    private SHA256 sha256;
    
    private final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

//    public LicenseController(){
//        System.out.println("License Controller Constructor..."); 
//    }
//    public Integer getUserID(Integer userid){
//        Map<Integer, Integer> map = userInfoDao.showUserIdThroughCount();
//        System.out.println("map = "+map);
//        int value = 0;
//        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
//            if(entry.getKey() == userid){
//                value = entry.getValue();
//            }
//        }
//        System.out.println("value = "+value);
//        return 0;
//    }
    /**
     * This url is called if the user request to create a license,
     * If the admin request for this then this method will take the user to the create_admin_license page,
     * And if a client request for this then this method will take the user to the createuserlicense page.
     * 
     * @param userName
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/createlicense")
    public ModelAndView getCreateLicense(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                modelMap.addAttribute("clientusers", userInfoDao.getUserInfoByUserType(UserType.Client.getUserTypeValue()));
                modelMap.addAttribute("today", new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
                return new ModelAndView("create_admin_license");
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                modelMap.addAttribute("users", userInfoDao.getUserInfoByCreatedBy(userInfo.getUserInfoId()));
                modelMap.addAttribute("courses", courseDao.getCourses(userInfo.getUserInfoId()));
                modelMap.addAttribute("watermarks", watermarkDao.getWatermarkList(userInfo.getUserInfoId()));
                return new ModelAndView("createuserlicense");
            } else {
                return new ModelAndView("redirect:/");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }
    
    @GetMapping(value = "/{userName}/selectcoursesforlicense/{selecteduser}")
    public ModelAndView selectCourseForLicense(@PathVariable String userName, @PathVariable Integer selecteduser, ModelMap modelMap){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                modelMap.addAttribute("selecteduser", selecteduser);
                modelMap.addAttribute("courses", courseDao.getCourses(userInfo.getUserInfoId()));
//                System.out.println(courseDao.getCourses(userInfo.getUserInfoId()));
                return new ModelAndView("selectcourseforcreatelicense");
            } else {
                return new ModelAndView("redirect:/");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }
    
    @GetMapping(value = "/{userName}/generatelicense/{selecteduser}/{selectedcourse}")
    public ModelAndView getGenerateLicense(@PathVariable String userName, @PathVariable Integer selecteduser, @PathVariable Integer selectedcourse, ModelMap modelMap){
       UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserInfo userInfo1 = userInfoDao.getUserInfo(selecteduser);
                System.out.println(userInfo1);
                modelMap.addAttribute("selecteduser", userInfo1);
                Course course = courseDao.getCourse(userInfo.getUserInfoId(), selectedcourse);
                modelMap.addAttribute("selectedcourse", course);
                modelMap.addAttribute("watermarks", watermarkDao.getWatermarkList(userInfo.getUserInfoId()));
                return new ModelAndView("createuserlicense");
            } else {
                return new ModelAndView("redirect:/");
            }
        } else {
            return new ModelAndView("redirect:/");
        } 
    }

    @PostMapping(value = "/{userName}/confirmdisplayforlicense")
    @ResponseBody
    public String confirmDisplayForLicense(@PathVariable String userName, ModelMap modelMap){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserLicense userLicense = userLicenseDao.getConfirmLicenseForDisplay();
                System.out.println("confirmDisplayForLicense"+userLicense);
                String Country = userLicense.getUserInfo().getCountryCode().getCountry();
                Map<Integer, Integer> showUserIdThroughCount = userInfoDao.showUserIdThroughCount();
                Integer value = 0;
                for (Map.Entry<Integer, Integer> entry : showUserIdThroughCount.entrySet()) {
                   if(entry.getKey() == userLicense.getUserId()){
                        value = entry.getValue();
                    }
                }
                System.out.println("license id==="+userLicense.getUserLicenseId());
                value = value + 1000;
                String validity;
                if(userLicense.getExpiryDate() == null){
                    validity = "Unlimited";
                } else{
                    validity = new SimpleDateFormat("dd-MM-yyyy").format(userLicense.getIssueDate())+" to "+new SimpleDateFormat("dd-MM-yyyy").format(userLicense.getExpiryDate());
                }
                return "success#{\"userName\":\""+userLicense.getUserInfo().getUserName()+"\","
                            +"\"courseName\":\""+userLicense.getCourse().getCourseName()+"\","
                            +"\"validity\":\""+validity+ "\","
                            + "\"userId\":\"AD"+value + "\","
                        + "\"country\":\""+Country+"\","
                        + "\"licenseId\":\""+userLicense.getUserLicenseId()+"\"}";
            } else {
                return "Unauthorized Access";
            }
        } else {
            return "Login Again";
        } 
    }
    
    /**
     * This method will generate a license for a client by admin,
     * the method have the complete information about license in method parameters, 
     * if the values of these parameters are correct then it will add the entry in database and
     * generate an otp(random number).
     * And if any value have a wrong entry then it will return an appropriate error message.
     * 
     * @param clientuserid
     * @param lifetime
     * @param validity
     * @param startDate
     * @param endDate
     * @param desktopunlimited
     * @param dekstoplicenseLimit
     * @param weblicenseLimit
     * @param systemId
     * @param unlimitedcourse
     * @param courselimit
     * @param courseSetting
     * @param smsTransaction
     * @param webunlimited
     * @param smsPromotion
     * @param sendOtpOn
     * @param userName
     * @param modelMap
     * @param request
     * @return String
     * @throws ParseException
     */
    @PostMapping(value = "/{userName}/createlicense")
    @ResponseBody
    public String createLicense(@RequestParam Integer clientuserid, @RequestParam Boolean lifetime, @RequestParam Integer validity, @RequestParam String startDate,
            @RequestParam String endDate, @RequestParam Boolean webunlimited, @RequestParam Boolean desktopunlimited,@RequestParam Integer dekstoplicenseLimit,
            @RequestParam Integer weblicenseLimit, @RequestParam Boolean unlimitedcourse, @RequestParam Integer courselimit, @RequestParam Integer courseSetting,
            @RequestParam Integer smsTransaction, @RequestParam Integer smsPromotion, @RequestParam String systemId, @RequestParam Integer sendOtpOn,
            @PathVariable String userName, ModelMap modelMap, HttpServletRequest request) throws ParseException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
       
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            modelMap.addAttribute("OTP", null);
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                if (modelMap.get("OTP") != null) {
                    return "Unable to create multiple license at a time.";
                } else if (clientuserid == null || clientuserid == 0) {
                    return "Please select Admin.";
                } else if (systemId == null || systemId.trim().isEmpty()) {
                    return "System Id is empty.";
                } else if (adminLicenseDao.getAdminLicenseByClient(clientuserid) != null) {
                    return "This user license already created you can renew license not create license.";
                } else if(!lifetime){
                    if (startDate == null || startDate.trim().isEmpty()) {
                        return "Please select Issue Date.";
                    } else if (endDate == null || endDate.trim().isEmpty()) {
                        return "Please select Expiry Date.";
                    } else if (format.parse(endDate).compareTo(format.parse(startDate)) < 0) {
                        return "Exiry Date is less than Issue Date.";
                    } else if (!webunlimited){
                        if(weblicenseLimit==null){
                             return "Please enter web license limit.";
                        } else{
                            return getAdminLicense(userInfo, clientuserid, validity, startDate, endDate, systemId, webunlimited, desktopunlimited, weblicenseLimit, dekstoplicenseLimit, unlimitedcourse, courselimit, courseSetting, smsTransaction, smsPromotion, modelMap, request, sendOtpOn);
                        }
                    } else if(!desktopunlimited){
                        if(dekstoplicenseLimit==null){
                             return "Please enter desktop license limit.";
                        } else{
                            return getAdminLicense(userInfo, clientuserid, validity, startDate, endDate, systemId, webunlimited, desktopunlimited, weblicenseLimit, dekstoplicenseLimit, unlimitedcourse, courselimit, courseSetting, smsTransaction, smsPromotion, modelMap, request, sendOtpOn);
                        }
                    } else {
                        return getAdminLicense(userInfo, clientuserid, validity, startDate, endDate, systemId, webunlimited, desktopunlimited, weblicenseLimit, dekstoplicenseLimit, unlimitedcourse, courselimit, courseSetting, smsTransaction, smsPromotion, modelMap, request, sendOtpOn);
                    }
                } else{
                    if (!webunlimited ){
                        if(weblicenseLimit==null){
                            return "Please enter web license limits.";
                        }else{
                            return getLifetimeAdminLicense(userInfo, clientuserid, startDate, systemId, webunlimited, desktopunlimited, weblicenseLimit, dekstoplicenseLimit, unlimitedcourse, courselimit, courseSetting, smsTransaction, smsPromotion, modelMap, request, sendOtpOn);
                        }
                    } else if(!desktopunlimited){
                        if(dekstoplicenseLimit==null){
                            return "Please enter desktop license limits.";
                        }else{
                            return getLifetimeAdminLicense(userInfo, clientuserid, startDate, systemId, webunlimited, desktopunlimited, weblicenseLimit, dekstoplicenseLimit, unlimitedcourse, courselimit, courseSetting, smsTransaction, smsPromotion, modelMap, request, sendOtpOn);
                        }
                    } else {
                        return getLifetimeAdminLicense(userInfo, clientuserid, startDate, systemId, webunlimited, desktopunlimited, weblicenseLimit, dekstoplicenseLimit, unlimitedcourse, courselimit, courseSetting, smsTransaction, smsPromotion, modelMap, request, sendOtpOn);
                    }
                }
            } else {
                return "Please login again.";
            }
        } else {
            return "Please login again.";
        }
    }

    private String getAdminLicense(UserInfo userInfo, Integer clientuserid, Integer validity, String startDate, String endDate, String systemId,
            Boolean webunlimited, Boolean desktopunlimited, Integer weblicenseLimit, Integer dekstoplicenseLimit, Boolean unlimitedcourse, Integer courselimit,
            Integer courseSetting, Integer smsTransaction, Integer smsPromotion, ModelMap modelMap, HttpServletRequest request, Integer sendOtpOn) throws ParseException{
        try {
            AdminLicense adminLicense = adminLicenseService.getAdminLicense(userInfo.getUserInfoId(), clientuserid, validity, format.parse(startDate), format.parse(endDate), systemId, webunlimited,
                    desktopunlimited, weblicenseLimit, dekstoplicenseLimit, unlimitedcourse, courselimit, courseSetting, smsTransaction, smsPromotion);
            return generateOTP(adminLicense, modelMap, request, sendOtpOn);
        } catch (InvalidInformationException ex) {
            Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
    }
    
    private String getLifetimeAdminLicense(UserInfo userInfo, Integer clientuserid, String startDate, String systemId,
            Boolean webunlimited, Boolean desktopunlimited, Integer weblicenseLimit, Integer dekstoplicenseLimit, Boolean unlimitedcourse, Integer courselimit,
            Integer courseSetting, Integer smsTransaction, Integer smsPromotion, ModelMap modelMap, HttpServletRequest request, Integer sendOtpOn) throws ParseException{
        try {
            AdminLicense adminLicense = adminLicenseService.getAdminLicense(userInfo.getUserInfoId(), clientuserid, 0, format.parse(startDate), null, systemId, webunlimited,
                    desktopunlimited, weblicenseLimit, dekstoplicenseLimit, unlimitedcourse, courselimit, courseSetting, smsTransaction, smsPromotion);
            return generateOTP(adminLicense, modelMap, request, sendOtpOn);
        } catch (InvalidInformationException ex) {
            Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
    }
    /**
     * This method will generate a license for a user by client,
     * the method have the complete information about license in method parameters, 
     * if the values of these parameters are correct then it will add the entry in database and
     * generate an otp(random number).
     * And if any value have a wrong entry then it will return an appropriate error message.
     * 
     * @param userId
     * @param courseId
     * @param lifetime
     * @param validity
     * @param startDate
     * @param endDate
     * @param systemId
     * @param watermarkId
     * @param sendOtpOn
     * @param userName
     * @param modelMap
     * @param request
     * @return
     * @throws ParseException
     */
    @PostMapping(value = "/{userName}/createuserlicense")
    @ResponseBody
    public String createUserLicense(@RequestParam Integer userId, @RequestParam Integer courseId, @RequestParam Boolean lifetime,
            @RequestParam Integer validity, @RequestParam String startDate, @RequestParam String endDate,
            @RequestParam Integer systemId, @RequestParam Integer watermarkId, @RequestParam Integer sendOtpOn, @PathVariable String userName, ModelMap modelMap, HttpServletRequest request) throws ParseException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            modelMap.addAttribute("OTP", null);
             System.out.println("modelMap.get(\"OTP\")"+((Object[])modelMap.get("OTP")));
             System.out.println(startDate);
             System.out.println(endDate);
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                if (modelMap.get("OTP") != null) {
                    return "Unable to create multiple license at a time.";
                } else if (userId == null || userId == 0) {
                    return "Please select User.";
                } else if (courseId == null || courseId == 0) {
                    return "Please select Course.";
                } else if (systemId == null || systemId == 0) {
                    return "Please select System.";
                } else if (watermarkId == null || watermarkId == 0) {
                    return "Please select Watermark.";
                } else if(!lifetime){
                    if (startDate == null || startDate.trim().isEmpty()) {
                        return "Please select Issue Date.";
                    } else if (endDate == null || endDate.trim().isEmpty()) {
                        return "Please select Expiry Date.";
                    } else if (format.parse(endDate).compareTo(format.parse(startDate)) < 0) {
                        return "Exiry Date is less than Issue Date.";
                    } else{
                        try {
                            if(validity == 0){
                                validity = null;
                            }
                            UserLicense userLicense = userLicenseService.getUserLicense(userInfo.getUserInfoId(), userId, courseId, validity, format.parse(startDate), format.parse(endDate), systemId, watermarkId);
                            return generateUserOTP(userLicense, modelMap, request, sendOtpOn);
                        } catch (InvalidInformationException ex) {
                            Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                            return ex.getMessage();
                        }
                    }
                } else {
                    try {
                        validity = 0;
                        startDate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                        Date expiryDate = null;
                        UserLicense userLicense = userLicenseService.getUserLicense(userInfo.getUserInfoId(), userId, courseId, validity, format.parse(startDate), expiryDate, systemId, watermarkId);
                        return generateUserOTP(userLicense, modelMap, request, sendOtpOn);
                    } catch (InvalidInformationException ex) {
                        Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                        return ex.getMessage();
                    }
                }
            } else {
                return "Please login again.";
            }
        } else {
            return "Please login again.";
        }
    }

    /**
     * This url is called if the user request to view the list of all license,
     * If the admin is in session then this method will return on admin_license page with list of all licenses of clients,
     * And if the client is in session then this method will return on license page with list of all license of users generated by the user.
     * 
     * @param userName
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/viewlicense")
    public ModelAndView viewLicense(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                modelMap.addAttribute("clientlicenses", adminLicenseDao.getAdminLicense());
                return new ModelAndView("admin_license");
//                return new ModelAndView("viewlicense");
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                modelMap.addAttribute("userlicenses", userLicenseDao.getUserLicenseByCreator(userInfo.getUserInfoId()));
                return new ModelAndView("license");
            } else {
                return new ModelAndView("redirect:/");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will delete a license present at specified licenseId 
     * and display a appropriate success message or error message. 
     * 
     * @param userName
     * @param licenseId
     * @param redirectAttributes
     * @param request
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/deletelicense/{licenseId}")
    public ModelAndView deleteLicense(@PathVariable String userName, @PathVariable Integer licenseId, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                if (adminLicenseDao.deleteAdminLicense(licenseId)) {
                    redirectAttributes.addFlashAttribute("message", "Client License Successfully deleted.");
                } else {
                    redirectAttributes.addFlashAttribute("message", "Unable to delete client license.");
                }
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserLicense userLicense = userLicenseDao.getUserLicense(licenseId);
                if (userLicense != null) {
                    userLicense.setStatus(Boolean.FALSE);
                    if (userLicenseDao.updateUserLicense(userLicense) != null) {
                        redirectAttributes.addFlashAttribute("message", "User License Successfully deleted.");
                    } else {
                        redirectAttributes.addFlashAttribute("message", "Unable to delete user license.");
                    }
                } else {
                    redirectAttributes.addFlashAttribute("message", "Unable to delete user license.");
                }
            }
            return new ModelAndView("redirect:/" + userName + "/viewlicense");
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This url is called on the request of user for renew license.
     * If the user is admin then the method will take the user to the renewlicense page,
     * And if the user is client then the method will take the user to the renewuserlicense page.
     * 
     * @param userName
     * @param licenseId
     * @param redirectAttributes
     * @param request
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/renewlicense/{licenseId}")
    public ModelAndView renewLicense(@PathVariable String userName, @PathVariable Integer licenseId, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                AdminLicense adminLicense = adminLicenseDao.getAdminLicense(licenseId);
                if (adminLicense != null) {
                    Date expiryDate = adminLicense.getExpiryDate();
                    if(expiryDate != null){
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(expiryDate);
                        cal.add(Calendar.DAY_OF_WEEK, 1); 
                        System.out.println("Next day::::"+cal.getTime());
                        adminLicense.setIssueDate(cal.getTime());
                        modelMap.addAttribute("license", adminLicense);
                        modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                        return new ModelAndView("renewlicense");
                    }else{
                        modelMap.addAttribute("license", adminLicense);
                        return new ModelAndView("renewlicense");
                    }
                }
                return new ModelAndView("redirect:/");
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserLicense userLicense = userLicenseDao.getUserLicense(licenseId);
                Date expiryDate = userLicense.getExpiryDate();
                Calendar cal = Calendar.getInstance();
                cal.setTime(expiryDate);
                cal.add(Calendar.DAY_OF_WEEK, 1); 
                System.out.println("Next day::::"+cal.getTime());
                userLicense.setIssueDate(cal.getTime());
                
                if (userLicense != null) {
                    modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                    modelMap.addAttribute("users", userInfoDao.getUserInfoByCreatedBy(userInfo.getUserInfoId()));
                    modelMap.addAttribute("courses", courseDao.getCourses(userInfo.getUserInfoId()));
                    modelMap.addAttribute("watermarks", watermarkDao.getWatermarkList(userInfo.getUserInfoId()));
                    modelMap.addAttribute("systems", systemInfoService.getSystemInfoListByUser(userLicense.getUserId()));
                    modelMap.addAttribute("license", userLicense);
                    return new ModelAndView("renewuserlicense");
                }
            } else {
                return new ModelAndView("redirect:/");
            }
            redirectAttributes.addFlashAttribute("message", "This license is no longer available.");
            return new ModelAndView("redirect:/" + userName + "/viewlicense");
        } else {
            return new ModelAndView("redirect:/");
        }
    }
    /**
     * 
     * @param userName
     * @param licenseId
     * @param redirectAttributes
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/editlicense/{licenseId}")
    public ModelAndView getEditLicense(@PathVariable String userName, @PathVariable Integer licenseId, RedirectAttributes redirectAttributes, ModelMap modelMap){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
         if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                AdminLicense adminLicense = adminLicenseService.getAdminLicenseById(licenseId);
                modelMap.addAttribute("license", adminLicense);
                System.out.println("adminlicense="+adminLicense);
                if(adminLicense == null){
                    redirectAttributes.addFlashAttribute("message", "This license is no longer available.");
                    return new ModelAndView("redirect:/" + userName + "/viewlicense");
                } else{
                    modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                    return new ModelAndView("edit_license");
                } 
            }else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                return new ModelAndView("redirect:/" + userName + "/viewlicense");
            } else {
                return new ModelAndView("redirect:/");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }
    @PostMapping(value = "/{userName}/editlicense")
    @ResponseBody
    public String editLicense(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer licenseId, @RequestParam String systemId,
            @RequestParam Boolean unlimitedcourse, @RequestParam Integer courselimit, @RequestParam Integer courseSetting, @RequestParam Integer smsTransaction, 
            @RequestParam Integer smsPromotion, @RequestParam Integer sendOtpOn, HttpServletRequest request) throws InvalidInformationException{ 
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                AdminLicense editLicense = adminLicenseService.getEditLicense(licenseId, systemId, unlimitedcourse, courselimit, courseSetting, smsTransaction, smsPromotion);
                return generateOTP(editLicense, modelMap, request, sendOtpOn);
            } else{
                return "Unauthorised access";
            }
        } else{
            return "please login again";
        }
    }
    /**
     * This method will return the complete information of license of a client in a string.
     * only admin can view license details of a client, client is not authorized for it.
     * 
     * @param userName
     * @param licenseId
     * @param redirectAttributes
     * @param request
     * @param modelMap
     * @return String
     */
    @PostMapping(value = "/{userName}/getsinglelicense")
    @ResponseBody
    public String rgetSingleLicenseLicense(@PathVariable String userName, @RequestParam Integer licenseId, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) throws ParseException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                AdminLicense adminLicense = adminLicenseDao.getAdminLicense(licenseId);
                if (adminLicense != null) {
                    modelMap.addAttribute("license", adminLicense);
                    return "success#{\"licenseId\":\"" + adminLicense.getAdminLicenseId() + "\","
                            + "\"userName\":\"" + adminLicense.getUserInfo().getUserName() + "\","
                            + "\"organization\":\"" + adminLicense.getUserInfo().getOrganization() + "\","
                            + "\"systemId\":\"" + adminLicense.getSystemInfoId() + "\"}";
                }
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserLicense userLicense = userLicenseDao.getUserLicense(licenseId);
                if (userLicense != null) {
                    String expiry = new SimpleDateFormat("dd-MM-yyyy").format(userLicense.getExpiryDate());
                    return "success#{\"userName\":\""+userLicense.getUserInfo().getUserName()+"\","
                            +"\"userid\":\""+userLicense.getUserInfo().getUserInfoId()+"\","
                            +"\"courseid\":\""+userLicense.getCourseId()+"\","
                            +"\"courseName\":\""+userLicense.getCourse().getCourseName()+"\","
                            +"\"licenseId\":\""+userLicense.getUserLicenseId()+"\","
                            +"\"licensekey\":\""+userLicense.getLicense() + "\","
                            +"\"systeminfoid\":\""+userLicense.getSystemInfoId()+ "\","
                            + "\"systemId\":\""+userLicense.getSystemInfo().getSystemId() + "\","
                            + "\"expirydate\":\""+expiry+"\"}";
//                    modelMap.addAttribute("users", userInfoDao.getUserInfoByCreatedBy(userInfo.getUserInfoId()));
//                    modelMap.addAttribute("courses", courseDao.getCourses(userInfo.getUserInfoId()));
//                    modelMap.addAttribute("watermarks", watermarkDao.getWatermarkList(userInfo.getUserInfoId()));
//                    modelMap.addAttribute("systems", systemInfoService.getSystemInfoListByUser(userLicense.getUserId()));
//                    modelMap.addAttribute("license", userLicense);
//                    return new ModelAndView("renewuserlicense");
                }
                return "Unauthorized Access";
            } else {
                return "Unauthorized Access";
            }
            return "This license is no longer available.";
        } else {
            return "Please login again";
        }
    }

    /**
     * If parameter values are fulfill the required conditions
     * then this method will generate an otp(random number), and renew the license for client.
     * 
     * @param userName
     * @param licenseId
     * @param lifetime
     * @param validity
     * @param startDate
     * @param endDate
     * @param webunlimited
     * @param desktopunlimited
     * @param weblicenseLimit
     * @param dekstoplicenseLimit
     * @param carryforward
     * @param sendOtpOn
     * @param modelMap
     * @param request
     * @return
     * @throws ParseException
     */
    @PostMapping(value = "/{userName}/renewlicense")
    @ResponseBody
    public String renewLicense(@PathVariable String userName, @RequestParam Integer licenseId, @RequestParam Boolean lifetime, @RequestParam Integer validity, @RequestParam String startDate,
            @RequestParam String endDate, @RequestParam Boolean webunlimited, @RequestParam Boolean desktopunlimited, @RequestParam Integer weblicenseLimit, @RequestParam Integer dekstoplicenseLimit,
            @RequestParam Integer carryforward, @RequestParam Integer sendOtpOn, ModelMap modelMap, HttpServletRequest request) throws ParseException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            modelMap.addAttribute("OTP", null);
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                System.out.println("modelMap.get(\"OTP\") : " + modelMap.get("OTP"));
                if (modelMap.get("OTP") != null) {
                    return "Unable to create multiple license at a time.";
                } else if (licenseId == null || licenseId == 0) {
                    return "Please select License first.";
                } else if(!lifetime){
                    if (startDate == null || startDate.trim().isEmpty()) {
                        return "Please select Issue Date.";
                    } else if (endDate == null || endDate.trim().isEmpty()) {
                        return "Please select Expiry Date.";
                    } else if (format.parse(endDate).compareTo(format.parse(startDate)) < 0) {
                        return "Exiry Date is less than Issue Date.";
                    } else if (!webunlimited ){
                        if(weblicenseLimit == null){
                            return "Please Enter web Limits";
                        } else{
                            return getRenewAdminLicense(licenseId, userInfo, lifetime,validity, startDate, 
                                endDate, webunlimited, desktopunlimited, weblicenseLimit, dekstoplicenseLimit, carryforward, 
                                    modelMap, request, sendOtpOn);
                        }
                    } else if (!desktopunlimited){
                        if(dekstoplicenseLimit == null){
                            return "Please Enter desktop Limits";
                        } else{
                            return getRenewAdminLicense(licenseId, userInfo, lifetime,validity, startDate, 
                                endDate, webunlimited, desktopunlimited, weblicenseLimit, dekstoplicenseLimit, carryforward, 
                                    modelMap, request, sendOtpOn);
                        }
                    } else {
                        return getRenewAdminLicense(licenseId, userInfo, lifetime,validity, startDate, 
                                endDate, webunlimited, desktopunlimited, weblicenseLimit, dekstoplicenseLimit, carryforward, 
                                    modelMap, request, sendOtpOn);
                    }
                } else{
                    if (!webunlimited ){
                        if(weblicenseLimit == null){
                            return "Please Enter Web Limit";
                        } else{
                            return getLifetimeRenewLicense(licenseId, userInfo, lifetime, startDate, 
                                webunlimited, desktopunlimited, weblicenseLimit, dekstoplicenseLimit, carryforward, 
                                modelMap, request, sendOtpOn);
                        }
                    } else if(!desktopunlimited){
                        if(dekstoplicenseLimit == null){
                            return "Please Enter Desktop Limit";
                        } else{
                            return getLifetimeRenewLicense(licenseId, userInfo, lifetime, startDate, 
                                webunlimited, desktopunlimited, weblicenseLimit, dekstoplicenseLimit, carryforward, 
                                modelMap, request, sendOtpOn);
                        }
                    } else {
                        return getLifetimeRenewLicense(licenseId, userInfo, lifetime, startDate, 
                                webunlimited, desktopunlimited, weblicenseLimit, dekstoplicenseLimit, carryforward, 
                                modelMap, request, sendOtpOn);
                    }
                }
            } else {
                return "Please login again.";
            }
        } else {
            return "Please login again.";
        }
    }
    
    private String getRenewAdminLicense(Integer licenseId, UserInfo userInfo, Boolean lifetime, Integer validity, String startDate, 
            String endDate, Boolean webunlimited, Boolean desktopunlimited, Integer weblicenseLimit, Integer dekstoplicenseLimit, Integer carryforward, 
            ModelMap modelMap, HttpServletRequest request, Integer sendOtpOn) throws ParseException{
        try {
            AdminLicense adminLicense = adminLicenseService.getAdminLicenseById(licenseId);
            startDate = new SimpleDateFormat("dd-MM-yyyy").format(adminLicense.getIssueDate());
            AdminLicense renewadminLicense = adminLicenseService.getRenewAdminLicense(userInfo.getUserInfoId(), licenseId, lifetime, validity,
                    format.parse(startDate), format.parse(endDate), webunlimited, desktopunlimited, weblicenseLimit, dekstoplicenseLimit, carryforward);
            System.out.println("admin license===="+renewadminLicense);
            return generateOTP(renewadminLicense, modelMap, request, sendOtpOn);
        } catch (InvalidInformationException ex) {
            Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
    }
    
    private String getLifetimeRenewLicense(Integer licenseId, UserInfo userInfo, Boolean lifetime, String startDate, 
            Boolean webunlimited, Boolean desktopunlimited, Integer weblicenseLimit, Integer dekstoplicenseLimit, Integer carryforward, 
            ModelMap modelMap, HttpServletRequest request, Integer sendOtpOn) throws ParseException{
        try {
            AdminLicense adminLicense = adminLicenseService.getAdminLicenseById(licenseId);
            startDate = new SimpleDateFormat("dd-MM-yyyy").format(adminLicense.getIssueDate());
            AdminLicense renewadminLicense = adminLicenseService.getRenewAdminLicense(userInfo.getUserInfoId(), licenseId, lifetime, 0, format.parse(startDate), null, webunlimited, desktopunlimited, weblicenseLimit, dekstoplicenseLimit, carryforward);
            System.out.println("admin license===="+renewadminLicense);
            return generateOTP(renewadminLicense, modelMap, request, sendOtpOn);
        } catch (InvalidInformationException ex) {
            Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
    }
    
    @PostMapping(value = "/{userName}/renewuserlicense")
    @ResponseBody
    public String renewUserLicense(@RequestParam Integer userId, @RequestParam Integer courseId, @RequestParam Boolean lifetime, @RequestParam Integer licenseId,
            @RequestParam Integer validity, @RequestParam String startDate, @RequestParam String endDate,
            @RequestParam Integer systemId, @RequestParam Integer sendOtpOn, @PathVariable String userName, ModelMap modelMap, HttpServletRequest request) throws ParseException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            modelMap.addAttribute("OTP", null);
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                System.out.println("modelMap.get(\"OTP\") : " + modelMap.get("OTP"));
                if (modelMap.get("OTP") != null) {
                    return "Unable to create multiple license at a time.";
                } else if (courseId == null || courseId == 0) {
                    return "Please select Course.";
                } else if (systemId == null || systemId == 0) {
                    return "Please select System.";
                } else if (licenseId == null || licenseId == 0) {
                    return "Please select License first.";
                } else if(!lifetime){
                    if (startDate == null || startDate.trim().isEmpty()) {
                        return "Please select Issue Date.";
                    } else if (endDate == null || endDate.trim().isEmpty()) {
                        return "Please select Expiry Date.";
                    } else if (format.parse(endDate).compareTo(format.parse(startDate)) < 0) {
                        return "Exiry Date is less than Issue Date.";
                    }else{
                        try{
                            UserLicense userLicense = userLicenseDao.getUserLicense(licenseId);
                            startDate = new SimpleDateFormat("dd-MM-yyyy").format(userLicense.getIssueDate());
                            AdminLicense adminLicense = userInfo.getAdminLicense();
                            UserLicense renewUserLicense = userLicenseService.renewUserLicense(userInfoDao.getUserInfo(userId), licenseId, courseDao.getCourse(userInfo.getUserInfoId(), courseId), systemInfoService.getSystemInfo(userId, systemId), validity, format.parse(startDate), format.parse(endDate), userInfo.getUserInfoId(), adminLicense);
                            return generateUserOTP(renewUserLicense, modelMap, request, sendOtpOn);
                        } catch (InvalidInformationException ex) {
                            Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                            return ex.getMessage();
                        }
                    }
                } else {
                    try {
                        validity = 0;
                        UserLicense userLicense = userLicenseDao.getUserLicense(licenseId);
                        startDate = new SimpleDateFormat("dd-MM-yyyy").format(userLicense.getIssueDate());
                        Date expirydate = null;
                        AdminLicense adminLicense = userInfo.getAdminLicense();
                        UserLicense renewUserLicense = userLicenseService.renewUserLicense(userInfoDao.getUserInfo(userId), licenseId, courseDao.getCourse(userInfo.getUserInfoId(), courseId), systemInfoService.getSystemInfo(userId, systemId), validity, format.parse(startDate), expirydate, userInfo.getUserInfoId(), adminLicense);
                        return generateUserOTP(renewUserLicense, modelMap, request, sendOtpOn);
                    } catch (InvalidInformationException ex) {
                        Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                        return ex.getMessage();
                    }
                }
            } else {
                return "Please login again.";
            }
        } else {
            return "Please login again.";
        }
    }
    
    /**
     * This method will show the report of user. 
     * it will return on report page with the list of notifications and license expiry alerts.
     * 
     * @param userName
     * @param modelMap
     * @return on page report
     */
    @GetMapping(value = "/{userName}/report")
    public ModelAndView getReport(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("licenseexpirealert", adminLicenseService.getLicenseExpireAlert());
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                return new ModelAndView("report");
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                System.out.println("EXPIRY"+userLicenseService.getLicenseExpireAlert());
                modelMap.addAttribute("licenseexpirealert", userLicenseService.getLicenseExpireAlert());
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                return new ModelAndView("report");
            } else {
                return new ModelAndView("redirect:/");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This url is called if the user request to get the license information.
     * If the user is admin then the method will take the user to the getlicenseinfo page,
     * And if the user is client then the method will take the user to the  getuserlicenseinfo page.
     * 
     * @param userName
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/getlicenseinfo")
    public ModelAndView getLicenseInfo(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                return new ModelAndView("getlicenseinfo");
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                return new ModelAndView("getuserlicenseinfo");
            } else {
                return new ModelAndView("redirect:/");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will return the information of license of requested license Id.
     * 
     * @param licenseId
     * @param userName
     * @param modelMap
     * @return String
     */
    @PostMapping(value = "/{userName}/getlicenseinfo")
    @ResponseBody
    public String getLicenseInfo(@RequestParam String licenseId,
            @PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                try {
                    System.out.println(licenseId);
                    System.out.println("SecurityKey=="+new LicenseInformation(licenseId.trim()).getLicenseInfo());
                    return new LicenseInformation(licenseId.trim()).getLicenseInfo();
                } catch (InvalidInformationException ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                    return ex.getMessage();
                }
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                try {
                    return new UserLicenseInformation(licenseId.trim()).getLicenseInfo();
                } catch (InvalidInformationException ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                    return ex.getMessage();
                }
            } else {
                return "Please login again.";
            }
        } else {
            return "Please login again.";
        }
    }

    /**
     * This method will simply return on getsysteminfo page.
     * 
     * @param userName
     * @param modelMap
     * @return on page getsysteminfo
     */
    @GetMapping(value = "/{userName}/getsysteminfo")
    public ModelAndView getSystemInfo(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            return new ModelAndView("getsysteminfo");
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will return the information of specified system id.
     * 
     * @param systemId
     * @param userName
     * @param modelMap
     * @return String
     */
    @PostMapping(value = "/{userName}/getsysteminfo")
    @ResponseBody
    public String getSystemInfo(@RequestParam String systemId,
            @PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                try {
                    return new SystemInformation(systemId.trim()).getSystemInfo();
                } catch (InvalidInformationException ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                    return ex.getMessage();
                }
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                try {
                    return new UserSystemInformation(systemId.trim()).getSystemInfo();
                } catch (InvalidInformationException ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                    return ex.getMessage();
                }
            } else {
                return "Please login again.";
            }
        } else {
            return "Please login again.";
        }
    }

    /**
     * This method will take the user on page viewlicensehistory with a list of notifications related to user.
     * 
     * @param userName
     * @param modelMap
     * @return on page viewlicensehistory
     */
    @GetMapping(value = "/{userName}/licensehistory")
    public ModelAndView viewLicenseHistory(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("licensehistories", notificationDao.getNotificationForAdmin());
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                modelMap.addAttribute("licensehistories", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
            } else {
                return new ModelAndView("redirect:/");
            }
            return new ModelAndView("viewlicensehistory");
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * 
     * @param userName
     * @param typs
     * @param type
     * @param modelMap
     * @return 
     */
    @PostMapping(value = "/{userName}/getnotificationbytype")
    @ResponseBody
    public String getNotificationByType(@PathVariable String userName ,@RequestParam(value = "typs[]") Integer[] typs, ModelMap modelMap){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if(UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())){
                String notify = "";
                List<Notification> notifications = new ArrayList<>();
                for (Integer type : typs) {
                    System.out.println("type===="+type);
                List<Notification> notificationByType = notificationDao.getNotificationsForAdminByType(type);
                if(type == 2){
                    List<Notification> notificationByType1 = notificationDao.getNotificationsForAdminByType(3);
                    List<Notification> notificationByType2 = notificationDao.getNotificationsForAdminByType(4);
                        for (Notification notification : notificationByType1) {
                            notificationByType.add(notification);
                        }
                        for (Notification notification : notificationByType2) {
                            notificationByType.add(notification);
                        }
                }   
                    for (Notification notification : notificationByType) {
                        System.out.println(notification);
                        notifications.add(notification);
                    }
                }
                for (Notification notification : notifications) {
                    notification.getNotificationMessage();
                    notification.getCreateOn();
                    notify += ",{\"message\":\""+ notification.getNotificationMessage()+"\","
                               + "\"createOn\":\""+notification.getCreateOn()+"\"}";
                }
                if(notify.equals("")){
                    return "No notifications Available.";
                }else{
                    String wholeString = "success#[" + notify.substring(1) + "]";
                    return wholeString;
                }
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                String notify = "";
                List<Notification> notifications = new ArrayList<>();
                for (Integer type : typs) {
                    System.out.println("type===="+type);
                List<Notification> notificationByType = notificationDao.getNotificationByType(userInfo.getUserInfoId(), type);
                if(type == 2){
                    List<Notification> notificationByType1 = notificationDao.getNotificationByType(userInfo.getUserInfoId(), 3);
                    List<Notification> notificationByType2 = notificationDao.getNotificationByType(userInfo.getUserInfoId(), 4);
                        for (Notification notification : notificationByType1) {
                            notificationByType.add(notification);
                        }
                        for (Notification notification : notificationByType2) {
                            notificationByType.add(notification);
                        }
                }   
                    for (Notification notification : notificationByType) {
                        System.out.println(notification);
                         notifications.add(notification);
                    }
                }
                for (Notification notification : notifications) {
                    notification.getNotificationMessage();
                    notification.getCreateOn();
                    notify += ",{\"message\":\""+ notification.getNotificationMessage()+"\","
                               + "\"createOn\":\""+notification.getCreateOn()+"\"}";
                }
                if(notify.equals("")){
                    return "No notifications Available.";
                }else{
                    String wholeString = "success#[" + notify.substring(1) + "]";
                    return wholeString;
                }
            } else {
                return null; 
            }
        }
        else{
            return null;
        }
    }
    @GetMapping(value = "/{userName}/getallnotification")
    @ResponseBody
    public String getAllNotification(@PathVariable String userName , ModelMap modelMap){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if(UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())){
                String notify = "";
                List<Notification> allnotification = notificationDao.getNotificationForAdmin();
                for (Notification notification : allnotification) {
                    notification.getNotificationMessage();
                    notification.getCreateOn();
                    notify += ",{\"message\":\""+ notification.getNotificationMessage()+"\","
                               + "\"createOn\":\""+notification.getCreateOn()+"\"}";
                }
                if(notify.equals("")){
                    return "No notifications Available.";
                }else{
                    String wholeString = "success#[" + notify.substring(1) + "]";
                    return wholeString;
                }
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                String notify = "";
                List<Notification> allnotification = notificationDao.getNotificationForClient(userInfo.getUserInfoId());
                for (Notification notification : allnotification) {
                    notification.getNotificationMessage();
                    notification.getCreateOn();
                    notify += ",{\"message\":\""+ notification.getNotificationMessage()+"\","
                               + "\"createOn\":\""+notification.getCreateOn()+"\"}";
                }
                if(notify.equals("")){
                    return "No notifications Available.";
                }else{
                    String wholeString = "success#[" + notify.substring(1) + "]";
                    return wholeString;
                }
            } else {
                return null;
            }  
        }
        else{
            return null;
        }
    }
    
    /**
     * This method will return on page viewlicenseexpirealert 
     * with a list of licenses whose expiry date is in coming 5 days.
     * 
     * @param userName
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/licenseexpirealert")
    public ModelAndView viewLicenseExpireAlert(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("licenseexpirealert", adminLicenseService.getLicenseExpireAlert());
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                return new ModelAndView("viewlicenseexpirealert");
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("licenseexpirealert", userLicenseService.getLicenseExpireAlert());
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                return new ModelAndView("viewlicenseexpirealert");
            } else {
                return new ModelAndView("redirect:/");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will return on page showuseroncourse,
     * with the list of users who assigned the course which is present at specified id.
     * 
     * @param userName
     * @param id
     * @param redirectAttributes
     * @param request
     * @param modelMap
     * @return on page showuseroncourse
     */
    @GetMapping(value = "/{userName}/showuser/{id}")
    public ModelAndView showUser(@PathVariable String userName, @PathVariable Integer id, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                Course course = courseDao.getCourse(userInfo.getUserInfoId(), id);
                if (course != null) {
                    Set<UserInfo> userInfos = userLicenseDao.getAssignUserListOnCourse(id);
                    if (userInfos != null) {
                        modelMap.addAttribute("users", userInfos);
                        modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                        return new ModelAndView("showuseroncourse");
                    } else {
                        redirectAttributes.addFlashAttribute("message", "No user is assign to this course.");
                    }
                } else {
                    redirectAttributes.addFlashAttribute("message", "This course is unavailable.");
                }
                return new ModelAndView("redirect:/" + userName + "/viewcourse");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will take the user on showsystemoncourse page,
     * with the list of systems who is assigned on a course present at specified id.
     * 
     * @param userName
     * @param id
     * @param redirectAttributes
     * @param request
     * @param modelMap
     * @return on page showsystemoncourse
     */
    @GetMapping(value = "/{userName}/showsystem/{id}")
    public ModelAndView showSystem(@PathVariable String userName, @PathVariable Integer id, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                Course course = courseDao.getCourse(userInfo.getUserInfoId(), id);
                if (course != null) {
                    Set<SystemInfo> systemInfos = userLicenseDao.getAssignSystemListOnCourse(id);
                    if (systemInfos != null) {
                        modelMap.addAttribute("systems", systemInfos);
                        modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                        return new ModelAndView("showsystemoncourse");
                    } else {
                        redirectAttributes.addFlashAttribute("message", "No System is assign to this course.");
                    }
                } else {
                    redirectAttributes.addFlashAttribute("message", "This course is unavailable.");
                }
                return new ModelAndView("redirect:/" + userName + "/viewcourse");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will return on page getselflicenseinfo,
     * with the information of license of the user itself.
     * 
     * @param userName
     * @param redirectAttributes
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/getselflicenseinfo")
    public ModelAndView getSelfLicenseInfo(@PathVariable String userName, RedirectAttributes redirectAttributes, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                try {
                    modelMap.addAttribute("license", new LicenseInformation(adminLicenseDao.getAdminLicenseByClient(userInfo.getUserInfoId()).getLicense()));
                    return new ModelAndView("getselflicenseinfo");
                } catch (InvalidInformationException ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                    redirectAttributes.addFlashAttribute("message", "This course is unavailable.");
                    return new ModelAndView("redirect:/" + userName + "/dashboard");
                }
            } else if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            } else {
                return new ModelAndView("redirect:/");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will generate an string containing OTP(random number).
     * 
     * @param adminLicense
     * @param modelMap
     * @return String
     */
    private String generateOTP(Object adminLicense, ModelMap modelMap, HttpServletRequest request, Integer sendOtpOn) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        String otp = String.format("%06d", (int) (Math.random() * 1000000));
        double random = Math.random();
        Object[] objects = {adminLicense, otp, random};
        System.out.println("obejct : " + Arrays.toString(objects));
        System.out.println("OTP : " + otp);
        System.out.println("RANDOM : " + random);
        
        if(sendOtpOn == 1){
            modelMap.addAttribute("OTP", objects);
            mailController.sendEmail("OTP", otp, userInfo.getEmailId(), request, userInfo);
            return "success#" + random;
        }else{
            modelMap.addAttribute("OTP", objects);
            System.out.println("USERINFO=="+userInfo);
            System.out.println("userInfo.getMobileNumber()"+userInfo.getMobileNumber());
            mailController.sendMessage(userInfo.getMobileNumber(), "OTP: "+otp);
            return "success#" + random;
        }
    }
    private String generateUserOTP(Object userLicense, ModelMap modelMap, HttpServletRequest request, Integer sendOtpOn) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        String otp = String.format("%06d", (int) (Math.random() * 1000000));
        double random = Math.random();
        Object[] objects = {userLicense, otp, random};
        System.out.println("obejct : " + Arrays.toString(objects));
        System.out.println("OTP : " + otp);
        System.out.println("RANDOM : " + random);
        if(sendOtpOn == 1){
            Boolean sendMail = mailController.sendMail("OTP", otp, userInfo.getEmailId(), request, userInfo.getUserInfoId());
            if(sendMail){
                modelMap.addAttribute("OTP", objects);
                return "success#" + random;
            } else{
                return "OTP can not send Due to incomplete mail settings";
            }
        }else{
            AdminLicense adminLicenseByClient = adminLicenseService.getAdminLicenseByClient(userInfo.getUserInfoId());
            Integer transactionalCounter = adminLicenseByClient.getTransactionalCounter();
            Integer smsTransactional = adminLicenseByClient.getSmsTransactional();
            if(transactionalCounter < smsTransactional){
                modelMap.addAttribute("OTP", objects);
                mailController.sendMessage(userInfo.getMobileNumber(), "OTP: "+otp);
                adminLicenseByClient.setTransactionalCounter(transactionalCounter+1);
                adminLicenseDao.updateAdminLicense(adminLicenseByClient);
                return "success#" + random;
            } else{
                return "Can not send Otp on mobile, Your limit is over, You can select Email option.";
            }
            
        } 
    }

    /**
     * This method will verify the OTP and return an appropriate message.
     * 
     * @param userName
     * @param otp
     * @param random
     * @param request
     * @param sessionStatus
     * @param httpSession
     * @param httpServletRequest
     * @param modelMap
     * @return 
     */
    @PostMapping(value = "/{userName}/varifyotp")
    @ResponseBody
    public String verifyOTP(@PathVariable String userName, @RequestParam String otp, @RequestParam Double random,
            WebRequest request, SessionStatus sessionStatus, HttpSession httpSession,
            HttpServletRequest httpServletRequest, ModelMap modelMap) throws NoSuchAlgorithmException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                Object[] objects = (Object[]) modelMap.get("OTP");
                if (objects != null) {
                    if (((double) objects[2]) == random && String.valueOf(objects[1]).equals(otp)) {
                        AdminLicense license = (AdminLicense)objects[0];
                        if(license.getAdminLicenseId() == null){
                            String saveAdminLicense = adminLicenseService.saveAdminLicense((AdminLicense) objects[0], userInfo.getUserInfoId());
                            modelMap.addAttribute("OTP", null);
                            modelMap.addAttribute("user", userInfo);
                            AdminLicense al = (AdminLicense)objects[0];
                            RandomPasswordGenerator generator = new RandomPasswordGenerator();
                            String password = String.copyValueOf(generator.generatePswd(5, 10, 2, 2, 1));
                            UserInfo userInfo1 = userInfoDao.getUserInfo(al.getUserId());
                            userInfo1.setPassword(sha256.getSHA256(password));
                            userInfoDao.updateUser(userInfo1);
                            System.out.println(al);
                            System.out.println("password=="+password);
                            mailController.sendEmailToClient(al, userInfo1, "Account Created", password, httpServletRequest);
                            return saveAdminLicense+"#"+al.getAdminLicenseId();
                        } else{
                            String saveAdminLicense = adminLicenseService.saveAdminLicense((AdminLicense) objects[0], userInfo.getUserInfoId());
                            AdminLicense al = (AdminLicense)objects[0];
                            UserInfo userInfo1 = userInfoDao.getUserInfo(al.getUserId());
                            System.out.println("License Renew OTP verification...");
                            mailController.sendRenewMailToClient(al, userInfo1, "Renew License", httpServletRequest);
                            return saveAdminLicense+"#"+al.getAdminLicenseId();
                        }
                    }
                }
                return "Wrong OTP";
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                Object[] objects = (Object[]) modelMap.get("OTP");
                if (objects != null) {
                    if (((double) objects[2]) == random && String.valueOf(objects[1]).equals(otp)) {
                        UserLicense userLicense = (UserLicense) objects[0];
                        if(userLicense.getUserLicenseId() == null){
                            String saveUserLicense = userLicenseService.saveUserLicense((UserLicense) objects[0], userInfo.getUserInfoId());
                            modelMap.addAttribute("OTP", null);
                            modelMap.addAttribute("user", userInfo);
                            return saveUserLicense;
                        } else{
                            String saveUserLicense = userLicenseService.saveUserLicense((UserLicense) objects[0], userInfo.getUserInfoId());
                            modelMap.addAttribute("OTP", null);
                            modelMap.addAttribute("user", userInfo);
                            Boolean sendEmail = mailController.sendEmailForLicense("License Renew", userLicense, httpServletRequest, userInfo);
                            if(sendEmail){
                                System.out.println("mail send.....");
                            } else{
                                System.out.println("mail not sent due to incomplete mail settings.....");
                            }
                            return saveUserLicense;
                        }
                        
                    }
                }
                return "Wrong OTP";
            } else {
                return "Please login again.";
            }
        } else {
            return "Please login again.";
        }
    }

    @PostMapping(value = "/{userName}/varifyeditotp")
    @ResponseBody
    public String verifyEditOTP(@PathVariable String userName, @RequestParam String otp, @RequestParam Double random,
            WebRequest request, SessionStatus sessionStatus, HttpSession httpSession,
            HttpServletRequest httpServletRequest, ModelMap modelMap) throws NoSuchAlgorithmException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                Object[] objects = (Object[]) modelMap.get("OTP");
                if (objects != null) {
                    if (((double) objects[2]) == random && String.valueOf(objects[1]).equals(otp)) {
                        String editAdminLicense = adminLicenseService.editAdminLicense((AdminLicense) objects[0], userInfo.getUserInfoId());
                        AdminLicense al = (AdminLicense)objects[0];
                        UserInfo userInfo1 = userInfoDao.getUserInfo(al.getUserId());
                        System.out.println("License Edit OTP verification...");
                        mailController.sendEditMailToClient(al, userInfo1, "Edit License", httpServletRequest);
                        return editAdminLicense+"#"+al.getAdminLicenseId();
                    }
                }
                return "Wrong OTP";
            } else {
                return "Unauthorised Access";
            }
        } else {
            return "Please login again.";
        }
    }
    /**
     * This method will regenerate and resend a different otp number.
     * 
     * @param userName
     * @param random
     * @param sendOtpOn
     * @param modelMap
     * @param request
     * @return 
     */
    @PostMapping(value = "/{userName}/resendotp")
    @ResponseBody
    public String resendOTP(@PathVariable String userName, @RequestParam Double random, @RequestParam Integer sendOtpOn, ModelMap modelMap, HttpServletRequest request) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                System.out.println("random="+random);
                System.out.println("sendOtpOn="+sendOtpOn);
                Object[] objects = (Object[]) modelMap.get("OTP");
                System.out.println(objects);
                if (objects != null) {
                    if (((double) objects[2]) == random) {
                        return generateUserOTP(objects[0], modelMap, request, sendOtpOn);
                    }
                }
                return "Invalid entry";
            } else if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                Object[] objects = (Object[]) modelMap.get("OTP");
                if (objects != null) {
                    if (((double) objects[2]) == random) {
                        return generateOTP(objects[0], modelMap, request, sendOtpOn);
                    }
                }
                return "Invalid entry";
            } else {
                return "Please login again.";
            }
        } else {
            return "Please login again.";
        }
    }
    @GetMapping(value="/{userName}/dates")
    @ResponseBody
    public String getDates(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer validity) throws ParseException{
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            Calendar cal = Calendar.getInstance();
            String today = new SimpleDateFormat("dd-MM-yyyy").format(cal.getTime());
            cal.add(Calendar.YEAR, validity);
            cal.add(Calendar.DAY_OF_WEEK, -1);
            String endDate = new SimpleDateFormat("dd-MM-yyyy").format(cal.getTime());
            System.out.println("date=============="+endDate); 
            System.out.println(cal.getTime());
            return "success#"+today+"*"+endDate;
        }else{
            return "login again";
        }
    }
    
    @GetMapping(value = "/{userName}/getdate")
    @ResponseBody
    public String getDate(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer validity, @RequestParam String startdate) throws ParseException{
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(format.parse(startdate));
            cal.add(Calendar.YEAR, validity);
            cal.add(Calendar.DAY_OF_WEEK, -1);
            String endDate = new SimpleDateFormat("dd-MM-yyyy").format(cal.getTime());
            System.out.println("date=============="+endDate); 
            System.out.println(cal.getTime());
            return "success#"+endDate;
        }else{
            return "login again";
        }
    }
    
//    @GetMapping(value = "/{userName}/showuserinfo/{userId}")
//    public String showUserInfo(@PathVariable String userName, ModelMap modelMap,@PathVariable Integer userId){
//        UserInfo userInfo = (UserInfo) modelMap.get("user");
//        if (userInfo != null && userInfo.getUserName().equals(userName)) {
//            System.out.println(userId);
//            UserInfo userInfo1 = userInfoDao.getUserInfo(userId);
//            System.out.println(userInfo1);
//            Integer value = null;
//            Map<Integer, Integer> showUserIdThroughCount = userInfoDao.showUserIdThroughCount();
//            System.out.println("MAP=="+showUserIdThroughCount);
//            for (Map.Entry<Integer, Integer> entry : showUserIdThroughCount.entrySet()) {
//                System.out.println("keyyyy==="+entry.getKey());
//                System.out.println("valueee==="+entry.getValue());
//                if(entry.getKey()==userId){
//                    value = entry.getValue();
//                }
//            }
//            System.out.println(value);
//            JSONObject jSONObject = new JSONObject();
//            jSONObject.put("userName", userInfo1.getUserName());
//            jSONObject.put("emailId", userInfo1.getEmailId());
//            jSONObject.put("countryCodeId", userInfo1.getCountryCodeId());
//            jSONObject.put("mobileNumber", userInfo1.getMobileNumber());
//            jSONObject.put("userInfoId", value);
//            System.out.println(jSONObject);
//            return jSONObject.toString();
//            
//        }else{
//            return "login again";
//        }
//    }
//    
    @PostMapping(value = "/{userName}/getdropdownuserinfo")
    @ResponseBody
    public String getDropdownUserInfo(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer selectBy){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                String dropdownuser = "";
                List<UserInfo> userInfoByCreater = userInfoDao.getUserInfoByCreatedBy(userInfo.getUserInfoId());
                if(userInfoByCreater.isEmpty()){
                    return "No Users Available";
                }else{
                    if(selectBy == 1){ 
                        for (UserInfo userInfo1 : userInfoByCreater) {
                            dropdownuser += ",{\"userId\":\"" + userInfo1.getUserInfoId() + "\","
                                + "\"userName\":\"" + userInfo1.getUserName() + "\"}";
                        }
                    }
                    if(selectBy == 2){
                        for (UserInfo userInfo1 : userInfoByCreater) {
                            dropdownuser += ",{\"userId\":\"" + userInfo1.getUserInfoId() + "\","
                                + "\"emailId\":\"" + userInfo1.getEmailId()+ "\"}";
                        }
                    }
                    if(selectBy == 3){
                        for (UserInfo userInfo1 : userInfoByCreater) {
                            dropdownuser += ",{\"userId\":\"" + userInfo1.getUserInfoId() + "\","
                                + "\"mobileNumber\":\"" + userInfo1.getMobileNumber()+ "\"}";
                        }
                    }
                    if(selectBy == 4){
                        Map<Integer, Integer> map = userInfoDao.showUserIdThroughCount();
                        for (UserInfo userInfo1 : userInfoByCreater) {
                            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                                if(userInfo1.getUserInfoId() == entry.getKey()){
                                    String displayId = "AD"+(1000+entry.getValue());
                                    dropdownuser += ",{\"userId\":\"" + userInfo1.getUserInfoId() + "\","
                                        + "\"displayId\":\"" + displayId+ "\"}";
                                }
                            }
                        }
                    }
                    String userinfo = "success#[" + dropdownuser.substring(1) + "]";
                    return userinfo;
                }
            } else if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                String dropdownuser = "";
                List<UserInfo> userInfoByCreater = userInfoDao.getUserInfoByCreatedBy(userInfo.getUserInfoId());
                if(selectBy == 1){ 
                    for (UserInfo userInfo1 : userInfoByCreater) {
                        dropdownuser += ",{\"userId\":\"" + userInfo1.getUserInfoId() + "\","
                            + "\"userName\":\"" + userInfo1.getUserName() + "\"}";
                    }
                }
                if(selectBy == 2){
                    for (UserInfo userInfo1 : userInfoByCreater) {
                        dropdownuser += ",{\"userId\":\"" + userInfo1.getUserInfoId() + "\","
                            + "\"emailId\":\"" + userInfo1.getEmailId()+ "\"}";
                    }
                }
                if(selectBy == 3){
                    for (UserInfo userInfo1 : userInfoByCreater) {
                        dropdownuser += ",{\"userId\":\"" + userInfo1.getUserInfoId() + "\","
                            + "\"mobileNumber\":\"" + userInfo1.getMobileNumber()+ "\"}";
                    }
                }
                String userinfo = "success#[" + dropdownuser.substring(1) + "]";
                return userinfo;
            } else {
                return "Please login again.";
            }
        } else {
            return "Please login again.";
        }
    }
    
    @PostMapping(value ="/{userName}/getcoursedetails")
    @ResponseBody
    public String getCourseDetails(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer courseid){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                Course course = courseDao.getCourse(userInfo.getUserInfoId(), courseid);
                String courseId = course.getCourseId();
                String courseDescription = course.getCourseDescription();
                Integer totalUsedCourse = userLicenseService.getTotalUsedCourseCount(courseid);
                return "success#{\"courseId\":\""+courseId+"\","
                            +"\"courseDescription\":\""+courseDescription+"\","
                            +"\"totalUsedCourse\":\""+totalUsedCourse+ "\"}";
                          
            } else {
                return "Please login again.";
            }
        } else {
            return "Please login again.";
        }
    }
   
    @PostMapping(value = "/{userName}/getuserinfofordisplay")
    @ResponseBody
    public String getUserInfoForDisplay(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer userId){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if(UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())){
                UserInfo userInfo1 = userInfoDao.getUserInfo(userId);
                Map<Integer, Integer> UserId = userInfoDao.showClientIdThroughCount();
                Integer value = 0;
                for (Map.Entry<Integer, Integer> entry : UserId.entrySet()) {
                    if(entry.getKey() == userId){
                        value = entry.getValue();
                    }   
                }
                value = value + 1000;
                String userid = "SA" + value;
                String Country = userInfo1.getCountryCode().getCountry();
                return "success#{\"userId\":\""+userid+"\","
                            +"\"Name\":\""+userInfo1.getUserName()+"\","
                            +"\"Organization\":\""+userInfo1.getOrganization()+"\","
                            +"\"Email\":\""+userInfo1.getEmailId()+ "\","
                            + "\"Country\":\""+Country+ "\","
                            + "\"Mobile\":\""+userInfo1.getMobileNumber()+ "\"}";
                
            }else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserInfo userInfo1 = userInfoDao.getUserInfo(userId);
                Map<Integer, Integer> UserId = userInfoDao.showUserIdThroughCount();
                Integer value = 0;
                for (Map.Entry<Integer, Integer> entry : UserId.entrySet()) {
                    if(entry.getKey() == userId){
                        value = entry.getValue();
                    }   
                }
                value = value + 1000;
                String userid = "AD" + value;
                String Country = userInfo1.getCountryCode().getCountry();
                return "success#{\"userId\":\""+userid+"\","
                            +"\"Name\":\""+userInfo1.getUserName()+"\","
                            +"\"Email\":\""+userInfo1.getEmailId()+ "\","
                        + "\"Country\":\""+Country+ "\","
                        + "\"Mobile\":\""+userInfo1.getMobileNumber()+ "\"}";
                          
            } else {
                return "Unauthorised Access";
            }
        } else {
            return "Please login again.";
        }
    }
    
    @PostMapping(value = "/{userName}/sendmailtouserforlicense")
    @ResponseBody
    public String sendMailToUserForLicense(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer licenseId, HttpServletRequest request){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserLicense userLicense = userLicenseDao.getUserLicense(licenseId);
                System.out.println(userLicense.getLicense());
                String validity;
                if(userLicense.getExpiryDate() == null){
                    validity = "Unlimited";
                } else{
                    validity = new SimpleDateFormat("dd-MM-yyyy").format(userLicense.getIssueDate()) +" TO "+ new SimpleDateFormat("dd-MM-yyyy").format(userLicense.getExpiryDate());
                }
                
                Boolean sendEmail = mailController.sendEmailForLicense("License Information", userLicense, request, userInfo);
                if(sendEmail){
                    return "success";
                }
                else{
                    return "Your Mail Settings are Incomplete";
                }          
            } else {
                return "Fail";
            }
        } else {
            return "Please login again.";
        }
    }
    
    @PostMapping(value = "/{userName}/sendmessagetouserforlicense")
    @ResponseBody
    public String sendMessageToUserForLicense(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer licenseId, HttpServletRequest request){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserLicense userLicense = userLicenseDao.getUserLicense(licenseId);
                System.out.println(userLicense.getLicense());
                mailController.sendMessage(userLicense.getUserInfo().getMobileNumber(), userLicense.getLicense().substring(0, 140)+"...");
                return "success";       
            } else {
                return "Unauthorised Access";
            }
        } else {
            return "Please login again.";
        }
    }
    
    @GetMapping(value = "/{userName}/downloadlicensekey")
    public void downloadLicenseKey(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer licenseId, HttpServletRequest request, HttpServletResponse response){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                AdminLicense adminLicense = adminLicenseDao.getAdminLicense(licenseId);
                String validity;
                if(adminLicense.getExpiryDate() == null){
                    validity = "Lifetime";
                } else{
                    validity = new SimpleDateFormat("dd-MM-yyyy").format(adminLicense.getIssueDate()) +" TO "+ new SimpleDateFormat("dd-MM-yyyy").format(adminLicense.getExpiryDate());
                }
                try{
                    ServletContext servletContext = request.getSession().getServletContext();
                    String appPath = servletContext.getRealPath("/");
                    System.out.println("appPath = " + appPath);
                    String fullPath = appPath.replace("\\", "/")+adminLicense.getAdminLicenseId()+".txt";    
                    FileOutputStream fis = new FileOutputStream(fullPath);
                   
                    fis.write(("Client Desktop License : "+adminLicense.getDesktopLicense()).getBytes());
                    fis.close();
                    
                    FileInputStream fins = new FileInputStream(fullPath);
                    response.getOutputStream();
                    // set content attributes for the response
                    response.setContentType("text/plain");
                    // set headers for the response
                    String headerKey = "Content-Disposition";
                    String headerValue = String.format("attachment; filename=\"%s\"",
                          validity );
                      response.setHeader(headerKey, headerValue);
                    IOUtils.copy(fins, response.getOutputStream());
                      
                }catch(Exception e){
                    System.out.println(e);
                  
                }
            }else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserLicense userLicense = userLicenseDao.getUserLicense(licenseId);
                String validity;
                if(userLicense.getExpiryDate() == null){
                    validity = "Unlimited";
                } else{
                    validity = new SimpleDateFormat("dd-MM-yyyy").format(userLicense.getIssueDate()) +" TO "+ new SimpleDateFormat("dd-MM-yyyy").format(userLicense.getExpiryDate());
                }
                
                try{
                    ServletContext servletContext = request.getSession().getServletContext();
                    String appPath = servletContext.getRealPath("/");
                    System.out.println("appPath = " + appPath);
                    String fullPath = appPath.replace("\\", "/")+userLicense.getUserLicenseId()+".txt";    
                    FileOutputStream fis = new FileOutputStream(fullPath);
                   
                    fis.write(("User License : "+userLicense.getLicense()).getBytes());
                    fis.close();
                    
                    FileInputStream fins = new FileInputStream(fullPath);
                    response.getOutputStream();
                    // set content attributes for the response
                    response.setContentType("text/plain");
                    // set headers for the response
                    String headerKey = "Content-Disposition";
                    String headerValue = String.format("attachment; filename=\"%s\"",
                          validity );
                      response.setHeader(headerKey, headerValue);
                    IOUtils.copy(fins, response.getOutputStream());
                      
                }catch(Exception e){
                    System.out.println(e);
                  
                }
            } else {
                
            }
        } else {
        }
    }
    
//    @PostMapping(value = "/{userName}/searchusers")
//    @ResponseBody
//    public String searchByUserName(@PathVariable String userName, ModelMap modelMap, @RequestParam String match, @RequestParam Integer type){
//        UserInfo userInfo = (UserInfo) modelMap.get("user");
//        if (userInfo != null && userInfo.getUserName().equals(userName)) {
//            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
//                if(type == 1){
//                    String searchresults = userInfoService.searchUserInfoByUserName(userInfo.getUserInfoId(), match);
//                    return searchresults;      
//                } else if(type == 2){
//                    String searchresults = userInfoService.searchUserInfoByEmailId(userInfo.getUserInfoId(), match);
//                    return searchresults;
//                } else if(type == 3){
//                    String searchresults = userInfoService.searchUserInfoByMobilenumber(userInfo.getUserInfoId(), match);
//                    return searchresults;
//                } else{
//                    return "Invalid choice";
//                }
//            } else if(UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())){
//                
//                return "Fail";
//            } else{
//                return "Unauthorised Access";
//            }
//        } else {
//            return "Please login again.";
//        }
//    }
    
    @PostMapping(value = "/{userName}/getorganizationofclient")
    @ResponseBody
    public String getOrganiztion(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer userId){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if(UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())){
                UserInfo userInfo1 = userInfoDao.getUserInfo(userId);
                return "success#"+userInfo1.getOrganization();
            }else{
                return "Unauthorised Access";
            }
        }else{
             return "Please Login Again";
        }
    }
    
    @PostMapping(value = "/{userName}/checksystemid")
    @ResponseBody
    public String checkSystemID(@PathVariable String userName, ModelMap modelMap, @RequestParam String systemId) throws InvalidInformationException{
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if(UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())){
                try{
                    LicenseInformation licenseInformation = new LicenseInformation();
                    return licenseInformation.checkSystemId(systemId);
                } catch (InvalidInformationException ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                    return ex.getMessage();
                }
            }else{
                return "Unauthorised Access";
            }
        }else{
             return "Please Login Again";
        }
    }
    
    @GetMapping(value = "/downloadlicensethroughmail")
    public void downloadLicenseKeyThroughMail(@RequestParam String filepath, HttpServletRequest request, HttpServletResponse response){
        try{
            ServletContext servletContext = request.getSession().getServletContext();
            String appPath = servletContext.getRealPath("/");
            String fullPath = appPath.replace("\\", "/")+filepath;    
            FileInputStream fins = new FileInputStream(fullPath);
            response.getOutputStream();
            // set content attributes for the response
            response.setContentType("text/plain");
            // set headers for the response
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"",
                "License Key" );
            response.setHeader(headerKey, headerValue);
            IOUtils.copy(fins, response.getOutputStream());
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}

