/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.domain;

import java.io.Serializable;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author CoreTechiesLP
 */
@Entity
@Table(name = "userpersonalinfo")
@Access(AccessType.FIELD)
public class UserPersonalInfo implements Serializable {
    
    @Id
    @GeneratedValue(generator = "userpersonalinfo")
    @GenericGenerator(name = "userpersonalinfo", strategy = "increment")
    private Integer personalinfoId;
    
    @Column
    private Integer userInfoId;
    
    @Column
    private String firstName;
    
    @Column
    private String lastName;
    
    @Column
    private String mobileNumber;
    
    @Column
    private String interest;
    
    @Column
    private String occupation;
    
    @Column
    private String about;
    
    @Column
    private String website;
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userInfoId", insertable = false, updatable = false, nullable = false, unique = false)
    private UserInfo userInfo;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public Integer getPersonalinfoId() {
        return personalinfoId;
    }

    public void setPersonalinfoId(Integer personalinfoId) {
        this.personalinfoId = personalinfoId;
    }

    public Integer getUserInfoId() {
        return userInfoId;
    }

    public void setUserInfoId(Integer userInfoId) {
        this.userInfoId = userInfoId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Override
    public String toString() {
        return "UserPersonalInfo{" + "personalinfoId=" + personalinfoId + ", userInfoId=" + userInfoId + ", firstName=" + firstName + ", lastName=" + lastName + ", mobileNumber=" + mobileNumber + ", interest=" + interest + ", occupation=" + occupation + ", about=" + about + ", website=" + website + '}';
    }
    
}
