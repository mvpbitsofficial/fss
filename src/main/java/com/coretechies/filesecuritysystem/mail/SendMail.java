/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.filesecuritysystem.mail;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

/**
 *
 * @author CoreTechies
 */

public class SendMail {
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private JavaMailSender mailSender;

    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public SendMail(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }
    
    public Boolean sendMailSameSubject(List<String> to, String subject, List<String> content) throws MessagingException{
        logger.info(" Prabhat: DEBUG: sendMailSameSubject: TO: "+to.size()+"; SUBJECT: "+subject+"; CONTENT: "+content.size()+"\n\n");
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, false);
        for (int i = 0; i < to.size(); i++) {
            logger.info(" Prabhat: DEBUG: sendMailSameSubject: send mail to: "+to.get(i));
            helper.setTo(to.get(i));
            helper.setSubject(subject);
            
            helper.setText(content.get(i), true);
            //message.setContent(content.get(i), "text/html; charset=utf-8");
            mailSender.send(message);
        }
        return true;
    }
    
    public Boolean sendMailSameSubject(List<String> to, String subject, String content) throws MessagingException{
        logger.info(" Prabhat: DEBUG: sendMailSameSubject: TO: "+to.size()+"; SUBJECT: "+subject+"; CONTENT: "+content+"\n\n");
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, false);
        for (int i = 0; i < to.size(); i++) {
            logger.info(" Prabhat: DEBUG: sendMailSameSubject: send mail to: "+to.get(i));
            helper.setTo(to.get(i));
            helper.setSubject(subject);
            
            helper.setText(content, true);
            //message.setContent(content.get(i), "text/html; charset=utf-8");
            mailSender.send(message);
        }
        return true;
    }
    
    public Boolean sendMail(List<String> to, List<String> subject, List<String> content) throws MessagingException{
        logger.info(" Prabhat: DEBUG: sendMail: TO: "+to.size()+"; SUBJECT: "+subject.size()+"; CONTENT: "+content.size()+"\n\n");
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, false);
        
        for (int i = 0; i < to.size(); i++) {
            logger.info(" Prabhat: DEBUG: sendMail: send mail to: "+to.get(i));
            helper.setTo(to.get(i));
            helper.setSubject(subject.get(i));
            helper.setText(content.get(i), true);
            mailSender.send(message);
        }
        return true;
    }
    
    public Boolean sendMailSingle(String to, String subject, String content) throws MessagingException{
        logger.info(" Prabhat: DEBUG: sendMailSingle: TO: "+to+"; SUBJECT: "+subject+"; CONTENT: "+content+"\n\n");
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, false);
            
        helper.setFrom("coretestingemail@gmail.com"); 
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(content, true);
        mailSender.send(message);
        
        return true;
    }
    public Boolean sendMailSameSubjectAndSameAttachment(List<String> to, String subject, List<String> content,String filepath) throws MessagingException{
        logger.info(" Prabhat: DEBUG: sendMailSameSubject: TO: "+to.size()+"; SUBJECT: "+subject+"; CONTENT: "+content.size()+"\n\n");
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true,"UTF-8");
        for (int i = 0; i < to.size(); i++) {
            logger.info(" Prabhat: DEBUG: sendMailSameSubject: send mail to: "+to.get(i));
            helper.setTo(to.get(i));
            helper.setSubject(subject);
            helper.setText(content.get(i), true);
            File file = new File(filepath);
            if(file.exists()){
            helper.addAttachment(file.getName(),file);
               
            }
            mailSender.send(message);
        }
        return true;
    }
    
    public Boolean sendMailSameSubjectWithAttachement(List<String> to, String subject, List<String> content,String filepath) throws MessagingException{
        logger.info(" Prabhat: DEBUG: sendMailSameSubject: TO: "+to.size()+"; SUBJECT: "+subject+"; CONTENT: "+content.size()+"\n\n");
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, false);
        for (int i = 0; i < to.size(); i++) {
            logger.info(" Prabhat: DEBUG: sendMailSameSubject: send mail to: "+to.get(i));
            helper.setTo(to.get(i));
            helper.setSubject(subject);
            helper.setText(content.get(i), true);
            File file = new File(filepath);
            if(file.exists()){
                helper.addAttachment(file.getName(),file);
            }
            mailSender.send(message);
        }
        return true;
    }
    
    public void sendEmail(String toemailid, String fromemailid, String content, String subject, String smtpserver, Integer port, String psw, String smtpuser) throws UnsupportedEncodingException{
        
        final String fromEmail = fromemailid; //requires valid gmail id
		final String password = psw; // correct password for gmail id
		final String toEmail = toemailid; // can be any email id 
		
		System.out.println("Email Sending...");
		Properties props = new Properties();
		props.put("mail.smtp.host", smtpserver); //SMTP Host
                props.put("mail.smtp.user", smtpuser);  //SMTP User
		props.put("mail.smtp.port", port); //TLS Port
		props.put("mail.smtp.auth", "true"); //enable authentication
		props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
		
                //create Authenticator object to pass in Session.getInstance argument
		Authenticator auth = new Authenticator() {
			//override the getPasswordAuthentication method
                        @Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		};
		Session session = Session.getInstance(props, auth);
		
		try {
            // Create a default MimeMessage object.
                MimeMessage message = new MimeMessage(session);

                // Set From: header field of the header.
                message.setFrom(new InternetAddress(fromEmail, smtpuser));

                // Set To: header field of the header.
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));

                // Set Subject: header field
                message.setSubject(subject);

                // Now set the actual message
                message.setText(content);
                MimeMessageHelper helper = new MimeMessageHelper(message, false);
                helper.setText(content, true);
                // Send message
                Transport.send(message);
                System.out.println("Sent mail successfully....");
             }catch (MessagingException mex) {
                mex.printStackTrace();
             }
		
	}


}
