/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.UserInfo;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;

/**
 *
 * @author CoreTechies M8
 */
public interface UserInfoDao {

    public UserInfo authenticate(String userName, String password);

    public UserInfo updateUser(UserInfo userInfo);

    public UserInfo getUserInfo(String newUserName);

    public UserInfo getUserInfo(String newUserName, Integer userId);

    public UserInfo getUserInfo(Integer userId);

    public UserInfo createUser(UserInfo userInfo);

    public List<UserInfo> getUserInfoByUserType(Integer userType);

    public Boolean deleteClient(Integer userId);

    public List<UserInfo> getUserInfoByCreatedBy(Integer createdBy);

    public List<UserInfo> getUserInfoByCreatedByDescendingOrder(Integer createdBy);

    public List<UserInfo> getUserInfoByCreatedOn(Integer createdBy, Date fromDate, Date toDate);
    
    public List<UserInfo> chartUserData(Integer createdBy, Date date);
    
    public Map<Integer, Integer> showUserIdThroughCount();
    
    public Map<Integer, Integer> showClientIdThroughCount();
    
    public Integer showUserChartData(Date startDate, Date endDate, Integer type, Integer createdBy);

    public UserInfo getUserInfoEmailAndType(String email, Integer type);
    
    public UserInfo getUserInfoMobileAndType(String mobile, Integer type);
}
