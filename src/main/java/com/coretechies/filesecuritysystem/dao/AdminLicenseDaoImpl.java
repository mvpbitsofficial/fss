/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.AdminLicense;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("AdminLicenseDao")
public class AdminLicenseDaoImpl implements AdminLicenseDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = false)
    public AdminLicense saveAdminLicense(AdminLicense adminLicense) {
        try {
            entityManager.persist(adminLicense);
            return adminLicense;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = false)
    public AdminLicense updateAdminLicense(AdminLicense adminLicense) {
        try {
            entityManager.merge(adminLicense);
            return adminLicense;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional(readOnly = false)
    public List<AdminLicense> getAdminLicense() {
        try {
            TypedQuery<AdminLicense> createQuery = entityManager.createQuery("SELECT AL FROM AdminLicense AS AL", AdminLicense.class);
            return createQuery.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = false)
    public Boolean deleteAdminLicense(Integer adminLicenseId) {
        try {
            entityManager.remove(entityManager.getReference(AdminLicense.class, adminLicenseId));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public AdminLicense getAdminLicense(Integer adminLicenseId) {
        try {
            AdminLicense adminLicense = entityManager.find(AdminLicense.class, adminLicenseId);
            Hibernate.initialize(adminLicense.getUserInfo());
            return adminLicense;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdminLicense> getLicenseExpireAlert(Date date) {
        try {
            TypedQuery<AdminLicense> createQuery = entityManager.createQuery("SELECT AL FROM AdminLicense AS AL WHERE AL.expiryDate IS NOT NULL AND AL.expiryDate<=:date", AdminLicense.class);
            createQuery.setParameter("date", date);
            List<AdminLicense> adminLicenses = createQuery.getResultList();
            for (AdminLicense adminLicense : adminLicenses) {
                Hibernate.initialize(adminLicense.getUserInfo());
            }
            return adminLicenses;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public AdminLicense getAdminLicenseByClient(Integer clientuserid) {
        try {
            TypedQuery<AdminLicense> createQuery = entityManager.createQuery("SELECT AL FROM AdminLicense AS AL WHERE AL.userId=:userId", AdminLicense.class);
            createQuery.setParameter("userId", clientuserid);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdminLicense> getAdminLicenseListByExpiryDate(Date fromDate, Date toDate) {
        try {
            TypedQuery<AdminLicense> createQuery = entityManager.createQuery("SELECT AL FROM AdminLicense AS AL WHERE AL.expiryDate>=:startDate AND AL.expiryDate<=:endDate", AdminLicense.class);
            createQuery.setParameter("startDate", fromDate);
            createQuery.setParameter("endDate", toDate);
            List<AdminLicense> adminLicenses = createQuery.getResultList();
            for (AdminLicense adminLicense : adminLicenses) {
                Hibernate.initialize(adminLicense.getUserInfo());
            }
            return adminLicenses;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdminLicense> getAdminLicenseListByCreateOn(Date fromDate, Date toDate) {
        try {
            TypedQuery<AdminLicense> createQuery = entityManager.createQuery("SELECT AL FROM AdminLicense AS AL WHERE AL.createOn>=:startDate AND AL.createOn<=:endDate", AdminLicense.class);
            createQuery.setParameter("startDate", fromDate);
            createQuery.setParameter("endDate", toDate);
            List<AdminLicense> adminLicenses = createQuery.getResultList();
            for (AdminLicense adminLicense : adminLicenses) {
                Hibernate.initialize(adminLicense.getUserInfo());
            }
            return adminLicenses;
        } catch (Exception e) {
            return null;
        }
    }
    
    @Override
    public Integer getClientLicenseForChart(Date date){
        try{
            TypedQuery<AdminLicense> createQuery = entityManager.createQuery("SELECT AL FROM AdminLicense AS AL WHERE AL.createOn=:createon", AdminLicense.class);
            createQuery.setParameter("createon", date);
            List<AdminLicense> resultList = createQuery.getResultList();
            if(resultList != null){
                return resultList.size();
            } else{
                return 0;
            }
        } catch(Exception e){
            e.printStackTrace();
            return 0;
        }
    }
}
