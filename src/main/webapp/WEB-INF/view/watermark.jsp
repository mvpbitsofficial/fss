<!DOCTYPE html>

<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>FSS | Watermark</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->


        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
        <style>
            .watermark_body{display:table; height:300px; width:100%;}
            .watermark_content{display:table-cell; height:100%;}
            .top_left_content{vertical-align: top; text-align:left;}
            .top_content{vertical-align: top; text-align:center;}
            .top_right_content{vertical-align: top; text-align:right;}
            .bottom_left_content{vertical-align: bottom; text-align:left;}
            .bottom_content{vertical-align: bottom; text-align:center;}
            .bottom_right_content{vertical-align: bottom; text-align:right;}
            .center_left_content{vertical-align: middle; text-align:left;}
            .center_content{vertical-align: middle; text-align:center;}
            .center_right_contentt{vertical-align: middle; text-align:right;}

            .cke_panel{z-index: 10060!important;}
        </style>
        <style>
            .entry-content {font-family: arial;}
            .bs-example{
                width: 500px;
                float: right;
            }
            #watermarkmessagedisplaytop p{
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
                max-width: 580px;
            }
        </style>
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top"> 
            <!-- BEGIN HEADER INNER -->
            <jsp:include page="topbar.jsp" />
            <!-- END HEADER INNER --> 
        </div>
        <!-- END HEADER --> 
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER --> 
        <!-- BEGIN CONTAINER -->
        <div class="page-container"> 
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper"> 
                <!-- BEGIN SIDEBAR --> 
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse"> 
                    <!-- BEGIN SIDEBAR MENU --> 
                    <jsp:include page="sidebar.jsp" />
                    <!--   <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                         DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
                        <li class="sidebar-toggler-wrapper hide"> 
                          BEGIN SIDEBAR TOGGLER BUTTON 
                         <div class="sidebar-toggler"> <span></span> </div>
                          END SIDEBAR TOGGLER BUTTON  
                        </li>
                        <li class="nav-item  "> <a href="index.html" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
                        <li class="nav-item  "> <a href="users.html" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
                         
                        </li>
                        <li class="nav-item  "> <a href="licence.html" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">Licence</span>  </a>
                         
                        </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
                         
                        </li>
                        <li class="nav-item  "> <a href="report.html" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
                        <li class="nav-item  active"> <a href="watermark.html" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
                        <li class="nav-item  "> <a href="settings.html" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
                       </ul>-->
                    <!-- END SIDEBAR MENU --> 
                    <!-- END SIDEBAR MENU --> 
                </div>
                <!-- END SIDEBAR --> 
            </div>
            <!-- END SIDEBAR --> 
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper"> 
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content"> 
                    <!-- BEGIN PAGE HEADER--> 

                    <!-- BEGIN PAGE TITLE-->
                    <!--<h3 class="page-title"><i class="icon-docs "></i> Report
                    <!-- <small>blank page layout</small>--
                    </h3>-->
                    <!-- END PAGE TITLE--> 
                    <!-- END PAGE HEADER-->
                    <div class="clearfix"></div>
                    <c:if test="${message != null}">
                        <div id="successMessage" class="bs-example">
                            <div  class="alert alert-success fade in">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success!</strong> ${message}
                            </div>
                        </div>
                    </c:if>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="portlet box red   portlet-datatable  ">
                                <div class="portlet-title">
                                    <div class="caption caption-md">
                                        <i class="icon-globe theme-font hide"></i>
                                        <span class="caption-subject  bold uppercase"><i class="icon-frame "></i> Watermark</span>
                                    </div>
                                    <div class="actions">
                                        <a class="btn  " href="javascript:;" style="display:none;" data-toggle="tooltip" data-placement="top" title="Preview Watermark" id="previewwatermark" onclick="preview();">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a class="btn  " href="javascript:;" style="display:none;" data-toggle="tooltip" data-placement="top" title="Delete Watermark" id="deletewatermark">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                        <a class="btn  " href="javascript:;" style="display:none;" data-toggle="tooltip" data-placement="top" title="Edit Watermark" id="editwatermark">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a class="btn  " href="javascript:;" style="display:none;" data-toggle="tooltip" data-placement="top" title="Set As Default" id="setdefaultwatermark">
                                            <i class="icon-frame"></i>
                                        </a>
                                        <a href="javascript:;" data-toggle="tooltip" data-placement="top" title="Add Watermark" id="watermark_modal_show" class="btn btn-sm pull-right"><i class="icon-plus"></i> </a> 
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_5">
                                        <thead>
                                            <tr>
                                                <th class="table-checkbox">
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes" />
                                                        <span></span>
                                                    </label>
                                                </th>
                                                <th> Watermark Name </th>
                                                <th> Alignment </th>
                                                <th> Font Size </th>
                                                <th> Font Name </th>
                                                <th> Default Watermark </th>
                                            </tr>
                                        </thead>

                                        <tbody id="tBody">
                                            <c:forEach items="${watermarks}" var="watermark">
                                                <tr class="odd gradeX">
                                                    <td>
                                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" name="watermarkId" class="checkboxes" value="${watermark.watermarkId}" />
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td>${watermark.watermarkName}</td>
                                                    <c:if test="${watermark.textPosition == 1}">
                                                        <td>Top Left</td>
                                                    </c:if>
                                                    <c:if test="${watermark.textPosition == 2}">
                                                        <td>Top Center</td>
                                                    </c:if>
                                                    <c:if test="${watermark.textPosition == 3}">
                                                        <td>Top Right</td>
                                                    </c:if>
                                                    <c:if test="${watermark.textPosition == 4}">
                                                        <td>Center Left</td>
                                                    </c:if>
                                                    <c:if test="${watermark.textPosition == 5}">
                                                        <td>Center</td>
                                                    </c:if>
                                                    <c:if test="${watermark.textPosition == 6}">
                                                        <td>Center Right</td>
                                                    </c:if>
                                                    <c:if test="${watermark.textPosition == 7}">
                                                        <td>Bottom Left</td>
                                                    </c:if>
                                                    <c:if test="${watermark.textPosition == 8}">
                                                        <td>Bottom Center</td>
                                                    </c:if>
                                                    <c:if test="${watermark.textPosition == 9}">
                                                        <td>Bottom Right</td>
                                                    </c:if>

                                                    <td>${watermark.fontSize}</td>
                                                    <td>${watermark.fontFamily}</td>
                                                    <c:if test="${watermark.defaultWatermark == true}">
                                                        <td>Yes</td>
                                                    </c:if>
                                                    <c:if test="${watermark.defaultWatermark == false}">
                                                        <td>No</td>
                                                    </c:if>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    <span class="watermarkerror" style="color: red; display: block; font-size: 14px;" ></span> 
                                </div>
                            </div>
                        </div>               
                    </div>



                </div> 
            </div>
            <!-- END CONTENT BODY --> 
            <!-- END CONTENT --> 
            <!-- BEGIN QUICK SIDEBAR --> 
            <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 
        </div>

        <!-- END QUICK SIDEBAR --> 
        <!-- END CONTAINER --> 
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; Organization </div>
            <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
        </div>
        <!-- END FOOTER --> 

        <!---->
        <!-- Modal -->
        <div class="modal fade" id="watermark_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Watermark Editor</h4>
                    </div>
                    <form>
                        <div class="modal-body">
                            
                            <input type="text" class="form-control input-circle" placeholder="Enter Your Watermark Name" id="watermarkName">
                            <span style="color: red; display: block; font-size: 14px;" id="errorMessageWatermarkName"></span>
                            <br/>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <select class="form-control input-circle selectpicker" id="position" data-live-search='true' data-live-search-style='startsWith'>
                                            <option value="0" selected disabled>--Watermark Align--</option>
                                            <option value="1">Top Left</option>
                                            <option value="2">Top Center</option>
                                            <option value="3">Top Right</option>
                                            <option value="4">Center Left</option>
                                            <option value="5">Center</option>
                                            <option value="6">Center Right</option>
                                            <option value="7">Bottom Left</option>
                                            <option value="8">Bottom Center</option>
                                            <option value="9">Bottom Right</option>
                                        </select>
                                        <span style="color: red; display: block; font-size: 14px;" id="errorMessageWatermarkPosition"></span>
                                    </div>
                                    <div class="col-sm-6 text-right">
                                        <div class="mt-checkbox-inline" >
                                            <!--                                            <a href="javascript:;" class="btn btn-success btn-sm btn-circle" id="previewatcreate">Preview</a>
                                                                                        <a href="javascript:;" class="btn btn-success btn-sm" style="display: none;" id="edittextwhencreate">Edit</a>-->
                                            <label class="mt-checkbox mt-checkbox-outline">
                                                <input type="checkbox" id="defaultStatus"> Set Default
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
<!--                                <div class="editor_properties" style="background-color: lightblue; height: 45px;">
                                    <a class="btn text_bold" href="javascript:;" style="border: 1px;" >B</a>
                                </div>-->
                                <textarea class="textarea form-control" rows="6" id="watermarkMessage"></textarea>
                                <span style="color: red; display: block; font-size: 14px;" id="errorMessageWatermarkMessage"></span>
                        </div>
                        <div class="modal-footer">
                            <span id="successMessage" style="color: green; display: block; font-size: 14px;"  ></span>
                            <button type="reset" class="btn btn-circle btn-danger" id="resetcreatewatermark">Reset</button><!--data-dismiss="modal"-->
                            <button type="button" class="btn btn-circle btn-success" id="addWatermark">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!---->
        <!-- Update Modal -->
        <div class="modal fade" id="watermark_update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="updateModalLabel">Watermark Editor</h4>
                    </div>
                    <form>
                        <div class="modal-body">
                            
                            <input type="hidden" id="newwatermarkid" />
                            <input type="text" class="form-control input-circle" placeholder="Enter Your Watermark Name" id="newwatermarkName">
                            <span style="color: red; display: block; font-size: 14px;" id="errorMessagenewWatermarkName"></span>
                            <br/>
                            
                                <div class="row">
                                    <div class="col-sm-6">
                                        <select class="form-control input-circle selectpicker" id="newposition" data-live-search='true' data-live-search-style='startsWith'>
                                            <option value="0">Watermark Align</option>
                                            <option value="1">Top Left</option>
                                            <option value="2">Top Center</option>
                                            <option value="3">Top Right</option>
                                            <option value="4">Center Left</option>
                                            <option value="5">Center</option>
                                            <option value="6">Center Right</option>
                                            <option value="7">Bottom Left</option>
                                            <option value="8">Bottom Center</option>
                                            <option value="9">Bottom Right</option>
                                        </select>
                                        <span style="color: red; display: block; font-size: 14px;" id="errorMessagenewWatermarkPosition"></span>
                                    </div>
                                    <div class="col-sm-6 text-right">
                                        <div class="mt-checkbox-inline" >
                                            <!--                                            <a href="javascript:;" class="btn btn-success btn-sm btn-circle" id="previewatupdate">Preview</a>
                                                                                        <a href="javascript:;" class="btn btn-success btn-sm" style="display: none;" id="edittextwhenupdate">Edit</a>-->
                                            <label class="mt-checkbox mt-checkbox-outline">
                                                <input type="checkbox" id="defaultupdateStatus"> Set Default
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            
                            <textarea class="textarea1 form-control" rows="6" id="newwatermarkMessage"></textarea>
                            <span style="color: red; display: block; font-size: 14px;" id="errorMessagenewWatermarkMessage"></span>
                            
                        </div>
                        <div class="modal-footer">
                            <span id="responseMessage" style="color: green; display: block; font-size: 14px;" ></span>
                            <button type="reset" class="btn btn-circle btn-danger" id="resetupdatewatermark" >Reset</button><!--data-dismiss="modal"-->
                            <button type="button" class="btn btn-circle btn-success" id="updateWatermark">Update</button>
                            <span class="error-message" id="errorMessageUpdateWatermark"></span>
                            <label style="display: none;" id="convertText" ></label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!---->

        <!--Preview Modal -->
        <div class="modal fade" id="Preview_modal" tabindex="-1" role="dialog" aria-labelledby="previewModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="previewModalLabel">Watermark Preview</h4>
                    </div>
                    <form>
                        <div class="modal-body watermark_body" >

                            <span id="watermarkmessagedisplaytopleft" class="watermark_content top_left_content"></span>
                            <span id="watermarkmessagedisplaytop" class="watermark_content top_content"></span>
                            <span id="watermarkmessagedisplaytopright" class="watermark_content top_right_content"></span>
                            <span id="watermarkmessagedisplaybottomleft" class="watermark_content bottom_left_content"></span>
                            <span id="watermarkmessagedisplaybottom" class="watermark_content bottom_content"></span>
                            <span id="watermarkmessagedisplaybottomright" class="watermark_content bottom_right_content"></span>
                            <span id="watermarkmessagedisplaycenterleft" class="watermark_content center_left_content"></span>
                            <span id="watermarkmessagedisplaycenter" class="watermark_content center_content"></span>
                            <span id="watermarkmessagedisplaycenterright" class="watermark_content center_right_content"></span>

                        </div>
                        <div class="modal-footer">

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="confirm_delete" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="">Confirm Delete</h4>
                    </div>
                    <div class="modal-body"> 
                        <div class="form-group">
                            <span Style="font-style: normal;">Are You Sure To Want To Delete Selected Watermark ?</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <span id="response" style="color: green; display: block; font-size: 14px; float: left;" ></span>
                        <!--<button type="button" class=" btn btn green" data-dismiss="modal">NO</button>-->
                        <!--                        <button type="button" class="btn green" id="">YES</button>-->
                        <a href="javascript:;" id="Confirm" class="btn btn-circle btn-success">YES</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- BEGIN CORE PLUGINS --> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js" type="text/javascript"></script>  
        <script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
        <!-- END CORE PLUGINS --> 
        <!-- BEGIN THEME GLOBAL SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
        <!-- END THEME GLOBAL SCRIPTS --> 
        <!-- BEGIN THEME LAYOUT SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 

<!--<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>-->


        <!-- END THEME LAYOUT SCRIPTS --> 

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="${pageContext.request.contextPath}/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/ckeditor/adapters/jquery.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="${pageContext.request.contextPath}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>

<!--<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>-->
<!--<script src="${pageContext.request.contextPath}/assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>-->

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#side-menu').find('li').removeClass('active');
            $(document).find(".watermark").addClass('active');

        });
    </script>
    <script>
        $(function () {
            $(".textarea").ckeditor();
            $(".textarea1").ckeditor();
        });
        $(document).ready(function () {
            CKEDITOR.config.customConfig = 'configSimple';
        });

        CKEDITOR.editorConfig = function (config)
        {
            // default toolbar
            config.toolbar = [
                    {name: 'basicstyles', items: ['Bold', 'Italic']},
                    {name: 'source', items: ['ShowBlocks', 'Source']},
                    {name: 'clipboard', items: ['Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord']},
                    {name: 'editing', items: ['Find', 'Replace', 'SelectAll', 'Scayt']},
                    {name: 'p2', items: ['Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                    {name: 'colors', items: ['TextColor'], attributes: { 'color': '#(color)' }},
                    {name: 'morestyles', items: ['Font', 'FontSize']}
            ];

            // here is another custom toolbar
            config.toolbar_mycustom = [
                {name: 'styles', items: ['Format']},
                {name: 'morestyles', items: ['Font', 'FontSize']},
                {name: 'colors', items: ['TextColor']}
            ];
            config.fontSize_defaultLabel = '18px';

            config.font_defaultLabel = 'Arial';
        };

        $("#watermark_modal_show").on("click", function(){
            $("#watermarkName").val("");
            $("#position").prop('selectedIndex', 0);
            $("#position").selectpicker('refresh');
            $('#cke_watermarkMessage').find('.cke_wysiwyg_frame').contents().find('body').html("");
            $("#errorMessageWatermarkName").html("");
            $("#errorMessageWatermarkMessage").html("");
            $("#errorMessageWatermarkPosition").html("");
            $("#errorMessageAddWatermark").html("");
            $("#watermark_modal").modal("show");
        });

        /* This is block of code for adding the watermark  */
            
            
            $("#addWatermark").on("click", function () {
                var myarray = [];
                var fontSize;
                var fontFamily;
                var fontfamily_value;
                var font_color = "#000000";
                var bold = 0;
                var italic = 0;
                var align = 0;
                var property;
                var watermarkName = $("#watermarkName").val().trim();
                var watermarkMessage = $("#watermarkMessage").val().trim();;
                var watermarkContent;
                $('#cke_watermarkMessage').find('.cke_wysiwyg_frame').contents().find('p').each(function () {
                    var cssVar1 = $(this).html();
                    console.log(cssVar1);
                    var varCss = $(this).attr('style');
                    if(cssVar1.indexOf("<strong") !== -1){
                        bold = 1;
                    } 
                    if(cssVar1.indexOf("<em") !== -1){
                        italic = 1;
                    }
                    
                    if( varCss !== undefined){
                        if(varCss.indexOf("text-align: center") !== -1){
                            align = 1;
                        }
                        if(varCss.indexOf("text-align: right") !== -1){
                            align = 3;
                        }
                        if(varCss.indexOf("text-align: left") !== -1){
                            align = 2;
                        }
                        if(varCss.indexOf("text-align: justify") !== -1){
                            align = 0;
                        }
                    }
                    
                    $(this).find('span').each(function () {
                        var cssVar = $(this).attr('style');
                        myarray.push(cssVar);
                    });
                    watermarkContent = $(this).text();
                });
                console.log("myarray = "+myarray);
//                if (myarray.length > 0) {
//                    if (myarray[0].indexOf('font-family:') != -1) {
//                        myarray.reverse();
//                        console.log("reverse = "+myarray.reverse());
//                    }
//                    if(myarray[0].indexOf('color:') != -1){
//                        if (myarray[1].indexOf('font-family:') != -1) {
//                            myarray.reverse();
//                        }
//                        console.log("reverse1 = "+myarray.reverse());
//                    }
//                }
                var position = $("#position").select().val();
                var defaultStatus = $("#defaultStatus").is(":checked");
                if (defaultStatus) {
                    fontSize = "18px";
                    fontFamily = "Arial";
                } else {
                    if (myarray.length > 0) {
                        if(myarray[0].indexOf("font-size") !== -1){
                            fontSize = myarray[0].substring(10);
                        } else if(myarray[0].indexOf("font-family:") !== -1){
                            fontFamily = myarray[0].substring(12);
                        } else if(myarray[0].indexOf("color:") !== -1){
                            font_color = myarray[0].substring(6);
                        }
                        if(myarray[1].indexOf("font-size") !== -1){
                            fontSize = myarray[1].substring(10);
                        } else if(myarray[1].indexOf("font-family:") !== -1){
                            fontFamily = myarray[1].substring(12);
                        } else if(myarray[1].indexOf("color:") !== -1){
                            font_color = myarray[1].substring(6);
                        }
                        if(myarray[2] !== undefined){
                            if(myarray[2].indexOf("font-size") !== -1){
                                fontSize = myarray[2].substring(10);
                            } else if(myarray[2].indexOf("font-family:") !== -1){
                                fontFamily = myarray[2].substring(12);
                            } else if(myarray[2].indexOf("color:") !== -1){
                                font_color = myarray[2].substring(6);
                            }
                        }
//                        fontSize = myarray[0].substring(10);
//                        fontFamily = myarray[1].substring(12);
                        if(fontFamily.indexOf('arial') !== -1){
                            fontfamily_value = 2;
                        } 
                        if(fontFamily.indexOf('comic sans ms') !== -1){
                            fontfamily_value = 39;
                        } 
                        if(fontFamily.indexOf('courier new') !== -1){
                            fontfamily_value = 46;
                        } 
                        if(fontFamily.indexOf('georgia') !== -1){
                            fontfamily_value = 70;
                        } 
                        if(fontFamily.indexOf('lucida sans unicode') !== -1){
                            fontfamily_value = 101;
                        } 
                        if(fontFamily.indexOf('tahoma') !== -1){
                            fontfamily_value = 191;
                        } 
                        if(fontFamily.indexOf('times new roman') !== -1){
                            fontfamily_value = 194;
                        } 
                        if(fontFamily.indexOf('trebuchet ms') !== -1){
                            fontfamily_value = 195;
                        } 
                        if(fontFamily.indexOf('verdana') !== -1){
                            fontfamily_value = 199;
                        } 
                        console.log("font value = "+fontfamily_value);
                        console.log("font Family  = "+fontFamily);
                        console.log("font Size  = "+fontSize);
                        if(font_color.indexOf("rgb") !== -1){
                            console.debug(font_color);
                            var h = /rgb\(([0-9]+), ([0-9]+), ([0-9]+)\);/g.exec(font_color);
                            font_color = "#" + componentToHex(parseInt(h[1])) + componentToHex(parseInt(h[2])) + componentToHex(parseInt(h[3]));
                        }
                        console.log("font Color  = "+font_color);
                    }
                }
                function componentToHex(c) {
                    var hex = c.toString(16);
                    return hex.length == 1 ? "0" + hex : hex;
                }
                property = fontfamily_value+"::"+fontSize.replace("px;","").replace(" ","")+"::"+font_color.replace(";","").replace("#","")+"::"+bold+""+italic+""+0+"::"+align;
                console.log(property);
                $("#errorMessageWatermarkName").html("");
                $("#errorMessageWatermarkMessage").html("");
                $("#errorMessageWatermarkPosition").html("");
                $("#errorMessageAddWatermark").html("");
                var flag = true;
                if (watermarkName === null || watermarkName === "") {
                    $("#errorMessageWatermarkName").html("Please enter watermark name.");
                    flag = false;
                } else if(watermarkName.length > 40){
                    $("#errorMessageWatermarkName").html("Watermark name can not contain greater then 40 characters.");
                    flag = false;
                }
                if(fontSize === undefined || fontFamily === undefined){
                    $("#errorMessageWatermarkMessage").html("Please Apply font-size and font-family.");
                    flag= false;
                }
                if (watermarkMessage === null || watermarkMessage === "") {
                    $("#errorMessageWatermarkMessage").html("Please enter watermark message.");
                    flag = false;
                }
                if((watermarkMessage.length - 9) === 0){
                    $("#errorMessageWatermarkMessage").html("Please enter watermark message.");
                    flag = false;
                }
                if (position === null || position === "0") {
                    $("#errorMessageWatermarkPosition").html("Please select position.");
                    flag = false;
                } 
                if (flag) {
                    console.log(watermarkName +""+watermarkMessage+""+position+""+fontSize+""+fontFamily);
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/addwatermark", {watermarkName: watermarkName, 
                            watermarkMessage: watermarkMessage, watermarkContent: watermarkContent, property: property, position: position, fontSize: fontSize, fontFamily: fontFamily},
                        function (data, status) {
                            if (data === "success") {
                                $("#successMessage").html("Water mark has been added successfully");
                                setTimeout(function()
                                {
                                    $("#watermark_modal").modal("hide");
                                }, 10000);
                               window.location.reload();
                            } else {
                                $("#errorMessageAddWatermark").html(data);
                            }
                        }
                    );
                }
            });
            
            $("#watermarkName").on("change", function(){
                var watermarkName = $("#watermarkName").val().trim();
                if(watermarkName === "" || watermarkName === null){
                    $("#errorMessageWatermarkName").html("Please enter watermark name.");
                } else{
                    $("#errorMessageWatermarkName").html("");
                }
            });
            $("#position").on("change", function(){
                var position = $("#position").select().val();
                if(position === "0" || position === null){
                    $("#errorMessageWatermarkPosition").html("Please select position.");
                } else{
                    $("#errorMessageWatermarkPosition").html("");
                }
            });
            $("#watermarkMessage").on("change", function(){
                var watermarkMessage = $("#watermarkMessage").val().trim();
                if((watermarkMessage.length - 9) === 0){
                    $("#errorMessageWatermarkMessage").html("Please enter watermark Message.");
                } else{
                    $("#errorMessageWatermarkMessage").html("");
                }
            });
            
            /* We are performing action on reset button and clearing all content from fields */

            $("#resetcreatewatermark").on("click", function () {
                $("#watermarkName").val("");
                $("#position").prop('selectedIndex', 0);
                $("#position").selectpicker('refresh');
                $('#cke_watermarkMessage').find('.cke_wysiwyg_frame').contents().find('body').html("");
                $("#errorMessageWatermarkName").html("");
                $("#errorMessageWatermarkMessage").html("");
                $("#errorMessageWatermarkPosition").html("");
                $("#errorMessageAddWatermark").html("");
            });
            
            $("#resetupdatewatermark").on("click", function () {
                $("#newwatermarkName").val("");
                $("#newposition").prop('selectedIndex', 0);
                $("#newposition").selectpicker('refresh');
                $('#cke_newwatermarkMessage').find('.cke_wysiwyg_frame').contents().find('body').html("");
                $("#errorMessagenewWatermarkName").html("");
                $("#errorMessagenewWatermarkMessage").html("");
                $("#errorMessagenewWatermarkPosition").html("");
                $("#responseMessage").html("");
            });
            
        </script>
         
        <script>
            $(".checkboxes").on("click", function () {
                var checkedData = [];
                $("#tBody").find("input[type='checkbox'][name='watermarkId']:checked").each(function () {
                    checkedData.push($(this).val());
                });
                if (checkedData.length === 1) {
                    $("#deletewatermark").show();
                    $("#editwatermark").show();
                    $("#setdefaultwatermark").show();
                    $("#previewwatermark").show();

                } else if (checkedData.length > 1) {
                    $("#deletewatermark").show();
                    $("#editwatermark").hide();
                    $("#setdefaultwatermark").hide();
                    $("#previewwatermark").hide();
                } else if (checkedData === null || checkedData.length === 0) {
                    $("#deletewatermark").hide();
                    $("#editwatermark").hide();
                    $("#setdefaultwatermark").hide();
                    $("#previewwatermark").hide();
                }
            });

            $(".group-checkable").on("click", function () {
                var checkedData = [];
                var checkedboxes = [];
                $("#tBody").find("input[type='checkbox'][name='watermarkId']").each(function () {
                    checkedData.push($(this).val());
                });
                $("#tBody").find("input[type='checkbox'][name='watermarkId']:checked").each(function () {
                    checkedboxes.push($(this).val());
                });
                console.log($(this).is(":checked"));
                if ($(this).is(":checked")) {
                    console.log(checkedboxes);
                    if (checkedData === null || checkedData.length === 0) {
                        $("#deletewatermark").hide();
                    } else {
                        $("#deletewatermark").show();
                        if (checkedboxes !== null || checkedboxes.length !== 0) {
                            $("#editwatermark").hide();
                            $("#setdefaultwatermark").hide();
                            $("#previewwatermark").hide();
                        }
                    }
                } else {
                    $("#deletewatermark").hide();
                    $("#editwatermark").hide();
                    $("#setdefaultwatermark").hide();
                    $("#previewwatermark").hide();
                }
            });
        </script>
        <script>
            $("#editwatermark").on("click", function () {
                var checkedData = [];
                $("#tBody").find("input[type='checkbox'][name='watermarkId']:checked").each(function () {
                    checkedData.push($(this).val());
                });
                if (checkedData === null || checkedData.length === 0) {
                    $("#watermarkerror").text("Please Select a Watermark");
                } else if (checkedData.length > 1) {
                    $("#watermarkerror").text("More Than One Watermark Are Selected");
                } else {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.get("${pageContext.request.contextPath}/"+Uname+"/showpreview", {watermarkId: checkedData[0]},
                            function (data, status) {
                                if (data.indexOf("success") > -1) {
                                    console.log(data);
                                    var jsondata = JSON.parse(data);
                                    var str = (jsondata.watermarkMessage).replace("\\", " ");
                                    $("#newwatermarkName").val(jsondata.watermarkName);
                                    $("#newwatermarkMessage").val(str);
                                    $("#newposition").prop('selectedIndex', jsondata.textPosition);
                                    $("#newposition").selectpicker('refresh');
                                    $("#errorMessagenewWatermarkName").html("");
                                    $("#errorMessagenewWatermarkMessage").html("");
                                    $("#errorMessagenewWatermarkPosition").html("");
                                    $("#responseMessage").html("");
                                    $("#watermark_update").modal("show");
                                } else {
                                    $("#watermarkerror").text(data);
                                }
                            });
//                    window.location.href = "${pageContext.request.contextPath}/${user.userName}/updatewatermark/"+checkedData[0];
                }
            });

            
            $("#updateWatermark").on("click", function () {
                var myarray1 = []; 
                var fontSize;
                var fontFamily;
                var fontfamily_value;
                var font_color = "#000000";
                var bold = 0;
                var italic = 0;
                var align = 0;
                var property;
                var watermarkContent;
                $("#errorMessagenewWatermarkName").html("");
                $("#errorMessagenewWatermarkMessage").html("");
                $("#errorMessagenewWatermarkPosition").html("");
                    $('#cke_newwatermarkMessage').find('.cke_wysiwyg_frame').contents().find('p').each(function (){
                        var cssVar1 = $(this).html();
                        console.log(cssVar1);
                        var varCss = $(this).attr('style');
                        if(cssVar1.indexOf("<strong") !== -1){
                            bold = 1;
                        } 
                        if(cssVar1.indexOf("<em") !== -1){
                            italic = 1;
                        }
                        if( varCss !== undefined){
                            if(varCss.indexOf("text-align: center") !== -1){
                                align = 1;
                            }
                            if(varCss.indexOf("text-align: right") !== -1){
                                align = 3;
                            }
                        }
                        
                        $(this).find('span').each(function () {
                            var cssVar = $(this).attr('style');
                            myarray1.push(cssVar);
                        });
                        watermarkContent = $(this).text();
                    });
//                    if (myarray1.length > 0) {
//                        if (myarray1[0].indexOf('font-family:') !== -1) {
//                            myarray1.reverse();
//                        }
//                    }
                    var defaultStatus = $("#defaultupdateStatus").is(":checked");
                    if (defaultStatus) {
                        fontSize = "18px";
                        fontFamily = "Arial";
                    } else {
                        if (myarray1.length > 0) {
                            if(myarray1[0].indexOf("font-size") !== -1){
                                fontSize = myarray1[0].substring(10);
                            } else if(myarray1[0].indexOf("font-family:") !== -1){
                                fontFamily = myarray1[0].substring(12);
                            } else if(myarray1[0].indexOf("color:") !== -1){
                                font_color = myarray1[0].substring(6);
                            }
                            if(myarray1[1].indexOf("font-size") !== -1){
                                fontSize = myarray1[1].substring(10);
                            } else if(myarray1[1].indexOf("font-family:") !== -1){
                                fontFamily = myarray1[1].substring(12);
                            } else if(myarray1[1].indexOf("color:") !== -1){
                                font_color = myarray1[1].substring(6);
                            }
                            if(myarray1[2] !== undefined){
                                if(myarray1[2].indexOf("font-size") !== -1){
                                    fontSize = myarray1[2].substring(10);
                                } else if(myarray1[2].indexOf("font-family:") !== -1){
                                    fontFamily = myarray1[2].substring(12);
                                } else if(myarray1[2].indexOf("color:") !== -1){
                                    font_color = myarray1[2].substring(6);
                                }
                            }
    //                        fontSize = myarray[0].substring(10);
    //                        fontFamily = myarray[1].substring(12);
                            if(fontFamily.indexOf('arial') !== -1){
                                fontfamily_value = 2;
                            } 
                            if(fontFamily.indexOf('comic sans ms') !== -1){
                                fontfamily_value = 39;
                            } 
                            if(fontFamily.indexOf('courier new') !== -1){
                                fontfamily_value = 46;
                            } 
                            if(fontFamily.indexOf('georgia') !== -1){
                                fontfamily_value = 70;
                            } 
                            if(fontFamily.indexOf('lucida sans unicode') !== -1){
                                fontfamily_value = 101;
                            } 
                            if(fontFamily.indexOf('tahoma') !== -1){
                                fontfamily_value = 191;
                            } 
                            if(fontFamily.indexOf('times new roman') !== -1){
                                fontfamily_value = 194;
                            } 
                            if(fontFamily.indexOf('trebuchet ms') !== -1){
                                fontfamily_value = 195;
                            } 
                            if(fontFamily.indexOf('verdana') !== -1){
                                fontfamily_value = 199;
                            } 
                            console.log("font value = "+fontfamily_value);
                            console.log("font Family  = "+fontFamily);
                            console.log("font Size  = "+fontSize);
                            if(font_color.indexOf("rgb") !== -1){
                                console.debug(font_color);
                                var h = /rgb\(([0-9]+), ([0-9]+), ([0-9]+)\);/g.exec(font_color);
                                font_color = "#" + componentToHex(parseInt(h[1])) + componentToHex(parseInt(h[2])) + componentToHex(parseInt(h[3]));
                            }
                            console.log("font Color  = "+font_color);
                        }
//                        if (myarray1.length > 0) {
//                            fontSize = myarray1[0].substring(10);
//                            fontFamily = myarray1[1].substring(12);
//                        }
                    }
                function componentToHex(c) {
                    var hex = c.toString(16);
                    return hex.length == 1 ? "0" + hex : hex;
                }
                property = fontfamily_value+"::"+fontSize.replace("px;","").replace(" ","")+"::"+font_color.replace(";","").replace("#","")+"::"+bold+""+italic+""+0+"::"+align;
                console.log(property);
                var checkedData = [];
                $("#tBody").find("input[type='checkbox'][name='watermarkId']:checked").each(function () {
                    checkedData.push($(this).val());
                });
                if (checkedData === null || checkedData.length === 0) {
                    $("#watermarkerror").text("Please Select a Watermark");
                } else if (checkedData.length > 1) {
                    $("#watermarkerror").text("More Than One Watermark Are Selected");
                } else {
                    var watermarkname = $("#newwatermarkName").val().trim();
                    var watermarkpos = $("#newposition").select().val();
                    var watermarkmessage = $("#newwatermarkMessage").val().trim();
                    var flag = true;
                    if (watermarkname === null || watermarkname === "") {
                        $("#errorMessagenewWatermarkName").html("Please enter watermark name.");
                        flag = false;
                    } else if(watermarkname.length > 40){
                        $("#errorMessagenewWatermarkName").html("Watermark name can not contain greater then 40 characters.");
                        flag = false;
                    }
                    if(fontSize === undefined || fontFamily === undefined){
                        $("#errorMessagenewWatermarkMessage").html("Please Apply font-size and font-family.");
                        flag= false;
                    }
                    if (watermarkmessage === null || watermarkmessage === "") {
                        $("#errorMessagenewWatermarkMessage").html("Please enter watermark message.");
                        flag = false;
                    }
                    if ((watermarkmessage.length - 9) === 0) {
                        $("#errorMessagenewWatermarkMessage").html("Please enter watermark message.");
                        flag = false;
                    }
                    if (watermarkpos === null || watermarkpos === "0") {
                        $("#errorMessagenewWatermarkPosition").html("Please select position.");
                        flag = false;
                    } 
                    
                    if (flag) {
                        var Uname = "${user.userName}".replace(" ", "");
                        $.post("${pageContext.request.contextPath}/"+Uname+"/updatewatermark", 
                        {watermarkId: checkedData[0], watermarkPosition: watermarkpos, watermarkName: watermarkname, watermarkMessage: watermarkmessage,
                            watermarkContent: watermarkContent,property:property, fontSize: fontSize, fontFamily: fontFamily},
                                function (data, status) {
                                    if (data === "success") {
                                        $("#responseMessage").html("Watermark has been updated successfully");
                                        setTimeout(function()
                                        {
                                            $("#watermark_update").modal("hide");
                                        }, 10000);
                                        window.location.reload();
                                    } else {
                                        $("#responseMessage").html(data);
                                    }
                                }
                        );
                    }
                }
            });
            
            $("#setdefaultwatermark").on("click", function () {
                var checkedData = [];
                $("#tBody").find("input[type='checkbox'][name='watermarkId']:checked").each(function () {
                    checkedData.push($(this).val());
                });
                if (checkedData === null || checkedData.length === 0) {
                    $("#watermarkerror").text("Please Select a Watermark");
                } else if (checkedData.length > 1) {
                    $("#watermarkerror").text("More Than One Watermark Are Selected");
                } else {
                    var Uname = "${user.userName}".replace(" ", "");
                    window.location.href = "${pageContext.request.contextPath}/"+Uname+"/setdefaultwatermark/" + checkedData[0];
                }
            });

            function preview() {
                var checkedData = [];
                $("#tBody").find("input[type='checkbox'][name='watermarkId']:checked").each(function () {
                    checkedData.push($(this).val());
                });
                if (checkedData === null || checkedData.length === 0) {
                    $("#watermarkerror").text("Please Select a Watermark");
                } else if (checkedData.length > 1) {
                    $("#watermarkerror").text("More Than One Watermark Are Selected");
                } else {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.get("${pageContext.request.contextPath}/"+Uname+"/showpreview", {watermarkId: checkedData[0]},
                            function (data, status) {
                                console.log(data);
                                if (data.indexOf("success") > -1) {
                                    var jsondata = JSON.parse(data);
                                    var pos = jsondata.textPosition;
                                    console.log(pos);
                                    var str = (jsondata.watermarkMessage).replace("\\", " ");
                                    if (pos == 1) {
                                        console.log(str);
                                        $("#watermarkmessagedisplaytopleft").html(str);
                                        $("#watermarkmessagedisplaytop").html("");
                                        $("#watermarkmessagedisplaytopright").html("");
                                        $("#watermarkmessagedisplaybottomleft").html("");
                                        $("#watermarkmessagedisplaybottom").html("");
                                        $("#watermarkmessagedisplaycenterleft").html("");
                                        $("#watermarkmessagedisplaycenter").html("");
                                        $("#watermarkmessagedisplaycenterright").html("");
                                        $("#watermarkmessagedisplaybottomright").html("");
                                    } else if (pos == 2) {
                                        console.log(str);
                                        $("#watermarkmessagedisplaytop").html(str);
                                        $("#watermarkmessagedisplaytopleft").html("");
                                        $("#watermarkmessagedisplaytopright").html("");
                                        $("#watermarkmessagedisplaycenterleft").html("");
                                        $("#watermarkmessagedisplaycenter").html("");
                                        $("#watermarkmessagedisplaycenterright").html("");
                                        $("#watermarkmessagedisplaybottomleft").html("");
                                        $("#watermarkmessagedisplaybottom").html("");
                                        $("#watermarkmessagedisplaybottomright").html("");
                                    } else if (pos == 3) {
                                        console.log(str);
                                        $("#watermarkmessagedisplaytopright").html(str);
                                        $("#watermarkmessagedisplaytopleft").html("");
                                        $("#watermarkmessagedisplaytop").html("");
                                        $("#watermarkmessagedisplaycenterleft").html("");
                                        $("#watermarkmessagedisplaycenter").html("");
                                        $("#watermarkmessagedisplaycenterright").html("");
                                        $("#watermarkmessagedisplaybottomleft").html("");
                                        $("#watermarkmessagedisplaybottom").html("");
                                        $("#watermarkmessagedisplaybottomright").html("");
                                    } else if (pos == 4) {
                                        console.log(str);
                                        $("#watermarkmessagedisplaycenterleft").html(str);
                                        $("#watermarkmessagedisplaytopleft").html("");
                                        $("#watermarkmessagedisplaytop").html("");
                                        $("#watermarkmessagedisplaytopright").html("");
                                        $("#watermarkmessagedisplaycenter").html("");
                                        $("#watermarkmessagedisplaycenterright").html("");
                                        $("#watermarkmessagedisplaybottomleft").html("");
                                        $("#watermarkmessagedisplaybottom").html("");
                                        $("#watermarkmessagedisplaybottomright").html("");
                                    } else if (pos == 5) {
                                        console.log(str);
                                        $("#watermarkmessagedisplaycenter").html(jsondata.watermarkMessage);
                                        $("#watermarkmessagedisplaytopleft").html("");
                                        $("#watermarkmessagedisplaytop").html("");
                                        $("#watermarkmessagedisplaytopright").html("");
                                        $("#watermarkmessagedisplaycenterleft").html("");
                                        $("#watermarkmessagedisplaycenterright").html("");
                                        $("#watermarkmessagedisplaybottomleft").html("");
                                        $("#watermarkmessagedisplaybottom").html("");
                                        $("#watermarkmessagedisplaybottomright").html("");
                                    } else if (pos == 6) {
                                        console.log(str);
                                        $("#watermarkmessagedisplaycenterright").html(str);
                                        $("#watermarkmessagedisplaytopleft").html("");
                                        $("#watermarkmessagedisplaytop").html("");
                                        $("#watermarkmessagedisplaytopright").html("");
                                        $("#watermarkmessagedisplaycenterleft").html("");
                                        $("#watermarkmessagedisplaycenter").html("");
                                        $("#watermarkmessagedisplaybottomleft").html("");
                                        $("#watermarkmessagedisplaybottom").html("");
                                        $("#watermarkmessagedisplaybottomright").html("");
                                    } else if (pos == 7) {
                                        console.log(str);
                                        $("#watermarkmessagedisplaybottomleft").html(str);
                                        $("#watermarkmessagedisplaytopleft").html("");
                                        $("#watermarkmessagedisplaytop").html("");
                                        $("#watermarkmessagedisplaytopright").html("");
                                        $("#watermarkmessagedisplaycenterleft").html("");
                                        $("#watermarkmessagedisplaycenter").html("");
                                        $("#watermarkmessagedisplaycenterright").html("");
                                        $("#watermarkmessagedisplaybottom").html("");
                                        $("#watermarkmessagedisplaybottomright").html("");
                                    } else if (pos == 8) {
                                        console.log(str);
                                        $("#watermarkmessagedisplaybottom").html(str);
                                        $("#watermarkmessagedisplaytopleft").html("");
                                        $("#watermarkmessagedisplaytop").html("");
                                        $("#watermarkmessagedisplaytopright").html("");
                                        $("#watermarkmessagedisplaycenterleft").html("");
                                        $("#watermarkmessagedisplaycenter").html("");
                                        $("#watermarkmessagedisplaycenterright").html("");
                                        $("#watermarkmessagedisplaybottomleft").html("");
                                        $("#watermarkmessagedisplaybottomright").html("");
                                    } else if (pos == 9) {
                                        console.log(str);
                                        $("#watermarkmessagedisplaybottomright").html(str);
                                        $("#watermarkmessagedisplaytopleft").html("");
                                        $("#watermarkmessagedisplaytop").html("");
                                        $("#watermarkmessagedisplaytopright").html("");
                                        $("#watermarkmessagedisplaycenterleft").html("");
                                        $("#watermarkmessagedisplaycenter").html("");
                                        $("#watermarkmessagedisplaycenterright").html("");
                                        $("#watermarkmessagedisplaybottomleft").html("");
                                        $("#watermarkmessagedisplaybottom").html("");
                                    }
                                    $("#Preview_modal").modal("show");
                                } else {
                                    $("#watermarkerror").text(data);
                                }
                            }
                    );
                }
            }

            $("#deletewatermark").on("click", function () {
                $("#confirm_delete").modal("show");
            });
            $("#Confirm").on("click", function () {
                var checkedData = [];
                $("#tBody").find("input[type='checkbox'][name='watermarkId']:checked").each(function () {
                    checkedData.push($(this).val());
                });
                if (checkedData === null || checkedData.length === 0) {
                    $("#watermarkerror").text("Please Select a Watermark");
                    $("#confirm_delete").modal("hide");
                } else {
                    console.log(checkedData);
                    var Uname = "${user.userName}".replace(" ", "");
                    $.get("${pageContext.request.contextPath}/"+Uname+"/deletewatermark", {watermarkIds: checkedData},
                            function (data, status) {
                                if (data.indexOf("success") > -1) {
                                    var res = data.split("#")[1];
                                    $("#response").text(res);
                                    setTimeout( function()
                                    {
                                        $("#confirm_delete").modal("hide");
                                        window.location.reload();
                                    }, 5000);
                                } else {
                                    $("#response").text(data);
                                }
                            });
                }
            });

            $("#defaultStatus").on("click", function () {
                if ($("#defaultStatus").is(":checked")) {
                    console.log("set default");
                    $('.textarea').ckeditorGet().setReadOnly(true);
                    $('#cke_watermarkMessage').find('.cke_wysiwyg_frame').contents().find('body').attr("contenteditable", "true");
                    $('#cke_watermarkMessage').find('.cke_wysiwyg_frame').contents().find('body').each(function () {
                        $(this).find('');
                    });
                    $('#cke_watermarkMessage').find('.cke_wysiwyg_frame').contents().find('p').each(function () {
//                        $(this).parent().is("<em>")
                        $(this).find('span').each(function () {
                            $(this).removeAttr("style");
                            $(this).css("font-size", "18px");
                            $(this).css("font", "arial");
                            $(this).css("font-style", "normal");
                            $(this).css("font-weight", "normal");
                        });
                        $(this).find('em').each(function () {
                            $(this).removeAttr("style");
                            $(this).css("font-size", "18px");
                            $(this).css("font", "arial");
                            $(this).css("font-style", "normal");
                            $(this).css("font-weight", "normal");
                        });
                        $(this).find('strong').each(function () {
                            $(this).removeAttr("style");
                            $(this).css("font-size", "18px");
                            $(this).css("font", "arial");
                            $(this).css("font-style", "normal");
                            $(this).css("font-weight", "normal");
                        });
                        $(this).removeAttr("style");
                        $(this).css("font-size", "18px");
                        $(this).css("font", "arial");

                    });
                } else {
                    $('.textarea').ckeditorGet().setReadOnly(false);
                }
            });
            $("#defaultupdateStatus").on("click", function () {
                if ($("#defaultupdateStatus").is(":checked")) {
                    console.log("set default update");
                    $('.textarea1').ckeditorGet().setReadOnly(true);
                    $('#cke_newwatermarkMessage').find('.cke_wysiwyg_frame').contents().find('body').attr("contenteditable", "true");
                    $('#cke_newwatermarkMessage').find('.cke_wysiwyg_frame').contents().find('p').each(function () {
                        $(this).find('span').each(function () {
                            $(this).removeAttr("style");
                            $(this).css("font-size", "18px");
                            $(this).css("font", "arial");
                            $(this).css("font-style", "normal");
                            $(this).css("font-weight", "normal");
                        });
                        $(this).find('em').each(function () {
                            $(this).removeAttr("style");
                            $(this).css("font-size", "18px");
                            $(this).css("font", "arial");
                            $(this).css("font-style", "normal");
                            $(this).css("font-weight", "normal");
                        });
                        $(this).find('strong').each(function () {
                            $(this).removeAttr("style");
                            $(this).css("font-size", "18px");
                            $(this).css("font", "arial");
                            $(this).css("font-style", "normal");
                            $(this).css("font-weight", "normal");
                        });
                        $(this).removeAttr("style");
                        $(this).css("font-size", "18px");
                        $(this).css("font", "arial");
                    });
                } else {
                    $('.textarea1').ckeditorGet().setReadOnly(false);
                }
            });
            
            $("#previewatcreate").on("click", function () {
                $("#previewatcreate").hide();
                $("#edittextwhencreate").show();
                $('.textarea').ckeditorGet().setReadOnly(true);
                $("#watermarkName").prop("disabled", true);
                $("#position").prop("disabled", true);
                $("#defaultStatus").prop("disabled", true);
                $("#addWatermark").prop("disabled", true);
                $("#resetcreatewatermark").prop("disabled", true);
            });

            $("#edittextwhencreate").on("click", function () {
                $("#previewatcreate").show();
                $("#edittextwhencreate").hide();
                $('.textarea').ckeditorGet().setReadOnly(false);
                $("#watermarkName").prop("disabled", false);
                $("#position").prop("disabled", false);
                $("#defaultStatus").prop("disabled", false);
                $("#addWatermark").prop("disabled", false);
                $("#resetcreatewatermark").prop("disabled", false);
            });

            $("#previewatupdate").on("click", function () {
                $("#previewatupdate").hide();
                $("#edittextwhenupdate").show();
                $('.textarea1').ckeditorGet().setReadOnly(true);
                $("#newwatermarkName").prop("disabled", true);
                $("#newposition").prop("disabled", true);
                $("#defaultupdateStatus").prop("disabled", true);
                $("#updateWatermark").prop("disabled", true);
                $("#resetupdatewatermark").prop("disabled", true);
            });

            $("#edittextwhenupdate").on("click", function () {
                $("#previewatupdate").show();
                $("#edittextwhenupdate").hide();
                $('.textarea1').ckeditorGet().setReadOnly(false);
                $("#newwatermarkName").prop("disabled", false);
                $("#newposition").prop("disabled", false);
                $("#defaultupdateStatus").prop("disabled", false);
                $("#updateWatermark").prop("disabled", false);
                $("#resetupdatewatermark").prop("disabled", false);
            });

            
        </script>
    </body>
</html>
