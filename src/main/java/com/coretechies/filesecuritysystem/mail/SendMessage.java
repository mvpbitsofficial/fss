/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.mail;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
/**
 *
 * @author CoreTechies M8
 */
public class SendMessage {
    public SendMessage(){
        System.out.println("Send Message Constructor");
    }
    public void sendMultipleMessages(String mobilenumbers, String text) throws UnsupportedEncodingException{
        
            //Your authentication key
            String authkey = "2790AKboy6n4KUyW58086bd3";
            //Multiple mobiles numbers separated by comma
            String mobiles = mobilenumbers;
            //Sender ID,While using route4 sender id should be 6 characters long.
            String senderId = "Tecnox";
            //Your message to send, Add URL encoding here.
            String message = text;
            //define route
            String route="default";

            //Prepare Url
            URLConnection myURLConnection;
            URL myURL;
            BufferedReader reader;

            //encoding message
            String encoded_message = URLEncoder.encode(message, "UTF-8");

            //Send SMS API
            String mainUrl="http://signup.bulksms.bz/api/sendhttp.php?";

            //Prepare parameter string
            StringBuilder sbPostData= new StringBuilder(mainUrl);
            sbPostData.append("authkey="+authkey);
            sbPostData.append("&mobiles="+mobiles);
            sbPostData.append("&message="+encoded_message);
            sbPostData.append("&route="+route);
            sbPostData.append("&sender="+senderId);

            //final string
            mainUrl = sbPostData.toString();
            System.out.println("mainURL="+mainUrl);
            try
            {
                //prepare connection
                myURL = new URL(mainUrl);
                myURLConnection = myURL.openConnection();
                myURLConnection.connect();
                reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
                //reading response
                String response;
                while ((response = reader.readLine()) != null)
                //print response
                System.out.println(response);

                //finally close connection
                reader.close();
            }
            catch (IOException e)
            {
                    e.printStackTrace();
            }
    }
}
