
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>FSS | Create License</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
        <style>
            .input-daterange .input-group-addon{
                padding: 6px 12px;
                width: 1%;
            }
        #other-email-section {
                display: none;
        }

        .number-dropdown{ width:200px;} 
        .number-dropdown .btn-group{width:100%;}
        .number-dropdown .btn-group label{ width:calc(100%/5);}

        .number-dropdown .btn-group label.btn-default.active{ border-color:#e7505a; background:#e7505a; color:#000;}
        .number-dropdown .btn-group .btn+.btn{ margin-left:0px;}
        .form-wizard .steps > li.done > a.step .desc i{display: none !important;}
        </style>
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top"> 
            <!-- BEGIN HEADER INNER -->
            <jsp:include page="topbar.jsp" />
            <!-- END HEADER INNER --> 
        </div>
        <!-- END HEADER --> 
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER --> 
        <!-- BEGIN CONTAINER -->
        <div class="page-container"> 
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper"> 
                <!-- BEGIN SIDEBAR --> 
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse"> 
                    <!-- BEGIN SIDEBAR MENU --> 
                    <jsp:include page="sidebar.jsp" />
<!--                    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                         DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
                        <li class="sidebar-toggler-wrapper hide"> 
                             BEGIN SIDEBAR TOGGLER BUTTON 
                            <div class="sidebar-toggler"> <span></span> </div>
                             END SIDEBAR TOGGLER BUTTON  
                        </li>
                        <li class="nav-item  "> <a href="dashboard" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>

                        </li>
                        <li class="nav-item  active"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">License</span>  </a>

                        </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>

                        </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
                    </ul>-->
                    <!-- END SIDEBAR MENU --> 
                    <!-- END SIDEBAR MENU --> 
                </div>
                <!-- END SIDEBAR --> 
            </div>
            <!-- END SIDEBAR --> 
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper"> 
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content"> 
                   <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light portlet-fit portlet-form  portlet-datatable transparet_bg">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-key "></i>
                                        <span class="caption-subject  sbold uppercase">Generate License</span>
                                    </div>
                                   
                                </div>
                                <div class="portlet-body clearfix">
                                    <div class="col-md-12 clearfix" style="margin: auto; float: none;">
                                        <form class="create_licence_form  form-horizontal">
                                        
                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                              <h4 class="panel-title">
                                                <div id="ribbon">
                                                    <span id="content"><i class="fa fa-check"></i></span>
                                                </div>
                                                <b>Company Details</b>
                                              </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                <div class="panel-body">
                                                    <div class="form-group clearfix">
                                                        <label class="col-sm-4 control-label"> Client</label>
                                                        <div class="col-sm-7">
                                                            <div class="input-group" id="client_div">
                                                                <span class="input-group-addon input-circle-left">
                                                                    <i class="fa fa-user"></i>
                                                                </span>
                                                                <select class="form-control input-circle-right selectpicker" data-live-search='true' id="clientuserid">
                                                                    <option selected disabled>Select Client</option>
                                                                    <c:forEach items="${clientusers}" var="client">
                                                                        <option value="${client.userInfoId}">${client.userName}</option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                            <span style="color: red; display: block; font-size: 14px;" id="errorselectclient" ></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group clearfix">
                                                        <label class="col-sm-4 control-label"> Organization</label>
                                                        <div class="col-sm-7">
                                                            <div class="input-group">
                                                                <span class="input-group-addon input-circle-left">
                                                                    <i class="fa fa-building-o"></i>
                                                                </span>
                                                                <input type="text" readonly class="form-control input-circle-right" placeholder="Organization" id="clientorganizationid" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group clearfix">
                                                        <label class="col-sm-4 control-label">System ID</label>
                                                        <div class="col-sm-7">
                                                            <div class="one">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon input-circle-left">
                                                                        <i class="fa fa-desktop"></i>
                                                                    </span>
                                                                    <input class="form-control input-circle-right" placeholder=" System ID" id="systemId">
                                                                </div>
                                                                <span style="color: red; display: block; font-size: 14px;" id="errorsystemid" ></span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="form-group clearfix">
                                                        <a class="collapsed btn btn-circle btn-success" id="companyDetails" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapseTwo" aria-controls="collapseTwo" style="float: right;">Next</a>
                                                    </div>  
                                                </div>
                                            </div>
                                          </div>
                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingTwo">
                                              <h4 class="panel-title">
                                                <div id="ribbon">
                                                    <span id="content"><i class="fa fa-check"></i></span>
                                                </div>
                                                <b>License Validity </b>
                                              </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                              <div class="panel-body">
                                              	<div class="form-group clearfix">
                                                <label class="col-sm-4 control-label"> Validity</label>
                                                <div class="one">
                                                    <div class="col-sm-7">
                                                        <div class="input-group" id="validity_div">
                                                        <span class="input-group-addon input-circle-left">
                                                            <i class="fa fa-star"></i>
                                                        </span>
                                                      <div class="dropdown">
                                                        <button class="form-control input-circle-right dropdown-toggle" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <span class="status pull-left"></span> <span class="caret pull-right" style="margin-top:5px;"></span> </button>
                                                        <div class="dropdown-menu number-dropdown" aria-labelledby="dLabel" style="width: 250px;">
                                                         <div class="btn-group" data-toggle="buttons" id="radiobuttons">
                                                          <label class="btn btn-default valid" id="1">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           1 </label>
                                                          <label class="btn btn-default valid" id="2">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           2 </label>
                                                          <label class="btn btn-default valid" id="3">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           3 </label>
                                                          <label class="btn btn-default valid" id="4">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           4 </label>
                                                          <label class="btn btn-default valid" id="5">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           5 </label>
                                                          <label class="btn btn-default valid " id="6">
                                                           <input type="radio" name="options" autocomplete="off" >
                                                           6 </label>
                                                          <label class="btn btn-default valid" id="7">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           7 </label>
                                                          <label class="btn btn-default valid" id="8">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           8 </label>
                                                          <label class="btn btn-default valid" id="9">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           9 </label>
                                                          <label class="btn btn-default valid" id="10">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           10 </label>
                                                          <label class="btn btn-default valid " id="11">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           11 </label>
                                                          <label class="btn btn-default valid" id="12">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           12 </label>
                                                          <label class="btn btn-default valid" id="13">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           13 </label>
                                                          <label class="btn btn-default valid" id="14">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           14 </label>
                                                          <label class="btn btn-default valid" id="15">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           15 </label>
                                                          <label class="btn btn-default valid " id="16">
                                                           <input type="radio" name="options" autocomplete="off" >
                                                           16 </label>
                                                          <label class="btn btn-default valid" id="17">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           17 </label>
                                                          <label class="btn btn-default valid" id="18">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           18 </label>
                                                          <label class="btn btn-default valid" id="19">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           19 </label>
                                                          <label class="btn btn-default valid" id="20">
                                                           <input type="radio" name="options" autocomplete="off">
                                                           20 </label>
                                                        </div>
                                                            <input type="hidden" id="validity" />
                                                        <div style="text-align:left; padding-bottom:5px;">
                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="margin-top: 8px; margin-left: 8px;">
                                                                <input type="hidden" id="validity" />
                                                            <input type="checkbox" class="group-checkable" name="options" id="lifetime" value="0"/>
                                                            <span></span>LifeTime</label>
                                                        </div>
                                                        </div>
                                                       </div>
                                                    </div>
                                                    <span style="color: red; display: block; font-size: 14px;" id="errorvalidity" ></span>
                                                    </div>
                                                </div>
                                            </div> 
                                                <div class="form-group clearfix date-picker input-daterange"  data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                                    <label class="col-sm-4 control-label"> Issue Date</label>
                                                    <div class="col-sm-7">
                                                        <div class="input-group">
                                                            <span class="input-group-addon input-circle-left">
                                                                <i class="fa fa-calendar"></i>
                                                            </span>
                                                            <input class="form-control input-circle-right" placeholder=" Issue Date" id="startDate" value="${today}" style="text-align: left;">
                                                        </div>
                                                        <span style="color: red; display: block; font-size: 14px;" id="errorstartdate" ></span>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix date-picker input-daterange" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                                    <label class="col-sm-4 control-label"> Expiry Date</label>
                                                    <div class="col-sm-7">
                                                        <div class="input-group" >
                                                            <span class="input-group-addon input-circle-left">
                                                                <i class="fa fa-calendar"></i>
                                                            </span>
                                                            <input class="form-control input-circle-right" placeholder=" Expiry Date" id="endDate" style="text-align: left;">
                                                        </div>
                                                        <span style="color: red; display: block; font-size: 14px;" id="errorenddate" ></span>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix"> 
                                                    <a class="collapsed btn btn-circle btn-success" id="licenseValidity" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapseThree" aria-controls="collapseThree" style="float: right;">Next</a>
                                                    <a class="collapsed btn btn-circle btn-danger" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapseOne" aria-controls="collapseOne" style="float: right; margin-right: 10px;">Previous</a>
                                                </div>  
                                              </div>
                                            </div>
                                          </div>
                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingThree">
                                              <h4 class="panel-title">
                                                <div id="ribbon">
                                                    <span id="content"><i class="fa fa-check"></i></span>
                                                </div>
                                                <b>License  Limit </b>
                                              </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                <div class="panel-body">
                                                    <div class="form-group clearfix">
                                                        <label class="col-sm-4 control-label">Web Limit</label>
                                                        <div class="one-half col-sm-7">
                                                            <div class="two" style="float:right">
                                                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="margin-top: 8px;">
                                                                    <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes"  id="webunlimited"/>
                                                                    <span></span>
                                                                </label>

                                                            </div>
                                                            <div class="one" style="float:left">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon input-circle-left">
                                                                        <i class="fa fa-hand-paper-o"></i>
                                                                    </span>
                                                                    <input class="form-control input-circle-right" placeholder=" License Limit for web" id="licenselimitweb">
                                                                </div>
                                                                <div class="hint text-right">(please click checkbox for unlimited license)</div>
                                                                <span style="color: red; display: block; font-size: 14px;" id="errorweblimit" ></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                            
                                                    <div class="form-group clearfix">
                                                        <label class="col-sm-4 control-label">Desktop  Limit</label>
                                                        <div class="one-half col-sm-7">
                                                            <div class="two" style="float:right">
                                                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="margin-top: 8px;">
                                                                    <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes"  id="desktopunlimited"/>
                                                                    <span></span>
                                                                </label>

                                                            </div>
                                                            <div class="one" style="float:left">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon input-circle-left">
                                                                        <i class="fa fa-hand-paper-o"></i>
                                                                    </span>
                                                                    <input class="form-control input-circle-right" placeholder=" License Limit for desktop" id="licenselimitdesktop">
                                                                </div>
                                                                <div class="hint text-right">(please click checkbox for unlimited license)</div>
                                                                <span style="color: red; display: block; font-size: 14px;" id="errordesktoplimit" ></span>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group clearfix">
                                                        <label class="col-sm-4 control-label">Total Limit</label>
                                                        <div class="col-sm-7">
                                                            <div class="one">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon input-circle-left">
                                                                        <i class="fa fa-hand-paper-o"></i>
                                                                    </span>
                                                                    <input class="form-control input-circle-right" readonly placeholder=" Total License Limit" id="totallimitdisplay">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group clearfix"> 
                                                        <a class="collapsed btn btn-circle btn-success" id="licenseLimit" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapseFour" aria-controls="collapseFour" style="float: right;">Next</a>
                                                        <a class="collapsed btn btn-circle btn-danger" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapseTwo" aria-controls="collapseTwo" style="float: right; margin-right: 10px;">Previous</a>
                                                    </div> 
                                                </div>
                                            </div>
                                          </div>
                                          
                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingFour">
                                              <h4 class="panel-title">
                                                <div id="ribbon">
                                                    <span id="content"><i class="fa fa-check"></i></span>
                                                </div>
                                                <b>Course</b>
                                              </h4>
                                            </div>
                                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                              <div class="panel-body">
                                               <div class="form-group clearfix">
                                                <label class="col-sm-4 control-label">Total Courses</label>
                                                <div class="one-half col-sm-7">
                                                    <div class="two" style="float:right">
                                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="margin-top: 8px;">
                                                            <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes"  id="unlimitedcourse"/>
                                                            <span></span>
                                                        </label>
                                                        
                                                    </div>
                                                    <div class="one" style="float:left">
                                                        <div class="input-group">
                                                            <span class="input-group-addon input-circle-left">
                                                                <i class="fa fa-book"></i>
                                                            </span>
                                                            <input class="form-control input-circle-right" placeholder="Upload Course limit" id="courselimit">
                                                        </div>
                                                        <div class="hint text-right">(please click checkbox for unlimited course)</div>
                                                        <span style="color: red; display: block; font-size: 14px; margin-left: 0px;" id="errorcourselimit" ></span>
                                                    </div>
                                                </div>  
                                            </div> 
                                            
                                            <div class="form-group clearfix">
                                                <label class="col-sm-4 control-label">Course setting</label>
                                                <div class="one-half col-sm-7">
                                                
                                                    <div class="one" >
                                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="margin-top: 8px;">
                                                            <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes"  id="excelupload"/>
                                                            <span></span>
                                                        </label>
                                                        <span>Excel Upload</span>
                                                    </div>
                                                    <div class="one">
                                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="margin-top: 8px;">
                                                            <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes"  id="backupfileupload"/>
                                                            <span></span>
                                                        </label>
                                                        <span>Backup File Upload</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <span style="color: red; display: block; font-size: 14px; margin-left: 220px; margin-bottom: 10px;" id="errorcoursesetting" ></span>
                                            <div class="form-group clearfix"> 
                                                <a class="collapsed btn btn-circle btn-success" id="course" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapseFive" aria-controls="collapseFive" style="float: right;">Next</a>
                                                <a class="collapsed btn btn-circle btn-danger" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapseThree" aria-controls="collapseThree" style="float: right; margin-right: 10px;">Previous</a>
                                            </div>
                                            </div>
                                            </div>
                                          </div>
                                          
                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingFive">
                                              <h4 class="panel-title">
                                                <div id="ribbon">
                                                    <span id="content"><i class="fa fa-check"></i></span>
                                                </div>
                                                <b>SMS</b>
                                              </h4>
                                            </div>
                                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                              <div class="panel-body">
                                               	<div class="form-group clearfix">
                                                    <label class="col-sm-4 control-label">SMS Transactional</label>
                                                    <div class="one">
                                                        <div class="col-sm-7">
                                                            <div class="input-group">
                                                                <span class="input-group-addon input-circle-left">
                                                                    <i class="fa fa-envelope"></i>
                                                                </span>
                                                                <input class="form-control input-circle-right" placeholder="Total allowed SMS Transactional " id="smslimit">
                                                            </div>
                                                            <span style="color: red; display: block; font-size: 14px;" id="errorsmslimit" ></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                            <div class="form-group clearfix">
                                                <label class="col-sm-4 control-label">SMS Promotional</label>
                                                <div class="one">
                                                    <div class="col-sm-7">
                                                        <div class="input-group">
                                                            <span class="input-group-addon input-circle-left">
                                                                <i class="fa fa-envelope"></i>
                                                            </span>
                                                            <input class="form-control input-circle-right" placeholder="Total allowed SMS Promotional " id="promotionallimit">
                                                        </div>
                                                        <span style="color: red; display: block; font-size: 14px;" id="errorpromotionallimit" ></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix"> 
                                                <a class="btn btn-circle btn-success" id="generateLicense" style="float: right; margin-left: 10px;"><i class="fa fa-key"></i> Generate License</a>
                                                <a class="collapsed btn btn-circle btn-danger" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapseFour" aria-controls="collapseFour" style="float: right;">Previous</a>
                                            </div>
                                            
                                            </div>
                                            </div>
                                          </div>
                                          
                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingSix">
                                              <h4 class="panel-title">
                                                <div id="ribbon">
                                                    <span id="content"><i class="fa fa-check"></i></span>
                                                </div>
                                                <b>OTP</b>
                                              </h4>
                                            </div>
                                            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                              <div class="panel-body">
                                               	<div class="form-group clearfix">
                                                    <label class="col-sm-4 control-label">License Key</label>
                                                    <div class="two-half col-sm-7">
                                                        <div class="one input-group">
                                                            <span class="input-group-addon input-circle-left">
                                                                <i class="fa fa-credit-card"></i>
                                                            </span>
                                                            <input readonly class="form-control input-circle-right" placeholder=" License Key" id="licenceKey"/>
                                                        </div>
                                                        <div class="two">
                                                            <a id="copy" class="btn  btn-default btn-circle"><i class="fa fa-copy"></i></a>
                                                            <a href="javascript:;" id="keydownload" class="btn  btn-default btn-circle"><i class="fa fa-download"></i></a>
                                                        </div>
                                                    </div> 
                                                </div>
                                                <span style="color: red; display: block; font-size: 14px; margin-left: 225px;" id="responsedisplay" ></span>
                                                <span style="color: green; display: block; font-size: 14px; margin-left: 225px;" id="responsesuccess" ></span>
                                              </div>
                                            </div>
                                          </div>
                                          
                                        </div>
                                        <div id="loading" style="display: none;">
                                            <center>
                                                <!--<span style="color: blue;">Processing...</span>-->
                                                <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/24.gif" />
                                            </center>
                                        </div>
<!--                                        <div class="form-group clearfix text-right" id="button">
                                            <a class="btn btn-circle btn-success" id="generateLicense"><i class="fa fa-key"></i> Generate License</a>
                                        </div>-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>               
                    </div>


                </div>
                <!-- END CONTENT BODY -->  
            </div>
            <!-- END CONTENT --> 
            <!-- BEGIN QUICK SIDEBAR --> 
            <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 

            <!-- END QUICK SIDEBAR --> 
        </div>
        <!-- END CONTAINER --> 
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; Organization </div>
            <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
        </div>
        <!-- END FOOTER --> 
        <div class="modal fade " id="otpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm text-center" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Send OTP</h4>
                    </div>
                    <div class="modal-body">
                        <div class="mt-radio-inline">
                            <label class="mt-radio">
                                <input type="radio" name="sendotp" class="radiovalue" value="1" checked=""> Email
                                <span></span>
                            </label>
                            <label class="mt-radio">
                                <input type="radio" name="sendotp" class="radiovalue" value="2" > Mobile
                                <span></span>
                            </label>			
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align:center;">
                      <button type="button" class="btn btn-success btn-circle" data-dismiss="modal" data-toggle="modal" data-target="" id="createlicense"> Send <i class="fa fa-angle-right"></i> </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade " id="otpModal_text" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm text-center" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Confirm OTP</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="random"/>
                    <input type="hidden" id="licenseId" />
                  <input type="text" class="form-control input-circle" id="otp"/>
                  <span style="font-size: smaller; font-weight:bold;  color: red" id="otpError" ></span>
                </div>
                <div class="modal-footer" style="text-align:center;">
                  <button type="button" class="btn btn-success btn-circle otp_btn" id="submitotp"> Continue <i class="fa fa-angle-right"></i> </button>
                  <button class="btn btn-default btn-circle otp_btn" type="button" id="resendotp">Resend OTP</button>
                </div>
              </div>
            </div>
          </div>

       
        <!-- BEGIN CORE PLUGINS --> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/scripts/bootstrap-select.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
        <!-- END CORE PLUGINS --> 
        <!-- BEGIN THEME GLOBAL SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
        <!-- END THEME GLOBAL SCRIPTS --> 
        <!-- BEGIN THEME LAYOUT SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
        <!-- END THEME LAYOUT SCRIPTS --> 
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="${pageContext.request.contextPath}/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="${pageContext.request.contextPath}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
        <script>
        $( ".form-control" ).focus(function() {
            $(this).prev('.input-group-addon').removeClass().addClass('input-group-addon-focus');
            $(this).next('.input-group-addon').removeClass().addClass('input-group-addon-focus');
        });

        $( ".form-control" ).focusout(function() {
            $(this).prev('.input-group-addon-focus').removeClass().addClass('input-group-addon');
            $(this).next('.input-group-addon-focus').removeClass().addClass('input-group-addon');
        });
        $('#client_div').on('mouseenter',function(){
            $(this).find('.input-group-addon').removeClass('input-group-addon').addClass('input-group-addon-focus');
            $('.selectpicker ').css('border-color','#e7505a');
        }).on('mouseleave',function(){
            $(this).find('.input-group-addon-focus ').removeClass('input-group-addon-focus').addClass('input-group-addon');$('.selectpicker ').css('border-color','#c2cad8');
        });
        $('#validity_div').on('mouseenter',function(){
            $(this).find('.input-group-addon').removeClass('input-group-addon').addClass('input-group-addon-focus');
            $('.selectpicker ').css('border-color','#e7505a');
        }).on('mouseleave',function(){
            $(this).find('.input-group-addon-focus ').removeClass('input-group-addon-focus').addClass('input-group-addon');$('.selectpicker ').css('border-color','#c2cad8');
        });
        $(".number-dropdown label").on('mouseenter', function(){
            $(this).css('background','#ADD8E6');
        }).on('mouseleave', function(){
            $(this).css('background','#ffffff');
        });
        </script>
        <script>
            $("#lifetime").on("click", function(){
                if($("#lifetime").is(":checked")){
                    $("#validity").val("");
                    $("#endDate").val("");
                    $("#radiobuttons").attr("disabled", "disabled").off('click');
                    $("#startDate").prop( "disabled", true );
                    $("#endDate").prop( "disabled", true );
                    $("#errorstartdate").text("");
                    $("#errorenddate").text("");
                    $(".btn-default").removeClass("active");
                } else{
                    $("#radiobuttons").prop( "disabled", false );
                    $("#startDate").prop( "disabled", false );
                    $("#endDate").prop( "disabled", false );
                    $(".status").css("display", "none");
                }
            });
            
            $("#endDate").on("change", function(){
                var endDate = $("#endDate").val();
                if(endDate === null || endDate === ""){
                    $("#errorenddate").text("Please Enter Expiry Date");
                    $(".status").css("display", "none");
                } else{
                    $("#errorenddate").text("");
                }     
            });
            $("#clientuserid").on("change", function(){
                var clientuserid = $("#clientuserid").select().val();
                if(clientuserid === null || clientuserid === ""){
                    $("#errorselectclient").text("Please Select Client");
                } else{
                    $("#errorselectclient").text("");
                }
            });
            $("#systemId").on("keyup", function(){
                $("#companyDetails").css("display", "none");
            });
            $("#systemId").on("change", function(){
                var systemId = $("#systemId").val();
                if(systemId === null || systemId === ""){
                    $("#errorsystemid").text("Please Enter System ID");
                } else{
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/checksystemid",{systemId: systemId},
                        function (data, status){
                            console.log(data);
                            if (data === "success"){
                                $("#errorsystemid").text("");
                                $("#companyDetails").css("display", "block");
                            } else{
                                $("#errorsystemid").text(data);
                            }
                        }
                    );
                }
            });
            $("#webunlimited").on("click", function(){
                if($("#webunlimited").is(":checked")){
                    $("#licenselimitweb").val("");
                    $("#errorweblimit").text("");
                    $("#licenselimitweb").prop( "disabled", true );
                    $("#totallimitdisplay").val("Unlimited");
                } else{
                    $("#licenselimitweb").prop( "disabled", false );
                    $("#totallimitdisplay").val("");
                }
            });
            $("#desktopunlimited").on("click", function(){
                if($("#desktopunlimited").is(":checked")){
                    $("#licenselimitdesktop").val("");
                    $("#errordesktoplimit").text("");
                    $("#licenselimitdesktop").prop( "disabled", true );
                    $("#totallimitdisplay").val("Unlimited");
                } else{
                    $("#licenselimitdesktop").prop( "disabled", false );
                    $("#totallimitdisplay").val("");
                }
            });
            $("#unlimitedcourse").on("click", function(){
                if($("#unlimitedcourse").is(":checked")){
                    $("#courselimit").val("");
                    $("#errorcourselimit").text("");
                    $("#courselimit").prop( "disabled", true );
                } else{
                    $("#courselimit").prop( "disabled", false );
                }
            });
            $("#licenselimitweb").on("change", function(){
                var weblicense = $("#licenselimitweb").val();
                var desktoplicense= $("#licenselimitdesktop").val();
                var numericExp =/^[0-9]+$/;
                if(weblicense === null || weblicense === ""){
                    $("#errorweblimit").text("Please Enter Web License Limit");
                }else if(!weblicense.match(numericExp)){
                    $("#errorweblimit").text("Invalid value for Web License Limit");
                } else{
                    $("#errorweblimit").text("");
                }
                if($("#webunlimited").is(":checked") || $("#desktopunlimited").is(":checked")){
                    $("#totallimitdisplay").val("Unlimited");
                } else{
                    $("#totallimitdisplay").val(+weblicense + +desktoplicense);
                }
            });
            $("#licenselimitdesktop").on("change", function(){
                var weblicense = $("#licenselimitweb").val();
                var desktoplicense= $("#licenselimitdesktop").val();
                var numericExp =/^[0-9]+$/;
                if(desktoplicense === null || desktoplicense === ""){
                    $("#errordesktoplimit").text("");
                } else if(!desktoplicense.match(numericExp)){
                    $("#errordesktoplimit").text("Invalid value for Desktop License Limit");
                } else{
                    $("#errordesktoplimit").text("");
                }
                if($("#webunlimited").is(":checked") || $("#desktopunlimited").is(":checked")){
                    $("#totallimitdisplay").val("Unlimited");
                } else{
                    $("#totallimitdisplay").val(+weblicense + +desktoplicense);
                }
            });
            $("#courselimit").on("change", function(){
                var courselimit= $("#courselimit").val();
                var numericExp =/^[0-9]+$/;
                if(courselimit === null || courselimit === ""){
                    $("#errorcourselimit").text("Please Enter course Limit");
                } else if(!courselimit.match(numericExp)){
                    $("#errorcourselimit").text("Invalid value for Course Limit");
                } else{
                    $("#errorcourselimit").text("");
                }
            });
            $("#smslimit").on("change", function(){
                var smslimit= $("#smslimit").val();
                var numericExp =/^[0-9]+$/;
                if(smslimit === null || smslimit === ""){
                    $("#errorsmslimit").text("Please Enter SMS transaction limit");
                } else if(!smslimit.match(numericExp)){
                    $("#errorsmslimit").text("Invalid value for SMS transaction Limit");
                } else{
                    $("#errorsmslimit").text("");
                }
            });
            $("#promotionallimit").on("change", function(){
                var promotionallimit= $("#promotionallimit").val();
                var numericExp =/^[0-9]+$/;
                if(promotionallimit === null || promotionallimit === ""){
                    $("#errorpromotionallimit").text("Please Enter SMS promotional limit");
                } else if(!promotionallimit.match(numericExp)){
                    $("#errorpromotionallimit").text("Invalid value for SMS promotional Limit");
                } else{
                    $("#errorpromotionallimit").text("");
                }
            });
            $("#companyDetails").on("click", function(e){
                var clientuserid = $("#clientuserid").select().val();
                var systemId = $("#systemId").val();
                var flag = true;
                if(clientuserid === null || clientuserid === ""){
                    console.log("Please Select Client");
                    $("#errorselectclient").text("Please Select Client");
                    flag = false;
                } else{
                    $("#errorselectclient").text("");
                }
                if(systemId === null || systemId === ""){
                    $("#errorsystemid").text("Please Enter System ID");
                    flag = false;
                } else{
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/checksystemid",{systemId: systemId},
                        function (data, status){
                            console.log(data);
                            if (data === "success"){
                                $("#errorsystemid").text("");
                            } else{
                                $("#errorsystemid").text(data);
                                flag = false;
                                console.log(flag);
                            }
                        }
                    );
                }
                console.log(flag);
                if(!flag){
                    e.stopImmediatePropagation();
                    return false;
                }
            });
            $("#licenseValidity").on("click", function(e){
                var startDate = $("#startDate").val();
                var endDate = $("#endDate").val();
                var lifetime = $("#lifetime").is(":checked");
                var flag = true;
                if(!lifetime){
                    if(startDate === null || startDate === ""){
                        $("#errorstartdate").text("Please Enter Issue Date");
                        flag = false;
                    } else{
                        $("#errorstartdate").text("");
                    }
                    if(endDate === null || endDate === ""){
                        $("#errorenddate").text("Please Enter Expiry Date");
                        flag = false;
                    } else{
                        $("#errorenddate").text("");
                    }
                }
                if(!flag){
                    e.stopImmediatePropagation();
                    return false;
                }
            });
            $("#licenseLimit").on("click", function(e){
                var webunlimited = $("#webunlimited").is(":checked");
                var desktopunlimited = $("#desktopunlimited").is(":checked");
                var weblicense= $("#licenselimitweb").val();
                var desktoplicense= $("#licenselimitdesktop").val();
                var numericExp=/^[0-9]+$/;
                var flag = true;
                if(!webunlimited){
                    if(weblicense === null || weblicense === ""){
                        $("#errorweblimit").text("Please Enter Web License Limit");
                        flag = false;
                    }else if(!weblicense.match(numericExp)){
                        $("#errorweblimit").text("Invalid value for Web License Limit");
                        flag = false;
                    } else{
                        $("#errorweblimit").text("");
                    }
                }
                if(!desktopunlimited){
                    if(desktoplicense === null || desktoplicense === ""){
//                        $("#errordesktoplimit").text("Please Enter Desktop License Limit");
//                        flag = false;
                    } else if(!desktoplicense.match(numericExp)){
                        $("#errordesktoplimit").text("Invalid value for Desktop License Limit");
                        flag = false;
                    } else{
                        $("#errordesktoplimit").text("");
                    }
                }
                if(!flag){
                    e.stopImmediatePropagation();
                    return false;
                }
            });
            $("#course").on("click", function(e){
                var unlimitedcourse = $("#unlimitedcourse").is(":checked");
                var courselimit= $("#courselimit").val();
                var excelupload = $("#excelupload").is(":checked");
                var backupfileupload = $("#backupfileupload").is(":checked");
                var numericExp=/^[0-9]+$/;
                var flag=true;
                if(!unlimitedcourse){
                    if(courselimit === null || courselimit === ""){
                        $("#errorcourselimit").text("Please Enter course Limit");
                        flag = false;
                    } else if(!courselimit.match(numericExp)){
                        $("#errorcourselimit").text("Invalid value for Course Limit");
                        flag = false;
                    } else{
                        $("#errorcourselimit").text("");
                    }
                }
                if(!excelupload && !backupfileupload){
                    console.log(excelupload);
                    console.log(backupfileupload);
                    $("#errorcoursesetting").text("Please check atleast one checkbox");
                    flag = false; 
                } else{
                    $("#errorcoursesetting").text("");
                }
                if(!flag){
                    e.stopImmediatePropagation();
                    return false;
                }
            });
            $("#generateLicense").on("click", function () {
                $("#errorsmslimit").text("");
                $("#errorpromotionallimit").text("");
                var smslimit= $("#smslimit").val();
                var promotionallimit= $("#promotionallimit").val();
                var numericExp=/^[0-9]+$/;
                var flag=true;
                
                if(smslimit === null || smslimit === ""){
                    $("#errorsmslimit").text("Please Enter SMS transaction limit");
                    flag = false;
                } else if(!smslimit.match(numericExp)){
                    $("#errorsmslimit").text("Invalid value for SMS transaction Limit");
                    flag = false;
                }
                if(promotionallimit === null || promotionallimit === ""){
                    $("#errorpromotionallimit").text("Please Enter SMS promotional limit");
                    flag = false;
                } else if(!promotionallimit.match(numericExp)){
                    $("#errorpromotionallimit").text("Invalid value for SMS promotional Limit");
                    flag = false;
                } 
                if(flag){
                    $("#otpModal").modal("show");
                }   
            });
            
            var issuedate = $('#startDate').datepicker({
                format: "dd-mm-yyyy"
            });
            var endDate = $('#endDate').datepicker({
                format: "dd-mm-yyyy"
            });

            issuedate.datepicker('setStartDate', new Date());
            issuedate.on('changeDate', function (e) {
                $('#endDate').val("");
                console.log(e);
                endDate.datepicker('setStartDate', e.date);
            }).datepicker("setDate", new Date());
            
            $("#createlicense").on("click", function () { 
                var clientuserid = $("#clientuserid").select().val();
                var validity = $("#validity").select().val();
                var startDate = $("#startDate").val();
                var endDate = $("#endDate").val();
                var systemId = $("#systemId").val();
                var webunlimited = $("#webunlimited").is(":checked");
                var desktopunlimited = $("#desktopunlimited").is(":checked");
                var weblicense= $("#licenselimitweb").val();
                var desktoplicense= $("#licenselimitdesktop").val();
                var unlimitedcourse = $("#unlimitedcourse").is(":checked");
                var courselimit = $("#courselimit").val();
                var smslimit= $("#smslimit").val();
                var promotionallimit= $("#promotionallimit").val();
                var excelupload = $("#excelupload").is(":checked");
                var backupfileupload = $("#backupfileupload").is(":checked");
                var coursesetting;
                var radioValue = $("input[name='sendotp']:checked"). val();
                var lifetime = $("#lifetime").is(":checked");
                
                if(excelupload){
                    coursesetting = 1;
                }
                if(backupfileupload){
                    coursesetting = 2;
                }
                if(excelupload && backupfileupload){
                    coursesetting = 3;
                }
                if(desktoplicense === null || desktoplicense === ""){
                    desktoplicense = 0;
                }
                var Uname = "${user.userName}".replace(" ", "");
                $("#loading").css('display','block');
                $.post("${pageContext.request.contextPath}/"+Uname+"/createlicense",
                        {clientuserid: clientuserid,
                            lifetime: lifetime,
                            validity: validity,
                            startDate: startDate,
                            endDate: endDate,
                            webunlimited: webunlimited,
                            desktopunlimited: desktopunlimited,
                            dekstoplicenseLimit: desktoplicense,
                            weblicenseLimit: weblicense,
                            unlimitedcourse: unlimitedcourse,
                            courselimit: courselimit,
                            courseSetting: coursesetting,
                            smsTransaction: smslimit,
                            smsPromotion: promotionallimit,
                            systemId: systemId,
                            sendOtpOn: radioValue},
                        function (data, status) {
                            if (data === "Please login again.") {
                                $("#loading").css("display","none");
                                window.location.href = "/";
                            } else if (data.indexOf("success") > -1) {
                                $("#loading").css("display","none");
                                $("#random").val(data.split("#")[1]);
                                $("#otpModal_text").modal("show");
                            } else {
                                $("#loading").css("display","none");
                                $("#collapseFive").removeClass("in");
                                $("#collapseSix").addClass("in");
                                $("#responsedisplay").text(data);
                            }
                });
            });
            $("#clientuserid").on("change", function(){
                var clientuserid = $("#clientuserid").select().val();
                if(clientuserid !== null || clientuserid !== ""){
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getorganizationofclient",{userId: clientuserid},
                        function (data, status){
                            console.log(data);
                            if (data.indexOf("success") > -1){
                                var organization= data.split("#")[1];
                                $("#clientorganizationid").val(organization);
                            } else{
                                $("#errorselectclient").text(data);
                            }
                        });
                }
            });
        </script>
        <script src="${pageContext.request.contextPath}/clipboard/clipboard.min.js"></script>
        <script>
            var clipboard = new Clipboard('#copy', {
                text: function () {
                    var licenseKey = $("#licenceKey").val();
                    if (licenseKey === null || licenseKey === "") {
                        $("#responsedisplay").text("No data available to copy.");
                    } else {
                        $("#responsesuccess").text("License Key has been copied successfully.");
                        return licenseKey;
                    }
                }
            });
            clipboard.on('success', function (e) {
                console.log(e);
            });
            clipboard.on('error', function (e) {
                console.log(e);
            });
            
            $("#keydownload").on("click", function(){
                        var license = $("#licenseId").val();
                        var licenseKey = $("#licenceKey").val();
                        if(licenseKey !== ""){
                            if(license !== null || license !== ""){
                                var Uname = "${user.userName}".replace(" ", "");
                                window.location.href = "${pageContext.request.contextPath}/"+Uname+"/downloadlicensekey?licenseId="+license;
                                $("#responsesuccess").text("File Download Successful");
                            }
                        } else{
                            $("#responsedisplay").text("No data available to Download.");
                        }
                    });
        </script>
        <script>
            $(document).on("click", "#submitotp", function () {
                $("#otpError").text("");
                $("#collapseFive").removeClass("in");
                $("#collapseSix").addClass("in");
                var otp = $("#otp").val();
                var random = $("#random").val();
                if (otp === "") {
                    $("#otpError").text("Please enter OTP");
                } else if (random === "") {
                   $("#otpError").text("Login again");
                   window.location.href = "/";
                } else {
                    $("#submitotp").text("processing...");
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/varifyotp", {otp: otp, random: random},
                            function (data, status) {
                                if (data === "Please login again.") {
                                    window.location.href = "/";
                                } else if (data.indexOf("success") > -1) {
                                    $("#licenceKey").val(data.split("#")[1]);
                                    $("#licenseId").val(data.split("#")[2]);
                                    $("#otpModal_text").modal("hide");
                                    $("#button").hide();
                                    $("#responsesuccess").text("License has been created successfully.");
                                } else {
                                    $("#submitotp").text("Continue");  
                                    $("#otpError").text(data);
                                }
                            });
                }
            });
            $(document).on("change", "#validity", function(){
                var valid = $("#validity").val();
                var Uname = "${user.userName}".replace(" ", "");
                $.get("${pageContext.request.contextPath}/"+Uname+"/dates",
                        {validity: valid}, function (data, status){
                            if (data.indexOf("success") > -1){
                                var abc= data.split("#")[1];
                                
                                $("#startDate").val(abc.split("*")[0]);
                                $("#endDate").val(abc.split("*")[1]);
                            } else{
                                $("#errorenddate").text(data);
                            }
                        });
            });
            $(document).on("click", "#resendotp", function () {
                var random = $("#random").val();
                var radioValue = $("input[name='sendotp']:checked"). val();
                if (random === "") {
                    $("#otpError").text("Login again");
                } else {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/resendotp", {random: random, sendOtpOn: radioValue},
                            function (data, status) {
                                if (data === "Please login again.") {
                                    window.location.href = "/";
                                } else if (data.indexOf("success") > -1) {
                                    $("#random").val(data.split("#")[1]);
                                    $("#otpError").text("Otp Sent");
                                } else {
                                    $("#otpError").text(data);
                                }
                            });
                }
            });
        </script>
        <script>
            $(".number-dropdown label").click(function(){
                var selText = $(this).text();
                $(this).parents().find('.status').html(selText);
            });
            $(".valid").on("click", function(){
                $(".status").css("display", "block");
                var ID = this.id;
                if(ID !== null || ID !== ""){
                    $("#errorstartdate").text("");
                    $("#errorenddate").text("");
                    $("#lifetime").attr('checked', false);
                    $("#startDate").prop( "disabled", false );
                    $("#endDate").prop( "disabled", false );
                    var Uname = "${user.userName}".replace(" ", "");
                  $.get("${pageContext.request.contextPath}/"+Uname+"/dates",
                      {validity: ID}, function (data, status){
                        if (data.indexOf("success") > -1){
                            var abc= data.split("#")[1];
                            $("#validity").val(ID);
                            $("#startDate").val(abc.split("*")[0]);
                            $("#endDate").val(abc.split("*")[1]);
                        } else{
                            $("#errorenddate").text(data);
                        }
                  });
                }
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#side-menu').find('li').removeClass('active');
                $(document).find(".license").addClass('active');
                $("#errorenddate").text("");
            });
        </script>
    </body>
</html>