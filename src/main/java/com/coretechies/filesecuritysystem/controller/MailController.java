/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.controller;

import com.coretechies.filesecuritysystem.dao.MailSettingDao;
import com.coretechies.filesecuritysystem.domain.AdminLicense;
import com.coretechies.filesecuritysystem.domain.MailSetting;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.domain.UserLicense;
import com.coretechies.filesecuritysystem.security.AES;
import com.coretechies.filesecuritysystem.mail.SendMail;
import com.coretechies.filesecuritysystem.mail.SendMessage;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;

/**
 * The MailController class implements an application that sends a mail to the user whenever needed.
 * 
 * @Controller annotation represents that this is a java controller class.
 * 
 * @author Jaishree
 * @version 1.0
 * @since 2016-09-03
 */
@Controller
public class MailController {

    @Autowired
    private JavaMailSender mailSender;
    
    @Autowired
    private MailSettingDao mailSettingDao;
    
    private final AES aes = new AES();
    /**
     * Send an email to given email Id
     *
     * @param subject - email Subject
     * @param centerContent
     * @param email - Receiver client email
     * @param request - HttpServeletRequest
     * @param userInfo
     */
    public void sendEmail(String subject, String centerContent, String email, HttpServletRequest request, UserInfo userInfo) {

        String content;
        if(subject.equals("Account Deactivated")){
            content= clientDeactivateContent(centerContent, request, userInfo);
        } else if(subject.equals("Account Activated")){
            content = clientActivateContent(centerContent, request, userInfo); 
        } else if(subject.equals("Profile Updated")){
            content = UpdateContent(centerContent, request, userInfo);  
        } else if(subject.equals("Account Deleted")){
            content = clientDeleteContent(centerContent, request, userInfo); 
        } else{
            content = completeContent(centerContent, request);
        }
        try {
            SendMail sendMail = new SendMail(mailSender);
            sendMail.sendMailSingle(email, subject, content);
            System.out.println("sendMail===" + sendMail);
        } catch (Exception e) {
            System.out.println("sendMail");
            e.printStackTrace();
        }
    }
    public void sendEmailToClient(AdminLicense adminLicense, UserInfo userInfo, String subject, String password, HttpServletRequest request) {

        String validity;
        if(adminLicense.getExpiryDate() != null){
            validity =new SimpleDateFormat("dd-MM-yyyy").format(adminLicense.getIssueDate())+" to "+new SimpleDateFormat("dd-MM-yyyy").format(adminLicense.getExpiryDate());
        }else{
            validity = "Lifetime";
        }
        String desktoplimit;
        String weblimit;
        if(adminLicense.getDesktopLicenseLimit() == -1){
            desktoplimit = "Unlimited";
        } else{
            desktoplimit = adminLicense.getDesktopLicenseLimit().toString(); 
        }
        if(adminLicense.getWeblicenseLimit() == -1){
            weblimit = "Unlimited";
        } else{
            weblimit = adminLicense.getWeblicenseLimit()+"";
        }
        try{
            ServletContext servletContext = request.getSession().getServletContext();
            String appPath = servletContext.getRealPath("/");
            System.out.println("appPath = " + appPath);
            String fullPath = appPath.replace("\\", "/")+"license"+adminLicense.getAdminLicenseId()+".txt";    
            FileOutputStream fis = new FileOutputStream(fullPath);

            fis.write(("Desktop License key: "+adminLicense.getDesktopLicense()).getBytes());
            fis.close();
            System.out.println("File Path == "+ fullPath);
        }catch (Exception e){
            e.printStackTrace();
        }
        String content = clientLicenceContent(userInfo.getUserName(), weblimit, desktoplimit, userInfo.getMobileNumber(), validity , adminLicense.getDesktopLicense(), userInfo.getOrganization(), userInfo.getEmailId(), password, request, "license"+adminLicense.getAdminLicenseId()+".txt");
        System.out.println(adminLicense);
        try {
            System.out.println("Sending Mail..."+subject);
            SendMail sendMail = new SendMail(mailSender);
            sendMail.sendMailSingle(userInfo.getEmailId(), subject, content);
            System.out.println("sendMail to client==" + sendMail);
        } catch (Exception e) {
            System.out.println("sendMail");
            e.printStackTrace();
        }
    }
    
    public void sendRenewMailToClient(AdminLicense adminLicense, UserInfo userInfo, String subject, HttpServletRequest request) {

        String validity;
        if(adminLicense.getExpiryDate() != null){
            validity =new SimpleDateFormat("dd-MM-yyyy").format(adminLicense.getIssueDate())+" to "+new SimpleDateFormat("dd-MM-yyyy").format(adminLicense.getExpiryDate());
        }else{
            validity = "Lifetime";
        }
        String desktoplimit;
        String weblimit;
        if(adminLicense.getDesktopLicenseLimit() == -1){
            desktoplimit = "Unlimited"; 
        } else{
            desktoplimit = adminLicense.getDesktopLicenseLimit().toString();
        }
        
        if(adminLicense.getWeblicenseLimit() == -1){
            weblimit = "Unlimited";
        } else{
           weblimit = adminLicense.getWeblicenseLimit().toString();
        }
        String format1 = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss").format(new Date());
        try{
            ServletContext servletContext = request.getSession().getServletContext();
            String appPath = servletContext.getRealPath("/");
            System.out.println("appPath = " + appPath);
            String fullPath = appPath.replace("\\", "/")+"Renewlicense"+format1+"_"+adminLicense.getAdminLicenseId()+".txt";    
            FileOutputStream fis = new FileOutputStream(fullPath);

            fis.write(("Desktop License key: "+adminLicense.getDesktopLicense()).getBytes());
            fis.close();
            System.out.println("File Path == "+ fullPath);
        }catch (Exception e){
            e.printStackTrace();
        }
        String content = clientLicenceRenewContent(userInfo.getUserName(), weblimit, desktoplimit, userInfo.getMobileNumber(), validity , adminLicense.getDesktopLicense(), userInfo.getOrganization(), userInfo.getEmailId(), request, "Renewlicense"+format1+"_"+adminLicense.getAdminLicenseId()+".txt");
        System.out.println(adminLicense);
        try {
            System.out.println("Sending Mail..."+subject);
            SendMail sendMail = new SendMail(mailSender);
            sendMail.sendMailSingle(userInfo.getEmailId(), subject, content);
            System.out.println("sendMail to client==" + sendMail);
        } catch (Exception e) {
            System.out.println("sendMail");
            e.printStackTrace();
        }
    }
    
    public void sendEditMailToClient(AdminLicense adminLicense, UserInfo userInfo, String subject, HttpServletRequest request) {

        String validity;
        if(adminLicense.getExpiryDate() != null){
            validity =new SimpleDateFormat("dd-MM-yyyy").format(adminLicense.getIssueDate())+" to "+new SimpleDateFormat("dd-MM-yyyy").format(adminLicense.getExpiryDate());
        }else{
            validity = "Lifetime";
        }
        String desktoplimit;
        String weblimit;
        if(adminLicense.getDesktopLicenseLimit() == -1){
            desktoplimit = "Unlimited"; 
        } else{
            desktoplimit = adminLicense.getDesktopLicenseLimit().toString();
        }
        
        if(adminLicense.getWeblicenseLimit() == -1){
            weblimit = "Unlimited";
        } else{
           weblimit = adminLicense.getWeblicenseLimit().toString();
        }
        String format1 = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss").format(new Date());
        try{
            ServletContext servletContext = request.getSession().getServletContext();
            String appPath = servletContext.getRealPath("/");
            System.out.println("appPath = " + appPath);
            String fullPath = appPath.replace("\\", "/")+"Renewlicense"+format1+"_"+adminLicense.getAdminLicenseId()+".txt";    
            FileOutputStream fis = new FileOutputStream(fullPath);

            fis.write(("Desktop License key: "+adminLicense.getDesktopLicense()).getBytes());
            fis.close();
            System.out.println("File Path == "+ fullPath);
        }catch (Exception e){
            e.printStackTrace();
        }
        String content = clientLicenceEditContent(userInfo.getUserName(), weblimit, desktoplimit, userInfo.getMobileNumber(), validity , adminLicense.getDesktopLicense(), userInfo.getOrganization(), userInfo.getEmailId(), request, "Renewlicense"+format1+"_"+adminLicense.getAdminLicenseId()+".txt");
        System.out.println(adminLicense);
        try {
            System.out.println("Sending Mail..."+subject);
            SendMail sendMail = new SendMail(mailSender);
            sendMail.sendMailSingle(userInfo.getEmailId(), subject, content);
            System.out.println("sendMail to client==" + sendMail);
        } catch (Exception e) {
            System.out.println("sendMail");
            e.printStackTrace();
        }
    }
    
    public Boolean sendMail(String subject, String centerContent, String email, HttpServletRequest request, Integer userid) {

        String content = completeContent(centerContent, request);
        try {
            SendMail sendMail = new SendMail(mailSender);
            MailSetting mailSetting = mailSettingDao.getMailSettingsById(userid);
            if(mailSetting != null){
                String decryptedpassword = aes.decrypt(mailSetting.getSmtpPassword(), "0123456789abcdef");
//                Thread t = new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
                            sendMail.sendEmail(email, mailSetting.getMailFrom(), content, subject, mailSetting.getSmtpServer(), mailSetting.getMailPort(), decryptedpassword, mailSetting.getSmtpUser());
//                        } catch (UnsupportedEncodingException ex) {
//                            Logger.getLogger(MailController.class.getName()).log(Level.SEVERE, null, ex);
//                        }
//                    }
//                });
//                t.start();
                System.out.println("sendMail::::" + sendMail);
                return true;
            } else{
                System.out.println("Mail Can Not Sent");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * Complete mail content by adding this centerContent
     *
     * @param centerContent - center content of mail
     * @return Complete Content of mail
     */
    private String completeContent(String centerContent, HttpServletRequest request) {
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                + "<html>\n"
                + "<head>\n"
                + " <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n"
                + "        <title> Email </title>\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n"
                + "\n"
                + "<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>\n"
                + "<style>	\n"
                + "	.container{ width:50%; margin:0 auto; background-color:#f4f3f8; min-height:200px; font-family: 'Open Sans', sans-serif; color:#2a2a2a;}\n"
                + "	.box{ width:50%;  min-height:150px; float:left; margin-bottom:2px;} \n"
                + "	.order-detail{ background-color:#fff; border-radius:5px;}\n"
                + "	.box-header{ background-color:#c1d400; padding:10px; border-top-left-radius:5px;  border-top-right-radius:5px; }\n"
                + "	.box ul{ list-style:none; margin:0px; padding:0px;}\n"
                + "	.box ul li{ font-size:12px; padding:3px 8px; font-weight:600; color:#4a4a4a}\n"
                + "	.pickup table{ background:#253370; border-radius:5px;}\n"
                + "	.pickup table tr td{ text-align:center; width:1%; color:#fff; padding:5px; }\n"
                + "	.footer{ background-color:#111717; border-radius:5px; min-height:150px; margin-top:2px;}\n"
                + "	.green-bg{background:#c1d400; text-align:center; font-size:12px; padding:3px; margin-bottom:1px;}\n"
                + "	.green-bg:first-child{border-top-left-radius:5px;  border-top-right-radius:5px;}\n"
                + "	.green-bg:last-child{border-bottom-left-radius:5px;  border-bottom-right-radius:5px; font-size:10px;}\n"
                + "	.footer table tr td{ padding:0px 5px;}\n"
                + "	.footer table tr td ul{ list-style:none;}\n"
                + "	.footer table tr td ul li{ color:#b8b8b8; font-size:12px; padding:2px 0px;}\n"
                + "</style>\n"
                + "</head>\n"
                + "    \n"
                + " <body color=\"#ffffff\" style=\" margin:0px; padding:0px; border-top:6px solid #1394DE;border-bottom:6px solid #1394DE; line-height: 25px;\">\n"
                + "<table style=\"width:90%; margin:auto; float:none;\">\n"
                + " 	<tr>\n"
                + "    	<td style=\"background:#fff; padding:10px 0px;float:none;margin:auto;\"><img src=\"http://" + request.getServerName() + ":" + request.getServerPort() +request.getContextPath()+ "/assets/layouts/layout/img/logo.png \" alt=\"Logo\"/></td>\n" + "<td style=\"text-align:right;\">\n"
                + " \n"
                + "    </tr>\n"
                + " </table>\n"
                + "	\n"
                + "	\n"
                + "	\n"
                + "    	\n"
                + "   \n"
                + "  <section style=\"width:100%; min-height:200px; color:#2a2a2a;\">\n"
                + "   <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\" style=\"background-color:#f6f2f1; margin:auto; float:none; margin-top:10px; padding-top:10px; padding-bottom:10px; margin-bottom:80px; font-weight:500;\">\n"
                + "     <tr>\n"
                + "      <td style=\"font-family:sans-serif; font-size: 14px; text-align:left; padding-left:20px; padding-right:20px;\">\n"
                + "       <h3 style=\"color:#253372;\"></h3>\n"
                + "            <span style=\"display: block; padding: 15px; background: #ededed; margin-bottom: 15px; border: 1px solid #dedede;\" >\n"
                +               centerContent
                + "            </span>\n"
                + "     <div>\n" 
                + "     <p style=\"text-align:left; font-size:12px; color:#666;\">\n" 
                + "        <span style=\"display:block\">Regards</span>\n" 
                + "        	Technox Consulting & IT Servises  \n" 
                + "        </p>\n" 
                + "     </div>\n" 
                + "     <div style=\"font-size:12px; background: #f1f1f1;margin: 0px -20px -20px; padding:2px 5px 10px; border-top:1px solid #dedede; text-align:center;\">\n" 
                + "     	<p style=\"margin-bottom:0px;\">\n" 
                + "        	<a href=\"javascript:;\" style=\"display:block; text-decoration:none; color:#d70000;\">www.technox.in</a>\n" 
                + "        	<span style=\"display:block;color:#666;\">All Right reservred , 2016,  Technox</span>\n" 
                + "        	<a href=\"javascript:;\" style=\"display:block; text-decoration:none; color:#d70000;\">support@technox.in</a>\n" 
                + "        </p>\n" 
                + "     </div>\n"
                + "        </td>\n"
                + "      </tr>\n"
                + "     </table>\n"
                + "    </section>\n"
                + "\n"
                + "</body>\n"
                + "</html>";

    }
    
    /**
     * @param centerContent - center content of mail
     * @param userInfo
     */
    private String clientDeactivateContent(String centerContent, HttpServletRequest request, UserInfo userInfo) {
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                + "<html>\n"
                + "<head>\n"
                + " <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n"
                + "        <title> Email </title>\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n"
                + "\n"
                + "<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>\n"
                + "<style>	\n"
                + "	.container{ width:50%; margin:0 auto; background-color:#f4f3f8; min-height:200px; font-family: 'Open Sans', sans-serif; color:#2a2a2a;}\n"
                + "	.box{ width:50%;  min-height:150px; float:left; margin-bottom:2px;} \n"
                + "	.order-detail{ background-color:#fff; border-radius:5px;}\n"
                + "	.box-header{ background-color:#c1d400; padding:10px; border-top-left-radius:5px;  border-top-right-radius:5px; }\n"
                + "	.box ul{ list-style:none; margin:0px; padding:0px;}\n"
                + "	.box ul li{ font-size:12px; padding:3px 8px; font-weight:600; color:#4a4a4a}\n"
                + "	.pickup table{ background:#253370; border-radius:5px;}\n"
                + "	.pickup table tr td{ text-align:center; width:1%; color:#fff; padding:5px; }\n"
                + "	.footer{ background-color:#111717; border-radius:5px; min-height:150px; margin-top:2px;}\n"
                + "	.green-bg{background:#c1d400; text-align:center; font-size:12px; padding:3px; margin-bottom:1px;}\n"
                + "	.green-bg:first-child{border-top-left-radius:5px;  border-top-right-radius:5px;}\n"
                + "	.green-bg:last-child{border-bottom-left-radius:5px;  border-bottom-right-radius:5px; font-size:10px;}\n"
                + "	.footer table tr td{ padding:0px 5px;}\n"
                + "	.footer table tr td ul{ list-style:none;}\n"
                + "	.footer table tr td ul li{ color:#b8b8b8; font-size:12px; padding:2px 0px;}\n"
                + "</style>\n"
                + "</head>\n"
                + "    \n"
                + " <body color=\"#ffffff\" style=\" margin:0px; padding:0px; border-top:6px solid #1394DE;border-bottom:6px solid #1394DE; line-height: 25px;\">\n"
                + "<table style=\"width:90%; margin:auto; float:none;\">\n"
                + " 	<tr>\n"
                + "    	<td style=\"background:#fff; padding:10px 0px;float:none;margin:auto;\"><img src=\"http://" + request.getServerName() + ":" + request.getServerPort() +request.getContextPath()+ "/assets/layouts/layout/img/logo.png \" alt=\"Logo\"/></td>\n" + "        <td style=\"text-align:right;\">\n"
                + " \n"
                + "    </tr>\n"
                + " </table>\n"
                + "	\n"
                + "	\n"
                + "	\n"
                + "    	\n"
                + "   \n"
                + "  <section style=\"width:100%; min-height:200px; color:#2a2a2a;\">\n"
                + "   <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\" style=\"background-color:#f6f2f1; margin:auto; float:none; margin-top:10px; padding-top:10px; padding-bottom:10px; margin-bottom:80px; font-weight:500;\">\n"
                + "     <tr>\n"
                + "      <td style=\"font-family:sans-serif; font-size: 14px; text-align:left; padding-left:20px; padding-right:20px;\">\n"
                + "       <h3 style=\"color:#253372;\">Account Deactivated !</h3>\n"
                + "       <p style=\"font-size:14px; color:#666;\"> <span style=\"display:block; color:#333; font-weight:600; font-size:16px; margin-bottom:10px;\">Dear "+userInfo.getUserName()+",</span> Your account has been deactivated , Please find below reason , for any question please contact to admin </p>\n"
                + "            <span style=\"display: block; padding: 15px; background: #ededed; margin-bottom: 15px; border: 1px solid #dedede;\" >\n"
                + centerContent
                + "            </span>\n"
                + "     <div>\n"
                + "     	<p style=\"text-align:left; font-size:12px; color:#666;\">\n"
                + "        <span style=\"display:block\">Regards</span>\n"
                + "        	Technox Consulting & IT Servises  \n"
                + "        </p>\n"
                + "     </div>\n"
                + "     <div style=\"font-size:12px; background: #f1f1f1;margin: 0px -20px -20px; padding:2px 5px 10px; border-top:1px solid #dedede; text-align:center;\">\n"
                + "     	<p style=\"margin-bottom:0px;\">\n"
                + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">www.technox.in</a>\n"
                + "        	<span style=\"display:block;color:#666;\">All Right reservred , 2016,  Technox</span>\n"
                + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">support@technox.in</a>\n"
                + "        </p>\n"
                + "     </div>\n"
                + "        </td>\n"
                + "      </tr>\n"
                + "     </table>\n"
                + "    </section>\n"
                + "\n"
                + "</body>\n"
                + "</html>";

    }
    
    /**
     * @param centerContent - center content of mail
     */
    private String clientActivateContent(String centerContent, HttpServletRequest request, UserInfo userInfo) {
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                + "<html>\n"
                + "<head>\n"
                + " <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n"
                + "        <title> Email </title>\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n"
                + "\n"
                + "<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>\n"
                + "<style>	\n"
                + "	.container{ width:50%; margin:0 auto; background-color:#f4f3f8; min-height:200px; font-family: 'Open Sans', sans-serif; color:#2a2a2a;}\n"
                + "	.box{ width:50%;  min-height:150px; float:left; margin-bottom:2px;} \n"
                + "	.order-detail{ background-color:#fff; border-radius:5px;}\n"
                + "	.box-header{ background-color:#c1d400; padding:10px; border-top-left-radius:5px;  border-top-right-radius:5px; }\n"
                + "	.box ul{ list-style:none; margin:0px; padding:0px;}\n"
                + "	.box ul li{ font-size:12px; padding:3px 8px; font-weight:600; color:#4a4a4a}\n"
                + "	.pickup table{ background:#253370; border-radius:5px;}\n"
                + "	.pickup table tr td{ text-align:center; width:1%; color:#fff; padding:5px; }\n"
                + "	.footer{ background-color:#111717; border-radius:5px; min-height:150px; margin-top:2px;}\n"
                + "	.green-bg{background:#c1d400; text-align:center; font-size:12px; padding:3px; margin-bottom:1px;}\n"
                + "	.green-bg:first-child{border-top-left-radius:5px;  border-top-right-radius:5px;}\n"
                + "	.green-bg:last-child{border-bottom-left-radius:5px;  border-bottom-right-radius:5px; font-size:10px;}\n"
                + "	.footer table tr td{ padding:0px 5px;}\n"
                + "	.footer table tr td ul{ list-style:none;}\n"
                + "	.footer table tr td ul li{ color:#b8b8b8; font-size:12px; padding:2px 0px;}\n"
                + "</style>\n"
                + "</head>\n"
                + "    \n"
                + " <body color=\"#ffffff\" style=\" margin:0px; padding:0px; border-top:6px solid #1394DE;border-bottom:6px solid #1394DE; line-height: 25px;\">\n"
                + "<table style=\"width:90%; margin:auto; float:none;\">\n"
                + " 	<tr>\n"
                + "    	<td style=\"background:#fff; padding:10px 0px;float:none;margin:auto;\"><img src=\"http://" + request.getServerName() + ":" + request.getServerPort() +request.getContextPath()+ "/assets/layouts/layout/img/logo.png \" alt=\"Logo\"/></td>\n" + "        <td style=\"text-align:right;\">\n"
                + " \n"
                + "    </tr>\n"
                + " </table>\n"
                + "	\n"
                + "	\n"
                + "	\n"
                + "    	\n"
                + "   \n"
                + "  <section style=\"width:100%; min-height:200px; color:#2a2a2a;\">\n"
                + "   <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\" style=\"background-color:#f6f2f1; margin:auto; float:none; margin-top:10px; padding-top:10px; padding-bottom:10px; margin-bottom:80px; font-weight:500;\">\n"
                + "     <tr>\n"
                + "      <td style=\"font-family:sans-serif; font-size: 14px; text-align:left; padding-left:20px; padding-right:20px;\">\n"
                + "       <h3 style=\"color:#253372;\">Account Activated !</h3>\n"
                + "       <p style=\"font-size:14px; color:#666;\"> <span style=\"display:block; color:#333; font-weight:600; font-size:16px; margin-bottom:10px;\">Dear "+userInfo.getUserName()+",</span> Your account has been activated, Now its Live , You can mange your account again  </p>\n"
                + "            <span style=\"display: block; padding: 15px; background: #ededed; margin-bottom: 15px; border: 1px solid #dedede;\" >\n"
                +               centerContent
                + "               </span>\n"
                + "     	<p style=\"text-align:left; font-size:12px; color:#666;\">\n"
                + "        <span style=\"display:block\">Regards</span>\n"
                + "        	Technox Consulting & IT Servises  \n"
                + "        </p>\n"
                + "     </div>\n"
                + "     <div style=\"font-size:12px; background: #f1f1f1;margin: 0px -20px -20px; padding:2px 5px 10px; border-top:1px solid #dedede; text-align:center;\">\n"
                + "     	<p style=\"margin-bottom:0px;\">\n"
                + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">www.technox.in</a>\n"
                + "        	<span style=\"display:block;color:#666;\">All Right reservred , 2016,  Technox</span>\n"
                + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">support@technox.in</a>\n"
                + "        </p>\n"
                + "     </div>\n"
                + "        </td>\n"
                + "      </tr>\n"
                + "     </table>\n"
                + "    </section>\n"
                + "\n"
                + "</body>\n"
                + "</html>";

    }
    
    /**
     * @param centerContent - center content of mail
     */
    private String UpdateContent(String centerContent, HttpServletRequest request, UserInfo userInfo) {
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                + "<html>\n"
                + "<head>\n"
                + " <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n"
                + "        <title> Email </title>\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n"
                + "\n"
                + "<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>\n"
                + "<style>	\n"
                + "	.container{ width:50%; margin:0 auto; background-color:#f4f3f8; min-height:200px; font-family: 'Open Sans', sans-serif; color:#2a2a2a;}\n"
                + "	.box{ width:50%;  min-height:150px; float:left; margin-bottom:2px;} \n"
                + "	.order-detail{ background-color:#fff; border-radius:5px;}\n"
                + "	.box-header{ background-color:#c1d400; padding:10px; border-top-left-radius:5px;  border-top-right-radius:5px; }\n"
                + "	.box ul{ list-style:none; margin:0px; padding:0px;}\n"
                + "	.box ul li{ font-size:12px; padding:3px 8px; font-weight:600; color:#4a4a4a}\n"
                + "	.pickup table{ background:#253370; border-radius:5px;}\n"
                + "	.pickup table tr td{ text-align:center; width:1%; color:#fff; padding:5px; }\n"
                + "	.footer{ background-color:#111717; border-radius:5px; min-height:150px; margin-top:2px;}\n"
                + "	.green-bg{background:#c1d400; text-align:center; font-size:12px; padding:3px; margin-bottom:1px;}\n"
                + "	.green-bg:first-child{border-top-left-radius:5px;  border-top-right-radius:5px;}\n"
                + "	.green-bg:last-child{border-bottom-left-radius:5px;  border-bottom-right-radius:5px; font-size:10px;}\n"
                + "	.footer table tr td{ padding:0px 5px;}\n"
                + "	.footer table tr td ul{ list-style:none;}\n"
                + "	.footer table tr td ul li{ color:#b8b8b8; font-size:12px; padding:2px 0px;}\n"
                + "</style>\n"
                + "</head>\n"
                + "    \n"
                + " <body color=\"#ffffff\" style=\" margin:0px; padding:0px; border-top:6px solid #1394DE;border-bottom:6px solid #1394DE; line-height: 25px;\">\n"
                + "<table style=\"width:90%; margin:auto; float:none;\">\n"
                + " 	<tr>\n"
                + "    	<td style=\"background:#fff; padding:10px 0px;float:none;margin:auto;\"><img src=\"http://" + request.getServerName() + ":" + request.getServerPort() +request.getContextPath()+ "/assets/layouts/layout/img/logo.png \" alt=\"Logo\"/></td>\n" + "        <td style=\"text-align:right;\">\n"
                + " \n"
                + "    </tr>\n"
                + " </table>\n"
                + "	\n"
                + "	\n"
                + "	\n"
                + "    	\n"
                + "   \n"
                + "  <section style=\"width:100%; min-height:200px; color:#2a2a2a;\">\n"
                + "   <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\" style=\"background-color:#f6f2f1; margin:auto; float:none; margin-top:10px; padding-top:10px; padding-bottom:10px; margin-bottom:80px; font-weight:500;\">\n"
                + "     <tr>\n"
                + "      <td style=\"font-family:sans-serif; font-size: 14px; text-align:left; padding-left:20px; padding-right:20px;\">\n"
                + "       <h3 style=\"color:#253372;\">Profile Updated !</h3>\n"
                + "       <p style=\"font-size:14px; color:#666;\"> <span style=\"display:block; color:#333; font-weight:600; font-size:16px; margin-bottom:10px;\">Dear "+userInfo.getUserName()+",</span> Your profile has been updated, following are new details. </p>\n"
                + "            <span style=\"display: block; padding: 15px; background: #ededed; margin-bottom: 15px; border: 1px solid #dedede;\" >\n"
                + "            <span style=\"font-weight: bold;\">User Name: </span> "+userInfo.getUserName()+"<br /><br />"
                + "            <span style=\"font-weight: bold;\">Organization: </span>"+userInfo.getOrganization()+"<br /><br />"
                + "            <span style=\"font-weight: bold;\">Email ID: </span>"+userInfo.getEmailId()+"<br /><br />"
                + "            <span style=\"font-weight: bold;\">Mobile Number: </span>+"+userInfo.getCountryCode().getMobileCode()+"-"+userInfo.getMobileNumber()+"<br /><br />"
                + "            <span style=\"font-weight: bold;\">Country: </span>"+userInfo.getCountryCode().getCountry()+"<br />"
                + "            </span>\n"
                + "     	<p style=\"text-align:left; font-size:12px; color:#666;\">\n"
                + "        <span style=\"display:block\">Regards</span>\n"
                + "        	Technox Consulting & IT Servises  \n"
                + "        </p>\n"
                + "     </div>\n"
                + "     <div style=\"font-size:12px; background: #f1f1f1;margin: 0px -20px -20px; padding:2px 5px 10px; border-top:1px solid #dedede; text-align:center;\">\n"
                + "     	<p style=\"margin-bottom:0px;\">\n"
                + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">www.technox.in</a>\n"
                + "        	<span style=\"display:block;color:#666;\">All Right reservred , 2016,  Technox</span>\n"
                + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">support@technox.in</a>\n"
                + "        </p>\n"
                + "     </div>\n"
                + "        </td>\n"
                + "      </tr>\n"
                + "     </table>\n"
                + "    </section>\n"
                + "\n"
                + "</body>\n"
                + "</html>";

    }
    
    /**
     * @param centerContent - center content of mail
     * @param userInfo
     */
    private String clientDeleteContent(String centerContent, HttpServletRequest request, UserInfo userInfo) {
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                + "<html>\n"
                + "<head>\n"
                + " <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n"
                + "        <title> Email </title>\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n"
                + "\n"
                + "<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>\n"
                + "<style>	\n"
                + "	.container{ width:50%; margin:0 auto; background-color:#f4f3f8; min-height:200px; font-family: 'Open Sans', sans-serif; color:#2a2a2a;}\n"
                + "	.box{ width:50%;  min-height:150px; float:left; margin-bottom:2px;} \n"
                + "	.order-detail{ background-color:#fff; border-radius:5px;}\n"
                + "	.box-header{ background-color:#c1d400; padding:10px; border-top-left-radius:5px;  border-top-right-radius:5px; }\n"
                + "	.box ul{ list-style:none; margin:0px; padding:0px;}\n"
                + "	.box ul li{ font-size:12px; padding:3px 8px; font-weight:600; color:#4a4a4a}\n"
                + "	.pickup table{ background:#253370; border-radius:5px;}\n"
                + "	.pickup table tr td{ text-align:center; width:1%; color:#fff; padding:5px; }\n"
                + "	.footer{ background-color:#111717; border-radius:5px; min-height:150px; margin-top:2px;}\n"
                + "	.green-bg{background:#c1d400; text-align:center; font-size:12px; padding:3px; margin-bottom:1px;}\n"
                + "	.green-bg:first-child{border-top-left-radius:5px;  border-top-right-radius:5px;}\n"
                + "	.green-bg:last-child{border-bottom-left-radius:5px;  border-bottom-right-radius:5px; font-size:10px;}\n"
                + "	.footer table tr td{ padding:0px 5px;}\n"
                + "	.footer table tr td ul{ list-style:none;}\n"
                + "	.footer table tr td ul li{ color:#b8b8b8; font-size:12px; padding:2px 0px;}\n"
                + "</style>\n"
                + "</head>\n"
                + "    \n"
                + " <body color=\"#ffffff\" style=\" margin:0px; padding:0px; border-top:6px solid #1394DE;border-bottom:6px solid #1394DE; line-height: 25px;\">\n"
                + "<table style=\"width:90%; margin:auto; float:none;\">\n"
                + " 	<tr>\n"
                + "    	<td style=\"background:#fff; padding:10px 0px;float:none;margin:auto;\"><img src=\"http://" + request.getServerName() + ":" + request.getServerPort() +request.getContextPath()+ "/assets/layouts/layout/img/logo.png \" alt=\"Logo\"/></td>\n" + "        <td style=\"text-align:right;\">\n"
                + " \n"
                + "    </tr>\n"
                + " </table>\n"
                + "	\n"
                + "	\n"
                + "	\n"
                + "    	\n"
                + "   \n"
                + "  <section style=\"width:100%; min-height:200px; color:#2a2a2a;\">\n"
                + "   <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\" style=\"background-color:#f6f2f1; margin:auto; float:none; margin-top:10px; padding-top:10px; padding-bottom:10px; margin-bottom:80px; font-weight:500;\">\n"
                + "     <tr>\n"
                + "      <td style=\"font-family:sans-serif; font-size: 14px; text-align:left; padding-left:20px; padding-right:20px;\">\n"
                + "       <h3 style=\"color:#253372;\">Account Deleted !</h3>\n"
                + "       <p style=\"font-size:14px; color:#666;\"> <span style=\"display:block; color:#333; font-weight:600; font-size:16px; margin-bottom:10px;\">Dear "+userInfo.getUserName()+",</span>Your account has been deleted by owner, please contact to administation. </p>\n"
//                + "            <span style=style=\"display: block; padding: 15px; background: #ededed; margin-bottom: 15px; border: 1px solid #dedede;\" >\n"
//                + centerContent
//                + "            </span>\n"
                + "     <div>\n"
                + "     	<p style=\"text-align:left; font-size:12px; color:#666;\">\n"
                + "        <span style=\"display:block\">Regards</span>\n"
                + "        	Technox Consulting & IT Servises  \n"
                + "        </p>\n"
                + "     </div>\n"
                + "     <div style=\"font-size:12px; background: #f1f1f1;margin: 0px -20px -20px; padding:2px 5px 10px; border-top:1px solid #dedede; text-align:center;\">\n"
                + "     	<p style=\"margin-bottom:0px;\">\n"
                + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">www.technox.in</a>\n"
                + "        	<span style=\"display:block;color:#666;\">All Right reservred , 2016,  Technox</span>\n"
                + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">support@technox.in</a>\n"
                + "        </p>\n"
                + "     </div>\n"
                + "        </td>\n"
                + "      </tr>\n"
                + "     </table>\n"
                + "    </section>\n"
                + "\n"
                + "</body>\n"
                + "</html>";

    }
    
    /**
     * 
     * @param subject
     * @param userlicense
     * @param userInfo
     * @param request
     * @return 
     */
    public Boolean sendEmailForLicense(String subject, UserLicense userlicense, HttpServletRequest request, UserInfo userInfo) {
        String validity;
        if(userlicense.getExpiryDate() != null){
            validity = new SimpleDateFormat("dd-MM-yyyy").format(userlicense.getIssueDate()) +" TO "+new SimpleDateFormat("dd-MM-yyyy").format(userlicense.getExpiryDate()); 
        } else{
            validity = "Unlimited";
        }
        try{
            ServletContext servletContext = request.getSession().getServletContext();
            String appPath = servletContext.getRealPath("/");
            System.out.println("appPath = " + appPath);
            String fullPath = appPath.replace("\\", "/")+"license"+userlicense.getUserLicenseId()+".txt";    
            FileOutputStream fis = new FileOutputStream(fullPath);

            fis.write(("User License : "+userlicense.getLicense()).getBytes());
            fis.close();
            System.out.println("File Path == "+ fullPath);
        }catch (Exception e){
            e.printStackTrace();
        }
        String content = userLicenceContent(userlicense.getUserInfo().getUserName(),userlicense.getCourse().getCourseName(),userlicense.getUserInfo().getEmailId(), userlicense.getUserInfo().getMobileNumber(), validity, userlicense.getSystemInfo().getSystemId(),userlicense.getLicense(), userInfo.getOrganization(),userInfo.getEmailId(), request, "license"+userlicense.getUserLicenseId()+".txt");
        System.out.println(content);
        try {
            SendMail sendMail = new SendMail(mailSender);
            MailSetting mailSetting = mailSettingDao.getMailSettingsById(userInfo.getUserInfoId());
            if(mailSetting != null){
                String decryptedpassword = aes.decrypt(mailSetting.getSmtpPassword(), "0123456789abcdef");
//                Thread t = new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
                            sendMail.sendEmail(userlicense.getUserInfo().getEmailId(), mailSetting.getMailFrom(), content, subject, mailSetting.getSmtpServer(), mailSetting.getMailPort(), decryptedpassword, mailSetting.getSmtpUser());
//                        } catch (UnsupportedEncodingException ex) {
//                            Logger.getLogger(MailController.class.getName()).log(Level.SEVERE, null, ex);
//                        }
//                    }
//                });
                System.out.println("sendMail::::" + sendMail);
                return true;
            } else{
                System.out.println("Mail Can Not Sent");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    private String userLicenceContent(String userName, String courseName, String email, String contact, String validity, String systemId, String licensekey, String clientCompanyName, String clientEmailid, HttpServletRequest request, String filepath){
        return "<!DOCTYPE html>\n"
                + "\n"
                + "<html lang=\"en\">\n"
                + "<head>\n"
                + "<meta charset=\"utf-8\" />\n"
                + "<title>FSS | Client Account</title>\n"
                + "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "<meta content=\"width=device-width, initial-scale=1\" name=\"viewport\" />\n"
                + "<meta content=\"\" name=\"description\" />\n"
                + "<meta content=\"\" name=\"author\" />\n"
                + "<style>\n"
                + "html, body {\n"
                + "	height: 100%;\n"
                + "	font-family: sans-serif;\n"
                + "}\n"
                + "</style>\n"
                + "</head>\n"
                + "<body style=\"background:url(img/86.jpg); background-size:cover;\">\n"
                + "<table style=\"width:100%; height:100%;\">\n"
                + " <tr>\n"
                + "  <td style=\"vertical-align:middle; text-align:center;\">\n"
                + "  <div style=\"width:550px; min-height:250px; border-radius:5px; box-shadow:0px 1px 2px rgba(0,0,0,0.2); background:#fff; display:inline-block;  overflow:hidden; text-align:left;\">\n"
                + "    <header style=\"padding:10px; background:#d70000; color:#fff;\"> Welcome to "+clientCompanyName+" </header>\n"
                + "    <div style=\"padding:0px 15px 15px;\">\n"
                + "     <h2 style=\"color:#8BC34A;\">License for "+courseName+"</h2>\n"
                + "     \n"
                + "     <p style=\"font-size:14px; color:#666;\"> <span style=\"display:block; color:#333; font-weight:600; font-size:16px; margin-bottom:10px;\">Dear "+userName+",</span> Welcome to "+clientCompanyName+", Your License Generated Successfully for "+courseName+"</p>\n"
                + "	 <p style=\"font-size:14px; color:#666;\"> Please find below details for liense and Download or Copy License  </p>\n"
                + "     <ul style=\"margin:0px; padding:0px; font-size:14px; color:#666; list-style:none;\">\n"
                + "      <li style=\"margin-bottom:5px;margin-left: 0px\"><b>License Details</b></li>\n"
                + "      \n"
                + "     </ul>\n"
                + "     <div style=\"margin-top:15px;\"> \n"
                + "     	<table style=\"text-align:left; font-size:13px; width:100%;  color:#666; border-collapse: collapse\">\n"
                + "        	<tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">User Name</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px;  text-align:center;\">"+userName+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Mobile No</td>\n"
                + "                <td style=\"border:1px solid #dedede;padding:6px 10px;text-align:center; \">"+contact+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px;font-weight:600; \">Course Name</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+courseName+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Validity</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+validity+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">System ID</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center; text-overflow: ellipsis;\n"
                + "  white-space: nowrap; overflow: hidden; max-width:150px;\">"+systemId+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">License Key</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center; text-overflow: ellipsis;\n"
                + "  white-space: nowrap; overflow: hidden; max-width:150px;\">"+licensekey+"</td>\n"
                + "            </tr>\n"
                + "            \n"
                + "        </table>\n"
                + "        <div style=\"text-align:right; padding:15px 0px;\">\n"
//                + "        	<a style=\" background-color:#666; color:#fff; padding:5px 10px; border:none; border-radius:4px;\" onclick=\"copyToClipboard()\" >Copy License Key</a>\n"
                + "            <a style=\" background-color:#390; color:#fff; padding:5px 10px; border:none; border-radius:4px;\" href=\"http://" + request.getServerName() + ":" + request.getServerPort() +request.getContextPath()+ "/downloadlicensethroughmail?filepath="+filepath+" \">Download Licence Key</a>\n"
                + "        </div>\n"
                + "     </div>\n"
                + "     <div>\n"
                + "     	<p style=\"text-align:left; font-size:12px; color:#666;\">\n"
                + "        <span style=\"display:block\">Regards</span>\n"
                + "        	"+clientCompanyName+" \n"
                + "        </p>\n"
                + "     </div>\n"
                + "     <div style=\"font-size:12px; background: #f1f1f1;margin: 0px -15px -15px; padding:2px 5px 10px; border-top:1px solid #dedede; text-align:center;\">\n"
                + "     	<p style=\"margin-bottom:0px;\">\n"
                + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">www.technox.in</a>\n"
                + "        	<span style=\"display:block;color:#666;\">All Right reservred , 2016, "+clientCompanyName+"</span>\n"
                + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">"+clientEmailid+"</a>\n"
                + "        </p>\n"
                + "     </div>\n"
                + "    </div>\n"
                + "   </div>\n"
                + "   </td>\n"
                + " </tr>\n"
                + "</table>\n"
//                + "<script>\n"
//                + "function copyToClipboard() {\n" 
//                + " Copied = "+licensekey+".createTextRange();\n" 
//                + " Copied.execCommand(\"Copy\");\n"
////                + "  window.prompt(\"Copy to clipboard: Ctrl+C, Enter\", "+licensekey+");\n" 
//                + "}"
//                + "</script>\n"
                + "</body>\n"
                + "</html>";
    }
    
    private String clientLicenceContent(String userName, String weblicenseLimit, String desktoplicenseLimit, String contact, String validity, String desktoplicensekey, String clientCompanyName, String clientEmailid, String password, HttpServletRequest request, String filepath){
        return "<!DOCTYPE html>\n" 
                +"\n"
                + "<html lang=\"en\">\n"
                + "<head>\n"
                + "<meta charset=\"utf-8\" />\n"
                + "<title>FSS | Client Account</title>\n"
                + "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "<meta content=\"width=device-width, initial-scale=1\" name=\"viewport\" />\n"
                + "<meta content=\"\" name=\"description\" />\n"
                + "<meta content=\"\" name=\"author\" />\n"
                + "<style>\n"
                + "html, body {\n"
                + "	height: 100%;\n"
                + "	font-family: sans-serif;\n"
                + "}\n"
                + "</style>\n"
                + "</head>\n"
                + "<body style=\"background:url(img/86.jpg); background-size:cover;\">\n"
                + "<table style=\"width:100%; height:100%;\">\n"
                + " <tr>\n"
                + "  <td style=\"vertical-align:middle; text-align:center;\">\n"
                + "  <div style=\"width:550px; min-height:250px; border-radius:5px; box-shadow:0px 1px 2px rgba(0,0,0,0.2); background:#fff; display:inline-block;  overflow:hidden; text-align:left;\">\n"
                + "    <header style=\"padding:10px; background:#d70000; color:#fff;\"> Welcome to Technox Consulting & IT Servises </header>\n"
                + "    <div style=\"padding:0px 15px 15px;\">\n"
                + "     <h2 style=\"color:#8BC34A;\">Account Created !</h2>\n"
                + "     \n"
                + "     <p style=\"font-size:14px; color:#666;\"> <span style=\"display:block; color:#333; font-weight:600; font-size:16px; margin-bottom:10px;\">Dear "+userName+"</span> Welcome to Technox, You account Created Successfully for File Security System. Please Manage your account with following login crenditial. </p>\n"
                + "     <ul style=\"margin:0px; padding:0px; font-size:14px; color:#666; list-style:none;\">\n"
                + "      <li style=\"margin-bottom:5px;margin-left: 0px\">Login ID : <b>"+userName+"</b></li>\n"
                + "      <li style=\"margin-left: 0px\">Password : <b>"+password+"</b></li>\n"
                + "     </ul>\n"
                + "     <div style=\"margin-top:15px;\"> \n"
                + "     	<table style=\"text-align:left; font-size:13px; width:100%;  color:#666; border-collapse: collapse\">\n"
                + "        	<tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Company Name</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px;  text-align:center;\">"+clientCompanyName+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Company Owner Name</td>\n"
                + "                <td style=\"border:1px solid #dedede;padding:6px 10px;text-align:center; \">"+userName+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px;font-weight:600; \">Email ID</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+clientEmailid+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Contact No</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+contact+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Validity </td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+validity+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Total License Balance for Desktop App</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+desktoplicenseLimit+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Total License Balance for Web App</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+weblicenseLimit+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">License Key</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center; text-overflow: ellipsis;\n"
                + "             white-space: nowrap; overflow: hidden; max-width:150px;\">"+desktoplicensekey+"</td>\n"
                + "            </tr>\n"
                + "        </table>\n"
                + "        <div style=\"text-align:right; padding:15px 0px;\">\n"
//                + "        	<button style=\" background-color:#666; color:#fff; padding:5px 10px; border:none; border-radius:4px;\" onclick=\"copyToClipboard()\">Copy License Key</button>\n"
                + "            <a style=\" background-color:#390; color:#fff; padding:5px 10px; border:none; border-radius:4px;\" href=\"http://" + request.getServerName() + ":" + request.getServerPort() +request.getContextPath()+ "/downloadlicensethroughmail?filepath="+filepath+" \">Download Licence Key</a>\n"
                + "        </div>\n"
                + "     </div>\n"
                + "     <div>\n"
                + "     	<p style=\"text-align:left; font-size:12px; color:#666;\">\n"
                + "        <span style=\"display:block\">Regards</span>\n"
                + "        	Technox Consulting & IT Servises \n"
                + "        </p>\n"
                + "     </div>\n"
                + "     <div style=\"font-size:12px; background: #f1f1f1;margin: 0px -15px -15px; padding:2px 5px 10px; border-top:1px solid #dedede; text-align:center;\">\n"
                + "     	<p style=\"margin-bottom:0px;\">\n"
                + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">www.technox.in</a>\n"
                + "        	<span style=\"display:block;color:#666;\">All Right reservred , 2016, Technox</span>\n"
                + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">support@technox.in</a>\n"
                + "        </p>\n"
                + "     </div>\n"
                + "    </div>\n"
                + "   </div>\n"
                + "   </td>\n"
                + " </tr>\n"
                + "</table>\n"
                + "</body>\n"
//                + "<script>\n"
//                + "function copyToClipboard() {\n" 
//                + "  window.prompt(\"Copy to clipboard: Ctrl+C, Enter\", "+desktoplicensekey+");\n" 
//                + "}"
//                + "</script>\n"
                + "</html>";
    }
    
    private String clientLicenceRenewContent(String userName, String weblicenseLimit, String desktoplicenseLimit, String contact, String validity, String desktoplicensekey, String clientCompanyName, String clientEmailid, HttpServletRequest request, String filepath){
        return "<!DOCTYPE html>\n" 
                +"\n"
                + "<html lang=\"en\">\n"
                + "<head>\n"
                + "<meta charset=\"utf-8\" />\n"
                + "<title>FSS | Client Account</title>\n"
                + "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "<meta content=\"width=device-width, initial-scale=1\" name=\"viewport\" />\n"
                + "<meta content=\"\" name=\"description\" />\n"
                + "<meta content=\"\" name=\"author\" />\n"
                + "<style>\n"
                + "html, body {\n"
                + "	height: 100%;\n"
                + "	font-family: sans-serif;\n"
                + "}\n"
                + "</style>\n"
                + "</head>\n"
                + "<body style=\"background:url(img/86.jpg); background-size:cover;\">\n"
                + "<table style=\"width:100%; height:100%;\">\n"
                + " <tr>\n"
                + "  <td style=\"vertical-align:middle; text-align:center;\">\n"
                + "  <div style=\"width:550px; min-height:250px; border-radius:5px; box-shadow:0px 1px 2px rgba(0,0,0,0.2); background:#fff; display:inline-block;  overflow:hidden; text-align:left;\">\n"
                + "    <header style=\"padding:10px; background:#d70000; color:#fff;\"> Welcome to Technox Consulting & IT Servises </header>\n"
                + "    <div style=\"padding:0px 15px 15px;\">\n"
                + "     <h2 style=\"color:#8BC34A;\">License Renew !</h2>\n"
                + "     \n"
                + "     <p style=\"font-size:14px; color:#666;\"> <span style=\"display:block; color:#333; font-weight:600; font-size:16px; margin-bottom:10px;\">Dear "+userName+"</span> Welcome to Technox, Your license Renwed Successfully for File Security System.</p>\n"
                + "     <div style=\"margin-top:15px;\"> \n"
                + "     	<table style=\"text-align:left; font-size:13px; width:100%;  color:#666; border-collapse: collapse\">\n"
                + "        	<tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Company Name</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px;  text-align:center;\">"+clientCompanyName+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Company Owner Name</td>\n"
                + "                <td style=\"border:1px solid #dedede;padding:6px 10px;text-align:center; \">"+userName+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px;font-weight:600; \">Email ID</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+clientEmailid+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Contact No</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+contact+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Validity </td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+validity+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Total License Balance for Desktop App</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+desktoplicenseLimit+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Total License Balance for Web App</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+weblicenseLimit+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">License Key</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center; text-overflow: ellipsis;\n"
                + "             white-space: nowrap; overflow: hidden; max-width:150px;\">"+desktoplicensekey+"</td>\n"
                + "            </tr>\n"
                + "        </table>\n"
                + "        <div style=\"text-align:right; padding:15px 0px;\">\n"
                + "            <a style=\" background-color:#390; color:#fff; padding:5px 10px; border:none; border-radius:4px;\" href=\"http://" + request.getServerName() + ":" + request.getServerPort() +request.getContextPath()+ "/downloadlicensethroughmail?filepath="+filepath+" \">Download Licence Key</a>\n"
                + "        </div>\n"
                + "     </div>\n"
                + "     <div>\n"
                + "     	<p style=\"text-align:left; font-size:12px; color:#666;\">\n"
                + "        <span style=\"display:block\">Regards</span>\n"
                + "        	Technox Consulting & IT Servises \n"
                + "        </p>\n"
                + "     </div>\n"
                + "     <div style=\"font-size:12px; background: #f1f1f1;margin: 0px -15px -15px; padding:2px 5px 10px; border-top:1px solid #dedede; text-align:center;\">\n"
                + "     	<p style=\"margin-bottom:0px;\">\n"
                + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">www.technox.in</a>\n"
                + "        	<span style=\"display:block;color:#666;\">All Right reservred , 2016, Technox</span>\n"
                + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">support@technox.in</a>\n"
                + "        </p>\n"
                + "     </div>\n"
                + "    </div>\n"
                + "   </div>\n"
                + "   </td>\n"
                + " </tr>\n"
                + "</table>\n"
                + "</body>\n"
                + "</html>";
    }
    
    private String clientLicenceEditContent(String userName, String weblicenseLimit, String desktoplicenseLimit, String contact, String validity, String desktoplicensekey, String clientCompanyName, String clientEmailid, HttpServletRequest request, String filepath){
        return "<!DOCTYPE html>\n" 
                +"\n"
                + "<html lang=\"en\">\n"
                + "<head>\n"
                + "<meta charset=\"utf-8\" />\n"
                + "<title>FSS | Client Account</title>\n"
                + "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "<meta content=\"width=device-width, initial-scale=1\" name=\"viewport\" />\n"
                + "<meta content=\"\" name=\"description\" />\n"
                + "<meta content=\"\" name=\"author\" />\n"
                + "<style>\n"
                + "html, body {\n"
                + "	height: 100%;\n"
                + "	font-family: sans-serif;\n"
                + "}\n"
                + "</style>\n"
                + "</head>\n"
                + "<body style=\"background:url(img/86.jpg); background-size:cover;\">\n"
                + "<table style=\"width:100%; height:100%;\">\n"
                + " <tr>\n"
                + "  <td style=\"vertical-align:middle; text-align:center;\">\n"
                + "  <div style=\"width:550px; min-height:250px; border-radius:5px; box-shadow:0px 1px 2px rgba(0,0,0,0.2); background:#fff; display:inline-block;  overflow:hidden; text-align:left;\">\n"
                + "    <header style=\"padding:10px; background:#d70000; color:#fff;\"> Welcome to Technox Consulting & IT Servises </header>\n"
                + "    <div style=\"padding:0px 15px 15px;\">\n"
                + "     <h2 style=\"color:#8BC34A;\">License Edit !</h2>\n"
                + "     \n"
                + "     <p style=\"font-size:14px; color:#666;\"> <span style=\"display:block; color:#333; font-weight:600; font-size:16px; margin-bottom:10px;\">Dear "+userName+"</span> Welcome to Technox, Your license Eddited Successfully for File Security System.</p>\n"
                + "     <div style=\"margin-top:15px;\"> \n"
                + "     	<table style=\"text-align:left; font-size:13px; width:100%;  color:#666; border-collapse: collapse\">\n"
                + "        	<tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Company Name</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px;  text-align:center;\">"+clientCompanyName+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Company Owner Name</td>\n"
                + "                <td style=\"border:1px solid #dedede;padding:6px 10px;text-align:center; \">"+userName+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px;font-weight:600; \">Email ID</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+clientEmailid+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Contact No</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+contact+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Validity </td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+validity+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Total License Balance for Desktop App</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+desktoplicenseLimit+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">Total License Balance for Web App</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center;\">"+weblicenseLimit+"</td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; font-weight:600;\">License Key</td>\n"
                + "                <td style=\"border:1px solid #dedede; padding:6px 10px; text-align:center; text-overflow: ellipsis;\n"
                + "             white-space: nowrap; overflow: hidden; max-width:150px;\">"+desktoplicensekey+"</td>\n"
                + "            </tr>\n"
                + "        </table>\n"
                + "        <div style=\"text-align:right; padding:15px 0px;\">\n"
                + "            <a style=\" background-color:#390; color:#fff; padding:5px 10px; border:none; border-radius:4px;\" href=\"http://" + request.getServerName() + ":" + request.getServerPort() +request.getContextPath()+ "/downloadlicensethroughmail?filepath="+filepath+" \">Download Licence Key</a>\n"
                + "        </div>\n"
                + "     </div>\n"
                + "     <div>\n"
                + "     	<p style=\"text-align:left; font-size:12px; color:#666;\">\n"
                + "        <span style=\"display:block\">Regards</span>\n"
                + "        	Technox Consulting & IT Servises \n"
                + "        </p>\n"
                + "     </div>\n"
                + "     <div style=\"font-size:12px; background: #f1f1f1;margin: 0px -15px -15px; padding:2px 5px 10px; border-top:1px solid #dedede; text-align:center;\">\n"
                + "     	<p style=\"margin-bottom:0px;\">\n"
                + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">www.technox.in</a>\n"
                + "        	<span style=\"display:block;color:#666;\">All Right reservred , 2016, Technox</span>\n"
                + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">support@technox.in</a>\n"
                + "        </p>\n"
                + "     </div>\n"
                + "    </div>\n"
                + "   </div>\n"
                + "   </td>\n"
                + " </tr>\n"
                + "</table>\n"
                + "</body>\n"
                + "</html>";
    }
    
    private String designContent(String userName, String courseName, String email, String contact, String validity, String systemId, String licensekey, String clientCompanyName, String clientEmailid, HttpServletRequest request){
        return "<!DOCTYPE html>\n" 
               + "\n" 
               + "<html lang=\"en\">\n" 
               + "<head>\n" 
               + "<meta charset=\"utf-8\" />\n" 
               + "<title>FSS | Client Account</title>\n" 
               + "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" 
               + "<meta content=\"width=device-width, initial-scale=1\" name=\"viewport\" />\n" 
               + "<meta content=\"\" name=\"description\" />\n" 
               + "<meta content=\"\" name=\"author\" />\n" 
               + "<style>\n" 
               + "html, body {\n" 
               + "	height: 100%;\n" 
               + "	font-family: sans-serif;\n" 
               + "}\n" 
               + "</style>\n" 
               + "</head>\n" 
               + "<body style=\"background:url(http://" + request.getServerName() + ":" + request.getServerPort() +request.getContextPath()+ "/img/86.jpg); background-size:cover;\">\n" 
               + "<table style=\"width:100%; height:100%;\">\n" 
               + " <tr>\n" 
               + "  <td style=\"vertical-align:middle; text-align:center;\">\n" 
               + "  <div style=\"width:550px; min-height:250px; border-radius:5px; box-shadow:0px 1px 2px rgba(0,0,0,0.2); background:#fff; display:inline-block;  overflow:hidden;\">\n" 
               + "    <header style=\"padding:10px; background:#d70000; color:#fff; font-weight:600; text-align:left;\"> Welcome to "+clientCompanyName+"</header>\n" 
               + "    <div style=\"padding:0px 15px 15px;\">\n" 
               + "     <h2 style=\"font-weight:600; text-align:left;\">License For ( "+courseName+" )</h2>\n" 
               + "     <p style=\"font-size:14px; color:#666; text-align:left;\"> <span style=\"display:block; color:#333; font-weight:600; font-size:16px; margin-bottom:10px;\">Dear "+userName+"</span> Welcome to "+clientCompanyName+", Your License Genereted Successfully for "+courseName+". </p>\n" 
               + "     <p style=\"font-size:14px; color:#666; text-align:left;\"> Please find below details for License and Download Or Copy License</p>\n" 
               + "     <p style=\"font-size:18px; color:#666; text-align:left;\"> <span style=\"display:block; color:#333; font-weight:600; font-size:16px; margin-bottom:10px;\"> License Details</p>\n" 
               + "     <div style=\"margin-top:15px;\"> \n" 
               + "     	<table style=\"text-align:left; font-size:13px; width:100%;  color:#666; border-collapse: collapse\">\n"  
               + "            <tr>\n" 
               + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; width: 40%;\">User Name</td>\n" 
               + "                <td style=\"border:1px solid #dedede; padding:6px 10px; \">"+userName+"</td>\n" 
               + "            </tr>\n" 
               + "            <tr>\n" 
               + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; width: 40%;\">Email ID</td>\n" 
               + "                <td style=\"border:1px solid #dedede; padding:6px 10px; \">"+email+"</td>\n" 
               + "            </tr>\n" 
               + "            <tr>\n" 
               + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; width: 40%;\">Contact No</td>\n" 
               + "                <td style=\"border:1px solid #dedede; padding:6px 10px; \"> +91-"+contact+"</td>\n" 
               + "            </tr>\n" 
               + "            <tr>\n" 
               + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; width: 40%;\">Course Name</td>\n" 
               + "                <td style=\"border:1px solid #dedede; padding:6px 10px; \">"+courseName+"</td>\n" 
               + "            </tr>\n" 
               + "            <tr>\n" 
               + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; width: 40%;\">Validity </td>\n" 
               + "                <td style=\"border:1px solid #dedede; padding:6px 10px; \">"+validity+"</td>\n" 
               + "            </tr>\n" 
               + "            <tr>\n" 
               + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; width: 40%;\">System Id</td>\n" 
               + "                <td style=\"border:1px solid #dedede; padding:6px 10px; \"><p class=\"text-ellips\">"+systemId+"</p></td>\n" 
               + "            </tr>\n" 
               + "            <tr>\n" 
               + "            	<td style=\"border:1px solid #dedede; padding:6px 10px; width: 40%;\">License Key </td>\n" 
               + "                <td style=\"border:1px solid #dedede; padding:6px 10px; \"><p class=\"text-ellips\">"+licensekey+"</p></td>\n" 
               + "            </tr>\n" 
               + "        </table>\n" 
               + "     </div>\n" 
               + "      <input type=\"buttton\" value=\"Copy License Key\"/>\n"
               + "      <input type=\"buttton\" value=\"Download License Key\"/>\n"
               + "     <div>\n" 
               + "     	<p style=\"text-align:left; font-size:12px; color:#666;\">\n" 
               + "        <span style=\"display:block\">Regards</span>\n" 
               + "        	"+clientCompanyName+" \n" 
               + "        </p>\n" 
               + "     </div>\n" 
               + "     <div style=\"font-size:12px; background: #f1f1f1;margin: 0px -15px -15px; padding:2px 5px 10px; border-top:1px solid #dedede;\">\n" 
               + "     	<p style=\"margin-bottom:0px;\">\n" 
               + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">Client Website</a>\n" 
               + "        	<span style=\"display:block;color:#666;\">All Right reservred , 2016, "+clientCompanyName+"</span>\n" 
               + "        	<a href=\"#\" style=\"display:block; text-decoration:none; color:#d70000;\">"+clientEmailid+"</a>\n" 
               + "        </p>\n" 
               + "     </div>\n" 
               + "    </div>\n" 
               + "   </div>\n" 
               + "   </td>\n" 
               + " </tr>\n" 
               + "</table>\n" 
               + "</body>\n" 
               + "</html>";
    }
    
    public void sendMessage(String mobilenumbers, String text){
        try{
            SendMessage sendMessage = new SendMessage();
            System.out.println("comma Seperated mobile numbers:-"+mobilenumbers);
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    try {
                        sendMessage.sendMultipleMessages(mobilenumbers, text);
//                    } catch (UnsupportedEncodingException ex) {
//                        Logger.getLogger(MailController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//            }).start();
        }catch(Exception e){
            e.printStackTrace();
        }
        
    }
}
