<!DOCTYPE html>

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>FSS | Admin</title>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
 <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<style>
    .cke_panel{z-index: 10060!important;}
    .bs-example{
                width: 500px;
		float: right;
	}
</style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top"> 
 <!-- BEGIN HEADER INNER -->
 <jsp:include page="topbar.jsp" />
 <!-- END HEADER INNER --> 
</div>
<!-- END HEADER --> 
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER --> 
<!-- BEGIN CONTAINER -->
<div class="page-container"> 
 <!-- BEGIN SIDEBAR -->
 <div class="page-sidebar-wrapper"> 
  <!-- BEGIN SIDEBAR --> 
  <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
  <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
  <div class="page-sidebar navbar-collapse collapse"> 
   <!-- BEGIN SIDEBAR MENU --> 
   <jsp:include page="sidebar.jsp" />
<!--  <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
     DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
    <li class="sidebar-toggler-wrapper hide"> 
      BEGIN SIDEBAR TOGGLER BUTTON 
     <div class="sidebar-toggler"> <span></span> </div>
      END SIDEBAR TOGGLER BUTTON  
    </li>
    <li class="nav-item  "> <a href="index.html" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
    <li class="nav-item  active"> <a href="users.html" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
     
    </li>
    <li class="nav-item  "> <a href="licence.html" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">Licence</span>  </a>
     
    </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
     
    </li>
    <li class="nav-item  "> <a href="report.html" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
    <li class="nav-item  "> <a href="watermark.html" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
    <li class="nav-item  "> <a href="settings.html" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
   </ul>-->
   <!-- END SIDEBAR MENU --> 
   <!-- END SIDEBAR MENU --> 
  </div>
  <!-- END SIDEBAR --> 
 </div>
 <!-- END SIDEBAR --> 
 <!-- BEGIN CONTENT -->
 <div class="page-content-wrapper"> 
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content"> 
   <!-- BEGIN PAGE HEADER--> 
   
   <!-- BEGIN PAGE TITLE-->
   <!--<h3 class="page-title pull-left"> <i class="icon-user "></i> Users 
    <!-- <small>blank page layout</small>--
   </h3>--> 
   <!-- END PAGE TITLE--> 
   <!-- END PAGE HEADER-->
   
<div class="clearfix"></div>

<div id="successMessage" class="bs-example" style="display: none;">
    <div  class="alert alert-success fade in">
           <a href="#" class="close" data-dismiss="alert">&times;</a>
               <strong>Success!</strong> ${message}
    </div>
</div>
 <div class="clearfix"></div>
  <div class="row">
		<div class="col-sm-12">
			<div class="portlet  portlet-datatable box red ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-user  "></i>
                                       Users
                                    </div>
                                    <div class="actions">
                                        <c:set var="Uname" value="${user.userName}"/>
                                        <!--<a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/listview" class=" btn"  data-toggle="tooltip" data-placement="top" title="List View"> <i class="icon-list"></i></a>-->
                                        <a href="javascript:;" class=" btn" style="display:none;" data-toggle="tooltip" data-placement="top" title="View" id="viewoneclient"> <i class="fa fa-eye"></i></a>
                                        <a href="javascript:;" class=" btn" style="display:none;" data-toggle="tooltip" data-placement="top" title="Activate Client" id="activeclient"> <i class="fa fa-check"></i></a>
                                        <a href="javascript:;" class=" btn" style="display:none;" data-toggle="tooltip" data-placement="top" title="Deactivate Client" id="deactiveclient"> <i class="fa fa-times"></i></a>
                                        <a href="javascript:;" class=" btn" style="display:none;" data-toggle="tooltip" data-placement="top" title="Send Mail" id="sendsinglemail"> <i class="fa fa-envelope"></i></a>
                                        <a href="javascript:;" class=" btn" style="display:none;" data-toggle="tooltip" data-placement="top" title="Send Mail" id="sendmultiplemail"> <i class="fa fa-envelope"></i></a>
                                        <a href="javascript:;" class=" btn" style="display:none;" data-toggle="tooltip" data-placement="top" title="Send Message" id="sendmultiplemessage"> <i class="fa fa-comment"></i></a>
                                        <a href="javascript:;" class=" btn" style="display:none;" data-toggle="tooltip" data-placement="top" title="Send Message" id="sendsinglemessage"> <i class="fa fa-comment"></i></a>
                                        <a href="javascript:;" class=" btn" style="display:none;" data-toggle="tooltip" data-placement="top" title="Delete" id="deleteclient"> <i class="fa fa-trash"></i></a>
                                    	<a href="javascript:;" class=" btn" style="display:none;" data-toggle="tooltip" data-placement="top" title="Edit" id="editclient"> <i class="fa fa-edit"></i></a>
                                        <a class="btn  btn-icon-only " style="display: none;" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Print"  id="exportClient">
                                            <i class="icon-printer"></i>
                                        </a>
                                        <a class="btn  btn-icon-only " style="display: none;" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Print"  id="exportOneClient">
                                            <i class="icon-printer"></i>
                                        </a>
                                        <a  href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/viewclient" class=" btn" data-toggle="tooltip" data-placement="top" title="Grid View">
                                            <i class="icon-grid"></i>
                                        </a>
                                        
                                        <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/createclient" class=" btn" data-toggle="tooltip" data-placement="top" title="Add User">
                                            <i class="icon-plus"></i>  
                                        </a>
                                    </div>
                                   
                                </div>
                                <div class="portlet-body clearfix">
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column userlist-table" id="sample_5">
                                        <thead>
                                            <tr>
                                                <th class="table-checkbox">
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes" />
                                                        <span></span>
                                                    </label>
                                                </th>
                                                <th> User ID </th>
                                                <th> Name </th>
                                                <th> Organization Name </th>
                                                <th> Email ID </th>
                                                <th> Mobile No. </th>
                                                <th> Status </th>
                                            </tr>
                                        </thead>
                                       
                                        <tbody id="performactions">
                                    <c:forEach items="${users}" var="userdata">
                                                <tr class="odd gradeX">
                                                    <td>
                                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" name="userId" class="checkboxes" value="${userdata.userInfoId}" />
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td> ${userdata.displayName} </td>
                                                    <td> ${userdata.userName} </td>
                                                    <td> ${userdata.organization} </td>
                                                    <td> ${userdata.emailId} </td>
                                                    <td>+${userdata.countryCode.mobileCode}- ${userdata.mobileNumber} </td>
                                                    <td>
                                                        <c:choose>
                                                            <c:when test="${userdata.userStatus==true}">
                                                                Active
                                                            </c:when>
                                                            <c:otherwise>
                                                                Deactive
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </td>
<!--                                                    <td class="text-center">
                                                        
                                                        <c:choose>
                                                            <c:when test="${userdata.userStatus==true}">
                                                                <button class="btn btn-sm btn-default" type="button" data-toggle="tooltip" data-placement="top" title="Deactivate user"></button>
                                                                    <a href="${pageContext.request.contextPath}/${user.userName}/deactivateclient/${userdata.userInfoId}" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="Deactivate user">
                                                                        Deactive</a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <button class="btn btn-sm btn-default" type="button" data-toggle="tooltip" data-placement="top" title="Activate User">
                                                                    <a href="${pageContext.request.contextPath}/${user.userName}/activateclient/${userdata.userInfoId}" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="Activate user">
                                                                        Active</a>
                                                                </button> 
                                                            </c:otherwise>
                                                        </c:choose>
                                                        <button class="btn btn-sm btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete User">
                                                            <a href="${pageContext.request.contextPath}/${user.userName}/deleteclient/${userdata.userInfoId}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Delete User">
                                                                <i class="fa fa-trash-o"></i></a>
                                                        </button>
                                                        <button class="btn btn-sm btn-default" type="button" data-toggle="tooltip" data-placement="top" title="Edit User">
                                                            <a href="${pageContext.request.contextPath}/${user.userName}/updateclient/${userdata.userInfoId}" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="Edit User">
                                                                <i class="fa fa-edit"></i></a>
							</button>
                                                       
                                                    </td>-->
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    <span style="color: red; display: block; font-size: 14px;" id="selecterror" ></span>
                                </div>
                            </div>
			</div>
                    </div>
   
   
  </div>
  <!-- END CONTENT BODY --> 
 </div>
 <!-- END CONTENT --> 
 <!-- BEGIN QUICK SIDEBAR --> 
 <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 
 
 <!-- END QUICK SIDEBAR --> 
</div>
<!-- END CONTAINER --> 
<!-- BEGIN FOOTER -->
<div class="page-footer">
 <div class="page-footer-inner"> 2016 &copy; Organization </div>
 <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
</div>
<!-- END FOOTER --> 
<div class="modal fade" id="confirm_delete" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="">Confirm Delete</h4>
                    </div>
                    <div class="modal-body"> 
                        <div class="form-group">
                            <span Style="font-style: normal;">Are You Sure To Want To Delete Selected User ?</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <span style="color: green; display: block; font-size: 14px;" id="deleteresponse" ></span>
                        <!--<button type="button" class=" btn btn green" data-dismiss="modal">NO</button>-->
<!--                        <button type="button" class="btn green" id="">YES</button>-->
                        <a href="javascript:;" id="Confirm" class=" btn btn-circle btn-success">YES</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="singlemailModal" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="quick_mail_modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">New Email</h4>
                    </div>
                    <form>
                        <div class="modal-body" >
                            <input type="text" id="displaymailinfo" readonly class="form-control input-circle" />

                            <br/>
                            <input type="text" class="form-control input-circle" id="subject" placeholder="Subject">
                            <span style="color: red; display: block; font-size: 14px; float: left;" id="subjectError" ></span>
                            <br/>
                            <textarea class="textarea" name="textboxtext" id="messageContent"></textarea>
                            <span style="color: red; display: block; font-size: 14px; float: left;" id="messageError" ></span>
                        </div>
                        <div id="loading" style="display: none;">
                            <center>
                                <!--<span style="color: blue;">Processing...</span>-->
                                <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/24.gif" />
                            </center>
                        </div>
                        <div class="modal-footer" style="margin-top:0px;">
                            <span style="color: green; display: block; font-size: 14px; float: left;" id="mailResponseMessage" ></span>
                            <button type="button" class="btn btn-primary btn-circle" data-dismiss="modal">Close</button>
                            <button type="button" id="sendMailToUser" class="btn btn-success btn-circle">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="multiplemailModal" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="quick_mail_modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModal">New Email</h4>
                    </div>
                    <form>
                        <div class="modal-body" >
                            
                            <input type="text" id="" readonly class="form-control input-circle" value="Multiple Email"/>
                            <br/>
                            <input type="text" class="form-control input-circle" id="subject1" placeholder="Subject">
                            <span style="color: red; display: block; font-size: 14px; float: left;" id="subjectError1" ></span>
                            <br/>
                            <textarea class="textarea" name="textboxtext" id="messageContent1"></textarea>
                            <span style="color: red; display: block; font-size: 14px; float: left;" id="messageError1" ></span>
                        </div>
                        <div id="loading1" style="display: none;">
                            <center>
                                <!--<span style="color: blue;">Processing...</span>-->
                                <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/24.gif" />
                            </center>
                        </div>
                        <div class="modal-footer" style="margin-top:0px;">
                            <span style="color: green; display: block; font-size: 14px; float: left;" id="mailResponseMessage1" ></span>
                            <button type="button" class="btn btn-primary btn-circle" data-dismiss="modal">Close</button>
                            <button type="button" id="sendMailToMultipleUser" class="btn btn-success btn-circle">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="singlemessageModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="quick_mail_modal">
            <div class="modal-dialog" style="width: 661px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModal1">New Message</h4>
                    </div>
                <!-- dialog body -->
                    <div class="modal-body">
                        <div class="form-group clearfix"> 
                        <!--<label class="col-sm-1" style="display:block; margin-top: 11px; font-weight: bold;">To</label>-->
                            <div class="col-sm-12">
                                <input type="text" id="displaymessageinfo" readonly class="form-control input-circle" />
                            </div>
                            <br/>
                            <br/>
                            <label class="" style="font-weight: bold;">Text Message : </label>
                            <br/>
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class='box-body pad'>
                                        <textarea id="textmessage" name="textmessage" class="" placeholder="Place some text here" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                        <span style="color: red; display: block; font-size: 14px; float: left;" class="errortext" ></span> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- dialog buttons -->
                    <div class="modal-footer" style="margin-top: 0px;">
                        <span style="color: green; display: block; font-size: 14px; float: left;" class="messageResponse" ></span>
                        <button type="button" class="btn btn-success btn-circle" id="sendtextmessage">Send</button>
                        <button type="button" class="btn btn-primary btn-circle" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="multiplemessageModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="quick_mail_modal">
            <div class="modal-dialog" style="width: 661px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModal2">New Message</h4>
                    </div>
                <!-- dialog body -->
                    <div class="modal-body">
                        <div class="form-group clearfix"> 
                        <!--<label class="col-sm-1" style="display:block; margin-top: 11px; font-weight: bold;">To</label>-->
                            <div class="col-sm-12">
                                <input type="text" value="Multiple Number" readonly class="form-control input-circle" />
                            </div>
                            <br/>
                            <br/>
                            <label class="" style="font-weight: bold;">Text Message : </label>
                            <br/>
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class='box-body pad'>
                                        <textarea id="textmessage1" name="textmessage" class="" placeholder="Place some text here" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                        <span style="color: red; display: block; font-size: 14px; float: left;" id="errortext1" ></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- dialog buttons -->
                    <div class="modal-footer" style="margin-top: 0px;" >
                        <span style="color: green; display: block; font-size: 14px; float: left;" class="messageResponse" ></span>
                        <button type="button" class="btn btn-success btn-circle" id="sendtextmessage1">Send</button>
                        <button type="button" class="btn btn-primary btn-circle" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="activeClientModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="quick_mail_modal">
            <div class="modal-dialog" style="width: 661px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModal2">Comment</h4>
                    </div>
                <!-- dialog body -->
                    <div class="modal-body">
                        <div class="form-group clearfix"> 
                            <label class="" style="font-weight: bold;">Comment : </label>
                            <br/>
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class='box-body pad'>
                                        <textarea id="comment" name="comment" class="" placeholder="Place some text here" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                        <span style="color: red; display: block; font-size: 14px;" id="errorcomment" ></span> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- dialog buttons -->
                    <div class="modal-footer" style="margin-top: 0px;">
                        <span style="color: green; display: block; font-size: 14px; float: left;" class="activationResponse" ></span>
                        <button type="button" class="btn btn-success btn-circle" id="sendactivation">Send</button>
                        <button type="button" class="btn btn-primary btn-circle" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="deactiveClientModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="quick_mail_modal">
            <div class="modal-dialog" style="width: 661px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModal3">Comment</h4>
                    </div>
                <!-- dialog body -->
                    <div class="modal-body">
                        <div class="form-group clearfix"> 
                            <label class="" style="font-weight: bold;">Comment : </label>
                            <br/>
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class='box-body pad'>
                                        <textarea id="comment1" name="comment" class="" placeholder="Place some text here" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                        <span style="color: red; display: block; font-size: 14px;" id="errorcomment1" ></span> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- dialog buttons -->
                    <div class="modal-footer" style="margin-top: 0px;">
                        <span style="color: green; display: block; font-size: 14px; float: left;" class="deactivationResponse" ></span>
                        <button type="button" class="btn btn-success btn-circle" id="senddeactivation">Send</button>
                        <button type="button" class="btn btn-primary btn-circle" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
<!-- BEGIN CORE PLUGINS --> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
<!-- END CORE PLUGINS --> 
<!-- BEGIN THEME GLOBAL SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
<!-- END THEME GLOBAL SCRIPTS --> 
<!-- BEGIN THEME LAYOUT SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/ckeditor/adapters/jquery.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
<!-- END THEME LAYOUT SCRIPTS --> 

<!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="${pageContext.request.contextPath}/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="${pageContext.request.contextPath}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
		
		<script>
			$(function () {
				  $('[data-toggle="tooltip"]').tooltip();
				});
		</script>
                <script>
                    $(document).ready(function () {
                        $('#side-menu').find('li').removeClass('active');
                        $(document).find(".user").addClass('active');
                        
                        if('${message}'!==''){
                            $('#successMessage').show();
                            setTimeout(function()
                            {
                                $('#successMessage').hide();
                            }, 3000);
                        }
                    });
                </script>
                <script>
                    $(".checkboxes").on("click", function () {
                        var checkedData = [];
                        var status;
                        $("#performactions").find("input[type='checkbox'][name='userId']:checked").each(function () {
                            checkedData.push($(this).val());   
                            status = $(this).parent().parent().next().next().next().next().next().next().text();
                        });
                        if(checkedData.length === 1){
                            if((status).indexOf("Deactive") > -1){
                                $("#deactiveclient").hide();
                                $("#activeclient").show();
                            }else{
                                $("#deactiveclient").show();
                                $("#activeclient").hide();
                            }
                            $("#deleteclient").show();
                            $("#editclient").show();
                            $("#viewoneclient").show();
                            $("#exportOneClient").show();
                            $("#exportClient").hide();
                            $("#sendmultiplemail").hide();
                            $("#sendsinglemail").show();
                            $("#sendmultiplemessage").hide();
                            $("#sendsinglemessage").show();
                        }
                        else if (checkedData.length > 1) {
                                $("#activeclient").hide();
                                $("#deactiveclient").hide();
                                $("#deleteclient").show();
                                $("#editclient").hide();
                                $("#viewoneclient").hide();
                                $("#exportOneClient").hide();
                                $("#exportClient").hide();
                                $("#sendmultiplemail").show();
                                $("#sendsinglemail").hide();
                                $("#sendmultiplemessage").show();
                                $("#sendsinglemessage").hide();
                            }
                        else if (checkedData === null || checkedData.length === 0){
                                $("#activeclient").hide();
                                $("#deactiveclient").hide();
                                $("#deleteclient").hide();
                                $("#editclient").hide();
                                $("#viewoneclient").hide();
                                $("#exportOneClient").hide();
                                $("#exportClient").hide();
                                $("#sendmultiplemail").hide();
                                $("#sendsinglemail").hide();
                                $("#sendmultiplemessage").hide();
                                $("#sendsinglemessage").hide();
                        }
                    });
                    
                    $(".group-checkable").on("click", function () {
                        var checkedData = [];
                        var checkedboxes = [];
                        $("#performactions").find("input[type='checkbox'][name='userId']").each(function () {
                            checkedData.push($(this).val());      
                        });
                        $("#performactions").find("input[type='checkbox'][name='userId']:checked").each(function () {
                            checkedboxes.push($(this).val());   
                        });
                        console.log($(this).is(":checked"));
                        if($(this).is(":checked")){
                            console.log(checkedboxes);
                            if(checkedData === null || checkedData.length === 0){
                                $("#deleteclient").hide();
                            }else{
                                $("#deleteclient").show();
                                if(checkedboxes !== null || checkedboxes.length !== 0){
                                    $("#activeclient").hide();
                                    $("#deactiveclient").hide();
                                    $("#editclient").hide();
                                    $("#viewoneclient").hide();
                                    $("#exportClient").show();
                                    $("#exportOneClient").hide();
                                    $("#sendmultiplemail").show();
                                    $("#sendsinglemail").hide();
                                    $("#sendmultiplemessage").show();
                                    $("#sendsinglemessage").hide();
                                }
                            }
                        }else{
                            $("#activeclient").hide();
                            $("#deactiveclient").hide();
                            $("#deleteclient").hide();
                            $("#editclient").hide();
                            $("#viewoneclient").hide();
                            $("#exportClient").hide();
                            $("#exportOneClient").hide();
                            $("#sendmultiplemail").hide();
                            $("#sendsinglemail").hide();
                            $("#sendmultiplemessage").hide();
                            $("#sendsinglemessage").hide();
                        }
                        console.log(checkedData);
                    });
                </script>
                <script>
                    $("#viewoneclient").on("click", function () {
                        $("#performactions").find("input[type='checkbox'][name='userId']:checked").each(function () {
                            var checkeduserid=$(this).val();
                            var Uname = "${user.userName}".replace(" ", "");
                            window.location.href = "${pageContext.request.contextPath}/"+Uname+"/viewdetails/"+checkeduserid;
                        });
                    });
                    
                    $("#deactiveclient").on("click", function () {
                        $("#deactiveClientModal").modal("show");
                    });
                    $("#senddeactivation").on("click", function () {
                        var comment = $("#comment1").val();
                        var flag = true;
                        if(comment === null || comment === ""){
                            $("#errorcomment1").text("Please Enter a comment.");
                            flag = false;
                        }
                        if(flag){
                            $("#performactions").find("input[type='checkbox'][name='userId']:checked").each(function () {
                                var checkeduserid=$(this).val();
                                var Uname = "${user.userName}".replace(" ", "");
                                window.location.href = "${pageContext.request.contextPath}/"+Uname+"/deactivateclient/"+checkeduserid+"?comment="+comment;
                            });
                        }
                    });
                    
                    $("#activeclient").on("click", function () {
                        $("#activeClientModal").modal("show");
                    });
                    $("#sendactivation").on("click", function () {
                        var comment = $("#comment").val();
                        var flag = true;
                        if(comment === null || comment === ""){
                            $("#errorcomment").text("Please Enter a comment.");
                            flag = false;
                        }
                        if(flag){
                            $("#performactions").find("input[type='checkbox'][name='userId']:checked").each(function () {
                                var checkeduserid=$(this).val();
                                var Uname = "${user.userName}".replace(" ", "");
                                window.location.href = "${pageContext.request.contextPath}/"+Uname+"/activateclient/"+checkeduserid+"?comment="+comment;
                            });
                        }
                    });
                    
                    $("#editclient").on("click", function () {
                        var checkedData = [];
                        $("#performactions").find("input[type='checkbox'][name='userId']:checked").each(function () {
                            checkedData.push($(this).val());
                        });
                        if (checkedData === null || checkedData.length === 0) {
                            $("#selecterror").text("Please select a User.");
                        } else if (checkedData.length > 1) {
                            $("#selecterror").text("More than one User is selected.");
                        }else {
                            var Uname = "${user.userName}".replace(" ", "");
                           window.location.href = "${pageContext.request.contextPath}/"+Uname+"/updateclient/"+checkedData[0];
                        }
                        return false;
                    });
                    
                    $("#deleteclient").on("click", function(){
                        $("#confirm_delete").modal("show");
                    });
                    $("#Confirm").on("click", function(){
                        var checkedData = [];
                        $("#performactions").find("input[type='checkbox'][name='userId']:checked").each(function () {
                            checkedData.push($(this).val());
                        });
                        if (checkedData === null || checkedData.length === 0) {
                            $("#selecterror").text("Please select a User.");
                        } else {
                            var Uname = "${user.userName}".replace(" ", "");
                            $.get("${pageContext.request.contextPath}/"+Uname+"/deleteallclient", {userIds: checkedData},
                            function (data, status) {
                                if(data.indexOf("success") > -1){
                                    $("#deleteresponse").text(data.split("#")[1]);
                                    setTimeout(function()
                                    {
                                        window.location.href = "${pageContext.request.contextPath}/"+Uname+"/listview";
                                    }, 1000);
                                   
                                } else{
                                    $("#selecterror").text(data);
                                }
                            });
                        }
                    });
                </script>
                <script>
                    $("#exportOneClient").on("click", function(){
                        var checkedData = [];
                        $("#performactions").find("input[type='checkbox'][name='userId']:checked").each(function () {
                            checkedData.push($(this).val());
                        });
                        if (checkedData === null || checkedData.length === 0) {
                            $("#selecterror").text("Please select a User.");
                        } else if (checkedData.length > 1) {
                            $("#selecterror").text("More than one User is selected.");
                        }else {
                            var Uname = "${user.userName}".replace(" ", "");
                           window.location.href = "${pageContext.request.contextPath}/"+Uname+"/exportoneuser?userId="+checkedData[0];
                        }
                    });
                    
                    $("#exportClient").on("click", function(){  
                        var Uname = "${user.userName}".replace(" ", "");
                        window.location.href = "${pageContext.request.contextPath}/"+Uname+"/exportuser";
                    });
                </script>
                <script>
                    $(function() {
                        $(".textarea").ckeditor();
                    });
                    
                    var mailuser;
                    $('#sendsinglemail').on("click", function (e) {
                        $("#mailResponseMessage").html("");
                        $("#subjectError").html("");
                        $("#messageError").html("");
                        $("#performactions").find("input[type='checkbox'][name='userId']:checked").each(function () {
                            mailuser = $(this).val();
                        });
                        $('.cke_wysiwyg_frame').contents().find('body').html("");
                        $("#subject").val("");

                        var Uname = "${user.userName}".replace(" ", "");
                        $.post("${pageContext.request.contextPath}/"+Uname+"/getuserinfofordisplay", {userId: mailuser},
                            function (data, status) {
                                if(data.indexOf("success") > -1){
                                    var jsondata = JSON.parse(data.split("#")[1]);
                                    $("#displaymailinfo").val(jsondata.Name+" ( "+jsondata.Email+" )");
                                    $("#subjectError").html("");
                                    $("#messageError").html("");
                                    $("#subject").val("");
                                    $("#messageContent").val("");
                                    $("#singlemailModal").modal("show");
                                } else{
                                    console.log(data);
                                }
                            });
                    });
            
                    $("#sendMailToUser").on("click", function () {
                        $("#subjectError").html("");
                        $("#messageError").html("");
                        var subject = $("#subject").val();
                        var messageContent = $("#messageContent").val();
                        var flag = true;
                        var flag1 = true;

                        if (subject ==="") {
                            $("#subjectError").html("The Subject field is required.");
                            flag1 = false;
                        } else if(subject.length <=3 || subject.length >=101) {
                            $("#subjectError").html("Subject accepts 4-100 character only");
                            flag1 = false;
                        }else{
                            flag1 = true;
                        }

                        if (messageContent=== "") {
                            $("#messageError").html("The Message field is required.");
                            flag = false;
                        }else if(messageContent.length === 9){
                            $("#messageError").html("The Message field is required.");
                        }else if ((messageContent.length-9) <2 || (messageContent.length-9) >=1001) {
                            console.log(messageContent.length);
                            $("#messageError").html("Message accepts 4-1000 character only");
                            flag = false;
                        }else{
                            console.log(messageContent);
                            console.log(messageContent.length);
                            flag = true;
                        }

                        if(flag && flag1){
                            var Uname = "${user.userName}".replace(" ", "");
                            $("#loading").css('display','block');
                            $.post("${pageContext.request.contextPath}/"+Uname+"/sendmailtouser", {userId: mailuser, subject: subject, message: messageContent},
                            function (data, status) {
                                if(data === "success"){
                                    $("#loading").css("display","none");
                                    $("#mailResponseMessage").html("Email has been sent successfully.");
                                    setTimeout(function()
                                    {
                                        $("#singlemailModal").modal("hide");
                                    }, 1000);
                                } else{
                                    $("#loading").css("display","none");
                                    $("#mailResponseMessage").html(data);
                                }
                            });
                        }
                    });

                    $("#subject").on("change", function(){
                        $("#mailResponseMessage").html("");
                        var subject = $("#subject").val();
                        if (subject ==="") {
                            $("#subjectError").html("The Subject field is required.");
                        }else{
                            $("#subjectError").html("");
                        }
                    });
                    $("#messageContent").on("change", function(){
                        $("#mailResponseMessage").html("");
                        var messageContent = $("#messageContent").val();
                        if (messageContent ==="") {
                            $("#messageError").html("The Message field is required.");
                        }else{
                            $("#messageError").html("");
                        }
                    });
                    
                    $("#sendmultiplemail").on("click", function(){
                        $("#subjectError1").html("");
                        $("#messageError1").html("");
                        $("#subject1").val("");
                        $("#messageContent1").val("");
                        $("#multiplemailModal").modal("show");
                    });
                    $("#sendMailToMultipleUser").on("click", function(){
                        $("#subjectError1").html("");
                        $("#messageError1").html("");
                        var subject = $("#subject1").val();
                        var messageContent = $("#messageContent1").val();
                        var flag = true;
                        var flag1 = true;

                        if (subject ==="") {
                            $("#subjectError1").html("The Subject field is required.");
                            flag1 = false;
                        } else if(subject.length <=3 || subject.length >=101) {
                            $("#subjectError1").html("Subject accepts 4-100 character only");
                            flag1 = false;
                        }else{
                            flag1 = true;
                        }

                        if (messageContent=== "") {
                            $("#messageError1").html("The Message field is required.");
                            flag = false;
                        }else if(messageContent.length === 9){
                            $("#messageError1").html("The Message field is required.");
                        }else if ((messageContent.length-9) <=3 || (messageContent.length-9) >=1001) {
                            console.log(messageContent.length);
                            $("#messageError1").html("Message accepts 4-1000 character only");
                            flag = false;
                        }else{
                            console.log(messageContent);
                            console.log(messageContent.length);
                            flag = true;
                        }

                        if(flag && flag1){
                            var checkedData = [];
                            $("#performactions").find("input[type='checkbox'][name='userId']:checked").each(function () {
                                checkedData.push($(this).val());
                            });
                            var Uname = "${user.userName}".replace(" ", "");
                            $("#loading1").css('display','block');
                            $.post("${pageContext.request.contextPath}/"+Uname+"/sendmailtomultipleuser", {userIds: checkedData, subject: subject, message: messageContent},
                            function (data, status) {
                                if(data === "success"){
                                    $("#loading1").css("display","none");
                                    $("#mailResponseMessage1").html("Email has been sent successfully.");
                                    setTimeout(function()
                                    {
                                        $("#multiplemailModal").modal("hide");
                                    }, 1000);
                                } else{
                                    $("#loading1").css("display","none");
                                    $("#mailResponseMessage1").html(data);
                                }
                            });
                        }
                    });
                </script>
                <script>
                    var messageuserid;
                    $("#sendsinglemessage").on("click", function(){
                        $(".messageResponse").html("");
                        $(".errortext").html("");
                        $("#performactions").find("input[type='checkbox'][name='userId']:checked").each(function () {
                            messageuserid = $(this).val();
                        });
                        $('.cke_wysiwyg_frame').contents().find('body').html("");
                        $("#textmessage").val("");

                        var Uname = "${user.userName}".replace(" ", "");
                        $.post("${pageContext.request.contextPath}/"+Uname+"/getuserinfofordisplay", {userId: messageuserid},
                            function (data, status) {
                                if(data.indexOf("success") > -1){
                                    var jsondata = JSON.parse(data.split("#")[1]);
                                    $("#displaymessageinfo").val(jsondata.Name+" ( "+jsondata.Mobile+" )");
                                    $("#singlemessageModal").modal("show");
                                } else{
                                    console.log(data);
                                }
                            });
                    });
                    
                    $("#sendtextmessage").on("click", function(){
                        var text = $("#textmessage").val();
                        var flag = true;
                        console.log(text);
                        if(text === ""){
                            $(".errortext").text("Please Enter text.");
                            flag = false;
                        } else if(text.length > 160){
                            $(".errortext").text("Limit should be 160, User can not Write After 160 Characters.");
                            flag = false;
                        }
                        if(flag){
                            var checkedData = [];
                            $("#performactions").find("input[type='checkbox'][name='userId']:checked").each(function () {
                                checkedData.push($(this).val());
                            });
                            var Uname = "${user.userName}".replace(" ", "");
//                            $("#loading1").css('display','block');
                            $.post("${pageContext.request.contextPath}/"+Uname+"/sendmessagetousers", {userIds: checkedData, text: text},
                            function (data, status) {
                                if(data === "success"){
//                                    $("#loading1").css("display","none");
                                    $(".messageResponse").html("Message has been sent successfully.");
                                    setTimeout(function()
                                    {
                                        $("#singlemessageModal").modal("hide");
                                    }, 1000);
                                } else{
//                                    $("#loading1").css("display","none");
                                    $(".messageResponse").html(data);
                                }
                            });
                        }
                    });
                    
                    $("#sendmultiplemessage").on("click", function(){
                        $("#textmessage1").val("");
                        $("#errortext1").text("");
                        $("#mailResponseMessage1").html("");
                        $("#multiplemessageModal").modal("show");
                    });
                    
                    $("#sendtextmessage1").on("click", function(){
                        var text = $("#textmessage1").val();
                        var flag = true;
                        console.log(text);
                        if(text === ""){
                            $("#errortext1").text("Please Enter text.");
                            flag = false;
                        } else if(text.length > 160){
                            $("#errortext1").text("Limit should be 160, User can not Write After 160 Characters.");
                            flag = false;
                        }
                        if(flag){
                            var checkedData = [];
                            $("#performactions").find("input[type='checkbox'][name='userId']:checked").each(function () {
                                checkedData.push($(this).val());
                            });
                            var Uname = "${user.userName}".replace(" ", "");
//                            $("#loading1").css('display','block');
                            $.post("${pageContext.request.contextPath}/"+Uname+"/sendmessagetousers", {userIds: checkedData, text: text},
                            function (data, status) {
                                if(data === "success"){
//                                    $("#loading1").css("display","none");
                                    $("#mailResponseMessage1").html("Message has been sent successfully.");
                                    setTimeout(function()
                                    {
                                        $("#multiplemessageModal").modal("hide");
                                    }, 1000);
                                } else{
//                                    $("#loading1").css("display","none");
                                    $("#mailResponseMessage1").html(data);
                                }
                            });
                        }
                    });
                </script>
</body>
</html>