/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author CoreTechies M8
 */
@Entity
@Table(name = "userlicense")
@Access(AccessType.FIELD)
public class UserLicense implements Serializable {

    @Id
    @GeneratedValue(generator = "userlicense")
    @GenericGenerator(name = "userlicense", strategy = "increment")
    private Integer userLicenseId;

    @Column
    private Integer userId;

    @Column
    private Integer courseId;

    @Column
    private Integer validity;

    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date issueDate;

    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date expiryDate;

    @Column
    private Integer systemInfoId;

    @Column
    private String license;

    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date createOn;

    @Column
    private Integer createdBy;

    @Column
    private Boolean status;

    @Column
    private Integer watermarkId;
    
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date updateOn;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId", insertable = false, updatable = false, nullable = false, unique = false)
    private UserInfo userInfo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "courseId", insertable = false, updatable = false, nullable = false, unique = false)
    private Course course;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "systemInfoId", insertable = false, updatable = false, nullable = false, unique = false)
    private SystemInfo systemInfo;

    public Integer getUserLicenseId() {
        return userLicenseId;
    }

    public void setUserLicenseId(Integer userLicenseId) {
        this.userLicenseId = userLicenseId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getValidity() {
        return validity;
    }

    public void setValidity(Integer validity) {
        this.validity = validity;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Integer getSystemInfoId() {
        return systemInfoId;
    }

    public void setSystemInfoId(Integer systemInfoId) {
        this.systemInfoId = systemInfoId;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public Date getCreateOn() {
        return createOn;
    }

    public void setCreateOn(Date createOn) {
        this.createOn = createOn;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(Date updateOn) {
        this.updateOn = updateOn;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public SystemInfo getSystemInfo() {
        return systemInfo;
    }

    public void setSystemInfo(SystemInfo systemInfo) {
        this.systemInfo = systemInfo;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Integer getWatermarkId() {
        return watermarkId;
    }

    public void setWatermarkId(Integer watermarkId) {
        this.watermarkId = watermarkId;
    }

    @Override
    public String toString() {
        return "UserLicense{" + "userLicenseId=" + userLicenseId + ", userId=" + userId + ", courseId=" + courseId + ", validity=" + validity + ", issueDate=" + issueDate + ", expiryDate=" + expiryDate + ", systemInfoId=" + systemInfoId + ", license=" + license + ", createOn=" + createOn + ", createdBy=" + createdBy + ", status=" + status + ", watermarkId=" + watermarkId + ", updateOn=" + updateOn + '}';
    }
    
}
