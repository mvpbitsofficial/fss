/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.service;

import com.coretechies.filesecuritysystem.domain.AdminLicense;
import com.coretechies.filesecuritysystem.domain.Course;
import com.coretechies.filesecuritysystem.domain.SystemInfo;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.domain.UserLicense;
import com.coretechies.filesecuritysystem.security.InvalidInformationException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author CoreTechies M8
 */
public interface UserLicenseService {

    public UserLicense getUserLicense(Integer userInfoId, Integer useruserId, Integer courseId, Integer validity, Date startDate, Date endDate, Integer systemId, Integer watermarkId) throws InvalidInformationException;

    public String saveUserLicense(UserLicense userLicense, Integer creatorId);

    public List<UserLicense> getLicenseExpireAlert();

    public Integer getUserLicenseCountByCreator(Integer createdBy);

    public List<UserLicense> getUserLicenseListByCreateOn(Integer createdBy, Date fromDate, Date toDate);

    public List<UserLicense> getUserLicenseListByExpiryDate(Integer createdBy, Date fromDate, Date toDate);
    
    public List<UserLicense> getUserLicenseByUser(Integer userId);

    public List<UserLicense> getUserLicenseForChart(Integer createdBy, Date date);
    
    public UserLicense getConfirmLicenseForDisplay();
    
    public UserLicense renewUserLicense(UserInfo userInfo,Integer licenseId, Course course, SystemInfo systemInfo, Integer validity, Date startdate, Date endDate, Integer createdby, AdminLicense adminLicense) throws InvalidInformationException, ParseException;
    
    public Integer getTotalUsedCourseCount(Integer courseid);
}
