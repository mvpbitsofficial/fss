/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.domain;

import java.io.Serializable;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author CoreTechies M8
 */
@Entity
@Table(name = "securityquestionanswer")
@Access(AccessType.FIELD)
public class SecurityQuestionAnswer implements Serializable {

    @Id
    @GeneratedValue(generator = "securityquestionanswer")
    @GenericGenerator(name = "securityquestionanswer", strategy = "increment")
    private Integer securityQuestionAnswerId;

    @Column
    private Integer securityQuestionId;

    @Column
    private String answer;

    @Column
    private Integer userId;

    public Integer getSecurityQuestionAnswerId() {
        return securityQuestionAnswerId;
    }

    public void setSecurityQuestionAnswerId(Integer securityQuestionAnswerId) {
        this.securityQuestionAnswerId = securityQuestionAnswerId;
    }

    public Integer getSecurityQuestionId() {
        return securityQuestionId;
    }

    public void setSecurityQuestionId(Integer securityQuestionId) {
        this.securityQuestionId = securityQuestionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "SecurityQuestionAnswer{" + "securityQuestionAnswerId=" + securityQuestionAnswerId + ", securityQuestionId=" + securityQuestionId + ", answer=" + answer + ", userId=" + userId + '}';
    }

}
