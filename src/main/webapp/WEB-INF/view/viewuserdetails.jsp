<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <head>
        <meta charset="utf-8" />
        <title>FSS | User Information</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->

        <link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
        <style>
            .cke_panel {
                z-index: 10060!important;
            }
            .user-alphabate {
                display: block;
                position: relative;
                -webkit-transition: all .4s linear;
                transition: all .4s linear;
                width: 100%;
                height: 150px;
                line-height: 150px;
                background: #36c6d3;
                font-size: 30px;
                color: #fff;
                text-shadow: 0px 1px 2px rgba(0,0,0,0.2);
            }
            .big-text {
                font-size: 45px;
            }
            .mt-element-overlay .mt-overlay-1:hover .user-alphabate {
                -ms-transform: scale(1.2) translateZ(0);
                -webkit-transform: scale(1.2) translateZ(0);
                transform: scale(1.2) translateZ(0);
            }
        </style>
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top"> 
            <!-- BEGIN HEADER INNER -->
            <jsp:include page="topbar.jsp" />
            <!-- END HEADER INNER --> 
        </div>
        <!-- END HEADER --> 
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER --> 
        <!-- BEGIN CONTAINER -->
        <div class="page-container"> 
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper"> 
                <!-- BEGIN SIDEBAR --> 
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse"> 
                    <!-- BEGIN SIDEBAR MENU -->
                    <jsp:include page="sidebar.jsp" />
                    <!--   <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                      DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
                     <li class="sidebar-toggler-wrapper hide"> 
                       BEGIN SIDEBAR TOGGLER BUTTON 
                      <div class="sidebar-toggler"> <span></span> </div>
                       END SIDEBAR TOGGLER BUTTON  
                     </li>
                     <li class="nav-item start "> <a href="index.html" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
                     <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
                      
                     </li>
                     <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">License</span>  </a>
                      
                     </li>
                     <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
                      
                     </li>
                     <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
                     <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
                     <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
                     <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
                     <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
                    </ul>--> 
                    <!-- END SIDEBAR MENU --> 
                    <!-- END SIDEBAR MENU --> 
                </div>
                <!-- END SIDEBAR --> 
            </div>
            <!-- END SIDEBAR --> 
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper"> 
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content"> 
                    <!-- BEGIN PAGE HEADER--> 

                    <!-- BEGIN PAGE TITLE-->
                    <h3 class="page-title"> User Profile 
                        <!-- <small>blank page layout</small>--> 
                    </h3>
                    <!-- END PAGE TITLE--> 
                    <!-- END PAGE HEADER-->

                    <div class="row">
                        <div class="col-md-12"> 
                            <!-- BEGIN PROFILE SIDEBAR -->
                            <div class="profile-sidebar mt-element-card mt-card-round mt-element-overlay"> 
                                <!-- PORTLET MAIN -->
                                <div class="portlet light profile-sidebar-portlet bordered box-shadow"> 
                                    <!-- SIDEBAR USERPIC -->
                                    <div class="mt-card-item mt-widget-1 clearfix"  style=" padding:00px 70px 00px; border:none;">
                                        <div class="mt-card-avatar mt-overlay-1 " style="margin-bottom:0px;"> <span class="user-alphabate big-text">${userdetail.displayName}</span> </div>
                                    </div>
                                    <!-- END SIDEBAR USERPIC --> 
                                    <!-- SIDEBAR USER TITLE -->
                                    <div class="profile-usertitle">
                                        <div class="profile-usertitle-name">${userdetail.userName}</div>
                                        <div class="profile-usertitle-job" style="text-transform:none;"> <a href="javascript:;">${userdetail.emailId}</a> </div>
                                        <div class="profile-usertitle-job" style="text-transform:none;">+${userdetail.countryCode.mobileCode}-${userdetail.mobileNumber}</div>
                                        <div class="profile-userbuttons" style="margin-bottom:25px;"> <a  class="btn btn-circle btn-success btn-sm sendMail" value="${userdetail.userInfoId}" data-toggle="tooltip" data-placement="top" title="Send Email"><i class="fa fa-envelope"></i> Email</a>
                                            <c:if test="${userdetail.mobileCode==99}"> <a href="javascript:;" class="btn btn-circle btn-info btn-sm sendMessage" value="${userdetail.userInfoId}" data-toggle="tooltip" data-placement="top" title="Send Message "> <i class="fa fa-comment"></i> Message</a> </c:if>
                                            </div>
                                        </div>

                                        <!--<div class="btn-group btn-group btn-group-justified" role="group" aria-label="..." style="margin-top:25px;">
                                                                                                                   <div class="btn-group" role="group">
                                                                                                                         <button type="button" class="btn  btn-success"><i class="icon-speech"></i> Message</button>
                                                                                                                   </div>
                                                                                                                  
                                                                                                                   <div class="btn-group" role="group">
                                                                                                                         <button type="button" class="btn btn-success"><i class="icon-envelope-letter"></i> Mail</button>
                                                                                                                   </div>
                                                                                                                 </div>--> 

                                        <!-- END SIDEBAR USER TITLE --> 
                                        <!-- SIDEBAR BUTTONS --> 
                                        <!--<div class="profile-userbuttons">
                                                                         <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                                                                         <button type="button" class="btn btn-circle red btn-sm">Message</button>
                                                                     </div>--> 
                                        <!-- END SIDEBAR BUTTONS --> 
                                        <!-- SIDEBAR MENU --> 

                                        <!-- END MENU --> 
                                    </div>
                                    <div class="portlet light bordered box-shadow"> 
                                        <!-- STAT -->
                                        <div class="row list-separated ">
                                            <div class="col-md-4 col-sm-4 col-xs-6">
                                                <div class="uppercase profile-stat-title">${totalLicense}</div>
                                            <div class="uppercase profile-stat-text"> Course </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <div class="uppercase profile-stat-title">${totalLicense} </div>
                                            <div class="uppercase profile-stat-text"> License </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <div class="uppercase profile-stat-title">${totalSystem}</div>
                                            <div class="uppercase profile-stat-text"> System </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- END PORTLET MAIN --> 
                                <!-- PORTLET MAIN --> 

                                <!-- END PORTLET MAIN --> 
                            </div>
                            <!-- END BEGIN PROFILE SIDEBAR --> 
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered  box-shadow">
                                            <div class="portlet-title tabbable-line">
                                                <div class="caption caption-md"> <i class="icon-globe theme-font hide"></i> <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span> </div>
                                                <ul class="nav nav-tabs">
                                                    <li class="active"> <a href="#tab_1_1" data-toggle="tab">Personal Info</a> </li>
                                                    <li> <a href="#tab_1_2" data-toggle="tab">Courses</a> </li>
                                                    <li> <a href="#tab_1_3" data-toggle="tab">System</a> </li>
                                                    <li> <a href="#tab_1_4" data-toggle="tab">License</a> </li>
                                                </ul>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="tab-content"> 
                                                    <!-- PERSONAL INFO TAB -->
                                                    <div class="tab-pane active" id="tab_1_1">
                                                        <form action="#" class="form-horizontal">
                                                            <div class="form-body">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">User ID</label>
                                                                    <div class="col-sm-6">
                                                                        <div class="input-group"> <span class="input-group-addon input-circle-left"> <i class="fa fa-user"></i> </span>
                                                                            <input type="text" readonly class="form-control input-circle-right" value="AD${USERID}">
                                                                        </div>
                                                                        <!--<span class="help-block"> A block of help text. </span>--> 
                                                                    </div>
                                                                </div>
                                                                <!--                                                        <div class="form-group">
                                                                                                              <label class="col-sm-3 control-label">User Name</label>
                                                                                                              <div class="col-sm-8">
                                                                                                                  <div class="input-group">
                                                                                                                          <span class="input-group-addon input-circle-left">
                                                                                                                                  <i class="fa fa-user"></i>
                                                                                                                          </span>
                                                                                                                          <input type="text" readonly class="form-control input-circle-right" value="${userdetail.userName}">
                                                                                                                  </div>
                                                                                                                  <span class="help-block"> A block of help text. </span>
                                                                                                              </div>
                                                                                                          </div>--> 
                                                                <!--							<div class="form-group">
                                                                                                              <label class="col-sm-3 control-label">Last Name</label>
                                                                                                              <div class="col-sm-8">
                                                                                                                  <div class="input-group">
                                                                                                                          <span class="input-group-addon input-circle-left">
                                                                                                                                  <i class="fa fa-user"></i>
                                                                                                                          </span>
                                                                                                                          <input type="text" class="form-control input-circle-right" placeholder="Enter Last Name">
                                                                                                                  </div>
                                                                                                                  <span class="help-block"> A block of help text. </span>
                                                                                                              </div>
                                                                                                          </div>-->
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Email Address</label>
                                                                    <div class="col-sm-6">
                                                                        <div class="input-group"> <span class="input-group-addon input-circle-left"> <i class="fa fa-envelope"></i> </span>
                                                                            <input type="text" readonly class="form-control input-circle-right" value="${userdetail.emailId}">
                                                                        </div>
                                                                        <!--<span class="help-block"> A block of help text. </span>--> 
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Mobile No.</label>
                                                                    <div class="col-sm-6">
                                                                        <div class="input-group"> <span class="input-group-addon input-circle-left"> <i class="fa fa-phone"></i> </span>
                                                                            <input type="text" readonly class="form-control input-circle-right" value="+${userdetail.countryCode.mobileCode}-${userdetail.mobileNumber}">
                                                                        </div>
                                                                        <!--<span class="help-block"> A block of help text. </span>--> 
                                                                    </div>
                                                                </div>
                                                                <!--							<div class="form-group">
                                                                                                              <label class="col-sm-3 control-label">Occupation</label>
                                                                                                              <div class="col-sm-8">
                                                                                                                  <input type="text" class="form-control input-circle" placeholder="Enter Your Occupation">
                                                                                                                  <span class="help-block"> A block of help text. </span>
                                                                                                              </div>
                                                                                                          </div>--> 
                                                                <!--                                                       <div class="form-group">
                                                                                                              <label class="col-sm-3 control-label">About</label>
                                                                                                              <div class="col-sm-8">
                                                                                                                  <textarea class="form-control input-circle" placeholder=""></textarea>
                                                                                                                  <span class="help-block"> A block of help text. </span>
                                                                                                              </div>
                                                                                                          </div>-->

                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">Country</label>
                                                                    <div class="col-sm-6">
                                                                        <div class="input-group"> <span class="input-group-addon input-circle-left"> <i class="fa fa-flag"></i> </span>
                                                                            <input type="text" readonly class="form-control input-circle-right" value="${userdetail.countryCode.country}">
                                                                        </div>
                                                                        <!--<span class="help-block"> A block of help text. </span>--> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-offset-4 col-md-6 text-right"> 
                                                                        <!--                                                                <button type="submit" class="btn btn-circle green">Save</button>
                                                                                                                        <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>--> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <!-- END PERSONAL INFO TAB --> 
                                                    <!-- CHANGE AVATAR TAB -->
                                                    <div class="tab-pane" id="tab_1_2">
                                                        <div class="panel-group accordion" id="courses-collapse">
                                                            <div class="panel panel-default">
                                                                <c:forEach items="${license}" var="license" varStatus="status">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title"> <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#courses-collapse" href="#${status.count}" aria-expanded="false">${license.course.courseName}</a> </h4>
                                                                    </div>
                                                                    <div id="${status.count}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                                        <div class="panel-body">
                                                                            <table class="table table-light detail-table">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="font-weight: bold;">Course ID </td>
                                                                                        <td>${license.course.courseId}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="font-weight: bold;">Description </td>
                                                                                        <td> ${license.course.courseDescription} </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </c:forEach>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- END CHANGE AVATAR TAB --> 
                                                    <!-- CHANGE PASSWORD TAB -->
                                                    <div class="tab-pane" id="tab_1_3">
                                                        <div class="panel-group accordion" id="system-collapse">
                                                            <div class="panel panel-default">
                                                                <c:forEach items="${systeminfo}" var="system" varStatus="status1">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title"> <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#system-collapse" href="#system${status1.count}" aria-expanded="false"  onclick="systemdetails(${status1.count}, ${system.systemInfoId});">${system.systemName}</a> </h4>
                                                                    </div>
                                                                    <div id="system${status1.count}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                                        <div class="panel-body">
                                                                            <table class="table table-light detail-table">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="font-weight: bold;">System ID </td>
                                                                                        <td class="clearfix"><span class="text-ellips" id="systemId${status1.count}">${system.systemId}</span> <a href="javascript:;" class="btn btn-sm btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Copy SystemId" id="copy${status1.count}" onclick="copysystemid(${status1.count});"><i class="fa fa-copy"></i></a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="font-weight: bold;">MAC ID </td>
                                                                                        <td class="clearfix"><span class="text-ellips" id="macid${status1.count}"></span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="font-weight: bold;">Serial Number </td>
                                                                                        <td class="clearfix"><span class="text-ellips" id="serialnumber${status1.count}"></span></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <span class="copyresponse" style="color: green; display: block; font-size: 14px;"></span>
                                                                            <span id="systemerror" style="color: red; display: block; font-size: 14px;"></span> </div>
                                                                    </div>
                                                                </c:forEach>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- END CHANGE PASSWORD TAB --> 
                                                    <!-- PRIVACY SETTINGS TAB -->
                                                    <div class="tab-pane" id="tab_1_4">
                                                        <div class="panel-group accordion" id="license-collapse">
                                                            <div class="panel panel-default">
                                                                <c:forEach items="${license}" var="license" varStatus="status2">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <c:choose>
                                                                                <c:when test="${license.expiryDate == null}"> <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#license-collapse" href="#license${status2.count}" aria-expanded="false">${license.course.courseName} ( Unlimited )</a> </c:when>
                                                                                <c:otherwise> <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#license-collapse" href="#license${status2.count}" aria-expanded="false">${license.course.courseName} (
                                                                                        <fmt:formatDate value="${license.issueDate}" pattern="dd-MM-yyyy" />
                                                                                        to
                                                                                        <fmt:formatDate value="${license.expiryDate}" pattern="dd-MM-yyyy" />
                                                                                        ) </a> </c:otherwise>
                                                                                </c:choose>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="license${status2.count}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                                        <div class="panel-body">
                                                                            <table class="table table-light detail-table">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="font-weight: bold;">Course Name </td>
                                                                                        <td>${license.course.courseName}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="font-weight: bold;">Validity </td>
                                                                                        <c:choose>
                                                                                            <c:when test="${license.expiryDate == null}">
                                                                                                <td> Unlimited </td>
                                                                                            </c:when>
                                                                                            <c:otherwise>
                                                                                                <td><fmt:formatDate value="${license.issueDate}" pattern="dd-MM-yyyy" />
                                                                                                    to
                                                                                                    <fmt:formatDate value="${license.expiryDate}" pattern="dd-MM-yyyy" /></td>
                                                                                                </c:otherwise>
                                                                                            </c:choose>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="font-weight: bold;">System ID </td>
                                                                                        <td class="clearfix"><span class="text-ellips" id="SystemId" >${license.systemInfo.systemId}</span><a href="javascript:;" class="btn btn-sm btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Copy System Id" id="copysystem"><i class="fa fa-copy"></i></a></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="font-weight: bold;">License Key</td>
                                                                                        <td class="clearfix"><span class="text-ellips" id="licensekey" > ${license.license} </span><a href="javascript:;" class="btn btn-sm btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Copy License Key" id="copy"><i class="fa fa-copy"></i></a></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <span class="copyresponse" style="color: green; display: block; font-size: 14px;"></span>
                                                                        <c:if test="${license.expiryDate != null}">
                                                                            <div class="margin-top-8" style="text-align: right;"> <span class="renewerror" style="color: red; display: block; font-size: 14px;"></span> <a href="javascript:;"  id="${license.userLicenseId}" class="btn red renew"> Renew</a> </div>
                                                                        </c:if>
                                                                    </div>
                                                                </c:forEach>
                                                            </div>
                                                        </div>
                                                        <!--                                                        <form action="#">
                                                        <c:forEach items="${license}" var="license">
                                                            <table class="table table-light detail-table">
                                                                <tbody><tr>
                                                                    <td>Course Name :</td>
                                                                    <td>${license.course.courseName}</td>
                                                                </tr>
                                                                <tr>
                                                                        <td>Organization :</td>
                                                                    <td>ABC</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Validity :</td>
                                                                    <td>${license.issueDate} to ${license.expiryDate}</td>
                                                                </tr>
                                                                 <tr>
                                                                    <td>License Limit :</td>
                                                                    <td>Unlimited</td>
                                                                </tr>
                                                                 <tr>
                                                                    <td>License Key :</td>
                                                                    <td class="clearfix">
                                                                        <span class="text-ellips">${license.license}</span><a href="#" class="pull-right btn btn-sm btn-success"><i class="fa fa-copy"></i></a>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                       
                                                        end profile-settings
                                                        <div class="margin-top-10">
                                                            <a href="javascript:;"   data-toggle="modal" data-target="#renew_modal" class="btn red"> Renew</a>
                                                             <a href="javascript:;" class="btn default"> Cancel </a>
                                                        </div>
                                                        </c:forEach>
                                                    </form>--> 
                                                    </div>
                                                    <!-- END PRIVACY SETTINGS TAB --> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PROFILE CONTENT --> 
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY --> 
            </div>
            <!-- END CONTENT --> 
            <!-- BEGIN QUICK SIDEBAR --> 
            <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 

            <!-- END QUICK SIDEBAR --> 
        </div>
        <!-- END CONTAINER --> 
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; Organization </div>
            <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
        </div>
        <!-- END FOOTER --> 
        <!-- Modal -->
<!--        <div class="modal fade" id="renew_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Renew License</h4>
                    </div>
                    <form>
                        <div class="modal-body">
                            <input type="hidden" id="licenseId"/>
                            <input type="hidden" id="useruserid"/>
                            <input type="hidden" id="courseid"/>
                            <input type="hidden" id="systemid"/>
                            <div class="form-group clearfix">
                                <label>User Name </label>
                                <input class="form-control" readonly value="XYZ" id="userName">
                            </div>
                            <div class="form-group clearfix">
                                <label>Course Name </label>
                                <input class="form-control" readonly value="ABC" id="coursename">
                            </div>
                            <div class="form-group clearfix">
                                <select class="form-control" id="validity">
                                    <option selected="" disabled="">Select Validity</option>
                                    <c:forEach var="i" begin="1" end="20">
                                        <option value="${i}"> ${i}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="form-group clearfix">
                                <div class="one-half-50">
                                    <div class="one">
                                        <input class="form-control" readonly placeholder=" Issue Date" id="issueDate">
                                    </div>
                                    <div class="one">
                                        <input class="form-control" placeholder=" Expiry Date" id="expiryDate">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label style=" margin-right:15px;"> System ID:</label>
                                <input readonly class="form-control" value="4f5454d64dsf5dsf4d5s4fdsf3ds4f54ds5f4s" id="systemId">
                            </div>
                            <div class="form-group clearfix">
                                <label style=" margin-right:15px;"> License Key:</label>
                                <div class="two-half">
                                    <div class="one">
                                        <input class="form-control" placeholder=" License Key" value="" id="licenceKey" readonly>
                                    </div>
                                    <div class="two"> <a id="copylicense" href="javascript:;" class="btn  btn-default"><i class="fa fa-copy"></i></a> <a href="javascript:;" class="btn  btn-default"><i class="fa fa-download"></i></a> </div>
                                </div>
                            </div>
                            <span class="renew_error" style="color: red; display: block; font-size: 14px;"></span> </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-default" >Reset</button>
                            data-dismiss="modal"
                            <button type="button" class="btn green" id="renew_license">Renew</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>-->

        <!-- Sending Eamil to User Pop up   -->

        <div class="modal fade" id="mailModal" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="quick_mail_modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">New Email</h4>
                    </div>
                    <form>
                        <div class="modal-body" >
                            <input type="text" value="${userdetail.userName} ( ${userdetail.emailId} )" readonly class="form-control input-circle" />
                            <br/>
                            <input type="text" class="form-control input-circle" id="subject" placeholder="Subject">
                            <span style="color: red; display: block; font-size: 14px;" id="subjectError" ></span>
                            <br/>
                            <textarea class="textarea" name="textboxtext" id="messageContent"></textarea>
                            <span style="color: red; display: block; font-size: 14px;" id="messageError" ></span> </div>
                        <div id="loading" style="display: none;">
                            <center>
                                <!--<span style="color: blue;">Processing...</span>--> 
                                <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/24.gif" />
                            </center>
                        </div>
                        <div class="modal-footer" style="margin-top:0px;">
                            <span style="color: green; display: block; font-size: 14px; float: left;" id="mailResponseMessage" ></span>
                            <button type="button" class="btn btn-circle btn-danger" data-dismiss="modal">Close</button>
                            <button type="button" id="sendMailToUser" class="btn btn-circle btn-success">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="messageModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="quick_mail_modal">
            <div class="modal-dialog" style="width: 661px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModal">New Message</h4>
                    </div>
                    <!-- dialog body -->
                    <div class="modal-body">
                        <div class="form-group clearfix"> 
                            <!--<label class="col-sm-1" style="display:block; margin-top: 11px; font-weight: bold;">To</label>-->
                            <div class="col-sm-12">
                                <input type="text" value="${userdetail.userName} ( +${userdetail.countryCode.mobileCode}-${userdetail.mobileNumber} )" readonly class="form-control input-circle" />
                            </div>
                            <br />
                            <br />
                            <br />
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class='box-body pad'>
                                        <textarea id="textmessage" name="textmessage" class="" placeholder="Please write your message here" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                        <span style="color: red; display: block; font-size: 14px;" id="errortext" ></span> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- dialog buttons -->
                    <div class="modal-footer" style="margin-top: 0px;">
                        <span style="color: green; display: block; font-size: 14px; float: left;" id="responseMessage" ></span>
                        <button type="button" class="btn btn-circle btn-danger" data-dismiss="modal">Cancel</button>  
                        <button type="button" class="btn btn-circle btn-success" id="sendtextmessage">Send</button>

                    </div>
                </div>
            </div>
        </div>
        <!----> 
        <!-- Modal -->
<!--        <div class="modal fade " id="otpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm text-center" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Send OTP</h4>
                    </div>
                    <div class="modal-body">
                        <div class="mt-radio-inline">
                            <label class="mt-radio">
                                <input type="radio" name="sendotp" id="" value="email" checked="">
                                Email <span></span> </label>
                            <label class="mt-radio">
                                <input type="radio" name="sendotp" id="" value="mobile" >
                                Mobile <span></span> </label>
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align:center;">
                        <button type="button" class="btn btn-success btn-circle" data-dismiss="modal" data-toggle="modal" data-target="" id="renewLicense"> Send <i class="fa fa-angle-right"></i> </button>
                    </div>
                </div>
            </div>
        </div>-->

        <!-- Modal -->
<!--        <div class="modal fade " id="otpModal_text" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm text-center" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Confirm OTP</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="random"/>
                        <input type="text" class="form-control input-circle" id="otp"/>
                        <span style="color: red; display: block; font-size: 14px;" id="otpError" ></span> </div>
                    <div class="modal-footer" style="text-align:center;">
                        <button type="button" class="btn btn-success btn-circle otp_btn" id="submitotp"> Continue <i class="fa fa-angle-right"></i> </button>
                        <button class="btn btn-default btn-circle otp_btn" type="button" id="resendotp">Resend OTP</button>
                    </div>
                    <span class="otperror" style="color: red; display: block; font-size: 14px;"></span> </div>
            </div>
        </div>-->
        <!-- BEGIN CORE PLUGINS --> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
        <!-- END CORE PLUGINS --> 
        <!-- BEGIN THEME GLOBAL SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
        <!-- END THEME GLOBAL SCRIPTS --> 
        <!-- BEGIN THEME LAYOUT SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/ckeditor/adapters/jquery.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/clipboard/clipboard.min.js"></script> 
        <!-- END THEME LAYOUT SCRIPTS --> 

        <script>
                        $(".form-control").focus(function () {
                            $(this).prev('.input-group-addon').removeClass().addClass('input-group-addon-focus');
                            $(this).next('.input-group-addon').removeClass().addClass('input-group-addon-focus');
                        });

                        $(".form-control").focusout(function () {
                            $(this).prev('.input-group-addon-focus').removeClass().addClass('input-group-addon');
                            $(this).next('.input-group-addon-focus').removeClass().addClass('input-group-addon');
                        });

        </script>
        <script>
            var clipboard = new Clipboard('#copy', {
                text: function () {
                    var key = $("#licensekey").text();
                    if (key === "") {
                        $(".copyresponse").text("Problem In copy");
                    } else {
                        $(".copyresponse").text("License Key Copied");
                        return key;
                    }
                }
            });
            clipboard.on('success', function (e) {
                console.log(e);
            });
            clipboard.on('error', function (e) {
                console.log(e);
            });

            var clipboard = new Clipboard('#copysystem', {
                text: function () {
                    var key = $("#SystemId").text();
                    if (key === "") {
                        $(".copyresponse").text("Problem In copy");
                    } else {
                        $(".copyresponse").text("System Id Copied");
                        return key;
                    }
                }
            });
            clipboard.on('success', function (e) {
                console.log(e);
            });
            clipboard.on('error', function (e) {
                console.log(e);
            });

        </script> 
        <script>
            $(document).ready(function () {
                $('#side-menu').find('li').removeClass('active');
                $(document).find(".user").addClass('active');
            });

            function systemdetails(count, id) {
                console.log(count + " " + id);
                var Uname = "${user.userName}".replace(" ", "");
                $.post("${pageContext.request.contextPath}/" + Uname + "/getsystemdetails", {systeminfoid: id},
                        function (data, status) {
                            console.log(data);
                            if (data.indexOf("success") > -1) {
                                var jsondata = JSON.parse(data.split("#")[1]);
                                $("#macid" + count).text(jsondata.serialNumber);
                                $("#serialnumber" + count).text(jsondata.macId);
                            } else {
                                $("#systemerror").text(data);
                            }
                        });
            }

            function copysystemid(count) {
                var clipboard = new Clipboard('#copy' + count, {
                    text: function () {
                        var key = $("#systemId" + count).text();
                        if (key === "") {
                            $(".copyresponse").text("Problem In copy");
                        } else {
                            $(".copyresponse").text("System Id Copied");
                            return key;
                        }
                    }
                });
                clipboard.on('success', function (e) {
                    console.log(e);
                });
                clipboard.on('error', function (e) {
                    console.log(e);
                });
            }

            $(".collapsed").on("click", function () {
                $(".copyresponse").text("");
            });
        </script> 
        <script>
            $(function () {
                $(".textarea").ckeditor();
            });
            var mailuser;
            $('.sendMail').on("click", function (e) {
                $("#mailResponseMessage").html("");
                $("#subjectError").html("");
                $("#messageError").html("");
                mailuser = $(e.currentTarget).attr("value");
                $('.cke_wysiwyg_frame').contents().find('body').html("");
                $("#subject").val("");
                $("#mailModal").modal("show");
            });

            $("#sendMailToUser").on("click", function () {
                $("#subjectError").html("");
                $("#messageError").html("");
                var subject = $("#subject").val();
                var messageContent = $("#messageContent").val();
                var flag = true;
                var flag1 = true;

                if (subject === "") {
                    $("#subjectError").html("The Subject field is required.");
                    flag1 = false;
                } else if (subject.length <= 3 || subject.length >= 101) {
                    $("#subjectError").html("Subject accepts 4-100 character only");
                    flag1 = false;
                } else {
                    flag1 = true;
                }

                if (messageContent === "") {
                    $("#messageError").html("The Message field is required.");
                    flag = false;
                } else if (messageContent.length === 9) {
                    $("#messageError").html("The Message field is required.");
                } else if ((messageContent.length - 9) <= 3 || (messageContent.length - 9) >= 1001) {
                    $("#subjectError").html("Message accepts 4-1000 character only");
                    flag = false;
                } else {
                    flag = true;
                }

                if (flag && flag1) {
                    var Uname = "${user.userName}".replace(" ", "");
                    $("#loading").css('display', 'block');
                    $.post("${pageContext.request.contextPath}/" + Uname + "/sendmailtouser", {userId: mailuser, subject: subject, message: messageContent},
                            function (data, status) {
                                if (data === "success") {
                                    $("#loading").css("display", "none");
                                    $("#mailResponseMessage").html("Email has been sent successfully.");
                                    setTimeout(function ()
                                    {
                                        $("#mailModal").modal("hide");
                                    }, 1000);
                                } else {
                                    $("#loading").css("display", "none");
                                    $("#mailResponseMessage").html(data);
                                }
                            });
                }
            });

            $("#subject").on("change", function () {
                $("#mailResponseMessage").html("");
                var subject = $("#subject").val();
                if (subject === "") {
                    $("#subjectError").html("The Subject field is required.");
                } else {
                    $("#subjectError").html("");
                }
            });
            $("#messageContent").on("change", function () {
                $("#mailResponseMessage").html("");
                var messageContent = $("#messageContent").val();
                if (messageContent === "") {
                    $("#messageError").html("The Message field is required.");
                } else {
                    $("#messageError").html("");
                }
            });

        </script> 
        <script>
            var messageuser;
            $('.sendMessage').on("click", function (e) {
                $("#textmessage").val("");
                $("#errortext").text("");
                $("#responseMessage").html("");
                messageuser = $(e.currentTarget).attr("value");
                $("#messageModal").modal("show");
            });

            $("#sendtextmessage").on("click", function () {
                var text = $("#textmessage").val();
                var flag = true;
                console.log(text);
                if (text === "") {
                    $("#errortext").text("Please Enter text.");
                    flag = false;
                } else if (text.length > 160) {
                    $("#errortext").text("Limit should be 160, User can not Write After 160 Characters.");
                    flag = false;
                }
                if (flag) {
                    var checkedData = [];
                    checkedData.push(messageuser);
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/" + Uname + "/sendmessagetousers", {userIds: checkedData, text: text},
                            function (data, status) {
                                if (data === "success") {
                                    $("#responseMessage").html("Message has been sent successfully.");
                                    setTimeout(function ()
                                    {
                                        $("#messageModal").modal("hide");
                                    }, 1000);
                                } else {
                                    $("#responseMessage").html(data);
                                }
                            });
                }
            });

            $(".renew").on("click", function () {
                var licenseid = this.id;
                var Uname = "${user.userName}".replace(" ", "");
                window.location.href = "${pageContext.request.contextPath}/"+Uname+"/renewlicense/"+licenseid; 
//                $.post("${pageContext.request.contextPath}/" + Uname + "/getsinglelicense", {licenseId: licenseid},
//                        function (data, status) {
//                            if (data.indexOf("success") > -1) {
//                                var jsondata = JSON.parse(data.split("#")[1]);
//        //                            $("#licenceKey").val(jsondata.licenseId);
//
//                                $("#userName").val(jsondata.userName);
//                                $("#coursename").val(jsondata.courseName);
//                                $("#systemId").val(jsondata.systemId);
//                                $("#issueDate").val(jsondata.expirydate);
//                                $("#licenseId").val(jsondata.licenseId);
//                                $("#useruserid").val(jsondata.userid);
//                                $("#courseid").val(jsondata.courseid);
//                                $("#systemid").val(jsondata.systeminfoid);
//
//                                $("#renew_modal").modal("show");
//                            } else {
//                                $("#renewerror").text(data);
//                            }
//                        });
            });

            $(document).on("change", "#validity", function () {
                var valid = $("#validity").val();
                var startdate = $("#issueDate").val();

                var Uname = "${user.userName}".replace(" ", "");
                $.get("${pageContext.request.contextPath}/" + Uname + "/getdate",
                        {validity: valid, startdate: startdate}, function (data, status) {
                    if (data.indexOf("success") > -1) {
                        $("#expiryDate").val(data.split("#")[1]);
                    } else {
                        window.location.href = "/";
                    }
                });
            });

//            $("#renew_license").on("click", function () {
//                $("#otpModal").modal("show");
//            });
//            $("#renewLicense").on("click", function () {
//                var licenseId = $("#licenseId").val();
//                var useruserId = $("#useruserid").val();
//                var courseId = $("#courseid").val();
//                var validity = $("#validity").val();
//                var startDate = $("#issueDate").val();
//                var endDate = $("#expiryDate").val();
//                var systemId = $("#systemid").val();
//                var radioValue = $("input[name='sendotp']:checked").val();
//                console.log(useruserId);
//                var Uname = "${user.userName}".replace(" ", "");
//                $.post("${pageContext.request.contextPath}/" + Uname + "/renewuserlicense",
//                        {userId: useruserId, courseId: courseId, licenseId: licenseId, validity: validity, startDate: startDate,
//                            endDate: endDate, systemId: systemId, sendOtpOn: radioValue},
//                        function (data, status) {
//                            if (data === "Please login again.") {
//                                window.location.href = "/";
//                            } else if (data.indexOf("success") !== -1) {
//        //                                alert(data.split("#")[1]);
//                                $("#random").val(data.split("#")[1]);
//                                $("#otpModal_text").modal("show");
//        //                                window.location.href = "${pageContext.request.contextPath}/${user.userName}/viewlicense";
//                            } else {
//                                $("#renew_error").text(data);
//                            }
//                        });
//            });

//            $(document).on("click", "#submitotp", function () {
//                var otp = $("#otp").val();
//                var random = $("#random").val();
//                if (otp === "") {
//                    $("#otperror").text("Please enter OTP");
//                } else if (random === "") {
//                    $("#otperror").text("Please Login again");
//                    window.location.href = "/";
//                } else {
//                var Uname = "${user.userName}".replace(" ", "");
//                $.post("${pageContext.request.contextPath}/" + Uname + "/varifyotp", {otp: otp, random: random},
//                            function (data, status) {
//                                if (data === "Please login again.") {
//                                    window.location.href = "/";
//                                } else if (data.indexOf("success") !== -1) {
//                                    $("#licenceKey").val(data.split("#")[1]);
//                                    $("#otpModal").modal("hide");
//                                    $("#renew_license").css("display", "none");
//                                } else {
//                                    $("#otperror").text(data);
//                                }
//                            });
//                }
//            });
//
//            $(document).on("click", "#resendotp", function () {
//                var radioValue = $("input[name='sendotp']:checked").val();
//                var random = $("#random").val();
//                if (random === "") {
//                    $("#otperror").text("Login Again");
//                    window.location.href = "/";
//                } else {
//                    var Uname = "${user.userName}".replace(" ", "");
//                    $.post("${pageContext.request.contextPath}/" + Uname + "/resendotp", {random: random, sendOtpOn: radioValue},
//                            function (data, status) {
//                                if (data === "Please login again.") {
//                                    window.location.href = "/";
//                                } else if (data.indexOf("success") !== -1) {
//                                    $("#random").val(data.split("#")[1]);
//                                } else {
//                                    $("#otperror").text(data);
//                                }
//                            });
//                }
//            });
        </script>

    </body>
</html>
