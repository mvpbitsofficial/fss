/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.controller;

import com.coretechies.filesecuritysystem.algorithm.UserStatus;
import com.coretechies.filesecuritysystem.algorithm.UserType;
import com.coretechies.filesecuritysystem.dao.CourseDao;
import com.coretechies.filesecuritysystem.dao.UserInfoDao;
import com.coretechies.filesecuritysystem.dao.NotificationDao;
import com.coretechies.filesecuritysystem.dao.UserLicenseDao;
import com.coretechies.filesecuritysystem.domain.AdminLicense;
import com.coretechies.filesecuritysystem.domain.Course;
import com.coretechies.filesecuritysystem.domain.Notification;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.domain.UserLicense;
import com.coretechies.filesecuritysystem.service.AdminLicenseService;
import com.coretechies.filesecuritysystem.service.UserLicenseService;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 * @Controller annotation represents that this is a java controller class.
 * The ExportController class implements an application that handles all the requests
 * to download the information of all users, report template, license details, 
 * license expiry alerts, license history, and course details in an excel sheet.
 * 
 * @SessionAttributes annotation represents a user in session.
 * "user" is a object of userInfo class and have a complete information
 * of that user who is present in the session at that time.
 * 
 * @author Jaishree
 * @version 1.0
 * @since 2016-09-02
 */
@Controller
@SessionAttributes({"user"})
public class ExportController {

    /** The @Autowired annotation is auto wire the bean by matching data type.
     *  adminLicenseService, userInfoDao, notificationDao, courseDao and userLicenseService 
     *  are objects to use methods of these classes in our controller. 
     */
    @Autowired
    private UserInfoDao userInfoDao;

    @Autowired
    private CourseDao courseDao;

    @Autowired
    private UserLicenseService userLicenseService;

    @Autowired
    private UserLicenseDao userLicenseDao;
    
    @Autowired
    private AdminLicenseService adminLicenseService;

    @Autowired
    private NotificationDao notificationDao;

    private final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

    private final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    /**
     * This method generate an excel sheet of list of users between a selected date, and write it on response.
     * If Admin is in session then list contains the information of all clients created between the selected period of date.
     * And If any client is in session then the list contains the information of all users created by his own.
     *
     * @param response
     * @param modelMap
     * @param userName
     * @throws java.io.IOException
     */
    @GetMapping(value = "/{userName}/exportuser")
    public void exportUser(@PathVariable String userName, HttpServletResponse response, ModelMap modelMap) throws IOException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                try { HSSFWorkbook workbook = new HSSFWorkbook();
                    List<UserInfo> userInfos = userInfoDao.getUserInfoByCreatedBy(userInfo.getUserInfoId());
                    
                    HSSFSheet sheet = workbook.createSheet("FirstSheet");
                    HSSFRow rowhead = sheet.createRow((short) 0);
                    rowhead.createCell(0).setCellValue("Sr. No.");
                    rowhead.createCell(1).setCellValue("User Id");
                    rowhead.createCell(2).setCellValue("User Name");
                    rowhead.createCell(3).setCellValue("Email Id");
                    rowhead.createCell(4).setCellValue("Mobile");
                    rowhead.createCell(5).setCellValue("Created On");

                    int i = 0;
                    for (UserInfo user : userInfos) {
                        HSSFRow row = sheet.createRow((short) ++i);
                        row.createCell(0).setCellValue(i);
                        Map<Integer, Integer> map = userInfoDao.showUserIdThroughCount();
                        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                            if(entry.getKey() == user.getUserInfoId()){
                                row.createCell(1).setCellValue("AD"+(entry.getValue()+1000));
                            } 
                        }   
                        row.createCell(2).setCellValue(user.getUserName());
                        row.createCell(3).setCellValue(user.getEmailId());
                        row.createCell(4).setCellValue("+"+user.getCountryCode().getMobileCode()+"-"+user.getMobileNumber());
                        row.createCell(5).setCellValue(dateTimeFormat.format(user.getCreateOn()));
                    }

                    String format1 = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss").format(new Date());
                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-Disposition", "attachment; filename=users_" + format1 + ".xls");

                    workbook.write(response.getOutputStream());

                } catch (Exception ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                    response.getOutputStream().write("Invalid date format : use this format dd-MM-yyyy".getBytes());
                }
            } else if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                try { HSSFWorkbook workbook = new HSSFWorkbook();
                    List<UserInfo> userInfos = userInfoDao.getUserInfoByUserType(2);

                    HSSFSheet sheet = workbook.createSheet("FirstSheet");
                    HSSFRow rowhead = sheet.createRow((short) 0);
                    rowhead.createCell(0).setCellValue("Sr. No.");
                    rowhead.createCell(1).setCellValue("User Id");
                    rowhead.createCell(2).setCellValue("User Name");
                    rowhead.createCell(3).setCellValue("Organization");
                    rowhead.createCell(4).setCellValue("Email Id");
                    rowhead.createCell(5).setCellValue("Mobile");
                    rowhead.createCell(6).setCellValue("Status");
                    rowhead.createCell(7).setCellValue("Created On");

                    int i = 0;
                    for (UserInfo user : userInfos) {
                        HSSFRow row = sheet.createRow((short) ++i);
                        row.createCell(0).setCellValue(i);
                        Map<Integer, Integer> map = userInfoDao.showClientIdThroughCount();
                        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                            if(entry.getKey() == user.getUserInfoId()){
                                row.createCell(1).setCellValue("SA"+(1000+entry.getValue()));
                            } 
                        }
                        row.createCell(2).setCellValue(user.getUserName());
                        row.createCell(3).setCellValue(user.getOrganization());
                        row.createCell(4).setCellValue(user.getEmailId());
                        row.createCell(5).setCellValue("+"+user.getCountryCode().getMobileCode()+"-"+user.getMobileNumber());
                        row.createCell(6).setCellValue(UserStatus.parse(user.getUserStatus()).toString());
                        row.createCell(7).setCellValue(dateTimeFormat.format(user.getCreateOn()));
                    }

                    String format1 = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss").format(new Date());
                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-Disposition", "attachment; filename=users_" + format1 + ".xls");

                    workbook.write(response.getOutputStream());

                } catch (Exception ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                    response.getOutputStream().write("Invalid date format : use this format dd-MM-yyyy".getBytes());
                }
            } else {
                response.getOutputStream().write("Unauthorized access, Please login again".getBytes());
            }
        } else {
            response.getOutputStream().write("Unauthorized access, Please login again".getBytes());
        }
    }

    /**
     * 
     * @param userName
     * @param userId
     * @param response
     * @param modelMap
     * @throws IOException
     */
    @GetMapping(value = "/{userName}/exportoneuser")
    public void exportOneUser(@PathVariable String userName, @RequestParam Integer userId, HttpServletResponse response, ModelMap modelMap) throws IOException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                try { HSSFWorkbook workbook = new HSSFWorkbook();
                    UserInfo userInfo1 = userInfoDao.getUserInfo(userId);
                    
                    HSSFSheet sheet = workbook.createSheet("FirstSheet");
                    HSSFRow rowhead = sheet.createRow((short) 0);
                    rowhead.createCell(0).setCellValue("Sr. No.");
                    rowhead.createCell(1).setCellValue("User Id");
                    rowhead.createCell(2).setCellValue("User Name");
                    rowhead.createCell(3).setCellValue("Email Id");
                    rowhead.createCell(4).setCellValue("Mobile");
                    rowhead.createCell(5).setCellValue("Created On");

                    HSSFRow row = sheet.createRow((short) 1);
                    row.createCell(0).setCellValue(1);
                    Map<Integer, Integer> map = userInfoDao.showUserIdThroughCount();
                        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                            if(entry.getKey() == userInfo1.getUserInfoId()){
                                row.createCell(1).setCellValue("AD"+(entry.getValue()+1000));
                            } 
                        } 
                    row.createCell(2).setCellValue(userInfo1.getUserName());
                    row.createCell(3).setCellValue(userInfo1.getEmailId());
                    row.createCell(4).setCellValue("+"+userInfo1.getCountryCode().getMobileCode()+"-"+userInfo1.getMobileNumber());
                    row.createCell(5).setCellValue(dateTimeFormat.format(userInfo1.getCreateOn()));
                    
                    String format1 = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss").format(new Date());
                    System.out.println("format========"+format1);
                    String uName = userInfo1.getUserName().replace(" ", "").substring(0, 7);
                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-Disposition", "attachment; filename="+uName+"_" + format1 + ".xls");

                    workbook.write(response.getOutputStream());

                } catch (Exception ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                    response.getOutputStream().write("Invalid date format : use this format dd-MM-yyyy".getBytes());
                }
            } else if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                try { HSSFWorkbook workbook = new HSSFWorkbook();
                    UserInfo userInfo1 = userInfoDao.getUserInfo(userId);

                    HSSFSheet sheet = workbook.createSheet("FirstSheet");
                    HSSFRow rowhead = sheet.createRow((short) 0);
                    rowhead.createCell(0).setCellValue("Sr. No.");
                    rowhead.createCell(1).setCellValue("User Id");
                    rowhead.createCell(2).setCellValue("User Name");
                    rowhead.createCell(3).setCellValue("Organization");
                    rowhead.createCell(4).setCellValue("Email Id");
                    rowhead.createCell(5).setCellValue("Mobile");
                    rowhead.createCell(6).setCellValue("Status");
                    rowhead.createCell(7).setCellValue("Created On");
                    
                    HSSFRow row = sheet.createRow((short) 1);
                    row.createCell(0).setCellValue(1);
                    Map<Integer, Integer> map = userInfoDao.showClientIdThroughCount();
                        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                            if(entry.getKey() == userInfo1.getUserInfoId()){
                                row.createCell(1).setCellValue("SA"+(1000+entry.getValue()));
                            } 
                        }
                    row.createCell(2).setCellValue(userInfo1.getUserName());
                    row.createCell(3).setCellValue(userInfo1.getOrganization());
                    row.createCell(4).setCellValue(userInfo1.getEmailId());
                    row.createCell(5).setCellValue("+"+userInfo1.getCountryCode().getMobileCode()+"-"+userInfo1.getMobileNumber());
                    row.createCell(6).setCellValue(UserStatus.parse(userInfo1.getUserStatus()).toString());
                    row.createCell(7).setCellValue(dateTimeFormat.format(userInfo1.getCreateOn()));

                    String format1 = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss").format(new Date());
                    System.out.println("format========"+format1);
                    String uName = userInfo1.getUserName().replace(" ", "").substring(0, 7);
                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-Disposition", "attachment; filename="+uName+"_" + format1 +".xls");

                    workbook.write(response.getOutputStream());

                } catch (Exception ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                    response.getOutputStream().write("Invalid date format : use this format dd-MM-yyyy".getBytes());
                }
            } else {
                response.getOutputStream().write("Unauthorized access, Please login again".getBytes());
            }
        } else {
            response.getOutputStream().write("Unauthorized access, Please login again".getBytes());
        }
    }
    /**
     * This method 
     * 
     * @param request
     * @param response
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     */
    public void generateReportTemplate(HttpServletRequest request, HttpServletResponse response) throws FileNotFoundException, IOException {
       
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("FirstSheet");

            HSSFRow rowhead = sheet.createRow((short) 0);
            rowhead.createCell(0).setCellValue("No.");
            rowhead.createCell(1).setCellValue("Name");
            rowhead.createCell(2).setCellValue("Address");
            rowhead.createCell(3).setCellValue("Email");

            HSSFRow row = sheet.createRow((short) 1);
            row.createCell(0).setCellValue("1");
            row.createCell(1).setCellValue("Sankumarsingh");
            row.createCell(2).setCellValue("India");
            row.createCell(3).setCellValue("sankumarsingh@gmail.com");

            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=filename" + new Date().getTime() + ".xls");

            workbook.write(response.getOutputStream());
      
    }

    /**
     * This method will generate an excel sheet containing the information of licenses
     * between a selected period of date and write it on response.
     * If the admin is in session then the list contains
     * details of all the license generated by the admin for any client between the specified period.
     * And if client is in session then list contains details of licenses
     * generated by that particular client for any user between the specified period.
     * 
     * @param startDate
     * @param endDate
     * @param userName
     * @param response
     * @param modelMap
     * @throws java.io.IOException
     */
    @GetMapping(value = "/{userName}/exportLicense")
    public void exportLicense(@RequestParam String startDate, @RequestParam String endDate, @PathVariable String userName, HttpServletResponse response, ModelMap modelMap) throws IOException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                System.out.println("startdate======"+startDate);
                System.out.println("enddate====="+endDate);
                try { HSSFWorkbook workbook = new HSSFWorkbook();
                    List<UserLicense> userLicenses = userLicenseService.getUserLicenseListByCreateOn(userInfo.getUserInfoId(), format.parse(startDate), format.parse(endDate));
                    System.out.println(userLicenses);
                    HSSFSheet sheet = workbook.createSheet("FirstSheet");

                    HSSFRow rowhead = sheet.createRow((short) 0);
                    rowhead.createCell(0).setCellValue("Sr. No.");
                    rowhead.createCell(1).setCellValue("User Name");
                    rowhead.createCell(2).setCellValue("Course Id");
                    rowhead.createCell(3).setCellValue("Course Name");
                    rowhead.createCell(4).setCellValue("License Key");
                    rowhead.createCell(5).setCellValue("Issue Date");
                    rowhead.createCell(6).setCellValue("Expiry Date");
                    rowhead.createCell(7).setCellValue("Status");

                    int i = 0;
                    for (UserLicense userLicense : userLicenses) {
                        if(userLicense.getExpiryDate() != null){
                            HSSFRow row = sheet.createRow((short) ++i);
                            row.createCell(0).setCellValue(i);
                            row.createCell(1).setCellValue(userLicense.getUserInfo().getUserName());
                            row.createCell(2).setCellValue(userLicense.getCourse().getCourseId());
                            row.createCell(3).setCellValue(userLicense.getCourse().getCourseName());
                            row.createCell(4).setCellValue(userLicense.getLicense());
                            row.createCell(5).setCellValue(format.format(userLicense.getIssueDate()));
                            row.createCell(6).setCellValue(format.format(userLicense.getExpiryDate()));
                            String today = format.format(new Date());
                            String issue = format.format(userLicense.getIssueDate());
                            String expiry = format.format(userLicense.getExpiryDate());
                            if (format.parse(today).compareTo(format.parse(issue)) < 0) {
                                row.createCell(7).setCellValue("Not started.");
                            } else if (format.parse(today).compareTo(format.parse(expiry)) > 0) { 
                                row.createCell(7).setCellValue("Expired.");
                            } else {
                                row.createCell(7).setCellValue("Active.");
                            }
                        }
                    }
                    String format1 = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss").format(new Date());
                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-Disposition", "attachment; filename=licenses_" + format1 + ".xls");

                    workbook.write(response.getOutputStream());

                } catch (ParseException ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                    response.getOutputStream().write("Invalid date format : use this format dd-MM-yyyy".getBytes());
                }
            } else if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {

                try { HSSFWorkbook workbook = new HSSFWorkbook();
                    List<AdminLicense> adminLicenses = adminLicenseService.getAdminLicenseListByCreateOn(format.parse(startDate), format.parse(endDate));
                    HSSFSheet sheet = workbook.createSheet("FirstSheet");

                    HSSFRow rowhead = sheet.createRow((short) 0);
                    rowhead.createCell(0).setCellValue("Sr. No.");
                    rowhead.createCell(1).setCellValue("User Name");
                    rowhead.createCell(2).setCellValue("Desktop License Key");
                    rowhead.createCell(3).setCellValue("Total License Limit");
                    rowhead.createCell(4).setCellValue("Web License Limit");
                    rowhead.createCell(5).setCellValue("Desktop License Limit");
                    rowhead.createCell(6).setCellValue("Issue Date");
                    rowhead.createCell(7).setCellValue("Expiry Date");
                    rowhead.createCell(8).setCellValue("Status");

                    int i = 0;
                    for (AdminLicense adminLicense : adminLicenses) {
                        if(adminLicense.getExpiryDate() != null){
                            String weblimit = "";
                            String desktoplimit = "";
                            if(adminLicense.getWeblicenseLimit() == -1){
                                weblimit = "Unlimited";
                            } else{
                                weblimit = adminLicense.getWeblicenseLimit().toString();
                            }
                            if(adminLicense.getDesktopLicenseLimit() == -1){
                                desktoplimit = "Unlimited";
                            } else{
                                desktoplimit = adminLicense.getDesktopLicenseLimit().toString();
                            }
                            HSSFRow row = sheet.createRow((short) ++i);
                            row.createCell(0).setCellValue(i);
                            row.createCell(1).setCellValue(adminLicense.getUserInfo().getUserName());
                            row.createCell(2).setCellValue(adminLicense.getDesktopLicense());
                            if(adminLicense.getWeblicenseLimit() == -1 && adminLicense.getDesktopLicenseLimit() == -1){
                                row.createCell(3).setCellValue("Unlimited");
                            }else if(adminLicense.getWeblicenseLimit() == -1 || adminLicense.getDesktopLicenseLimit() == -1){
                                row.createCell(3).setCellValue(weblimit +"/"+desktoplimit);
                            }else{
                               row.createCell(3).setCellValue((adminLicense.getWeblicenseLimit() + adminLicense.getDesktopLicenseLimit())); 
                            }
                            row.createCell(4).setCellValue(weblimit);
                            row.createCell(5).setCellValue(desktoplimit);
                            row.createCell(6).setCellValue(format.format(adminLicense.getIssueDate()));
                            row.createCell(7).setCellValue(format.format(adminLicense.getExpiryDate()));
                            if (new Date().compareTo(adminLicense.getIssueDate()) < 0) {
                                row.createCell(8).setCellValue("Not started.");
                            } else if (new Date().compareTo(adminLicense.getExpiryDate()) > 0) {
                                row.createCell(8).setCellValue("Expired.");
                            } else {
                                row.createCell(8).setCellValue("Active.");
                            }
                        }
                    }

                    String format1 = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss").format(new Date());
                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-Disposition", "attachment; filename=licenses_" + format1 + ".xls");

                    workbook.write(response.getOutputStream());

                } catch (ParseException ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                    response.getOutputStream().write("Invalid date format : use this format dd-MM-yyyy".getBytes());
                }
            } else {
                response.getOutputStream().write("Unauthorized access, Please login again".getBytes());
            }
        } else {
            response.getOutputStream().write("Unauthorized access, Please login again".getBytes());
        }
    }

    /**
     * 
     * @param licenseId
     * @param userName
     * @param response
     * @param modelMap
     * @throws IOException
     */
    @GetMapping(value = "/{userName}/exportsingleLicense")
    public void exportSingleLicense(@RequestParam Integer licenseId, @PathVariable String userName, HttpServletResponse response, ModelMap modelMap) throws IOException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                try { HSSFWorkbook workbook = new HSSFWorkbook();
                    UserLicense userLicense = userLicenseDao.getUserLicense(licenseId);
                    HSSFSheet sheet = workbook.createSheet("FirstSheet");

                    HSSFRow rowhead = sheet.createRow((short) 0);
                    rowhead.createCell(0).setCellValue("Sr. No.");
                    rowhead.createCell(1).setCellValue("User Name");
                    rowhead.createCell(2).setCellValue("Course Id");
                    rowhead.createCell(3).setCellValue("Course Name");
                    rowhead.createCell(4).setCellValue("License Key");
                    rowhead.createCell(5).setCellValue("Issue Date");
                    rowhead.createCell(6).setCellValue("Expiry Date");
                    rowhead.createCell(7).setCellValue("Status");

                    HSSFRow row = sheet.createRow((short) 1);
                    row.createCell(0).setCellValue(1);
                    row.createCell(1).setCellValue(userLicense.getUserInfo().getUserName());
                    row.createCell(2).setCellValue(userLicense.getCourse().getCourseId());
                    row.createCell(3).setCellValue(userLicense.getCourse().getCourseName());
                    row.createCell(4).setCellValue(userLicense.getLicense());
                    row.createCell(5).setCellValue(format.format(userLicense.getIssueDate()));
                    row.createCell(6).setCellValue(format.format(userLicense.getExpiryDate()));
                    if(userLicense.getExpiryDate()!=null){
                        if (new Date().compareTo(userLicense.getIssueDate()) < 0) {
                            row.createCell(7).setCellValue("Not started.");
                        } else if (new Date().compareTo(userLicense.getExpiryDate()) > 0) {
                            row.createCell(7).setCellValue("Expired.");
                        } else {
                            row.createCell(7).setCellValue("Active.");
                        }
                    }else{
                        row.createCell(7).setCellValue(" ");
                    }
                    String format1 = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss").format(new Date());
                    response.setContentType("application/vnd.ms-excel");
                    String uName = userLicense.getUserInfo().getUserName().replace(" ", "").substring(0, 7);
                    response.setHeader("Content-Disposition", "attachment; filename=license_"+uName+"_" + format1 + ".xls");

                    workbook.write(response.getOutputStream());

                } catch (Exception ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
//                    response.getOutputStream().write("Invalid date format : use this format dd-MM-yyyy".getBytes());
                }
            } else if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {

                try { HSSFWorkbook workbook = new HSSFWorkbook();
                    AdminLicense adminLicense = adminLicenseService.getAdminLicenseById(licenseId);
                    HSSFSheet sheet = workbook.createSheet("FirstSheet");

                    HSSFRow rowhead = sheet.createRow((short) 0);
                    rowhead.createCell(0).setCellValue("Sr. No.");
                    rowhead.createCell(1).setCellValue("User Name");
                    rowhead.createCell(2).setCellValue("Desktop License Key");
                    rowhead.createCell(3).setCellValue("Total License Limit");
                    rowhead.createCell(4).setCellValue("Web License Limit");
                    rowhead.createCell(5).setCellValue("Desktop License Limit");
                    rowhead.createCell(6).setCellValue("Issue Date");
                    rowhead.createCell(7).setCellValue("Expiry Date");
                    rowhead.createCell(8).setCellValue("Status");

                    String weblimit = "";
                    String desktoplimit = "";
                    if(adminLicense.getWeblicenseLimit() == -1){
                        weblimit = "Unlimited";
                    } else{
                        weblimit = adminLicense.getWeblicenseLimit().toString();
                    }
                    if(adminLicense.getDesktopLicenseLimit() == -1){
                        desktoplimit = "Unlimited";
                    } else{
                        desktoplimit = adminLicense.getDesktopLicenseLimit().toString();
                    }
                    HSSFRow row = sheet.createRow((short) 1);
                    row.createCell(0).setCellValue(1);
                    row.createCell(1).setCellValue(adminLicense.getUserInfo().getUserName());
                    row.createCell(2).setCellValue(adminLicense.getDesktopLicense());
                    if(adminLicense.getWeblicenseLimit() == -1 && adminLicense.getDesktopLicenseLimit() == -1){
                        row.createCell(3).setCellValue("Unlimited");
                    }else if(adminLicense.getWeblicenseLimit() == -1 || adminLicense.getDesktopLicenseLimit() == -1){
                        row.createCell(3).setCellValue(weblimit +"/"+desktoplimit);
                    }else{
                        row.createCell(3).setCellValue((adminLicense.getWeblicenseLimit() + adminLicense.getDesktopLicenseLimit())); 
                    }
                    row.createCell(4).setCellValue(weblimit);
                    row.createCell(5).setCellValue(desktoplimit);
                    row.createCell(6).setCellValue(format.format(adminLicense.getIssueDate()));
                    row.createCell(7).setCellValue(format.format(adminLicense.getExpiryDate()));
                    if(adminLicense.getExpiryDate() != null){
                        if (new Date().compareTo(adminLicense.getIssueDate()) < 0) {
                            row.createCell(8).setCellValue("Not started.");
                        } else if (new Date().compareTo(adminLicense.getExpiryDate()) > 0) {
                            row.createCell(8).setCellValue("Expired.");
                        } else {
                            row.createCell(8).setCellValue("Active.");
                        }
                    }else{
                        row.createCell(8).setCellValue(" ");
                    }
                    String format1 = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss").format(new Date());
                    response.setContentType("application/vnd.ms-excel");
                    String uName = adminLicense.getUserInfo().getUserName().replace(" ", "").substring(0, 7);
                    response.setHeader("Content-Disposition", "attachment; filename=license_" +uName+"_" + format1 +  ".xls");

                    workbook.write(response.getOutputStream());

                } catch (Exception ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
//                    response.getOutputStream().write("Invalid date format : use this format dd-MM-yyyy".getBytes());
                }
            } else {
                response.getOutputStream().write("Unauthorized access, Please login again".getBytes());
            }
        } else {
            response.getOutputStream().write("Unauthorized access, Please login again".getBytes());
        }
    }

    
    /**
     * This method will generate an excel sheet containing the information of all users whose license expiry date is lie between the specified period.
     * If the admin is in session then the list contains
     * details of all users whose license expiry date is lie between the specified period and the license generated by the admin for any client.
     * And if client is in session then list contains details who have the expiry date between the specified period and
     * the license should generated by that particular client.
     * 
     * @param startDate
     * @param endDate
     * @param userName
     * @param response
     * @param modelMap
     * @throws java.io.IOException
     */
    @GetMapping(value = "/{userName}/exportLicenseExpireAlert")
    public void exportLicenseExpireAlert(@RequestParam String startDate, @RequestParam String endDate, @PathVariable String userName, HttpServletResponse response, ModelMap modelMap) throws IOException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                try { HSSFWorkbook workbook = new HSSFWorkbook();
                    List<UserLicense> userLicenses = userLicenseService.getUserLicenseListByExpiryDate(userInfo.getUserInfoId(), format.parse(startDate), format.parse(endDate));
                    HSSFSheet sheet = workbook.createSheet("FirstSheet");

                    HSSFRow rowhead = sheet.createRow((short) 0);
                    rowhead.createCell(0).setCellValue("Sr. No.");
                    rowhead.createCell(1).setCellValue("User Name");
                    rowhead.createCell(2).setCellValue("License");
                    rowhead.createCell(3).setCellValue("Course Id");
                    rowhead.createCell(4).setCellValue("Issue Date");
                    rowhead.createCell(5).setCellValue("Expiry Date");
                    rowhead.createCell(6).setCellValue("Status");

                    int i = 0;
                    for (UserLicense userLicense : userLicenses) {
                        HSSFRow row = sheet.createRow((short) ++i);
                        row.createCell(0).setCellValue(i);
                        row.createCell(1).setCellValue(userLicense.getUserInfo().getUserName());
                        row.createCell(2).setCellValue(userLicense.getLicense());
                        row.createCell(3).setCellValue(userLicense.getCourse().getCourseId());
                        row.createCell(4).setCellValue(format.format(userLicense.getIssueDate()));
                        row.createCell(5).setCellValue(format.format(userLicense.getExpiryDate()));
                        if (new Date().compareTo(userLicense.getIssueDate()) < 0) {
                            row.createCell(6).setCellValue("Not started.");
                        } else if (new Date().compareTo(userLicense.getExpiryDate()) > 0) {
                            row.createCell(6).setCellValue("Expired.");
                        } else {
                            row.createCell(6).setCellValue("Active.");
                        }
                    }

                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-Disposition", "attachment; filename=license_expiry_alert_list_" + new Date().getTime() + ".xls");

                    workbook.write(response.getOutputStream());

                } catch (ParseException ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                    response.getOutputStream().write("Invalid date format : use this format dd-MM-yyyy".getBytes());
                }
            } else if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                try { HSSFWorkbook workbook = new HSSFWorkbook();
                    List<AdminLicense> adminLicenses = adminLicenseService.getAdminLicenseListByExpiryDate(format.parse(startDate), format.parse(endDate));
                    HSSFSheet sheet = workbook.createSheet("FirstSheet");

                    HSSFRow rowhead = sheet.createRow((short) 0);
                    rowhead.createCell(0).setCellValue("Sr. No.");
                    rowhead.createCell(1).setCellValue("User Name");
                    rowhead.createCell(2).setCellValue("License");
                    rowhead.createCell(3).setCellValue("License Limit");
                    rowhead.createCell(4).setCellValue("Issue Date");
                    rowhead.createCell(5).setCellValue("Expiry Date");
                    rowhead.createCell(6).setCellValue("Status");

                    int i = 0;
                    for (AdminLicense adminLicense : adminLicenses) {
                        HSSFRow row = sheet.createRow((short) ++i);
                        row.createCell(0).setCellValue(i);
                        row.createCell(1).setCellValue(adminLicense.getUserInfo().getUserName());
                        row.createCell(2).setCellValue(adminLicense.getLicense());
                        row.createCell(3).setCellValue(adminLicense.getWeblicenseLimit());
                        row.createCell(4).setCellValue(format.format(adminLicense.getIssueDate()));
                        row.createCell(5).setCellValue(format.format(adminLicense.getExpiryDate()));
                        if (new Date().compareTo(adminLicense.getIssueDate()) < 0) {
                            row.createCell(6).setCellValue("Not started.");
                        } else if (new Date().compareTo(adminLicense.getExpiryDate()) > 0) {
                            row.createCell(6).setCellValue("Expired.");
                        } else {
                            row.createCell(6).setCellValue("Active.");
                        }
                    }

                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-Disposition", "attachment; filename=license_expiry_alert_list_" + new Date().getTime() + ".xls");

                    workbook.write(response.getOutputStream());

                } catch (ParseException ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                    response.getOutputStream().write("Invalid date format : use this format dd-MM-yyyy".getBytes());
                }
            } else {
                response.getOutputStream().write("Unauthorized access, Please login again".getBytes());
            }
        } else {
            response.getOutputStream().write("Unauthorized access, Please login again".getBytes());
        }
    }

    /**
     * This method will generate an excel sheet containing the information
     * of licenses notifications created by that particular user(admin/client) between an specified period and write it on response.
     * 
     * @param userName
     * @param response
     * @param modelMap
     * @throws java.io.IOException
     */
    @GetMapping(value = "/{userName}/exportLicenseHistory")
    public void exportLicenseHistory(@PathVariable String userName, HttpServletResponse response, ModelMap modelMap) throws IOException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {

                try { HSSFWorkbook workbook = new HSSFWorkbook();
                    List<Notification> notifications = notificationDao.getNotificationForClient(userInfo.getUserInfoId());
                    System.out.println("size==="+notifications.size());
                    HSSFSheet sheet = workbook.createSheet("FirstSheet");
                    HSSFRow rowhead = sheet.createRow((short) 0);
                    rowhead.createCell(0).setCellValue("Sr. No.");
                    rowhead.createCell(1).setCellValue("Message");
                    rowhead.createCell(2).setCellValue("Date And Time");

                    int i = 0;
                    for (Notification notification : notifications) {
                        HSSFRow row = sheet.createRow((short) ++i);
                        row.createCell(0).setCellValue(i);
                        row.createCell(1).setCellValue(notification.getNotificationMessage());
                        row.createCell(2).setCellValue(dateTimeFormat.format(notification.getCreateOn()));
                    }
                    String format1 = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss").format(new Date());
                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-Disposition", "attachment; filename=Notifications_"+ format1 + ".xls");

                    workbook.write(response.getOutputStream());

                } catch (Exception ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                    response.getOutputStream().write("Invalid date format : use this format dd-MM-yyyy".getBytes());
                }
            } else if(UserType.Admin.getUserTypeValue().equals(userInfo.getUserInfoId())){
                try { HSSFWorkbook workbook = new HSSFWorkbook();
                    List<Notification> notifications = notificationDao.getNotificationForAdmin();

                    HSSFSheet sheet = workbook.createSheet("FirstSheet");
                    HSSFRow rowhead = sheet.createRow((short) 0);
                    rowhead.createCell(0).setCellValue("Sr. No.");
                    rowhead.createCell(1).setCellValue("Message");
                    rowhead.createCell(2).setCellValue("Date And Time");

                    int i = 0;
                    for (Notification notification : notifications) {
                        HSSFRow row = sheet.createRow((short) ++i);
                        row.createCell(0).setCellValue(i);
                        row.createCell(1).setCellValue(notification.getNotificationMessage());
                        row.createCell(2).setCellValue(dateTimeFormat.format(notification.getCreateOn()));
                    }
                    String format1 = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss").format(new Date());
                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-Disposition", "attachment; filename=Notifications_"+ format1 + ".xls");

                    workbook.write(response.getOutputStream());

                } catch (Exception ex) {
                    Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
                    response.getOutputStream().write("Invalid date format : use this format dd-MM-yyyy".getBytes());
                }
            }else {
                response.getOutputStream().write("Unauthorized access, Please login again".getBytes());
            }
        } else {
            response.getOutputStream().write("Unauthorized access, Please login again".getBytes());
        }
    }

    /**
     * This method will generate an excel sheet containing the information
     * of all courses created by that particular user who is present in the session and write it on response.
     * 
     * @param userName
     * @param response
     * @param modelMap
     * @throws java.io.IOException
     */
    @GetMapping(value = "/{userName}/exportcourse")
    public void exportCourse(@PathVariable String userName, HttpServletResponse response, ModelMap modelMap) throws IOException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
      
                    HSSFWorkbook workbook = new HSSFWorkbook();
                    List<Course> courses = courseDao.getCourseListwithCompleteDetail(userInfo.getUserInfoId());

                    HSSFSheet sheet = workbook.createSheet("FirstSheet");
                    HSSFRow rowhead = sheet.createRow((short) 0);
                    rowhead.createCell(0).setCellValue("Sr. No.");
                    rowhead.createCell(1).setCellValue("Course Id");
                    rowhead.createCell(2).setCellValue("Course Name");
                    rowhead.createCell(3).setCellValue("User Name");
                    rowhead.createCell(4).setCellValue("System Id");

                    int i = 0;
                    int a = 0;
                    for (Course course : courses) {
                        HSSFRow row = sheet.createRow((short) ++i);
                        a++;
                        row.createCell(0).setCellValue(a);
                        row.createCell(1).setCellValue(course.getCourseId());
                        row.createCell(2).setCellValue(course.getCourseName());
                        for (UserLicense userLicense : course.getUserLicenses()) {
                            row.createCell(3).setCellValue(userLicense.getUserInfo().getUserName());
                            row.createCell(4).setCellValue(userLicense.getSystemInfo().getSystemId());
                            row = sheet.createRow((short) ++i);
                        }
                    }

                    String format1 = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss").format(new Date());
                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-Disposition", "attachment; filename=course_list_" + format1 + ".xls");

                    workbook.write(response.getOutputStream());

              
            } else if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                response.getOutputStream().write("Unauthorized access".getBytes());
            } else {
                response.getOutputStream().write("Unauthorized access, Please login again".getBytes());
            }
        } else {
            response.getOutputStream().write("Unauthorized access, Please login again".getBytes());
        }
    }
}
