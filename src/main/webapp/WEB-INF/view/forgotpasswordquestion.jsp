<%-- 
    Document   : changepassword
    Created on : 9 Aug, 2016, 2:24:32 PM
    Author     : CoreTechies M8
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>FSS | Profile</title>
        <script>var myContextPath = "${pageContext.request.contextPath}";</script>
        <script src="${pageContext.request.contextPath}/js/jquery-1.10.2.min.js"></script>
    </head>
    <body>
        <h1>Hello World!</h1>

        <form action="#">
            <div>
                <select id="question1">
                    <c:forEach items="${questions}" var="question">
                        <option value="${question.securityQuestionId}">${question.question}</option>
                    </c:forEach>
                </select>
                <br/>
                <label style="color: red;" id="errorMessageQuestion1"></label>
            </div>
            <div>
                <input type="text" id="answer1" placeholder="Answer"/>
                <br/>
                <label style="color: red;" id="errorMessageAnswer1"></label>
            </div>
            <div>
                <select id="question2">
                    <c:forEach items="${questions}" var="question">
                        <option value="${question.securityQuestionId}">${question.question}</option>
                    </c:forEach>
                </select>
                <br/>
                <label style="color: red;" id="errorMessageQuestion2"></label>
            </div>
            <div>
                <input type="text" id="answer2" placeholder="Answer"/>
                <br/>
                <label style="color: red;" id="errorMessageAnswer2"></label>
            </div>
            <div>
                <button type="button" id="validateSecurityQuestion">Submit</button>
                <br/>
                <label style="color: red;" id="errorMessageValidate"></label>
            </div>
        </form>
        <script>
            $(document).on("click", "#validateSecurityQuestion", function () {
                var question1 = $("#question1").select().val();
                var answer1 = $("#answer1").val().trim();
                var question2 = $("#question2").select().val();
                var answer2 = $("#answer2").val().trim();
                var flag = true;
                $("#errorMessageQuestion1").text("");
                $("#errorMessageAnswer1").text("");
                $("#errorMessageQuestion2").text("");
                $("#errorMessageAnswer2").text("");
                $("#errorMessageValidate").text("");
                if (question1 === null || question1 === "") {
                    $("#errorMessageQuestion1").text("Please select first question.");
                    flag = false;
                }
                if (answer1 === null || answer1 === "") {
                    $("#errorMessageAnswer1").text("Please enter answer for first question.");
                    flag = false;
                }
                if (question2 === null || question2 === "") {
                    $("#errorMessageQuestion2").text("Please select second question.");
                    flag = false;
                }
                if (answer2 === null || answer2 === "") {
                    $("#errorMessageAnswer2").text("Please enter answer for second question.");
                    flag = false;
                }
                if (question1 === question2 && flag) {
                    $("#errorMessageValidate").text("Both Questions are same please select different question.");
                    flag = false;
                }
                if (flag) {
                    $.post("${pageContext.request.contextPath}/forgotpasswordquestion", {question1: question1, answer1: answer1, question2: question2, answer2: answer2},
                            function (data, status) {
                                if (data === "success") {
                                    window.location.href = "${pageContext.request.contextPath}/forgotpassword";
                                } else if (data === "Please login again.") {
                                    window.location.href = "${pageContext.request.contextPath}/";
                                } else {
                                    $("#errorMessageValidate").text(data);
                                }
                            }
                    );
                }
            });

            $(document).on("keyup", "#question", function () {
                var question = $("#question").select().val();
                if (question === "") {
                    $("#errorMessageQuestion").text("Please Select a Question.");
                } else {
                    $("#errorMessageQuestion").text("");
                }
            });

            $(document).on("keyup", "#answer", function () {
                var question = $("#answer").val();
                if (question === "") {
                    $("#errorMessageAnswer").text("Please Enter Answer.");
                } else {
                    $("#errorMessageAnswer").text("");
                }
            });
        </script>
    </body>
</html>
