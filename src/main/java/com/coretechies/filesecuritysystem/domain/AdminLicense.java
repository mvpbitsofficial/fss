/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author CoreTechies M8
 */
@Entity
@Table(name = "adminlicense")
@Access(AccessType.FIELD)
public class AdminLicense implements Serializable {

    @Id
    @GeneratedValue(generator = "adminlicense")
    @GenericGenerator(name = "adminlicense", strategy = "increment")
    private Integer adminLicenseId;

    @Column
    private Integer userId;

    @Column
    private Integer validity;

    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date issueDate;

    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date expiryDate;

    @Column
    private String systemInfoId;

    @Column
    private Integer weblicenseLimit;

    @Column
    private Integer desktopLicenseLimit;
            
    @Column
    private String license;

    @Column
    private String desktopLicense;
    
    @Column
    private Integer courseLimit;
    
    @Column
    private Integer courseSetting;
    
    @Column
    private Integer smsTransactional;
    
    @Column
    private Integer smsPromotional;
    
    @Column
    private Integer transactionalCounter;
    
    @Column
    private Integer promotionalCounter;
    
    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createOn;

    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updateOn;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId", insertable = false, updatable = false, nullable = false, unique = false)
    private UserInfo userInfo;

    public Integer getAdminLicenseId() {
        return adminLicenseId;
    }

    public void setAdminLicenseId(Integer adminLicenseId) {
        this.adminLicenseId = adminLicenseId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getValidity() {
        return validity;
    }

    public void setValidity(Integer validity) {
        this.validity = validity;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getSystemInfoId() {
        return systemInfoId;
    }

    public void setSystemInfoId(String systemInfoId) {
        this.systemInfoId = systemInfoId;
    }

    public Integer getWeblicenseLimit() {
        return weblicenseLimit;
    }

    public void setWeblicenseLimit(Integer weblicenseLimit) {
        this.weblicenseLimit = weblicenseLimit;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public Integer getDesktopLicenseLimit() {
        return desktopLicenseLimit;
    }

    public void setDesktopLicenseLimit(Integer desktopLicenseLimit) {
        this.desktopLicenseLimit = desktopLicenseLimit;
    }

    
    public String getDesktopLicense() {
        return desktopLicense;
    }

    public void setDesktopLicense(String desktopLicense) {
        this.desktopLicense = desktopLicense;
    }

    public Integer getCourseLimit() {
        return courseLimit;
    }

    public void setCourseLimit(Integer courseLimit) {
        this.courseLimit = courseLimit;
    }

    public Integer getCourseSetting() {
        return courseSetting;
    }

    public void setCourseSetting(Integer courseSetting) {
        this.courseSetting = courseSetting;
    }

    public Integer getSmsTransactional() {
        return smsTransactional;
    }

    public void setSmsTransactional(Integer smsTransactional) {
        this.smsTransactional = smsTransactional;
    }

    public Integer getSmsPromotional() {
        return smsPromotional;
    }

    public void setSmsPromotional(Integer smsPromotional) {
        this.smsPromotional = smsPromotional;
    }

    public Date getCreateOn() {
        return createOn;
    }

    public void setCreateOn(Date createOn) {
        this.createOn = createOn;
    }

    public Date getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(Date updateOn) {
        this.updateOn = updateOn;
    }

    public Integer getTransactionalCounter() {
        return transactionalCounter;
    }

    public void setTransactionalCounter(Integer transactionalCounter) {
        this.transactionalCounter = transactionalCounter;
    }

    public Integer getPromotionalCounter() {
        return promotionalCounter;
    }

    public void setPromotionalCounter(Integer promotionalCounter) {
        this.promotionalCounter = promotionalCounter;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public String toString() {
        return "AdminLicense{" + "adminLicenseId=" + adminLicenseId + ", userId=" + userId + ", validity=" + validity + ", issueDate=" + issueDate + ", expiryDate=" + expiryDate + ", systemInfoId=" + systemInfoId + ", weblicenseLimit=" + weblicenseLimit + ", desktopLicenseLimit=" + desktopLicenseLimit + ", license=" + license + ", desktopLicense=" + desktopLicense + ", courseLimit=" + courseLimit + ", courseSetting=" + courseSetting + ", smsTransactional=" + smsTransactional + ", smsPromotional=" + smsPromotional + ", createOn=" + createOn + ", updateOn=" + updateOn + '}';
    }
}
