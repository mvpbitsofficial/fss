/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.SecurityQuestion;
import java.util.List;

/**
 *
 * @author CoreTechies M8
 */
public interface SecurityQuestionDao {

    public SecurityQuestion saveSecurityQuestions(SecurityQuestion securityQuestion);

    public SecurityQuestion getSecurityQuestions(String question);

    public List<SecurityQuestion> getSecurityQuestions();
    
    public SecurityQuestion getSecurityQuestionById(Integer questionId);
    
    public Boolean deleteSecurityQuestions(Integer securityQuestionId);
}
