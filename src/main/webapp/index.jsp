<!DOCTYPE html>

<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>FSS | User Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <script>var myContextPath = "${pageContext.request.contextPath}";</script>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/dist/sweetalert.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" />
        <style>
            html{ 
                height:100%;
            }
            .login-body{
                background:url(${pageContext.request.contextPath}/assets/layouts/layout/img/bg.jpg); 
                background-size:cover;
            }
            .main-section.section-block { 
                background-color :#fff; 
                position:absolute; top:0; 
                right:0; 
                left:0; 
                min-height:600px;
            }
            .section-inner {
                margin: 0 auto;
                width: 890px;    
                padding-top:50px;
            }
            .header { 
                margin-bottom:60px; 
                position:relative;
            }
            .logo { 
                height :20px;
            }
            .section-body {
                margin-top:60px;
                position:relative;
                color:#58666e; 
            }
            .login-messaging {
                background :#eaeaea; 
                padding:60px; 
                border-radius:4px; 
                min-height:400px; 
                padding-right:400px; 
                -webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,0.2);    
                box-shadow: 0 2px 2px 0 rgba(0,0,0,0.2);
            }
            .login-messaging ul{ 
                padding:0px; margin:0px;
            }
            .login-messaging ul li{ 
                padding-bottom:15px;
            }
            .login-messaging ul li h4{
                margin:0px; 
                margin-bottom:2px;
            }
            .messaging-heading {    
                font-size: 16px;    
                font-weight: bold;
            }
            .messaging-text {
                font-size:14px;
                margin-top:10px;
            }
            .messaging-link {
                letter-spacing: 1px;    
                color: #6d5fba;    
                margin-top: 8px;    
                display: inline-block;    
                margin-bottom: 20px;
            }
            .messaging-link:hover { 
                text-decoration:none; 
                color:#2e2e2e;
            }
            .messaging-heading {   
                font-size: 16px;    
                font-weight: bold;
            }.m-t {    
                margin-top: 15px;
            }
            .login-content {
                position:absolute; 
                width:350px; 
                top:-30px; 
                right:30px; 
                bottom:-30px; 
                background-color:#fafafa; 
                -webkit-box-shadow: 0 5px 10px 0 rgba(0,0,0,0.15), 0 0 2px 0 rgba(0,0,0,0.2);    
                box-shadow: 0 5px 10px 0 rgba(0,0,0,0.15), 0 0 2px 0 rgba(0,0,0,0.2); 
                border-radius:4px; padding:40px;
            }
            .form-container { 
                padding-top:30px;
            }
            .form-control { 
                height:40px; 
                border-color: #cfdadd; 
                box-shadow:none;
            }
            .highlight-clickable-text {    
                cursor: pointer;    
                color:#013540;    
                text-decoration: underline; font-size:12px;
            }
            .btn btn-primary form-control:hover{    
                background-color: #204d73;
            }
            .mgt-20 { 
                margin-top:20px; 
            }
            .btn-blue , .btn-blue:active ,  .btn-blue:hover{ 
                border-color:#013540;
                background: rgba(5,74,89,1);
                background: -moz-linear-gradient(-45deg, rgba(5,74,89,1) 0%, rgba(1,53,64,1) 100%);
                background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(5,74,89,1)), color-stop(100%, rgba(1,53,64,1)));
                background: -webkit-linear-gradient(-45deg, rgba(5,74,89,1) 0%, rgba(1,53,64,1) 100%);
                background: -o-linear-gradient(-45deg, rgba(5,74,89,1) 0%, rgba(1,53,64,1) 100%);
                background: -ms-linear-gradient(-45deg, rgba(5,74,89,1) 0%, rgba(1,53,64,1) 100%);
                background: linear-gradient(135deg, rgba(5,74,89,1) 0%, rgba(1,53,64,1) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#054a59', endColorstr='#013540', GradientType=1 );
            }
        </style>
    </head>
    <!-- END HEAD -->

    <body class="login-body">
        <section class="main-section">
            <div class="container">
                <div class="section-block">
                    <div class="section-inner">
                        <div class="header">
                            <div class="logo"> <img class="logo" src="${pageContext.request.contextPath}/assets/layouts/layout/img/logo.png" alt="Logo"> </div>
                        </div>
                        <div class="section-body">
                            <div class="login-messaging">
                                
                            </div>
                            <div id="signin" class="login-content">
                                <div class="messaging-heading" style="font-size:18px;">Login to Dashboard </div>
                                <div class="messaging-text"> Enter your FSS Credentials </div>
                                <div class="form-container">
                                    <form action="#">
                                        <div class="form-group">
                                            <label class="">Email Address</label>
                                            <input type="text" class="form-control" id="userName" placeholder="Please enter email address">
                                            <span style="color: red;" id="errorMessageUserName"></span>
                                        </div>
                                        <div class="form-group">
                                            <label class="">Password</label>
                                            <input type="password" class="form-control" id="password" placeholder="Please enter the password">
                                            <span style="color: red;" id="errorMessagePassword"></span>
                                        </div>
                                        <div class="form-group">
                                            <a href="#" class="highlight-clickable-text" id="forget-password">Forget password?</a>
                                        </div>
                                        <button class="btn btn-danger btn-block btn-blue" id="login" type="button">Login</button>
                                        <span style="color: red;" id="errorMessageLogin"></span>
                                    </form>
                                </div>
                            </div>
                            <!-- BEGIN FORGOT PASSWORD FORM -->
                            <div id="forgot" style="display: none;" class="login-content">
                                <div class="messaging-heading" style="font-size:18px;">Forgot Password ?</div>
                                <div class="messaging-text">Enter your username and email id below to reset your password.</div>
                                <div class="form-container">
                                    <form action="#">
<!--                                    <h3 class="font-green">Forgot Password ?</h3>
                                        <p> Enter your username and email id below to reset your password. </p>-->
                                        <div class="form-group">
                                            <label class="">User Name</label>
                                            <input class="form-control" type="text" placeholder="UserName" id="username"/>
                                            <span style="color: red;" id="errorMessageusername"></span>
                                        </div>
                                        <div class="form-group">
                                            <label class="">Email</label>
                                            <input class="form-control" type="text" placeholder="Email Address" id="emailId"/>
                                            <span style="color: red;" id="errorMessageemailid"></span>
                                        </div>
                                        <div id="loading" style="display: none;">
                                            <center>
                                                <!--<span style="color: blue;">Processing...</span>-->
                                                <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/24.gif" />
                                            </center>
                                        </div>
                                        <span style="color: red;" id="errorResponse"></span>
                                        <span style="color: green;" id="successResponse"></span>
                                        <br />
                                        <div class="form-actions">
                                            <button type="button" id="back-btn" class="btn green btn-outline">Back</button>
                                            <button type="button" id="forgotPassword" class="btn btn-success uppercase pull-right">Submit</button>
                                        </div>
                                    </form>
                                </div>
                                
                            </div>
                        <!-- END FORGOT PASSWORD FORM -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- BEGIN CORE PLUGINS --> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/dist/sweetalert.min.js" type="text/javascript"></script>
        <script>
            $(document).on("click", "#login", function () {
                $("#errorMessageLogin").html("");
                var userName = $("#userName").val();
                var password = $("#password").val();
                var flag = true;
                if (userName === "") {
                    $("#errorMessageUserName").html("Please Enter Email Address");
                    flag = false;
                }
                if (password === "") {
                    $("#errorMessagePassword").html("Please Enter Password.");
                    flag = false;
                }
                if (flag) {
                    $.post(myContextPath + "/login", {userName: userName, password: password},
                            function (data, status) {
                                if (data.indexOf("success")!==-1) {
                                    //swal("Login Success!", "You have been logged in successfully.", "success");
                                    setTimeout(function()
                                    {
                                        window.location.href = myContextPath + "/" + data.split("#")[1].replace(" ", "") + "/dashboard";
                                    }, 400);
                                } else {
                                    $("#errorMessageLogin").html(data);
                                }
                            }
                    );
                }
            });
            $(document).on("keyup", "#userName", function () {
                var userName = $("#userName").val();
                if (userName === "") {
                    $("#errorMessageUserName").html("Please Enter User Name.");
                } else {
                    $("#errorMessageUserName").html("");
                }
            });
            $(document).on("keyup", "#password", function () {
                var password = $("#password").val();
                if (password === "") {
                    $("#errorMessagePassword").html("Please Enter Password.");
                } else {
                    $("#errorMessagePassword").html("");
                }
            });
            
        </script>
        <script>
            
            $("#forget-password").on("click", function(){
                $("#forgot").css("display", "block");
                $("#signin").css("display", "none");
                return false;
            });
            
            $("#back-btn").on("click", function(){
                $("#forgot").css("display", "none");
                $("#signin").css("display", "block");
                return false;
            });
            
            $(document).on("click", "#forgotPassword", function () {
                $("#errorMessageusername").html("");
                $("#errorMessageemailid").html("");
                $("#errorResponse").html("");
                $("#successResponse").html("");
                var username = $("#username").val();
                var email = $("#emailId").val();
                var flag = true;
                if(username === ""  || username === null){
                    $("#errorMessageusername").html("Please Enter User Name.");
                    flag = false;
                }
                if(email === ""  || email === null){
                    $("#errorMessageemailid").html("Please Enter Email ID.");
                    flag = false;
                }
                if(flag){
                    $("#loading").css('display','block');
                    $.post("${pageContext.request.contextPath}/forgotpasswordusername", {userName: username, emailId: email},
                        function (data, status) {
                            if (data === "success") {
                                $("#loading").css('display','none');
//                                $("#errorResponse").html("");
                                swal("Success!", "Your changed password is sent to your mail id.", "success");
//                                $("#successResponse").html("Your changed password is sent to your mail id.");
                            } else {
                                $("#loading").css('display','none');
                                swal("Request failed!", data, "error");
//                                $("#errorResponse").html(data);
//                                $("#successResponse").html("");
                            }
                    });
                }
                return false;
            });
        </script>
    </body>
</html>