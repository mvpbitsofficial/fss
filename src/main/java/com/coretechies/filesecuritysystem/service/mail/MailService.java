package com.coretechies.filesecuritysystem.service.mail;

/**

 */
public interface MailService {

    void sendMail(String subject, String content, String to);
}
