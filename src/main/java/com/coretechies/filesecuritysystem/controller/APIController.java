/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.controller;

import com.coretechies.filesecuritysystem.security.AES;
import com.coretechies.filesecuritysystem.security.UserLicenseInformation;
import com.coretechies.filesecuritysystem.security.UserSystemInformation;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author coretechiesi5
 */
@RestController
public class APIController {
    
    private final AES aes = new AES();
    
    /**
     *
     * @param courseId
     * @param courseName
     * @param systemId
     * @param userName
     * @param userId
     * @return
     */
    @RequestMapping(value = "/generateuserlicense", method = RequestMethod.POST)
    public String generateUserLicense(@RequestParam String courseId, @RequestParam String courseName, @RequestParam String systemId, @RequestParam String userName, @RequestParam Integer userId){
        
        if(courseId.equals("") || courseName.equals("") || systemId.equals("") || userName.equals("")){
            return "incomplete information";
        }
        if(userId == null){
            userId = 0;
        }
        try{
            String decrypt = aes.decrypt(systemId.trim(), "0123456789abcdef");
            String[] code = decrypt.split("#");
            if (code[1].trim().equals("FUSC")) {
                String rawText = courseId + ":" + courseName
                    + "#" + systemId
                    + "#" + new SimpleDateFormat("dd/MM/yyyy").format(new Date())
                    + "#" + ""
                    + "#" + "SAP"+"::"+""+ "::" + "5"
                    + "#" + "123"
                    + "#" + userName + "AD"+(userId +1000) + "FCLU";
                return aes.generate(rawText);
            }else{
                return "Invalid System id";
            }
        }catch(Exception e){
//            e.printStackTrace();
            return "Invalid System id";
        }
        
    }
    
    @RequestMapping(value = "/getuserlicenseinfo", method = RequestMethod.POST)
    public String getUserLicenseInformation(@RequestParam String licenseKey){
        try{
            return new UserLicenseInformation(licenseKey.trim()).getLicenseInfo();
        }catch(Exception e){
            return "Invalid license key.";
        }
    }
    
    @RequestMapping(value = "/getsystemidinfo", method = RequestMethod.POST)
    public String getSystemIdInformation(@RequestParam String systemId){
        try{
            return new UserSystemInformation(systemId.trim()).getSystemInfo();
        }catch(Exception e){
            return "Invalid system Id.";
        }
    }
    
}
