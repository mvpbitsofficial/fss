/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.SecurityQuestion;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("SecurityQuestionDao")
public class SecurityQuestionDaoImpl implements SecurityQuestionDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public SecurityQuestion saveSecurityQuestions(SecurityQuestion securityQuestion) {
        try {
            entityManager.persist(securityQuestion);
            return securityQuestion;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public SecurityQuestion getSecurityQuestions(String question) {
        try {
            TypedQuery<SecurityQuestion> createQuery = entityManager.createQuery("SELECT S FROM SecurityQuestion AS S WHERE S.question=:question", SecurityQuestion.class);
            createQuery.setParameter("question", question);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<SecurityQuestion> getSecurityQuestions() {
        try {
            return entityManager.createQuery("SELECT S FROM SecurityQuestion AS S", SecurityQuestion.class).getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    @Override
    public SecurityQuestion getSecurityQuestionById(Integer questionId){
        try {
            TypedQuery<SecurityQuestion> query = entityManager.createQuery("SELECT S FROM SecurityQuestion AS S WHERE S.securityQuestionId=:questionId", SecurityQuestion.class);
            query.setParameter("questionId", questionId);
            return query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    
    @Override
    @Transactional
    public Boolean deleteSecurityQuestions(Integer securityQuestionId) {
        try {
            entityManager.remove(entityManager.find(SecurityQuestion.class, securityQuestionId));
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
