/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.service;

import com.coretechies.filesecuritysystem.algorithm.NotificationType;
import com.coretechies.filesecuritysystem.dao.NotificationDao;
import com.coretechies.filesecuritysystem.dao.SystemInfoDao;
import com.coretechies.filesecuritysystem.domain.SystemInfo;
import com.coretechies.filesecuritysystem.security.AES;
import com.coretechies.filesecuritysystem.security.InvalidInformationException;
import java.util.Date;
import java.util.List;
import com.coretechies.filesecuritysystem.domain.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SystemInfoServiceImpl implements SystemInfoService {

    @Autowired
    private NotificationDao notificationDao;
     
    @Autowired
    private SystemInfoDao systemInfoDao;

    @Override
    public SystemInfo saveSystemInfo(String systemName, String systemId, Integer userId, Integer cretedBy) throws InvalidInformationException {
        try {
            AES aes = new AES();
            String decrypt = aes.decrypt(systemId.trim(), "0123456789abcdef");
            String[] code = decrypt.split("#");
            if (code[1].trim().equals("FUSC")) {
                SystemInfo systemInfo = new SystemInfo();
                systemInfo.setSystemId(systemId);
                systemInfo.setSystemName(systemName);
                systemInfo.setUserId(userId);
                systemInfo.setCreatedBy(cretedBy);
                Date date=new Date();
                systemInfo.setCreationDate(date);
                systemInfo.setUpdationDate(date);
                SystemInfo saveSystemInfo = systemInfoDao.saveSystemInfo(systemInfo);
                Notification notification = new Notification();
                notification.setCreateOn(date);
                notification.setCreatedBy(cretedBy);
                notification.setNotificationMessage("System "+systemName+" is created");
                notification.setNotificationType(NotificationType.System_History.getNotificationTypeValue());
                notification.setSeenby("0,");
                notification.setUserId(userId);
                notificationDao.saveNotification(notification);
                return saveSystemInfo;
            }
        } catch (Exception ex) {
        }
        throw new InvalidInformationException("Invalid System Id.");
    }

    @Override
    public List<SystemInfo> getSystemInfoByCreator(Integer createdBy) {
        return systemInfoDao.getSystemInfoByCreator(createdBy);
    }

    @Override
    public Integer getSystemInfoCountByCreator(Integer createdBy) {
        List<SystemInfo> systemInfos = systemInfoDao.getSystemInfoByCreator(createdBy);
        if (systemInfos != null) {
            System.out.println("COUNT===="+systemInfos.size());
            return systemInfos.size();
        } else {
            return 0;
        }
    }

    @Override
    public boolean deleteSystemId(Integer systemInfoId) {
        return systemInfoDao.deleteSystemId(systemInfoId);
    }

    @Override
    public SystemInfo getSystemInfoBySystemId(Integer userId, String systemName) {
        return systemInfoDao.getSystemInfoBySystemId(userId, systemName);
    }

    @Override
    public SystemInfo getSystemInfoBySystemName(Integer userId, String systemName) {
        return systemInfoDao.getSystemInfoBySystemName(userId, systemName);
    }

    @Override
    public SystemInfo getSystemInfo(Integer userInfoId, Integer systemInfoId) {
        return systemInfoDao.getSystemInfo(userInfoId, systemInfoId);
    }

    @Override
    public String getSystemInfoByUser(Integer userId) {
        List<SystemInfo> systemInfos = systemInfoDao.getSystemInfoByUser(userId);
        String data = "success#";
        for (SystemInfo systemInfo : systemInfos) {
            data += "<option value=\"" + systemInfo.getSystemInfoId() + "\">" + systemInfo.getSystemName() + "</option>";
        }
        return data;
    }

    @Override
    public List<SystemInfo> getSystemInfoListByUser(Integer userId) {
        return systemInfoDao.getSystemInfoByUser(userId);
    }
    
    @Override
    public Integer getSystemInfoCount(){
        List<SystemInfo> systems = systemInfoDao.getAllSystemList();
        if(systems != null){
            return systems.size();
        }else{
            return 0;
        }
    }
    
    @Override
    public Boolean editSystemName(SystemInfo systemInfo){
        return systemInfoDao.editSystemName(systemInfo);
    }
}
