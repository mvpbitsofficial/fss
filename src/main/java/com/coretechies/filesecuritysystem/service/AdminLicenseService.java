/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.service;

import com.coretechies.filesecuritysystem.domain.AdminLicense;
import com.coretechies.filesecuritysystem.security.InvalidInformationException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author CoreTechies M8
 */
public interface AdminLicenseService {

    public AdminLicense getAdminLicense(Integer creatorid, Integer clientuserid, Integer validity,
            Date startDate, Date endDate, String systemId, Boolean webunlimited, Boolean desktopunlimited, 
            Integer weblicenseLimit, Integer dekstoplicenselimit,Boolean unlimitedcourse, Integer courselimit,
            Integer courseSetting, Integer smsTransaction, Integer smsPromotion, Integer carryForward) throws InvalidInformationException;

    public AdminLicense getAdminLicense(Integer creatorid, Integer clientuserid, Integer validity,
            Date startDate, Date endDate, String systemId, Boolean webunlimited, Boolean desktopunlimited,
            Integer weblicenseLimit, Integer dekstoplicenselimit, Boolean unlimitedcourse, Integer courselimit,
            Integer courseSetting, Integer smsTransaction, Integer smsPromotion) throws InvalidInformationException;

    public String saveAdminLicense(AdminLicense adminLicense, Integer creator);
    
    public String editAdminLicense(AdminLicense adminLicense, Integer creatorid);

    /**
     *
     * @param creatorid
     * @param licenseId
     * @param lifetime
     * @param validity
     * @param startDate
     * @param endDate
     * @param webunlimited
     * @param desktopunlimited
     * @param weblicenseLimit
     * @param desktoplicenseLimit
     * @param carryForward:0 - For No:1 - For Yes
     * @return
     * @throws com.coretechies.filesecuritysystem.security.InvalidInformationException
     */
    public AdminLicense getRenewAdminLicense(Integer creatorid, Integer licenseId, Boolean lifetime, Integer validity, Date startDate, Date endDate, Boolean webunlimited, Boolean desktopunlimited, Integer weblicenseLimit, Integer desktoplicenseLimit, Integer carryForward) throws InvalidInformationException;

    public AdminLicense getEditLicense(Integer licenseId, String systemId, Boolean unlimitedcourse, Integer courselimit,
            Integer courseSetting, Integer smsTransaction, Integer smsPromotion ) throws InvalidInformationException;
    
    public List<AdminLicense> getLicenseExpireAlert();

    public List<AdminLicense> getAdminLicenseListByExpiryDate(Date fromDate, Date toDate);

    public List<AdminLicense> getAdminLicenseListByCreateOn(Date fromDate, Date toDate);
    
    public Integer getAdminLicenseCount();
    
    public AdminLicense getAdminLicenseByClient(Integer clientuserid);
    
    public Integer getClientLicenseForChart(Date date);
    
    public AdminLicense getAdminLicenseById(Integer adminLicenseId);
}
