
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>FSS | Edit License</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
        <style>
            .input-daterange .input-group-addon{
                padding: 6px 12px;
                width: 1%;
            }
        #other-email-section {
                display: none;
        }

        .number-dropdown{ width:200px;} 
        .number-dropdown .btn-group{width:100%;}
        .number-dropdown .btn-group label{ width:calc(100%/5);}

        .number-dropdown .btn-group label.btn-default.active{ background:#e7505a; border-color:#e7505a; color:#fff;}
        .number-dropdown .btn-group .btn+.btn{ margin-left:0px;}
        .form-wizard .steps > li.done > a.step .desc i{display: none !important;}
        </style>
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top"> 
            <!-- BEGIN HEADER INNER -->
            <jsp:include page="topbar.jsp" />
            <!-- END HEADER INNER --> 
        </div>
        <!-- END HEADER --> 
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER --> 
        <!-- BEGIN CONTAINER -->
        <div class="page-container"> 
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper"> 
                <!-- BEGIN SIDEBAR --> 
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse"> 
                    <!-- BEGIN SIDEBAR MENU --> 
                    <jsp:include page="sidebar.jsp" />
<!--                    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                         DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
                        <li class="sidebar-toggler-wrapper hide"> 
                             BEGIN SIDEBAR TOGGLER BUTTON 
                            <div class="sidebar-toggler"> <span></span> </div>
                             END SIDEBAR TOGGLER BUTTON  
                        </li>
                        <li class="nav-item  "> <a href="dashboard" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>

                        </li>
                        <li class="nav-item  active"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">License</span>  </a>

                        </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>

                        </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
                    </ul>-->
                    <!-- END SIDEBAR MENU --> 
                    <!-- END SIDEBAR MENU --> 
                </div>
                <!-- END SIDEBAR --> 
            </div>
            <!-- END SIDEBAR --> 
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper"> 
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content"> 
                   <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light portlet-fit portlet-form  portlet-datatable transparet_bg">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-key "></i>
                                        <span class="caption-subject  sbold uppercase">Edit License</span>
                                    </div>
                                   
                                </div>
                                <div class="portlet-body clearfix">
                                    <div class="col-md-12 clearfix" style="margin: auto; float: none;">
                                        <form class="create_licence_form  form-horizontal">
                                        
                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                              <h4 class="panel-title">
                                                <div id="ribbon">
                                                    <span id="content"><i class="fa fa-check"></i></span>
                                                </div>
                                                <b>Company Details</b>
                                              </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                <div class="panel-body">
                                                    <div>                
                                                        <input type="hidden" id="licenseId" value="${license.adminLicenseId}"/>
                                                    </div>
                                                    <div class="form-group clearfix">
                                                        <label class="col-sm-4 control-label"> Client</label>
                                                        <div class="col-sm-7">
                                                            <div class="input-group">
                                                                <span class="input-group-addon input-circle-left">
                                                                    <i class="fa fa-user"></i>
                                                                </span>
                                                                <input type="text" class="form-control input-circle-right" readonly value="${license.userInfo.userName}" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group clearfix">
                                                        <label class="col-sm-4 control-label"> Organization</label>
                                                        <div class="col-sm-7">
                                                            <div class="input-group">
                                                                <span class="input-group-addon input-circle-left">
                                                                    <i class="fa fa-building-o"></i>
                                                                </span>
                                                                <input type="text" readonly class="form-control input-circle-right" placeholder="Organization" value="${license.userInfo.organization}" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group clearfix">
                                                        <label class="col-sm-4 control-label">System ID</label>
                                                        <div class="one-half col-sm-7">
                                                            <div class="one input-group">
                                                                <span class="input-group-addon input-circle-left">
                                                                    <i class="fa fa-desktop"></i>
                                                                </span>
                                                                <input class="form-control input-circle-right" placeholder=" System ID" id="systemId" value="${license.systemInfoId}">
                                                            </div>
                                                            <div class="two">
                                                                <a href="javascript:;" id="resetsystemId" class="btn  btn-default btn-circle"><i class="fa fa-repeat"></i></a>
                                                            </div>
                                                            <span style="color: red; display: block; font-size: 14px;" id="errorsystemid"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group clearfix">
                                                        <a class="collapsed btn btn-circle btn-success" id="companyDetails" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapseTwo" aria-controls="collapseTwo" style="float: right;">Next</a>
                                                    </div>  
                                                </div>
                                            </div>
                                          </div>
                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingTwo">
                                              <h4 class="panel-title">
                                                <div id="ribbon">
                                                    <span id="content"><i class="fa fa-check"></i></span>
                                                </div>
                                                <b>License Validity </b>
                                              </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                              <div class="panel-body">
                                              	<div class="form-group clearfix">
                                                    <label class="col-sm-4 control-label"> Validity</label>
                                                    <div class="one">
                                                        <div class="col-sm-7">
                                                            <div class="input-group">
                                                                <span class="input-group-addon input-circle-left">
                                                                    <i class="fa fa-star"></i>
                                                                </span>
                                                                <input type="text" readonly class="form-control input-circle-right" value="${license.validity}" placeholder="Validity" />   
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="form-group clearfix">
                                                    <label class="col-sm-4 control-label"> Issue Date</label>
                                                    <div class="col-sm-7">
                                                        <div class="input-group">
                                                            <span class="input-group-addon input-circle-left">
                                                                <i class="fa fa-calendar"></i>
                                                            </span>
                                                            <fmt:formatDate value="${license.issueDate}"  
                                                                            type="date" 
                                                                            pattern="dd-MM-yyyy"
                                                                            var="theFormattedIssueDate" />
                                                            <input class="form-control input-circle-right" readonly placeholder=" Issue Date" value="${theFormattedIssueDate}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix">
                                                    <label class="col-sm-4 control-label"> Expiry Date</label>
                                                    <div class="col-sm-7">
                                                        <div class="input-group" >
                                                            <span class="input-group-addon input-circle-left">
                                                                <i class="fa fa-calendar"></i>
                                                            </span>
                                                            <c:if test="${license.expiryDate == null}">
                                                               <input class="form-control input-circle-right" readonly placeholder=" Expiry Date" value="Life Time"> 
                                                            </c:if>
                                                            <c:if test="${license.expiryDate != null}">
                                                                <fmt:formatDate value="${license.expiryDate}"  
                                                                            type="date" 
                                                                            pattern="dd-MM-yyyy"
                                                                            var="theFormattedExpiryDate" />
                                                                <input class="form-control input-circle-right" readonly placeholder=" Expiry Date" value="${theFormattedExpiryDate}">
                                                            </c:if>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix"> 
                                                    <a class="collapsed btn btn-circle btn-success" id="licenseValidity" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapseThree" aria-controls="collapseThree" style="float: right;">Next</a>
                                                    <a class="collapsed btn btn-circle btn-danger" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapseOne" aria-controls="collapseOne" style="float: right; margin-right: 10px;">Previous</a>
                                                </div>  
                                              </div>
                                            </div>
                                          </div>
                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingThree">
                                              <h4 class="panel-title">
                                                <div id="ribbon">
                                                    <span id="content"><i class="fa fa-check"></i></span>
                                                </div>
                                                <b>License  Limit </b>
                                              </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                <div class="panel-body">
                                                    <div class="form-group clearfix">
                                                        <label class="col-sm-4 control-label">Web Limit</label>
                                                        <div class="one col-sm-7">
                                                            <div class="input-group">
                                                                <span class="input-group-addon input-circle-left">
                                                                    <i class="fa fa-hand-paper-o"></i>
                                                                </span>
                                                                <c:if test="${license.weblicenseLimit == -1}">
                                                                    <input class="form-control input-circle-right" readonly placeholder=" License Limit for web" value="Unlimited">
                                                                </c:if>
                                                                <c:if test="${license.weblicenseLimit != -1}">
                                                                    <input class="form-control input-circle-right" readonly placeholder=" License Limit for web" value="${license.weblicenseLimit}">
                                                                </c:if> 
                                                            </div>
                                                        </div>
                                                    </div>
                                            
                                                    <div class="form-group clearfix">
                                                        <label class="col-sm-4 control-label">Desktop  Limit</label>
                                                        <div class="one col-sm-7">
                                                            <div class="input-group">
                                                                <span class="input-group-addon input-circle-left">
                                                                    <i class="fa fa-hand-paper-o"></i>
                                                                </span>
                                                                <c:if test="${license.desktopLicenseLimit == -1}">
                                                                    <input class="form-control input-circle-right" readonly placeholder=" License Limit for desktop" value="Unlimited">
                                                                </c:if>
                                                                <c:if test="${license.desktopLicenseLimit != -1}">
                                                                   <input class="form-control input-circle-right" readonly placeholder=" License Limit for desktop" value="${license.desktopLicenseLimit}"> 
                                                                </c:if>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group clearfix">
                                                        <label class="col-sm-4 control-label">Total Limit</label>
                                                        <div class="col-sm-7">
                                                            <div class="one">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon input-circle-left">
                                                                        <i class="fa fa-hand-paper-o"></i>
                                                                    </span>
                                                                    <c:if test="${license.weblicenseLimit == -1 || license.desktopLicenseLimit == -1}">
                                                                        <input class="form-control input-circle-right" readonly placeholder=" Total License Limit" value="Unlimited">
                                                                    </c:if>
                                                                    <c:if test="${license.weblicenseLimit != -1 || license.desktopLicenseLimit != -1}">
                                                                        <c:set var="total" value="${license.weblicenseLimit + license.desktopLicenseLimit}" />
                                                                        <input class="form-control input-circle-right" readonly placeholder=" Total License Limit" value="${total}">
                                                                    </c:if>    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group clearfix"> 
                                                        <a class="collapsed btn btn-circle btn-success" id="licenseLimit" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapseFour" aria-controls="collapseFour" style="float: right;">Next</a>
                                                        <a class="collapsed btn btn-circle btn-danger" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapseTwo" aria-controls="collapseTwo" style="float: right; margin-right: 10px;">Previous</a>
                                                    </div> 
                                                </div>
                                            </div>
                                          </div>
                                          
                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingFour">
                                              <h4 class="panel-title">
                                                <div id="ribbon">
                                                    <span id="content"><i class="fa fa-check"></i></span>
                                                </div>
                                                <b>Course</b>
                                              </h4>
                                            </div>
                                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                              <div class="panel-body">
                                               <div class="form-group clearfix">
                                                <label class="col-sm-4 control-label">Total Courses</label>
                                                <div class="one-half col-sm-7">
                                                    <c:if test="${license.courseLimit == -1}">
                                                        <div class="two" style="float:right">
                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="margin-top: 8px;">
                                                                <input type="checkbox" class="group-checkable" checked data-set="#sample_5 .checkboxes"  id="unlimitedcourse"/>
                                                                <span></span>
                                                            </label>

                                                        </div>
                                                        <div class="one" style="float:left">
                                                            <div class="input-group">
                                                                <span class="input-group-addon input-circle-left">
                                                                    <i class="fa fa-book"></i>
                                                                </span>
                                                                <input class="form-control input-circle-right" id="courselimit" disabled placeholder="Upload Course limit">  
                                                            </div>
                                                            <div class="hint text-right">(please click checkbox for unlimited course)</div>
                                                            <span style="color: red; display: block; font-size: 14px; margin-left: 0px;" id="errorcourselimit" ></span>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${license.courseLimit != -1}">
                                                        <div class="two" style="float:right">
                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="margin-top: 8px;">
                                                                <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes"  id="unlimitedcourse"/>
                                                                <span></span>
                                                            </label>

                                                        </div>
                                                        <div class="one" style="float:left">
                                                            <div class="input-group">
                                                                <span class="input-group-addon input-circle-left">
                                                                    <i class="fa fa-book"></i>
                                                                </span>
                                                                <input class="form-control input-circle-right" id="courselimit" placeholder="Upload Course limit" value="${license.courseLimit}">
                                                            </div>
                                                            <div class="hint text-right">(please click checkbox for unlimited course)</div>
                                                            <span style="color: red; display: block; font-size: 14px; margin-left: 0px;" id="errorcourselimit" ></span>
                                                        </div> 
                                                    </c:if>
                                                </div>  
                                            </div> 
                                            
                                            <div class="form-group clearfix">
                                                <label class="col-sm-4 control-label">Course setting</label>
                                                <div class="one-half col-sm-7">
                                                    <c:if test="${license.courseSetting == 1}">
                                                        <div class="one" >
                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="margin-top: 8px;">
                                                                <input type="checkbox" class="group-checkable" checked data-set="#sample_5 .checkboxes" id="excelupload"/>
                                                                <span></span>
                                                            </label>
                                                            <span>Excel Upload</span>
                                                        </div>
                                                        <div class="one">
                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="margin-top: 8px;">
                                                                <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes" id="backupfileupload"/>
                                                                <span></span>
                                                            </label>
                                                            <span>Backup File Upload</span>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${license.courseSetting == 2}">
                                                        <div class="one" >
                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="margin-top: 8px;">
                                                                <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes" id="excelupload"/>
                                                                <span></span>
                                                            </label>
                                                            <span>Excel Upload</span>
                                                        </div>
                                                        <div class="one">
                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="margin-top: 8px;">
                                                                <input type="checkbox" class="group-checkable" checked data-set="#sample_5 .checkboxes" id="backupfileupload"/>
                                                                <span></span>
                                                            </label>
                                                            <span>Backup File Upload</span>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${license.courseSetting == 3}">
                                                        <div class="one" >
                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="margin-top: 8px;">
                                                                <input type="checkbox" class="group-checkable" checked data-set="#sample_5 .checkboxes" id="excelupload"/>
                                                                <span></span>
                                                            </label>
                                                            <span>Excel Upload</span>
                                                        </div>
                                                        <div class="one">
                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="margin-top: 8px;">
                                                                <input type="checkbox" class="group-checkable" checked data-set="#sample_5 .checkboxes" id="backupfileupload"/>
                                                                <span></span>
                                                            </label>
                                                            <span>Backup File Upload</span>
                                                        </div>
                                                    </c:if>
                                                </div>
                                            </div>
                                            <span style="color: red; display: block; font-size: 14px; margin-left: 220px; margin-bottom: 10px;" id="errorcoursesetting" ></span>
                                            <div class="form-group clearfix"> 
                                                <a class="collapsed btn btn-circle btn-success" id="course" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapseFive" aria-controls="collapseFive" style="float: right;">Next</a>
                                                <a class="collapsed btn btn-circle btn-danger" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapseThree" aria-controls="collapseThree" style="float: right; margin-right: 10px;">Previous</a>
                                            </div>
                                            </div>
                                            </div>
                                          </div>
                                          
                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingFive">
                                              <h4 class="panel-title">
                                                <div id="ribbon">
                                                    <span id="content"><i class="fa fa-check"></i></span>
                                                </div>
                                                <b>SMS</b>
                                              </h4>
                                            </div>
                                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                              <div class="panel-body">
                                               	<div class="form-group clearfix">
                                                    <label class="col-sm-4 control-label">SMS Transactional</label>
                                                    <div class="one">
                                                        <div class="col-sm-7">
                                                            <div class="input-group">
                                                                <span class="input-group-addon input-circle-left">
                                                                    <i class="fa fa-envelope"></i>
                                                                </span>
                                                                <input class="form-control input-circle-right" placeholder="Total allowed SMS Transactional " id="smslimit" value="${license.smsTransactional}">
                                                            </div>
                                                            <span style="color: red; display: block; font-size: 14px;" id="errorsmslimit" ></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                                <div class="form-group clearfix">
                                                    <label class="col-sm-4 control-label">SMS Promotional</label>
                                                    <div class="one">
                                                        <div class="col-sm-7">
                                                            <div class="input-group">
                                                                <span class="input-group-addon input-circle-left">
                                                                    <i class="fa fa-envelope"></i>
                                                                </span>
                                                                <input class="form-control input-circle-right" placeholder="Total allowed SMS Promotional " id="promotionallimit" value="${license.smsPromotional}">
                                                            </div>
                                                            <span style="color: red; display: block; font-size: 14px;" id="errorpromotionallimit" ></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group clearfix"> 
                                                    <a class="btn btn-circle btn-success" id="generateLicense" style="float: right; margin-left: 10px;"><i class="fa fa-key"></i> Generate License</a>
                                                    <a class="collapsed btn btn-circle btn-danger" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" href="#collapseFour" aria-controls="collapseFour" style="float: right;">Previous</a>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          
                                          <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingSix">
                                              <h4 class="panel-title">
                                                <div id="ribbon">
                                                    <span id="content"><i class="fa fa-check"></i></span>
                                                </div>
                                                <b>OTP</b>
                                              </h4>
                                            </div>
                                            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                              <div class="panel-body">
                                               	<div class="form-group clearfix">
                                                    <label class="col-sm-4 control-label">License Key</label>
                                                    <div class="two-half col-sm-7">
                                                        <div class="one input-group">
                                                            <span class="input-group-addon input-circle-left">
                                                                <i class="fa fa-credit-card"></i>
                                                            </span>
                                                            <input readonly class="form-control input-circle-right" placeholder=" License Key" id="licenceKey"/>
                                                        </div>
                                                        <div class="two">
                                                            <a id="copy" class="btn  btn-default btn-circle"><i class="fa fa-copy"></i></a>
                                                            <a href="javascript:;" id="keydownload" class="btn  btn-default btn-circle"><i class="fa fa-download"></i></a>
                                                        </div>
                                                    </div> 
                                                </div>
                                                <span style="color: red; display: block; font-size: 14px; margin-left: 225px;" id="responsedisplay" ></span>
                                                <span style="color: green; display: block; font-size: 14px; margin-left: 225px;" id="responsesuccess" ></span>
                                              </div>
                                            </div>
                                          </div>
                                          
                                        </div>
                                        <div id="loading" style="display: none;">
                                            <center>
                                                <!--<span style="color: blue;">Processing...</span>-->
                                                <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/24.gif" />
                                            </center>
                                        </div>
<!--                                        <div class="form-group clearfix text-right" id="button">
                                            <a class="btn btn-circle btn-success" id="generateLicense"><i class="fa fa-key"></i> Generate License</a>
                                        </div>-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>               
                    </div>


                </div>
                <!-- END CONTENT BODY -->  
            </div>
            <!-- END CONTENT --> 
            <!-- BEGIN QUICK SIDEBAR --> 
            <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 

            <!-- END QUICK SIDEBAR --> 
        </div>
        <!-- END CONTAINER --> 
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; Organization </div>
            <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
        </div>
        <!-- END FOOTER --> 
        <div class="modal fade " id="otpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm text-center" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Send OTP</h4>
                    </div>
                    <div class="modal-body">
                        <div class="mt-radio-inline">
                            <label class="mt-radio">
                                <input type="radio" name="sendotp" class="radiovalue" value="1" checked=""> Email
                                <span></span>
                            </label>
                            <label class="mt-radio">
                                <input type="radio" name="sendotp" class="radiovalue" value="2" > Mobile
                                <span></span>
                            </label>			
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align:center;">
                      <button type="button" class="btn btn-success btn-circle" data-dismiss="modal" data-toggle="modal" data-target="" id="createlicense"> Send <i class="fa fa-angle-right"></i> </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade " id="otpModal_text" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm text-center" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Confirm OTP</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="random"/>
                  <input type="text" class="form-control input-circle" id="otp"/>
                  <span style="font-size: smaller; font-weight:bold;  color: red" id="otpError" ></span>
                </div>
                <div class="modal-footer" style="text-align:center;">
                  <button type="button" class="btn btn-success btn-circle otp_btn" id="submitotp"> Continue <i class="fa fa-angle-right"></i> </button>
                  <button class="btn btn-default btn-circle otp_btn" type="button" id="resendotp">Resend OTP</button>
                </div>
              </div>
            </div>
          </div>

       
        <!-- BEGIN CORE PLUGINS --> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/scripts/bootstrap-select.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
        <!-- END CORE PLUGINS --> 
        <!-- BEGIN THEME GLOBAL SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
        <!-- END THEME GLOBAL SCRIPTS --> 
        <!-- BEGIN THEME LAYOUT SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
        <!-- END THEME LAYOUT SCRIPTS --> 
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="${pageContext.request.contextPath}/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="${pageContext.request.contextPath}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
        <script>
        $( ".form-control" ).focus(function() {
            $(this).prev('.input-group-addon').removeClass().addClass('input-group-addon-focus');
            $(this).next('.input-group-addon').removeClass().addClass('input-group-addon-focus');
        });

        $( ".form-control" ).focusout(function() {
            $(this).prev('.input-group-addon-focus').removeClass().addClass('input-group-addon');
            $(this).next('.input-group-addon-focus').removeClass().addClass('input-group-addon');
        });
        </script>
        <script>
            var systemID;
            $("#resetsystemId").on("click", function(){
                $("#systemId").val("");
                $("#errorsystemid").text("Please enter system ID.");
                $("#companyDetails").css("display", "none");
            });
            $("#systemId").on("keyup", function(){
                var systemId = $("#systemId").val();
                if(systemId === systemID){
                    $("#companyDetails").css("display", "block");
                    $("#errorsystemid").text("");
                } else{
                    $("#companyDetails").css("display", "none");
                }
            });
            $("#systemId").on("change", function(){
                var systemId = $("#systemId").val();
                if(systemId === null || systemId === ""){
                    $("#errorsystemid").text("Please Enter System ID");
                } else{
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/checksystemid",{systemId: systemId},
                        function (data, status){
                            console.log(data);
                            if (data === "success"){
                                $("#errorsystemid").text("");
                                $("#companyDetails").css("display", "block");
                            } else{
                                $("#errorsystemid").text(data);
                            }
                        }
                    );
                }
            });
            $("#companyDetails").on("click", function(e){
                var systemId = $("#systemId").val();
                var flag = true;
                
                if(systemId === null || systemId === ""){
                    $("#errorsystemid").text("Please Enter System ID");
                    flag = false;
                } else{
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/checksystemid",{systemId: systemId},
                        function (data, status){
                            console.log(data);
                            if (data === "success"){
                                $("#errorsystemid").text("");
                                $("#companyDetails").css("display", "block");
                            } else{
                                $("#errorsystemid").text(data); 
                            }
                        });
                }
                if(!flag){
                    e.stopImmediatePropagation();
                    return false;
                }
            });
            $("#unlimitedcourse").on("click", function(){
                if($("#unlimitedcourse").is(":checked")){
                    $("#courselimit").val("");
                    $("#errorcourselimit").text("");
                    $("#courselimit").prop( "disabled", true );
                } else{
                    $("#courselimit").prop( "disabled", false );
                }
            });
            $("#courselimit").on("change", function(){
                var courselimit= $("#courselimit").val();
                var numericExp =/^[0-9]+$/;
                if(courselimit === null || courselimit === ""){
                    $("#errorcourselimit").text("Please Enter course Limit");
                } else if(!courselimit.match(numericExp)){
                    $("#errorcourselimit").text("Invalid value for Course Limit");
                } else{
                    $("#errorcourselimit").text("");
                }
            });
            $("#course").on("click", function(e){
                var unlimitedcourse = $("#unlimitedcourse").is(":checked");
                var courselimit= $("#courselimit").val();
                var excelupload = $("#excelupload").is(":checked");
                var backupfileupload = $("#backupfileupload").is(":checked");
                var numericExp=/^[0-9]+$/;
                var flag=true;
                if(!unlimitedcourse){
                    if(courselimit === null || courselimit === ""){
                        $("#errorcourselimit").text("Please Enter course Limit");
                        flag = false;
                    } else if(!courselimit.match(numericExp)){
                        $("#errorcourselimit").text("Invalid value for Course Limit");
                        flag = false;
                    } else{
                        $("#errorcourselimit").text("");
                    }
                }
                if(!excelupload && !backupfileupload){
                    console.log(excelupload);
                    console.log(backupfileupload);
                    $("#errorcoursesetting").text("Please check atleast one checkbox");
                    flag = false; 
                } else{
                    $("#errorcoursesetting").text("");
                }
                if(!flag){
                    e.stopImmediatePropagation();
                    return false;
                }
            });
            $("#smslimit").on("change", function(){
                var smslimit= $("#smslimit").val();
                var numericExp =/^[0-9]+$/;
                if(smslimit === null || smslimit === ""){
                    $("#errorsmslimit").text("Please Enter SMS transaction limit");
                } else if(!smslimit.match(numericExp)){
                    $("#errorsmslimit").text("Invalid value for SMS transaction Limit");
                } else{
                    $("#errorsmslimit").text("");
                }
            });
            $("#promotionallimit").on("change", function(){
                var promotionallimit= $("#promotionallimit").val();
                var numericExp =/^[0-9]+$/;
                if(promotionallimit === null || promotionallimit === ""){
                    $("#errorpromotionallimit").text("Please Enter SMS promotional limit");
                } else if(!promotionallimit.match(numericExp)){
                    $("#errorpromotionallimit").text("Invalid value for SMS promotional Limit");
                } else{
                    $("#errorpromotionallimit").text("");
                }
            });
            $("#generateLicense").on("click", function () {
                $("#errorsmslimit").text("");
                $("#errorpromotionallimit").text("");
                var smslimit= $("#smslimit").val();
                var promotionallimit= $("#promotionallimit").val();
                var numericExp=/^[0-9]+$/;
                var flag=true;
                
                if(smslimit === null || smslimit === ""){
                    $("#errorsmslimit").text("Please Enter SMS transaction limit");
                    flag = false;
                } else if(!smslimit.match(numericExp)){
                    $("#errorsmslimit").text("Invalid value for SMS transaction Limit");
                    flag = false;
                }
                if(promotionallimit === null || promotionallimit === ""){
                    $("#errorpromotionallimit").text("Please Enter SMS promotional limit");
                    flag = false;
                } else if(!promotionallimit.match(numericExp)){
                    $("#errorpromotionallimit").text("Invalid value for SMS promotional Limit");
                    flag = false;
                } 
                if(flag){
                    $("#otpModal").modal("show");
                }
            });
            
            $("#createlicense").on("click", function () { 
                var licenseid = $("#licenseId").val();
                var systemId = $("#systemId").val();
                var radioValue = $("input[name='sendotp']:checked"). val();
                var unlimitedcourse = $("#unlimitedcourse").is(":checked");
                var courselimit= $("#courselimit").val();
                var excelupload = $("#excelupload").is(":checked");
                var backupfileupload = $("#backupfileupload").is(":checked");
                var coursesetting;
                var smslimit= $("#smslimit").val();
                var promotionallimit= $("#promotionallimit").val();
                if(excelupload){
                    coursesetting = 1;
                }
                if(backupfileupload){
                    coursesetting = 2;
                }
                if(excelupload && backupfileupload){
                    coursesetting = 3;
                }
                var Uname = "${user.userName}".replace(" ", "");
                $("#loading").css('display','block');
                $.post("${pageContext.request.contextPath}/"+Uname+"/editlicense",
                        {licenseId: licenseid,
                            systemId: systemId,
                            unlimitedcourse: unlimitedcourse,
                            courselimit: courselimit,
                            courseSetting: coursesetting,
                            smsTransaction: smslimit,
                            smsPromotion: promotionallimit,
                            sendOtpOn: radioValue},
                        function (data, status) {
                            if (data === "please login again") {
                                $("#loading").css("display","none");
                                window.location.href = "/";
                            } else if (data.indexOf("success") > -1) {
                                $("#loading").css("display","none");
                                $("#random").val(data.split("#")[1]);
                                $("#otpModal_text").modal("show");
                            } else {
                                $("#loading").css("display","none");
                                $("#responsedisplay").text(data);
                            }
                });
            });
            
        </script>
        <script>
            $(document).on("click", "#submitotp", function () {
                $("#otpError").text("");
                $("#collapseFive").removeClass("in");
                $("#collapseSix").addClass("in");
                var otp = $("#otp").val();
                var random = $("#random").val();
                if (otp === "") {
                    $("#otpError").text("Please enter OTP");
                } else if (random === "") {
                   $("#otpError").text("Login again");
                   window.location.href = "/";
                } else {
                    $("#submitotp").text("processing...");
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/varifyeditotp", {otp: otp, random: random},
                            function (data, status) {
                                if (data === "Please login again.") {
                                    window.location.href = "/";
                                } else if (data.indexOf("success") > -1) {
                                    $("#licenceKey").val(data.split("#")[1]);
                                    $("#otpModal_text").modal("hide");
                                    $("#button").hide();
                                    $("#responsesuccess").text("License has been edited successfully.");
                                } else {
                                    $("#submitotp").text("Continue");  
                                    $("#otpError").text(data);
                                }
                            });
                }
            });
            
            $(document).on("click", "#resendotp", function () {
                var random = $("#random").val();
                var radioValue = $("input[name='sendotp']:checked"). val();
                if (random === "") {
                    $("#otpError").text("Login again");
                } else {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/resendotp", {random: random, sendOtpOn: radioValue},
                            function (data, status) {
                                if (data === "Please login again.") {
                                    window.location.href = "/";
                                } else if (data.indexOf("success") > -1) {
                                    $("#random").val(data.split("#")[1]);
                                    $("#otpError").text("Otp Sent");
                                } else {
                                    $("#otpError").text(data);
                                }
                            });
                }
            });
        </script>
        <script src="${pageContext.request.contextPath}/clipboard/clipboard.min.js"></script>
        <script>
            var clipboard = new Clipboard('#copy', {
                text: function () {
                    var licenseKey = $("#licenceKey").val();
                    if (licenseKey === null || licenseKey === "") {
                        $("#responsedisplay").text("No data available to copy.");
                    } else {
                        $("#responsesuccess").text("License Key has been copied successfully.");
                        return licenseKey;
                    }
                }
            });
            clipboard.on('success', function (e) {
                console.log(e);
            });
            clipboard.on('error', function (e) {
                console.log(e);
            });
            
            $("#keydownload").on("click", function(){
                        var license = $("#licenseId").val();
                        var licenseKey = $("#licenceKey").val();
                        if(licenseKey !== ""){
                            if(license !== null || license !== ""){
                                var Uname = "${user.userName}".replace(" ", "");
                                window.location.href = "${pageContext.request.contextPath}/"+Uname+"/downloadlicensekey?licenseId="+license;
                                $("#responsesuccess").text("File Download Successful");
                            }
                        } else{
                            $("#responsedisplay").text("No data available to Download.");
                        }
                    });
        </script>
        <script>
            $(document).ready(function () {
                $('#side-menu').find('li').removeClass('active');
                $(document).find(".license").addClass('active');
                systemID = $("#systemId").val();
                $("#errorenddate").text("");
            });
        </script>
    </body>
</html>