<%-- 
    Document   : getlicenseinfo
    Created on : 20 Aug, 2016, 10:44:00 AM
    Author     : CoreTechies M8
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>FSS | License Info</title>
    </head>
    <body>
        <h1>Get License Info!</h1>
        <form action="#">
            <div>
                <input type="text" id="licenseId"/>
            </div>
            <div>
                <button type="button" id="getlicenseinfo">Get System Info</button>
            </div>
        </form>
        <div>
            <label id="userId"></label>
                <br/>
            <label id="name"></label>
                <br/>
            <label id="organization"></label>
                <br/>
            <label id="validity"></label>
                <br/>
            <label id="licenseLimit"></label>
                <br/>
            <label id="systemId"></label>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.10.2.min.js"></script>
        <script>
            $("#getlicenseinfo").on("click", function () {
                var licenseId = $("#licenseId").val();
                if (licenseId === null || licenseId.trim() === "") {
                    alert("Please enter license id");
                } else {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getlicenseinfo", {licenseId: licenseId},
                            function (data, status) {
                                if (data.indexOf("success") !== -1) {
                                    var jsondata = JSON.parse(data.split("#")[1]);
                                    $("#userId").text(jsondata.userId);
                                    $("#name").text(jsondata.userName);
                                    $("#organization").text(jsondata.organization);
                                    $("#validity").text(jsondata.validity);
                                    $("#licenseLimit").text(jsondata.licenseLimit);
                                    $("#systemId").text(jsondata.systemId);
                                } else {
                                    alert(data);
                                }
                            }
                    );
                }
            });
        </script>
    </body>
</html>
