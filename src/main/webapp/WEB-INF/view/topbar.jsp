
<%@page import="com.coretechies.filesecuritysystem.domain.Notification"%>
<%@page import="com.coretechies.filesecuritysystem.domain.UserInfo"%>
<%@page import="com.coretechies.filesecuritysystem.dao.NotificationDao"%>
<%@page import="com.coretechies.filesecuritysystem.dao.NotificationDaoImpl"%>
<%@page import="java.util.Date"%>
<%@page import="org.ocpsoft.prettytime.PrettyTime"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%!
   
    
      
    public String joinDate(Date date){
        String data="";
        PrettyTime p = new PrettyTime();
        data=  p.format(date);
        return data;
    }


%>
<%--<%!
    @Autowired
    private NotificationDao notificationDao;

%>
<%
    ModelMap modelMap = new ModelMap();
    UserInfo userInfo = (UserInfo) modelMap.get("user");

    List<Notification> notications = notificationDao.getNotificationForClient(userInfo.getUserInfoId());
%>--%>
<div class="page-header-inner "> 
    <c:set var="Uname" value="${user.userName}"/>   
    <!-- BEGIN LOGO -->
    <div class="page-logo"> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/dashboard"> <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/logo.png" alt="logo" class="logo-default" /> </a>
        <div class="menu-toggler sidebar-toggler"> <span></span> </div>
    </div>
    <!-- END LOGO --> 
    <!-- BEGIN RESPONSIVE MENU TOGGLER --> 
    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> <span></span> </a> 
    <!-- END RESPONSIVE MENU TOGGLER -->

    <div class="top-menu"> 
        <!-- BEGIN TOP NAVIGATION MENU -->
        <ul class="nav navbar-nav ">
            <!-- BEGIN USER LOGIN DROPDOWN --> 
            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
            <li class="dropdown dropdown-user"> 
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> 
                    <c:if test="${user.imagePath == null}">
                        <img alt="" class="img-circle" src="${pageContext.request.contextPath}/assets/layouts/layout/img/3.png" />
                    </c:if>
                    <c:if test="${user.imagePath != null}"> 
                        <img alt="" class="img-circle" src="${pageContext.request.contextPath}${user.imagePath}" />
                    </c:if>
                    <!--<img alt="" class="img-circle" src="${pageContext.request.contextPath}/assets/layouts/layout/img/teambg1.jpg" />-->
                    <span class="username username-hide-on-mobile"> ${user.userName} </span> <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-default">
                    <li> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/profile"> <i class="icon-user"></i> My Profile </a> </li>

                    <li class="divider"> </li>
                    <li id="support"> <a href="javascript:;"> <i class="fa-exclamation-circle"></i> Support </a> </li>
                    <li> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/logout"> <i class="icon-key"></i> Log Out </a> </li>
                </ul>
            </li> 
            <!-- END USER LOGIN DROPDOWN -->
        </ul>
    </div>
    <div class="top-menu">
        <ul class="nav navbar-nav pull-right">
            <!-- BEGIN NOTIFICATION DROPDOWN --> 
<!--             DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <i class="icon-bell"></i> <span class="badge badge-default">${notifications.size()}</span> </a>
                <ul class="dropdown-menu">
                    <li><span  style="color: rgb(255, 255, 255); font-weight: bold; margin-right: 25px; font-size: 18px; font-family: Oswald;" id="timer"></span>
                </li>
                    <li class="external">
                        <h3> <span class="bold">${notifications.size()} pending</span> notifications</h3>
                        <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/licensehistory">view all</a> </li>
                    <li>
                        <ul class="dropdown-menu-list scroller" style="height: 300px;" data-handle-color="#637283">
                                <c:forEach items="${notifications}" var="notification">
                                    <c:if test="${notification.notificationType==1}">
                                        <li> <a href="javascript:;"> 
                                                <span class="time"><% 
                                                    Notification notification = (Notification)pageContext.getAttribute("notification"); 
                                                    out.print(joinDate(notification.getCreateOn()));
                                                %></span> 
                                                <span class="details"> <span class="label label-sm label-icon label-success"> <i class="fa fa-credit-card"></i>  </span> ${notification.notificationMessage} </span> </a>
                                        </li>
                                    </c:if>
                                    <c:if test="${notification.notificationType==2 || notification.notificationType==3 || notification.notificationType==4}">
                                        <li> <a href="javascript:;"> 
                                                <span class="time"><% 
                                                    Notification notification1 = (Notification)pageContext.getAttribute("notification"); 
                                                    out.print(joinDate(notification1.getCreateOn()));
                                                %></span> 
                                                <span class="details"> <span class="label label-sm label-icon label-success"> <i class="fa fa-user"></i>  </span> ${notification.notificationMessage} </span> </a>
                                        </li>
                                    </c:if>
                                    <c:if test="${notification.notificationType==5}">
                                        <li> <a href="javascript:;"> 
                                                <span class="time"><% 
                                                    Notification notification2 = (Notification)pageContext.getAttribute("notification"); 
                                                    out.print(joinDate(notification2.getCreateOn()));
                                                %></span> 
                                                <span class="details"> <span class="label label-sm label-icon label-success"> <i class="fa fa-book"></i> </span> ${notification.notificationMessage} </span> </a>
                                        </li>
                                    </c:if>
                                    <c:if test="${notification.notificationType==6}">
                                        <li> <a href="javascript:;"> 
                                                <span class="time"><% 
                                                    Notification notification3 = (Notification)pageContext.getAttribute("notification"); 
                                                    out.print(joinDate(notification3.getCreateOn()));
                                                %></span> 
                                                <span class="details"> <span class="label label-sm label-icon label-success"> <i class="icon-frame"></i> </span> ${notification.notificationMessage} </span> </a>
                                        </li>
                                    </c:if>
                                    <c:if test="${notification.notificationType==7}">
                                        <li> <a href="javascript:;"> 
                                                <span class="time"><% 
                                                    Notification notification4 = (Notification)pageContext.getAttribute("notification"); 
                                                    out.print(joinDate(notification4.getCreateOn()));
                                                %></span> 
                                                <span class="details"> <span class="label label-sm label-icon label-success"> <i class="fa fa-desktop"></i> </span> ${notification.notificationMessage} </span> </a>
                                        </li>
                                    </c:if>
                                    <c:if test="${notification.notificationType==8}">
                                        <li> <a href="javascript:;"> 
                                                <span class="time"><% 
                                                    Notification notification5 = (Notification)pageContext.getAttribute("notification"); 
                                                    out.print(joinDate(notification5.getCreateOn()));
                                                %></span> 
                                                <span class="details"> <span class="label label-sm label-icon label-success"> <i class="fa fa-book"></i> </span> ${notification.notificationMessage} </span> </a>
                                        </li>
                                    </c:if>
                                </c:forEach> 
                        </ul>
                    </li>
                </ul>
            </li>
<!--            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <i class="icon-bell"></i> <span class="badge badge-default">${notifications.size()}</span> </a>
                <ul class="dropdown-menu dropdown-alerts" style="width: 150px;">
                    <li>
                        <p>You have ${notifications.size()} new operations</p>
                    </li>
                    <li>
                        <div class="dropdown-slimscroll">
                            <ul>
                                <c:forEach items="${notifications}" var="notification">
                                    <li><span class="label label-blue"></span>${notification.notificationMessage}
                                        <span class="pull-right text-muted small">
                                            
                                        </span>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                    </li>
                    <li class="last"><a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/licensehistory" class="text-left">See all alerts</a></li>
                </ul>
            </li>-->
            <!-- END NOTIFICATION DROPDOWN --> 
            <!-- BEGIN INBOX DROPDOWN --> 
            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte 
            <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar"> <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <i class="icon-envelope-open"></i> <span class="badge badge-default"> 4 </span> </a>
                <ul class="dropdown-menu">
                    <li class="external">
                        <h3>You have <span class="bold">7 New</span> Messages</h3>
                        <a href="app_inbox.html">view all</a> </li>
                    <li>
                        <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                            <li> <a href="#"> <span class="photo"> <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/avatar2.jpg" class="img-circle" alt=""> </span> <span class="subject"> <span class="from"> Lisa Wong </span> <span class="time">Just Now </span> </span> <span class="message"> Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span> </a> </li>
                            <li> <a href="#"> <span class="photo"> <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/avatar3.jpg" class="img-circle" alt=""> </span> <span class="subject"> <span class="from"> Richard Doe </span> <span class="time">16 mins </span> </span> <span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span> </a> </li>
                            <li> <a href="#"> <span class="photo"> <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/avatar1.jpg" class="img-circle" alt=""> </span> <span class="subject"> <span class="from"> Bob Nilson </span> <span class="time">2 hrs </span> </span> <span class="message"> Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span> </a> </li>
                            <li> <a href="#"> <span class="photo"> <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/avatar2.jpg" class="img-circle" alt=""> </span> <span class="subject"> <span class="from"> Lisa Wong </span> <span class="time">40 mins </span> </span> <span class="message"> Vivamus sed auctor 40% nibh congue nibh... </span> </a> </li>
                            <li> <a href="#"> <span class="photo"> <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/avatar3.jpg" class="img-circle" alt=""> </span> <span class="subject"> <span class="from"> Richard Doe </span> <span class="time">46 mins </span> </span> <span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span> </a> </li>
                        </ul>
                    </li>
                </ul>
            </li>-->
            <!-- END INBOX DROPDOWN -->

        </ul>
    </div>
    <!-- END TOP NAVIGATION MENU --> 
</div>
<script>
            function get_elapsed_time_string(total_seconds) {
                        function pretty_time_string(num) {
                          return ( num < 10 ? "0" : "" ) + num;
                        }

                        var hours = Math.floor(total_seconds / 3600);
                        total_seconds = total_seconds % 3600;

                        var minutes = Math.floor(total_seconds / 60);
                        total_seconds = total_seconds % 60;

                        var seconds = Math.floor(total_seconds);

                        // Pad the minutes and seconds with leading zeros, if required
                        hours = pretty_time_string(hours);
                        minutes = pretty_time_string(minutes);
                        seconds = pretty_time_string(seconds);

                        // Compose the string for display
                        var currentTimeString = hours + ":" + minutes + ":" + seconds;

                        return currentTimeString;
                      }
                      var d = ${user.logoutTime};
                      d = parseInt(d);
                      d = ((d*60)+1);
                      var elapsed_seconds = d;
                      setInterval(function() {
                          if(elapsed_seconds>0){
                            elapsed_seconds = elapsed_seconds - 1;
                            document.getElementById("timer").innerHTML = get_elapsed_time_string(elapsed_seconds);
                          }else{
                              var Uname = "${user.userName}".replace(" ", "");
                              window.location.href = "${pageContext.request.contextPath}/"+Uname+"/logout";
                          }
                      }, 1000);
                    document.addEventListener("mousemove", function(event) {
                           elapsed_seconds=d;
                    });
</script>
