<!DOCTYPE html>

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<head>
<meta charset="utf-8" />
<title>FSS | Profile</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<style>
.user-alphabate {
	display: block;
	position: relative;
	-webkit-transition: all .4s linear;
	transition: all .4s linear;
	width: 100%;
	height: 150px;
	line-height: 150px;
	background: #36c6d3;
	font-size: 30px;
	color: #fff;
	text-shadow: 0px 1px 2px rgba(0,0,0,0.2);
}
.big-text {
	font-size: 45px;
}
.mt-element-overlay .mt-overlay-1:hover .user-alphabate {
	-ms-transform: scale(1.2) translateZ(0);
	-webkit-transform: scale(1.2) translateZ(0);
	transform: scale(1.2) translateZ(0);
}
</style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top"> 
 <!-- BEGIN HEADER INNER -->
 <jsp:include page="topbar.jsp" />
 <!-- END HEADER INNER --> 
</div>
<!-- END HEADER --> 
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER --> 
<!-- BEGIN CONTAINER -->
<div class="page-container"> 
 <!-- BEGIN SIDEBAR -->
 <div class="page-sidebar-wrapper"> 
  <!-- BEGIN SIDEBAR --> 
  <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
  <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
  <div class="page-sidebar navbar-collapse collapse"> 
   <!-- BEGIN SIDEBAR MENU -->
   <jsp:include page="sidebar.jsp" />
   <!--   <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
     DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
    <li class="sidebar-toggler-wrapper hide"> 
      BEGIN SIDEBAR TOGGLER BUTTON 
     <div class="sidebar-toggler"> <span></span> </div>
      END SIDEBAR TOGGLER BUTTON  
    </li>
    <li class="nav-item start "> <a href="index.html" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
     
    </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">License</span>  </a>
     
    </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
     
    </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
   </ul>--> 
   <!-- END SIDEBAR MENU --> 
   <!-- END SIDEBAR MENU --> 
  </div>
  <!-- END SIDEBAR --> 
 </div>
 <!-- END SIDEBAR --> 
 <!-- BEGIN CONTENT -->
 <div class="page-content-wrapper"> 
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content"> 
   <!-- BEGIN PAGE HEADER--> 
   
   <!-- BEGIN PAGE TITLE-->
   <h3 class="page-title"> Client Profile 
    <!-- <small>blank page layout</small>--> 
   </h3>
   <!-- END PAGE TITLE--> 
   <!-- END PAGE HEADER-->
   
   <div class="row">
    <div class="col-md-12"> 
     <!-- BEGIN PROFILE SIDEBAR -->
     <div class="profile-sidebar mt-element-card mt-card-round mt-element-overlay"> 
      <!-- PORTLET MAIN -->
      <div class="portlet light profile-sidebar-portlet bordered box-shadow"> 
       <!-- SIDEBAR USERPIC -->
       <div class="mt-card-item mt-widget-1 clearfix"  style=" padding:00px 70px 00px; border:none;">
        <div class="mt-card-avatar mt-overlay-1 " style="margin-bottom:0px;">
        <c:if test="${user.imagePath == null}"> 
            <span><img alt="" class="img-circle profile-pic" src="${pageContext.request.contextPath}/assets/layouts/layout/img/3.png" /></span> 
        </c:if>
        <c:if test="${user.imagePath != null}"> 
            <span><img alt="" class="img-circle profile-pic" src="${pageContext.request.contextPath}${user.imagePath}" /></span> 
        </c:if>
         <!--<span class="user-alphabate big-text">${user.displayName}</span>--> 
        </div>
       </div>
       <!-- END SIDEBAR USERPIC --> 
       <!-- SIDEBAR USER TITLE -->
       <div class="profile-usertitle">
        <div class="profile-usertitle-name">${user.userName}</div>
        <div class="profile-usertitle-job" style="text-transform:none;"> <a href="javascript:;">${user.emailId}</a></div>
        <div class="profile-usertitle-job" style="text-transform:none;">+${user.countryCode.mobileCode}-${user.mobileNumber}</div>
        <div class="profile-userbuttons" style="margin-bottom:25px;"> 
         <!--                                    <button type="button" class="btn btn-circle green btn-sm sendMail">E-Mail</button>
                                        <button type="button" class="btn btn-circle red btn-sm">Message</button>--> 
        </div>
       </div>
       
       <!--<div class="btn-group btn-group btn-group-justified" role="group" aria-label="..." style="margin-top:25px;">
										  <div class="btn-group" role="group">
											<button type="button" class="btn  btn-success"><i class="icon-speech"></i> Message</button>
										  </div>
										 
										  <div class="btn-group" role="group">
											<button type="button" class="btn btn-success"><i class="icon-envelope-letter"></i> Mail</button>
										  </div>
										</div>--> 
       
       <!-- END SIDEBAR USER TITLE --> 
       <!-- SIDEBAR BUTTONS --> 
       <!--<div class="profile-userbuttons">
                                        <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                                        <button type="button" class="btn btn-circle red btn-sm">Message</button>
                                    </div>--> 
       <!-- END SIDEBAR BUTTONS --> 
       <!-- SIDEBAR MENU --> 
       
       <!-- END MENU --> 
      </div>
      <div class="portlet light bordered box-shadow"> 
       <!-- STAT -->
       <div class="row list-separated ">
        <div class="col-md-4 col-sm-4 col-xs-6">
         <div class="uppercase profile-stat-title">${totalCourse}</div>
         <div class="uppercase profile-stat-text"> Course </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6">
         <div class="uppercase profile-stat-title">${totalLicense} </div>
         <div class="uppercase profile-stat-text"> License </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6">
         <div class="uppercase profile-stat-title">${totalSystem}</div>
         <div class="uppercase profile-stat-text"> System </div>
        </div>
       </div>
      </div>
      
      <!-- END PORTLET MAIN --> 
      <!-- PORTLET MAIN --> 
      
      <!-- END PORTLET MAIN --> 
     </div>
     <!-- END BEGIN PROFILE SIDEBAR --> 
     <!-- BEGIN PROFILE CONTENT -->
     <div class="profile-content">
      <div class="row">
       <div class="col-md-12">
        <div class="portlet light bordered  box-shadow">
         <div class="portlet-title tabbable-line">
          <div class="caption caption-md"> <i class="icon-globe theme-font hide"></i> <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span> </div>
          <ul class="nav nav-tabs">
           <li class="active"> <a href="#tab_1_1" data-toggle="tab">Personal Info</a> </li>
           <!--                                                    <li>
                                                        <a href="#tab_1_2" data-toggle="tab">Courses</a>
                                                    </li>-->
           <li> <a href="#tab_1_3" data-toggle="tab">System</a> </li>
           <li> <a href="#tab_1_4" data-toggle="tab">License</a> </li>
          </ul>
         </div>
         <div class="portlet-body">
          <div class="tab-content"> 
           <!-- PERSONAL INFO TAB -->
           <div class="tab-pane active" id="tab_1_1">
            <form action="#" class="form-horizontal">
             <div class="form-body"> 
              <!--                                                        <div>
                                                                <div class="form-group clearfix">
                                                                    <label class="col-sm-3 control-label" style="font-weight: bold; font-size: large">SA${USERID}</label>
                                                                    <label class="col-sm-8 control-label"></label>
                                                                </div>
                                                            </div>-->
              <div class="form-group">
               <label class="col-sm-4 control-label">User ID</label>
               <div class="col-sm-6">
                <div class="input-group"> <span class="input-group-addon input-circle-left"> <i class="fa fa-user"></i> </span>
                 <input type="text" readonly class="form-control input-circle-right" value="SA${USERID}">
                </div>
                <!--<span class="help-block"> A block of help text. </span>--> 
               </div>
              </div>
              <div class="form-group">
               <label class="col-sm-4 control-label">Organization</label>
               <div class="col-sm-6">
                <div class="input-group"> <span class="input-group-addon input-circle-left"> <i class="fa fa-globe"></i> </span>
                 <input type="text" readonly class="form-control input-circle-right" value="${user.organization}">
                </div>
                <!--<span class="help-block"> A block of help text. </span>--> 
               </div>
              </div>
              <div class="form-group">
               <label class="col-sm-4 control-label">Email Address</label>
               <div class="col-sm-6">
                <div class="input-group"> <span class="input-group-addon input-circle-left"> <i class="fa fa-envelope"></i> </span>
                 <input type="text" readonly class="form-control input-circle-right" value="${user.emailId}">
                </div>
                <!--<span class="help-block"> A block of help text. </span>--> 
               </div>
              </div>
              <div class="form-group">
               <label class="col-sm-4 control-label">Mobile No.</label>
               <div class="col-sm-6">
                <div class="input-group"> <span class="input-group-addon input-circle-left"> <i class="fa fa-phone"></i> </span>
                   <input type="text" readonly class="form-control input-circle-right" value="+${user.countryCode.mobileCode}-${user.mobileNumber}">
                </div>
                <!--<span class="help-block"> A block of help text. </span>--> 
               </div>
              </div>
              <div class="form-group">
               <label class="col-sm-4 control-label">Country</label>
               <div class="col-sm-6">
                <div class="input-group"> <span class="input-group-addon input-circle-left"> <i class="fa fa-flag"></i> </span>
                   <input type="text" readonly class="form-control input-circle-right" value="${user.countryCode.country}">
                </div>
                <!--<span class="help-block"> A block of help text. </span>--> 
               </div>
              </div>
             </div>
             <div class="form-actions">
              <div class="row">
               <div class="col-md-offset-4 col-md-6 text-right"> </div>
              </div>
             </div>
            </form>
           </div>
           <!-- END PERSONAL INFO TAB --> 
           <!-- DISPLAY SYSTEM TAB -->
           <div class="tab-pane" id="tab_1_3">
            <form action="#">
             <table class="table table-light detail-table">
              <tbody>
               <tr>
                <td style="font-weight: bold;">System Id</td>
                <td class="clearfix"><span class="text-ellips" id="systemId">${systemid}</span><a href="javascript:;" class="btn btn-sm btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Copy System Id" id="copysystemid"><i class="fa fa-copy"></i></a></td>
               </tr>
               <!--                                                                    <tr>
                                                                	<td style="font-weight: bold;">MAC Id</td>
                                                                        <td>${systemmacid}</td>
                                                                    </tr>  
                                                                    <tr>
                                                                	<td style="font-weight: bold;">Serial Number</td>
                                                                        <td>${systemserialnumber}</td>
                                                                    </tr>  -->
              </tbody>
             </table>
             <span id="copyresponse" style="color: green; display: block; font-size: 14px;"></span>
            </form>
           </div>
           <!-- END CHANGE PASSWORD TAB --> 
           <!-- PRIVACY SETTINGS TAB -->
           <div class="tab-pane" id="tab_1_4">
            <form action="#">
             <table class="table table-light detail-table">
              <tbody>
               <tr>
                <td style="font-weight: bold;">Organization</td>
                <td>${adminlicense.userInfo.organization}</td>
               </tr>
               <tr>
                <td style="font-weight: bold;">Validity</td>
                <c:if test="${adminlicense.expiryDate == null}">
                    <td>Life Time</td>
                </c:if> 
                <c:if test="${adminlicense.expiryDate != null}">
                    <td>
                        <fmt:formatDate value="${adminlicense.issueDate}" pattern="dd-MM-yyyy" />
                        to
                        <fmt:formatDate value="${adminlicense.expiryDate}" pattern="dd-MM-yyyy" />
                    </td>
                </c:if>
               </tr>
               <tr>
                <td style="font-weight: bold;">Desktop License Limit</td>
                <c:if test="${adminlicense.desktopLicenseLimit == -1}">
                    <td>Unlimited</td>
                </c:if>
                <c:if test="${adminlicense.desktopLicenseLimit != -1}">
                    <td>${adminlicense.desktopLicenseLimit}</td>
                </c:if>
               </tr>
               <tr>
                <td style="font-weight: bold;">Web License Limit</td>
                <c:if test="${adminlicense.weblicenseLimit == -1}">
                    <td>Unlimited</td>
                </c:if>
                <c:if test="${adminlicense.weblicenseLimit != -1}">
                    <td>${adminlicense.weblicenseLimit}</td>
                </c:if>
               </tr>
               <tr>
                <td style="font-weight: bold;">License Used </td>
                <td>${totalLicense}</td>
               </tr>
               <tr>
                <td style="font-weight: bold;">Remaining License </td>
                <c:set var="remaining" scope="page" value="${adminlicense.weblicenseLimit - totalLicense}" />
                <c:if test="${adminlicense.weblicenseLimit == -1}">
                    <td>Unlimited</td>
                </c:if>
                <c:if test="${adminlicense.weblicenseLimit != -1}">
                    <td>${remaining}</td>
                </c:if>
               </tr>
               <!--                                                                <tr>
                                                                        <td style="font-weight: bold;">Web License Key </td>
                                                                        <td class="clearfix">
                                                                            <span class="text-ellips" id="weblicensekey">${adminlicense.license}</span><a href="javascript:;" class="pull-right btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Copy Web License Key" id="weblicensecopy"><i class="fa fa-copy"></i></a>
                                                                        </td>
                                                                </tr>-->
               <tr>
                <td style="font-weight: bold;">Desktop License Key </td>
                <td class="clearfix"><span class="text-ellips" id="desktoplicensekey">${adminlicense.desktopLicense}</span><a href="javascript:;" class="pull-right btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Copy Desktop License Key" id="desktoplicensecopy"><i class="fa fa-copy"></i></a></td>
               </tr>
              </tbody>
             </table>
             <!--end profile-settings-->
             <div class="margin-top-10"> <span id="copylicenseresponse" style="color: green; display: block; font-size: 14px;"></span> 
              <!--<a href="javascript:;" class="btn default"> Cancel </a>--> 
             </div>
            </form>
           </div>
           <!-- END PRIVACY SETTINGS TAB --> 
          </div>
         </div>
        </div>
       </div>
      </div>
     </div>
     <!-- END PROFILE CONTENT --> 
    </div>
   </div>
  </div>
  <!-- END CONTENT BODY --> 
 </div>
 <!-- END CONTENT --> 
 <!-- BEGIN QUICK SIDEBAR --> 
 <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 
 
 <!-- END QUICK SIDEBAR --> 
</div>
<!-- END CONTAINER --> 
<!-- BEGIN FOOTER -->
<div class="page-footer">
 <div class="page-footer-inner"> 2016 &copy; Organization </div>
 <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
</div>
<!-- END FOOTER --> 
<!-- Modal -->
<div class="modal fade" id="renew_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog" role="document">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Renew License</h4>
   </div>
   <form>
    <div class="modal-body">
     <div class="form-group clearfix">
      <label>User Name </label>
      <input class="form-control" readonly value="XYZ">
     </div>
     <div class="form-group clearfix">
      <label>Organization Name </label>
      <input class="form-control" readonly value="ABC">
     </div>
     <div class="form-group clearfix">
      <select class="form-control">
       <option selected="" disabled="">Select Validity</option>
       <option>Option 1</option>
       <option>Option 2</option>
       <option>Option 3</option>
      </select>
     </div>
     <div class="form-group clearfix">
      <div class="one-half-50">
       <div class="one">
        <input class="form-control" placeholder=" Issue Date">
       </div>
       <div class="one">
        <input class="form-control" placeholder=" Expiry Date">
       </div>
      </div>
     </div>
     <div class="form-group clearfix">
      <div class="one-half">
       <div class="two" style="float:left">
        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="    margin-top: 8px;">
         <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes">
         <span></span> </label>
       </div>
       <div class="one" style="float:right">
        <input class="form-control" placeholder=" License Limit">
        <div class="hint text-right">(please click checkbox for unlimited license)</div>
       </div>
      </div>
     </div>
     <div class="form-group clearfix">
      <div class="mt-radio-inline" style=" padding:0px;">
       <label style=" margin-right:15px;">Carry Forward :</label>
       <label class="mt-radio mt-radio-outline">
        <input type="radio" name="carry" id="radio1" value="yes" >
        Yes <span></span> </label>
       <label class="mt-radio mt-radio-outline">
        <input type="radio" name="carry" id="radio2" value="no">
        No <span></span> </label>
      </div>
     </div>
     <div class="form-group clearfix">
      <label style=" margin-right:15px;"> System ID:</label>
      <input readonly class="form-control" value="4f5454d64dsf5dsf4d5s4fdsf3ds4f54ds5f4s">
     </div>
     <div class="form-group clearfix">
      <div class="two-half">
       <div class="one">
        <input class="form-control" placeholder=" License Key">
       </div>
       <div class="two"> <a href="javascript:;" class="btn  btn-default"><i class="fa fa-copy"></i></a> <a href="javascript:;" class="btn  btn-default"><i class="fa fa-download"></i></a> </div>
      </div>
     </div>
    </div>
    <div class="modal-footer">
     <button type="reset" class="btn btn-default" >Reset</button>
     <!--data-dismiss="modal"-->
     <button type="button" class="btn green">Save</button>
    </div>
   </form>
  </div>
 </div>
</div>
<!----> 
<!-- BEGIN CORE PLUGINS --> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
<!-- END CORE PLUGINS --> 
<!-- BEGIN THEME GLOBAL SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
<!-- END THEME GLOBAL SCRIPTS --> 
<!-- BEGIN THEME LAYOUT SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/clipboard/clipboard.min.js"></script> 
<!-- END THEME LAYOUT SCRIPTS --> 
<script>
        $( ".form-control" ).focus(function() {
            $(this).prev('.input-group-addon').removeClass().addClass('input-group-addon-focus');
            $(this).next('.input-group-addon').removeClass().addClass('input-group-addon-focus');
        });

        $( ".form-control" ).focusout(function() {
            $(this).prev('.input-group-addon-focus').removeClass().addClass('input-group-addon');
            $(this).next('.input-group-addon-focus').removeClass().addClass('input-group-addon');
        });
        </script>
<script>
                    var clipboard = new Clipboard('#copysystemid', {
                        text: function () {
                            $("#copyresponse").text("System Id copied");
                            $("#copylicenseresponse").text("");
                            return $("#systemId").text();
                        }
                    });
                    clipboard.on('success', function (e) {
                        console.log(e);
                    });
                    clipboard.on('error', function (e) {
                        console.log(e);
                    });
                    
                    var clipboard = new Clipboard('#weblicensecopy', {
                        text: function () {
                            var key = $("#weblicensekey").text();
                            if(key===""){
                                alert("no license copied");
                            }else{
                                $("#copylicenseresponse").text("Web License Key copied");
                                return key;
                            }
                        }
                    });
                    clipboard.on('success', function (e) {
                        console.log(e);
                    });
                    clipboard.on('error', function (e) {
                        console.log(e);
                    });
            
                    var clipboard = new Clipboard('#desktoplicensecopy', {
                        text: function () {
                            var key = $("#desktoplicensekey").text();
                            if(key===""){
                                alert("no license copied");
                            }else{
                                $("#copylicenseresponse").text("Desktop License Key copied");
                                $("#copyresponse").text("");
                                return key;
                            }
                        }
                    });
                    clipboard.on('success', function (e) {
                        console.log(e);
                    });
                    clipboard.on('error', function (e) {
                        console.log(e);
                    });
            
                </script> 
<script>
            $(document).ready(function () {
                $('#side-menu').find('li').removeClass('active');
            });
        </script>
</body>
</html>
