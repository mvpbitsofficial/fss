/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.Notification;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("NotificationDao")
public class NotificationDaoImpl implements NotificationDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = false)
    public Notification saveNotification(Notification notification) {
        try {
            entityManager.persist(notification);
            return notification;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Notification> getNotificationForClient(Integer userId) {
        try {
            TypedQuery<Notification> createQuery = entityManager.createQuery("SELECT N FROM Notification AS N WHERE N.createdBy=:userId AND N.seenby NOT LIKE :seen ORDER BY N.notificationId DESC", Notification.class);
            createQuery.setParameter("userId", userId);
            createQuery.setParameter("seen", "%," + userId + ",%");
            return createQuery.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Notification> getNotification(Integer userId, Date fromDate, Date toDate) {
        try {
            TypedQuery<Notification> createQuery = entityManager.createQuery("SELECT N FROM Notification AS N WHERE N.createdBy=:userId AND N.createOn>=:startDate AND N.createOn<=:endDate", Notification.class);
            createQuery.setParameter("userId", userId);
            createQuery.setParameter("startDate", fromDate);
            createQuery.setParameter("endDate", toDate);
            return createQuery.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Notification> getNotificationForAdmin() {
        try {
            TypedQuery<Notification> createQuery = entityManager.createQuery("SELECT N FROM Notification AS N ORDER BY N.notificationId DESC", Notification.class);
            return createQuery.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<Notification> getNotificationByType(Integer userId, Integer type){
        try{
            TypedQuery<Notification> createQuery = entityManager.createQuery("SELECT N FROM Notification AS N WHERE N.createdBy=:userId AND N.notificationType=:type AND N.seenby NOT LIKE :seen ORDER BY N.notificationId DESC", Notification.class);
            createQuery.setParameter("userId", userId);
            createQuery.setParameter("seen", "%," + userId + ",%");
            createQuery.setParameter("type", type);
            return createQuery.getResultList();
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<Notification> getNotificationsForAdminByType(Integer type){
        try{
            TypedQuery<Notification> createQuery = entityManager.createQuery("SELECT N FROM Notification AS N WHERE N.notificationType=:type ORDER BY N.notificationId DESC", Notification.class);
            createQuery.setParameter("type", type);
            return createQuery.getResultList();
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public Map<String,Integer> getCourseNotificationHistory(Integer createdBy){
        try{
            TypedQuery<Notification> createQuery = entityManager.createQuery("SELECT N FROM Notification AS N WHERE N.createdBy=:createdBy AND N.notificationType=:type", Notification.class);
            createQuery.setParameter("createdBy", createdBy);
            createQuery.setParameter("type", 5);
            List<Notification> resultList = createQuery.getResultList();
            Map<String,Integer> map = new HashMap<>();
            Integer count = 0;
            for (Notification notification : resultList) {
                String date = new SimpleDateFormat("dd-MM-yyyy").format(notification.getCreateOn());
                if(map.containsKey(date)){
                    count++;
                    map.put(date, count);
                }else{
                    count=1;
                    map.put(date, count);
                }
//                if(map.isEmpty()){
//                    
//                    map.put(notification.getCreateOn().toString(), count);
//                }else{
//                    for(Map.Entry<String,Integer> entry : map.entrySet()){
//                        
//                    }
//                }
            }
            return map;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
