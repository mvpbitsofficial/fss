/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.SecurityQuestionAnswer;
import java.util.List;

/**
 *
 * @author CoreTechies M8
 */
public interface SecurityQuestionAnswerDao {

    public SecurityQuestionAnswer saveSecurityQuestionAnswers(SecurityQuestionAnswer securityQuestionAnswer);

    public List<SecurityQuestionAnswer> getSecurityQuestionAnswers(Integer userId);

    public Boolean getSecurityQuestionAnswers(Integer userId, Integer questionId, String answer);
    
    public Boolean editAnswer(SecurityQuestionAnswer sqa);
    
    public SecurityQuestionAnswer getAnswerByQuestionId(Integer questionId, Integer userId);

    public List<SecurityQuestionAnswer> getSecurityQuestionAnswersByQuestionId(Integer questionId);
}
