/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.service;

import com.coretechies.filesecuritysystem.domain.SystemInfo;
import com.coretechies.filesecuritysystem.security.InvalidInformationException;
import java.util.List;

/**
 *
 * @author CoreTechies M8
 */
public interface SystemInfoService {

    public SystemInfo saveSystemInfo(String systemName, String systemId, Integer userId, Integer cretedBy) throws InvalidInformationException;

    public List<SystemInfo> getSystemInfoByCreator(Integer createdBy);

    public boolean deleteSystemId(Integer systemInfoId);

    public SystemInfo getSystemInfo(Integer userInfoId, Integer systemInfoId);

    public SystemInfo getSystemInfoBySystemName(Integer userId, String systemName);

    public SystemInfo getSystemInfoBySystemId(Integer userId, String systemId);

    public String getSystemInfoByUser(Integer userId);

    public List<SystemInfo> getSystemInfoListByUser(Integer userId);

    public Integer getSystemInfoCountByCreator(Integer createdBy);
    
    public Integer getSystemInfoCount();
    
    public Boolean editSystemName(SystemInfo systemInfo);
}
