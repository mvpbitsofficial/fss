function createLineChart(divId, jsonData){
    try{
        var obj = JSON.parse(jsonData);
        
        var categories = [];
        for(var a in obj.category){
            categories.push(obj.category[a]);
        }
        
        chart = new Highcharts.Chart({
             'exporting' : false,
            chart: {
                zoomType: 'xy',
                renderTo: divId,
                type:"line"
            },
            title: {
//                text: ""+obj.title+""
                text: ""+obj.title+""
            },
            xAxis: [{
                categories: categories,
                
            }],
            yAxis: {
                title: {
                    text:"Values",
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                }
            },
            
            tooltip: {
               headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{point.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                line: {
                    pointPadding: 0.2,
                    borderWidth: 0
                } ,
                series: {
                        color: 'red'
                    }
            },
            series:  obj.series
        });
        
        return chart;
    } catch (e) {
        alert(e);
    }
}
