/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.security;

import com.coretechies.filesecuritysystem.domain.AdminLicense;
import com.coretechies.filesecuritysystem.domain.Course;
import com.coretechies.filesecuritysystem.domain.SystemInfo;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.domain.UserLicense;
import com.coretechies.filesecuritysystem.domain.Watermark;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CoreTechies M8
 */
public final class UserLicenseInformation {
 
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private final AES aes = new AES();

    private String userId;
    private String userName;
    private Course course;
    private Watermark watermark;

    private String systemId;
    private String issueDate;
    private String expiryDate;
    private String securityKey;
//    private String identity = "FOLC";
//    private String validity;
    private UserLicense userLicense;
    private String licenseInfo;
    private String validity;

    public UserLicenseInformation(UserInfo userInfo, Course course, SystemInfo systemInfo, Watermark watermark, Integer validity, Date startDate, Date endDate, String securityKey, Integer createdBy, AdminLicense adminLicense, Integer createdLicense, UserLicense ul) throws InvalidInformationException {
        if (course == null) {
            throw new InvalidInformationException("Please select a course.");
        }
        if (systemInfo == null) {
            throw new InvalidInformationException("Please select a system.");
        }
        if (watermark == null) {
            throw new InvalidInformationException("Please select watermark.");
        }
        /*
/////////////Check Self License
        LoginService ls = new LoginServiceImpl();
        LicenceInfoService service = new LicenceInfoServiceImpl();
        List<LicenceInfo> licenceInfos = service.getALLLicenceInfo();
        if (licenceInfos == null || licenceInfos.isEmpty()) {
            throw new InvalidInformationException("No Licence is available.");
        }
/////////////Check Self License Over

/////////////Check Self License validation
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        decrypt = aes.decrypt(licenceInfos.get(0).getLicence(), "0123456789abcdef");
        String[] split = decrypt.split("#");
        if (split.length != 7) {
            throw new InvalidInformationException("Licence Error");
        }
/////////////Check Self License validation Over
        LicenceInfoService serviceUpdate = new LicenceInfoServiceImpl();
        LicenceInfo licenceInfo = serviceUpdate.getLicenceInfo(1);
        if (licenceInfo.getLicenceLimit().equals(licenceInfo.getUsed())) {
            throw new InvalidInformationException("Licence limit over.");
        }
        try {
            if (new Date().compareTo(format.parse(split[3])) > 0) {
                throw new InvalidInformationException("Licence expired.");
            }
        } catch (Exception e) {
        }
        if (new Date().compareTo(format.parse(split[2])) < 0) {
            throw new InvalidInformationException("Licence not started yet.");
        }
         */

        if (adminLicense == null) {
            throw new InvalidInformationException("No Licence is available.");
        }

//        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
//        String decrypt;
//        try {
//            decrypt = aes.decrypt(adminLicense.getLicense(), "0123456789abcdef");
//            String[] split = decrypt.split("#");
//            if (split.length != 7) {
//                throw new InvalidInformationException("Licence Error");
//            }
//        } catch (Exception ex) {
//            throw new InvalidInformationException("Licence Error");
//        }
        Integer weblimit = adminLicense.getWeblicenseLimit();
        if (weblimit.equals(createdLicense)) {
            throw new InvalidInformationException("Licence limit over.");
        }
        try {
            if (new Date().compareTo(adminLicense.getExpiryDate()) > 0) {
                throw new InvalidInformationException("Licence expired.");
            }
        } catch (Exception e) {
        }
        if (new Date().compareTo(adminLicense.getIssueDate()) < 0) {
            throw new InvalidInformationException("Licence not started yet.");
        }

        userLicense = ul;
        userLicense.setCourseId(course.getId());
        userLicense.setCreateOn(new Date());
        userLicense.setIssueDate(startDate);
        userLicense.setSystemInfoId(systemInfo.getSystemInfoId());
        userLicense.setUpdateOn(new Date());
        userLicense.setUserId(userInfo.getUserInfoId());
        userLicense.setValidity(validity);
        userLicense.setCreatedBy(createdBy);
        userLicense.setWatermarkId(watermark.getWatermarkId());

        if(validity != null){
            if (validity == 20) {
                userLicense.setExpiryDate(null);
            } else {
                userLicense.setExpiryDate(endDate);
            }
        } else{
            userLicense.setExpiryDate(endDate);
        }
        this.userName = userInfo.getUserName();
        this.course = course;
        this.watermark = watermark;
        this.issueDate = simpleDateFormat.format(startDate);
        this.expiryDate = userLicense.getExpiryDate() == null ? null : simpleDateFormat.format(userLicense.getExpiryDate());
        this.securityKey = securityKey;

        setUserId(userInfo.getUserInfoId());
        setSystemId(systemInfo);
        userLicense.setLicense(generateLicense());
    }

    public UserLicenseInformation(String license) throws InvalidInformationException {
        if (license.isEmpty()) {
            throw new InvalidInformationException("Please enter License.");
        } else {
            try {
                String decrypt = aes.decrypt(license, "0123456789abcdef");
                String[] split = decrypt.split("#");
                String userInfo = split[6].trim();

                this.userName = userInfo.substring(0, userInfo.length() - 10);
                this.userId = userInfo.substring(userName.length(), userInfo.length() - 4);

                System.out.println("userName = "+userName);
                System.out.println("userId = "+userId);
                System.out.println(userInfo);
                if (split[6].trim().contains("FCLU")) {
                    String app = split[0].trim();
                    String[] appSplit = app.split(":");
                    String courseId = appSplit[0].trim();
                    String courseName = appSplit[1].trim();
                    System.out.println("courseId = "+courseId);
                    systemId = split[1].trim();
                    if (split[3].trim().equals("null") || split[3].trim().equals("")) {
                        this.validity = "Lifetime";
                    } else {
                        this.validity = split[2].trim() + " to " + split[3].trim();
                    }

                    this.licenseInfo = "success#{\"userId\":\"" + userId.trim() + "\",\"userName\":\"" + userName.trim() + "\",\"courseId\":\"" + courseId.trim() + "\","
                            + "\"courseName\":\"" + courseName.trim() + "\",\"validity\":\"" + validity.trim() + "\",\"systemId\":\"" + systemId.trim() + "\"}";
                } else {
                    throw new InvalidInformationException("Invalid License.");
                }
            } catch (Exception ex) {
                throw new InvalidInformationException("Invalid License.");
            }
        }
    }

    public String getLicenseInfo() {
        return licenseInfo;
    }

    public UserLicense getUserLicense() {
        return userLicense;
    }

    private String generateLicense() {
        String rawText = course.getCourseId() + ":" + course.getCourseName()
                + "#" + systemId
                + "#" + issueDate
                + "#" + expiryDate
                + "#" + watermark.getWatermarkContent()+"::"+watermark.getProperty() + "::" + watermark.getTextPosition()
                + "#" + securityKey
                + "#" + userName + userId + "FCLU";
        return aes.generate(rawText);
    }

    private void setUserId(Integer userid) {
//        LicenseController licenseController = new LicenseController();
//        Integer daoObject = licenseController.getDaoObject(userid);
//        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
//            if(userid == entry.getKey()){
//                this.userId = "AD"+(1000+entry.getValue());
//                System.out.println("UserId="+userId);
//            }
//        }
        userid = 1000 + userid;
        this.userId = "AD" + userid;
    }

    public void setSystemId(SystemInfo systemInfo) throws InvalidInformationException {
        try {
            this.systemId = systemInfo.getSystemId();
            String decrypt = aes.decrypt(systemId, "0123456789abcdef");
            String[] code = decrypt.split("#");
            if (!code[1].trim().equals("FUSC")) {
                throw new InvalidInformationException("Invalid System Id.");
            }
        } catch (Exception ex) {
            Logger.getLogger(UserLicenseInformation.class.getName()).log(Level.SEVERE, null, ex);
            throw new InvalidInformationException("Invalid System Id.");
        }
    }
}
/*
    public UserLicenseInformation(String license) throws InvalidInformationException {
        if (license.isEmpty()) {
            throw new InvalidInformationException("Please enter Licence.");
        } else {
            try {
                String decrypt = aes.decrypt(license, "0123456789abcdef");
                String[] split = decrypt.split("#");
                if (split[6].trim().equals(identity)) {
                    String user = split[0].trim();
                    String[] userSplit = user.split(":");
                    this.userId = userSplit[0].trim();
                    this.userName = userSplit[1].trim();
                    this.organization = userSplit[2].trim();
                    this.systemId = split[1].trim();
                    if (split[3].trim().contains("null")) {
                        //lbDETLICEnd.setText("LifeTime");
                        this.validity = "LifeTime";
                    } else {
                        Calendar startCalendar = Calendar.getInstance();
                        startCalendar.setTime(simpleDateFormat.parse(split[2].trim()));
                        Calendar endCalendar = Calendar.getInstance();
                        startCalendar.setTime(simpleDateFormat.parse(split[3].trim()));
                        if (startCalendar.get(Calendar.DATE) == endCalendar.get(Calendar.DATE) && startCalendar.get(Calendar.MONTH) == endCalendar.get(Calendar.MONTH)) {
                            this.validity = String.valueOf(endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR));
                        } else {
                            this.validity = "No validity";
                        }
                        this.validity = split[2].trim() + " to " + split[3].trim();
                    }
                    if (split[4].trim().substring(2).equals("-1")) {
                        this.licenseLimit = "Unlimited";
                    } else {
                        this.licenseLimit = split[4].trim().substring(2);
                    }
                } else {
                    throw new InvalidInformationException("Invalid Licence");
                }
                this.licenseInfo = "success#{\"userId\":\"" + userId + "\",\"userName\":\"" + userName + "\",\"organization\":\"" + organization + "\","
                        + "\"validity\":\"" + validity + "\",\"licenseLimit\":\"" + licenseLimit + "\",\"systemId\":\"" + systemId + "\"}";
            } catch (Exception ex) {
                Logger.getLogger(UserLicenseInformation.class.getName()).log(Level.SEVERE, null, ex);
                throw new InvalidInformationException("Invalid Licence");
            }
        }
    }

 */
