/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.Watermark;
import java.util.List;

/**
 *
 * @author CoreTechies M8
 */
public interface WatermarkDao {

    public Watermark saveWatermark(Watermark watermark);

    public Watermark updateWatermark(Watermark watermark);

    public List<Watermark> getWatermarkList(Integer userId);

    public Watermark getWatermarkByName(Integer userInfoId, String watermarkName);

    public Watermark getWatermarkByMessage(Integer userInfoId, String watermarkMessage);

    public Watermark getWatermark(Integer userInfoId, Integer watermarkId);

    public Boolean deletewatermark(Integer watermarkId);

    public Watermark getWatermarkByMessage(Integer userInfoId, String watermarkMessage, Integer watermarkId);

    public Watermark getWatermarkByName(Integer userInfoId, String watermarkName, Integer watermarkId);

    public Watermark getDefaultWatermark(Integer userInfoId);
    
    public Watermark getWatermarkForPreview(Integer watermarkId);
}
