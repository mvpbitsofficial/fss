
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>FSS | License</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
 <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<style>
    .input-daterange .input-group-addon{
        padding: 6px 12px;
        width: 1%;
    }
</style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top"> 
 <!-- BEGIN HEADER INNER -->
 <jsp:include page="topbar.jsp" />
 <!-- END HEADER INNER --> 
</div>
<!-- END HEADER --> 
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER --> 
<!-- BEGIN CONTAINER -->
<div class="page-container"> 
 <!-- BEGIN SIDEBAR -->
 <div class="page-sidebar-wrapper"> 
  <!-- BEGIN SIDEBAR --> 
  <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
  <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
  <div class="page-sidebar navbar-collapse collapse"> 
   <!-- BEGIN SIDEBAR MENU --> 
   <jsp:include page="sidebar.jsp" />
<!--   <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
     DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
    <li class="sidebar-toggler-wrapper hide"> 
      BEGIN SIDEBAR TOGGLER BUTTON 
     <div class="sidebar-toggler"> <span></span> </div>
      END SIDEBAR TOGGLER BUTTON  
    </li>
    <li class="nav-item  "> <a href="index.html" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
    <li class="nav-item  "> <a href="users.html" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
     
    </li>
    <li class="nav-item  active"> <a href="licence.html" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">Licence</span>  </a>
     
    </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
     
    </li>
    <li class="nav-item  "> <a href="report.html" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
    <li class="nav-item  "> <a href="watermark.html" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
    <li class="nav-item  "> <a href="settings.html" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
   </ul>-->
   <!-- END SIDEBAR MENU --> 
   <!-- END SIDEBAR MENU --> 
  </div>
  <!-- END SIDEBAR --> 
 </div>
 <!-- END SIDEBAR --> 
 <!-- BEGIN CONTENT -->
 <div class="page-content-wrapper"> 
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content"> 
   <!-- BEGIN PAGE HEADER--> 
   
   <!-- BEGIN PAGE TITLE-->
   <!--<h3 class="page-title"> Licence 
    <!-- <small>blank page layout</small>-- 
   </h3>-->
   <!-- END PAGE TITLE--> 
   <!-- END PAGE HEADER-->
   
 
  <div class="row">
         <div class="col-sm-12">
         	<div class="portlet box red   portlet-datatable  ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-key "></i>
                                        License
                                    </div>
                                    <div class="actions">
                                        <a class="btn  " href="javascript:;" style="display:none;" data-toggle="tooltip" data-placement="top" title="Edit Licence" id="editlicense">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a class="btn  " href="javascript:;" style="display:none;" data-toggle="tooltip" data-placement="top" title="Delete Licence" id="deleteusers">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                        <a class="btn  btn-icon-only " style="display: none;" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Renew Licence" id="renewlicense">
                                            <i class="icon-refresh"></i>
                                        </a>
                                        <a class="btn  btn-icon-only " style="display: none;" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Print" id="exportDialog">
                                            <i class="icon-printer"></i>
                                        </a>
                                        <a class="btn  btn-icon-only " style="display: none;" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Print" id="exportLicense">
                                            <i class="icon-printer"></i>
                                        </a>
                                        <a class="btn btn-icon-only " style="display: none;" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Copy" id="copy">
                                            <i class="icon-docs"></i>
                                        </a>
                                         <a class="btn  " href="createlicense" data-toggle="tooltip" data-placement="top" title="Add Licence">
                                            <i class="icon-plus"></i> 
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_5">
                                        <thead>
                                            <tr>
                                                <th class="table-checkbox">
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes" />
                                                        <span></span>
                                                    </label>
                                                </th>
                                                <th> Username </th>
                                                <th> Organization </th>
                                                <th> License Key </th>
                                                <th> Web Limit </th>
                                                <th> Desktop Limit </th>
                                                <th> Start Date </th>
                                                <th> End Date </th>
                                            </tr>
                                        </thead>
                                       
                                        <tbody id="tBody">
                                            <c:forEach items="${clientlicenses}" var="license">
                                                <tr class="odd gradeX">
                                                    <td>
                                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" name="licenceId" class="checkboxes" value="${license.adminLicenseId}" />
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td> ${license.userInfo.userName} </td>
                                                    <td> ${license.userInfo.organization} </td>
                                                    <td> <span class="ellipsis">${license.license}</span> </td>
                                                    <c:if test="${license.weblicenseLimit == -1}">
                                                        <td>Unlimited</td>
                                                    </c:if>
                                                    <c:if test="${license.weblicenseLimit != -1}">
                                                        <td>${license.weblicenseLimit}</td>
                                                    </c:if>
                                                    
                                                    <c:if test="${license.desktopLicenseLimit == -1}">
                                                        <td class="desktoplimit${license.adminLicenseId}">Unlimited</td>
                                                    </c:if>
                                                    <c:if test="${license.desktopLicenseLimit != -1}">
                                                        <td class="desktoplimit${license.adminLicenseId}">${license.desktopLicenseLimit}</td>
                                                    </c:if>
                                                    
                                                    <td><fmt:formatDate value="${license.issueDate}" pattern="dd-MM-yyyy" /></td>
                                                    <td id="expiry${license.adminLicenseId}"><fmt:formatDate value="${license.expiryDate}" pattern="dd-MM-yyyy" /></td>                         
<!--                                                    <td>
                                                        <a href="${pageContext.request.contextPath}/${user.userName}/deletelicense/${license.adminLicenseId}">Delete</a>
                                                        <a href="${pageContext.request.contextPath}/${user.userName}/renewlicense/${license.adminLicenseId}">Renew</a>
                                                    </td>-->
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    <span class="licenseerror" style="font-weight: bold; color: red;"></span> 
                                </div>
                            </div>
         </div>               
  </div>
   
   
  </div>
  <!-- END CONTENT BODY --> 
 </div>
 <!-- END CONTENT --> 
 <!-- BEGIN QUICK SIDEBAR --> 
 <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 
 
 <!-- END QUICK SIDEBAR --> 
</div>
<!-- END CONTAINER --> 
<!-- BEGIN FOOTER -->
<div class="page-footer">
 <div class="page-footer-inner"> 2016 &copy; Organization </div>
 <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
</div>
        
        <div class="modal fade" id="exportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel1">Export License Details</h4>
                    </div>
                    <!-- dialog body -->
                    <div class="modal-body">

                        <div class="date-picker input-daterange "  data-date-format="dd-mm-yyyy">
                            <div class="form-group clearfix">
                                <label class="col-sm-3" style="font-weight: bold;">Start Date </label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" class="form-control input-circle-right" id="startDate" placeholder="Start Date" style="text-align: left;"/>
                                    </div>
                                </div>
                                <span id="errorstartdate" class="error-message" style="margin-left: 155px;"></span> 
                            </div>
                        </div>
                        <div class="date-picker input-daterange "  data-date-format="dd-mm-yyyy">
                            <div class="form-group clearfix">
                                <label class="col-sm-3" style="font-weight: bold;">End Date </label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" class="form-control input-circle-right" id="endDate" placeholder="End Date" style="text-align: left;"/>
                                    </div>
                                </div>
                                <span id="errorenddate" class="error-message" style="margin-left: 155px;"></span> 
                            </div> 
                        </div>
<!--                        <div>
                            <button type="button" id="export">Export</button>
                        </div>-->
                    </div>
                    <div class="modal-footer">
                        <!--<button type="reset" class="btn btn-circle btn-danger" >Cancel</button>data-dismiss="modal"-->
                        <button type="button" class="btn btn-circle btn-success" id="export">Export</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END FOOTER --> 
        <div class="modal fade" id="renew_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Renew License</h4>
                    </div>
                    <form>
                        <div class="modal-body">
                            <input type="hidden" id="licenseId"/>
                            <div class="form-group clearfix">
                                <label>User Name </label>
                                <input class="form-control" readonly value="XYZ" id="userName">
                            </div>
                            <div class="form-group clearfix">
                                <label>Organization Name </label>
                                <input class="form-control" readonly value="ABC" id="organization">
                            </div>
                            <div class="form-group clearfix">
                                <select class="form-control" id="validity">
                                    <option selected="" disabled="">Select Validity</option>
                                    <c:forEach var="i" begin="1" end="20">
                                        <option value="${i}"> ${i}</option>
                                    </c:forEach>
                                </select>
                            </div>

                            <div class="form-group clearfix">
                                <div class="one-half-50">
                                    <div class="one">
                                        <input class="form-control" placeholder=" Issue Date" id="issueDate">
                                    </div>
                                    <div class="one">
                                        <input class="form-control" placeholder=" Expiry Date" id="expiryDate">
                                    </div>	
                                </div>
                            </div>

                            <div class="form-group clearfix">
                                <div class="one-half">
                                    <div class="two" style="float:left">
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="    margin-top: 8px;">
                                            <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes" id="unlimited">
                                            <span></span>
                                        </label>

                                    </div>
                                    <div class="one" style="float:right">
                                        <input class="form-control" placeholder=" License Limit" id="licenseLimit">
                                        <div class="hint text-right">(please click checkbox for unlimited license)</div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group clearfix">

                                <div class="mt-radio-inline" style=" padding:0px;">
                                    <label style=" margin-right:15px;">Carry Forward :</label>
                                    <label class="mt-radio mt-radio-outline">
                                        <input type="radio" name="carryforward" id="radio1" value="1" checked="checked"> Yes
                                        <span></span>
                                    </label>
                                    <label class="mt-radio mt-radio-outline">
                                        <input type="radio" name="carryforward" id="radio2" value="0"> No
                                        <span></span>
                                    </label>

                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label style=" margin-right:15px;"> System ID:</label>
                                <input readonly class="form-control" value="4f5454d64dsf5dsf4d5s4fdsf3ds4f54ds5f4s" id="systemId">
                            </div>
                            <div class="form-group clearfix">
                                <div class="two-half">
                                    <div class="one">
                                        <input class="form-control" placeholder=" License Key" id="licenceKey" readonly>
                                    </div>
                                    <div class="two">
                                        <a id="copylicense" href="#" class="btn  btn-default"><i class="fa fa-copy"></i></a>
                                        <a href="#" class="btn  btn-default"><i class="fa fa-download"></i></a>
                                    </div>
                                </div>
                            </div>
                            <span style="font-size: smaller; font-weight:bold;  color: red" id="licenseError" ></span>

                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-default" >Reset</button><!--data-dismiss="modal"-->
                            <button type="button" class="btn green" id="generateLicense">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="otpModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- dialog body -->
                    <div class="modal-body">

                        <div>
                            <input type="hidden" id="random"/>
                            <input type="text" id="otp"/>
                        </div>
                        <div>
                            <button type="button" id="submitotp">Submit</button>
                        </div>
                        <div>
                            <button type="button" id="resendotp">Resend OTP</button>
                        </div>
                        <span style="font-size: smaller; font-weight:bold;  color: red" id="otpError" ></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confirm_delete" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="">Confirm Delete</h4>
                    </div>
                    <div class="modal-body"> 
                        <div class="form-group">
                            <span Style="font-style: normal;">Are You Sure To Want To Delete This User ?</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class=" btn btn green" data-dismiss="modal">NO</button>-->
<!--                        <button type="button" class="btn green" id="">YES</button>-->
                        <a href="#" id="Confirm" class=" btn btn-circle btn-success">YES</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
<!-- BEGIN CORE PLUGINS --> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
<!-- END CORE PLUGINS --> 
<!-- BEGIN THEME GLOBAL SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
<!-- END THEME GLOBAL SCRIPTS --> 
<!-- BEGIN THEME LAYOUT SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
<!-- END THEME LAYOUT SCRIPTS --> 
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="${pageContext.request.contextPath}/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="${pageContext.request.contextPath}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/clipboard/clipboard.min.js"></script>
                <script>
			$(function () {
                            $('[data-toggle="tooltip"]').tooltip();
			});
		</script>
                <script>
                    $( ".form-control" ).focus(function() {
                        $(this).prev('.input-group-addon').removeClass().addClass('input-group-addon-focus');
                        $(this).next('.input-group-addon').removeClass().addClass('input-group-addon-focus');
                    });

                    $( ".form-control" ).focusout(function() {
                        $(this).prev('.input-group-addon-focus').removeClass().addClass('input-group-addon');
                        $(this).next('.input-group-addon-focus').removeClass().addClass('input-group-addon');
                    });
                </script>
                <script>
                    var issuedate = $('#startDate').datepicker({
                        format: "dd-mm-yyyy"
                    });
                    var endDate = $('#endDate').datepicker({
                        format: "dd-mm-yyyy"
                    });

                    issuedate.on('changeDate', function (e) {
                        $('#endDate').val("");
                        console.log(e);
                        endDate.datepicker('setStartDate', e.date);
                    });
            
                    $(document).on("click", "#exportDialog", function () {
                        $('#startDate').val("");
                        $('#endDate').val("");
                        $("#errorstartdate").text("");
                        $("#errorenddate").text("");
                        $("#exportModal").modal("show");
                    });
                    $(document).on("click", "#export", function () {
                        var startDate = $("#startDate").val();
                        var endDate = $("#endDate").val();
                        var flag = true;
                        if (startDate === null || startDate === "") {
                            $("#errorstartdate").text("Please select start date.");
                            flag = false;
                        }
                        if (endDate === null || endDate === "") {
                            $("#errorenddate").text("Please select end date.");
                            flag = false;
                        }
                        if (flag) {
                            var Uname = "${user.userName}".replace(" ", "");
                            window.location.href = "${pageContext.request.contextPath}/"+Uname+"/exportLicense?startDate=" + startDate + "&endDate=" + endDate;
                            $("#exportModal").modal("hide");
                        }
                    });
                    
                    $("#exportLicense").on("click", function(){
                        var checkedData = [];
                        $("#tBody").find("input[type='checkbox'][name='licenceId']:checked").each(function () {
                            checkedData.push($(this).val());
                        });
                        if (checkedData === null || checkedData.length === 0) {
                            $(".licenseerror").text("Please select a license.");
                        } else if (checkedData.length > 1) {
                            $(".licenseerror").text("More than one license is selected.");
                        } else {
                            var Uname = "${user.userName}".replace(" ", "");
                            window.location.href = "${pageContext.request.contextPath}/"+Uname+"/exportsingleLicense?licenseId="+checkedData[0];
                        }
                    });
                </script>
                <script>
                    $(document).on("click", "#renewlicense", function () {
                        var checkedData = [];
                        $("#tBody").find("input[type='checkbox'][name='licenceId']:checked").each(function () {
                            checkedData.push($(this).val());
                        });
                        if (checkedData === null || checkedData.length === 0) {
                            $(".licenseerror").text("Please select a license.");
                        } else if (checkedData.length > 1) {
                            $(".licenseerror").text("More than one license is selected.");
                        } else {
                            var e = $("#expiry"+checkedData[0]).text();
                            var Uname = "${user.userName}".replace(" ", "");
                            if(e === null || e === "" ){
                                console.log(e);
                                var limit = $(".desktoplimit"+checkedData[0]).text();
                                if(limit.indexOf("Unlimited") > -1){
                                    $(".licenseerror").text("This License have LifeTime Validity and Unlimited License Limit You can not renew It.");
                                }else{
                                    window.location.href = "${pageContext.request.contextPath}/"+Uname+"/renewlicense/"+ checkedData[0];
                                }
                            } else{
                                window.location.href = "${pageContext.request.contextPath}/"+Uname+"/renewlicense/"+ checkedData[0];
                            } 
                            
//                                    function (data, status) {
//                                        if (data.indexOf("success") > -1) {
//                                            var jsondata = JSON.parse(data.split("#")[1]);
//                                            $("#licenseId").val(jsondata.licenseId);
//                                            $("#userName").val(jsondata.userName);
//                                            $("#organization").val(jsondata.organization);
//                                            $("#systemId").val(jsondata.systemId);
//
//                                            $("#renew_modal").modal("show");
//                                        } else {
//                                            alert(data);
//                                        }
                        }
                    });
                    $(document).on("click", "#editlicense", function () {
                        var checkedData = [];
                        $("#tBody").find("input[type='checkbox'][name='licenceId']:checked").each(function () {
                            checkedData.push($(this).val());
                        });
                        if (checkedData === null || checkedData.length === 0) {
                            $(".licenseerror").text("Please select a license.");
                        } else if (checkedData.length > 1) {
                            $(".licenseerror").text("More than one license is selected.");
                        } else {
                            var Uname = "${user.userName}".replace(" ", "");
                            window.location.href = "${pageContext.request.contextPath}/"+Uname+"/editlicense/"+ checkedData[0];
                        }
                    });
                </script>
                <script>
                    var clipboard = new Clipboard('#copy', {
                        text: function () {
                            var checkedData = [];
                            $("#tBody").find("input[type='checkbox'][name='licenceId']:checked").each(function () {
                                checkedData.push($(this).parent().parent().next().next().next().children().html());
                            });
                            if (checkedData === null || checkedData.length === 0) {
                                $(".licenseerror").text("Please select a license.");
                            } else if (checkedData.length > 1) {
                                $(".licenseerror").text("More than one license is selected.");
                            } else {
                                return checkedData[0];
                            }
                        }
                    });
                    clipboard.on('success', function (e) {
                        console.log(e);
                    });
                    clipboard.on('error', function (e) {
                        console.log(e);
                    });
                </script>
                <script>
                    var clipboard = new Clipboard('#copylicense', {
                        text: function () {
                            return $("#licenceKey").val();
                        }
                    });
                    clipboard.on('success', function (e) {
                        console.log(e);
                    });
                    clipboard.on('error', function (e) {
                        console.log(e);
                    });
                </script>
                <script>
                    $(document).ready(function () {
                        $('#side-menu').find('li').removeClass('active');
                        $(document).find(".license").addClass('active');
                        
                        $(".checkboxes").attr('checked', false);
                    });
                </script>
                <script>
                    $("#generateLicense").on("click", function () {
                        var licenseId = $("#licenseId").val();
                        var validity = $("#validity").val();
                        var startDate = $("#issueDate").val();
                        var endDate = $("#expiryDate").val();
                        var licenseLimit = $("#licenseLimit").val();
                        var unlimited = $("#unlimited").is(":checked");
                        var carry = $('input[name="carryforward"]:checked').val();
                        console.log("carry : " + carry);
                        var Uname = "${user.userName}".replace(" ", "");
                        $.post("${pageContext.request.contextPath}/"+Uname+"/renewlicense",
                                {licenseId: licenseId, validity: validity, startDate: startDate, endDate: endDate,
                                    licenseLimit: licenseLimit, unlimited: unlimited, carryforward: carry},
                                function (data, status) {
                                    if (data === "Please login again.") {
                                        window.location.href = "/";
                                    } else if (data.contains("success")) {
        //                                alert(data.split("#")[1]);
                                        $("#random").val(data.split("#")[1]);
                                        $("#otpModal").modal("show");
                                    } else {
                                        $("#licenseError").text(data);
                                    }
                                });
                    });
                </script>
                <script>
                    $(document).on("click", "#submitotp", function () {
                        var otp = $("#otp").val();
                        var random = $("#random").val();
                        if (otp === "") {
                           $("#otpError").text("Please Enter OTP"); 
                        } else if (random === "") {
                            $("#otpError").text("Login again"); 
                            window.location.href = "/";
                        } else {
                            var Uname = "${user.userName}".replace(" ", "");
                            $.post("${pageContext.request.contextPath}/"+Uname+"/varifyotp", {otp: otp, random: random},
                                    function (data, status) {
                                        if (data === "Please login again.") {
                                            window.location.href = "/";
                                        } else if (data.indexOf("success")!==-1) {
                                            $("#licenceKey").val(data.split("#")[1]);
                                            $("#otpModal").modal("hide");
                                        } else {
                                            $("#otpError").text(data); 
                                        }
                                    });
                        }
                    });
                    $(document).on("click", "#resendotp", function () {
                        var random = $("#random").val();
                        if (random === "") {
                            $("#otpError").text("Login again"); 
                            window.location.href = "/";
                        } else {
                            var Uname = "${user.userName}".replace(" ", "");
                            $.post("${pageContext.request.contextPath}/"+Uname+"/resendotp", {random: random},
                                    function (data, status) {
                                        if (data === "Please login again.") {
                                            window.location.href = "/";
                                        } else if (data.contains("success")) {
                                            $("#random").val(data.split("#")[1]);
                                        } else {
                                            $("#otpError").text(data); 
                                        }
                                    });
                        }
                    });
                    
                    $(document).on("click", "#deleteusers", function () {
                         $("#confirm_delete").modal("show");
                    });
                    
                    $(document).on("click", "#Confirm", function () {
                        var checkedData = [];
                        $("#tBody").find("input[type='checkbox'][name='licenceId']:checked").each(function () {
                            checkedData.push($(this).val());
                        });
                        if (checkedData === null || checkedData.length === 0) {
                            $(".licenseerror").text("Please select a License.");
                            $("#confirm_delete").modal("hide");
                        } else if (checkedData.length > 1) {
                            $(".licenseerror").text("More than one License is selected.");
                            $("#confirm_delete").modal("hide");
                        }else {
                            var Uname = "${user.userName}".replace(" ", "");
                            window.location.href = "${pageContext.request.contextPath}/"+Uname+"/deletelicense/"+checkedData[0];         
                        }
                    });
                </script>
        <script>
            $(".checkboxes").on("click", function(){
                var checkedData = [];
                        $("#tBody").find("input[type='checkbox'][name='licenceId']:checked").each(function () {
                            checkedData.push($(this).val());
                        });
                        if (checkedData === null || checkedData.length === 0) {
                            $("#deleteusers").hide();
                            $("#renewlicense").hide();
                            $("#editlicense").hide();
                            $("#copy").hide();
                            $("#exportDialog").hide();
                            $("#exportLicense").hide();
                        } else if (checkedData.length > 1) {
                            $("#deleteusers").hide();
                            $("#renewlicense").hide();
                            $("#editlicense").hide();
                            $("#copy").hide();
                            $("#exportDialog").hide();
                            $("#exportLicense").hide();
                        } else if (checkedData.length === 1){
                            $("#deleteusers").show();
                            $("#renewlicense").show();
                            $("#editlicense").show();
                            $("#copy").show();
                            $("#exportDialog").hide();
                            $("#exportLicense").show();
                        }
            });
            
            $(".group-checkable").on("click", function () {
                        $(".licenseerror").text("");
                        $("#exportLicense").hide();
                        var checkedData = [];
                        var checkedboxes = [];
                        $("#tBody").find("input[type='checkbox'][name='licenceId']").each(function () {
                            checkedData.push($(this).val());      
                        });
                        $("#tBody").find("input[type='checkbox'][name='licenceId']:checked").each(function () {
                            checkedboxes.push($(this).val());   
                        });
                        console.log($(this).is(":checked"));
                        if($(this).is(":checked")){
                            console.log(checkedboxes);
                            if(checkedData === null || checkedData.length === 0){
                                $("#deleteusers").hide();
                            }else{
//                                $("#deleteusers").show();
                                if(checkedboxes !== null || checkedboxes.length !== 0){
                                    $("#deleteusers").hide();
                                    $("#renewlicense").hide();
                                    $("#editlicense").hide();
                                    $("#copy").hide();
                                    $("#exportDialog").show();
                                }
                            }
                        }else{
                            $("#deleteusers").hide();
                            $("#renewlicense").hide();
                            $("#editlicense").hide();
                            $("#copy").hide();
                            $("#exportDialog").hide();
                        }
                        console.log(checkedData);
                    });
        </script>
                
</body>
</html>
