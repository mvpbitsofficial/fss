<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>FSS | Create License</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />-->
        <link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="${pageContext.request.contextPath}/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- END THEME GLOBAL STYLES -->
        <!--<link href="${pageContext.request.contextPath}/assets/global/dist/easy-autocomplete.min.css" rel="stylesheet" type="text/css">
        <link href="${pageContext.request.contextPath}/assets/global/dist/easy-autocomplete.themes.min.css" rel="stylesheet" type="text/css">-->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->

        <style>
            #other-email-section {
                display: none;
            }

            .number-dropdown{ width:200px;} 
            .number-dropdown .btn-group{width:100%;}
            .number-dropdown .btn-group label{ width:calc(100%/5);}

            .number-dropdown .btn-group label.btn-default.active{ background:#e7505a; border-color:#e7505a; color:#fff;}
            .number-dropdown .btn-group .btn+.btn{ margin-left:0px;}
            .form-wizard .steps > li.done > a.step .desc i{display: none !important;}
        </style>
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top"> 
            <!-- BEGIN HEADER INNER -->
            <jsp:include page="topbar.jsp" />
            <!-- END HEADER INNER --> 
        </div>
        <!-- END HEADER --> 
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER --> 
        <!-- BEGIN CONTAINER -->
        <div class="page-container"> 
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper"> 
                <!-- BEGIN SIDEBAR --> 
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse"> 
                    <!-- BEGIN SIDEBAR MENU -->
                    <jsp:include page="sidebar.jsp" />
                    <!--   <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                         DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
                        <li class="sidebar-toggler-wrapper hide"> 
                          BEGIN SIDEBAR TOGGLER BUTTON 
                         <div class="sidebar-toggler"> <span></span> </div>
                          END SIDEBAR TOGGLER BUTTON  
                        </li>
                        <li class="nav-item  "> <a href="index.html" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
                        <li class="nav-item  "> <a href="users.html" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a> </li>
                        <li class="nav-item  "> <a href="licence.html" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">Licence</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a> </li>
                        <li class="nav-item  "> <a href="report.html" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
                        <li class="nav-item  "> <a href="watermark.html" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
                        <li class="nav-item "> <a href="settings.html" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
                       </ul>-->
                    <!-- END SIDEBAR MENU --> 
                    <!-- END SIDEBAR MENU --> 
                </div>
                <!-- END SIDEBAR --> 
            </div>
            <!-- END SIDEBAR --> 
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper"> 
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content"> 
                    <!-- BEGIN PAGE HEADER--> 

                    <!-- BEGIN PAGE TITLE--> 
                    <!--<h3 class="page-title"><i class="icon-docs "></i> Report
                    <!-- <small>blank page layout</small>--
                    </h3>--> 
                    <!-- END PAGE TITLE--> 
                    <!-- END PAGE HEADER-->

                    <div class="row">
                        <div class="col-md-12"> 
                            <!-- BEGIN PROFILE SIDEBAR --> 

                            <!-- END BEGIN PROFILE SIDEBAR --> 
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered  box-shadow" id="form_wizard_1">
                                            <div class="portlet-title tabbable-line">
                                                <div class="caption caption-md" > <i class="icon-globe theme-font hide"></i>
                                                    <span class="caption-subject  bold uppercase"><i class="icon-key "></i> Create License</span> </div>
                                                <!--<ul class="nav nav-tabs ">
                                                                                          <li class="active">
                                                                                              <a href="#tab_1_1" data-toggle="tab">Profile</a>
                                                                                          </li>
                                                                                           <li class="">
                                                                                              <a href="#tab_1_2" data-toggle="tab">Email</a>
                                                                                          </li>
                                                                                                                                              
                                                                                                                                              <li class="">
                                                                                              <a href="#tab_1_4" data-toggle="tab">Notification</a>
                                                                                          </li>
                                                                                                                                              <li class="">
                                                                                              <a href="#tab_1_5" data-toggle="tab">Default Settings</a>
                                                                                          </li>
                                                                                                                                              <li class="">
                                                                                              <a href="#tab_1_6" data-toggle="tab">Other Settings</a>
                                                                                          </li>
                                                                                      </ul>--> 
                                            </div>
                                            <div class="portlet-body form">
                                                <form class="form-horizontal" action="#" id="submit_form" method="POST">
                                                    <div class="form-wizard">
                                                        <div class="form-body">
                                                            <ul class="nav nav-pills nav-justified steps" style="height: 60px; margin-top: 0px;">

                                                                <li> <a href="#tab1" data-toggle="tab" class="step"> <span class="number"><b> 1 </b></span> <span class="desc"> <i class="fa fa-check"></i> Select User </span> </a> </li>
                                                                <li> <a href="#tab2" data-toggle="tab" class="step"> <span class="number"><b> 2 </b></span> <span class="desc"> <i class="fa fa-check"></i> Select Application </span> </a> </li>
                                                                <li> <a href="#tab3" data-toggle="tab" class="step steptab3"> <span class="number"><b> 3 </b></span> <span class="desc"> <i class="fa fa-check"></i> License for System </span> </a> </li>
                                                                <li> <a href="#tab4" data-toggle="tab" class="step steptab4"> <span class="number"><b> 4 </b></span> <span class="desc"> <i class="fa fa-check"></i> Confirm </span> </a> </li>
                                                            </ul>
                                                            <div id="bar" class="progress progress-striped" role="progressbar">
                                                                <div class="progress-bar progress-bar-success"> </div>
                                                            </div>
                                                            <div class="tab-content">
                                                                <div class="alert alert-danger display-none">
                                                                    <button class="close" data-dismiss="alert"></button>
                                                                    You have some form errors. Please check below. </div>
                                                                <div class="alert alert-success display-none">
                                                                    <button class="close" data-dismiss="alert"></button>
                                                                    Your form validation is successful! </div>

                                                                <div class="tab-pane active" id="tab1">
                                                                    <!-- <h3 class="block">Provide your profile details</h3>-->
                                                                    <div class="col-sm-4" style="margin:0 auto; float:none;">
                                                                        <div class="input-group">
                                                                            <div class="input-group-btn">
                                                                                <button type="button" class="btn btn-default dropdown-toggle input-circle-left" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> </button>
                                                                                <div class="dropdown-menu hold-on-click dropdown-checkboxes">
                                                                                    <label class="mt-checkbox">
                                                                                        <input type="checkbox" class="" id="getinfobyid" checked/>
                                                                                        User ID <span></span> </label>
                                                                                    <label class="mt-checkbox">
                                                                                        <input type="checkbox" class="" id="getinfobyname"/>
                                                                                        User Name <span></span> </label>
                                                                                    <label class="mt-checkbox ">
                                                                                        <input type="checkbox" class="" id="getinfobyemail" />
                                                                                        Email <span></span> </label>
                                                                                    <label class="mt-checkbox ">
                                                                                        <input type="checkbox" class="" id="getinfobymobile" />
                                                                                        Mobile Number<span></span> </label>
                                                                                </div>
                                                                                <!--                        <ul class="dropdown-menu radio_dropdown">
                                                                                                            <li class="radio">
                                                                                                                <label>
                                                                                                                    <input type="radio" name="radioname" id="nameradio" value="1" onchange="getinfobyname()" > UserName
                                                                                                                </label>
                                                                                                             </li>
                                                                                                            <li>
                                                                                                                <label>
                                                                                                                     <input type="radio" name="radioname" id="emailradio" value="2" onchange="getinfobyemail()"> Email
                                                                                                                </label>
                                                                                                            </li>
                                                                                                            <li>
                                                                                                                <label>
                                                                                                                 <input type="radio" name="radioname" id="mobileradio" value="3" onchange="getinfobymobile()"> Mobile no.
                                                                                                                </label>
                                                                                                            </li>
                                                                                                         
                                                                                                        </ul>-->
                                                                            </div><!-- /btn-group -->
                                                                            <div class="input-group" id="dropdown" style="width: 100%;">
                                                                                <select id="useruserid" class="form-control input-circle-right selectpicker" data-live-search='true'>
                                                                                    <option value="" >Select user</option>
                                                                                </select>
                                                                            </div>
                                                                            <!--                        <div class="input-group" id="inputbox" style="display: none;">
                                                                                                        <input type="text" id="userid" class="form-control" />
                                                                                                     </div>
                                                                                                  <input type="text" class="form-control" aria-label="...">
                                                                                                   <div class="input-group-btn">
                                                                                                       <button type="button" class="btn btn-default dropdown-toggle input-circle-right" id="search"><i class="fa fa-search"></i></button>
                                                                                                  </div> -->
                                                                        </div><!-- /input-group -->
                                                                        <!--                    <select id="searchdropdown" style="display: none;" class="form-control input-circle">
                                                                                                <option></option>
                                                                                            </select>-->
                                                                        <div style="display: none;" id="displayuserdetails">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 control-label">User ID</label>
                                                                                <div class="col-sm-8">
                                                                                    <div class="input-group">		 
                                                                                        <label class="control-label" id="displayuserid"></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 control-label">Name</label>
                                                                                <div class="col-sm-8">
                                                                                    <div class="input-group">		 
                                                                                        <label class="control-label" id="displayname"></label>
                                                                                    </div><!-- /input-group -->

                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 control-label">Email</label>
                                                                                <div class="col-sm-8">
                                                                                    <div class="input-group">		 
                                                                                        <label class="control-label" id="displayemail"></label>
                                                                                    </div><!-- /input-group -->

                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 control-label">Country</label>
                                                                                <div class="col-sm-8">
                                                                                    <div class="input-group">		 
                                                                                        <label class="control-label" id="displaycountry"></label>
                                                                                    </div><!-- /input-group -->

                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 control-label">Mobile</label>
                                                                                <div class="col-sm-8">
                                                                                    <div class="input-group">		 
                                                                                        <label class="control-label" id="displaymobilenumber"></label>
                                                                                    </div><!-- /input-group -->

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <span class="error-message" id="userError" ></span>
                                                                    </div><!-- /.col-lg-6 -->
                                                                </div>
                                                                <div class="tab-pane" id="tab2">
                                                                    <div  class="form-horizontal col-sm-4" style="margin:0 auto; float:none;">
                                                                        <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon input-circle-left">
                                                                                        <i class="fa fa-book"></i>
                                                                                    </span>
                                                                                    <select class="form-control input-circle-right selectpicker"  id="courseid" style="width: 100%;" data-live-search='true'>
                                                                                        <option selected disabled>Select Course</option>
                                                                                        <c:forEach items="${courses}" var="course">
                                                                                            <option value="${course.id}">${course.courseName}</option>
                                                                                        </c:forEach>
                                                                                    </select>
                                                                                </div><!-- /input-group -->
                                                                        </div>
                                                                        <div style="display: none;" id="showcoursedetail">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-4 control-label">Course ID</label>
                                                                                <div class="col-sm-8">
                                                                                    <div class="input-group">		 
                                                                                        <label class="control-label" id="displaycourseid"></label>
                                                                                    </div><!-- /input-group -->

                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-sm-4 control-label">Total Used</label>
                                                                                <div class="col-sm-8">
                                                                                    <div class="input-group">		 
                                                                                        <label class="control-label" id="displaytotalused"></label>
                                                                                    </div><!-- /input-group -->

                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-sm-4 control-label">Course Details</label>
                                                                                <div class="col-sm-8">
                                                                                    <div class="input-group">		 
                                                                                        <label class="control-label" id="displaycoursedetails"></label>
                                                                                    </div><!-- /input-group -->
                                                                                </div>
                                                                            </div>
                                                                        </div>   
                                                                        <span class="error-message" style="margin-left: 35%;" id="courseError" ></span>
                                                                    </div>

                                                                </div>
                                                                <div class="tab-pane" id="tab3">
                                                                    <div  class="form-horizontal col-sm-6" style="margin:0 auto; float:none;">
                                                                        <div class="form-group">
                                                                            <label class="col-sm-4 control-label">Name</label>
                                                                            <div class="col-sm-8">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon input-circle-left">
                                                                                        <i class="fa fa-user"></i>
                                                                                    </span>  
                                                                                    <input type="text" readonly class="form-control input-circle-right" id="selecteduser"/>
                                                                                    <!--			<select class="form-control input-circle" disabled>
                                                                                                                    <option id="selecteduser">Harish</option>
                                                                                                            </select>-->
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-4 control-label">Course</label>
                                                                            <div class="col-sm-8">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon input-circle-left">
                                                                                        <i class="fa fa-book"></i>
                                                                                    </span>
                                                                                    <input type="text" readonly class="form-control input-circle-right" id="selectedcourse"/>
                                                                                    <!--                            <select class="form-control input-circle" disabled>
                                                                                                            </select>-->
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">                                            
                                                                            <label class="col-sm-4 control-label">Validity</label>
                                                                            <div class="col-sm-8">
                                                                                <div class="input-group">  
                                                                                    <span class="input-group-addon input-circle-left">
                                                                                        <i class="fa fa-star"></i>
                                                                                    </span>
                                                                                    <div class="dropdown">
                                                                                        <button class="form-control input-circle-right dropdown-toggle" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <span class="status pull-left"></span> <span class="caret pull-right" style="margin-top:5px;"></span> </button>
                                                                                        <div class="dropdown-menu number-dropdown col-sm-5" aria-labelledby="dLabel" style="width: 250px;">
                                                                                            <div class="btn-group" data-toggle="buttons" id="radiobuttons">
                                                                                                <label class="btn btn-default valid" id="1">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    1 </label>
                                                                                                <label class="btn btn-default valid" id="2">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    2 </label>
                                                                                                <label class="btn btn-default valid" id="3">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    3 </label>
                                                                                                <label class="btn btn-default valid" id="4">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    4 </label>
                                                                                                <label class="btn btn-default valid" id="5">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    5 </label>
                                                                                                <label class="btn btn-default valid " id="6">
                                                                                                    <input type="radio" name="options" autocomplete="off" >
                                                                                                    6 </label>
                                                                                                <label class="btn btn-default valid" id="7">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    7 </label>
                                                                                                <label class="btn btn-default valid" id="8">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    8 </label>
                                                                                                <label class="btn btn-default valid" id="9">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    9 </label>
                                                                                                <label class="btn btn-default valid" id="10">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    10 </label>
                                                                                                <label class="btn btn-default valid " id="11">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    11 </label>
                                                                                                <label class="btn btn-default valid" id="12">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    12 </label>
                                                                                                <label class="btn btn-default valid" id="13">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    13 </label>
                                                                                                <label class="btn btn-default valid" id="14">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    14 </label>
                                                                                                <label class="btn btn-default valid" id="15">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    15 </label>
                                                                                                <label class="btn btn-default valid " id="16">
                                                                                                    <input type="radio" name="options" autocomplete="off" >
                                                                                                    16 </label>
                                                                                                <label class="btn btn-default valid" id="17">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    17 </label>
                                                                                                <label class="btn btn-default valid" id="18">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    18 </label>
                                                                                                <label class="btn btn-default valid" id="19">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    19 </label>
                                                                                                <label class="btn btn-default valid" id="20">
                                                                                                    <input type="radio" name="options" autocomplete="off">
                                                                                                    20 </label>
                                                                                            </div>
                                                                                            <div style="text-align:left; padding-bottom:5px;">
                                                                                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline" style="margin-top: 8px; margin-left: 8px;">
                                                                                                    <input type="hidden" id="validity" />
                                                                                                    <input type="checkbox" class="group-checkable" name="options" id="lifetime" value="0"/>
                                                                                                    <span></span>LifeTime</label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <span class="error-message" id="validityError" ></span>		
                                                                                <!--           <div class="hint text-right">(please click checkbox for unlimited licence)</div>-->

                                                                                <!--                            <table style="width:100%;">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <input type="checkbox" class="form-control" id="lifetime"/> 
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <input type="number" class="form-control input-circle" min="1" max="20" id="validity"/>
                                                                                                                        <select class="form-control input-circle" id="validity">
                                                                                                                            <option selected disabled>Select Validity</option>
                                                                                <c:forEach var="i" begin="1" end="20">
                                                                                    <option value="${i}"> ${i}</option>
                                                                                </c:forEach>
                                                                        </select>  
                                                                    </td>
                                                                </tr>
                                                            </table> -->
                                                                            </div>
                                                                        </div>
                                                                        <div class="date-picker input-daterange "  data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                                                            <div class="form-group ">
                                                                                <label class="col-sm-4 control-label">Issue Date</label>
                                                                                <div class="col-sm-8">
                                                                                    <div class="input-group" style="width: 100%;">
                                                                                        <span class="input-group-addon input-circle-left">
                                                                                            <i class="fa fa-calendar"></i>
                                                                                        </span>
                                                                                        <input type="text" class="form-control input-circle-right " id="startDate" name="from" style="text-align:left !important;"/>
                                                                                    </div>
                                                                                    <span class="error-message" id="startdateError" ></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="date-picker input-daterange "  data-date-format="dd-mm-yyyy">			
                                                                            <div class="form-group">
                                                                                <label class="col-sm-4 control-label">Expiry Date</label>
                                                                                <div class="col-sm-8">
                                                                                    <div class="input-group" style="width: 100%;">
                                                                                        <span class="input-group-addon input-circle-left">
                                                                                            <i class="fa fa-calendar"></i>
                                                                                        </span>
                                                                                        <input type="text" class="form-control input-circle-right " id="endDate" name="to" style="text-align:left !important;"/>
                                                                                    </div>
                                                                                    <span class="error-message" id="enddateError" ></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-sm-4 control-label">System Name</label>
                                                                            <div class="col-sm-8">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon input-circle-left">
                                                                                        <i class="fa fa-desktop"></i>
                                                                                    </span>
                                                                                    <table style="width:100%;">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <select class="form-control input-circle-right selectpicker" id="systemId" data-live-search='true'>
                                                                                                    <option selected disabled>Select System</option>
                                                                                                </select>
                                                                                            </td>
                                                                                            <!--								<td style="width:40px; padding-left:10px;">
                                                                                                                                                                    <a href="#" class="btn btn-default btn-circle"><i class="fa fa-plus"></i></a>
                                                                                                                                                            </td>-->
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <span class="error-message" id="systemError" ></span>					
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="col-sm-4 control-label">System ID</label>
                                                                            <div class="col-sm-8">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon input-circle-left">
                                                                                        <i class="fa fa-desktop"></i>
                                                                                    </span>
                                                                                    <input type="text" class="form-control input-circle-right" id="systemid"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="col-sm-4 control-label">Watermark</label>
                                                                            <div class="col-sm-8">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon input-circle-left">
                                                                                        <i class="icon-frame"></i>
                                                                                    </span>
                                                                                    <select class="form-control input-circle-right selectpicker"  id="watermarkId" data-live-search='true'>
                                                                                        <c:forEach items="${watermarks}" var="watermark">
                                                                                            <option value="${watermark.watermarkId}">${watermark.watermarkName}</option>
                                                                                        </c:forEach>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div id="loading" style="display: none;">
                                                                            <center>
                                                                                <!--<span style="color: blue;">Processing...</span>-->
                                                                                <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/24.gif" />
                                                                            </center>
                                                                        </div>
                                                                        <span class="error-message" id="responseError" style="margin-left: 185px;" ></span>		
                                                                    </div>

                                                                </div>
                                                                <div class="tab-pane" id="tab4">
                                                                    <div  class="form-horizontal col-sm-6" style="margin:0 auto; float:none;">
                                                                        <div class="form-group">
                                                                            <input type="hidden" id="licenseId" />
                                                                            <label class="control-label col-md-4">User ID:</label>
                                                                            <div class="col-md-8">
                                                                                <p class="form-control-static" data-display="userid" id="Confirmuserid"> </p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-4">Username:</label>
                                                                            <div class="col-md-8">
                                                                                <p class="form-control-static" data-display="username" id="Confirmusername"> </p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-4">Course Name:</label>
                                                                            <div class="col-md-8">
                                                                                <p class="form-control-static" data-display="coursename" id="Confirmcoursename"> </p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-4">Validity:</label>
                                                                            <div class="col-md-8">
                                                                                <p class="form-control-static" data-display="validity" id="Confirmvalidity"> </p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-4">License Key:</label>
                                                                            <div class="col-md-8">
                                                                                <p class="form-control-static text-ellips" id="licenceKey"> </p>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-4">Country:</label>
                                                                            <div class="col-md-8">
                                                                                <p class="form-control-static" data-display="country" id="Confirmcountry"> </p>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <span style="color: green; display: block; padding-left: 34px; font-size: 14px;" id="responsemessage" ></span>
                                                            <span style="color: red; display: block; padding-left: 34px; font-size: 14px;" id="errorresponse" ></span>
                                                            <div class="row">
                                                                <div class=" col-md-12">
                                                                    <a href="javascript:;" class="btn btn-circle btn-danger button-previous" id="previoustab"> <i class="fa fa-angle-left"></i> Back </a> 
                                                                    <a href="javascript:;" class="btn btn-circle btn-success button-next" id="continuebutton"> Continue <i class="fa fa-angle-right"></i> </a> 
                                                                    <a href="javascript:;" class="btn btn-outline green btn-modal btn-circle" data-toggle="modal" data-target="#otpModal" id="generateotp"> Continue <i class="fa fa-angle-right"></i> </a> 
                                                                    <!--<a href="${pageContext.request.contextPath}/${user.userName}/viewlicense" class="btn green button-submit btn-circle"> Submit <i class="fa fa-check"></i> </a>--> 
                                                                    <div id="actiondiv" style="display: none;">
                                                                        <div id="loading1" style="display: none;">
                                                                            <center>
                                                                                <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/24.gif" />
                                                                            </center>
                                                                        </div>
                                                                        <a href="javascript:;" class="btn btn-success btn-circle " data-toggle="tooltip" data-placement="top" title="Download Key" id="keydownload" ><i class="fa fa-download"></i></a>
                                                                        <a href="javascript:;" class="btn btn-default btn-circle" data-toggle="tooltip" data-placement="top" title="Copy" id="copy"><i class="fa fa-copy"></i></a>
                                                                        <a href="javascript:;" class="btn btn-default btn-circle" data-toggle="tooltip" data-placement="top" title="Send Email" id="keytomail"><i class="fa fa-envelope"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PROFILE CONTENT --> 
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY --> 
            </div>
            <!-- END CONTENT --> 
            <!-- BEGIN QUICK SIDEBAR --> 
            <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 

            <!-- END QUICK SIDEBAR --> 
        </div>
        <!-- END CONTAINER --> 
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; Organization </div>
            <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
        </div>
        <!-- END FOOTER --> 

        <!-- Modal -->
        <div class="modal fade " id="otpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm text-center" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Send OTP</h4>
                    </div>
                    <div class="modal-body">
                        <div class="mt-radio-inline">
                            <label class="mt-radio">
                                <input type="radio" name="sendotp" class="radiovalue" value="1" checked=""> Email
                                <span></span>
                            </label>
                            <label class="mt-radio">
                                <input type="radio" name="sendotp" class="radiovalue" value="2" > Mobile
                                <span></span>
                            </label>			
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align:center;">
                        <button type="button" class="btn btn-success btn-circle" data-dismiss="modal" data-toggle="modal" data-target="" id="createlicense"> Send <i class="fa fa-angle-right"></i> </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade " id="otpModal_text" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm text-center" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Confirm OTP</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="random"/>
                        <input type="text" class="form-control input-circle" id="otp"/>
                        <span class="error-message" id="otpError" ></span>
                    </div>
                    <div class="modal-footer" style="text-align:center;">
                        <button type="button" class="btn btn-success btn-circle otp_btn" id="submitotp"> Continue <i class="fa fa-angle-right"></i> </button>
                        <button class="btn btn-default btn-circle otp_btn" type="button" id="resendotp">Resend OTP</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- BEGIN CORE PLUGINS --> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
        <script type="text/javascript" src="${pageContext.request.contextPath}/assets/global/plugins/jQuery-Autocomplete-master/scripts/jquery.mockjax.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/assets/global/plugins/jQuery-Autocomplete-master/src/jquery.autocomplete.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/assets/global/plugins/jQuery-Autocomplete-master/scripts/countries.js"></script>
        <!--<script type="text/javascript" src="${pageContext.request.contextPath}/assets/global/plugins/jQuery-Autocomplete-master/scripts/demo.js"></script>-->
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/scripts/bootstrap-select.min.js" type="text/javascript"></script> 

        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
            var observeDOM = (function () {
                var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
                        eventListenerSupported = window.addEventListener;
                return function (obj, callback) {
                    if (MutationObserver) {
                        // define a new observer
                        var obs = new MutationObserver(function (mutations, observer) {
                            callback();
                        });
                        // have the observer observe foo for changes in children
                        obs.observe(obj, {attributes: true});
                    } else if (eventListenerSupported) {
                        obj.addEventListener('DOMNodeInserted', callback, false);
                        obj.addEventListener('DOMNodeRemoved', callback, false);
                    }
                };
            });

        // Observe a specific DOM element:
            observeDOM(document.getElementById('tab3'), function () {
                $('.btn-modal').css('display', 'inline-block');
                $('.button-next').css('display', 'none');
            });
            $('.otp_btn').on('click', function () {
                $('.btn-modal').css('display', 'none');
                $('.button-next').css('display', 'inline-block');
            });
        </script>

        <script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
        <!-- END CORE PLUGINS --> 
        <!-- BEGIN PAGE LEVEL PLUGINS --> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js" type="text/javascript"></script> 
        <!-- END PAGE LEVEL PLUGINS --> 

        <!-- BEGIN PAGE LEVEL SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/pages/scripts/form-wizard.js" type="text/javascript"></script> 
        <!-- END PAGE LEVEL SCRIPTS --> 
        <!-- BEGIN THEME GLOBAL SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
        <!-- END THEME GLOBAL SCRIPTS --> 
        <!-- BEGIN THEME LAYOUT SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
        <!-- END THEME LAYOUT SCRIPTS --> 
        <!-- BEGIN PAGE LEVEL PLUGINS --> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script> 
        <!-- END PAGE LEVEL PLUGINS --> 


        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS --> 
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="${pageContext.request.contextPath}/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/clipboard/clipboard.min.js"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- <script src="../assets/pages/scripts/profile.min.js" type="text/javascript"></script>-->

        <script>
    $("#getinfobyname").on("click", function () {
        if ($("#getinfobyname").is(":checked")) {
//                    $("#dropdown").css("display", "block");
//                    $("#inputbox").css("display", "none");
            $("#searchdropdown").hide();
            $("#userid").val("");
            var Uname = "${user.userName}".replace(" ", "");
            $.post("${pageContext.request.contextPath}/" + Uname + "/getdropdownuserinfo", {selectBy: 1},
                    function (data, success) {
                        console.log(data);
                        $("#displayuserdetails").hide();
                        var dropdownList = "<option selected disabled>Select user By User Name</option>";
                        if (data.indexOf("success") !== -1) {
                            obj = JSON.parse(data.split("#")[1]);
                            var shareInfoLen = obj.length;
                            for (var i = 0; i < shareInfoLen; i++) {
                                //                            console.log( obj[i].userId +"," + obj[i].userName);
                                dropdownList += '<option value="' + obj[i].userId + '">' + obj[i].userName + '</option>';
                            }
                            console.log(dropdownList);
                            $("#useruserid").html(dropdownList);
                            $("#useruserid").selectpicker('refresh');
                        } else {
                            $("#userError").text(data);
                        }
                    });
        } else {
            $("#useruserid").html("<option selected disabled>--Select user--</option>");
            $("#useruserid").selectpicker('refresh');
            $("#displayuserdetails").hide();
        }
    });
    $("#getinfobyid").on("click", function () {
        if ($("#getinfobyid").is(":checked")) {
//                    $("#dropdown").css("display", "block");
//                    $("#inputbox").css("display", "none");
            $("#searchdropdown").hide();
            $("#userid").val("");
            var Uname = "${user.userName}".replace(" ", "");
            $.post("${pageContext.request.contextPath}/" + Uname + "/getdropdownuserinfo", {selectBy: 4},
                    function (data, success) {
                        console.log(data);
                        $("#displayuserdetails").hide();
                        var dropdownList = "<option selected disabled>Select user By User ID</option>";
                        if (data.indexOf("success") !== -1) {
                            obj = JSON.parse(data.split("#")[1]);
                            var shareInfoLen = obj.length;
                            for (var i = 0; i < shareInfoLen; i++) {
                                //                            console.log( obj[i].userId +"," + obj[i].userName);
                                dropdownList += '<option value="' + obj[i].userId + '">' + obj[i].displayId + '</option>';
                            }
                            console.log(dropdownList);
                            $("#useruserid").html(dropdownList);
                            $("#useruserid").selectpicker('refresh');
                        } else {
                            $("#userError").text(data);
                        }
                    });
        } else {
            $("#useruserid").html("<option selected disabled>--Select user--</option>");
            $("#useruserid").selectpicker('refresh');
            $("#displayuserdetails").hide();
        }
    });
    $("#getinfobyemail").on("click", function () {
        if ($("#getinfobyemail").is(":checked")) {
//                $("#dropdown").css("display", "block");
//                $("#inputbox").css("display", "none");
            $("#searchdropdown").hide();
            $("#userid").val("");
            var Uname = "${user.userName}".replace(" ", "");
            $.post("${pageContext.request.contextPath}/" + Uname + "/getdropdownuserinfo", {selectBy: 2},
                    function (data, success) {
                        console.log(data);
                        $("#displayuserdetails").hide();
                        var dropdownList = "<option value=''>Select user By Email Address</option>";
                        if (data.indexOf("success") !== -1) {
                            obj = JSON.parse(data.split("#")[1]);
                            var shareInfoLen = obj.length;
                            for (var i = 0; i < shareInfoLen; i++) {
//                            console.log( obj[i].userId +"," + obj[i].userName);
                                dropdownList += '<option value="' + obj[i].userId + '">' + obj[i].emailId + '</option>';
                            }
                            console.log(dropdownList);
                            $("#useruserid").html(dropdownList);
                            $("#useruserid").selectpicker('refresh');
                        } else {
                            $("#userError").text(data);
                        }
                    });
        } else {
            $("#useruserid").html("<option value=''>---Select user---</option>");
            $("#useruserid").selectpicker('refresh');
            $("#displayuserdetails").hide();
        }
    });
    $("#getinfobymobile").on("click", function () {
        if ($("#getinfobymobile").is(":checked")) {
//                $("#dropdown").css("display", "block");
//                $("#inputbox").css("display", "none");
            $("#searchdropdown").hide();
            $("#userid").val("");
            var Uname = "${user.userName}".replace(" ", "");
            $.post("${pageContext.request.contextPath}/" + Uname + "/getdropdownuserinfo", {selectBy: 3},
                    function (data, success) {
                        console.log(data);
                        $("#displayuserdetails").hide();
                        var dropdownList = "<option value=''>Select user By Mobile Number</option>";
                        if (data.indexOf("success") !== -1) {
                            obj = JSON.parse(data.split("#")[1]);
                            var shareInfoLen = obj.length;
                            for (var i = 0; i < shareInfoLen; i++) {
//                            console.log( obj[i].userId +"," + obj[i].userName);
                                dropdownList += '<option value="' + obj[i].userId + '">' + obj[i].mobileNumber + '</option>';
                            }
                            console.log(dropdownList);
                            $("#useruserid").html(dropdownList);
                            $("#useruserid").selectpicker('refresh');
                        } else {
                            $("#userError").text(data);
                        }
                    });
        } else {
            $("#useruserid").html("<option value=''>---Select user---</option>");
            $("#useruserid").selectpicker('refresh');
            $("#displayuserdetails").hide();
        }
    });

    $("#getinfobyname").on("click", function () {
        if ($("#getinfobyname").is(":checked")) {
            $("#getinfobyemail").attr('checked', false);
            $("#getinfobymobile").attr('checked', false);
            $("#getinfobyid").attr('checked', false);
        }
    });
    $("#getinfobyemail").on("click", function () {
        if ($("#getinfobyemail").is(":checked")) {
            $("#getinfobyname").attr('checked', false);
            $("#getinfobymobile").attr('checked', false);
            $("#getinfobyid").attr('checked', false);
        }
    });
    $("#getinfobymobile").on("click", function () {
        if ($("#getinfobymobile").is(":checked")) {
            $("#getinfobyemail").attr('checked', false);
            $("#getinfobyname").attr('checked', false);
            $("#getinfobyid").attr('checked', false);
        }
    });
    $("#getinfobyid").on("click", function () {
        if ($("#getinfobyid").is(":checked")) {
            $("#getinfobyemail").attr('checked', false);
            $("#getinfobyname").attr('checked', false);
            $("#getinfobymobile").attr('checked', false);
        }
    });

        </script>
        <script>
            $("#courseid").on("change", function () {
                var course = $("#courseid").val();
                console.log(course);
                var Uname = "${user.userName}".replace(" ", "");
                $.post("${pageContext.request.contextPath}/" + Uname + "/getcoursedetails", {courseid: course},
                        function (data, success) {
                            console.log(data);
                            if (data.indexOf("success") > -1) {
                                var jsondata = JSON.parse(data.split("#")[1]);
                                $("#displaycourseid").text(jsondata.courseId);
                                $("#displaytotalused").text(jsondata.totalUsedCourse);
                                $("#displaycoursedetails").text(jsondata.courseDescription);
                                $("#selectedcourse").val(jsondata.courseId);
                                $("#showcoursedetail").show();
                                $("#courseError").text("");
                            } else {
                                $("#courseError").text(data);
                            }
                        });
            });
        </script>
        <script>
            $("#useruserid").on("change", function () {
                var user = $("#useruserid").val();
                var Uname = "${user.userName}".replace(" ", "");
                $.post("${pageContext.request.contextPath}/" + Uname + "/getuserinfofordisplay", {userId: user},
                        function (data, success) {
                            console.log(data);
                            if (data.indexOf("success") > -1) {
                                var jsondata = JSON.parse(data.split("#")[1]);
                                $("#displayuserid").text(jsondata.userId);
                                $("#displayname").text(jsondata.Name);
                                $("#displayemail").text(jsondata.Email);
                                $("#displaycountry").text(jsondata.Country);
                                $("#displaymobilenumber").text(jsondata.Mobile);
                                $("#selecteduser").val(jsondata.Name);
                                $("#displayuserdetails").show();
                                $("#systemid").val("");
                                $("#userError").text("");
                            } else {
                                $("#userError").text(data);
                            }
                        });
                $.post("${pageContext.request.contextPath}/" + Uname + "/getusersystem",
                        {userId: user},
                        function (data, status) {
                            $("#systemId").html("<option value=\"\">Please select a system.</option>");
                            if (data.indexOf("success") !== -1) {
                                $("#systemId").append(data.split("#")[1]);
                                $("#systemId").selectpicker('refresh');
                            } else {
                                $("#systemError").text(data);
                            }
                        });
            });

            $(document).on("change", "#validity", function () {
                var valid = $("#validity").val();
                var Uname = "${user.userName}".replace(" ", "");
                $.get("${pageContext.request.contextPath}/" + Uname + "/dates",
                        {validity: valid}, function (data, status) {
                    if (data.indexOf("success") > -1) {
                        var abc = data.split("#")[1];

                        $("#startDate").val(abc.split("*")[0]);
                        $("#endDate").val(abc.split("*")[1]);
                    }
                });
            });

//            $(document).on("change","#startDate", function(){
//                var issuedate = $("#startDate").val();
//                //disabling past date from datepicker
//                var nowDate = new Date();
//                var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
//                //initializing datepicker
//                $('#endDate').datepicker({format:'dd-mm-yyyy', startDate: issuedate });
//            });
//            $("#endDate").on("change", function(){
//                 var issuedate = $('#startDate').val();
//                 var expirydate = $("#endDate").val();
//                 if(expirydate < issuedate){
//                     alert("egiygdeywi");
//                 }
//            });

            var issuedate = $('#startDate').datepicker({
                format: "dd-mm-yyyy"
            });
            var endDate = $('#endDate').datepicker({
                format: "dd-mm-yyyy"
            });

            issuedate.on('changeDate', function (e) {
                $('#endDate').val("");
                console.log(e);
                endDate.datepicker('setStartDate', e.date);
            }).datepicker("setDate", new Date());

//            $("#startDate").on("change", function () {
//                var issuedate = $('#startDate').val();
//                $('#endDate').datepicker("option", "minDate", issuedate);
//            });

            $("#systemId").on("change", function () {
                var systemId = $("#systemId").val();
                var Uname = "${user.userName}".replace(" ", "");
                $.post("${pageContext.request.contextPath}/" + Uname + "/getsystemid",
                        {systemInfoId: systemId},
                        function (data, status) {
                            if (data.indexOf("success") > -1) {
                                $("#systemid").val(data.split("#")[1]);
                            } else {
                                $("#systemError").text(data);
                            }
                        });
            });
        </script>
        <script>
            $("#previoustab").on("click", function (e) {
                $("#generateotp").hide();
            });
            $("#continuebutton").on("click", function (e) {
                if ($("#tab1").is(".active")) {
                    var user = $("#useruserid").val();
                    $("#generateotp").hide();
                    if (user === null || user === "") {
                        $("#userError").text("Please Select a User.");
                        e.stopImmediatePropagation();
                        return false;
                    } else {
                        $("#userError").text("");
                    }
                }
                if ($("#tab2").is(".active")) {
                    var course = $("#courseid").val();
                    if (course === null || course === "") {
                        $("#courseError").text("Please Select a Course.");
                        e.stopImmediatePropagation();
                        return false;
                    } else {
                        $("#courseError").text("");
//                            $("#continuebutton").hide();
//                            $("#generateotp").show();
                    }
                }
                if ($("#tab3").is(".active")) {
                    var lifetime = $("#lifetime").is(":checked");
                    var validity = $("#validity").val();
                    var startdate = $("#startDate").val();
                    var enddate = $("#endDate").val();
                    var systemid = $("#systemId").val();
                    var watermarkid = $("#watermarkId").val();
                    var flag = true;
                    console.log(startdate);
                    console.log(enddate);
                    if (!lifetime) {
                        if (startdate === null || startdate === "") {
                            $("#startdateError").text("Please Enter Issue Date");
                            flag = false;
                        } else {
                            $("#startdateError").text("");
                        }
                        if (enddate === null || enddate === "") {
                            $("#enddateError").text("Please Enter Expiry date");
                            flag = false;
                        } else {
                            $("#enddateError").text("");
                        }
                        if (startdate !== "" && enddate !== "") {
                            var stdate = startdate.replace("-", "").replace("-", "");
                            var nddate = enddate.replace("-", "").replace("-", "");
                            var newstdate = "";
                            var newnddate = "";
                            for (var i = stdate.length; i >= 0; i--) {
                                newstdate += stdate.charAt(i);
                            }
                            for (var i = nddate.length; i >= 0; i--) {
                                newnddate += nddate.charAt(i);
                            }
                            console.log(newstdate);
                            console.log(newnddate);
                            if (newnddate < newstdate) {
                                $("#enddateError").text("Expiry date is less then issue date.");
                                flag = false;
                            }
                        }
                    }
                    if (systemid === null || systemid === "") {
                        $("#systemError").text("Please Select System");
                        flag = false;
                    } else {
                        $("#systemError").text("");
                    }
                    if (watermarkid === null || watermarkid === "") {
                        $("#responseError").text("Please Select Watermark");
                        flag = false;
                    } else {
                        $("#responseError").text("");
                    }
                    if (flag) {
                        $("#otpModal").modal("show");
                        e.stopImmediatePropagation();
                        return false;
                    } else {
                        e.stopImmediatePropagation();
                        return false;
                    }

                }

                if ($("#tab4").is(".active")) {
//                        alert("tab4");
                }
            });

        </script>
        <script>
            $("#createlicense").on("click", function () {
                var useruserId = $("#useruserid").select().val();
                var courseId = $("#courseid").select().val();
                var lifetime = $("#lifetime").is(":checked");
                var validity = $("#validity").val();
                var startDate = $("#startDate").val();
                var endDate = $("#endDate").val();
                var systemId = $("#systemId").select().val();
                var watermarkId = $("#watermarkId").select().val();
                var radioValue = $("input[name='sendotp']:checked").val();
                $("#startdateError").text("");
                $("#enddateError").text("");
                $("#systemError").text("");
                $("#responseError").text("");
                if (validity === "" || validity === null) {
                    validity = 0;
                }
                console.log(startDate);
                console.log(endDate);
                $("#loading").css('display', 'block');
                var Uname = "${user.userName}".replace(" ", "");
                $.post("${pageContext.request.contextPath}/" + Uname + "/createuserlicense",
                        {userId: useruserId, courseId: courseId, lifetime: lifetime, validity: validity, startDate: startDate,
                            endDate: endDate, systemId: systemId, watermarkId: watermarkId, sendOtpOn: radioValue},
                        function (data, status) {
                            if (data === "Please login again.") {
                                $("#loading").css("display", "none");
                                window.location.href = "/";
                            } else if (data.indexOf("success") !== -1) {
                                $("#loading").css("display", "none");
                                $("#random").val(data.split("#")[1]);
                                $("#otpModal_text").modal("show");
//                                window.location.href = "${pageContext.request.contextPath}/${user.userName}/viewlicense";
                            } else {
                                $("#loading").css("display", "none");
                                $("#errorresponse").text(data);
                            }
                        });
            });

            $(document).on("click", "#submitotp", function () {
                var otp = $("#otp").val();
                var random = $("#random").val();
                if (otp === "") {
                    $("#otpError").text("Please Enter OTP");
                } else if (random === "") {
                    $("#otpError").text("Login Again");
                } else {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/" + Uname + "/varifyotp", {otp: otp, random: random},
                            function (data, status) {
                                if (data === "Please login again.") {
                                    window.location.href = "/";
                                } else if (data.indexOf("success") !== -1) {
                                    $("#otpModal_text").modal("hide");
                                    console.log(data);
                                    $("#licenceKey").text(data.split("#")[1]);
                                    $.post("${pageContext.request.contextPath}/" + Uname + "/confirmdisplayforlicense",
                                            function (data, success) {
                                                if (data.indexOf("success") > -1) {
                                                    var jsondata = JSON.parse(data.split("#")[1]);
                                                    $("#Confirmuserid").text(jsondata.userId);
                                                    $("#Confirmusername").text(jsondata.userName);
                                                    $("#Confirmcoursename").text(jsondata.courseName);
                                                    $("#Confirmvalidity").text(jsondata.validity);
                                                    $("#Confirmcountry").text(jsondata.country);
                                                    $("#licenseId").val(jsondata.licenseId);
                                                    console.log(jsondata.licenseId);
                                                    $(".steptab3").parent("li").removeClass("active").addClass("done");
                                                    $(".steptab4").parent("li").addClass("active");
                                                    $("#tab3").removeClass("active");
                                                    $("#tab4").addClass("active");
                                                    $("#previoustab").css("display", "none");
                                                    $("#continuebutton").css("display", "none");
                                                    $(".button-submit").css("display", "none");
                                                    $(".progress-bar-success").css("width", "100%");
                                                    $("#actiondiv").show();
                                                } else if (data.indexOf("Login Again") > -1) {
                                                    window.location.href = "/";
                                                } else {
                                                    console.log(data);
                                                }
                                            });
                                } else {
                                    console.log(data);
                                    $("#otpError").text(data);
                                }
                            });
                }
            });

            $(document).on("click", "#resendotp", function () {
                var radioValue = $("input[name='sendotp']:checked").val();
                var random = $("#random").val();
                if (random === "") {
                    $("#otpError").text("login again");
                    window.location.href = "/";
                } else {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/" + Uname + "/resendotp", {random: random, sendOtpOn: radioValue},
                            function (data, status) {
                                if (data === "Please login again.") {
                                    window.location.href = "/";
                                } else if (data.indexOf("success") !== -1) {
                                    $("#random").val(data.split("#")[1]);
                                } else {
                                    $("#otpError").text(data);
                                }
                            });
                }
            });
        </script>
        <script>
            var clipboard = new Clipboard('#copy', {
                text: function () {
                    console.log($("#licenceKey").text());
                    $("#responsemessage").text("license key copied");
                    return $("#licenceKey").text();
                }
            });
            clipboard.on('success', function (e) {
                console.log(e);
            });
            clipboard.on('error', function (e) {
                console.log(e);
            });

            $("#keytomail").on("click", function () {
                var license = $("#licenseId").val();
                console.log(license);
                var Uname = "${user.userName}".replace(" ", "");
                $("#loading1").css('display', 'block');
                $.post("${pageContext.request.contextPath}/" + Uname + "/sendmailtouserforlicense", {licenseId: license},
                        function (data, status) {
                            console.log(data);
                            if (data === "success") {
                                $("#loading1").css("display", "none");
                                $("#responsemessage").text("Mail Has Been Sent Successfully");
                            } else if (data === "Please login again.") {
                                $("#loading1").css("display", "none");
                                $("#responsemessage").val("");
                                window.location.href = "${pageContext.request.contextPath}/";
                            } else {
                                $("#loading1").css("display", "none");
                                $("#responsemessage").text(data);
                            }
                        });
            });

            $("#keydownload").on("click", function () {
                var license = $("#licenseId").val();
                console.log(license);
                if (license !== null || license !== "") {
                    var Uname = "${user.userName}".replace(" ", "");
                    window.location.href = "${pageContext.request.contextPath}/" + Uname + "/downloadlicensekey?licenseId=" + license;
                    $("#responsemessage").text("File Download Successful");
                }
            });

            $("#lifetime").on("click", function () {
                if ($("#lifetime").is(":checked")) {
                    $("#validity").val("");
                    $("#startDate").val("");
                    $("#endDate").val("");
                    $("#radiobuttons").attr("disabled", "disabled").off('click');
                    $("#startDate").prop("disabled", true);
                    $("#endDate").prop("disabled", true);
                    $(".btn-default").removeClass("active");
                } else {
                    $("#radiobuttons").prop("disabled", false);
                    $("#startDate").prop("disabled", false);
                    $("#endDate").prop("disabled", false);
                    $(".status").css("display", "none");
                }
            });

        </script>
        <script>
            $(document).ready(function () {
                $('#side-menu').find('li').removeClass('active');
                $(document).find(".license").addClass('active');

                var Uname = "${user.userName}".replace(" ", "");
                $.post("${pageContext.request.contextPath}/" + Uname + "/getdropdownuserinfo", {selectBy: 4},
                        function (data, success) {
                            console.log(data);
                            $("#displayuserdetails").hide();
                            var dropdownList = "<option selected disabled>Select user By User ID</option>";
                            if (data.indexOf("success") !== -1) {
                                obj = JSON.parse(data.split("#")[1]);
                                var shareInfoLen = obj.length;
                                for (var i = 0; i < shareInfoLen; i++) {
//                            console.log( obj[i].userId +"," + obj[i].userName);
                                    dropdownList += '<option value="' + obj[i].userId + '">' + obj[i].displayId + '</option>';
                                }
                                console.log(dropdownList);
                                $("#useruserid").html(dropdownList);
                                $("#useruserid").selectpicker('refresh');
                            } else {
                                $("#userError").text(data);
                            }
                        });
            });
        </script>
        <script>
            $(".number-dropdown label").click(function () {
                var selText = $(this).text();
                $(this).parents().find('.status').html(selText);
            });
            $(".valid").on("click", function () {
                $(".status").css("display", "block");
                var ID = this.id;
                if (ID !== null || ID !== "") {
                    $("#lifetime").attr('checked', false);
                    $("#startDate").prop("disabled", true);
                    $("#endDate").prop("disabled", true);
                    var Uname = "${user.userName}".replace(" ", "");
                    $.get("${pageContext.request.contextPath}/" + Uname + "/dates",
                            {validity: ID}, function (data, status) {
                        if (data.indexOf("success") > -1) {
                            var abc = data.split("#")[1];
                            $("#validity").val(ID);
                            $("#startDate").val(abc.split("*")[0]);
                            $("#endDate").val(abc.split("*")[1]);
                        }
                    });
                }
            });
        </script>
        <!--        <script>
                    $("#search").on("click", function(){
        //                $("#dropdown").css("display", "none");
        //                $("#inputbox").css("display", "block");
                        $("#displayuserdetails").hide();
                    });
                    
                    $("#userid").on("keyup", function(){
                        var user = $("#userid").val();
                        var name = $("#getinfobyname").is(":checked");
                        var email = $("#getinfobyemail").is(":checked");
                        var mobile = $("#getinfobymobile").is(":checked");
                        var Uname = "${user.userName}".replace(" ", "");
                        if(name){
                            $.post("${pageContext.request.contextPath}/"+Uname+"/searchusers", {match: user, type: 1},
                            function (data, status) {
                                console.log(data);  
                                if(data.indexOf("success") > -1){
                                    $("#searchdropdown").show();
                                    $("#searchdropdown").html(data);
                                } else if(data.indexOf("No") > -1){
                                    $("#searchdropdown").hide();
                                }
                                
                            }); 
                        }
                        if(email){
                            $.post("${pageContext.request.contextPath}/"+Uname+"/searchusers", {match: user, type: 2},
                            function (data, status) {
                                console.log(data);  
                                if(data.indexOf("success") > -1){
                                    $("#searchdropdown").show();
                                    $("#searchdropdown").html(data);
                                } else if(data.indexOf("No") > -1){
                                    $("#searchdropdown").hide();
                                }
                            });  
                        }
                        if(mobile){
                            $.post("${pageContext.request.contextPath}/"+Uname+"/searchusers", {match: user, type: 3},
                            function (data, status) {
                                console.log(data);
                                if(data.indexOf("success") > -1){
                                    $("#searchdropdown").show();
                                    $("#searchdropdown").html(data);
                                } else if(data.indexOf("No") > -1){
                                    $("#searchdropdown").hide();
                                }
                            }); 
                        }
                    });
                </script>-->
        <script>
            $(".form-control").focus(function () {
                $(this).prev('.input-group-addon').removeClass().addClass('input-group-addon-focus');
                $(this).next('.input-group-addon').removeClass().addClass('input-group-addon-focus');
            });

            $(".form-control").focusout(function () {
                $(this).prev('.input-group-addon-focus').removeClass().addClass('input-group-addon');
                $(this).next('.input-group-addon-focus').removeClass().addClass('input-group-addon');
            });
        </script>
    </body>
</html>
