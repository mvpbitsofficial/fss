/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.domain;

import java.io.Serializable;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author CoreTechies M8
 */
@Entity
@Table(name = "securityquestion")
@Access(AccessType.FIELD)
public class SecurityQuestion implements Serializable {
    
    @Id
    @GeneratedValue(generator = "securityquestion")
    @GenericGenerator(name = "securityquestion", strategy = "increment")
    private Integer securityQuestionId;
    
    @Column
    private String question;

    public Integer getSecurityQuestionId() {
        return securityQuestionId;
    }

    public void setSecurityQuestionId(Integer securityQuestionId) {
        this.securityQuestionId = securityQuestionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "SecurityQuestion{" + "securityQuestionId=" + securityQuestionId + ", question=" + question + '}';
    }
    
}
