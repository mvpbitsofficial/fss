/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.Course;
import java.util.List;

/**
 *
 * @author CoreTechies M8
 */
public interface CourseDao {

    public Course saveCourse(Course course);

    public List<Course> getCourses(Integer userId);

    public Course getCourse(Integer userInfoId, Integer id);

    public Course getCourse(Integer userInfoId, String courseName);

    public boolean deleteCourse(Integer id);

    public Course updateCourse(Course course);

    public List<Course> getCourseListwithCompleteDetail(Integer userId);
    
    public List<Course> getCourseList();
    
    public Course getCourseById(Integer id);
}
