<%-- 
    Document   : changepassword
    Created on : 9 Aug, 2016, 2:24:32 PM
    Author     : CoreTechies M8
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>FSS | Profile</title>
    </head>
    <body>
        <form action="#">
            <div>
                <input type="text" id="userName" placeholder="username"/>
                <br/>
                <label style="color: red;" id="errorMessageUserName"></label>
            </div>
            <div>
                <button type="button" id="submitUserName">Submit</button>
                <br/>
                <label style="color: red;" id="errorMessageSubmit"></label>
            </div>
        </form>
        <script src="${pageContext.request.contextPath}/js/jquery-1.10.2.min.js"></script>
        <script>
            $(document).on("click", "#submitUserName", function () {
                var userName = $("#userName").val();
                var flag = true;
                if (userName === null || userName === "") {
                    $("#errorMessageUserName").text("Please enter user name.");
                    flag = false;
                }
                if (flag) {
                    $.post("${pageContext.request.contextPath}/forgotpasswordusername", {userName: userName},
                            function (data, status) {
                                if (data === "success") {
                                    window.location.href="${pageContext.request.contextPath}/forgotpasswordquestion";
                                } else if (data === "Please login again.") {
                                    window.location.href = "${pageContext.request.contextPath}/";
                                } else {
                                    $("#errorMessageSubmit").text(data);
                                }
                            }
                    );
                }
            });
            $(document).on("keyup", "#userName", function () {
                var firstName = $("#userName").val();
                if (firstName === "") {
                    $("#errorMessageUserName").text("Please Enter user Name.");
                } else {
                    $("#errorMessageUserName").text("");
                }
            });
        </script>
    </body>
</html>
