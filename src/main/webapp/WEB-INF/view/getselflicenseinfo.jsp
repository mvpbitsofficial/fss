<%-- 
    Document   : getlicenseinfo
    Created on : 20 Aug, 2016, 10:44:00 AM
    Author     : CoreTechies M8
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>FSS | License Info</title>
    </head>
    <body>
        <h1>Get License Info!</h1>
        <div>
            <label >${license.userId}</label>
                <br/>
            <label >${license.userName}</label>
                <br/>
            <label >${license.organization}</label>
                <br/>
            <label >${license.validity}</label>
                <br/>
            <label >${license.licenseLimit}</label>
                <br/>
            <label >${license.systemId}</label>
        </div>
    </body>
</html>
