/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.controller;

import com.coretechies.filesecuritysystem.algorithm.UserStatus;
import com.coretechies.filesecuritysystem.algorithm.UserType;
import com.coretechies.filesecuritysystem.dao.AdminLicenseDao;
import com.coretechies.filesecuritysystem.dao.CountryDao;
import com.coretechies.filesecuritysystem.dao.NotificationDao;
import com.coretechies.filesecuritysystem.dao.UserInfoDao;
import com.coretechies.filesecuritysystem.domain.AdminLicense;
import com.coretechies.filesecuritysystem.domain.SystemInfo;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.domain.UserLicense;
import com.coretechies.filesecuritysystem.security.InvalidInformationException;
import com.coretechies.filesecuritysystem.security.RandomPasswordGenerator;
import com.coretechies.filesecuritysystem.security.SystemInformation;
import com.coretechies.filesecuritysystem.service.AdminLicenseService;
import com.coretechies.filesecuritysystem.service.CourseService;
import com.coretechies.filesecuritysystem.service.SystemInfoService;
import com.coretechies.filesecuritysystem.service.UserInfoService;
import com.coretechies.filesecuritysystem.service.UserLicenseService;
import com.coretechies.filesecuritysystem.web.webdomain.CreateClientBean;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @Controller annotation represents that this is a java controller class.
 * The ClientController class implements an application that handles all the requests by owner,
 * like create a client, update client, delete a existing client, deactivate or activate clients,
 * view client informations.
 * 
 * @SessionAttributes annotation represents a user in session.
 * "user" is a object of userInfo class and have a complete information
 * of that user who is present in the session at that time.
 * 
 * @author Jaishree
 * @version 1.0
 * @since 2016-09-02
 */
@Controller
@SessionAttributes({"user"})
public class ClientController {

    /** The @Autowired annotation is auto wire the bean by matching data type.
     *  userInfoService, userInfoDao, countryDao and mailController are objects to use methods of these classes in our controller. 
     */
    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private UserInfoDao userInfoDao;

    @Autowired
    private CountryDao countryDao;

    @Autowired
    private MailController mailController;
    
    @Autowired
    private CourseService courseService;
    
    @Autowired
    private UserLicenseService userLicenseService;
    
    @Autowired
    private SystemInfoService systemInfoService;
     
    @Autowired
    private AdminLicenseService adminLicenseService;
    
    @Autowired
    private NotificationDao notificationDao;
    
    @Autowired
    private AdminLicenseDao adminLicenseDao;

    /**
     * @GetMapping annotation represents the url is mapped with get request, value contains the url name.
     * This method will check that if owner is in session then admin come to the "admin_users" page and the page will show a list of all the users created by the owner. 
     * 
     * @param userName
     * @param modelMap
     * @return on page admin_users
     *  
     */
    @GetMapping(value = "/{userName}/viewclient")
    public ModelAndView viewClient(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
//        System.out.println("session vala name"+ userInfo.getUserName().replace(" ", ""));
//        System.out.println("url vala name"+userName);
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                modelMap.addAttribute("users", userInfoDao.getUserInfoByUserType(UserType.Client.getUserTypeValue()));
                return new ModelAndView("admin_users");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    @GetMapping(value = "/{userName}/listview")
    public ModelAndView viewClientList(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
//        System.out.println("session vala name"+ userInfo.getUserName().replace(" ", ""));
//        System.out.println("url vala name"+userName);
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                List<UserInfo> userInfos = userInfoDao.getUserInfoByUserType(UserType.Client.getUserTypeValue());
                for (UserInfo userInfo1 : userInfos) {
                    Map<Integer, Integer> map = userInfoDao.showClientIdThroughCount();
                    for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                        if(entry.getKey() == userInfo1.getUserInfoId()){
                            userInfo1.setDisplayName("SA"+(1000 + entry.getValue()));
                        }  
                    }
                }
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                modelMap.addAttribute("users", userInfos);
                return new ModelAndView("viewclient");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }  
    }
    
    @GetMapping(value = "/{userName}/viewdetails/{userId}")
    public ModelAndView viewDetails(@PathVariable String userName, ModelMap modelMap, @PathVariable Integer userId) throws InvalidInformationException {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("courses", courseService.getCourseById(userId));
                UserInfo userInfo1 = userInfoDao.getUserInfo(userId);
                modelMap.addAttribute("userdetail", userInfo1);
                Map<Integer, Integer> map = userInfoDao.showClientIdThroughCount();
                Integer userid = 0;
                for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                    if(userId == entry.getKey()){
                        userid = entry.getValue();
                    }
                }
                modelMap.addAttribute("USERID", userid+1000);
                AdminLicense adminLicenses = adminLicenseService.getAdminLicenseByClient(userId);
                if(adminLicenses==null){
                    System.out.println(adminLicenses+"null");
                    modelMap.addAttribute("adminlicenses", adminLicenses);
                    modelMap.addAttribute("systemmacid", "");
                    modelMap.addAttribute("systemserialnumber", "");
                }else{
                    System.out.println("not null");
                    modelMap.addAttribute("adminlicenses", adminLicenses);
                    System.out.println(adminLicenses);
                    String systemInfo = new SystemInformation(adminLicenses.getSystemInfoId()).getSystemInfo();
                    String[] split = systemInfo.split("#");
                    JSONObject jSONObject = new JSONObject(split[1]);
                    System.out.println("MAC ID" + jSONObject.getString("macId"));
                    System.out.println("Serial Number" + jSONObject.getString("serialNumber"));
                    modelMap.addAttribute("systemmacid", jSONObject.getString("macId"));
                    modelMap.addAttribute("systemserialnumber", jSONObject.getString("serialNumber"));
                }
                
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                modelMap.addAttribute("totalCourse", courseService.getCourseCount(userId));
                System.out.println(userLicenseService.getUserLicenseCountByCreator(userId));
                modelMap.addAttribute("totalLicense", userLicenseService.getUserLicenseCountByCreator(userId));
                modelMap.addAttribute("totalSystem", systemInfoService.getSystemInfoCountByCreator(userId)); 
                return new ModelAndView("viewclientdetails");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }
    /**
     * This method will send the admin on the page createclient on the request to create a new client.
     * client and user can not access this page so if they are in session
     * this method will redirect the request to the url /dashboard.
     * 
     * users is a list of all the clients, set at the attribute to display at the next jsp page.
     * countries is a list, set at the attribute to display country code information at the next jsp page.
     * 
     * @param userName
     * @param createclientbean
     * @param modelMap
     * @return on page createclient 
     */
    @GetMapping(value = "/{userName}/createclient")
    public ModelAndView getCreateClient(@PathVariable String userName, @ModelAttribute("createclientbean") CreateClientBean createclientbean, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
//        System.out.println("session vala name="+userInfo.getUserName().replace(" ", ""));
//        System.out.println("url vala name="+userName);
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("users", userInfoDao.getUserInfoByUserType(UserType.Client.getUserTypeValue()));
                modelMap.addAttribute("countries", countryDao.getCountryCodes());
                System.out.println("asd : " + userInfo);
                Map<Integer, Integer> map = userInfoDao.showClientIdThroughCount();
                Integer value = 0;
                for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                    value = entry.getValue();
                }
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                modelMap.addAttribute("USERID", 1000+value+1);
                return new ModelAndView("createclient");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * creatclient page containing the form to create a new client and submit button will send
     * the request to this url with a postMapping, parameters of method have the information to create a new client.
     * if the information filled in the form is correct, the client is added to the database and
     * client will get a mail having username and password to login in system.
     * after successful creation the request is send to the /viewclient url with a proper message 
     * 
     * @param userName
     * @param image
     * @param createclientbean
     * @param result
     * @param request
     * @param redirectAttributes 
     * @param modelMap
     * @return 
     * @throws java.security.NoSuchAlgorithmException 
     */
    @RequestMapping(value = "/{userName}/createclient", method = RequestMethod.POST)
    public ModelAndView createClientpost(@PathVariable String userName,
            @RequestParam(name = "image", required = false) MultipartFile image,
            @Valid @ModelAttribute("createclientbean") CreateClientBean createclientbean,
            BindingResult result,
            HttpServletRequest request,
            RedirectAttributes redirectAttributes,
            ModelMap modelMap) throws NoSuchAlgorithmException {

        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            System.out.println("createclientbean : " + createclientbean.toString());
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                UserInfo userInfo1 = userInfoDao.getUserInfo(createclientbean.getNewUserName());
                if (userInfo1 != null) {
                    result.addError(new FieldError("createclient", "newUserName", "This user name already in use please choose another username."));
                }
//                UserInfo userInfo2 = userInfoDao.getUserInfo(createclientbean.getEmailId());
//                if (userInfo2 != null) {
//                    result.addError(new FieldError("createclient", "emailId", "This emailId already in use please choose another emailId."));
//                }

                if (result.hasErrors()) {
                    modelMap.addAttribute("users", userInfoDao.getUserInfoByUserType(UserType.Client.getUserTypeValue()));
                    modelMap.addAttribute("countries", countryDao.getCountryCodes());
                    Map<Integer, Integer> map = userInfoDao.showClientIdThroughCount();
                    Integer value = 0;
                    for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                        value = entry.getValue();
                    }
                    modelMap.addAttribute("USERID", 1000+value+1);
                    return new ModelAndView("createclient");
                }

                String profilePic = null;
                if (image != null && !image.isEmpty()) {
                    try {
                        ServletContext servletContext = request.getSession().getServletContext();
                        String profilePicturePath = servletContext.getRealPath("/");
                        System.out.println("first path:::" + profilePicturePath);
                        profilePicturePath = profilePicturePath.replace("\\", "/");
                        File profilePictureFile = new File(profilePicturePath, "profilepic");
                        if (!profilePictureFile.exists()) {
                            profilePictureFile.mkdir();
                        }
                        System.out.println("second path:::" + profilePicturePath);
                        profilePic = image.getName() + "_" + new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(new Date()) + "_" + userName + "." + image.getOriginalFilename().substring(image.getOriginalFilename().lastIndexOf(".") + 1);
                        System.out.println("asd: " + profilePic);
                        Path imagePath = Paths.get(profilePictureFile.getAbsolutePath(), profilePic);
                        Files.copy(image.getInputStream(), imagePath);
                        profilePic = imagePath.toString();
                        profilePic = profilePic.replace("\\", "/");
                        profilePic = profilePic.substring(profilePic.indexOf("/profilepic"));
                    } catch (Exception ex) {
                        Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                RandomPasswordGenerator generator = new RandomPasswordGenerator();
                String password = String.copyValueOf(generator.generatePswd(5, 10, 2, 2, 1));
                createclientbean.setMobileNumberCode(createclientbean.getCountry()); 
                if (userInfoService.createClientUser(createclientbean.getNewUserName(), password, createclientbean.getEmailId(), profilePic, createclientbean.getCountry(), createclientbean.getMobileNumberCode(), createclientbean.getMobileNumber(), UserType.Client.getUserTypeValue(), createclientbean.getCompany(), UserStatus.Activated.getUserStatusValue(), userInfo.getUserInfoId()) != null) {
                    redirectAttributes.addFlashAttribute("message", "Client has been created successfully.");
                    return new ModelAndView("redirect:/" + userName + "/listview");
                } else {
                    redirectAttributes.addFlashAttribute("message", "Unable to create user.");
                    return new ModelAndView("redirect:/" + userName + "/createclient");
                }
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will call if the admin want to edit or update the information of any client.
     * This method will redirect the request to the page updateclient 
     * with the complete already stored information of that particular client.
     * 
     * @param userName
     * @param userId
     * @param updateclientbean
     * @param modelMap
     * @return on page updateclient
     */
    @GetMapping(value = "/{userName}/updateclient/{userId}")
    public ModelAndView getUpdateClient(@PathVariable String userName, @PathVariable Integer userId,
            @ModelAttribute("updateclientbean") CreateClientBean updateclientbean, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                System.out.println("TYPE====="+userInfo.getUserType());
                UserInfo userInfo1 = userInfoDao.getUserInfo(userId);
                updateclientbean.setUserId(userInfo1.getUserInfoId());
                updateclientbean.setCompany(userInfo1.getOrganization());
                updateclientbean.setCountry(userInfo1.getCountryCodeId());
                updateclientbean.setEmailId(userInfo1.getEmailId());
                updateclientbean.setMobileNumber(userInfo1.getMobileNumber());
                updateclientbean.setMobileNumberCode(userInfo1.getCountryCodeId());
                updateclientbean.setNewUserName(userInfo1.getUserName());
                if(userInfo1.getImagePath() == null){
                    modelMap.addAttribute("Profilepic", 0);
                }else{
                    modelMap.addAttribute("Profilepic", userInfo1.getImagePath());
                }
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                modelMap.addAttribute("Profilepic", userInfo1.getImagePath());
                modelMap.addAttribute("countries", countryDao.getCountryCodes());
                Map<Integer, Integer> map = userInfoDao.showClientIdThroughCount();
                Integer value = 0;
                for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                    if(entry.getKey() == userId){
                        value = entry.getValue();
                    } 
                }
                modelMap.addAttribute("USERID", 1000+value);
                return new ModelAndView("updateclient");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * parameters have the updated information of client and 
     * this method will update the information in database if the values are correct.
     * after successful update the request is redirect to the url /viewclient with a proper success message.
     * 
     * @param userName
     * @param userId
     * @param image
     * @param modelMap
     * @param updateclientbean
     * @param result
     * @param request
     * @param redirectAttributes
     * @return 
     * @throws java.security.NoSuchAlgorithmException 
     */
    @RequestMapping(value = "/{userName}/updateclient/{userId}", method = RequestMethod.POST)
    public ModelAndView updateClientpost(@PathVariable String userName, @PathVariable Integer userId, @RequestParam(name = "image", required = false) MultipartFile image, @Valid @ModelAttribute("updateclientbean") CreateClientBean updateclientbean, BindingResult result, HttpServletRequest request, RedirectAttributes redirectAttributes, ModelMap modelMap) throws NoSuchAlgorithmException {

        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {

                if (userInfoDao.getUserInfo(updateclientbean.getNewUserName(), updateclientbean.getUserId()) != null) {
                    result.addError(new FieldError("updateclientbean", "newUserName", "This user name already in use please choose another username."));
                }
//                if (userInfoDao.getUserInfo(updateclientbean.getEmailId(), updateclientbean.getUserId()) != null) {
//                    result.addError(new FieldError("updateclientbean", "emailId", "This emailId already in use please choose another emailId."));
//                }

                if (result.hasErrors()) {
                    modelMap.addAttribute("users", userInfoDao.getUserInfoByUserType(UserType.Client.getUserTypeValue()));
                    modelMap.addAttribute("countries", countryDao.getCountryCodes());
                    Map<Integer, Integer> map = userInfoDao.showClientIdThroughCount();
                    Integer value = 0;
                    for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                        if(entry.getKey() == updateclientbean.getUserId()){
                            value = entry.getValue();
                        } 
                    }
                    modelMap.addAttribute("USERID", 1000+value);
                        return new ModelAndView("updateclient");
                    }
                UserInfo userInfo1 = userInfoDao.getUserInfo(updateclientbean.getUserId());
                System.out.println("Image path="+image);
                String profilePic = null;
                if (image != null && !image.isEmpty()) {
                    try {
                        ServletContext servletContext = request.getSession().getServletContext();
                        String profilePicturePath = servletContext.getRealPath("/");
                        System.out.println("first path:::" + profilePicturePath);
                        profilePicturePath = profilePicturePath.replace("\\", "/");
                        File profilePictureFile = new File(profilePicturePath, "profilepic");
                        if (!profilePictureFile.exists()) {
                            profilePictureFile.mkdir();
                        }
                        System.out.println("second path:::" + profilePicturePath);
                        profilePic = image.getName() + "_" + new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(new Date()) + "_" + userName + "." + image.getOriginalFilename().substring(image.getOriginalFilename().lastIndexOf(".") + 1);
                        System.out.println("asd: " + profilePic);
                        Path imagePath = Paths.get(profilePictureFile.getAbsolutePath(), profilePic);
                        Files.copy(image.getInputStream(), imagePath);
                        profilePic = imagePath.toString();
                        profilePic = profilePic.replace("\\", "/");
                        profilePic = profilePic.substring(profilePic.indexOf("/profilepic"));
                    } catch (Exception ex) {
                        Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else{
                    System.out.println("DB image=="+userInfo1.getImagePath());
                    profilePic = userInfo1.getImagePath();
                }
                updateclientbean.setMobileNumberCode(updateclientbean.getCountry()); 
                if (updateclientbean.getUserId() == 0) {
                    redirectAttributes.addFlashAttribute("message", "Invalid access.");
                    return new ModelAndView("redirect:/" + userName + "/listview");
                } else if (userInfoService.updateClientUser(updateclientbean.getUserId(), updateclientbean.getNewUserName(), updateclientbean.getEmailId(), profilePic, updateclientbean.getCountry(), updateclientbean.getMobileNumberCode(), updateclientbean.getMobileNumber(), UserType.Client.getUserTypeValue(), updateclientbean.getCompany(), UserStatus.Activated.getUserStatusValue(), userInfo.getUserInfoId()) != null) {
                    mailController.sendEmail("Profile Updated",
                            "Your profile is updated.\n",
                            updateclientbean.getEmailId(), request, userInfo1);
                    redirectAttributes.addFlashAttribute("message", "Client has been updated successfully.");
                    return new ModelAndView("redirect:/" + userName + "/listview");
                } else {
                    redirectAttributes.addFlashAttribute("message", "Unable to update user.");
                    return new ModelAndView("redirect:/" + userName + "/updateclient/" + userId);
                }
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method is call at the request to activate a client,
     * Owner have the authority to activate or deactivate a client, 
     * A deactivated client can not operate his/her account.
     * 
     * @param userName
     * @param userId
     * @param comment
     * @param request
     * @param redirectAttributes
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/activateclient/{userId}")
    public ModelAndView activateClient(@PathVariable String userName, @PathVariable Integer userId, @RequestParam String comment, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                UserInfo userInfo1;
                if ((userInfo1 = userInfoService.activateClient(userId)) != null) {
                    mailController.sendEmail("Account Activated",
                            comment+"\n",
                            userInfo1.getEmailId(), request, userInfo1);
                    redirectAttributes.addFlashAttribute("message", "Client has been activated Successfully.");
                } else {
                    redirectAttributes.addFlashAttribute("message", "Unable to Activate user.");
                }
                return new ModelAndView("redirect:/" + userName + "/listview");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method is call at the request to deactivate a client,
     * Owner have the authority to activate or deactivate a client, 
     * A deactivated client can not operate his/her account.
     * 
     * @param userName
     * @param userId
     * @param comment
     * @param redirectAttributes
     * @param request
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/deactivateclient/{userId}")
    public ModelAndView deactivateClient(@PathVariable String userName, @PathVariable Integer userId, @RequestParam String comment, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                UserInfo userInfo1;
                if ((userInfo1 = userInfoService.deactivateClient(userId)) != null) {
                    mailController.sendEmail("Account Deactivated",
                            comment+"\n",
                            userInfo1.getEmailId(), request, userInfo1);
                    redirectAttributes.addFlashAttribute("message", "Client has been deactivated Successfully .");
                } else {
                    redirectAttributes.addFlashAttribute("message", "Unable to Deactivate user.");
                }
                return new ModelAndView("redirect:/" + userName + "/listview");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method simply delete a client.
     * And redirect the request to the url /viewclient with a proper success message.
     * 
     * @param userName
     * @param userId
     * @param redirectAttributes
     * @param request
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/deleteclient/{userId}")
    public ModelAndView deleteClient(@PathVariable String userName, @PathVariable Integer userId, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                UserInfo userInfo1;
                AdminLicense adminLicense = adminLicenseDao.getAdminLicenseByClient(userId);
                System.out.println("adminLicense"+adminLicense);
                if(adminLicense == null){
                   if ((userInfo1 = userInfoService.deleteUser(userId)) != null) {
                        mailController.sendEmail("Account Deleted",
                                "Your profile is Deleted.\n",
                                userInfo1.getEmailId(), request, userInfo1);
                        redirectAttributes.addFlashAttribute("message", "Client has been deleted Successfully.");
                    } else {
                        redirectAttributes.addFlashAttribute("message", "Unable to delete user.");
                    } 
                } else{
                    redirectAttributes.addFlashAttribute("message", "This Client can not delete, ID is in use.");
                }
                return new ModelAndView("redirect:/" + userName + "/listview");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }
    
    @GetMapping(value = "/{userName}/deleteallclient")
    @ResponseBody
    public String deleteAllClient(@PathVariable String userName, @RequestParam(value = "userIds[]") Integer[] userIds, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                UserInfo userInfo1;
                String skippeduser = "";
                String successdelete = "";
                for (Integer userId : userIds) {
                    System.out.println(userId);
                    userInfo1 = userInfoDao.getUserInfo(userId);
                    AdminLicense adminLicenseByClient = adminLicenseDao.getAdminLicenseByClient(userId);
                    System.out.println("userlicense"+adminLicenseByClient);
                    if(adminLicenseByClient == null){
                        if ((userInfo1 = userInfoService.deleteUser(userId)) != null) {
                            mailController.sendEmail("Account Deleted",
                                "Your profile is Deleted.\n",
                                userInfo1.getEmailId(), request, userInfo1);
                            successdelete = "Users has been deleted successfully.";
                        }
                    } else{
                        skippeduser += ","+userInfo1.getUserName(); 
                        System.out.println("usernames=="+skippeduser);
                    }
                }
                if(!skippeduser.equals("")){
                    System.out.println("usernames======="+skippeduser);
                    return "success#Unable to Delete user "+"\""+skippeduser.substring(1)+"\"";
                } else{
                    return "success#"+successdelete+".";
                }
            } else {
                return "Login again";
            }
        } else {
            return "Login again";
        }
    }
    
    @GetMapping(value = "/{userName}/joiningMessage")
    @ResponseBody
    public String displayJoiningMessage(@PathVariable String userName, ModelMap modelMap, @RequestParam Date date){
//        Object seconds = Math.floor((new Date() - date) / 1000);
//
//        var interval = Math.floor(seconds / 31536000);
//
//        if (interval > 1) {
//            return interval + " years";
//        }
//        interval = Math.floor(seconds / 2592000);
//        if (interval > 1) {
//            return interval + " months";
//        }
//        interval = Math.floor(seconds / 86400);
//        if (interval > 1) {
//            return interval + " days";
//        }
//        interval = Math.floor(seconds / 3600);
//        if (interval > 1) {
//            return interval + " hours";
//        }
//        interval = Math.floor(seconds / 60);
//        if (interval > 1) {
//            return interval + " minutes";
//        }
//        return Math.floor(seconds) + " seconds";
          return "abc";
    }
    @PostMapping(value = "/{userName}/getclientdetailsfordispaly")
    @ResponseBody
    public String getClientDetailsForDisplay(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer userId){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())) {
                UserInfo userInfo1 = userInfoDao.getUserInfo(userId);
                Map<Integer, Integer> map = userInfoDao.showClientIdThroughCount();
                Integer userid = 0;
                for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                    if(userId == entry.getKey()){
                       userid =  entry.getValue();
                    }
                }
                userid = userid + 1000;
                String UserId = "SA" + userid; 
                String Country = userInfo1.getCountryCode().getCountry();
                
                return "success#{\"userId\":\""+UserId+"\","
                            +"\"Name\":\""+userInfo1.getUserName()+"\","
                            +"\"Email\":\""+userInfo1.getEmailId()+ "\","
                        + "\"Country\":\""+Country+ "\","
                        + "\"Mobile\":\""+userInfo1.getMobileNumber()+ "\"}";
            } else {
                return "Unauthorised Access";
            }
        } else {
            return "Please Login Again";
        }
    }
    
    @PostMapping(value = "/{userName}/getowneremailId")
    @ResponseBody
    public String getOwnerEmailId(@PathVariable String userName, ModelMap modelMap){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserInfo userInfo1 = userInfoDao.getUserInfo(1);
                return "success#{\"Name\":\""+userInfo1.getUserName()+"\","
                            +"\"Email\":\""+userInfo1.getEmailId()+ "\"}";
            } else {
                return "Unauthorised Access";
            }
        } else {
            return "Please Login Again";
        }
    }
    
    @PostMapping(value = "/{userName}/sendmailtoowner")
    @ResponseBody
    public String sendMailToOwner(@PathVariable String userName, ModelMap modelMap, @RequestParam String subject, @RequestParam String message, HttpServletRequest request){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserInfo userInfo1 = userInfoDao.getUserInfo(1);
                Boolean sendMail = mailController.sendMail(subject, message, userInfo1.getEmailId(), request, userInfo.getUserInfoId());
                if(sendMail){
                    return "success";
                } else{
                    return "Your Mail Settings Are Incomplete";
                }
            } else {
                return "Unauthorised Access";
            }
        } else {
            return "Please Login Again";
        }
    }
}
