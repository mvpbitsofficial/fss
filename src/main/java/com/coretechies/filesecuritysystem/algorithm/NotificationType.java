/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.algorithm;

/**
 *
 * @author CoreTechies M8
 */
public enum NotificationType {

    License_History(1),
    
    Creation_History(2),
    
    Updation_History(3),
    
    Deletion_History(4),
    
    Course_History(5),
    
    Watermark_History(6),
    
    System_History(7),
    
    Course_Delete_History(8);

    private final Integer notificationTypeValue;

    private NotificationType(Integer notificationTypeValue) {
        this.notificationTypeValue = notificationTypeValue;
    }

    public Integer getNotificationTypeValue() {
        return notificationTypeValue;
    }

    public static NotificationType parse(Integer notificationTypeValue) {
        for (NotificationType notificationType : NotificationType.values()) {
            if (notificationType.getNotificationTypeValue().equals(notificationTypeValue)) {
                return notificationType;
            }
        }
        return null;
    }
}
