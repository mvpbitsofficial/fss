/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.service;

import com.coretechies.filesecuritysystem.algorithm.NotificationType;
import com.coretechies.filesecuritysystem.dao.AdminLicenseDao;
import com.coretechies.filesecuritysystem.dao.CourseDao;
import com.coretechies.filesecuritysystem.dao.NotificationDao;
import com.coretechies.filesecuritysystem.dao.SystemInfoDao;
import com.coretechies.filesecuritysystem.dao.UserInfoDao;
import com.coretechies.filesecuritysystem.dao.UserLicenseDao;
import com.coretechies.filesecuritysystem.dao.WatermarkDao;
import com.coretechies.filesecuritysystem.domain.AdminLicense;
import com.coretechies.filesecuritysystem.domain.Course;
import com.coretechies.filesecuritysystem.domain.Notification;
import com.coretechies.filesecuritysystem.domain.SystemInfo;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.domain.UserLicense;
import com.coretechies.filesecuritysystem.domain.Watermark;
import com.coretechies.filesecuritysystem.security.InvalidInformationException;
import com.coretechies.filesecuritysystem.security.UserLicenseInformation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserLicenseServiceImpl implements UserLicenseService {

    @Autowired
    private UserLicenseDao userLicenseDao;

    @Autowired
    private UserLicenseService userLicenseService;

    @Autowired
    private CourseDao courseDao;

    @Autowired
    private SystemInfoDao systemInfoDao;

    @Autowired
    private WatermarkDao watermarkDao;

    @Autowired
    private UserInfoDao userInfoDao;

    @Autowired
    private AdminLicenseDao adminLicenseDao;

    @Autowired
    private NotificationDao notificationDao;

    private final String SECURITY_KEY = "123";

    @Override
    public UserLicense getUserLicense(Integer createdBy, Integer useruserId, Integer courseId, Integer validity, Date startDate, Date endDate, Integer systemId, Integer watermarkId) throws InvalidInformationException {
        UserInfo userInfo = userInfoDao.getUserInfo(useruserId);
        UserLicenseInformation userLicenseInformation = new UserLicenseInformation(userInfo, courseDao.getCourse(createdBy, courseId),
                systemInfoDao.getSystemInfo(useruserId, systemId), watermarkDao.getWatermark(createdBy, watermarkId),
                validity, startDate, endDate, SECURITY_KEY, createdBy, adminLicenseDao.getAdminLicenseByClient(createdBy),
                userLicenseService.getUserLicenseCountByCreator(createdBy), new UserLicense());
        return userLicenseInformation.getUserLicense();
    }

    @Override
    public String saveUserLicense(UserLicense userLicense, Integer createdBy) {
        if(userLicense.getUserLicenseId() == null){
            userLicense.setStatus(Boolean.TRUE);
            if (userLicenseDao.saveUserLicense(userLicense) != null) {
                UserInfo userInfo = userInfoDao.getUserInfo(userLicense.getUserId());
                Notification notification = new Notification();
                notification.setCreatedBy(createdBy);
                notification.setNotificationMessage("Licence generated for " + userInfo.getUserName() + " User.");
                notification.setSeenby("0,");
                notification.setNotificationType(NotificationType.License_History.getNotificationTypeValue());
                notification.setCreateOn(new Date());
                notification.setUserId(userLicense.getUserId());
                notificationDao.saveNotification(notification);
                return "success#" + userLicense.getLicense();
            } else {
                return "Unexpected Error Occur.";
            }
        }
        else if(userLicenseDao.updateUserLicense(userLicense) != null){
            UserInfo userInfo = userInfoDao.getUserInfo(userLicense.getUserId());
                Notification notification = new Notification();
                notification.setCreatedBy(createdBy);
                notification.setNotificationMessage("Licence renew for " + userInfo.getUserName() + " User.");
                notification.setSeenby("0,");
                notification.setNotificationType(NotificationType.License_History.getNotificationTypeValue());
                notification.setCreateOn(new Date());
                notification.setUserId(userLicense.getUserId());
                notificationDao.saveNotification(notification);
            return "success#" + userLicense.getLicense();
        }
        else{
            return "unexpected error";
        }
    }

    @Override
    public List<UserLicense> getLicenseExpireAlert() {
        DateTime dateTime = new DateTime(new Date());
        System.out.println("service date===="+dateTime);
        return userLicenseDao.getLicenseExpireAlert(dateTime.plusDays(5).toDate());
    }

    @Override
    public Integer getUserLicenseCountByCreator(Integer createdBy) {
        System.out.println("CREATED BY=="+createdBy);
        List<UserLicense> userLicenses = userLicenseDao.getUserLicenseByCreator(createdBy);
        if (userLicenses != null) {
            System.out.println("COUNT===="+userLicenses.size());
            return userLicenses.size();
        } else {
            return 0;
        }
    }

    @Override
    public List<UserLicense> getUserLicenseListByCreateOn(Integer createdBy, Date fromDate, Date toDate) {
        return userLicenseDao.getUserLicenseListByCreateOn(createdBy, fromDate, toDate);
    }

    @Override
    public List<UserLicense> getUserLicenseListByExpiryDate(Integer createdBy, Date fromDate, Date toDate) {
        return userLicenseDao.getUserLicenseListByExpiryDate(createdBy, fromDate, toDate);
    }

    @Override
    public List<UserLicense> getUserLicenseByUser(Integer userId){
        return userLicenseDao.getUserLicenseByUser(userId);
    }
    
    @Override
    public List<UserLicense> getUserLicenseForChart(Integer createdBy, Date date){
        return userLicenseDao.getUserLicenseForChart(createdBy, date);
    }
    
    @Override
    public UserLicense getConfirmLicenseForDisplay(){
        return userLicenseDao.getConfirmLicenseForDisplay();
    }
    
    @Override
    public UserLicense renewUserLicense(UserInfo userInfo,Integer licenseId, Course course, SystemInfo systemInfo, Integer validity, Date startdate, Date endDate, Integer createdby, AdminLicense adminLicense) throws InvalidInformationException, ParseException{
        Integer createdLicense = getUserLicenseCountByCreator(createdby);
        System.out.println("LICENSE ID==" + licenseId );
        UserLicense userLicense = userLicenseDao.getUserLicense(licenseId);
        System.out.println("WATERMARK ID=="+userLicense.getWatermarkId());
        Watermark watermark = watermarkDao.getWatermarkForPreview(userLicense.getWatermarkId());
        
        UserLicenseInformation userLicenseInformation = new UserLicenseInformation(userInfo, course, systemInfo, watermark, validity, startdate, endDate, SECURITY_KEY, createdby, adminLicense, createdLicense, userLicense);
        return userLicenseInformation.getUserLicense();
    }
    
    @Override
    public Integer getTotalUsedCourseCount(Integer courseid){
        List<UserLicense> totalUsedCourseCount = userLicenseDao.getTotalUsedCourseCount(courseid);
        if (totalUsedCourseCount != null) {
            return totalUsedCourseCount.size();
        } else {
            return 0;
        }
    }
}
