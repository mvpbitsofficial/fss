
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<ul id="side-menu" class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
    <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
    <c:choose>
        <c:when test="${user.userType==1}">
            <li class="sidebar-toggler-wrapper hide"> 
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler"> <span></span> </div>
                <!-- END SIDEBAR TOGGLER BUTTON --> 
            </li>
            <c:set var="Uname" value="${user.userName}"/>
            <li class="nav-item dashboard active "> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/dashboard" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a>
            </li>
            <li class="nav-item user"> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/viewclient" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
            </li>
            <li class="nav-item license"> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/viewlicense" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">License</span>  </a>
            </li>
            <li class="nav-item report "> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/report" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
            <li class="nav-item history "> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/licensehistory" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
            <!--<li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>-->
            <!--<li class="nav-item  "> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/viewwatermark" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>-->
            <li class="nav-item setting "> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/setting" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> 
            </li>
        </c:when>
        <c:when test="${user.userType==2}">
            <c:set var="Uname" value="${user.userName}"/>
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper hide"> 
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler"> <span></span> </div>
                <!-- END SIDEBAR TOGGLER BUTTON --> 
            </li>
            <li class="nav-item dashboard active "> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/dashboard" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
            <li class="nav-item user "> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/viewuser" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>

            </li>
            <li class="nav-item license "> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/viewlicense" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">License</span>  </a>

            </li>
            <li class="nav-item course "> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/viewcourse" class="nav-link nav-toggle"> <i class="icon-book-open"></i> <span class="title">Courses</span></a>

            </li>
            <li class="nav-item system "> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/viewsystem" class="nav-link nav-toggle"> <i class="fa fa-desktop"></i> <span class="title">Systems</span></a>

            </li>
            <li class="nav-item report "> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/report" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
                <li class="nav-item history "> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/licensehistory" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
            <!--<li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>-->
            <li class="nav-item watermark "> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/viewwatermark" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
            <li class="nav-item setting "> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/setting" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> 
            </li>
        </c:when>
    </c:choose>
</ul>