

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>FSS | Create User</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/TextInputEffects/css/set1.css" rel="stylesheet" type="text/css" />

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top"> 
            <!-- BEGIN HEADER INNER -->
            <jsp:include page="topbar.jsp" />
            <!-- END HEADER INNER --> 
        </div>
        <!-- END HEADER --> 
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER --> 
        <!-- BEGIN CONTAINER -->
        <div class="page-container"> 
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper"> 
                <!-- BEGIN SIDEBAR --> 
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse"> 
                    <!-- BEGIN SIDEBAR MENU --> 
                    <jsp:include page="sidebar.jsp" />
                    <!--   <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                         DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
                        <li class="sidebar-toggler-wrapper hide"> 
                          BEGIN SIDEBAR TOGGLER BUTTON 
                         <div class="sidebar-toggler"> <span></span> </div>
                          END SIDEBAR TOGGLER BUTTON  
                        </li>
                        <li class="nav-item  "> <a href="index.html" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
                        <li class="nav-item  "> <a href="users.html" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
                         
                        </li>
                        <li class="nav-item  "> <a href="licence.html" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">Licence</span>  </a>
                         
                        </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
                         
                        </li>
                        <li class="nav-item  "> <a href="report.html" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
                        <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
                        <li class="nav-item  "> <a href="watermark.html" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
                        <li class="nav-item  "> <a href="settings.html" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
                       </ul>-->
                    <!-- END SIDEBAR MENU --> 
                    <!-- END SIDEBAR MENU --> 
                </div>
                <!-- END SIDEBAR --> 
            </div>
            <!-- END SIDEBAR --> 
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper"> 
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content"> 
                    <!-- BEGIN PAGE HEADER--> 

                    <!-- BEGIN PAGE TITLE-->
                    <!--<h3 class="page-title"> Licence 
                    <!-- <small>blank page layout</small>-- 
                   </h3>-->
                    <!-- END PAGE TITLE--> 
                    <!-- END PAGE HEADER-->


                    <div class="row">
                        <div class="col-sm-12">
                            <div class="portlet red box  portlet-datatable  ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-user "></i>
                                        <span class="caption-subject  sbold uppercase">Create User</span>
                                    </div>
                                    <!--<div class="actions">
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-refresh"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-printer"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-docs"></i>
                                        </a>
                                         
                                    </div>-->
                                </div>
                                <div class="portlet-body clearfix">
                                    <div class="col-sm-6 clearfix" style="float:none; margin:0 auto;">
                                        <form:form action="" modelAttribute="createuserbean" enctype="multipart/form-data" method="POST" class="form-horizontal">
                                            <div class="form-group clearfix">
                                                <table class="user-img-table">
                                                    <tr>
                                                        <!--                                                        <td>
                                                                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                                                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                                                                        <div>
                                                                                                                            <span class="btn default btn-file">
                                                                                                                                <span class="fileinput-new"> Select image </span>
                                                                                                                                <span class="fileinput-exists"> Change </span>
                                                                                                                                <input type="file" name="image" id="image"> </span>
                                                                                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </td>-->
                                                        <td>
                                                            <div>
                                                                <form:input path="userId" type="hidden" id="userId"/>
                                                            </div>
                                                            <div class="form-group clearfix">
                                                                <label class="col-sm-3 control-label">User Id</label>
                                                                <div class="col-sm-8">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                        <input type="text" class="form-control input-circle-right" readonly placeholder="User Id" value="AD${USERID}"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group clearfix">
                                                                <label class="col-sm-3 control-label">User Name</label>
                                                                <div class="col-sm-8">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                        <form:input type="text" class="form-control input-circle-right" id="newUserName" placeholder="Please Enter User Name" path="newUserName"/> 
                                                                    </div>
                                                                    <span class="error-message" id="errorusername" ></span>
                                                                    <form:errors path="newUserName"  style="color: red;"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group clearfix">
                                                                <label class="col-sm-3 control-label">Email</label>
                                                                <div class="col-sm-8">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-envelope"></i>
                                                                        </span>
                                                                        <form:input type="text" class="form-control input-circle-right" id="emailId" placeholder="Please Enter Email Address" path="emailId"/> 
                                                                    </div>
                                                                    <span class="error-message" id="erroremailid" ></span>
                                                                    <form:errors path="emailId"  style="color: red;"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group clearfix">
                                                                <label class="col-sm-3 control-label">Country</label>
                                                                <div class="col-sm-8">
                                                                    <div class="input-group" id="country_div">
                                                                        <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-flag"></i>
                                                                        </span>
                                                                        <form:select class="form-control input-circle-right selectpicker" data-live-search='true' data-live-search-style='startsWith' path="country" id="country">
                                                                            <option selected disabled>Select Country</option>
                                                                            <c:forEach items="${countries}" var="country">
                                                                                <option value="${country.countryCodeId}">${country.country}</option>
                                                                            </c:forEach>
                                                                        </form:select>
                                                                    </div>
                                                                    <span class="error-message" id="errorcountry" ></span>
                                                                    <form:errors path="country"  style="color: red;"/>
                                                                </div>

                                                            </div>
                                                            <div class="form-group clearfix">
                                                                <label class="col-sm-3 control-label">Mobile Number</label>
                                                                <div class="col-sm-8 mobile-number-input ">
                                                                    <div class="mobile-number" style="width: 100%;">
                                                                        <div class="code">
                                                                            <form:select class="form-control input-circle" path="mobileNumberCode" id="mobilecode">
                                                                                <option selected disabled>Code</option>
                                                                                <c:forEach items="${countries}" var="country">
                                                                                    <option value="${country.countryCodeId}" disabled>+${country.mobileCode}</option>
                                                                                </c:forEach>
                                                                            </form:select>
                                                                            <form:errors path="mobileNumberCode"  style="color: red;"/>
                                                                        </div>

                                                                        <div class="mobile-input-div">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon input-circle-left">
                                                                                    <i class="fa fa-phone"></i>
                                                                                </span>
                                                                                <form:input type=" text" class="form-control input-circle-right" id="mobileno" placeholder="Mobile Number " path="mobileNumber"/>
                                                                            </div>
                                                                            <span class="error-message" id="errormobileno" ></span>
                                                                            <form:errors path="mobileNumber"  style="color: red;"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-sm-3"></div>	
                                                                    <div class="col-sm-8 text-right">
                                                                        <form:button type="submit" class="btn btn-circle btn-success" id="createuser">Add User</form:button>
                                                                            <!--<a href="#" class="btn green">Create User</a>-->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                        </form:form>
                                    </div>
                                </div>
                            </div>
                        </div>               
                    </div>


                </div>
                <!-- END CONTENT BODY -->  
            </div>
            <!-- END CONTENT --> 
            <!-- BEGIN QUICK SIDEBAR --> 
            <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 

            <!-- END QUICK SIDEBAR --> 
        </div>
        <!-- END CONTAINER --> 
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; Organization </div>
            <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
        </div>
        <!-- END FOOTER --> 


        <!--Create User Modal 
        <div class="modal fade" id="create_user_modal" tabindex="-1" role="dialog" aria-labelledby="create_user_modalLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              
              <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <table class="create_user_modal_table">
                                <tr>
                                        <td>Company Name</td>
                                        <td>XYZ</td>
                                </tr>
                                <tr>
                                        <td>Mail ID</td>
                                        <td>XYZ@demo.com</td>
                                </tr>
                                <tr>
                                        <td>Login ID</td>
                                        <td>ABC@demo.com</td>
                                </tr>
                                <tr>
                                        <td>Password</td>
                                        <td>Z#1571a</td>
                                </tr>
                        </table>
              </div>
              <div class="modal-footer">
                <div class="pull-left">
                                <button type="button" class="btn btn-sm btn-default" title="Copy"><i class="fa fa-copy"></i></button>
                                <button type="button" class="btn btn-sm btn-default" title="Send Mail"><i class="icon-envelope-open"></i></button>
                        </div>
                <button type="button" class="btn  btn-sm btn-success">Login</button>
              </div>
            </div>
          </div>
        </div>-->


        <!-- BEGIN CORE PLUGINS --> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
        <!-- END CORE PLUGINS --> 
        <!-- BEGIN THEME GLOBAL SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
        <!-- END THEME GLOBAL SCRIPTS --> 
        <!-- BEGIN THEME LAYOUT SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
        <!-- END THEME LAYOUT SCRIPTS --> 
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- END PAGE LEVEL PLUGINS -->   
        <script src="${pageContext.request.contextPath}/assets/global/plugins/TextInputEffects/js/classie.js" type="text/javascript"></script>
        <!--<script src="${pageContext.request.contextPath}/assets/pages/scripts/profile.min.js" type="text/javascript"></script>-->
        <!--         <script>
                                (function() {
                                        // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
                                        if (!String.prototype.trim) {
                                                (function() {
                                                        // Make sure we trim BOM and NBSP
                                                        var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                                                        String.prototype.trim = function() {
                                                                return this.replace(rtrim, '');
                                                        };
                                                })();
                                        }
        
                                        [].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
                                                // in case the input is already filled..
                                                if( inputEl.value.trim() !== '' ) {
                                                        classie.add( inputEl.parentNode, 'input--filled' );
                                                }
        
                                                // events:
                                                inputEl.addEventListener( 'focus', onInputFocus );
                                                inputEl.addEventListener( 'blur', onInputBlur );
                                        } );
        
                                        function onInputFocus( ev ) {
                                                classie.add( ev.target.parentNode, 'input--filled' );
                                        }
        
                                        function onInputBlur( ev ) {
                                                if( ev.target.value.trim() === '' ) {
                                                        classie.remove( ev.target.parentNode, 'input--filled' );
                                                }
                                        }
                        </script>-->
        <script>
            $(".form-control").focus(function () {
                $(this).prev('.input-group-addon').removeClass().addClass('input-group-addon-focus');
                $(this).next('.input-group-addon').removeClass().addClass('input-group-addon-focus');
            });

            $(".form-control").focusout(function () {
                $(this).prev('.input-group-addon-focus').removeClass().addClass('input-group-addon');
                $(this).next('.input-group-addon-focus').removeClass().addClass('input-group-addon');
            });

            $('#country_div').on('mouseenter', function () {
                $(this).find('.input-group-addon').removeClass('input-group-addon').addClass('input-group-addon-focus');
                $('.selectpicker ').css('border-color', '#e7505a');
            }).on('mouseleave', function () {
                $(this).find('.input-group-addon-focus ').removeClass('input-group-addon-focus').addClass('input-group-addon');
                $('.selectpicker ').css('border-color', '#c2cad8');
            });
        </script>


        <script>
            
            $(document).ready(function () {
                $('#side-menu').find('li').removeClass('active');
                $(document).find(".user").addClass('active');
            });
        </script>
        <script>
            $("#country").on("change", function () {
                var code = $("#country").select().val();
                $("#mobilecode").val(code).change();
                $("#mobilecode").attr('selectedIndex', code);
            });
        </script>
        <script>
            Flag = true;
            $("#createuser").on("click", function () {
                var username = $("#newUserName").val();
                var email = $("#emailId").val();
                var country = $("#country").select().val();
                var mobile = $("#mobileno").val();
                var flag = true;
                var alphaExp = /^[a-zA-Z ]+$/;
                var emailExp = /^[0-9a-zA-Z-.+]+@[a-zA-Z0-9.-]+.[a-zA-z0-9]{2,4}$/;
                var numericExp = /^[0-9]+$/;
                if (username === "" || username === null) {
                    $("#errorusername").text("Please enter user name");
                    flag = false;
                } else if (username.match(alphaExp)) {
                    
                } else {
                    $("#errorusername").text("Please enter a valid user name");
                    flag = false;
                }
                if (email === "" || email === null) {
                    $("#erroremailid").text("Please enter email address");
                    flag = false;
                } else if (email.match(emailExp)) {
                    
                } else {
                    $("#erroremailid").text("Please enter a valid email address!");
                    flag = false;
                }
                if (country === "" || country === null) {
                    $("#errorcountry").text("Please select country");
                    flag = false;
                } else {
                    
                }
                if (mobile === "" || mobile === null) {
                    $("#errormobileno").text("Please enter mobile number");
                    flag = false;
                } else if (mobile.match(numericExp)) {
                    
                } else {
                    $("#errormobileno").text("Please enter a valid mobile number");
                    flag = false;
                }
                if (!flag || !Flag) {
                    return false;
                }
            });


            $("#country").on("change", function () {
                var country = $("#country").select().val();
                if (country === "" || country === null) {
                    $("#errorcountry").text("Please select country");
                } else {
                    $("#errorcountry").text("");
                }
            });
            
        $("#emailId").on("change", function(e){
            console.log("Check...Now");
            if(typeof $(this).val()!=="undefined"&& $(this).val()!==""){
                var check = new RegExp(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,6})$/);
                if(!check.test($(this).val())){
                      $("#erroremailid").html("Please enter a valid email address");
                      Flag = false;
                }else{
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/checkforuseremail",{email:$(this).val(),type:3},
                    function(data,status){
                      if(data!=="success"){
                        $("#erroremailid").html(data);
                        Flag = false;
                      }else{
                       $("#erroremailid").html("");
                      }
                    });
                }
            }else{
                $("#erroremailid").html("Please enter email address.");
                Flag = false;
            }
        });
        $("#newUserName").on("change", function (e) {
            if(typeof $(this).val()!=="undefined"&& $(this).val()!==""){
                var check = new RegExp(/^[a-zA-Z ]+$/);
                if(!check.test($(this).val())){
                      $("#errorusername").html("Please enter a valid user name");
                      Flag = false;
                }else{
                    $("#errorusername").text("");
                }
            }else{
                $("#errorusername").html("Please enter User Name");
                Flag = false;
            }
        });
        
        $("#mobileno").on("change", function (e) {
            if(typeof $(this).val()!=="undefined"&& $(this).val()!==""){
                var check = new RegExp(/^[0-9]+$/);
                if(!check.test($(this).val())){
                      $("#errormobileno").html("Please enter a valid mobile number");
                      Flag = false;
                }else{
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/checkforusermobileno",{mobile:$(this).val(),type:3},
                    function(data,status){
                        if(data!=="success"){
                          $("#errormobileno").html(data);
                          Flag = false;
                        }else{
                         $("#errormobileno").html("");
                        }
                    });
                }
            }else{
                $("#errormobileno").html("Please enter Mobile Number");
                Flag = false;
            }
        });
</script>
    </body>
</html>
