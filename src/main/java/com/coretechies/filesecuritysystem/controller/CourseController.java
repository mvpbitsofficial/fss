/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.controller;

import com.coretechies.filesecuritysystem.algorithm.UserType;
import com.coretechies.filesecuritysystem.dao.CourseDao;
import com.coretechies.filesecuritysystem.dao.NotificationDao;
import com.coretechies.filesecuritysystem.dao.UserLicenseDao;
import com.coretechies.filesecuritysystem.domain.AdminLicense;
import com.coretechies.filesecuritysystem.domain.Course;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.domain.UserLicense;
import com.coretechies.filesecuritysystem.service.AdminLicenseService;
import com.coretechies.filesecuritysystem.service.CourseService;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @Controller annotation represents that this is a java controller class.
 * The CourseController class implements an application that handles all the request regarding course,
 * like add a new course, view all already added courses and delete a course. 
 * 
 * @SessionAttributes annotation represents a user in session.
 * "user" is a object of userInfo class and have a complete information
 * of that user who is present in the session at that time.
 * 
 * @author Jaishree
 * @version 1.0
 * @since 2016-09-02
 */
@Controller
@SessionAttributes({"user"})
public class CourseController {

    /** The @Autowired annotation is auto wire the bean by matching data type.
     *  courseDao, courseService and mailController are objects to use methods of these classes in our controller. 
     */
    @Autowired
    private CourseDao courseDao;

    @Autowired
    private CourseService courseService;
    
    @Autowired
    private UserLicenseDao userLicenseDao;
    
    @Autowired
    private NotificationDao notificationDao;
    
    @Autowired
    private AdminLicenseService adminLicenseService;

    /**
     * This method will check if a client is in session at that time then 
     * request is returns on a page viewcourse with a list of all courses added by that particular client.
     * owner can not see the courses added by any client.
     * 
     * @param userName
     * @param modelMap
     * @return on page viewcourse
     */
    @GetMapping(value = "/{userName}/viewcourse")
    public ModelAndView viewCourse(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                List<Course> courses = courseDao.getCourses(userInfo.getUserInfoId());
                modelMap.addAttribute("courses", courses);
                for (Course course : courses) {
                    List<UserLicense> userLicenses = course.getUserLicenses();
                    for (UserLicense userLicense : userLicenses) {
                        userLicense.getSystemInfoId();
                        // code to check same systemid  
                    }
                }
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                modelMap.addAttribute("history", notificationDao.getCourseNotificationHistory(userInfo.getUserInfoId()));
                return new ModelAndView("viewcourse");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method or the url for this mehod is called if client want to add a new course.
     * To add a course required entries need to fill on the form and 
     * for this purpose, this method will take the client on addcourse page. 
     * 
     * @param userName
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/addcourse")
    public ModelAndView getAddCourse(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                return new ModelAndView("addcourse");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will call on the click of submit the add course.
     * Before submit or adding the courses all the entries are checked and
     * if any entry have a problematic value it will show a error message.
     * and if the entries are correct then on successful addition a success message will shown. 
     * 
     * @param userName
     * @param courseName
     * @param courseDescription
     * @param modelMap
     * @return an output string 
     */
    @PostMapping(value = "/{userName}/addcourse")
    @ResponseBody
    @Transactional(propagation = Propagation.REQUIRED)
    public String addCourse(@PathVariable String userName, @RequestParam String courseName, @RequestParam String courseDescription, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                if (courseName == null || courseName.trim().isEmpty()) {
                    return "Please enter course name.";
                }
                if (courseDescription == null || courseDescription.trim().isEmpty()) {
                    return "Please enter course description.";
                }
                if (courseName.replace(" ", "").length() < 7) {
                    return "Name contains min 7 character.";
                }
                if (courseDao.getCourse(userInfo.getUserInfoId(), courseName) != null) {
                    return "This course is already added by you.";
                }
                if (courseService.saveCourse("FSSABCD1000", courseName, courseDescription, userInfo.getUserInfoId()) != null) {
                    return "success";
                } else {
                    return "Unable to add data";
                }
            } else {
                return "Please login again";
            }
        } else {
            return "Please login again";
        }
    }

    /**
     * This method is simply delete a existing course, 
     * And after successful deletion a mail will send to the client who added that particular course.
     * The request is redirected to the url /viewcourse with a success message of deletion.
     * 
     * @param userName
     * @param courseIds
     * @param request
     * @param redirectAttributes
     * @param modelMap
     * @return via a url
     */
    @GetMapping(value = "/{userName}/deletecourse")
    @ResponseBody
    public String deleteCourse(@PathVariable String userName, @RequestParam(value = "courseIds[]") Integer[] courseIds, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                String message = "";
                String message1 = "";
                for (Integer id : courseIds) {
                    System.out.println("course Id=="+id);
                    Course course = courseDao.getCourse(userInfo.getUserInfoId(), id);
                    List<UserLicense> licenseList = userLicenseDao.getLicenseListOfAssignedCourse(id);
                    if(licenseList == null){
                        if (course != null) {
                            if (courseService.deleteCourse(id)) {
                                message += ","+course.getCourseName();
                                redirectAttributes.addFlashAttribute("message", "Course has been deleted successfully.");
                            } else {
                                redirectAttributes.addFlashAttribute("message", "Unable to delete course.");
                            }
                        } else {
                            redirectAttributes.addFlashAttribute("message", "This course is unavailable.");
                        }
                    } else{
                        message1 += ","+course.getCourseName();
                    }
                }
                if(!message.isEmpty()){
                    message = "Course "+ "\""+message.substring(1)+"\""+" has been deleted successfully";
                }
                if(!message1.isEmpty()){
                    message1 = "Course "+"\""+ message1.substring(1)+"\""+" can not delete, Id is in use.";
                }
                System.out.println("message1=="+message+" message2=="+message1);
                if(!message.isEmpty() && !message1.isEmpty()){
                    return "success#"+message+" and "+message1;
                }else{
                    return "success#"+message+""+message1;
                }
            } else {
                return "Unauthorised Access";
            }
        } else {
            return "Please Login Again";
        }
    }
    
    /**
     * 
     * @param userName
     * @param request
     * @param response
     * @param modelMap
     * @return 
     */
    @RequestMapping(value = "/{userName}/uploadcoursefile",method = RequestMethod.POST )
    @ResponseBody
    public String uploadCourseFile(@PathVariable String userName,
            MultipartHttpServletRequest request, HttpServletResponse response, ModelMap modelMap){
        System.out.println("uploading course");
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                Iterator<String> itr = request.getFileNames();
                MultipartFile course = request.getFile(itr.next());
                System.out.println(course);
                if(course != null && !course.isEmpty()){
                    if(course.getOriginalFilename().substring(course.getOriginalFilename().lastIndexOf(".") + 1).equals("xlsx")){
                        try{ 
                            String originalFilename = course.getOriginalFilename();
                            System.out.println(originalFilename);
//                              FileInputStream file = new FileInputStream(originalFilename);
                                XSSFWorkbook workbook = new XSSFWorkbook(course.getInputStream());
//                              HSSFWorkbook workbook = new HSSFWorkbook(course.getInputStream());
//                              HSSFSheet sheet = workbook.getSheetAt(0);
                                XSSFSheet sheet = workbook.getSheetAt(0);
                                Iterator<Row> rowIterator = sheet.iterator();
                                int rowcount = 0;
                                String check = "";
                                while (rowIterator.hasNext()) 
                                {
                                    String courseId = "";
                                    String courseDiscription = "";
                                    String courseName = "";
                                    int count=0;
                                    boolean flag = true;
                                    Row row = rowIterator.next();
                                    //For each row, iterate through all the columns
                                    Iterator<Cell> cellIterator = row.cellIterator();
                                    rowcount++;
                                    if(rowcount == 1){
                                        while (cellIterator.hasNext()) {
                                            Cell cell = cellIterator.next();
                                            String desktopLicense = userInfo.getAdminLicense().getDesktopLicense();
                                            System.out.println(desktopLicense);
                                            System.out.println(cell.getStringCellValue());
                                            Boolean flag1 = desktopLicense.equals(cell.getStringCellValue());
                                            if(flag1){
                                                break;
                                            } else{
                                                System.out.println("Matching.....");
                                                return "Desktop License Not Match.";
                                            }
                                        }
                                    }else{
                                        while (cellIterator.hasNext()) 
                                        {
                                            count++;
                                            Cell cell = cellIterator.next();
                                            //Check the cell type and format accordingly
    //                                        System.out.print(cell.getStringCellValue() + "\n");
                                            System.out.println(count);

                                            switch (count) {
                                                case 1: 
                                                    courseId = cell.getStringCellValue();
                                                    System.out.println("course id=="+courseId);
                                                    break;
                                                case 2:
                                                    courseName = cell.getStringCellValue();
                                                    System.out.println("case 2:"+courseName);
                                                    break;
                                                case 3:
                                                    courseDiscription += cell.getStringCellValue()+" ";
                                                    break;
                                                default:
                                                    break;
                                            } 
                                            System.out.println("discription:"+courseDiscription);
    //                                        switch (cell.getCellType()) 
    //                                        {
    //                                            case Cell.CELL_TYPE_NUMERIC:
    //                                                System.out.print(cell.getNumericCellValue() + "t");
    //                                                break;
    //                                            case Cell.CELL_TYPE_STRING:
    //                                                System.out.print(cell.getStringCellValue() + "t");
    //                                                break;
    //                                        }
                                        }
                                    }
                                    
                                    System.out.println("Course Name======"+courseName);
                                    List<Course> courseById = courseService.getCourseById(userInfo.getUserInfoId());
                                    for (Course course1 : courseById) {
                                        System.out.println("course:::::"+course1);
                                        if(course1.getCourseName().equals(courseName)){
                                            check += ","+courseName;
                                            System.out.println(check);
                                        }else{
                                            System.out.println(courseName);
                                        }
                                        System.out.println(check.indexOf(courseName));
                                    }
                                    if(flag){
                                        if(courseName.equals("")){
                                            System.out.println("Blank ROW");
                                        }else if(!check.contains(courseName)){
                                            AdminLicense adminLicense = adminLicenseService.getAdminLicenseByClient(userInfo.getUserInfoId());
                                            Integer courseLimit = adminLicense.getCourseLimit();
                                            int size = courseById.size();
                                            if(courseLimit == -1){
                                                courseService.saveCourse(courseId, courseName, courseDiscription, userInfo.getUserInfoId());
                                            } else{
                                                if(courseLimit > size){
                                                    courseService.saveCourse(courseId, courseName, courseDiscription, userInfo.getUserInfoId());
                                                }else{
                                                    return "Course upload limit over";
                                                }
                                            }
                                        }
                                    } else{
                                        System.out.println("problem occurs");
                                    }
                                    
                                }
//                                file.close();
                                if(check.equals("")){
                                    return "success";
                                } else{
                                    return check.substring(1)+" course skipped due to already existing names";
                                }
                        } catch(Exception e){
                            Logger.getLogger(CourseController.class.getName()).log(Level.SEVERE, null, e);
                            return "error";
                        }
                } else {
                       return "Please choose Only a Excel file having extension .xlsx";  
                    }
                } else {
                    return "empty file";
                }
            } else {
                return "unauthorised access";
            }
        } else {
            return "login again";
        }
    }
    
    @PostMapping(value = "/{userName}/coursedetails")
    @ResponseBody
    public String getCourseDetails(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer courseid){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                List<UserLicense> userLicenses = userLicenseDao.getLicenseListOfAssignedCourse(courseid);
                if(userLicenses != null){
                    String displaylist = "";
                    for (UserLicense userLicense : userLicenses) {
                        displaylist += ",{\"userName\":\"" + userLicense.getUserInfo().getUserName() + "\","
                                + "\"Email\":\"" + userLicense.getUserInfo().getEmailId() + "\","
                                + "\"SystemName\":\""+userLicense.getSystemInfo().getSystemName()+"\","
                                + "\"SystemId\":\""+userLicense.getSystemInfo().getSystemId()+"\"}";
                    }
                    String coursedetail = "success#[" + displaylist.substring(1) + "]";
                    return coursedetail;
                }else{
                    return "No Data Available";
                }
            } else {
                return "Unauthorised Access";
            }
        } else {
            return "Please Login Again";
        }
    }
}
