/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.algorithm;

/**
 *
 * @author CoreTechies M8
 */
public enum UserType {

    Admin(1),

    Client(2),

    User(3);

    private final Integer userTypeValue;

    private UserType(Integer userTypeValue) {
        this.userTypeValue = userTypeValue;
    }

    public Integer getUserTypeValue() {
        return userTypeValue;
    }

    public static UserType parse(Integer userTypeValue) {
        for (UserType value : UserType.values()) {
            if (value.getUserTypeValue().equals(userTypeValue)) {
                return value;
            }
        }
        return null;
    }
}
