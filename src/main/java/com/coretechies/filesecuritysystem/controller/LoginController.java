/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.controller;

import com.coretechies.filesecuritysystem.algorithm.UserType;
import com.coretechies.filesecuritysystem.dao.SecurityQuestionAnswerDao;
import com.coretechies.filesecuritysystem.dao.SecurityQuestionDao;
import com.coretechies.filesecuritysystem.dao.UserInfoDao;
import com.coretechies.filesecuritysystem.domain.AdminLicense;
import com.coretechies.filesecuritysystem.domain.SecurityQuestion;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.security.RandomPasswordGenerator;
import com.coretechies.filesecuritysystem.security.SHA256;
import com.coretechies.filesecuritysystem.service.UserInfoService;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 * @Controller annotation represents that this is a java controller class.
 * The LoginController class implements an application that handles all the requests related to login,
 * like check userName and password for an existing user and forgot password.
 * 
 * @SessionAttribute annotation represents a user in session.
 * "user" is a object of userInfo class and have a complete information
 * of that user who is present in the session at that time.
 * "userId" is ID of the user present in session.
 * 
 * @author Jaishree
 * @version 1.0
 * @since 2016-09-02
 */
@Controller
@SessionAttributes({"user", "userId"})
public class LoginController {

    /** The @Autowired annotation is auto wire the bean by matching data type.
     *  userInfoService, userInfoDao, securityQuestionDao, securityQuestionAnswerDao and mailController are objects to use methods of these classes in our controller. 
     */
    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private UserInfoDao userInfoDao;

    @Autowired
    private MailController mailController;

    @Autowired
    private SecurityQuestionDao securityQuestionDao;

    @Autowired
    private SecurityQuestionAnswerDao securityQuestionAnswerDao;
    
    @Autowired
    private SHA256 sha256;

    @GetMapping(value = "/{userName}/welcome")
    public ModelAndView welcome(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo.getUserName().equals(userName)) {
            return new ModelAndView("welcome");
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This class will check the userName and password entered by the user is matching in the database or not,
     * If the userName and password is correct, it checks the issue date and expiry date of license for the user.
     * If the user is authenticate it return a string contain success#userName otherwise it return a appropriate error message.
     * 
     * @ResponseBody annotations are used to bind the HTTP request/response body
     * with a domain object in method parameter or return type. 
     * 
     * @param userName
     * @param password
     * @param modelMap
     * @return String
     * @throws java.security.NoSuchAlgorithmException 
     */
    @PostMapping(value = "/login")
    @ResponseBody
    public String authentication(@RequestParam String userName, @RequestParam String password, ModelMap modelMap) throws NoSuchAlgorithmException {
        UserInfo userInfo = userInfoService.authenticate(userName, password);
        if (userInfo != null) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                AdminLicense adminLicense = userInfo.getAdminLicense();
                System.out.println("userLicense : " + adminLicense);
                if (adminLicense == null) {
                    return "No Lincese available.";
                }else if (adminLicense.getLicense()==null){
                    return "your web license is not available.";
                }else if (new Date().compareTo(adminLicense.getIssueDate()) < 0) {
                    return "Your License not started yet.";
                } else if (adminLicense.getExpiryDate() != null){
                    if(new Date().compareTo(adminLicense.getExpiryDate()) > 0) {
                        return "Your License is expired.";
                    } 
                } else if(adminLicense.getWeblicenseLimit() == 0){
                    return "You are not authorised to access.";
                }
                else if(!userInfo.getUserStatus()){
                    return "You are not a active user";
                }
            }
            modelMap.addAttribute("user", userInfo);
            return "success#" + userInfo.getUserName();
        } else {
            return "Invalid User Name and Password";
        }
    }

    /**
     * This url is call if any user request for forgot password. 
     * It simply return the user to the forgotpasswordusername page.
     *  
     * @param modelMap
     * @return on page forgotpasswordusername
     */
    @GetMapping(value = "/forgotpasswordusername")
    public ModelAndView getForgotPasswordUserName(ModelMap modelMap) {
        return new ModelAndView("forgotpasswordusername");
    }

    /**
     * This method will check that a user of this userName is exists or not?
     * And return String depend upon the result.
     * 
     * @param emailId
     * @param status
     * @param request
     * @param session
     * @ResponseBody annotations are used to bind the HTTP request/response body
     * with a domain object in method parameter or return type. 
     *
     * @param userName
     * @param model
     * @return String
     * @throws java.security.NoSuchAlgorithmException 
     */
    @PostMapping(value = "/forgotpasswordusername")
    @ResponseBody
    public String forgotPasswordUserName(@RequestParam String userName, @RequestParam String emailId, SessionStatus status, HttpSession session, HttpServletRequest request, ModelMap model) throws NoSuchAlgorithmException {
        UserInfo userInfo = userInfoDao.getUserInfo(userName);
        if (userInfo != null) {
            if(userInfo.getEmailId().equals(emailId)){
                model.addAttribute("userId", userInfo.getUserInfoId());
                RandomPasswordGenerator generator = new RandomPasswordGenerator();
                String password = String.copyValueOf(generator.generatePswd(5, 10, 2, 2, 1));
                UserInfo userInfo1 = userInfoDao.getUserInfo(userInfo.getUserInfoId());
                userInfo1.setPassword(sha256.getSHA256(password));
                userInfoDao.updateUser(userInfo1);
                System.out.println("password=="+password);
                try {
                    mailController.sendEmail("Forgotten password reset", 
                            "Dear "+userInfo.getUserName()+", <br><br>Your password has been changed successfully. <br><br> you can manage your account with following creadentials. <br><br> Login ID: <b>"+userInfo.getUserName()+"</b><br> Password : <b>"+password+"</b>", 
                            userInfo.getEmailId(), request, userInfo);
                    session.invalidate(); 
                    status.setComplete();
                    return "success";
                } catch (Exception e) {
                    e.printStackTrace();
                    return "error";
                }
            } else{
                return "Incorrect User Name or Email ID.";
            }   
        } else {
            return "Invalid user name.";
        }
    }

    /**
     * User have to put answers of questions to use them in case of forgot password.
     * This method is simply take the user to the forgotpasswordquestion page.
     * 
     * @param modelMap
     * @return String
     */
    @PostMapping(value = "/forgotpasswordquestiondisplay")
    @ResponseBody
    public String getForgotPasswordQuestion(ModelMap modelMap) {
        UserInfo userInfo = userInfoDao.getUserInfo((Integer) modelMap.get("userId"));
        if (userInfo != null) {
            String dropdownquestions = "";
            List<SecurityQuestion> securityQuestions = securityQuestionDao.getSecurityQuestions();
            for (SecurityQuestion securityQuestion : securityQuestions) {
                dropdownquestions += ",{\"questionId\":\"" + securityQuestion.getSecurityQuestionId() + "\","
                                + "\"question\":\"" + securityQuestion.getQuestion() + "\"}";
            }
            System.out.println("dropdown"+dropdownquestions);
            return "success#["+dropdownquestions.substring(1)+"]";
        } else {
            return "login again";
        }
    }

    /**
     * This method will take the questions and answers added by the user as the method parameters,
     * and match it with questions and answers stored in database.
     * And returns a string of success or error message.
     * 
     * @ResponseBody annotations are used to bind the HTTP request/response body
     * with a domain object in method parameter or return type. 
     * 
     * @param question1
     * @param answer1
     * @param question2
     * @param answer2
     * @param model
     * @return String
     * @throws java.security.NoSuchAlgorithmException 
     */
    @PostMapping(value = "/forgotpasswordquestion")
    @ResponseBody
    public String forgotPasswordQuestion(@RequestParam Integer question1, @RequestParam String answer1,
            @RequestParam Integer question2, @RequestParam String answer2,
            ModelMap model) throws NoSuchAlgorithmException {

        UserInfo userInfo = userInfoDao.getUserInfo((Integer) model.get("userId"));
        if (userInfo != null) {
            if (question1 == null || question1 == 0) {
                return "Please select first question.";
            } else if (answer1 == null || answer1.trim().isEmpty()) {
                return "Please enter answer for first question.";
            } else if (question2 == null || question2 == 0) {
                return "Please select second question.";
            } else if (answer2 == null || answer2.trim().isEmpty()) {
                return "Please enter answer for second question.";
            } else if (Objects.equals(question1, question2)) {
                return "Both Questions are same please select different question.";
            }
            if (securityQuestionAnswerDao.getSecurityQuestionAnswers(userInfo.getUserInfoId(), question1, answer1)) {
                if (securityQuestionAnswerDao.getSecurityQuestionAnswers(userInfo.getUserInfoId(), question2, answer2)) {
                    return "success";
                }
            }
            return "Wrong question or answer.";
        } else {
            return "Invalid try.";
        }
    }

    /**
     * This method will take the user to forgotpassword page
     * 
     * @param modelMap
     * @return on page forgotpassword
     */
    @GetMapping(value = "/forgotpassword")
    public ModelAndView getForgotPassword(ModelMap modelMap) {
        UserInfo userInfo = userInfoDao.getUserInfo((Integer) modelMap.get("userId"));
        if (userInfo != null) {
            return new ModelAndView("forgotpassword");
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will reset the password of the requested user and send a mail for confirmation.
     * 
     * @param status
     * @param session
     * @param newPassword
     * @param request
     * @param model
     * @return
     * @throws NoSuchAlgorithmException
     */
    @PostMapping(value = "/forgotpassword")
    @ResponseBody
    public String forgotPassword(SessionStatus status, HttpSession session, @RequestParam String newPassword, HttpServletRequest request, ModelMap model) throws NoSuchAlgorithmException {

        UserInfo userInfo = userInfoDao.getUserInfo((Integer) model.get("userId"));
        if (userInfo != null) {
            if ((userInfo = userInfoService.updatePassword(userInfo, newPassword)) != null) {
                try {
                    Boolean sendMail = mailController.sendMail("Password Changes", "Your password is successfully changed.\n", userInfo.getEmailId(), request, userInfo.getUserInfoId());
                    if(sendMail){
                        session.invalidate();
                        status.setComplete();
                        return "success";
                    } else{
                        return "Mail Cannot be sent due to incomplete mail settings.";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return "error";
                }
                
            } else {
                return "Unexpected error occur, Please try again later or contact to administrator.";
            }
        } else {
            return "Invalid try.";
        }
    }
    
//    @Scheduled(fixedRate=1*60*1000)
//    public void updateEmployeeInventory(){
//        System.out.println("Started fixed rate job");
//        /**
//         * add your scheduled job logic here
//         */
//    }
    
}
