/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.SystemInfo;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.domain.UserLicense;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 * @author CoreTechies M8
 */
public interface UserLicenseDao {

    public UserLicense saveUserLicense(UserLicense userLicense);

    public UserLicense updateUserLicense(UserLicense userLicense);

    public List<UserLicense> getUserLicenseByUser(Integer userId);

    public UserLicense getUserLicense(Integer licenseId);

//    public Boolean deleteUserLicense(Integer licenseId);
    public List<UserLicense> getLicenseExpireAlert(Date date);

    public List<UserLicense> getUserLicenseByCreator(Integer createdBy);

    public Set<UserInfo> getAssignUserListOnCourse(Integer courseId);

    public Set<SystemInfo> getAssignSystemListOnCourse(Integer courseId);
    
    public List<UserLicense> getUserLicenseBySystemId(Integer systemInfoId);

    public List<UserLicense> getUserLicenseListByCreateOn(Integer createdBy, Date fromDate, Date toDate);

    public List<UserLicense> getUserLicenseListByExpiryDate(Integer createdBy, Date fromDate, Date toDate);

    public List<UserLicense> getUserLicenseForChart(Integer createdBy, Date date);
    
    public UserLicense getConfirmLicenseForDisplay();
    
    public List<UserLicense> getTotalUsedCourseCount(Integer courseid);
    
    public Integer showLicenseChartData(Date startDate, Date endDate, Integer createdBy);
    
    public List<UserLicense> getLicenseListOfAssignedCourse(Integer courseId);
    
    public List<UserLicense> getUserLicenseByWatermarkId(Integer watermarkId);
    
    public Integer getAllUserLicenseCount();
}
