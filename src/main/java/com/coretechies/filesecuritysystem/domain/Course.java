/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author CoreTechies M8
 */
@Entity
@Table(name = "course")
@Access(AccessType.FIELD)
public class Course implements Serializable {

    @Id
    @GeneratedValue(generator = "course")
    @GenericGenerator(name = "course", strategy = "increment")
    private Integer id;

    @Column
    private String courseId;

    @Column
    private String courseName;

    @Column
    private String courseDescription;

    @Column
    private Integer userId;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId", insertable = false, updatable = false, nullable = false, unique = false)
    private UserInfo userInfo;
    
    @OneToMany(mappedBy = "course", fetch = FetchType.LAZY)
    private List<UserLicense> userLicenses;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<UserLicense> getUserLicenses() {
        return userLicenses;
    }

    public void setUserLicenses(List<UserLicense> userLicenses) {
        this.userLicenses = userLicenses;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public String toString() {
        return "Course{" + "id=" + id + ", courseId=" + courseId + ", courseName=" + courseName + ", courseDescription=" + courseDescription + ", userId=" + userId + '}';
    }

}
