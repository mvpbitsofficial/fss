/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.algorithm;

import java.util.Objects;

/**
 *
 * @author CoreTechies M8
 */
public enum UserStatus {

    Activated(true),
    Deactivated(false);

    private final Boolean userStatusValue;

    private UserStatus(Boolean userStatusValue) {
        this.userStatusValue = userStatusValue;
    }

    public Boolean getUserStatusValue() {
        return userStatusValue;
    }

    public static UserStatus parse(Boolean userStatusValue) {
        for (UserStatus userStatus : UserStatus.values()) {
            if (Objects.equals(userStatus.getUserStatusValue(), userStatusValue)) {
                return userStatus;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        switch (this) {
            case Activated:
                return "Active";
            case Deactivated:
                return "Deactive";
            default:
                System.out.println(this);
                return null;
        }
    }
}
