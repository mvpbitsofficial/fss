/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author CoreTechies M8
 */
@Entity
@Table(name = "userinfo")
@Access(AccessType.FIELD)
public class UserInfo implements Serializable {

    @Id
    @GeneratedValue(generator = "userinfo")
    @GenericGenerator(name = "userinfo", strategy = "increment")
    private Integer userInfoId;

    @Column
    private String userName;

    @Column
    private String password;

    @Column
    private String emailId;

    @Column
    private String imagePath;

    @Column
    private Integer countryCodeId;

    @Column
    private String mobileNumber;
    
    @Column
    private String displayName;

    @Column
    private Integer userType;

    @Column
    private String organization;

    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createOn;

    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updateOn;

    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date creationDate;
     
    @Column
    private Boolean userStatus;

    @Column
    private Integer createdBy;

    @Column
    private Integer updatedBy;
    
    @Column
    private Integer mobileCode;

    @Column
    private Integer logoutTime;
    
    @Column
    private Integer securityKey;
    
    @OneToOne(mappedBy = "userInfo")
    private AdminLicense adminLicense;
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "countryCodeId", insertable = false, updatable = false, nullable = false, unique = false)
    private CountryCode countryCode;

    public CountryCode getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(CountryCode countryCode) {
        this.countryCode = countryCode;
    }
    
    @OneToMany(mappedBy = "userInfo", fetch = FetchType.LAZY)
    private List<UserLicense> userLicense;
    
    @OneToMany(mappedBy = "userInfo", fetch = FetchType.LAZY)
    private List<SystemInfo> systemInfos;
    
    public Integer getUserInfoId() {
        return userInfoId;
    }

    public void setUserInfoId(Integer userInfoId) {
        this.userInfoId = userInfoId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Integer getCountryCodeId() {
        return countryCodeId;
    }

    public void setCountryCodeId(Integer countryCodeId) {
        this.countryCodeId = countryCodeId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public Date getCreateOn() {
        return createOn;
    }

    public void setCreateOn(Date createOn) {
        this.createOn = createOn;
    }

    public Date getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(Date updateOn) {
        this.updateOn = updateOn;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
    
    public Boolean getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Boolean userStatus) {
        this.userStatus = userStatus;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public AdminLicense getAdminLicense() {
        return adminLicense;
    }

    public void setAdminLicense(AdminLicense adminLicense) {
        this.adminLicense = adminLicense;
    }

    public Integer getMobileCode() {
        return mobileCode;
    }

    public void setMobileCode(Integer mobileCode) {
        this.mobileCode = mobileCode;
    }

    public List<UserLicense> getUserLicense() {
        return userLicense;
    }

    public void setUserLicense(List<UserLicense> userLicense) {
        this.userLicense = userLicense;
    }

    public List<SystemInfo> getSystemInfos() {
        return systemInfos;
    }

    public void setSystemInfos(List<SystemInfo> systemInfos) {
        this.systemInfos = systemInfos;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getLogoutTime() {
        return logoutTime;
    }

    public void setLogoutTime(Integer logoutTime) {
        this.logoutTime = logoutTime;
    }

    public Integer getSecurityKey() {
        return securityKey;
    }

    public void setSecurityKey(Integer securityKey) {
        this.securityKey = securityKey;
    }

    @Override
    public String toString() {
        return "UserInfo{" + "userInfoId=" + userInfoId + ", userName=" + userName + ", password=" + password + ", emailId=" + emailId + ", imagePath=" + imagePath + ", countryCodeId=" + countryCodeId + ", mobileNumber=" + mobileNumber + ", displayName=" + displayName + ", userType=" + userType + ", organization=" + organization + ", createOn=" + createOn + ", updateOn=" + updateOn + ", userStatus=" + userStatus + ", createdBy=" + createdBy + ", updatedBy=" + updatedBy + ", mobileCode=" + mobileCode + ", logoutTime=" + logoutTime + ", securityKey=" + securityKey + ", countryCode=" + countryCode + '}';
    }
    
}
