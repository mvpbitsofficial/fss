/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.service;

import com.coretechies.filesecuritysystem.algorithm.NotificationType;
import com.coretechies.filesecuritysystem.algorithm.UserStatus;
import com.coretechies.filesecuritysystem.dao.NotificationDao;
import com.coretechies.filesecuritysystem.dao.UserInfoDao;
import com.coretechies.filesecuritysystem.domain.Notification;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.security.SHA256;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserInfoDao userInfoDao;
  
    @Autowired
    private NotificationDao notificationDao;

    @Autowired
    private SHA256 sha256;

    @Override
    public UserInfo authenticate(String userName, String password) throws NoSuchAlgorithmException {
        return userInfoDao.authenticate(userName, sha256.getSHA256(password));
    }

    @Override
    public UserInfo updatePassword(UserInfo userInfo, String password) throws NoSuchAlgorithmException {
        userInfo.setPassword(sha256.getSHA256(password));
        UserInfo updateUser = userInfoDao.updateUser(userInfo);
        return updateUser;
    }

    @Override
    public boolean authenticate(String userName, String currentPassword, UserInfo userInfo) throws NoSuchAlgorithmException {
        return sha256.getSHA256(currentPassword).equals(userInfo.getPassword());
    }

    @Override
    public UserInfo createClientUser(String userName, String password, String emailId, String profilePic, Integer countryId, Integer mobileCode, String mobileNumber, Integer userType, String organization, Boolean userStatus, Integer createdBy) throws NoSuchAlgorithmException {
        UserInfo userInfo = new UserInfo();
        userInfo.setCountryCodeId(countryId);
        userInfo.setCreateOn(new Date());
        userInfo.setCreatedBy(createdBy);
        userInfo.setUpdateOn(new Date());
        userInfo.setCreationDate(new Date());
        userInfo.setUpdatedBy(createdBy);
        userInfo.setEmailId(emailId);
        userInfo.setImagePath(profilePic);
        userInfo.setMobileCode(mobileCode);
        userInfo.setMobileNumber(mobileNumber);
        userInfo.setOrganization(organization);
        userInfo.setPassword(sha256.getSHA256(password));
        userInfo.setUserName(userName);
        userInfo.setUserStatus(userStatus);
        userInfo.setUserType(userType);
        String[] split = userName.split(" ");
        String displayname="";
        for (String split1 : split) {
            displayname+=split1.charAt(0);
        }
        displayname = displayname.toUpperCase();
        userInfo.setDisplayName(displayname);
        UserInfo createUser = userInfoDao.createUser(userInfo);
        Notification notification = new Notification();
        notification.setCreatedBy(createUser.getCreatedBy());
        notification.setNotificationMessage("New Client "+createUser.getUserName()+" is created");
        notification.setCreateOn(createUser.getCreateOn());
        notification.setNotificationType(NotificationType.Creation_History.getNotificationTypeValue());
        notification.setSeenby("0,");
        notification.setUserId(createUser.getUserInfoId());
        notificationDao.saveNotification(notification);
        return createUser;
    }

    @Override
    public UserInfo updateClientUser(Integer userId, String userName, String emailId, String profilePic, Integer countryId, Integer mobileCode, String mobileNumber, Integer userType, String organization, Boolean userStatus, Integer createdBy) throws NoSuchAlgorithmException {
        UserInfo userInfo = userInfoDao.getUserInfo(userId);
        userInfo.setCountryCodeId(countryId);
        userInfo.setUpdateOn(new Date());
        userInfo.setUpdatedBy(createdBy);
        userInfo.setEmailId(emailId);
        if(profilePic != null){
            userInfo.setImagePath(profilePic);
        } 
        userInfo.setMobileCode(mobileCode);
        userInfo.setMobileNumber(mobileNumber);
        userInfo.setOrganization(organization);
        userInfo.setUserName(userName);
        userInfo.setUserStatus(userStatus);
        String[] split = userName.split(" ");
        String displayname="";
        for (String split1 : split) {
            displayname+=split1.charAt(0);
        }
        displayname = displayname.toUpperCase();
        userInfo.setDisplayName(displayname);
        userInfo.setUserType(userType);
        UserInfo updateUser = userInfoDao.updateUser(userInfo);
        Notification notification = new Notification();
        notification.setCreatedBy(updateUser.getCreatedBy());
        notification.setNotificationMessage("Client "+updateUser.getUserName()+" is updated");
        notification.setCreateOn(updateUser.getUpdateOn());
        notification.setNotificationType(NotificationType.Updation_History.getNotificationTypeValue());
        notification.setSeenby("0,");
        notification.setUserId(updateUser.getUserInfoId());
        notificationDao.saveNotification(notification);
        return updateUser;
    }

    @Override
    public UserInfo deleteUser(Integer userId) {
        UserInfo userInfo = userInfoDao.getUserInfo(userId);
        if (userInfoDao.deleteClient(userId)) {
            Notification notification = new Notification();
            notification.setCreateOn(new Date());
            notification.setCreatedBy(userInfo.getCreatedBy());
            notification.setUserId(userId);
            notification.setNotificationType(NotificationType.Deletion_History.getNotificationTypeValue());
            notification.setSeenby("0,");
            notification.setNotificationMessage("User "+userInfo.getUserName()+" is Deleted");
            notificationDao.saveNotification(notification);
            return userInfo;
        } else {
            return null;
        }
    }

    @Override
    public UserInfo deactivateClient(Integer userId) {
        UserInfo userInfo = userInfoDao.getUserInfo(userId);
        userInfo.setUserStatus(UserStatus.Deactivated.getUserStatusValue());
        return userInfoDao.updateUser(userInfo);
    }

    @Override
    public UserInfo activateClient(Integer userId) {
        UserInfo userInfo = userInfoDao.getUserInfo(userId);
        userInfo.setUserStatus(UserStatus.Activated.getUserStatusValue());
        return userInfoDao.updateUser(userInfo);
    }

    @Override
    public UserInfo createUser(String userName, String password, String emailId, String profilePic, Integer countryId, Integer mobileCode, String mobileNumber, Integer userType, Boolean userStatus, Integer createdBy) throws NoSuchAlgorithmException {
        UserInfo userInfo = new UserInfo();
        userInfo.setCountryCodeId(countryId);
        userInfo.setCreateOn(new Date());
        userInfo.setCreatedBy(createdBy);
        userInfo.setUpdateOn(new Date());
        userInfo.setCreationDate(new Date());
        userInfo.setUpdatedBy(createdBy);
        userInfo.setEmailId(emailId);
        userInfo.setImagePath(profilePic);
        userInfo.setMobileCode(mobileCode);
        userInfo.setMobileNumber(mobileNumber);
        userInfo.setOrganization(null);
        userInfo.setPassword(sha256.getSHA256(password));
        userInfo.setUserName(userName);
        userInfo.setUserStatus(userStatus);
        String[] split = userName.split(" ");
        String displayname="";
        for (String split1 : split) {
            displayname+=split1.charAt(0);
        }
        displayname = displayname.toUpperCase();
        userInfo.setDisplayName(displayname);
        userInfo.setUserType(userType);
        UserInfo createUser = userInfoDao.createUser(userInfo);
        Notification notification = new Notification();
        notification.setCreatedBy(createUser.getCreatedBy());
        notification.setNotificationMessage("New User "+createUser.getUserName()+" is created");
        notification.setCreateOn(createUser.getCreateOn());
        notification.setNotificationType(NotificationType.Creation_History.getNotificationTypeValue());
        notification.setSeenby("0,");
        notification.setUserId(createUser.getUserInfoId());
        notificationDao.saveNotification(notification);
        return createUser;
    }

    @Override
    public UserInfo updateUser(Integer userId, String userName, String emailId, String profilePic, Integer countryId, Integer mobileCode, String mobileNumber, Integer userType, Boolean userStatus, Integer createdBy) throws NoSuchAlgorithmException {
        UserInfo userInfo = userInfoDao.getUserInfo(userId);
        userInfo.setCountryCodeId(countryId);
        userInfo.setUpdateOn(new Date());
        userInfo.setUpdatedBy(createdBy);
        userInfo.setEmailId(emailId);
        userInfo.setImagePath(profilePic);
        userInfo.setMobileCode(mobileCode);
        userInfo.setMobileNumber(mobileNumber);
        userInfo.setOrganization(null);
        userInfo.setUserName(userName);
        userInfo.setUserStatus(userStatus);
        String[] split = userName.split(" ");
        String displayname="";
        for (String split1 : split) {
            displayname+=split1.charAt(0);
        }
        displayname = displayname.toUpperCase();
        userInfo.setDisplayName(displayname);
        userInfo.setUserType(userType);
        UserInfo updateUser = userInfoDao.updateUser(userInfo);
        Notification notification = new Notification();
        notification.setCreatedBy(updateUser.getCreatedBy());
        notification.setNotificationMessage("User "+updateUser.getUserName()+" is updated");
        notification.setCreateOn(updateUser.getUpdateOn());
        notification.setNotificationType(NotificationType.Updation_History.getNotificationTypeValue());
        notification.setSeenby("0,");
        notification.setUserId(updateUser.getUserInfoId());
        notificationDao.saveNotification(notification);
        return updateUser;
    }

    @Override
    public Integer getUserInfoCountByCreatedBy(Integer createdBy) {
        List<UserInfo> userInfos = userInfoDao.getUserInfoByCreatedBy(createdBy);
        if (userInfos != null) {
            return userInfos.size();
        } else {
            return 0;
        }
    }
    
    @Override
    public List<UserInfo> chartUserData(Integer createdBy, Date date){
         return userInfoDao.chartUserData(createdBy, date);
    }
    
    @Override
    public Map<Integer, Integer> showUserIdThroughCount(){
        return userInfoDao.showUserIdThroughCount();
    }
}
