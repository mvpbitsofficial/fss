/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.domain;

import java.io.Serializable;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author CoreTechies M8
 */
@Entity
@Table(name = "emailaddress")
@Access(AccessType.FIELD)
public class EmailAddress implements Serializable {

    @Id
    @GeneratedValue(generator = "emailaddress")
    @GenericGenerator(name = "emailaddress", strategy = "increment")
    private Integer emailAddressId;

    @Column
    private String emailaddress;

    @Column
    private Integer userId;

    public Integer getEmailAddressId() {
        return emailAddressId;
    }

    public void setEmailAddressId(Integer emailAddressId) {
        this.emailAddressId = emailAddressId;
    }

    public String getEmailaddress() {
        return emailaddress;
    }

    public void setEmailaddress(String emailaddress) {
        this.emailaddress = emailaddress;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "EmailAddress{" + "emailAddressId=" + emailAddressId + ", emailaddress=" + emailaddress + ", userId=" + userId + '}';
    }

}
