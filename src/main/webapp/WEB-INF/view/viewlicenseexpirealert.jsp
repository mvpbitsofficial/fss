<%-- 
    Document   : viewlicenseexpirealert
    Created on : 9 Aug, 2016, 2:24:32 PM
    Author     : CoreTechies M8
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>FSS | View License History</title>
    </head>
    <body>
        <h1>View License History!</h1>
        <h1>${message}</h1>
        <p>
            <jsp:useBean id="today" class="java.util.Date" />
            <b><c:out value="${today}"/></b>
        </p>
        <table>
            <tr>
                <td>Sr. No.</td>
                <td>User Name</td>
                <td>Expire Date</td>
                <td>Status</td>
            </tr>
            <c:forEach items="${licenseexpirealert}" var="license" varStatus="status">
                <tr>
                    <td>${status.count}</td>
                    <td>${license.userInfo.userName}</td>
                    <td><fmt:formatDate type="both" pattern="dd-MM-yyyy" value="${license.expiryDate}" />
                    <td>
                        <c:choose>
                            <c:when test="${license.expiryDate.time gt today.time}">
                                Active
                            </c:when>    
                            <c:otherwise>
                                Expired
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>