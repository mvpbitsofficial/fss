/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.domain;

import java.io.Serializable;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author CoreTechies M8
 */
@Entity
@Table(name = "mailsetting")
@Access(AccessType.FIELD)
public class MailSetting implements Serializable {

    @Id
    @GeneratedValue(generator = "mailsetting")
    @GenericGenerator(name = "mailsetting", strategy = "increment")
    private Integer mailSettingId;

    @Column
    private String smtpServer;

    @Column
    private String smtpUser;

    @Column
    private String smtpPassword;

    @Column
    private Integer mailPort;

    @Column
    private String mailFrom;

    @Column
    private String subject;
    
    @Column
    private String text;
    
    @Column
    private Integer userId;

    public Integer getMailSettingId() {
        return mailSettingId;
    }

    public void setMailSettingId(Integer mailSettingId) {
        this.mailSettingId = mailSettingId;
    }

    public String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    public String getSmtpUser() {
        return smtpUser;
    }

    public void setSmtpUser(String smtpUser) {
        this.smtpUser = smtpUser;
    }

    public String getSmtpPassword() {
        return smtpPassword;
    }

    public void setSmtpPassword(String smtpPassword) {
        this.smtpPassword = smtpPassword;
    }

    public Integer getMailPort() {
        return mailPort;
    }

    public void setMailPort(Integer mailPort) {
        this.mailPort = mailPort;
    }

    public String getMailFrom() {
        return mailFrom;
    }

    public void setMailFrom(String mailFrom) {
        this.mailFrom = mailFrom;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "MailSetting{" + "mailSettingId=" + mailSettingId + ", smtpServer=" + smtpServer + ", smtpUser=" + smtpUser + ", smtpPassword=" + smtpPassword + ", mailPort=" + mailPort + ", mailFrom=" + mailFrom + ", userId=" + userId + '}';
    }

}
