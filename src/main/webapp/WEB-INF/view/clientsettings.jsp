
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>FSS | Setting</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
 <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link href="${pageContext.request.contextPath}/dist/sweetalert.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="favicon.ico" />
<style>
	#other-email-section{ display:none;}
</style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top"> 
 <!-- BEGIN HEADER INNER -->
    <jsp:include page="topbar.jsp" />
 <!-- END HEADER INNER --> 
</div>
<!-- END HEADER --> 
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER --> 
<!-- BEGIN CONTAINER -->
<div class="page-container"> 
 <!-- BEGIN SIDEBAR -->
 <div class="page-sidebar-wrapper"> 
  <!-- BEGIN SIDEBAR --> 
  <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
  <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
  <div class="page-sidebar navbar-collapse collapse"> 
   <!-- BEGIN SIDEBAR MENU --> 
   <jsp:include page="sidebar.jsp" />
<!--   <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
     DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
    <li class="sidebar-toggler-wrapper hide"> 
      BEGIN SIDEBAR TOGGLER BUTTON 
     <div class="sidebar-toggler"> <span></span> </div>
      END SIDEBAR TOGGLER BUTTON  
    </li>
    <li class="nav-item  "> <a href="index.html" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
    <li class="nav-item  "> <a href="users.html" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
     
    </li>
    <li class="nav-item  "> <a href="licence.html" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">Licence</span>  </a>
     
    </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
     
    </li>
    <li class="nav-item  "> <a href="report.html" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
    <li class="nav-item  "> <a href="watermark.html" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
    <li class="nav-item  active"> <a href="settings.html" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
   </ul>-->
   <!-- END SIDEBAR MENU --> 
   <!-- END SIDEBAR MENU --> 
  </div>
  <!-- END SIDEBAR --> 
 </div>
 <!-- END SIDEBAR --> 
 <!-- BEGIN CONTENT -->
 <div class="page-content-wrapper"> 
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content"> 
   <!-- BEGIN PAGE HEADER--> 
   
   <!-- BEGIN PAGE TITLE-->
   <!--<h3 class="page-title"><i class="icon-docs "></i> Report
    <!-- <small>blank page layout</small>--
   </h3>-->
   <!-- END PAGE TITLE--> 
   <!-- END PAGE HEADER-->
   
 
  <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PROFILE SIDEBAR -->
                            
                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content">
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered  box-shadow">
                                            <div class="portlet-title tabbable-line">
                                                <div class="caption caption-md" >
                                                    <i class="icon-globe theme-font hide"></i>
                                                    <span class="caption-subject  bold uppercase"><i class="icon-settings "></i> Settings</span>
                                                </div>
                                                <ul class="nav nav-tabs ">
                                                    <li class="active">
                                                        <a href="#tab_1_1" data-toggle="tab">Profile</a>
                                                    </li>
                                                     <li class="">
                                                        <a href="#tab_1_2" data-toggle="tab">Email</a>
                                                    </li>
<!--                                                    <li class="">
                                                        <a href="#tab_1_4" data-toggle="tab">Notification</a>
                                                    </li>
                                                    <li class="">
                                                        <a href="#tab_1_5" data-toggle="tab">Default Settings</a>
                                                    </li>-->
                                                    <li class="">
                                                        <a href="#tab_1_6" data-toggle="tab">Other Settings</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="portlet-body">
						<div class="tab-content">
                                                    <div class="tab-pane active " id="tab_1_1">
							<div class="row clearfix">
                                                            <div class="clearfix col-sm-12" >
								<div class="portlet box red   portlet-datatable" style="margin-bottom:0px;">
                                                                    <div class="portlet-title">
									<div class="caption">
									Profile Settings
									</div>
                                                                    </div>
                                                                    <div class="portlet-body">
									<div class="col-sm-6" style="margin:0 auto; float:none;">
                                                                            <form action="javascript:;" class="form-horizontal">
                                                                                <div class="form-body">
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-4 control-label">User Name</label>
                                                                                            <div class="col-sm-8">
												<div class="input-group">
                                                                                                    <span class="input-group-addon input-circle-left">
													<i class="fa fa-user"></i>
                                                                                                    </span>
                                                                                                    <input type="text" class="form-control input-circle-right" readonly placeholder="" value="${user.userName}">
												</div>
                                                                                                
                                                                                            <!--<span class="help-block"> A block of help text. </span>-->
                                                                                            </div>
                                                                                    </div>
											
                                                                                    <div class="form-group">
                                                                                    <label class="col-sm-4 control-label">Current Password</label>
                                                                                        <div class="col-sm-8">
                                                                                            <div class="input-group">
												<span class="input-group-addon input-circle-left">
                                                                                                    <i class="fa fa-lock"></i>
												</span>
												<input type="password" class="form-control input-circle-right" placeholder="Current Password" value="" id="currentPassword" />
                                                                                            </div>
                                                                                            <span class="error-message" id="errorMessageCurrentPassword"></span>
                                                                                        <!--<span class="help-block"> A block of help text. </span>-->
                                                                                        </div>
                                                                                    </div>
                                                                                                
                                                                                    <div class="form-group">
                                                                                    <label class="col-sm-4 control-label">New Password</label>
                                                                                        <div class="col-sm-8">
                                                                                            <div class="input-group">
												<span class="input-group-addon input-circle-left">
                                                                                                    <i class="fa fa-lock"></i>
												</span>
												<input  type="password" class="form-control input-circle-right" placeholder="New Password" value="" id="newPassword"/>
                                                                                            </div>
                                                                                            <span class="error-message" id="errorMessageNewPassword"></span>
                                                                                        <!--<span class="help-block"> A block of help text. </span>-->
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-4 control-label">Confirm Password</label>
                                                                                            <div class="col-sm-8">
                                                                                                <div class="input-group">
                                                                                                    <span class="input-group-addon input-circle-left">
													<i class="fa fa-lock"></i>
                                                                                                    </span>
                                                                                                    <input type="password" class="form-control input-circle-right" placeholder="Confirm Password" value="" id="confirmPassword">
												</div>
                                                                                                <span class="error-message" id="errorMessageConfirmPassword"></span>
                                                                                            <!--<span class="help-block"> A block of help text. </span>-->
                                                                                            </div>
                                                                                    </div>
                                                      
                                                                                </div>
                                                                                <div class="form-actions">
                                                                                    <div class="row"> 
                                                                                        <div class="col-md-offset-4 col-md-8 text-right">
                                                                                            <span  style="color: #1e9765; font-size: 14px; display: block;" id="errorMessageChangePassword"></span>
                                                                                            <button type="submit" class="btn btn-circle btn-success" id="changePassword">Update</button>
                                                                                            <!--<button type="button" class="btn btn-circle grey-salsa btn-outline red-hover">Cancel</button>-->
                                                                                            
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
									</div>
                                                                    </div>							
								</div>
                                                            </div>
							</div>
                                                    </div>
							<div class="tab-pane  " id="tab_1_2">
                                                            <div class="row clearfix">
								<div class="clearfix col-sm-12" >
                                                                    <div class="portlet box red   portlet-datatable" style="margin-bottom:0px;">
									<div class="portlet-title">
                                                                            <div class="caption">
										Email Settings
                                                                            </div>
									</div>
									<div class="portlet-body">
                                                                            <div class="col-sm-6" style="margin:0 auto; float:none;">
										<form action="javascript:;" class="form-horizontal">
                                                                                    <div class="form-body">
                                                                                        <div class="form-group">
                                                                                            <label class="col-sm-4 control-label">SMTP Server</label>
                                                                                            <div class="col-sm-8">
												<input type="text" class="form-control input-circle" placeholder="mail.gandi.net" value="${mailsetting.smtpServer}" id="smtpserver" >
                                                                                                <span style="color: red; display: block; font-size: 14px;" id="errorsmtpserver" ></span>
												<!--<span class="help-block"> A block of help text. </span>-->
                                                                                            </div>
                                                                                        </div>
											<div class="form-group">
                                                                                            <label class="col-sm-4 control-label">SMTP User</label>
                                                                                            <div class="col-sm-8">
												<input type="text" class="form-control input-circle" placeholder="SMTP User" value="${mailsetting.smtpUser}" id="smtpuser">
                                                                                                <span style="color: red; display: block; font-size: 14px;" id="errorsmtpuser" ></span>
												<!--<span class="help-block"> A block of help text. </span>-->
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label class="col-sm-4 control-label">Mail From</label>
                                                                                            <div class="col-sm-8">
												<input type="text" class="form-control input-circle" placeholder="info@fss.co.in" value="${mailsetting.mailFrom}" id="mailform">
                                                                                                <span style="color: red; display: block; font-size: 14px;" id="errormailform" ></span>
                                                                                                <!--<span class="help-block"> A block of help text. </span>-->
                                                                                            </div>
                                                                                        </div>
											<div class="form-group">
                                                                                            <label class="col-sm-4 control-label">Password</label>
                                                                                            <div class="col-sm-8">
                                                                                                <input type="password" class="form-control input-circle" placeholder="Password" value="${mailsetting.smtpPassword}" id="smtppassword">
                                                                                                <span style="color: red; display: block; font-size: 14px;" id="errorsmtppassword" ></span>
												<!--<span class="help-block"> A block of help text. </span>-->
                                                                                            </div>
                                                                                        </div>
											<div class="form-group">
                                                                                            <label class="col-sm-4 control-label">Mail Port</label>
                                                                                            <div class="col-sm-8">
												<input type="text" class="form-control input-circle" placeholder="Mail Port" value="${mailsetting.mailPort}" id="mailport">
                                                                                                <span style="color: red; display: block; font-size: 14px;" id="errormailport" ></span>
												<!--<span class="help-block"> A block of help text. </span>-->
                                                                                            </div>
                                                                                        </div>
<!--                                                                                        <div class="form-group">
                                                                                            <label class="col-sm-3 control-label">Subject</label>
                                                                                            <div class="col-sm-8">
												<input type="text" class="form-control input-circle" placeholder="Please Enter Subject" value="${mailsetting.subject}" id="subject">
                                                                                                <span style="font-size: smaller; font-weight:bold;  color: red" id="errorsubject" ></span>
												<span class="help-block"> A block of help text. </span>
                                                                                            </div>
                                                                                        </div>
											<div class="form-group">
                                                                                            <label class="col-sm-3 control-label">Text</label>
                                                                                            <div class="col-sm-8">
												<textarea  class="form-control input-circle" placeholder="Please Enter Some Text" value="" id="text">${mailsetting.text}</textarea>
                                                                                                <span style="font-size: smaller; font-weight:bold;  color: red" id="errortext" ></span>
												<span class="help-block"> A block of help text. </span>
                                                                                            </div>
                                                                                        </div>-->
                                                                                    </div>
                                                                                    <div class="form-actions">
                                                                                        <div class="row">
                                                                                            <div class="col-md-offset-4 col-md-8 text-right">
                                                                                                <span style="color: green; display: block; font-size: 14px;" id="updateresponse" ></span>
                                                                                                    <c:if test="${mailsetting == null}">
                                                                                                        <button type="submit" class="btn btn-circle btn-success" onclick="mailsettingchanges()">Add</button>
                                                                                                    </c:if>
                                                                                                    <c:if test="${mailsetting != null}">
                                                                                                        <button type="submit" class="btn btn-circle btn-success" onclick="mailsettingchanges()">Update</button>
                                                                                                    </c:if>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
									</div>
                                                                    </div>							
								</div>
                                                            </div>
							</div>
													
													
							<div class="tab-pane  " id="tab_1_4">
                                                            <div class="row clearfix">
								<div class="clearfix col-sm-12" >
                                                                    <div class="portlet box red   portlet-datatable" style="margin-bottom:0px;">
									<div class="portlet-title">
                                                                            <div class="caption">
										Notification Settings
                                                                            </div>
									</div>
									<div class="portlet-body">
                                                                            <div class="col-sm-6" style="margin:0 auto; float:none;">
										<div class="check">
                                                                                    <label class="mt-checkbox mt-checkbox-outline"> Select All
											<input type="checkbox" value="1" name="test">
											<span></span>
                                                                                    </label>
										</div>
										<div class="sub-check">
                                                                                    <label class="mt-checkbox mt-checkbox-outline"> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
											<input type="checkbox" value="1" name="test">
											<span></span>
                                                                                    </label>
										</div>
										<div class="sub-check">
                                                                                    <label class="mt-checkbox mt-checkbox-outline"> Nulla laoreet elit sit amet hendrerit suscipit.
											<input type="checkbox" value="1" name="test">
											<span></span>
                                                                                    </label>
										</div>
										<div class="sub-check">
                                                                                    <label class="mt-checkbox mt-checkbox-outline"> Suspendisse volutpat diam in metus varius consequat.
											<input type="checkbox" value="1" name="test">
											<span></span>
                                                                                    </label>
										</div>
																		
										<div class="form-group text-right">
                                                                                    <button class="btn grey-salsa btn-outline red-hover btn-circle">Save</button>
										</div>
                                                                            </div>
									</div>
                                                                    </div>							
								</div>
                                                            </div>
							</div>
													
							<div class="tab-pane  " id="tab_1_5">
                                                            <div class="row clearfix">
								<div class="clearfix col-sm-12" >
                                                                    <div class="portlet box red   portlet-datatable" style="margin-bottom:0px;">
									<div class="portlet-title">
                                                                            <div class="caption">
										Default Settings
                                                                            </div>
									</div>
									<div class="portlet-body">
                                                                            <div class="col-sm-8" style="margin:0 auto; float:none;">
																		
                                                                            </div>
									</div>
                                                                    </div>							
                                                                </div>
                                                            </div>
							</div>
							<div class="tab-pane  " id="tab_1_6">
                                                            <div class="row clearfix">
								<div class="clearfix col-sm-12" >
                                                                    <div class="portlet box red   portlet-datatable" style="margin-bottom:0px;">
									<div class="portlet-title">
                                                                            <div class="caption">
										Other Settings
                                                                            </div>
									</div>
									<div class="portlet-body">
																
                                                                            <div class="col-sm-8" style="margin:0 auto; float:none;">
										<!--<h4>Logout Time</h4>-->
										<form action="javascript:;" class="form-horizontal">
                                                                                    <div class="form-body">
											<div class="form-group">
                                                                                            <label class="col-sm-4 control-label">Automatic Logout Time (In Minutes)</label>
                                                                                            <div class="col-sm-6">
												<input id="touchspin_1" type="text" value="${userinfo.logoutTime}" name="demo1"  class="form-control input-circle ">
                                                                                                <span style="color: red; display: block; font-size: 14px;" id="timeerror" ></span>
                                                                                                <span style="color: green; display: block; font-size: 14px;" id="timesuccess" ></span>
                                                                                            </div>
                                                                                            <c:if test="${userinfo.logoutTime == null}">
                                                                                                <div class="col-sm-2 text-right">
                                                                                                    <a href="javascript:;" class="btn btn-circle btn-success" id="savelogouttime">Add</a>
                                                                                                </div>    
                                                                                            </c:if>
                                                                                            <c:if test="${userinfo.logoutTime != null}">
                                                                                                <div class="col-sm-2 text-right">
                                                                                                    <a href="javascript:;" class="btn btn-circle btn-success" id="savelogouttime">Update</a>
                                                                                                </div>    
                                                                                            </c:if>
											</div>
                                                                                    </div>
										</form>
<!--										<hr>
																	
										<form action="javascript:;" class="form-horizontal">
                                                                                    <div class="form-body">
											<div class="form-group">
                                                                                            <label class="col-sm-4 control-label">Security Key</label>
                                                                                            <div class="col-sm-6">
                                                                                                <div class="input-group">
                                                                                                    <span class="input-group-addon input-circle-left">
                                                                                                            <i class="fa fa-key"></i>
                                                                                                    </span>
                                                                                                    <c:choose>
                                                                                                        <c:when test="${userinfo.securityKey == null}">
                                                                                                            <input id="securitykey" class="form-control input-circle-right" type="password" value="">
                                                                                                        </c:when>
                                                                                                        <c:otherwise>
                                                                                                            <input id="securitykey1" class="form-control input-circle-right" readonly type="password" value="${userinfo.securityKey}">
                                                                                                        </c:otherwise>
                                                                                                    </c:choose> 
                                                                                                </div>
                                                                                                <span class="error-message" id="keyerror" ></span>
                                                                                                <span style="color: green; display: block; font-size: 14px;" id="keysuccess" ></span>
                                                                                            </div>
                                                                                            <div class="col-sm-2 text-right">
                                                                                                <c:choose>
                                                                                                    <c:when test="${userinfo.securityKey == null}">
                                                                                                        <a href="javascript:;" class="btn btn-circle btn-success" id="addsecuritykey">Add</a>
                                                                                                    </c:when>
                                                                                                    <c:otherwise>
                                                                                                        <a href="javascript:;" class="btn btn-circle btn-success" id="viewsecuritykey">View</a>
                                                                                                    </c:otherwise>
                                                                                                </c:choose>
                                                                                            </div>
											</div>
                                                                                    </div>
										</form>
										<hr>-->
																	
<!--										<form action="javascript:;" class="form-horizontal">
                                                                                    <div class="form-body">
											<div class="form-group">
                                                                                            <label class="col-sm-4 control-label">Select Question</label>
                                                                                            <div class="col-sm-6">
												<select class="form-control input-circle selectpicker" data-live-search='true' data-live-search-style='startsWith' id="questionnumber">
                                                                                                    <option selected disabled>--Select Questions No--</option>
                                                                                                    <c:forEach items="${questions}" var="quest">
                                                                                                        <option value="${quest.securityQuestionId}">${quest.securityQuestionId}</option>
                                                                                                    </c:forEach>
												</select>
                                                                                            </div>
											</div>
											<div class="form-group">
                                                                                            <label class="col-sm-4 control-label"> Question</label>
												<div class="col-sm-6">
                                                                                                    <div class="input-group">
                                                                                                        <span class="input-group-addon input-circle-left">
                                                                                                                <i class="fa fa-question"></i>
                                                                                                        </span>
                                                                                                        <input  type="text" class="form-control input-circle-right" readonly value="" id="selectedquestion">
                                                                                                    </div>
                                                                                                </div>
											</div>
											<div class="form-group">
                                                                                            <label class="col-sm-4 control-label"> Answer</label>
                                                                                            <div class="col-sm-6">
                                                                                                <div class="input-group">
                                                                                                    <span class="input-group-addon input-circle-left">
                                                                                                            <i class="fa fa-check-circle"></i>
                                                                                                    </span>
                                                                                                    <input  type="text" class="form-control input-circle-right" id="selectedanswer">
                                                                                                </div>
                                                                                                <span class="error-message" id="securityerror" ></span>
                                                                                                <span style="color: green; display: block; font-size: 14px;" id="securitysuccess" ></span>
                                                                                            </div>
											</div>
											<div class="form-group">
                                                                                            <div class="col-sm-10 text-right">
                                                                                                <button  class="btn btn-circle btn-success" type="button" id="editanswer"> Update</button>
                                                                                            </div>
											</div>
                                                                                    </div>
										</form>-->
                                                                            </div>
									</div>
                                                                    </div>							
								</div>
                                                            </div>
							</div>
                                                    </div>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <!-- END PROFILE CONTENT -->
                        </div>
                    </div>
  </div>
  <!-- END CONTENT BODY --> 
 </div>
 <!-- END CONTENT --> 
 <!-- BEGIN QUICK SIDEBAR --> 
 <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 
 
 <!-- END QUICK SIDEBAR --> 
</div>
<!-- END CONTAINER --> 
<!-- BEGIN FOOTER -->
<div class="page-footer">
 <div class="page-footer-inner"> 2016 &copy; Organization </div>
 <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
</div>
        <div class="modal fade" id="authenticate" tabindex="-1" role="authenticate" aria-hidden="true" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Authenticating</h4>
                    </div>
                    <div class="modal-body"> 
                        <div class="form-group clearfix">
                            <label class="col-sm-3 control-label">User Name</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon input-circle-left">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input type="text" placeholder="userName" id="username" class="form-control input-circle-right clearfix" />
                                </div>
                                <span style="color: red; display: block; font-size: 14px;" id="nameerror" ></span>
                            </div>
			</div>
                        <div class="form-group clearfix">
                            <label class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon input-circle-left">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                    <input type="password" placeholder="password" id="psw" class="form-control input-circle-right clearfix" />
                                </div>
                                <span style="color: red; display: block; font-size: 14px;" id="pswerror" ></span>
                            </div>
			</div>
                    </div>
                    <div class="modal-footer">
                        <span style="color: red; display: block; font-size: 14px; float: left;" id="authenticationerror" ></span>
                        <button type="button" class=" btn btn-circle btn-danger" data-dismiss="modal">Cancel</button>
<!--                        <button type="button" class="btn green" id="">YES</button>-->
                        <a href="javascript:;" class=" btn btn-circle btn-success" id="checkandviewkey">Continue</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
<!-- END FOOTER --> 

<!-- BEGIN CORE PLUGINS --> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/dist/sweetalert.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/dist/sweetalert-dev.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
<!-- END CORE PLUGINS --> 
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.css" rel="stylesheet" type="text/css" />-->
<!-- BEGIN THEME GLOBAL SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
<!-- END THEME GLOBAL SCRIPTS --> 

<!-- BEGIN THEME LAYOUT SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
<!-- END THEME LAYOUT SCRIPTS --> 
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="${pageContext.request.contextPath}/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
		       <!-- END PAGE LEVEL PLUGINS -->
<script src="${pageContext.request.contextPath}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<script src="${pageContext.request.contextPath}/assets/global/plugins/fuelux/js/spinner.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
  BEGIN PAGE LEVEL SCRIPTS 
<script src="${pageContext.request.contextPath}/assets/pages/scripts/components-bootstrap-touchspin.min.js" type="text/javascript"></script>-->
        <script>
                $('#other-email-btn').on('click', function(){
                        $('#other-email-section').show();
                });
        </script>
        <script>
            $(document).ready(function() {
                $('#side-menu').find('li').removeClass('active');
                $(document).find(".setting").addClass('active');
                
                $("#securitykey").val("");
              });
        </script>
        <script>
            $("#questionnumber").on("change", function(){
               var question = $("#questionnumber").val(); 
               console.log(question);
               $("#securityerror").text("");
               var Uname = "${user.userName}".replace(" ", "");
               $.post("${pageContext.request.contextPath}/"+Uname+"/getquestionanswer", {questionid: question},
                function(data, success){
                    console.log(data);
                    if(data.indexOf("success") > -1){
                        var questionanswer = data.split("#")[1];
                        $("#selectedquestion").val(questionanswer.split("*")[0]);
                        $("#selectedanswer").val(questionanswer.split("*")[1]);
                    } else{
                        $("#securityerror").text(data);
                    }
                });
            });
            
            $("#editanswer").on("click", function(){
                var question = $("#questionnumber").val(); 
                var answer = $("#selectedanswer").val(); 
                
                var Uname = "${user.userName}".replace(" ", "");
                $.post("${pageContext.request.contextPath}/"+Uname+"/editsecurityanswer", {questionId: question, answer: answer},
                function(data, success){
                    console.log(data);
                    if(data === "success"){
                        $("#securitysuccess").text("Security question has been updated successfully.");
                    } else{
                        $("#securityerror").text(data);
                    }
                });
            });
            
            $("#currentPassword").on("change", function(){
                var currentPassword = $("#currentPassword").val();
                if (currentPassword === null || currentPassword === "") {
                    $("#errorMessageCurrentPassword").text("Please enter current password.");
                } else{
                    $("#errorMessageCurrentPassword").text("");
                }
            });
            $("#newPassword").on("change", function(){
                var newPassword = $("#newPassword").val();
                if (newPassword === null || newPassword === "") {
                    $("#errorMessageNewPassword").text("Please enter new password.");
                } else{
                    $("#errorMessageNewPassword").text("");
                }
            });
            $("#confirmPassword").on("change", function(){
                var confirmPassword = $("#confirmPassword").val();
                if (confirmPassword === null || confirmPassword === "") {
                    $("#errorMessageConfirmPassword").text("Please enter confirm password.");
                    flag = false;
                } else{
                    $("#errorMessageConfirmPassword").text("");
                }
            });
            
            $(document).on("click", "#changePassword", function () {
                var currentPassword = $("#currentPassword").val();
                var newPassword = $("#newPassword").val();
                var confirmPassword = $("#confirmPassword").val();
                $("#errorMessageConfirmPassword").text("");
                var flag = true;
                if (currentPassword === null || currentPassword === "") {
                    $("#errorMessageCurrentPassword").text("Please enter current password.");
                    flag = false;
                }
                if (newPassword === null || newPassword === "") {
                    $("#errorMessageNewPassword").text("Please enter new password.");
                    flag = false;
                }
                if (confirmPassword === null || confirmPassword === "") {
                    $("#errorMessageConfirmPassword").text("Please enter confirm password.");
                    flag = false;
                }
                if (newPassword !== confirmPassword) {
                    $("#errorMessageConfirmPassword").text("New Password and confirm password are mismatch.");
                    flag = false;
                }
                if (flag) {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/changepassword", {currentPassword: currentPassword, newPassword: newPassword},
                            function (data, status) {
                                if (data === "success") {
                                    $("#currentPassword").val("");
                                    $("#newPassword").val("");
                                    $("#confirmPassword").val("");
                                    $("#errorMessageConfirmPassword").text("");
                                    $("#errorMessageChangePassword").text("Your password has been changed successfully");
                                    setTimeout(function()
                                        {
                                            window.location.reload();
                                        }, 3000);
                                    //window.location.href = "${pageContext.request.contextPath}/";
                                } else if (data === "Please login again.") {
                                    window.location.href = "${pageContext.request.contextPath}/";
                                } else {
                                    $("#errorMessageChangePassword").text(data);
                                }
                            }
                    );
                }
            });
            
        function mailsettingchanges(){
            var validation = true;
                var smtpserver = $("#smtpserver").val();
                var smtpuser = $("#smtpuser").val();
                var smtppassword = $("#smtppassword").val();
                var mailport = $("#mailport").val();
                var mailform = $("#mailform").val();
                var subject = "";
                var text = "";
                var emailExp= /^[0-9a-zA-Z-.+]+@[a-zA-Z0-9.-]+.[a-zA-z0-9]{2,4}$/;
                var serverExp= /^[0-9a-zA-Z-.+]+.[a-zA-Z0-9.-]+.[a-zA-z0-9]{2,4}$/;
                var numericExp=/^[0-9]+$/;
                var alphaExp=/^[a-zA-Z ]+$/;
                if(smtpserver===""){
                    $("#errorsmtpserver").text("Please enter SMTP Server.");
                    validation = false;
                } else if(smtpserver.match(serverExp)){
                    $("#errorsmtpserver").text("");
                } else{
                    $("#errorsmtpserver").text("Please enter Valid SMTP Server.");
                    validation = false;
                }
                
                if(smtpuser===""){
                    $("#errorsmtpuser").text("Please enter SMTP User.");
                    validation = false;
                } else if(smtpuser.match(alphaExp)){
                    $("#errorsmtpuser").text("");
                } else{
                    $("#errorsmtpuser").text("Please enter Valid SMTP User.");
                    validation = false;
                }
                
                if(smtppassword===""){
                    $("#errorsmtppassword").text("Please enter Password.");
                    validation = false;
                } else if(smtppassword.length <= 4){
                    $("#errorsmtppassword").text("Password must have atleast 5 characters.");
                    validation = false;
                } else if(smtppassword.trim()===""){
                    $("#errorsmtppassword").text("Please don't enter spaces.");
                    validation = false;
                } else{
                    $("#errorsmtppassword").text("");
                }
                
                if(mailport===""){
                    $("#errormailport").text("Please enter Mail Port.");
                    validation = false;
                } else if(mailport.match(numericExp)){
                    $("#errormailport").text("");
                } else{
                    $("#errormailport").text("Please enter Valid port.");
                    validation = false;
                }
                
                if(mailform===""){
                    $("#errormailform").text("Please enter Mail Form.");
                    validation = false;
                } else if(mailform.match(emailExp)){
                    $("#errormailform").text("");
                } else{
                    $("#errormailform").text("Please enter Valid Mail Form.");
                    validation = false;
                }
                
                if(validation){
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/savemailsettings", {SMTPServer: smtpserver, SMTPUser: smtpuser, Password: smtppassword, Mailport: mailport, mailform: mailform, subject: subject, text: text},
                            function (data, status) {
                                console.log(data);
                                if (data === "success") {
                                    $("#updateresponse").text("Mail Settings has been updated succesfully.");
                                } else if (data === "Please login again.") {
                                     $("#updateresponse").val("");
                                    window.location.href = "${pageContext.request.contextPath}/";
                                } else {
                                    $("#updateresponse").text(data);
                                }
                            }
                    );
                }
            }
        </script>
        <script>
            $("#savelogouttime").on("click", function(){
                var logout = $("#touchspin_1").val();
                var numericExp=/^[0-9]+$/;
                var valid = true;
                console.log(logout);
                if(logout ===""){
                    $("#timeerror").text("Please Enter Logout Time");
                    valid = false;
                } else if(logout.match(numericExp)){
                    $("#timeerror").text("");
                } else{
                    $("#timeerror").text("Please Enter Valid Time");
                    valid = false;
                }
                if(valid){
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/savelogouttime", {logouttime: logout},
                            function (data, status) {
                                console.log(data);
                                if (data === "success") {
                                    swal({
                                        title: "Sweet!",
                                        text: "Logout time has been saved successfully.",
                                        imageUrl: "images/thumbs-up.jpg"
                                      });
                                    $("#timesuccess").text("Time Successfully saved");
                                    setTimeout(function()
                                    {
                                        window.location.reload();
                                    }, 1000);
                                } else if (data === "Please login again.") {
                                     $("#timeerror").val("");
                                    window.location.href = "${pageContext.request.contextPath}/";
                                } else {
                                    $("#timeerror").text(data);
                                }
                            }
                    );
                }
                
            });
            
            $("#addsecuritykey").on("click", function(){
                var securityKey = $("#securitykey").val();
                var numericExp=/^[0-9]+$/;
                var valid = true;
                if(securityKey ===""){
                    $("#keyerror").text("Please Enter Security Key");
                    valid = false;
                } else if(securityKey.match(numericExp)){
                     $("#keyerror").text("");
                } else{
                    $("#keyerror").text("Security key Support only Integers.");
                    valid = false;
                }
                if(valid){
                var Uname = "${user.userName}".replace(" ", "");
                $.post("${pageContext.request.contextPath}/"+Uname+"/addsecuritykey", {securitykey: securityKey},
                            function (data, status) {
                                console.log(data);
                                if (data === "success") {
                                    $("#keysuccess").text("Security Key has been saved scuccessfully.");
                                    setTimeout(function()
                                        {
                                            window.location.reload();
                                        }, 1000);
                                } else if (data === "Please login again.") {
                                     $("#keyerror").val("");
                                    window.location.href = "${pageContext.request.contextPath}/";
                                } else {
                                    $("#keyerror").text(data);
                                }
                            }
                    );
                }
            });
            
            $("#viewsecuritykey").on("click", function(){
                var security = $("#securitykey1").val();
                $("#nameerror").text("");
                $("#pswerror").text("");
                console.log(security);
                $("#authenticate").modal("show");
            });
            
            $("#checkandviewkey").on("click", function(){
                var username = $("#username").val();
                var password = $("#psw").val();
                var flag = true;
                if(username === ""){
                    $("#nameerror").text("Please Enter UserName");
                    flag = false;
                }
                if(password === ""){
                    $("#pswerror").text("Please Enter Password");
                    flag = false;
                }
                if(flag){
                $.post("${pageContext.request.contextPath}/login", {userName: username, password: password},
                            function (data, status) {
                                console.log(data);
                                if (data.indexOf("success") > -1) {
                                    $("#nameerror").text("");
                                    $("#pswerror").text("");
                                    $("#securitykey1").prop("type", "text");
                                    $("#authenticate").modal("hide");
                                } else {
                                    $("#nameerror").text("");
                                    $("#pswerror").text("");
                                    $("#authenticationerror").text(data);
                                }
                            }
                    );
                }
            });
        </script>
        <script>
        $( ".form-control" ).focus(function() {
            $(this).prev('.input-group-addon').removeClass().addClass('input-group-addon-focus');
            $(this).next('.input-group-addon').removeClass().addClass('input-group-addon-focus');
        });

        $( ".form-control" ).focusout(function() {
            $(this).prev('.input-group-addon-focus').removeClass().addClass('input-group-addon');
            $(this).next('.input-group-addon-focus').removeClass().addClass('input-group-addon');
        });
        </script>
</body>
</html>
