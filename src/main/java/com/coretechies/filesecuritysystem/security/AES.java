/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.security;

import java.util.ArrayList;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES {

    public String IV = "AAAAAAAAAAAAAAAA";
    static String plaintext = "as#101#12/8/2015#19/8/2015#12"; /*Note null padding*/

    public String encryptionKey = "0123456789abcdef";
    public String key = "";

    public String generate(String plaintext) {
        try {

            System.out.println("==Java==");
            System.out.println("plain:   " + plaintext);
            ArrayList<String> textList = new ArrayList<>();
            for (int i = 0; plaintext.length() >= 16; i++) {
                textList.add(plaintext.substring(0, 16));
                plaintext = plaintext.substring(16);
            }
            if (plaintext.length() < 16) {
                System.out.println("B" + plaintext);
                int ptl = plaintext.length();
                for (int i = 0; i < (16 - ptl); i++) {
                    plaintext = plaintext + "\0";
//                    System.out.println("P" + plaintext);
                }
                textList.add(plaintext);
            }
//            System.out.println("SIZ" + textList.size());
            for (String plaintexta : textList) {
//                System.out.println("E" + plaintexta);
                byte[] cipher = encrypt(plaintexta, encryptionKey);

//                System.out.print("cipher:  " + new String(cipher, 0, cipher.length));
                for (int i = 0; i < cipher.length; i++) {
//                    Integer integer = new Integer(cipher[i]);
//                    System.out.print(new Integer(cipher[i]) + " ");
//                    System.out.println("S:" + Integer.toHexString((int) cipher[i]));
//                    if (Integer.toHexString((int) cipher[i]).length() != 1) {
//                        key = key + Integer.toHexString((int) cipher[i]).substring(Integer.toHexString((int) cipher[i]).length() - 2);
//                    } else {
//                        key = key +"0"+ Integer.toHexString((int) cipher[i]);
//                    }
                    String f = "00" + Integer.toHexString(cipher[i]);
                    key = key + f.substring(f.length() - 2);
//                    System.out.println("key : " + key);
                }
//                System.out.println("");

//                String decrypted = decrypt(cipher, encryptionKey);
//                System.out.println("decrypt: " + decrypted);
            }
            System.out.println("KEY:" + key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return key;
    }

    public byte[] encrypt(String plainText, String encryptionKey) throws Exception {

        Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", "SunJCE");
        SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
        return cipher.doFinal(plainText.getBytes("UTF-8"));
    }

    public String decrypt(String generate, String encryptionKey) throws Exception {

        String result = "";
        Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", "SunJCE");
        SecretKeySpec key1 = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
        cipher.init(Cipher.DECRYPT_MODE, key1, new IvParameterSpec(IV.getBytes("UTF-8")));
        while (generate.length() > 0) {
            String generate1 = generate;
            if(generate.length() >= 32){
                generate1 = generate.substring(0, 32);
                generate = generate.substring(32);
            }
            byte[] bs = new byte[generate1.length() / 2];
            for (int i = 0; generate1.length() > 0; i++) {
                bs[i] = (byte) Integer.parseInt(generate1.substring(0, 2), 16);
                System.out.println("generate : " + generate1);
                generate1 = generate1.substring(2);
            }
            System.out.println("bs : "+bs);
//            result = result + aes.decrypt(bs, "0123456789abcdef");
            result = result + new String(cipher.doFinal(bs), "UTF-8");
            System.out.println("result : " + result);
            System.out.println("remaining string :: "+generate);
        }
        System.out.println("result : " + result);

//            String decrypted = decrypt(bs, encryptionKey);
//            System.out.println("decrypt: " + decrypted);
//        Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", "SunJCE");
//        SecretKeySpec key1 = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
//        cipher.init(Cipher.DECRYPT_MODE, key1, new IvParameterSpec(IV.getBytes("UTF-8")));
//        return new String(cipher.doFinal(bs), "UTF-8");
        return result;
    }

    public String decrypt(byte[] bs, String encryptionKey) throws Exception {
//            byte[] bs = new byte[key.length() / 2];
//            for (int i = 0; key.length() > 0; i++) {
//                bs[i] = (byte) Integer.parseInt(key.substring(0, 2), 16);
//                System.out.println("key : " + key);
//                key = key.substring(2);
//            }
//            String decrypted = decrypt(bs, encryptionKey);

//            System.out.println("decrypt: " + decrypted);
        Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", "SunJCE");
        SecretKeySpec key1 = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
        cipher.init(Cipher.DECRYPT_MODE, key1, new IvParameterSpec(IV.getBytes("UTF-8")));
        return new String(cipher.doFinal(bs), "UTF-8");
    }
}
