<%-- 
    Document   : changepassword
    Created on : 9 Aug, 2016, 2:24:32 PM
    Author     : CoreTechies M8
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>FSS | View License</title>
    </head>
    <body>
        <h1>View License!</h1>
        <h1>${message}</h1>
        <table>
            <tr>
                <td>Sr. No.</td>
                <td>User Id</td>
                <td>License</td>
                <td>Action</td>
            </tr>
            <c:forEach items="${userlicenses}" var="license" varStatus="status">
                <tr>
                    <td>${status.count}</td>
                    <td>${license.userId}</td>
                    <td>${license.license}</td>
                    <td>
                        <a href="${pageContext.request.contextPath}/${user.userName}/deletelicense/${license.userLicenseId}">Delete</a>
                        <a href="${pageContext.request.contextPath}/${user.userName}/renewlicense/${license.userLicenseId}">Renew</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>