<!DOCTYPE html>

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>FSS | Admin</title>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
 <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />

        <style>
            .cke_panel{z-index: 10060!important;}
        </style>
        <style>
            .mt-widget-1{ min-height:245px;}
            .mt-body{ margin-top:120px;}
            .user-alphabate{
                display: block;
             position: relative;
                -webkit-transition: all .4s linear;
                transition: all .4s linear;
                width: 100%;
                height: 85%;
                line-height:150px;
                background:#36c6d3;
                font-size:30px;
                color:#fff;
                text-shadow:0px 1px 2px rgba(0,0,0,0.2);
            }

            .big-text{ font-size:45px;}
            .mt-element-overlay .mt-overlay-1:hover .user-alphabate{
            -ms-transform: scale(1.2) translateZ(0);
               -webkit-transform: scale(1.2) translateZ(0);
               transform: scale(1.2) translateZ(0);
            }
 
        </style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top"> 
 <!-- BEGIN HEADER INNER -->
 <jsp:include page="topbar.jsp" />
 <!-- END HEADER INNER --> 
</div>
<!-- END HEADER --> 
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER --> 
<!-- BEGIN CONTAINER -->
<div class="page-container"> 
 <!-- BEGIN SIDEBAR -->
 <div class="page-sidebar-wrapper"> 
  <!-- BEGIN SIDEBAR --> 
  <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
  <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
  <div class="page-sidebar navbar-collapse collapse"> 
   <!-- BEGIN SIDEBAR MENU --> 
   <jsp:include page="sidebar.jsp" />
<!--  <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
     DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
    <li class="sidebar-toggler-wrapper hide"> 
      BEGIN SIDEBAR TOGGLER BUTTON 
     <div class="sidebar-toggler"> <span></span> </div>
      END SIDEBAR TOGGLER BUTTON  
    </li>
    <li class="nav-item  "> <a href="index.jsp" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
    <li class="nav-item  active"> <a href="users.html" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
     
    </li>
    <li class="nav-item  "> <a href="licence.html" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">Licence</span>  </a>
     
    </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
     
    </li>
    <li class="nav-item  "> <a href="report.html" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
    <li class="nav-item  "> <a href="watermark.html" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
    <li class="nav-item  "> <a href="settings.html" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
   </ul>-->
   <!-- END SIDEBAR MENU --> 
   <!-- END SIDEBAR MENU --> 
  </div>
  <!-- END SIDEBAR --> 
 </div>
 <!-- END SIDEBAR --> 
 <!-- BEGIN CONTENT -->
 <div class="page-content-wrapper"> 
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content"> 
   <!-- BEGIN PAGE HEADER--> 
   
   <!-- BEGIN PAGE TITLE-->
   <!--<h3 class="page-title pull-left"> <i class="icon-user "></i> Users 
    <!-- <small>blank page layout</small>--
   </h3>--> 
   <!-- END PAGE TITLE--> 
   <!-- END PAGE HEADER-->
  ${message}
 <div class="clearfix"></div>
  <div class="row">
		<div class="col-sm-12">
			<div class="portlet box red  portlet-datatable  ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-user  "></i>
                                        Users
                                    </div>
                                    <div class="actions">
                                        <c:set var="Uname" value="${user.userName}"/>
                                        <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/listview" class="btn"  data-toggle="tooltip" data-placement="top" title="List View"> <i class="icon-list"></i></a>
<!--                                        <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/viewclient" class=" btn" data-toggle="tooltip" data-placement="top" title="Grid View">
                                            <i class="icon-grid"></i>
                                        </a>-->
                                        
                                        <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/createclient" class=" btn" data-toggle="tooltip" data-placement="top" title="Add User">
                                            <i class="icon-plus"></i>  
                                        </a>
                                    </div>
                                  
                                </div>
                                <div class="portlet-body clearfix">
                                    <div class="mt-element-card mt-card-round  mt-element-overlay">
                                        <div class=" row">      
                                            
                                            <c:forEach items="${users}" var="userInfo">
                                               
                                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                <div class="mt-card-item">
							<div class="actions ">
                                                             <c:set var="Uname" value="${user.userName}"/>
								<div class="btn-group"> <a href="" class="btn  btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <i class="icon-options-vertical"></i> </a>
									<ul class="dropdown-menu pull-right">
                                                                            <li><a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/updateclient/${userInfo.userInfoId}"><i class="fa fa-edit"></i> Edit</a></li>
                                                                            <li><a href="#" class="deleteclient" id="${userInfo.userInfoId}"><i class="fa fa-trash-o"></i> Delete</a></li>
									</ul>
								</div>
							</div>
                                                        <div class="mt-card-avatar mt-overlay-1 " style="margin-bottom:0px;">
                                                            <c:if test="${userInfo.imagePath == null}">
                                                                <c:set var="Uname" value="${user.userName}"/>
                                                                <span><img alt="" class="img-circle" src="${pageContext.request.contextPath}/assets/layouts/layout/img/3.png" /></span>
                                                                <div class="mt-overlay mt-top">
                                                                    <ul class="mt-info">
                                                                        <li>
                                                                            <a class="btn default btn-outline"  href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/viewdetails/${userInfo.userInfoId}">
                                                                                <i class="icon-eye"></i>
                                                                            </a>
                                                                        </li>

                                                                    </ul>
                                                                </div>
                                                            </c:if>
                                                            <c:if test="${userInfo.imagePath != null}">
                                                                <span><img alt="" class="img-circle" src="${pageContext.request.contextPath}${userInfo.imagePath}" /></span>
                                                                <div class="mt-overlay mt-top">
                                                                    <c:set var="Uname" value="${user.userName}"/>
                                                                    <ul class="mt-info">
                                                                        <li>
                                                                            <a class="btn default btn-outline"  href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/viewdetails/${userInfo.userInfoId}">
                                                                                <i class="icon-eye"></i>
                                                                            </a>
                                                                        </li>

                                                                    </ul>
                                                                </div>
                                                            </c:if>
                                                            <!--<span class="user-alphabate big-text">${user.displayName}</span>-->
                                                        </div>	
                                                    <div class="mt-card-content">
                                                        <h3 class="mt-card-name">${userInfo.userName}</h3>
                                                        <p class="mt-card-desc font-grey-mint"><a href="#">${userInfo.emailId}</a></p>
							<p class="mt-card-desc font-grey-mint">+${userInfo.countryCode.mobileCode}-${userInfo.mobileNumber}</p>
                                                        <div class="mt-card-social">
                                                            <a  class="btn default sendMail" value="${userInfo.userInfoId}" data-toggle="tooltip" data-placement="top" title="Send Email">Email</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </div>
			</div>
                    </div>
   
  </div>
  <!-- END CONTENT BODY --> 
 </div>
 <!-- END CONTENT --> 
 <!-- BEGIN QUICK SIDEBAR --> 
 <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 
 
 <!-- END QUICK SIDEBAR --> 
</div>
<!-- END CONTAINER --> 
<!-- BEGIN FOOTER -->
<div class="page-footer">
 <div class="page-footer-inner"> 2016 &copy; Organization </div>
 <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
</div>
<!-- END FOOTER --> 
        <div class="modal fade" id="mailModal" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="quick_mail_modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">New Email</h4>
                        </div>
                    <form>
                        <div class="modal-body" >
                            <input type="text" id="displaymailinfo" readonly class="form-control input-circle" />
                            <br/>
                            <input type="text" class="form-control input-circle" id="subject" placeholder="Subject">
                            <span style="color: red; display: block; font-size: 14px;" id="subjectError" ></span>
                            <br/>
                            <textarea class="textarea" name="textboxtext" id="messageContent"></textarea>
                            <span style="color: red; display: block; font-size: 14px;" id="messageError" ></span>
                        </div>
                        <div id="loading" style="display: none;">
                            <center>
                            <!--<span style="color: blue;">Processing...</span>-->
                                <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/24.gif" />
                            </center>
                        </div>
                        <div class="modal-footer" style="margin-top:0px;">
                            <span style="color: green; display: block; font-size: 14px;" id="mailResponseMessage" ></span>
                            <button type="button" class="btn btn-primary btn-circle" data-dismiss="modal">Close</button>
                            <button type="button" id="sendMailToClient" class="btn btn-success btn-circle">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confirm_delete" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="">Confirm Delete</h4>
                    </div>
                    <div class="modal-body"> 
                        <div class="form-group">
                            <input type="hidden" id="deleteId" />
                            <span Style="font-style: normal; text-align: center;">Are You Sure To Want To Delete This User ?</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class=" btn btn green" data-dismiss="modal">NO</button>-->
<!--                        <button type="button" class="btn green" id="">YES</button>-->
                        <a href="javascript:;" class=" btn btn-circle btn-success" id="confirmdeleteclient">YES</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
<!-- BEGIN CORE PLUGINS --> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
<!-- END CORE PLUGINS --> 
<!-- BEGIN THEME GLOBAL SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
<!-- END THEME GLOBAL SCRIPTS --> 
<!-- BEGIN THEME LAYOUT SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/ckeditor/adapters/jquery.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
<!-- END THEME LAYOUT SCRIPTS --> 

	<script>
		$(function () {
                    $('[data-toggle="tooltip"]').tooltip();
		});
        </script>
        <script>
            $(document).ready(function () {
                $('#side-menu').find('li').removeClass('active');
                $(document).find(".user").addClass('active');
            });
        </script>
        <script>
            $(function() {
                $(".textarea").ckeditor();
            });
            var mailuser;
            $(".sendMail").on("click", function(e){
                mailuser = $(e.currentTarget).attr("value");
                $('.cke_wysiwyg_frame').contents().find('body').html("");
                $("#subject").val("");
                
                var Uname = "${user.userName}".replace(" ", "");
                $.post("${pageContext.request.contextPath}/"+Uname+"/getclientdetailsfordispaly", {userId: mailuser},
                    function (data, status) {
                        console.log(data);
                        if(data.indexOf("success") > -1){
                            var jsondata = JSON.parse(data.split("#")[1]);
                            $("#displaymailinfo").val(jsondata.Name+" ( "+jsondata.Email+" )");
                            $("#subjectError").html("");
                            $("#messageError").html("");
                            $("#subject").val("");
                            $("#messageContent").val("");
                            $("#mailModal").modal("show");
                        } else{
                            console.log(data);
                        }
                    });
            });
            
            $("#sendMailToClient").on("click", function(){
                $("#subjectError").html("");
                $("#messageError").html("");
                var subject = $("#subject").val();
                var messageContent = $("#messageContent").val();
                var flag = true;
                var flag1 = true;
                
                if (subject ==="") {
                    $("#subjectError").html("The Subject field is required.");
                    flag1 = false;
                } else if(subject.length <=3 || subject.length >=101) {
                    $("#subjectError").html("Subject accepts 4-100 character only");
                    flag1 = false;
                }else{
                    flag1 = true;
                }
                console.log(messageContent.length);
                if (messageContent=== "") {
                    $("#messageError").html("The Message field is required.");
                    flag = false;
                }else if(messageContent.length === 9){
                    $("#messageError").html("The Message field is required.");
                }else if ((messageContent.length-9) <2 || (messageContent.length-9) >=1001) {
                    $("#messageError").html("Message accepts 4-1000 character only");
                    flag = false;
                }else{
                    console.log(messageContent.length);
                    flag = true;
                }
                
                if(flag && flag1){
                    $("#loading").css('display','block');
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/sendmailtouser", {userId: mailuser, subject: subject, message: messageContent},
                    function (data, status) {
                        if(data === "success"){
                            $("#mailResponseMessage").html("The mail has been sent successfully.");
                            $("#loading").css('display','none');
                            setTimeout(function()
                            {
                                $("#mailModal").modal("hide");
                            }, 1000);
                        } else{
                            $("#mailResponseMessage").html(data);
                            $("#loading").css('display','none');
                        }
                    });
                }
            });
            
            $(".deleteclient").on("click", function(){
                $("#deleteId").val(this.id);
                $("#confirm_delete").modal("show");
            });
            
            $("#confirmdeleteclient").on("click", function(){
                var id = $("#deleteId").val();
                var Uname = "${user.userName}".replace(" ", "");
                window.location.href = "${pageContext.request.contextPath}/"+Uname+"/deleteclient/"+id;
            });
        </script>
</body>
</html>