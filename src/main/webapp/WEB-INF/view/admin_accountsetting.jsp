<!DOCTYPE html>

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>FSS | Profile</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
 <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<!--<link rel="shortcut icon" href="favicon.ico" />-->
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top"> 
 <!-- BEGIN HEADER INNER -->
 <jsp:include page="topbar.jsp" />
 <!-- END HEADER INNER --> 
</div>
<!-- END HEADER --> 
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER --> 
<!-- BEGIN CONTAINER -->
<div class="page-container"> 
 <!-- BEGIN SIDEBAR -->
 <div class="page-sidebar-wrapper"> 
  <!-- BEGIN SIDEBAR --> 
  <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
  <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
  <div class="page-sidebar navbar-collapse collapse"> 
   <!-- BEGIN SIDEBAR MENU --> 
  <jsp:include page="sidebar.jsp" />
<!--   <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
     DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
    <li class="sidebar-toggler-wrapper hide"> 
      BEGIN SIDEBAR TOGGLER BUTTON 
     <div class="sidebar-toggler"> <span></span> </div>
      END SIDEBAR TOGGLER BUTTON  
    </li>
    <li class="nav-item start "> <a href="index.html" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
     
    </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">License</span>  </a>
     
    </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
     
    </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
   </ul>-->
   <!-- END SIDEBAR MENU --> 
   <!-- END SIDEBAR MENU --> 
  </div>
  <!-- END SIDEBAR --> 
 </div>
 <!-- END SIDEBAR --> 
 <!-- BEGIN CONTENT -->
 <div class="page-content-wrapper"> 
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content"> 
   <!-- BEGIN PAGE HEADER--> 
   
   <!-- BEGIN PAGE TITLE-->
   <h3 class="page-title"> User Profile 
    <!-- <small>blank page layout</small>--> 
   </h3>
   <!-- END PAGE TITLE--> 
   <!-- END PAGE HEADER-->
   
 
  <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PROFILE SIDEBAR -->
                            <div class="profile-sidebar">
                                <!-- PORTLET MAIN -->
                                <div class="portlet light profile-sidebar-portlet bordered box-shadow">
                                    <!-- SIDEBAR USERPIC -->
                                    <div class="profile-userpic" style="margin-left: 100px;">
                                        <img alt="" class="img-circle" src="${pageContext.request.contextPath}/assets/layouts/layout/img/3.png" /> </div>
                                    <!-- END SIDEBAR USERPIC -->
                                    <!-- SIDEBAR USER TITLE -->
                                    <div class="profile-usertitle">
                                        <div class="profile-usertitle-name"> ${user.userName}  </div>
                                        <div class="profile-usertitle-job" style="text-transform:none;"> <a href="#">${user.emailId}</a> </div>
                                    </div>
                                    <!-- END SIDEBAR USER TITLE -->
                                    <!-- SIDEBAR BUTTONS -->
                                    <!--<div class="profile-userbuttons">
                                        <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                                        <button type="button" class="btn btn-circle red btn-sm">Message</button>
                                    </div>-->
                                    <!-- END SIDEBAR BUTTONS -->
                                    <!-- SIDEBAR MENU -->
<!--                                    <div class="profile-usermenu">
                                        <ul class="nav">
                                            <li >
                                                <a href="${pageContext.request.contextPath}/${user.userName}/profile">
                                                    <i class="icon-home"></i> Overview </a>
                                            </li>
                                            <li class="active">
                                                <a href="${pageContext.request.contextPath}/${user.userName}/profileaccountsetting">
                                                    <i class="icon-settings"></i> Account Settings </a>
                                            </li>
                                           
                                        </ul>
                                    </div>-->
                                    <!-- END MENU -->
                                </div>
                                <!-- END PORTLET MAIN -->
                                <!-- PORTLET MAIN -->
                                <div class="portlet light bordered box-shadow">
                                    <!-- STAT -->
                                    <div class="row list-separated ">
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <div class="uppercase profile-stat-title"> ${totalCourse}</div>
                                            <div class="uppercase profile-stat-text"> Course </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <div class="uppercase profile-stat-title">${totalLicense}</div>
                                            <div class="uppercase profile-stat-text"> License </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <div class="uppercase profile-stat-title">${totalSystem}</div>
                                            <div class="uppercase profile-stat-text"> System </div>
                                        </div>
                                    </div>
                                    <!-- END STAT -->
                                    <!--<div>
                                        <h4 class="profile-desc-title">About Marcus Doe</h4>
                                        <span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span>
                                        <div class="margin-top-20 profile-desc-link">
                                            <i class="fa fa-globe"></i>
                                            <a href="http://www.keenthemes.com">www.keenthemes.com</a>
                                        </div>
                                        <div class="margin-top-20 profile-desc-link">
                                            <i class="fa fa-twitter"></i>
                                            <a href="http://www.twitter.com/keenthemes/">@keenthemes</a>
                                        </div>
                                        <div class="margin-top-20 profile-desc-link">
                                            <i class="fa fa-facebook"></i>
                                            <a href="http://www.facebook.com/keenthemes/">keenthemes</a>
                                        </div>
                                    </div>-->
                                </div>
                                <!-- END PORTLET MAIN -->
                            </div>
                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content">
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered  box-shadow">
                                            <div class="portlet-title tabbable-line">
                                                <div class="caption caption-md">
                                                    <i class="icon-globe theme-font hide"></i>
                                                    <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                                </div>
                                                <ul class="nav nav-tabs">
<!--                                                    <li class="active">
                                                        <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                                    </li>-->
<!--                                                    <li>
                                                        <a href="#tab_1_2" data-toggle="tab">Courses</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_3" data-toggle="tab">System</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_4" data-toggle="tab">License</a>
                                                    </li>-->
                                                </ul>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="tab-content">
                                                    <!-- PERSONAL INFO TAB -->
                                                    <div class="tab-pane active" id="tab_1_1">
							<form action="javascript:;" class="form-horizontal">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">User Name</label>
                                                            <div class="col-sm-8">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon input-circle-left">
                                                                        <i class="fa fa-user"></i>
                                                                    </span>
                                                                    <input type="text" class="form-control input-circle-right" placeholder="Enter First Name" id="userName" value="${userInfo.userName}">
                                                                    <!--<span class="help-block"> A block of help text. </span>-->
                                                                </div>
                                                                <span style="color: red; display: block; font-size: 14px;" id="errorusername" ></span>
                                                            </div>
                                                        </div>
							<div class="form-group">
                                                            <label class="col-sm-3 control-label">Organization</label>
                                                            <div class="col-sm-8">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon input-circle-left">
                                                                        <i class="fa fa-building-o"></i>
                                                                    </span>
                                                                    <input type="text" class="form-control input-circle-right" placeholder="Enter Organization" id="organization" value="${userInfo.organization}">
                                                                    <!--<span class="help-block"> A block of help text. </span>-->
                                                                </div>
                                                                <span style="color: red; display: block; font-size: 14px;" id="errororganization" ></span>      
                                                            </div>
                                                        </div>
							<div class="form-group">
                                                            <label class="col-sm-3 control-label">Mobile No.</label>
                                                            <div class="col-sm-8">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon input-circle-left">
                                                                        <i class="fa fa-phone"></i>
                                                                    </span>
                                                                    <input type="text" class="form-control input-circle-right" placeholder="Enter Mobile No" id="mobileno" value="${userInfo.mobileNumber}">
                                                                    <!--<span class="help-block"> A block of help text. </span>-->
                                                                </div>
                                                                <span style="color: red; display: block; font-size: 14px;" id="errormobileno" ></span>      
                                                            </div>
                                                        </div>
							<div class="form-group">
                                                            <label class="col-sm-3 control-label">Email Address</label>
                                                            <div class="col-sm-8">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon input-circle-left">
                                                                        <i class="fa fa-envelope"></i>
                                                                    </span>
                                                                    <input type="text" class="form-control input-circle-right" placeholder="Enter Your Email Address" id="emailId" value="${userInfo.emailId}">
                                                                    <!--<span class="help-block"> A block of help text. </span>-->
                                                                </div>
                                                                <span style="color: red; display: block; font-size: 14px;" id="erroremailid" ></span>      
                                                            </div>
                                                        </div>
<!--                                                       <div class="form-group">
                                                            <label class="col-sm-3 control-label">About</label>
                                                            <div class="col-sm-8">
                                                                <textarea  class="form-control input-circle" placeholder=""></textarea>
                                                                <span class="help-block"> A block of help text. </span>
                                                            </div>
                                                        </div>-->
<!--							<div class="form-group">
                                                            <label class="col-sm-3 control-label">Website Url</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control input-circle" placeholder="http://www.mywebsite.com">
                                                                <span class="help-block"> A block of help text. </span>
                                                            </div>
                                                        </div>-->
                                                    </div>
                                                    <span style="color: green; display: block; margin-left: 170px; font-size: 14px;" id="updateresponse" ></span>      
                                                    <div id="loading" style="display: none;">
                                                        <center>
                                                            <!--<span style="color: blue;">Processing...</span>-->
                                                            <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/24.gif" />
                                                        </center>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-8 text-right">
                                                                <button type="submit" class="btn btn-circle btn-success" id="updateprofile">Update</button>
<!--                                                                <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                        
                                                    </div>
                                                    <!-- END PERSONAL INFO TAB -->
                                                    <!-- CHANGE AVATAR TAB -->
<!--                                                    <div class="tab-pane" id="tab_1_2">
                                                        
                                                        <form action="#" role="form">
                                                        <table class="table table-light detail-table">
                                                              	<tr>
                                                                	<td>Course Name:</td>
                                                                    <td>XYZ</td>
                                                                </tr>
                                                                <tr>
                                                                	<td>Course ID :</td>
                                                                    <td>54564dsf</td>
                                                                </tr>
                                                               <tr>
                                                                	<td>Description :</td>
                                                                    <td>
                                                                     Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                                                                     </td>
                                                                </tr>
                                                                 
                                                            </table>
                                                            
                                                        </form>
                                                    </div>
                                                     END CHANGE AVATAR TAB 
                                                     CHANGE PASSWORD TAB 
                                                    <div class="tab-pane" id="tab_1_3">
                                                        <form action="#">
                                                           <table class="table table-light detail-table">
                                                              	<tr>
                                                                	<td>Desk Serial Number:</td>
                                                                    <td>123456789</td>
                                                                </tr>
                                                                <tr>
                                                                	<td>Mac ID :</td>
                                                                    <td>BC-77-14-26-57-10</td>
                                                                </tr>
                                                               
                                                                 
                                                            </table>
                                                        </form>
                                                    </div>
                                                     END CHANGE PASSWORD TAB 
                                                     PRIVACY SETTINGS TAB 
                                                    <div class="tab-pane" id="tab_1_4">
                                                        <form action="#">
                                                            <table class="table table-light detail-table">
                                                              	<tr>
                                                                	<td>Course Name :</td>
                                                                    <td>XYZ</td>
                                                                </tr>
                                                                <tr>
                                                                	<td>Organization :</td>
                                                                    <td>ABC</td>
                                                                </tr>
                                                                <tr>
                                                                	<td>Validity :</td>
                                                                    <td>10-08-2016 to 10-08-2020</td>
                                                                </tr>
                                                                 <tr>
                                                                	<td>License Limit :</td>
                                                                    <td>Unlimited</td>
                                                                </tr>
                                                                 <tr>
                                                                	<td>System ID :</td>
                                                                    <td class="clearfix">
                                                                    	<span class="text-ellips">5671d54f5df3ds4f54dfd2f3d4f35ds1f2dsf53d53fsd21f21ds53</span>	<a href="#" class="pull-right btn btn-sm btn-success btn-circle"><i class="fa fa-copy"></i></a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            end profile-settings
                                                           <div class="margin-top-10">
                                                                <a href="javascript:;" class="btn btn-circle red"> Renew</a>
                                                                 <a href="javascript:;" class="btn default"> Cancel </a>
                                                            </div>
                                                        </form>
                                                    </div>-->
                                                    <!-- END PRIVACY SETTINGS TAB -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- END PROFILE CONTENT -->
                        </div>
                    </div>
   
   
  </div>
  <!-- END CONTENT BODY --> 
 </div>
 <!-- END CONTENT --> 
 <!-- BEGIN QUICK SIDEBAR --> 
 <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 
 
 <!-- END QUICK SIDEBAR --> 
</div>
<!-- END CONTAINER --> 
<!-- BEGIN FOOTER -->
<div class="page-footer">
 <div class="page-footer-inner"> 2016 &copy; Organization </div>
 <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
</div>
<!-- END FOOTER --> 

<!-- BEGIN CORE PLUGINS --> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
<!-- END CORE PLUGINS --> 
<!-- BEGIN THEME GLOBAL SCRIPTS --> 
 <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
<!-- END THEME GLOBAL SCRIPTS --> 
<!-- BEGIN THEME LAYOUT SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
<!-- END THEME LAYOUT SCRIPTS --> 
<!-- BEGIN PAGE LEVEL PLUGINS --> 


<!-- END PAGE LEVEL PLUGINS --> 
        <script>
            $(document).ready(function () {
                $('#side-menu').find('li').removeClass('active');
            });
        </script>
        <script>
        $( ".form-control" ).focus(function() {
            $(this).prev('.input-group-addon').removeClass().addClass('input-group-addon-focus');
            $(this).next('.input-group-addon').removeClass().addClass('input-group-addon-focus');
        });

        $( ".form-control" ).focusout(function() {
            $(this).prev('.input-group-addon-focus').removeClass().addClass('input-group-addon');
            $(this).next('.input-group-addon-focus').removeClass().addClass('input-group-addon');
        });
        </script>
        <script>
            $("#updateprofile").on("click", function(){
                var username = $("#userName").val();
                var company = $("#organization").val();
                var mobile = $("#mobileno").val();
                var mail = $("#emailId").val();
                var alphaExp=/^[a-zA-Z ]+$/;
                var emailExp= /^[0-9a-zA-Z-.+]+@[a-zA-Z0-9.-]+.[a-zA-z0-9]{2,4}$/;
                var numericExp=/^[0-9]+$/;
                var flag = true;
                if(username === null || username === ""){
                    $("#errorusername").text("Please Enter user name");
                    flag = false;
                } else if(username.match(alphaExp)){
                    
                } else{
                    $("#errorusername").text("Please enter a valid user name");
                    flag = false;
                }
                if(company === null || company === ""){
                    $("#errororganization").text("Please Enter Organization");
                    flag = false;
                }
                if(mobile === null || mobile === ""){
                    $("#errormobileno").text("Please Enter mobile number");
                    flag = false;
                } else if(mobile.match(numericExp)){
                    
                } else{
                    $("#errormobileno").text("Please enter a valid mobile number");
                    flag = false;
                }
                if(mail === null || mail === ""){
                    $("#erroremailid").text("Please Enter email address");
                    flag = false;
                } else if(mail.match(emailExp)){
                    
                } else{
                    $("#erroremailid").text("Please enter a valid email address!");
                    flag = false;
                }
                
                if(flag){
                    $("#loading").css('display','block');
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/updateprofile", 
                        {newUserName: username, neworganization: company, newmobile: mobile, newemail: mail},
                            function (data, status) {
                                if (data === "Please login again.") {
                                    $("#loading").css("display","none");
                                    window.location.href = "/";
                                } else if (data.indexOf("success") !== -1) {
                                    $("#loading").css("display","none");
                                    $("#updateresponse").text("Information has been updated successfully.");
                                    setTimeout(function()
                                        {
                                            window.location.reload();
                                        }, 1000);
                                } else {
                                    $("#loading").css("display","none");
                                    $("#updateresponse").text(data);
                                }
                            });
                }
            });
        </script>
</body>
</html>