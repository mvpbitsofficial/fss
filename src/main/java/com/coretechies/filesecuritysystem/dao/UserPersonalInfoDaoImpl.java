/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.UserPersonalInfo;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("UserPersonalInfoDao")
public class UserPersonalInfoDaoImpl implements UserPersonalInfoDao{
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    @Transactional
    public Boolean saveInfo(UserPersonalInfo userPersonalInfo){
        try{
            entityManager.persist(userPersonalInfo);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
    
    @Transactional
    public Boolean editInfo(UserPersonalInfo userPersonalInfo){
        try{
            entityManager.merge(userPersonalInfo);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
