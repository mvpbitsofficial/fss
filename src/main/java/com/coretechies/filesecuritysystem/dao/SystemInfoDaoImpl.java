/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.SystemInfo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("SystemInfoDao")
public class SystemInfoDaoImpl implements SystemInfoDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = false)
    public SystemInfo saveSystemInfo(SystemInfo systemInfo) {
        try {
            entityManager.persist(systemInfo);
            return systemInfo;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<SystemInfo> getSystemInfoByCreator(Integer createdBy) {
        try {
            System.out.println("createdBy : " + createdBy);
            TypedQuery<SystemInfo> createQuery = entityManager.createQuery("SELECT S FROM SystemInfo AS S LEFT JOIN S.userInfo AS U WHERE U.createdBy=:createdBy", SystemInfo.class);
            createQuery.setParameter("createdBy", createdBy);
            return createQuery.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = false)
    public boolean deleteSystemId(Integer systemInfoId) {
        try {
            entityManager.remove(entityManager.find(SystemInfo.class, systemInfoId));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public SystemInfo getSystemInfo(Integer userId, Integer systemInfoId) {
        try {
            TypedQuery<SystemInfo> createQuery = entityManager.createQuery("SELECT S FROM SystemInfo AS S WHERE S.userId=:userId AND S.systemInfoId=:systemInfoId", SystemInfo.class);
            createQuery.setParameter("userId", userId);
            createQuery.setParameter("systemInfoId", systemInfoId);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public SystemInfo getSystemInfoBySystemName(Integer userId, String systemName) {
        try {
            TypedQuery<SystemInfo> createQuery = entityManager.createQuery("SELECT S FROM SystemInfo AS S WHERE S.userId=:userId AND S.systemName=:systemName", SystemInfo.class);
            createQuery.setParameter("userId", userId);
            createQuery.setParameter("systemName", systemName);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public SystemInfo getSystemInfoBySystemId(Integer userId, String systemId) {
        try {
            TypedQuery<SystemInfo> createQuery = entityManager.createQuery("SELECT S FROM SystemInfo AS S WHERE S.userId=:userId AND S.systemId=:systemId", SystemInfo.class);
            createQuery.setParameter("userId", userId);
            createQuery.setParameter("systemId", systemId);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<SystemInfo> getSystemInfoByUser(Integer userId) {
        try {
            TypedQuery<SystemInfo> createQuery = entityManager.createQuery("SELECT S FROM SystemInfo AS S WHERE S.userId=:userId", SystemInfo.class);
            createQuery.setParameter("userId", userId);
            return createQuery.getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    @Override
    public List<SystemInfo> getAllSystemList(){
      try{
          TypedQuery<SystemInfo> query = entityManager.createQuery("SELECT S FROM SystemInfo AS S", SystemInfo.class);
          return query.getResultList();
      }catch(Exception e){
          return null;
      }  
    }

    @Override
    public SystemInfo getSystemById(Integer systeminfoid){
        try{
             TypedQuery<SystemInfo> query = entityManager.createQuery("SELECT S FROM SystemInfo AS S WHERE S.systemInfoId=:systemid", SystemInfo.class);
             query.setParameter("systemid", systeminfoid);
             return query.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    @Transactional
    public Boolean editSystemName(SystemInfo systemInfo){
        try{
            SystemInfo findsystem = entityManager.find(SystemInfo.class,systemInfo.getSystemInfoId());
            if(findsystem!=null){
                entityManager.merge(systemInfo);
            }
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
