/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.Course;
import com.coretechies.filesecuritysystem.domain.UserLicense;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("CourseDao")
public class CourseDaoImpl implements CourseDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = false)
    public Course saveCourse(Course course) {
        try {
            entityManager.persist(course);
            return course;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = false)
    public Course updateCourse(Course course) {
        try {
            entityManager.merge(course);
            return course;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Course> getCourses(Integer userId) {
        try {
            TypedQuery<Course> createQuery = entityManager.createQuery("SELECT C FROM Course AS C WHERE C.userId=:userId", Course.class);
            createQuery.setParameter("userId", userId);
            List<Course> resultList = createQuery.getResultList();
            for (Course course : resultList) {
                Hibernate.initialize(course.getUserLicenses());
            }
            return resultList;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Course getCourse(Integer userId, Integer id) {
        try {
            TypedQuery<Course> createQuery = entityManager.createQuery("SELECT C FROM Course AS C WHERE C.userId=:userId AND C.id=:id", Course.class);
            createQuery.setParameter("userId", userId);
            createQuery.setParameter("id", id);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Course getCourse(Integer userId, String courseName) {
        try {
            TypedQuery<Course> createQuery = entityManager.createQuery("SELECT C FROM Course AS C WHERE C.userId=:userId AND C.courseName=:courseName", Course.class);
            createQuery.setParameter("userId", userId);
            createQuery.setParameter("courseName", courseName);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = false)
    public boolean deleteCourse(Integer id) {
        try {
            entityManager.remove(entityManager.find(Course.class, id));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Course> getCourseListwithCompleteDetail(Integer userId) {
        try {
            TypedQuery<Course> createQuery = entityManager.createQuery("SELECT C FROM Course AS C WHERE C.userId=:userId", Course.class);
            createQuery.setParameter("userId", userId);
            List<Course> courses = createQuery.getResultList();
            for (Course course : courses) {
                Hibernate.initialize(course.getUserLicenses());
                for (UserLicense userLicense : course.getUserLicenses()) {
                    Hibernate.initialize(userLicense.getUserInfo());
                    Hibernate.initialize(userLicense.getSystemInfo());
                }
            }
            return courses;
        } catch (Exception e) {
            return null;
        }
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<Course> getCourseList(){
        try{
            TypedQuery<Course> query = entityManager.createQuery("SELECT C FROM Course AS C", Course.class);
            return query.getResultList();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public Course getCourseById(Integer id){
        try{
            TypedQuery<Course> query = entityManager.createQuery("SELECT C FROM Course AS C WHERE C.id=:courseid", Course.class);
            query.setParameter("courseid", id);
            return query.getSingleResult();
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
