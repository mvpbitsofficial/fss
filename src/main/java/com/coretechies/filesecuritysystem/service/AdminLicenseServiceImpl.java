/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.service;

import com.coretechies.filesecuritysystem.algorithm.NotificationType;
import com.coretechies.filesecuritysystem.dao.AdminLicenseDao;
import com.coretechies.filesecuritysystem.dao.NotificationDao;
import com.coretechies.filesecuritysystem.dao.UserInfoDao;
import com.coretechies.filesecuritysystem.domain.AdminLicense;
import com.coretechies.filesecuritysystem.domain.Notification;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.security.InvalidInformationException;
import com.coretechies.filesecuritysystem.security.LicenseInformation;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminLicenseServiceImpl implements AdminLicenseService {

    @Autowired
    private UserInfoDao userInfoDao;

    @Autowired
    private NotificationDao notificationDao;

    @Autowired
    private UserLicenseService userLicenseService;

    @Autowired
    private AdminLicenseDao adminLicenseDao;

    private final String SECURITY_KEY = "123";

    @Override
    public AdminLicense getAdminLicense(Integer creatorid, Integer clientuserid, Integer validity, 
            Date startDate, Date endDate, String systemId, Boolean webunlimited, Boolean desktopunlimited,
            Integer weblicenseLimit, Integer dekstoplicenselimit, Boolean unlimitedcourse, Integer courselimit,
            Integer courseSetting, Integer smsTransaction, Integer smsPromotion, Integer carryForward) throws InvalidInformationException {
        UserInfo userInfo = userInfoDao.getUserInfo(clientuserid);
        LicenseInformation licenseInformation = new LicenseInformation(clientuserid, userInfo.getUserName(), userInfo.getOrganization(), validity, startDate, endDate, systemId, webunlimited, desktopunlimited, weblicenseLimit,dekstoplicenselimit, unlimitedcourse, courselimit, courseSetting, smsTransaction, smsPromotion, carryForward, SECURITY_KEY);
        System.out.println("License="+licenseInformation.getAdminLicense());
        return licenseInformation.getAdminLicense();
    }

    @Override
    public AdminLicense getAdminLicense(Integer creatorid, Integer clientuserid, Integer validity, Date startDate,
            Date endDate, String systemId, Boolean webunlimited, Boolean desktopunlimited, Integer weblicenseLimit,
            Integer dekstoplicenselimit, Boolean unlimitedcourse, Integer courselimit, Integer courseSetting,
            Integer smsTransaction, Integer smsPromotion) throws InvalidInformationException {
        return getAdminLicense(creatorid, clientuserid, validity, startDate, endDate, systemId, webunlimited, desktopunlimited, weblicenseLimit, dekstoplicenselimit, unlimitedcourse, courselimit, courseSetting, smsTransaction, smsPromotion, 0);
    }

    @Override
    public String saveAdminLicense(AdminLicense adminLicense, Integer creatorid) {
        if (adminLicense.getAdminLicenseId() == null) {
            if (adminLicenseDao.saveAdminLicense(adminLicense) != null) {
                UserInfo userInfo = userInfoDao.getUserInfo(adminLicense.getUserId());
                Notification notification = new Notification();
                notification.setCreatedBy(creatorid);
                notification.setNotificationMessage("Licence generated for Client " + userInfo.getUserName());
                notification.setSeenby("0,");
                notification.setNotificationType(NotificationType.License_History.getNotificationTypeValue());
                notification.setCreateOn(new Date());
                notification.setUserId(userInfo.getUserInfoId());
                notificationDao.saveNotification(notification);
                return "success#" + adminLicense.getDesktopLicense();
            } else {
                return "Unexpected Error Occur.";
            }
        } else if (adminLicenseDao.updateAdminLicense(adminLicense) != null) {
            UserInfo userInfo = userInfoDao.getUserInfo(adminLicense.getUserId());
            Notification notification = new Notification();
            notification.setCreatedBy(creatorid);
            notification.setNotificationMessage("Licence renew for Client " + userInfo.getUserName());
            notification.setSeenby("0,");
            notification.setNotificationType(NotificationType.License_History.getNotificationTypeValue());
            notification.setCreateOn(new Date());
            notification.setUserId(userInfo.getUserInfoId());
            notificationDao.saveNotification(notification);
            return "success#" + adminLicense.getDesktopLicense();
        } else {
            return "Unexpected Error Occur.";
        }
    }

    @Override
    public String editAdminLicense(AdminLicense adminLicense, Integer creatorid){
        if (adminLicenseDao.updateAdminLicense(adminLicense) != null) {
            UserInfo userInfo = userInfoDao.getUserInfo(adminLicense.getUserId());
            Notification notification = new Notification();
            notification.setCreatedBy(creatorid);
            notification.setNotificationMessage("Licence Edit for Client " + userInfo.getUserName());
            notification.setSeenby("0,");
            notification.setNotificationType(NotificationType.License_History.getNotificationTypeValue());
            notification.setCreateOn(new Date());
            notification.setUserId(userInfo.getUserInfoId());
            notificationDao.saveNotification(notification);
            return "success#" + adminLicense.getDesktopLicense();
        } else {
            return "Unexpected Error Occur.";
        }
    }
    
    @Override
    public AdminLicense getRenewAdminLicense(Integer creatorid, Integer licenseId, Boolean lifetime, Integer validity, Date startDate, Date endDate, Boolean webunlimited, Boolean desktopunlimited, Integer weblicenseLimit, Integer desktoplicenseLimit, Integer carryForward) throws InvalidInformationException {
        AdminLicense adminLicense = adminLicenseDao.getAdminLicense(licenseId);
        UserInfo userInfo = adminLicense.getUserInfo();
        System.out.println(userInfo);
        if (carryForward == 1) {
            if (!webunlimited && weblicenseLimit != -1) {
                Integer userLicenseCount = userLicenseService.getUserLicenseCountByCreator(adminLicense.getUserId());
                Integer remaining = adminLicense.getWeblicenseLimit() - userLicenseCount;
                weblicenseLimit += remaining;
                desktoplicenseLimit += adminLicense.getDesktopLicenseLimit();
                System.out.println(weblicenseLimit+" AND "+desktoplicenseLimit);
            }
        }
        LicenseInformation licenseInformation = new LicenseInformation(userInfo, lifetime, validity, startDate, endDate, webunlimited, desktopunlimited, weblicenseLimit, desktoplicenseLimit, carryForward, SECURITY_KEY, adminLicense);
        return licenseInformation.getAdminLicense();
    }
    
    @Override
    public AdminLicense getEditLicense(Integer licenseId, String systemId, Boolean unlimitedcourse, Integer courselimit, Integer courseSetting,
            Integer smsTransaction, Integer smsPromotion ) throws InvalidInformationException{
        AdminLicense adminLicense = adminLicenseDao.getAdminLicense(licenseId);
        UserInfo userInfo = adminLicense.getUserInfo();
        System.out.println(userInfo);
        LicenseInformation licenseInformation = null;
        try {
            licenseInformation = new LicenseInformation(adminLicense, systemId, unlimitedcourse, courselimit, courseSetting, smsTransaction, smsPromotion, userInfo, SECURITY_KEY);
        } catch (ParseException ex) {
            Logger.getLogger(AdminLicenseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return licenseInformation.getAdminLicense();
    }
    @Override
    public List<AdminLicense> getLicenseExpireAlert() {
        DateTime dateTime = new DateTime(new Date());
        return adminLicenseDao.getLicenseExpireAlert(dateTime.plusDays(5).toDate());
    }

    @Override
    public List<AdminLicense> getAdminLicenseListByExpiryDate(Date fromDate, Date toDate) {
        return adminLicenseDao.getAdminLicenseListByExpiryDate(fromDate, toDate);
    }

    @Override
    public List<AdminLicense> getAdminLicenseListByCreateOn(Date fromDate, Date toDate) {
        return adminLicenseDao.getAdminLicenseListByCreateOn(fromDate, toDate);
    }
    
    @Override
    public Integer getAdminLicenseCount(){
        List<AdminLicense> licenses = adminLicenseDao.getAdminLicense();
        if(licenses != null){
            return licenses.size();
        }else{
            return 0;
        }
    }
    
    @Override
    public AdminLicense getAdminLicenseByClient(Integer clientuserid){
        return adminLicenseDao.getAdminLicenseByClient(clientuserid);
    }
    
    @Override
    public Integer getClientLicenseForChart(Date date){
        return adminLicenseDao.getClientLicenseForChart(date);
    }
    
    @Override
    public AdminLicense getAdminLicenseById(Integer adminLicenseId){
        return adminLicenseDao.getAdminLicense(adminLicenseId);
    }
}
