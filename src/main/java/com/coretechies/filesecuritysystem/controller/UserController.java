/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.controller;

import com.coretechies.filesecuritysystem.algorithm.UserStatus;
import com.coretechies.filesecuritysystem.algorithm.UserType;
import com.coretechies.filesecuritysystem.dao.AdminLicenseDao;
import com.coretechies.filesecuritysystem.dao.CountryDao;
import com.coretechies.filesecuritysystem.dao.NotificationDao;
import com.coretechies.filesecuritysystem.dao.SystemInfoDao;
import com.coretechies.filesecuritysystem.dao.UserInfoDao;
import com.coretechies.filesecuritysystem.domain.AdminLicense;
import com.coretechies.filesecuritysystem.domain.CountryCode;
import com.coretechies.filesecuritysystem.domain.SystemInfo;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import com.coretechies.filesecuritysystem.domain.UserLicense;
import com.coretechies.filesecuritysystem.security.InvalidInformationException;
import com.coretechies.filesecuritysystem.security.RandomPasswordGenerator;
import com.coretechies.filesecuritysystem.security.SystemInformation;
import com.coretechies.filesecuritysystem.service.AdminLicenseService;
import com.coretechies.filesecuritysystem.service.CourseService;
import com.coretechies.filesecuritysystem.service.SystemInfoService;
import com.coretechies.filesecuritysystem.service.UserInfoService;
import com.coretechies.filesecuritysystem.service.UserLicenseService;
import com.coretechies.filesecuritysystem.web.webdomain.CreateUserBean;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * The UserController class implements an application that handles all the requests related to user,
 * like create user, update user, delete user by client.
 * 
 *@Controller annotation represents that this is a java controller class.
 * 
 * @SessionAttributes annotation represents a user in session.
 * "user" is a object of userInfo class and have a complete information
 * of that user who is present in the session at that time.
 * 
 * @author Jaishree
 * @version 1.0
 * @since 2016-09-02
 */
@Controller
@SessionAttributes({"user"})
public class UserController {

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private UserInfoDao userInfoDao;

    @Autowired
    private CourseService courseService;
    
    @Autowired
    private SystemInfoService systemInfoService;
     
    @Autowired
    private UserLicenseService userLicenseService;
    
    @Autowired
    private CountryDao countryDao;

    @Autowired
    private SystemInfoDao systemInfoDao;
    
    @Autowired
    private MailController mailController;
    
    @Autowired
    private AdminLicenseService adminLicenseService;
    
    @Autowired
    private AdminLicenseDao adminLicenseDao;

    @Autowired
    private NotificationDao notificationDao;
    
    /**
     * This method will set the list of users at attribute and return on viewuser page.
     * 
     * @param userName
     * @param modelMap
     * @return on page viewuser
     */
    @GetMapping(value = "/{userName}/viewuser")
    public ModelAndView viewUser(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
//        System.out.println("session vala name"+ userInfo.getUserName().replace(" ", ""));
//        System.out.println("url vala name"+userName);
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                modelMap.addAttribute("users", userInfoDao.getUserInfoByCreatedBy(userInfo.getUserInfoId()));
                return new ModelAndView("viewuser");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * 
     * @param userName
     * @param modelMap
     * @param userId
     * @return 
     */
    @GetMapping(value = "/{userName}/view/{userId}")
    public ModelAndView view(@PathVariable String userName, ModelMap modelMap, @PathVariable Integer userId) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                //modelMap.addAttribute("courses", courseService.getCourseById(userId));
                UserInfo userInfo1 = userInfoDao.getUserInfo(userId);
                List<UserLicense> userLicenseByUser = userLicenseService.getUserLicenseByUser(userId);
                modelMap.addAttribute("totalLicense", userLicenseByUser.size());
                List<SystemInfo> systemInfoListByUser = systemInfoService.getSystemInfoListByUser(userId);
                modelMap.addAttribute("totalSystem", systemInfoListByUser.size());
                modelMap.addAttribute("userdetail", userInfo1);
                List<SystemInfo> systemInfoList = systemInfoService.getSystemInfoListByUser(userId);
                modelMap.addAttribute("systeminfo", systemInfoList);
                List<UserLicense> userLicense = userLicenseService.getUserLicenseByUser(userId);
                modelMap.addAttribute("license", userLicense);
                Map<Integer, Integer> showUserIdThroughCount = userInfoDao.showUserIdThroughCount();
                Integer value=0;
                for (Map.Entry<Integer, Integer> entry : showUserIdThroughCount.entrySet()) {
                    if(entry.getKey() == userId){
                        value = entry.getValue();
                    }
                }
                value =  value +1000;
                modelMap.addAttribute("USERID", value);
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                return new ModelAndView("viewuserdetails");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }
    
    /**
     * 
     * @param userName
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/userlistview")
    public ModelAndView viewUserList(@PathVariable String userName, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                List<UserInfo> userInfoByCreatedBy = userInfoDao.getUserInfoByCreatedBy(userInfo.getUserInfoId());
                for (UserInfo userInfo1 : userInfoByCreatedBy) {
                    Map<Integer, Integer> newuserId = userInfoDao.showUserIdThroughCount();
                    for (Map.Entry<Integer, Integer> entry : newuserId.entrySet()) {
                        if(entry.getKey()== userInfo1.getUserInfoId()){
                            userInfo1.setDisplayName("AD"+(1000+entry.getValue()));    
                        }
                    }
                }
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                modelMap.addAttribute("users", userInfoByCreatedBy);
                return new ModelAndView("viewuserlist");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }
    /**
     * This method will take the user at createuser page, to create a new user.
     * 
     * @param userName
     * @param createuserbean
     * @param modelMap
     * @return on page createuser
     */
    @GetMapping(value = "/{userName}/createuser")
    public ModelAndView getCreateClient(@PathVariable String userName, @ModelAttribute("createuserbean") CreateUserBean createuserbean, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                modelMap.addAttribute("users", userInfoDao.getUserInfoByUserType(UserType.User.getUserTypeValue()));
                modelMap.addAttribute("countries", countryDao.getCountryCodes());
                Map<Integer, Integer> showUserIdThroughCount = userInfoDao.showUserIdThroughCount();
                Integer value = 0;
                for (Map.Entry<Integer, Integer> entry : showUserIdThroughCount.entrySet()) {
                    value = entry.getValue();
                }
                modelMap.addAttribute("USERID", 1000+value+1);
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                return new ModelAndView("createuser");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method have all the information to create a new user in method parameters,
     * the user is added in database, and a confirmation mail is send to user. 
     * 
     * @param userName
     * @param image
     * @param createuserbean
     * @param result
     * @param request
     * @param redirectAttributes
     * @param modelMap
     * @return
     * @throws NoSuchAlgorithmException
     */
    @RequestMapping(value = "/{userName}/createuser", method = RequestMethod.POST)
    public ModelAndView createUser(@PathVariable String userName,
            @RequestParam(name = "image", required = false) MultipartFile image,
            @Valid @ModelAttribute("createuserbean") CreateUserBean createuserbean,
            BindingResult result,
            HttpServletRequest request,
            RedirectAttributes redirectAttributes,
            ModelMap modelMap) throws NoSuchAlgorithmException {

        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {

            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                if (userInfoDao.getUserInfo(createuserbean.getNewUserName()) != null) {
                    result.addError(new FieldError("createclient", "newUserName", "This user name already in use please choose another username."));
                }
//                if (userInfoDao.getUserInfo(createuserbean.getEmailId()) != null) {
//                    result.addError(new FieldError("createclient", "emailId", "This emailId already in use please choose another emailId."));
//                }

                if (result.hasErrors()) {
                    modelMap.addAttribute("users", userInfoDao.getUserInfoByUserType(UserType.User.getUserTypeValue()));
                    modelMap.addAttribute("countries", countryDao.getCountryCodes());
                    Map<Integer, Integer> showUserIdThroughCount = userInfoDao.showUserIdThroughCount();
                    Integer value = 0;
                    for (Map.Entry<Integer, Integer> entry : showUserIdThroughCount.entrySet()) {
                        value = entry.getValue();
                    }
                    modelMap.addAttribute("USERID", 1000+value+1);
                    return new ModelAndView("createuser");
                }

                String profilePic = null;
                if (image != null && !image.isEmpty()) {
                    try {
                        ServletContext servletContext = request.getSession().getServletContext();
                        String profilePicturePath = servletContext.getRealPath("/");
                        System.out.println("first path:::" + profilePicturePath);
                        profilePicturePath = profilePicturePath.replace("\\", "/");
                        File profilePictureFile = new File(profilePicturePath, "profilepic");
                        if (!profilePictureFile.exists()) {
                            profilePictureFile.mkdir();
                        }
                        System.out.println("second path:::" + profilePicturePath);
                        profilePic = image.getName() + "_" + new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(new Date()) + "_" + userName + "." + image.getOriginalFilename().substring(image.getOriginalFilename().lastIndexOf(".") + 1);
                        System.out.println("asd: " + profilePic);
                        Path imagePath = Paths.get(profilePictureFile.getAbsolutePath(), profilePic);
                        Files.copy(image.getInputStream(), imagePath);
                        profilePic = imagePath.toString();
                        profilePic = profilePic.substring(profilePic.indexOf("\\profilepic"));

                    } catch (Exception ex) {
                        Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                RandomPasswordGenerator generator = new RandomPasswordGenerator();
                String password = String.copyValueOf(generator.generatePswd(5, 10, 2, 2, 1));
                createuserbean.setMobileNumberCode(createuserbean.getCountry()); 
                if (userInfoService.createUser(createuserbean.getNewUserName(), password, createuserbean.getEmailId(), profilePic, createuserbean.getCountry(), createuserbean.getMobileNumberCode(), createuserbean.getMobileNumber(), UserType.User.getUserTypeValue(), UserStatus.Activated.getUserStatusValue(), userInfo.getUserInfoId()) != null) {
                    redirectAttributes.addFlashAttribute("message", "User account has been created successfully.");
                    return new ModelAndView("redirect:/" + userName + "/userlistview");
                } else {
                    redirectAttributes.addFlashAttribute("message", "Unable to create user.");
                    return new ModelAndView("redirect:/" + userName + "/createuser");
                }
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will return on updateuser page with complete information of that particular user.
     * 
     * @param userName
     * @param userId
     * @param updateuserbean
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/updateuser/{userId}")
    public ModelAndView getUpdateClient(@PathVariable String userName, @PathVariable Integer userId,
            @ModelAttribute("updateuserbean") CreateUserBean updateuserbean, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserInfo userInfo1 = userInfoDao.getUserInfo(userId);
                updateuserbean.setUserId(userInfo1.getUserInfoId());
                updateuserbean.setCountry(userInfo1.getCountryCodeId());
                updateuserbean.setEmailId(userInfo1.getEmailId());
                updateuserbean.setMobileNumber(userInfo1.getMobileNumber());
                updateuserbean.setMobileNumberCode(userInfo1.getCountryCodeId());
                updateuserbean.setNewUserName(userInfo1.getUserName());
                modelMap.addAttribute("countries", countryDao.getCountryCodes());
                Map<Integer, Integer> showUserIdThroughCount = userInfoDao.showUserIdThroughCount();
                Integer value=0;
                for (Map.Entry<Integer, Integer> entry : showUserIdThroughCount.entrySet()) {
                    if(entry.getKey() == userId){
                        value = entry.getValue();
                    }
                }
                value =  value +1000;
                modelMap.addAttribute("USERID", value);
                modelMap.addAttribute("notifications", notificationDao.getNotificationForClient(userInfo.getUserInfoId()));
                return new ModelAndView("updateuser");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will update the user with new values present in the method parameter,
     * And send a confirmation mail to the user.
     * 
     * @param userName
     * @param userId
     * @param image
     * @param updateuserbean
     * @param result
     * @param request
     * @param redirectAttributes
     * @param modelMap
     * @return
     * @throws NoSuchAlgorithmException
     */
    @PostMapping(value = "/{userName}/updateuser/{userId}")
    public ModelAndView updateClientpost(@PathVariable String userName, @PathVariable String userId, @RequestParam(name = "image", required = false) MultipartFile image, @Valid @ModelAttribute("updateuserbean") CreateUserBean updateuserbean, BindingResult result, HttpServletRequest request, RedirectAttributes redirectAttributes, ModelMap modelMap) throws NoSuchAlgorithmException {

        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {

            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                if (userInfoDao.getUserInfo(updateuserbean.getNewUserName(), updateuserbean.getUserId()) != null) {
                    result.addError(new FieldError("updateuserbean", "newUserName", "This user name already in use please choose another username."));
                }
//                if (userInfoDao.getUserInfo(updateuserbean.getEmailId(), updateuserbean.getUserId()) != null) {
//                    result.addError(new FieldError("updateuserbean", "emailId", "This emailId already in use please choose another emailId."));
//                }

                if (result.hasErrors()) {
                    modelMap.addAttribute("users", userInfoDao.getUserInfoByUserType(UserType.User.getUserTypeValue()));
                    modelMap.addAttribute("countries", countryDao.getCountryCodes());
                    Map<Integer, Integer> showUserIdThroughCount = userInfoDao.showUserIdThroughCount();
                    Integer value=0;
                    for (Map.Entry<Integer, Integer> entry : showUserIdThroughCount.entrySet()) {
                        if(entry.getKey() == updateuserbean.getUserId()){
                            value = entry.getValue();
                        }
                    }
                    value =  value +1000;
                    modelMap.addAttribute("USERID", value);
                    return new ModelAndView("updateuser");
                }

                String profilePic = null;
                if (image != null && !image.isEmpty()) {
                    try {
                        ServletContext servletContext = request.getSession().getServletContext();
                        String profilePicturePath = servletContext.getRealPath("/");
                        System.out.println("first path:::" + profilePicturePath);
                        profilePicturePath = profilePicturePath.replace("\\", "/");
                        File profilePictureFile = new File(profilePicturePath, "profilepic");
                        if (!profilePictureFile.exists()) {
                            profilePictureFile.mkdir();
                        }
                        System.out.println("second path:::" + profilePicturePath);
                        profilePic = image.getName() + "_" + new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(new Date()) + "_" + userName + "." + image.getOriginalFilename().substring(image.getOriginalFilename().lastIndexOf(".") + 1);
                        System.out.println("asd: " + profilePic);
                        Path imagePath = Paths.get(profilePictureFile.getAbsolutePath(), profilePic);
                        Files.copy(image.getInputStream(), imagePath);
                        profilePic = imagePath.toString();
                        profilePic = profilePic.substring(profilePic.indexOf("\\profilepic"));

                    } catch (Exception ex) {
                        Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                updateuserbean.setMobileNumberCode(updateuserbean.getCountry()); 
                if (updateuserbean.getUserId() == 0) {
                    redirectAttributes.addFlashAttribute("message", "Invalid access.");
                    return new ModelAndView("redirect:/" + userName + "/viewclient");
                } else if (userInfoService.updateUser(updateuserbean.getUserId(), updateuserbean.getNewUserName(), updateuserbean.getEmailId(), profilePic, updateuserbean.getCountry(), updateuserbean.getMobileNumberCode(), updateuserbean.getMobileNumber(), UserType.User.getUserTypeValue(), UserStatus.Activated.getUserStatusValue(), userInfo.getUserInfoId()) != null) {
                    redirectAttributes.addFlashAttribute("message", "User account has been updated successfully.");
                    return new ModelAndView("redirect:/" + userName + "/userlistview");
                } else {
                    redirectAttributes.addFlashAttribute("message", "Unable to update user.");
                    return new ModelAndView("updateuser");
                }
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /** This method is used to activate a deactivated user.
     * 
     * @param userName
     * @param userId
     * @param redirectAttributes
     * @param request
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/activateuser/{userId}")
    public ModelAndView activateClient(@PathVariable String userName, @PathVariable Integer userId, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserInfo userInfo1;
                if ((userInfo1 = userInfoService.activateClient(userId)) != null) {
                    Boolean sendMail = mailController.sendMail("User Profile Activate", "Your profile is Activated.\n", userInfo1.getEmailId(), request, userInfo.getUserInfoId());
//                    mailController.sendEmail("User Profile Activate",
//                            "Your profile is Activated.\n",
//                            userInfo1.getEmailId(), request);
                    if(sendMail){
                        redirectAttributes.addFlashAttribute("message", "User account has been activated successfully.");
                    } else{
                        redirectAttributes.addFlashAttribute("message", "User Successfully Activated But Please Complete your Mail Settings.");
                    }
                } else {
                    redirectAttributes.addFlashAttribute("message", "Unable to create user.");
                }
                return new ModelAndView("redirect:/" + userName + "/userlistview");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method is used to deactivate a active user.
     * 
     * @param userName
     * @param userId
     * @param redirectAttributes
     * @param request
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/deactivateuser/{userId}")
    public ModelAndView deactivateClient(@PathVariable String userName, @PathVariable Integer userId, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserInfo userInfo1;
                if ((userInfo1 = userInfoService.deactivateClient(userId)) != null) {
                    Boolean sendMail = mailController.sendMail("User Profile Deactivate", "Your profile is Deactivated.\n", userInfo1.getEmailId(), request, userInfo.getUserInfoId());
//                    mailController.sendEmail("User Profile Deactivate",
//                            "Your profile is Deactivate.\n",
//                            userInfo1.getEmailId(), request);
                    if(sendMail){
                        redirectAttributes.addFlashAttribute("message", "User account has been deactivated successfully. ");
                    } else{
                        redirectAttributes.addFlashAttribute("message", "User Successfully Deactivated But Please Complete your Mail Settings.");
                    }
                } else {
                    redirectAttributes.addFlashAttribute("message", "Unable to create user.");
                }
                return new ModelAndView("redirect:/" + userName + "/userlistview");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }

    /**
     * This method will delete a user from the database and send a mail to the user.
     * 
     * @param userName
     * @param userId
     * @param redirectAttributes
     * @param request
     * @param modelMap
     * @return 
     */
    @GetMapping(value = "/{userName}/deleteuser/{userId}")
    public ModelAndView deleteUser(@PathVariable String userName, @PathVariable Integer userId, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserInfo userInfo1;
                List<UserLicense> userLicenseByUser = userLicenseService.getUserLicenseByUser(userId);
                if(userLicenseByUser == null || userLicenseByUser.isEmpty()){
                    if ((userInfo1 = userInfoService.deleteUser(userId)) != null) {
                        redirectAttributes.addFlashAttribute("message", "User account has been deleted successfully. ");
                    } else {
                        redirectAttributes.addFlashAttribute("message", "Unable to Delete user.");
                    }
                } else{
                    redirectAttributes.addFlashAttribute("message", "You can not Delete this user, Id is in use.");
                }
                return new ModelAndView("redirect:/" + userName + "/userlistview");
            } else {
                return new ModelAndView("redirect:/" + userName + "/dashboard");
            }
        } else {
            return new ModelAndView("redirect:/");
        }
    }
    @GetMapping(value = "/{userName}/deletealluser")
    @ResponseBody
    public String deleteAllUser(@PathVariable String userName, @RequestParam(value = "userIds[]") Integer[] userIds, RedirectAttributes redirectAttributes, HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserInfo userInfo1;
                String skippeduser = "";
                String successdelete = "";
                for (Integer userId : userIds) {
                    System.out.println(userId);
                    userInfo1 = userInfoDao.getUserInfo(userId);
                    List<UserLicense> userLicenseByUser = userLicenseService.getUserLicenseByUser(userId);
                    System.out.println("userlicense"+userLicenseByUser);
                    if(userLicenseByUser == null || userLicenseByUser.isEmpty()){
                        if ((userInfo1 = userInfoService.deleteUser(userId)) != null) {
                            successdelete = "Users has been deleted successfully.";
                        }
                    } else{
                        skippeduser += ","+userInfo1.getUserName(); 
                        System.out.println("usernames=="+skippeduser);
                    }
                }
                if(!skippeduser.equals("")){
                    System.out.println("usernames======="+skippeduser);
                    return "success#Unable to Delete user "+"\""+skippeduser.substring(1)+"\"";
                } else{
                    return "success#"+successdelete+".";
                }
            } else {
                return "Login again";
            }
        } else {
            return "Login again";
        }
    }
    
    /**
     *
     * @param userName
     * @param subject
     * @param message
     * @param userId
     * @param request
     * @param modelMap
     * @return
     */
    @PostMapping(value = "/{userName}/sendmailtouser")
    @ResponseBody
    public String sendMailToUser(@PathVariable String userName, @RequestParam String subject, @RequestParam String message, @RequestParam Integer userId, HttpServletRequest request, ModelMap modelMap){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
         if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
             if(UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())){
                UserInfo userInfo1 = userInfoDao.getUserInfo(userId);
                mailController.sendEmail(subject, message, userInfo1.getEmailId(), request, userInfo1);
                return "success";
             } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                UserInfo userInfo1= userInfoDao.getUserInfo(userId);
                Boolean sendMail = mailController.sendMail(subject, message, userInfo1.getEmailId(), request, userInfo.getUserInfoId());
//                mailController.sendEmail(subject, message, userInfo1.getEmailId(), request);
                if(sendMail){
                    System.out.println("Email id=== "+userInfo1.getEmailId());
                    return "success";
                } else{
                    return "Mail cannot be sent due to incomplete mail settings.";
                }
             }else{
                 return "fail";
             }
         }else {
            return "Login again";
        }
    }

    @PostMapping(value = "/{userName}/sendmailtomultipleuser")
    @ResponseBody
    public String sendMailToMultipleUser(@PathVariable String userName, ModelMap modelMap, @RequestParam(value = "userIds[]") Integer[] userIds, @RequestParam String subject, @RequestParam String message, HttpServletRequest request){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
         if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if(UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())){
                if(userIds == null){
                    return "No user Available To send Mail";
                }else{
                    for(Integer userId : userIds){
                        UserInfo userInfo1 = userInfoDao.getUserInfo(userId);
                        mailController.sendEmail(subject, message, userInfo1.getEmailId(), request, userInfo1);
                    }
                    return "success";
                }
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                if(userIds == null){
                    return "No user Available to send mail";
                }else{
                    Boolean sendMail = false;
                    for(Integer userId : userIds){
                        UserInfo userInfo1= userInfoDao.getUserInfo(userId);
                        sendMail = mailController.sendMail(subject, message, userInfo1.getEmailId(), request, userInfo.getUserInfoId());
        //                mailController.sendEmail(subject, message, userInfo1.getEmailId(), request);  
                    } 
                    if(sendMail){
                        return "success";
                    } else{
                        return "Mail cannot be sent due to incomplete mail settings.";
                    }
                }
            }else{
                 return "fail";
            }
        }else {
            return "Login again";
        }
    }
    
    @PostMapping(value = "/{userName}/sendmessagetousers")
    @ResponseBody
    public String sendMessageToUsers(@PathVariable String userName, ModelMap modelMap, @RequestParam(value = "userIds[]") Integer[] userIds, @RequestParam String text){
        UserInfo userInfo = (UserInfo) modelMap.get("user");
        if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
            if(UserType.Admin.getUserTypeValue().equals(userInfo.getUserType())){
                String mobilenumbers = "";
                for(Integer userId : userIds){
                    UserInfo userInfo1 = userInfoDao.getUserInfo(userId);
                    mobilenumbers += ","+userInfo1.getMobileNumber();
                }
                mailController.sendMessage(mobilenumbers.substring(1), text); 
                return "success";
            } else if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                AdminLicense adminLicenseByClient = adminLicenseService.getAdminLicenseByClient(userInfo.getUserInfoId());
                Integer promotionalCounter = adminLicenseByClient.getPromotionalCounter();
                Integer promotionalLimit = adminLicenseByClient.getSmsPromotional();
                Integer count = 0;
                String mobilenumbers = "";
                for(Integer userId : userIds){
                    UserInfo userInfo1 = userInfoDao.getUserInfo(userId);
                    mobilenumbers += ","+userInfo1.getMobileNumber();
                    count++;
                }
                if(promotionalLimit == promotionalCounter){
                    return "Your SMS Limit is over.";
                } else{
                    System.out.println("ffe=="+(promotionalLimit - promotionalCounter));
                    if(promotionalLimit - promotionalCounter > count){
                        mailController.sendMessage(mobilenumbers.substring(1), text); 
                        adminLicenseByClient.setPromotionalCounter(promotionalCounter+count);
                        adminLicenseDao.updateAdminLicense(adminLicenseByClient);
                        return "success";
                    } else{
                        return "you have "+(promotionalLimit - promotionalCounter)+" free SMSs, you can not send message more then that.";
                    } 
                }
            } else{
                return "fail";
            }
        }else {
            return "Login again";
        }
    }
    
    @PostMapping(value = "/{userName}/getsystemdetails")
    @ResponseBody
    public String getSystemDetails(@PathVariable String userName, ModelMap modelMap, @RequestParam Integer systeminfoid) throws InvalidInformationException{
        UserInfo userInfo = (UserInfo) modelMap.get("user");
         if (userInfo != null && userInfo.getUserName().replace(" ", "").equals(userName)) {
             if (UserType.Client.getUserTypeValue().equals(userInfo.getUserType())) {
                SystemInfo systemById = systemInfoDao.getSystemById(systeminfoid);
                String systemInfo = new SystemInformation(systemById.getSystemId()).getSystemInfo();
                return systemInfo;
             }else{
                 return "fail";
             }
         }else {
            return "Login again";
        }
    }
    
    @PostMapping(value = "/{userName}/checkforuseremail")
    @ResponseBody
    String checkForUserEmailID(@RequestParam String email,@RequestParam Integer type,ModelMap model) throws Exception{
        UserInfo userInfo = (UserInfo) model.get("user");
        if(userInfo!=null){
            UserInfo userInfoByUserName = userInfoDao.getUserInfoEmailAndType(email,type);
            if(userInfoByUserName!=null){
                return "This Email id is already registered with us";
            }else{
                return "success";
            }
        }
        return "Login First...!!";
    }
    
    @PostMapping(value = "/{userName}/checkforusermobileno")
    @ResponseBody
    String checkForUserMobileNumber(@RequestParam String mobile,@RequestParam Integer type,ModelMap model) throws Exception{
        UserInfo userInfo = (UserInfo) model.get("user");
        if(userInfo!=null){
            UserInfo userInfo1 = userInfoDao.getUserInfoMobileAndType(mobile, type);
            if(userInfo1!=null){
                return "This Mobile Number is already registered with us";
            }else{
                return "success";
            }
        }
        return "Login First...!!";
    }
}
