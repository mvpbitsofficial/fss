package com.coretechies.filesecuritysystem.service.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MailMail mail;

    @Override
    public void sendMail(String subject, String content, String to) {
        mail.sendMail(subject, content, to);
    }
}
