/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.domain;

import java.io.Serializable;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author CoreTechies M8 mobilenumber` ( `mobileNumberId` int(11) NOT NULL
 * AUTO_INCREMENT, `countryCodeId` int(11) NOT NULL, `mobileNumber` varchar(45)
 * NOT NULL, `userId
 */
@Entity
@Table(name = "mobilenumber")
@Access(AccessType.FIELD)
public class MobileNumber implements Serializable {

    @Id
    @GeneratedValue(generator = "mobilenumber")
    @GenericGenerator(name = "mobilenumber", strategy = "increment")
    private Integer mobileNumberId;

    @Column
    private Integer countryCodeId;

    @Column
    private String mobileNumber;

    @Column
    private Integer userId;

    public Integer getMobileNumberId() {
        return mobileNumberId;
    }

    public void setMobileNumberId(Integer mobileNumberId) {
        this.mobileNumberId = mobileNumberId;
    }

    public Integer getCountryCodeId() {
        return countryCodeId;
    }

    public void setCountryCodeId(Integer countryCodeId) {
        this.countryCodeId = countryCodeId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "MobileNumber{" + "mobileNumberId=" + mobileNumberId + ", countryCodeId=" + countryCodeId + ", mobileNumber=" + mobileNumber + ", userId=" + userId + '}';
    }

}
