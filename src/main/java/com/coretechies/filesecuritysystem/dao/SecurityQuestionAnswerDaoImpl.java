/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.SecurityQuestionAnswer;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("SecurityQuestionAnswerDao")
public class SecurityQuestionAnswerDaoImpl implements SecurityQuestionAnswerDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = false)
    public SecurityQuestionAnswer saveSecurityQuestionAnswers(SecurityQuestionAnswer securityQuestionAnswer) {
        try {
            entityManager.persist(securityQuestionAnswer);
            return securityQuestionAnswer;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<SecurityQuestionAnswer> getSecurityQuestionAnswers(Integer userId) {
        try {
            TypedQuery<SecurityQuestionAnswer> createQuery = entityManager.createQuery("SELECT SA FROM SecurityQuestionAnswer AS SA WHERE SA.userId=:userId", SecurityQuestionAnswer.class);
            createQuery.setParameter("userId", userId);
            return createQuery.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.EMPTY_LIST;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean getSecurityQuestionAnswers(Integer userId, Integer questionId, String answer) {
        try {
            TypedQuery<SecurityQuestionAnswer> createQuery = entityManager.createQuery("SELECT SA FROM SecurityQuestionAnswer AS SA WHERE SA.userId=:userId AND SA.securityQuestionId=:questionId AND SA.answer=:answer", SecurityQuestionAnswer.class);
            createQuery.setParameter("userId", userId);
            createQuery.setParameter("questionId", questionId);
            createQuery.setParameter("answer", answer);
            return createQuery.getSingleResult() != null;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    @Override
    public SecurityQuestionAnswer getAnswerByQuestionId(Integer questionId, Integer userId){
        try {
            TypedQuery<SecurityQuestionAnswer> createQuery = entityManager.createQuery("SELECT SA FROM SecurityQuestionAnswer AS SA WHERE SA.userId=:userId AND SA.securityQuestionId=:questionId", SecurityQuestionAnswer.class);
            createQuery.setParameter("userId", userId);
            createQuery.setParameter("questionId", questionId);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    @Transactional
    public Boolean editAnswer(SecurityQuestionAnswer sqa){
        try{
            SecurityQuestionAnswer add=entityManager.find(SecurityQuestionAnswer.class,sqa.getSecurityQuestionAnswerId());
            if(add!=null){
                entityManager.merge(sqa);
            }
            return true;
        }
        catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
    
    @Override
    public List<SecurityQuestionAnswer> getSecurityQuestionAnswersByQuestionId(Integer questionId) {
        try {
            System.out.println("QUESTION ID=="+questionId);
            TypedQuery<SecurityQuestionAnswer> createQuery = entityManager.createQuery("SELECT SA FROM SecurityQuestionAnswer AS SA WHERE SA.securityQuestionId=:questionId", SecurityQuestionAnswer.class);
            createQuery.setParameter("questionId", questionId);
            return createQuery.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
