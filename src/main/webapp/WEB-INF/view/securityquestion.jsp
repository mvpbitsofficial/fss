<%-- 
    Document   : changepassword
    Created on : 9 Aug, 2016, 2:24:32 PM
    Author     : CoreTechies M8
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>FSS | Profile</title>
        <script>var myContextPath = "${pageContext.request.contextPath}";</script>
        <script src="${pageContext.request.contextPath}/js/jquery-1.10.2.min.js"></script>
    </head>
    <body>
        <h1>Hello World!</h1>
        <table>
            <tr>
                <td>
                    Sr. No.
                </td>
                <td>
                    Question
                </td>
            </tr>
            <c:forEach items="${questions}" var="question" varStatus="status">
                <tr>
                    <td>
                        ${status.count}
                    </td>
                    <td>
                        ${question.question}
                    </td>
                </tr>
            </c:forEach>
        </table>
        <form action="#">
            <div>
                <input type="text" id="question"/>
                <br/>
                <label style="color: red;" id="errorMessageQuestion"></label>
            </div>
            <div>
                <button type="button" id="addSecurityQuestion">Submit</button>
                <br/>
                <label style="color: red;" id="errorMessageAdd"></label>
            </div>
        </form>
        <script>
            $(document).on("click", "#addSecurityQuestion", function () {
                var question = $("#question").val().trim();
                if (question === null || question === "") {
                    $("#errorMessageQuestion").text("Please Enter Question.");
                } else {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/addsecurityquestion", {question: question},
                            function (data, status) {
                                if (data === "success") {
                                    window.location.reload();
                                } else if (data === "Please login again.") {
                                    window.location.href = "${pageContext.request.contextPath}/";
                                } else {
                                    $("#errorMessageAdd").text(data);
                                }
                            }
                    );
                }
            });

            $(document).on("keyup", "#question", function () {
                var question = $("#question").val();
                if (question === "") {
                    $("#errorMessageQuestion").text("Please Enter Question.");
                } else {
                    $("#errorMessageQuestion").text("");
                }
            });

        </script>
    </body>
</html>
