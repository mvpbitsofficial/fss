<%-- 
    Document   : changepassword
    Created on : 9 Aug, 2016, 2:24:32 PM
    Author     : CoreTechies M8
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>FSS | Update Watermark</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top"> 
            <!-- BEGIN HEADER INNER -->
            <jsp:include page="topbar.jsp" />
            <!-- END HEADER INNER --> 
        </div>
        <!-- END HEADER --> 
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER --> 
        <!-- BEGIN CONTAINER -->
        <div class="page-container"> 
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper"> 
                <!-- BEGIN SIDEBAR --> 
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse"> 
                    <!-- BEGIN SIDEBAR MENU --> 
                    <jsp:include page="sidebar.jsp" />
                    <!--                    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                                             DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
                                            <li class="sidebar-toggler-wrapper hide"> 
                                                 BEGIN SIDEBAR TOGGLER BUTTON 
                                                <div class="sidebar-toggler"> <span></span> </div>
                                                 END SIDEBAR TOGGLER BUTTON  
                                            </li>
                                            <li class="nav-item  "> <a href="dashboard" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
                                            <li class="nav-item  active"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
                    
                                            </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">License</span>  </a>
                    
                                            </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
                    
                                            </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
                                            <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
                                        </ul>-->
                    <!-- END SIDEBAR MENU --> 
                    <!-- END SIDEBAR MENU --> 
                </div>
                <!-- END SIDEBAR --> 
            </div>
            <!-- END SIDEBAR --> 
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper"> 
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content"> 
                    <!-- BEGIN PAGE HEADER--> 

                    <!-- BEGIN PAGE TITLE-->
                    <!--<h3 class="page-title"> License 
                    <!-- <small>blank page layout</small>-- 
                   </h3>-->
                    <!-- END PAGE TITLE--> 
                    <!-- END PAGE HEADER-->


                    <div class="row">
                        ${message}
                        <div class="col-sm-12">
                            <div class="portlet light portlet-fit portlet-datatable borderd box-shadow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-frame "></i>
                                        <span class="caption-subject  sbold uppercase">Update Watermark</span>
                                    </div>
                                    <!--<div class="actions">
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-refresh"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-printer"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-docs"></i>
                                        </a>
                                         
                                    </div>-->
                                </div>
                                <div class="portlet-body clearfix">
                                    <div class="col-sm-8 clearfix">
                                        <form action="#">
                                            <div class="form-group clearfix">
                                                <div>
                                                    <input type="hidden" id="watermarkId" value="${watermark.watermarkId}"/>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label"> Watermark Name</label>
                                                    <div class="col-sm-8" >
                                                        <input type="text" class="form-control input-circle" id="watermarkName" value="${watermark.watermarkName}"/>
                                                        <br/>
                                                        <label style="color: red;" id="errorMessageWatermarkName"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label"> Watermark Position</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control input-circle" id="watermarkPosition" value="${watermark.textPosition}"/>
                                                        <br/>
                                                        <label style="color: red;" id="errorMessageWatermarkPosition"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label"> Watermark Message</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control input-circle" id="watermarkMessage" value="${watermark.watermarkMessage}"/>
                                                        <br/>
                                                        <label style="color: red;" id="errorMessageWatermarkMessage"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-circle green" id="updateWatermark">Update Watermark</button>
                                                    <br/>
                                                    <label style="color: red;" id="errorMessageUpdateWatermark"></label>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="border border-default use-div">
                                            <h4>How to Work</h4>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
                                            </p><p> Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>               
                    </div>


                </div>
                <!-- END CONTENT BODY -->  
            </div>
            <!-- END CONTENT --> 
            <!-- BEGIN QUICK SIDEBAR --> 
            <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 

            <!-- END QUICK SIDEBAR --> 
        </div>
        <!-- END CONTAINER --> 
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; Organization </div>
            <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
        </div>
        <!-- END FOOTER --> 

        <!-- BEGIN CORE PLUGINS --> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
        <!-- END CORE PLUGINS --> 
        <!-- BEGIN THEME GLOBAL SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
        <!-- END THEME GLOBAL SCRIPTS --> 
        <!-- BEGIN THEME LAYOUT SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
        <!-- END THEME LAYOUT SCRIPTS --> 
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<!--        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>-->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- END PAGE LEVEL PLUGINS -->
<!--         <script src="${pageContext.request.contextPath}/assets/pages/scripts/profile.min.js" type="text/javascript"></script>-->
        <script src="${pageContext.request.contextPath}/js/jquery-1.10.2.min.js"></script>
        
        <script>
            $("#updateWatermark").on("click", function () {
                var watermarkId = $("#watermarkId").val().trim();
                var watermarkName = $("#watermarkName").val().trim();
                var watermarkPosition = $("#watermarkPosition").val().trim();
                var watermarkMessage = $("#watermarkMessage").val().trim();
                $("#errorMessageWatermarkName").html("");
                $("#errorMessageWatermarkMessage").html("");
                $("#errorMessageUpdateWatermark").html("");
                var flag = true;
                if (watermarkId === null || watermarkId === "") {
                    $("#errorMessageUpdateWatermark").html("Please choose appropriate watermark.");
                    flag = false;
                }
                if (watermarkName === null || watermarkName === "") {
                    $("#errorMessageWatermarkName").html("Please enter watermark name.");
                    flag = false;
                }
                if (watermarkPosition === null || watermarkPosition === "") {
                    $("#errorMessageWatermarkPosition").html("Please enter watermark Position.");
                    flag = false;
                }
                if (watermarkPosition !== "1" && watermarkPosition !== "2" && watermarkPosition !== "3" && watermarkPosition !== "4" && watermarkPosition !== "5") {
                    $("#errorMessageWatermarkPosition").html("Please enter watermark Position Between 1 to 5.");
                    flag = false;
                }
                if (watermarkMessage === null || watermarkMessage === "") {
                    $("#errorMessageWatermarkMessage").html("Please enter watermark message.");
                    flag = false;
                }
                if (flag) {
                    $.post("${pageContext.request.contextPath}/${user.userName}/updatewatermark", {watermarkId: watermarkId, watermarkName: watermarkName, watermarkMessage: watermarkMessage, watermarkPosition: watermarkPosition },
                            function (data, status) {
                                if (data === "success") {
                                    alert("Watermark updated successfully");
                                    window.location.href = "${pageContext.request.contextPath}/${user.userName}/viewwatermark";
                                } else {
                                    $("#errorMessageUpdateWatermark").html(data);
                                }
                            }
                    );
                }
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#side-menu').find('li').removeClass('active');
                $(document).find(".user").addClass('active');
            });
        </script>
    </body>
</html>