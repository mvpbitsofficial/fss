/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.CountryCode;
import java.util.List;

/**
 *
 * @author CoreTechies M8
 */
public interface CountryDao {
    
    public List<CountryCode> getCountryCodes();
    
    public CountryCode getCountryById(Integer countryCodeId);
}
