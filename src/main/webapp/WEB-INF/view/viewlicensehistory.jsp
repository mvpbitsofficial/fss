<%-- 
    Document   : changepassword
    Created on : 9 Aug, 2016, 2:24:32 PM
    Author     : CoreTechies M8
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>FSS | History</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
 <link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top"> 
 <!-- BEGIN HEADER INNER -->
 <jsp:include page="topbar.jsp" />
 <!-- END HEADER INNER --> 
</div>
<!-- END HEADER --> 
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER --> 
<!-- BEGIN CONTAINER -->
<div class="page-container"> 
 <!-- BEGIN SIDEBAR -->
 <div class="page-sidebar-wrapper"> 
  <!-- BEGIN SIDEBAR --> 
  <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
  <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
  <div class="page-sidebar navbar-collapse collapse"> 
   <!-- BEGIN SIDEBAR MENU --> 
   <jsp:include page="sidebar.jsp" />
<!--   <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
     DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
    <li class="sidebar-toggler-wrapper hide"> 
      BEGIN SIDEBAR TOGGLER BUTTON 
     <div class="sidebar-toggler"> <span></span> </div>
      END SIDEBAR TOGGLER BUTTON  
    </li>
    <li class="nav-item  "> <a href="index.html" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
    <li class="nav-item  "> <a href="users.html" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
     
    </li>
    <li class="nav-item  active"> <a href="licence.html" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">Licence</span>  </a>
     
    </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
     
    </li>
    <li class="nav-item  "> <a href="report.html" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
    <li class="nav-item  "> <a href="watermark.html" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
    <li class="nav-item  "> <a href="settings.html" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
   </ul>-->
   <!-- END SIDEBAR MENU --> 
   <!-- END SIDEBAR MENU --> 
  </div>
  <!-- END SIDEBAR --> 
 </div>
 <!-- END SIDEBAR --> 
 <!-- BEGIN CONTENT -->
 <div class="page-content-wrapper"> 
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content"> 
   <!-- BEGIN PAGE HEADER--> 
   
   <!-- BEGIN PAGE TITLE-->
   <!--<h3 class="page-title"> Licence 
    <!-- <small>blank page layout</small>-- 
   </h3>-->
   <!-- END PAGE TITLE--> 
   <!-- END PAGE HEADER-->
   
 
  <div class="row">
         <div class="col-sm-12">
         	<div class="portlet box red   portlet-datatable  ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-clock "></i>
                                        History
                                    </div>
                                    <div class="actions">
                                        <a class="btn  btn-icon-only " href="javascript:;" data-toggle="tooltip" data-placement="top" title="Print"  id="exportnotification">
                                            <i class="icon-printer"></i>
                                        </a>
                                        <div class="btn-group"> <a class="btn btn-sm white btn-outline btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Filter By <i class="fa fa-angle-down"></i> </a>
                                                <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" >
                                                    <label class="mt-checkbox">
                                                        <input type="checkbox" class="mt-checkbox-outline" checked="" id="allnotification"/>
                                                        All <span></span> </label>
                                                    <label class="mt-checkbox ">
                                                        <input type="checkbox" class="mt-checkbox-outline" checked="" id="userscheckbox" />
                                                        Users <span></span> </label>
                                                    <label class="mt-checkbox ">
                                                        <input type="checkbox" class="mt-checkbox-outline" checked="" id="coursecheckbox" />
                                                        Courses <span></span> </label>
                                                    <label class="mt-checkbox ">
                                                        <input type="checkbox" class="mt-checkbox-outline" checked="" id="licensecheckbox" />
                                                        License <span></span> </label>
                                                    <label class="mt-checkbox ">
                                                        <input type="checkbox" class="mt-checkbox-outline" checked="" id="watermarkcheckbox"/>
                                                        Water Mark <span></span> </label>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_6">
                                        <thead>
                                            <tr>
                                                <th> Sr </th>
                                                <th> Notification message</th>
                                                <th> Date and Time</th>
                                            </tr>
                                        </thead>
                                       
                                        <tbody id="tBody">
                                            <c:forEach items="${licensehistories}" var="history" varStatus="status">
                                                
                                                <tr class="odd gradeX row_count">
                                                    <td>${status.count}</td>
                                                    <td>${history.notificationMessage}</td>
                                                    <td>${history.createOn}</td>
                                                </tr>
                                                
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    <span style="color: red; display: block; font-size: 14px;" id="errorhistory" ></span>
                                </div>
                            </div>
         </div>               
  </div>
   
   
  </div>
  <!-- END CONTENT BODY --> 
 </div>
 <!-- END CONTENT --> 
 <!-- BEGIN QUICK SIDEBAR --> 
 <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 
 
 <!-- END QUICK SIDEBAR --> 
</div>
<!-- END CONTAINER --> 
<!-- BEGIN FOOTER -->
<div class="page-footer">
 <div class="page-footer-inner"> 2016 &copy; Organization </div>
 <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
</div>



<!-- BEGIN CORE PLUGINS --> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
<!-- END CORE PLUGINS --> 
<!-- BEGIN THEME GLOBAL SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
<!-- END THEME GLOBAL SCRIPTS --> 
<!-- BEGIN THEME LAYOUT SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
<!-- END THEME LAYOUT SCRIPTS --> 
<!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="${pageContext.request.contextPath}/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
<script src="${pageContext.request.contextPath}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/clipboard/clipboard.min.js"></script>
		
            <script>
		$(function () {
                    $('[data-toggle="tooltip"]').tooltip();
		});
	    </script>
            <script>
                $(document).ready(function () {
                    $('#side-menu').find('li').removeClass('active');
                    $(document).find(".history").addClass('active');
                    $('#sample_6').dataTable();
                    
                    console.log($('.row_count').length);
                    if($('.row_count').length>0){
                        $('#exportnotification').css('display','inline');
                    }else{
                        $('#exportnotification').css('display','none');
                    }
                });
            </script>
            <script>
                 $("#watermarkcheckbox").on("click", function () {
                       $("#allnotification").attr('checked', false);
                 });
                 $("#licensecheckbox").on("click", function () {
                         $("#allnotification").attr('checked', false);
                 });
                 
                 $("#coursecheckbox").on("click", function () {
                         $("#allnotification").attr('checked', false);
                 });
                 $("#userscheckbox").on("click", function () {
                         $("#allnotification").attr('checked', false);
                 });
                 
            </script>
            <script>
                $("#allnotification").on("click", function () {
                     if($("#allnotification").is(':checked')){
                         $("#userscheckbox").prop('checked', true);
                         $("#watermarkcheckbox").prop('checked', true);
                         $("#licensecheckbox").prop('checked', true);
                         $("#coursecheckbox").prop('checked', true);
                         var Uname = "${user.userName}".replace(" ", "");
                        $.get("${pageContext.request.contextPath}/"+Uname+"/getallnotification",
                            function(data, status){
                                if(data.indexOf("success") > -1){
                                    var wsHtml = '';
                                    obj = JSON.parse(data.split("#")[1]);
                                    var shareInfoLen = obj.length;
                                    for(var i = 0; i < shareInfoLen; i++){
                                        console.log( obj[i].createOn +"," + obj[i].message);
                                    }
         //                           $('#sample_6').dataTable().fnDestroy();
         //                           $('#sample_6 tBody').empty();
                                    for (var i = 0; i < shareInfoLen; i++) {
                                         wsHtml += '<tr>';
                                         wsHtml += '<td>' + (i+1).valueOf() + '</td>';
                                         wsHtml += '<td>' + (obj[i].message).valueOf() + '</td>';
                                         wsHtml += '<td>' + (obj[i].createOn).valueOf() + '</td>';
                                         wsHtml += '</tr>';
                                     }
                                     $('#sample_6 > tBody:last').append(wsHtml);
                                     $('#sample_6').dataTable();
                                }else{
                                    $("#errorhistory").text(data);
                                }
                        });
                     } else{
                         $("#userscheckbox").attr('checked', false);
                         $("#watermarkcheckbox").attr('checked', false);
                         $("#licensecheckbox").attr('checked', false);
                         $("#coursecheckbox").attr('checked', false);
                     }
                 });
            </script>
            <script>
                $(".mt-checkbox-outline").on("click", function () {
                    $('#sample_6').dataTable().fnDestroy();
                    $('#sample_6 tBody').empty();
                    var wsHtml = '';
                    if(!$("#allnotification").is(':checked')){
                    var checkedData = [];
                    console.log($("#userscheckbox").is(':checked'));
                    console.log($("#coursecheckbox").is(':checked'));
                    console.log($("#licensecheckbox").is(':checked'));
                    console.log($("#watermarkcheckbox").is(':checked'));
                   
                    if($("#userscheckbox").is(':checked')){
//                       $("#coursecheckbox").attr('checked', false);
//                       $("#licensecheckbox").attr('checked', false);
//                       $("#watermarkcheckbox").attr('checked', false);
                       console.log("users");
                       checkedData.push(2);  
                    }
                    if($("#coursecheckbox").is(":checked")){
                       console.log("course");
                       checkedData.push(5);  
                    }
                    if($("#licensecheckbox").is(":checked")){
                       console.log("license");
                       checkedData.push(1);  
                    }
                    if($("#watermarkcheckbox").is(":checked")){
                       console.log("watermark");
                       checkedData.push(6);  
                    }
                        if($("#userscheckbox").is(':checked') || $("#coursecheckbox").is(":checked") || $("#licensecheckbox").is(":checked") || $("#watermarkcheckbox").is(":checked")){
                            var Uname = "${user.userName}".replace(" ", "");
                            $.post("${pageContext.request.contextPath}/"+Uname+"/getnotificationbytype", {typs: checkedData },
                            function(data, status){
                                if(data.indexOf("success") >-1){
                                    console.log(data);
                                    obj = JSON.parse(data.split("#")[1]);
                                    var shareInfoLen = obj.length;
                                    for(var i = 0; i < shareInfoLen; i++){
                                        console.log( obj[i].createOn +"," + obj[i].message);
                                    }
         //                           $('#sample_6').dataTable().fnDestroy();
         //                           $('#sample_6 tBody').empty();
                                    for (var i = 0; i < shareInfoLen; i++) {
                                         wsHtml += '<tr>';
                                         wsHtml += '<td>' + (i+1).valueOf() + '</td>';
                                         wsHtml += '<td>' + (obj[i].message).valueOf() + '</td>';
                                         wsHtml += '<td>' + (obj[i].createOn).valueOf() + '</td>';
                                         wsHtml += '</tr>';
                                     }
                                     $('#sample_6 > tBody:last').append(wsHtml);
                                     $('#sample_6').dataTable();
                                } else{
                                    $("#errorhistory").text(data);
                                }
                           });
                        }
                   }
                });
            </script>
            <script>
                    $("#exportnotification").on("click", function(){
                        var Uname = "${user.userName}".replace(" ", "");
                        window.location.href = "${pageContext.request.contextPath}/"+Uname+"/exportLicenseHistory";
                    });
            </script>