/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.service;

import com.coretechies.filesecuritysystem.domain.UserInfo;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;

/**
 *
 * @author CoreTechies M8
 */
public interface UserInfoService {

    public UserInfo authenticate(String userName, String password) throws NoSuchAlgorithmException;

    public UserInfo updatePassword(UserInfo userInfo, String password) throws NoSuchAlgorithmException;

    public boolean authenticate(String userName, String currentPassword, UserInfo userInfo) throws NoSuchAlgorithmException;

    public UserInfo createClientUser(String userName, String password, String emailId, String profilePic, Integer countryId, Integer mobileCode, String mobileNumber, Integer userType, String organization, Boolean userStatus, Integer createdBy) throws NoSuchAlgorithmException;

    public UserInfo updateClientUser(Integer userId, String userName, String emailId, String profilePic, Integer countryId, Integer mobileCode, String mobileNumber, Integer userType, String organization, Boolean userStatus, Integer createdBy) throws NoSuchAlgorithmException;

    public UserInfo createUser(String userName, String password, String emailId, String profilePic, Integer countryId, Integer mobileCode, String mobileNumber, Integer userType, Boolean userStatus, Integer createdBy) throws NoSuchAlgorithmException;

    public UserInfo updateUser(Integer userId, String userName, String emailId, String profilePic, Integer countryId, Integer mobileCode, String mobileNumber, Integer userType, Boolean userStatus, Integer createdBy) throws NoSuchAlgorithmException;

    public UserInfo deleteUser(Integer userId);

    public UserInfo deactivateClient(Integer userId);

    public UserInfo activateClient(Integer userId);
    
    public Integer getUserInfoCountByCreatedBy(Integer createdBy);
    
    public List<UserInfo> chartUserData(Integer createdBy, Date date);
    
    public Map<Integer, Integer> showUserIdThroughCount();
    
}
