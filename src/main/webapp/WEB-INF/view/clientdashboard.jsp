
<%@page import="com.coretechies.filesecuritysystem.domain.Notification"%>
<%@page import="com.coretechies.filesecuritysystem.domain.UserInfo"%>
<%@page import="java.util.Date"%>
<%@page import="org.ocpsoft.prettytime.PrettyTime"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>

<%!
   
    
      
    public String joinDate(Date date){
        String data="";
        PrettyTime p = new PrettyTime();
        data=  p.format(date);
        return data;
    }


%>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    

    <head>
        <meta charset="utf-8" />
        <title>FSS |Dashboard</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="${pageContext.request.contextPath}/assets/global/plugins/slick-master/slick/slick.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <!--<link rel="shortcut icon" href="favicon.ico" />-->
        <style>
            .cke_panel{z-index: 10060!important;}
        </style>
        <style>
            .mt-widget-1{ min-height:245px;}
            .mt-body{ margin-top:120px;}
            .user-alphabate{
                display: block;
             position: relative;
                -webkit-transition: all .4s linear;
                transition: all .4s linear;
                width: 100%;
                height: 85px;
                line-height:80px;
                background:#36c6d3;
                font-size:30px;
                color:#fff;
                text-shadow:0px 1px 2px rgba(0,0,0,0.2);
            }

            .big-text{ font-size:45px;}
            .mt-element-overlay .mt-overlay-1:hover .user-alphabate{
            -ms-transform: scale(1.2) translateZ(0);
               -webkit-transform: scale(1.2) translateZ(0);
               transform: scale(1.2) translateZ(0);
            }
 
        </style>
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top"> 
            <!-- BEGIN HEADER INNER -->
            <jsp:include page="topbar.jsp" />
            <!-- END HEADER INNER --> 
        </div>
        <!-- END HEADER --> 
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER --> 
        <!-- BEGIN CONTAINER -->
        <div class="page-container"> 
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper"> 
                <!-- BEGIN SIDEBAR --> 
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse"> 
                    <!-- BEGIN SIDEBAR MENU --> 
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) --> 
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode --> 
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode --> 
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded --> 
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <jsp:include page="sidebar.jsp" />
                    <!-- END SIDEBAR MENU --> 
                    <!-- END SIDEBAR MENU --> 
                </div>
                <!-- END SIDEBAR --> 
            </div>
            <!-- END SIDEBAR --> 
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper"> 
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content"> 
                    <!-- BEGIN PAGE HEADER--> 

                    <!-- BEGIN PAGE TITLE-->
                    <h3 class="page-title"> Dashboard 
                        <!-- <small>blank page layout</small>--> 
                    </h3>
                    <!-- END PAGE TITLE--> 
                    <!-- END PAGE HEADER-->
                    <div class="row widget-row">
                        <div class="col-md-3"> 
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb portlet light bordered box-shadow">
                                <div class="portlet-title widget-title">
                                    <div class="caption"> <i class="icon-cursor font-dark hide"></i> <span class="caption-subject font-dark  ">Users</span> </div>

                                </div>
                                <div class="portlet-body">
                                    <div class="widget-thumb-wrap"> <i class="widget-thumb-icon  fa fa-users"></i>
                                        <div class="widget-thumb-body"> <span class="widget-thumb-body-stat" data-counter="counterup" data-value="${totalUsers}">0</span> <!--<span class="widget-thumb-subtitle">Orders since 2016</span>--> </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB --> 
                        </div>
                        <div class="col-md-3"> 
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb portlet light bordered box-shadow">
                                <div class="portlet-title widget-title">
                                    <div class="caption"> <i class="icon-cursor font-dark hide"></i> <span class="caption-subject font-dark  ">Courses</span> </div>

                                </div>
                                <div class="portlet-body">
                                    <div class="widget-thumb-wrap"> <i class="widget-thumb-icon  fa fa-book"></i>
                                        <div class="widget-thumb-body"> <span class="widget-thumb-body-stat" data-counter="counterup" data-value="${totalCourses}" > 0 </span> <!--<span class="widget-thumb-subtitle">Raised from 89 orders.</span> --></div>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB --> 
                        </div>
                        <div class="col-md-3"> 
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb portlet light bordered box-shadow">
                                <div class="portlet-title widget-title">
                                    <div class="caption"> <i class="icon-cursor font-dark hide"></i> <span class="caption-subject font-dark  ">License</span> </div>

                                </div>
                                <div class="portlet-body">
                                    <div class="widget-thumb-wrap"> <i class="widget-thumb-icon  fa fa-credit-card"></i>
                                        <div class="widget-thumb-body"> <span class="widget-thumb-body-stat" data-counter="counterup" data-value="${totalLicenses}">0</span> <!--<span class="widget-thumb-subtitle">37 average daily visitors.</span>--> </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB --> 
                        </div>
                        <div class="col-md-3"> 
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb portlet light bordered box-shadow">
                                <div class="portlet-title widget-title">
                                    <div class="caption"> <i class="icon-cursor font-dark hide"></i> <span class="caption-subject font-dark  ">System</span> </div>

                                </div>
                                <div class="portlet-body">
                                    <div class="widget-thumb-wrap"> <i class="widget-thumb-icon  fa fa-desktop"></i> 
                                        <div class="widget-thumb-body"> <span class="widget-thumb-body-stat" data-counter="counterup" data-value="${totalSystems}">0</span> <!--<span class="widget-thumb-subtitle">23 new customers </span>--> </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB --> 
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                    <div class="clearfix">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="portlet light bordered box-shadow">
                                    <div class="portlet-title">
                                        <div class="caption"> <i class="icon-bubble font-dark hide"></i> <span class="caption-subject font-hide bold uppercase">Recent Users</span> </div>
                                        <div class="actions">
                                            <c:set var="Uname" value="${user.userName}"/>
                                            <div class="btn-group"> <a class="btn green-haze btn-outline btn-circle btn-sm" href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/viewuser" > View All </a> </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body mt-element-card mt-card-round  mt-element-overlay">
                                        <div class="row slider user-slider">
                                            <c:forEach items="${users}" var="userInfo">
                                               <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="mt-card-item mt-widget-1" style=" padding:40px 30px 10px;">
							<div class="actions ">
                                                            <div class="btn-group"> <a href="" class="btn  btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <i class="icon-options-vertical"></i> </a>
                                                                    <c:set var="Uname" value="${user.userName}"/>
                                                                    <ul class="dropdown-menu pull-right">
                                                                        <li><a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/updateuser/${userInfo.userInfoId}"><i class="fa fa-edit"></i> Edit</a></li>
                                                                        <li><a href="#" class="deleteuser" id="${userInfo.userInfoId}"><i class="fa fa-trash-o"></i> Delete</a></li>
                                                                    </ul>
                                                            </div>
							</div>
                                                    <div class="mt-card-avatar mt-overlay-1 ">
                                                        <span class="user-alphabate big-text">${userInfo.displayName}</span>
                                                        <div class="mt-overlay mt-top">
                                                            <c:set var="Uname" value="${user.userName}"/>
                                                            <ul class="mt-info">
                                                                <li>
                                                                    <a class="btn default btn-outline"  href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/view/${userInfo.userInfoId}">
                                                                        <i class="icon-eye"></i>
                                                                    </a>
                                                                </li>
                                                               
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="mt-body" style="margin-left:-25px; margin-right:-25px;">
                                                        <h3 class="mt-username">${userInfo.userName}</h3>
							<p class="mt-user-title" id="joindate" value="${userInfo.creationDate}"> Joined
                                                        <% 
                                                            UserInfo userInfo = (UserInfo)pageContext.getAttribute("userInfo"); 
                                                                out.print(joinDate(userInfo.getCreationDate()));
                                                            %>
                                                        </p>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            </c:forEach>
                                        </div>

                                        <div class="scroller-footer">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="portlet light bordered box-shadow">
                                    <div class="portlet-title">
                                        <div class="caption"> <i class="icon-share font-dark hide"></i> <span class="caption-subject font-dark bold uppercase">Recent Activities</span> </div>
                                        <div class="actions">
                                            <div class="btn-group"> <a class="btn btn-sm blue btn-outline btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Filter By <i class="fa fa-angle-down"></i> </a>
                                                <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
                                                     <label class="mt-checkbox">
                                                        <input type="checkbox" class="mt-checkbox-outline" checked="" id="allnotification"/>
                                                        All <span></span> </label>
                                                    <label class="mt-checkbox ">
                                                        <input type="checkbox" class="mt-checkbox-outline" checked="" id="userscheckbox" />
                                                        Users <span></span> </label>
                                                    <label class="mt-checkbox ">
                                                        <input type="checkbox" class="mt-checkbox-outline" checked="" id="coursecheckbox" />
                                                        Courses <span></span> </label>
                                                    <label class="mt-checkbox ">
                                                        <input type="checkbox" class="mt-checkbox-outline" checked="" id="licensecheckbox" />
                                                        License <span></span> </label>
                                                    <label class="mt-checkbox ">
                                                        <input type="checkbox" class="mt-checkbox-outline" checked="" id="watermarkcheckbox"/>
                                                        Water Mark <span></span> </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="scroller" style="height: 295px;" data-always-visible="1" data-rail-visible="0">
                                            <ul class="feeds">
                                                <c:forEach var="notify" items="${notifications}">
                                                    <li class="${notify.notificationType}" style="display: none;">
                                                    <div class="col1">
                                                        <div class="cont">
                                                            <c:if test="${notify.notificationType==1}">
<!--                                                                <div>-->
                                                                    <div class="cont-col1">
                                                                        <div class="label label-sm label-info"><i class="fa fa-credit-card"></i> </div>
                                                                    </div>
                                                                    <div class="cont-col2">
                                                                        <div class="desc">${notify.notificationMessage}</div>
                                                                    </div>
<!--                                                                </div>-->
                                                            </c:if>
                                                            <c:if test="${notify.notificationType==2 || notify.notificationType==3 || notify.notificationType==4}">
                                                                <!--<div>-->
                                                                    <div class="cont-col1">
                                                                        <div class="label label-sm label-info"><i class="fa fa-user"></i> </div>
                                                                    </div>
                                                                    <div class="cont-col2">
                                                                        <div class="desc">${notify.notificationMessage}</div>
                                                                    </div>
                                                                <!--</div>-->
                                                            </c:if>
                                                            <c:if test="${notify.notificationType==5}">
                                                                <!--<div>-->
                                                                    <div class="cont-col1">
                                                                        <div class="label label-sm label-info"> <i class="fa fa-book"></i></i> </div>
                                                                    </div>
                                                                    <div class="cont-col2">
                                                                        <div class="desc">${notify.notificationMessage}</div>
                                                                    </div>
                                                                <!--</div>-->
                                                            </c:if>
                                                            <c:if test="${notify.notificationType==6}">
                                                                <!--<div>-->
                                                                    <div class="cont-col1">
                                                                        <div class="label label-sm label-info"><i class="icon-frame"></i> </div>
                                                                    </div>
                                                                    <div class="cont-col2">
                                                                        <div class="desc">${notify.notificationMessage}</div>
                                                                    </div>
                                                                <!--</div>-->
                                                            </c:if>
                                                            <c:if test="${notify.notificationType==7}">
                                                                <!--<div>-->
                                                                    <div class="cont-col1">
                                                                        <div class="label label-sm label-info"><i class="fa fa-desktop"></i></div>
                                                                    </div>
                                                                    <div class="cont-col2">
                                                                        <div class="desc">${notify.notificationMessage}</div>
                                                                    </div>
                                                                <!--</div>-->
                                                            </c:if>
                                                        </div>
                                                    </div>
                                                    <div class="col2">
                                                        <div class="date">
                                                        
                                                        <% 
                                                            Notification notification = (Notification)pageContext.getAttribute("notify"); 
                                                                out.print(joinDate(notification.getCreateOn()));
                                                        %>
                                                        
                                                        </div>
                                                    </div>
                                                </li>
                                                </c:forEach>
                                            </ul>
                                        </div>
                                        <div class="scroller-footer">
                                            <c:set var="Uname" value="${user.userName}"/>
                                            <div class="btn-arrow-link pull-right"> <a href="${pageContext.request.contextPath}/${fn:replace(Uname, " ", "")}/licensehistory">See All Records</a> <i class="icon-arrow-right"></i> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6"> 
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light bordered box-shadow">
                                <div class="portlet-title">
                                    <div class="caption"> <i class="icon-bar-chart font-dark hide"></i> <span class="caption-subject font-dark bold uppercase">Users</span> <span class="caption-helper"></span> </div>
                                    <div class="actions">
                                        <div class="btn-group"> <a class="btn  btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true"> <i class="fa fa-bars"></i> </a>
                                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
                                                     <label class="mt-checkbox">
                                                        <input type="checkbox" class="" id="getchartdataofyear"/>
                                                        Year <span></span> </label>
                                                    <label class="mt-checkbox ">
                                                        <input type="checkbox" class="" id="getchartdataofhalfyear" />
                                                        Half-Year <span></span> </label>
                                                    <label class="mt-checkbox ">
                                                        <input type="checkbox" class="" id="getchartdataofquarter" />
                                                        Quarter <span></span> </label>
                                                    <label class="mt-checkbox ">
                                                        <input type="checkbox" class="" id="getchartdataofmonth" />
                                                        Month <span></span> </label>
                                                    <label class="mt-checkbox ">
                                                        <input type="checkbox" class="" checked id="getchartdataofweek"/>
                                                        Week <span></span> </label>
                                            </div>
<!--                                            <ul class="dropdown-menu pull-right">
                                                
                                            <li class="radio">
                                                <label>
                                                    <input type="radio" name="radioname" value="1" onclick="getchartdataofyear();"> Year
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="radioname" value="2" onclick="getchartdataofhalfyear();"> Half-Year
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="radioname" value="3" onclick="getchartdataofquarter();"> Quarter
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="radioname" value="4" onclick="getchartdataofmonth();"> Month
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="radioname" value="5"  onclick="getchartdataofweek();"> Week
                                                </label>
                                            </li>
                                            </ul>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                 <div id="site_statistics_loading"> <!--<img src="${pageContext.request.contextPath}/assets/global/img/loading.gif" alt="loading" /> --></div>
                                    <div id="site_statistics_content" class="display-none">
                                        <div id="site_statistics" class="chart"> </div>
                                        <div id="userchart"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET--> 
                        </div>
                        <div class="col-md-6 col-sm-6"> 
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light bordered box-shadow">
                                <div class="portlet-title">
                                    <div class="caption"> <i class="icon-bar-chart font-dark hide"></i> <span class="caption-subject font-dark bold uppercase">License</span> <span class="caption-helper"></span> </div>
                                    <div class="actions">
                                        <div class="btn-group"> <a class="btn  btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true"><i class="fa fa-bars"></i></a>
                                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
                                                     <label class="mt-checkbox">
                                                        <input type="checkbox" class="" id="licensechartdataofyear"/>
                                                        Year <span></span> </label>
                                                    <label class="mt-checkbox ">
                                                        <input type="checkbox" class="" id="licensechartdataofhalfyear" />
                                                        Half-Year <span></span> </label>
                                                    <label class="mt-checkbox ">
                                                        <input type="checkbox" class="" id="licensechartdataofquater" />
                                                        Quarter <span></span> </label>
                                                    <label class="mt-checkbox ">
                                                        <input type="checkbox" class="" id="licensechartdataofmonth" />
                                                        Month <span></span> </label>
                                                    <label class="mt-checkbox ">
                                                        <input type="checkbox" class="" checked id="licensechartdataofweek"/>
                                                        Week <span></span> </label>
                                            </div>
<!--                                            <ul class="dropdown-menu pull-right">
                                                
                                                <li class="radio">
                                                    <label>
                                                        <input type="radio" name="radioname" value="1" onclick="licensechartdataofyear();"> Year
                                                    </label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="radioname" value="2" onclick="licensechartdataofhalfyear();"> Half-Year
                                                    </label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="radioname" value="3" onclick="licensechartdataofquater();"> Quarter
                                                    </label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="radioname" value="4" onclick="licensechartdataofmonth();"> Month
                                                    </label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="radioname" value="5" onclick="licensechartdataofweek();"> Week
                                                    </label>
                                                </li>
                                            </ul>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                 <div id="site_statistics_loading0"> <!--<img src="${pageContext.request.contextPath}/assets/global/img/loading.gif" alt="loading" /> --></div>
                                    <div id="site_statistics_content0" class="display-none">
                                        <div id="site_statistics0" class="chart"> </div>
                                        <div id="licensechart"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET--> 
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY --> 
            </div>
            <!-- END CONTENT --> 
            <!-- BEGIN QUICK SIDEBAR --> 
            <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 

            <!-- END QUICK SIDEBAR --> 
        </div>
        <!-- END CONTAINER --> 
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; Organization </div>
            <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
        </div>
        <!-- END FOOTER --> 


        <!-- Question Modal --> 
        <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Select Your Security Question </h4>
                    </div>
                    <div class="modal-body"> 
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon input-circle-left">
                                    <i class="fa fa-question"></i>
                                </span>
                                <select class="form-control input-circle-right" id="question1">
                                    <option disable selected>--Select Question--</option>
                                    <c:forEach items="${questions}" var="question">
                                        <option value="${question.securityQuestionId}">${question.question}</option>
                                    </c:forEach>
                                    <!--<option>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.<option>-->
                                </select>
                            </div>
                            <span class="error-message" id="errorMessageQuestion1"></span>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon input-circle-left">
                                    <i class="fa fa-check-circle"></i>
                                </span>
                                <input class="form-control input-circle-right" type="text" placeholder="Please Enter Answer" id="answer1">
                            </div>
                            <span class="error-message" id="errorMessageAnswer1"></span>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon input-circle-left">
                                    <i class="fa fa-question"></i>
                                </span>
                                <select class="form-control input-circle-right" id="question2">
                                    <option disable selected>--Select Question--</option>
                                    <c:forEach items="${questions}" var="question">
                                        <option value="${question.securityQuestionId}">${question.question}</option>
                                    </c:forEach>
                                    <!--<option>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.<option>-->
                                </select>
                            </div>
                            <span class="error-message" id="errorMessageQuestion2"></span>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon input-circle-left">
                                    <i class="fa fa-check-circle"></i>
                                </span>
                                <input class="form-control input-circle-right" type="text" placeholder="Please Enter Answer" id="answer2">
                            </div>
                            <span class="error-message" id="errorMessageAnswer2"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <span style="color: green; display: block; font-size: 14px;" id="addresponse" ></span>
                        <!--<button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>-->
                        <button type="button" class="btn btn-circle btn-success" id="addSecurityQuestion">Add</button>
                        <span class="error-message" id="errorMessageAdd"></span>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="confirm_delete" tabindex="-1" role="delete" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModal">Confirm Delete</h4>
                    </div>
                    <div class="modal-body"> 
                        <div class="form-group">
                            <input type="hidden" id="deleteId" />
                            <span Style="font-style: normal;">Are you sure you want to delete the user account.?</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class=" btn btn green" data-dismiss="modal">NO</button>-->
<!--                        <button type="button" class="btn green" id="">YES</button>-->
                        <a href="javascript:;" id="confirmdeleteuser" class="btn btn-circle btn-success">YES</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        
        <div class="modal fade" id="help_modal" tabindex="-1" role="support" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="helpModallabel">New Email</h4>
                    </div>
                    <div class="modal-body"> 
                        <input type="text" id="ownerEmailId" readonly class="form-control input-circle" />
                        <br/>
                        <input type="text" class="form-control input-circle" id="mailSubject" placeholder="Subject">
                        <span style="font-size: smaller; font-weight:bold;  color: red" id="subjectvalidation" ></span>
                        <br/>
                        <textarea class="textarea" name="textboxtext" id="mailMessage"></textarea>
                        <span style="font-size: smaller; font-weight:bold;color: red" id="messageValidation" ></span>
                    </div>
                    <div id="loading" style="display: none;">
                        <center>
                            <!--<span style="color: blue;">Processing...</span>-->
                            <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/24.gif" />
                        </center>
                    </div>
                    <div class="modal-footer" style="margin-top:0px;">
                        <span style="font-size: smaller;float:left; font-weight:bold;color: green;" id="sendmailresponse" ></span>
                        <button type="button" class="btn btn-default btn-circle" data-dismiss="modal">Close</button>
                        <button type="button" id="sendMailToOwner" class="btn btn-primary btn-circle">Send</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        
        <!-- BEGIN CORE PLUGINS --> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
        <!-- END CORE PLUGINS --> 
        <!-- BEGIN THEME GLOBAL SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
        <!-- END THEME GLOBAL SCRIPTS --> 
        <!-- BEGIN THEME LAYOUT SCRIPTS --> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/ckeditor/adapters/jquery.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
        <!-- END THEME LAYOUT SCRIPTS --> 
        <!-- BEGIN PAGE LEVEL PLUGINS --> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script> 

        <script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-highcharts/highcharts.js"></script>
        <!--<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-highcharts/exporting.js" type="text/javascript"></script>-->
        
        <!-- END PAGE LEVEL PLUGINS --> 

        <script src="${pageContext.request.contextPath}/assets/pages/scripts/dashboard.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/global/plugins/slick-master/slick/slick.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/linechart.js" type="text/javascript"></script>
        <script>
            $(document).ready(function() {
                var date = $("#joindate").val();
//                alert(date);
              });
              
            $(function() {
                $(".textarea").ckeditor();
            });
            
            $("#getchartdataofyear").on("click", function(){
                var Uname = "${user.userName}".replace(" ", "");
                if($("#getchartdataofyear").is(':checked')){
                    $("#getchartdataofhalfyear").attr('checked', false);
                    $("#getchartdataofquarter").attr('checked', false);
                    $("#getchartdataofmonth").attr('checked', false);
                    $("#getchartdataofweek").attr('checked', false);
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getuserDataforchart", {type: 1},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics').highcharts().destroy();
                        createLineChart("site_statistics",data);
                    });
                } else{
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getuserDataforchart", {type: 5},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics').highcharts().destroy();
                        createLineChart("site_statistics",data);
                    });
                }
            });
            
            $("#getchartdataofhalfyear").on("click", function(){
                var Uname = "${user.userName}".replace(" ", "");
                if($("#getchartdataofhalfyear").is(':checked')){
                    $("#getchartdataofyear").attr('checked', false);
                    $("#getchartdataofquarter").attr('checked', false);
                    $("#getchartdataofmonth").attr('checked', false);
                    $("#getchartdataofweek").attr('checked', false);
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getuserDataforchart", {type: 2},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics').highcharts().destroy();
                        createLineChart("site_statistics",data);
                    });
                } else{
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getuserDataforchart", {type: 5},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics').highcharts().destroy();
                        createLineChart("site_statistics",data);
                    });
                }
            });
            
            $("#getchartdataofquarter").on("click", function(){
                var Uname = "${user.userName}".replace(" ", "");
                if($("#getchartdataofquarter").is(':checked')){
                    $("#getchartdataofyear").attr('checked', false);
                    $("#getchartdataofhalfyear").attr('checked', false);
                    $("#getchartdataofmonth").attr('checked', false);
                    $("#getchartdataofweek").attr('checked', false);
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getuserDataforchart", {type: 3},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics').highcharts().destroy();
                        createLineChart("site_statistics",data);
                    });
                } else{
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getuserDataforchart", {type: 5},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics').highcharts().destroy();
                        createLineChart("site_statistics",data);
                    });
                }
            });
            
            $("#getchartdataofmonth").on("click", function(){
                var Uname = "${user.userName}".replace(" ", "");
                if($("#getchartdataofmonth").is(':checked')){
                    $("#getchartdataofyear").attr('checked', false);
                    $("#getchartdataofhalfyear").attr('checked', false);
                    $("#getchartdataofquarter").attr('checked', false);
                    $("#getchartdataofweek").attr('checked', false);
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getuserDataforchart", {type: 4},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics').highcharts().destroy();
                        createLineChart("site_statistics",data);
                    });
                } else{
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getuserDataforchart", {type: 5},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics').highcharts().destroy();
                        createLineChart("site_statistics",data);
                    });
                }
            });
            
            $("#getchartdataofweek").on("click", function(){
                var Uname = "${user.userName}".replace(" ", "");
                if($("#getchartdataofweek").is(':checked')){
                    $("#getchartdataofyear").attr('checked', false);
                    $("#getchartdataofhalfyear").attr('checked', false);
                    $("#getchartdataofquarter").attr('checked', false);
                    $("#getchartdataofmonth").attr('checked', false);
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getuserDataforchart", {type: 5},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics').highcharts().destroy();
                        createLineChart("site_statistics",data);
                    });
                }
            });
            
            
            $("#licensechartdataofyear").on("click", function(){
                var Uname = "${user.userName}".replace(" ", "");
                if($("#licensechartdataofyear").is(':checked')){
                    $("#licensechartdataofhalfyear").attr('checked', false);
                    $("#licensechartdataofquater").attr('checked', false);
                    $("#licensechartdataofmonth").attr('checked', false);
                    $("#licensechartdataofweek").attr('checked', false);
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getlicenseDataforchart", {type: 1},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics0').highcharts().destroy();
                        createLineChart("site_statistics0",data);
                    });
                } else{
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getlicenseDataforchart", {type: 5},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics0').highcharts().destroy();
                        createLineChart("site_statistics0",data);
                    });
                }
            });
            
            $("#licensechartdataofhalfyear").on("click", function(){
                var Uname = "${user.userName}".replace(" ", "");
                if($("#licensechartdataofhalfyear").is(':checked')){
                    $("#licensechartdataofyear").attr('checked', false);
                    $("#licensechartdataofquater").attr('checked', false);
                    $("#licensechartdataofmonth").attr('checked', false);
                    $("#licensechartdataofweek").attr('checked', false);
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getlicenseDataforchart", {type: 2},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics0').highcharts().destroy();
                        createLineChart("site_statistics0",data);
                    });
                } else{
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getlicenseDataforchart", {type: 5},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics0').highcharts().destroy();
                        createLineChart("site_statistics0",data);
                    });
                }
            });
            
            $("#licensechartdataofquater").on("click", function(){
                var Uname = "${user.userName}".replace(" ", "");
                if($("#licensechartdataofquater").is(':checked')){
                    $("#licensechartdataofyear").attr('checked', false);
                    $("#licensechartdataofhalfyear").attr('checked', false);
                    $("#licensechartdataofmonth").attr('checked', false);
                    $("#licensechartdataofweek").attr('checked', false);
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getlicenseDataforchart", {type: 3},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics0').highcharts().destroy();
                        createLineChart("site_statistics0",data);
                    });
                } else{
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getlicenseDataforchart", {type: 5},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics0').highcharts().destroy();
                        createLineChart("site_statistics0",data);
                    });
                }
            });
            
            $("#licensechartdataofmonth").on("click", function(){
                var Uname = "${user.userName}".replace(" ", "");
                if($("#licensechartdataofmonth").is(':checked')){
                    $("#licensechartdataofyear").attr('checked', false);
                    $("#licensechartdataofhalfyear").attr('checked', false);
                    $("#licensechartdataofquater").attr('checked', false);
                    $("#licensechartdataofweek").attr('checked', false);
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getlicenseDataforchart", {type: 4},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics0').highcharts().destroy();
                        createLineChart("site_statistics0",data);
                    });
                } else{
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getlicenseDataforchart", {type: 5},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics0').highcharts().destroy();
                        createLineChart("site_statistics0",data);
                    });
                }
            });
            
            $("#licensechartdataofweek").on("click", function(){
                var Uname = "${user.userName}".replace(" ", "");
                if($("#licensechartdataofweek").is(':checked')){
                    $("#licensechartdataofyear").attr('checked', false);
                    $("#licensechartdataofhalfyear").attr('checked', false);
                    $("#licensechartdataofquater").attr('checked', false);
                    $("#licensechartdataofmonth").attr('checked', false);
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getlicenseDataforchart", {type: 5},
                        function (data, status) {
                        console.log(data);
                        $('#site_statistics0').highcharts().destroy();
                        createLineChart("site_statistics0",data);
                    });
                }
            });
            
        </script>
        <script>
            $('.user-slider').slick({
                slidesToShow: 3,
                autoplay: false,
                dot: false,
		infinite: false,
            });
        </script>
        <script>
            $(function () {
            $('#site_statistics').highcharts({
            chart: {
            type: 'line'
            },
                    colors: ['#FF0000', '#FF0000'],
                    title: {
                    text: 'weekly users'
                    },
                    yAxis: {
                    min: 0
                    },
                    xAxis: {
            <c:set var="i" scope="page" value="1" />
                    categories: [
            <c:forEach items="${dates}" var="weekdates">
                <c:if test="${i eq dates.size()}">
                    '${weekdates}'
                </c:if>
                <c:if test="${i != dates.size()}">
                    '${weekdates}',
                </c:if>
                <c:set var="i" scope="page" value="${i+1}" />
            </c:forEach>
                    ]
                    },
                    credits: {
                    enabled: true
                    },
                    series: [{
                    name: 'Users',
                    data: ${weeklyusers}
                    }]
            });
            });
        </script>
        <script>
            $(function () {
            $('#site_statistics0').highcharts({
            chart: {
            type: 'line'
            },
                    colors: ['#FF0000', '#FF0000'],
                    title: {
                    text: 'Weekly License'
                    },
                    yAxis: {
                    min: 0
                    },
                    xAxis: {
            <c:set var="i" scope="page" value="1" />
                    categories: [
            <c:forEach items="${dates}" var="weekdates">
                <c:if test="${i eq dates.size()}">
                    '${weekdates}'
                </c:if>
                <c:if test="${i != dates.size()}">
                    '${weekdates}',
                </c:if>
                <c:set var="i" scope="page" value="${i+1}" />
            </c:forEach>
                    ]
                    },
                    credits: {
                    enabled: false
                    },
                    series: [{
                    name: 'License',
                            data: ${weeklylicense}
                    }]
            });
            });
        </script>
        <script>
//            if (${answers==null}) {
//                $('#basic').modal('show');
//            }
        </script>

        <script>
            $(document).on("click", "#addSecurityQuestion", function () {
                var question1 = $("#question1").select().val();
                var answer1 = $("#answer1").val().trim();
                var question2 = $("#question2").select().val();
                var answer2 = $("#answer2").val().trim();
                var flag = true;
                if (question1 === null || question1 === "") {
                    $("#errorMessageQuestion1").text("Please select first question.");
                    flag = false;
                }
                if (answer1 === null || answer1 === "") {
                    $("#errorMessageAnswer1").text("Please enter answer for first question.");
                    flag = false;
                }
                if (question2 === null || question2 === "") {
                    $("#errorMessageQuestion2").text("Please select second question.");
                    flag = false;
                }
                if (answer2 === null || answer2 === "") {
                    $("#errorMessageAnswer2").text("Please enter answer for second question.");
                    flag = false;
                }
                if (question1 === question2 && flag) {
                    $("#errorMessageAdd").text("Both Questions are same please select different question.");
                    flag = false;
                }
                if (flag) {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/addsecurityquestionanswer", {question1: question1, answer1: answer1, question2: question2, answer2: answer2},
                            function (data, status) {
                                if (data === "success") {
                                    $("#addresponse").text("Security Answers has been add successfully.");
                                    setTimeout(function()
                                    {
                                        $("#basic").modal("hide");
                                    }, 1000);
                                } else if (data === "Please login again.") {
                                    window.location.href = "${pageContext.request.contextPath}/";
                                } else {
                                    $("#errorMessageAdd").text(data);
                                }
                            }
                    );
                }
            });
            $(".deleteuser").on("click", function(){
                $("#deleteId").val(this.id);
                $("#confirm_delete").modal("show");
            });
            
            $("#confirmdeleteuser").on("click", function(){
                var id = $("#deleteId").val();
                var Uname = "${user.userName}".replace(" ", "");
                window.location.href = "${pageContext.request.contextPath}/"+Uname+"/deleteuser/"+id;
            });
            
            $(document).on("keyup", "#question", function () {
                var question = $("#question").select().val();
                if (question === "") {
                    $("#errorMessageQuestion").text("Please Select a Question.");
                } else {
                    $("#errorMessageQuestion").text("");
                }
            });

            $(document).on("keyup", "#answer", function () {
                var question = $("#answer").val();
                if (question === "") {
                    $("#errorMessageAnswer").text("Please Enter Answer.");
                } else {
                    $("#errorMessageAnswer").text("");
                }
            });
            
        </script>
        <script>
            $(document).ready(function () {
                $('#side-menu').find('li').removeClass('active');
                $(document).find(".dashboard").addClass('active');
                
                if($("#allnotification").is(':checked')){
                    $(".1").show();
                    $(".2").show();
                    $(".3").show();
                    $(".4").show();
                    $(".5").show();
                    $(".6").show();
                    $(".7").show();
                }
                
                $(".mt-checkbox-outline").find("input[type='checkbox'][name='filter']:checked").each(function () {
                    
                });
            });
        </script>
        <script>
             $("#watermarkcheckbox").on("click", function () {
                       $("#allnotification").attr('checked', false);
                 });
                 $("#licensecheckbox").on("click", function () {
                         $("#allnotification").attr('checked', false);
                 });
                 $("#coursecheckbox").on("click", function () {
                         $("#allnotification").attr('checked', false);
                 });
                 $("#userscheckbox").on("click", function () {
                         $("#allnotification").attr('checked', false);
                 });
                 
        </script>
        <script>
            $("#allnotification").on("click", function () {
                     if($("#allnotification").is(':checked')){
                         $("#userscheckbox").prop('checked', true);
                         $("#watermarkcheckbox").prop('checked', true);
                         $("#licensecheckbox").prop('checked', true);
                         $("#coursecheckbox").prop('checked', true);
                     } else{
                         $("#userscheckbox").attr('checked', false);
                         $("#watermarkcheckbox").attr('checked', false);
                         $("#licensecheckbox").attr('checked', false);
                         $("#coursecheckbox").attr('checked', false);
                     }
            });
        </script>
        <script>
            $(".mt-checkbox-outline").on("click", function () {
                if($("#allnotification").is(':checked')){
                    $(".1").show();
                    $(".2").show();
                    $(".3").show();
                    $(".4").show();
                    $(".5").show();
                    $(".6").show();
                    $(".7").show();
                } else{
                    $(".1").hide();
                    $(".2").hide();
                    $(".3").hide();
                    $(".4").hide();
                    $(".5").hide();
                    $(".6").hide();
                    $(".7").hide();
                }
                if($("#userscheckbox").is(':checked')){
                    $(".2").show();
                    $(".3").show();
                    $(".4").show();
                } else {
                    $(".2").hide();
                    $(".3").hide();
                    $(".4").hide();
                }
                if($("#coursecheckbox").is(":checked")){
                    $(".5").show();
                } else{
                    $(".5").hide();
                }
                if($("#licensecheckbox").is(":checked")){
                    $(".1").show();
                } else{
                    $(".1").hide();
                }
                if($("#watermarkcheckbox").is(":checked")){
                    $(".6").show();
                } else{
                    $(".6").hide();
                }
            });
        </script>
        <script>
            $("#support").on("click", function(){
                var Uname = "${user.userName}".replace(" ", "");
                $.post("${pageContext.request.contextPath}/"+Uname+"/getowneremailId",
                            function (data, status) {
                                if (data.indexOf("success") > -1) {
                                    console.log(data);
                                    var jsondata = JSON.parse(data.split("#")[1]);
                                    $("#ownerEmailId").val(jsondata.Email);
                                    $("#subjectvalidation").html("");
                                    $("#messageValidation").html("");
                                    $("#mailSubject").val("");
                                    $("#mailMessage").val("");
                                    $("#help_modal").modal("show");
                                } else if (data === "Please login again.") {
                                    window.location.href = "${pageContext.request.contextPath}/";
                                } else {
                                    console.log(data);
                                }
                            }
                    );
                
            });
            
            $("#sendMailToOwner").on("click", function(){
                $("#subjectvalidation").html("");
                $("#messageValidation").html("");
                var subject = $("#mailSubject").val();
                var messageContent = $("#mailMessage").val();
                var flag = true;
                var flag1 = true;
                
                if (subject ==="") {
                    $("#subjectvalidation").html("The Subject field is required.");
                    flag1 = false;
                } else if(subject.length <=3 || subject.length >=101) {
                    $("#subjectvalidation").html("Subject accepts 4-100 character only");
                    flag1 = false;
                }else{
                    flag1 = true;
                }
                
                if (messageContent=== "") {
                    $("#messageValidation").html("The Message field is required.");
                    flag = false;
                }else if(messageContent.length === 9){
                    $("#messageValidation").html("The Message field is required.");
                }else if ((messageContent.length-9) <=3 || (messageContent.length-9) >=1001) {
                    console.log(messageContent.length);
                    $("#messageValidation").html("Message accepts 4-1000 character only");
                    flag = false;
                }else{
                    console.log(messageContent);
                    console.log(messageContent.length);
                    flag = true;
                }
                
                if(flag && flag1){
                    var Uname = "${user.userName}".replace(" ", "");
                    $("#loading").css('display','block');
                    $.post("${pageContext.request.contextPath}/"+Uname+"/sendmailtoowner", {subject: subject, message: messageContent},
                    function (data, status) {
                        if(data === "success"){
                            $("#loading").css("display","none");
                            $("#sendmailresponse").html("Email has been sent successfully.");
                            setTimeout(function()
                            {
                                $("#help_modal").modal("hide");
                            }, 1000);
                        } else{
                            $("#loading").css("display","none");
                            $("#sendmailresponse").html(data);
                        }
                    });
                }
            });
        </script>
        <script>
        $( ".form-control" ).focus(function() {
            $(this).prev('.input-group-addon').removeClass().addClass('input-group-addon-focus');
            $(this).next('.input-group-addon').removeClass().addClass('input-group-addon-focus');
        });

        $( ".form-control" ).focusout(function() {
            $(this).prev('.input-group-addon-focus').removeClass().addClass('input-group-addon');
            $(this).next('.input-group-addon-focus').removeClass().addClass('input-group-addon');
        });
        </script>
    </body>
</html>
