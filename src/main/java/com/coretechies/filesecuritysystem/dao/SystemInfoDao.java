/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.SystemInfo;
import java.util.List;

/**
 *
 * @author CoreTechies M8
 */
public interface SystemInfoDao {

    public SystemInfo saveSystemInfo(SystemInfo systemInfo);

    public List<SystemInfo> getSystemInfoByCreator(Integer createdBy);
    
    public boolean deleteSystemId(Integer systemInfoId);

    public SystemInfo getSystemInfo(Integer userInfoId, Integer systemInfoId);

    public SystemInfo getSystemInfoBySystemName(Integer userId, String systemName);

    public SystemInfo getSystemInfoBySystemId(Integer userId, String systemName);

    public List<SystemInfo> getSystemInfoByUser(Integer userId);
    
    public List<SystemInfo> getAllSystemList();
    
    public SystemInfo getSystemById(Integer systeminfoid);
    
    public Boolean editSystemName(SystemInfo systemInfo);
}
