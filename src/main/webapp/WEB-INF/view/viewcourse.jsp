
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>FSS | View Course</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top"> 
 <!-- BEGIN HEADER INNER -->
 <jsp:include page="topbar.jsp" />
 <!-- END HEADER INNER --> 
</div>
<!-- END HEADER --> 
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER --> 
<!-- BEGIN CONTAINER -->
<div class="page-container"> 
 <!-- BEGIN SIDEBAR -->
 <div class="page-sidebar-wrapper"> 
  <!-- BEGIN SIDEBAR --> 
  <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
  <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
  <div class="page-sidebar navbar-collapse collapse"> 
   <!-- BEGIN SIDEBAR MENU --> 
   <jsp:include page="sidebar.jsp" />
<!--  <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
     DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
    <li class="sidebar-toggler-wrapper hide"> 
      BEGIN SIDEBAR TOGGLER BUTTON 
     <div class="sidebar-toggler"> <span></span> </div>
      END SIDEBAR TOGGLER BUTTON  
    </li>
    <li class="nav-item  "> <a href="index.html" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
    <li class="nav-item  "> <a href="users.html" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
     
    </li>
    <li class="nav-item  "> <a href="licence.html" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">Licence</span>  </a>
     
    </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
     
    </li>
    <li class="nav-item  active"> <a href="report.html" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
    <li class="nav-item  "> <a href="watermark.html" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
    <li class="nav-item  "> <a href="settings.html" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
   </ul>-->
   <!-- END SIDEBAR MENU --> 
   <!-- END SIDEBAR MENU --> 
  </div>
  <!-- END SIDEBAR --> 
 </div>
 <!-- END SIDEBAR --> 
 <!-- BEGIN CONTENT -->
 <div class="page-content-wrapper"> 
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content"> 
   <!-- BEGIN PAGE HEADER--> 
   
   <!-- BEGIN PAGE TITLE-->
   <!--<h3 class="page-title"><i class="icon-docs "></i> Report
    <!-- <small>blank page layout</small>--
   </h3>-->
   <!-- END PAGE TITLE--> 
   <!-- END PAGE HEADER-->
   
 
  <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PROFILE SIDEBAR -->
                            
                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content">
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered ">
                                            <div class="portlet-title tabbable-line">
                                                <div class="caption caption-md" style="width:100%;">
                                                    <i class="icon-globe theme-font hide"></i>
                                                    <span class="caption-subject  bold uppercase"><i class="icon-book-open "></i> Course</span>
                                                </div>
                                                <ul class="nav nav-tabs pull-left">
                                                    <li class="active">
                                                        <a href="#tab_1_1" data-toggle="tab">Course List</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_2" data-toggle="tab">Setting</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_3" data-toggle="tab">History</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="tab-content">
                                                    <!-- PERSONAL INFO TAB -->
                                            <div class="tab-pane active " id="tab_1_1">
                                            <div class="row">
                                                    <div class="col-sm-12">
                                                            <div class="portlet  portlet-datatable box red ">
                                                                    <div class="portlet-title">
                                                                        <div class="caption">
                                                                            <i class=" icon-book-open  "></i>
                                                                           Courses
                                                                        </div>
                                                                        <div class="actions">
                                                                             <a href="javascript:;" class=" btn" style="display:none;" data-toggle="tooltip" data-placement="top" title="Delete" id="deletecourse"> <i class="fa fa-trash-o"></i></a>
                                                                             <a href="javascript:;" class=" btn" data-toggle="tooltip" data-placement="top" title="Print" id="exportcourse"> <i class="icon-printer"></i></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="portlet-body clearfix">
                                                                        <table class="table table-striped table-bordered table-hover table-checkable order-column userlist-table datatable" id="sample_5">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th class="table-checkbox">
                                                                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                            <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes" />
                                                                                            <span></span>
                                                                                        </label>
                                                                                    </th>
                                                                                    <th> Course Id </th>
                                                                                    <th> Course Name </th>
                                                                                    <th> Users </th>
                                                                                    <th> PC </th>
                                                                                </tr>
                                                                            </thead>

                                                                            <tbody id="performactions">
                                                                                <c:forEach items="${courses}" var="course" varStatus="status">
                                                                                <tr class="odd gradeX">
                                                                                    <td>
                                                                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                            <input type="checkbox" name="courseId" class="checkboxes row_count" value="${course.id}" />
                                                                                            <span></span>
                                                                                        </label>
                                                                                    </td>
                                                                                    <td>${course.courseId}</td>
                                                                                    <td>${course.courseName}</td>
                                                                                    <td>${course.userLicenses.size()}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:;" class="btn  btn-sm  viewuser" id="${course.id}"><i class="fa fa-eye"></i></a></td>
                                                                                    <td>${course.userLicenses.size()}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:;" class="btn  btn-sm  viewsystem" id="${course.id}"><i class="fa fa-eye"></i></a></td>
                                                                               </tr>
                                                                            </c:forEach>
                                                                            </tbody>
                                                                        </table>
                                                                        <span style="color: red; display: block; font-size: 14px;" id="uploadresponse" ></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- END PERSONAL INFO TAB -->
                                                    <!-- UPLOAD COURSE TAB -->
                                                    <div class="tab-pane" id="tab_1_2">
                                               <div class="row">
                                                    <div class="col-sm-12">
                                                            <div class="portlet  portlet-datatable box red ">
                                                                    <div class="portlet-title">
                                                                        <div class="caption">
                                                                            <i class=" icon-book-open  "></i>
                                                                           Course Upload
                                                                        </div>
                                                                    </div>
                                                                    <div class="portlet-body clearfix">
                                                                         <div class="row clearfix">
                                                                            <div class="clearfix col-sm-8" >
                                                                            <form id="data" >
                                                                            <div class="form-group clearfix">
                                                                                <div style="margin-top: 10px;"></div>
                                                                                <table class="course-table">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                                <div>
                                                                                                    <span class="btn default btn-file">
                                                                                                        <input type="file" name="course" id="course" /> </span> 
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div id="loading" style="display: none;">
                                                                                                <center>
                                                                                                    <!--<span style="color: blue;">Processing...</span>-->
                                                                                                    <img src="${pageContext.request.contextPath}/assets/layouts/layout/img/24.gif" />
                                                                                                </center>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div class="form-actions">
                                                                                            <div class="row">
                                                                                            <div class="col-sm-3"></div>	
                                                                                                <div class="col-sm-8 text-right">
                                                                                                    <button type="submit" id="submit" class="btn btn-circle btn-success">upload</button>
                                                                                                    <!--<a href="#" class="btn green">Create User</a>-->
                                                                                                </div>
                                                                                            </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <span style="color: red; display: block; font-size: 14px;" id="responsemessage" ></span>
                                                                                <span style="color: green; display: block; font-size: 14px;" id="successupload" ></span>
                                                                            </div>
                                                                            </form>
                                                                            </div>							
                                                                    </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- END UPLOAD COURSE TAB -->
                                                    <!-- HISTORY TAB -->
                                                    <div class="tab-pane" id="tab_1_3">
                                                        <!--*********
														Alert Table
														*********-->
                                                                <div class="row">
                                                                        <div class="col-sm-12">
                                                                                <div class="portlet  portlet-datatable box red ">
                                                                                        <div class="portlet-title">
                                                                                            <div class="caption">
                                                                                                <i class=" icon-book-open  "></i>
                                                                                               Course History
                                                                                            </div>
                                                                                            <div class="actions">

                                                                                            </div>

                                                                                        </div>
                                                                                        <div class="portlet-body clearfix">
                                                                                            <table class="table table-striped table-bordered table-hover table-checkable order-column userlist-table datatable" id="sample_6">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th class="table-checkbox">
                                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                                <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes" />
                                                                                                                <span></span>
                                                                                                            </label>
                                                                                                        </th>
                                                                                                        <th> SN. </th>
                                                                                                        <th> Date </th>
                                                                                                        <th> Upload History </th>
                                                                                                    </tr>
                                                                                                </thead>

                                                                                                <tbody id="performactions">
                                                                                                <c:forEach items="${history}" var="history" varStatus="status">
                                                                                                <tr class="odd gradeX">
                                                                                                    <td>
                                                                                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                            <input type="checkbox" name="" class="checkboxes"  />
                                                                                                            <span></span>
                                                                                                        </label>
                                                                                                    </td>
                                                                                                    <td>${status.count}</td>
                                                                                                    <td>${history.key}</td>
                                                                                                    <td>${history.value} courses Uploaded</td>
                                                                                                </tr>
                                                                                                </c:forEach>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                    </div>
                                                    <!-- END History TAB -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- END PROFILE CONTENT -->
                        </div>
                    </div>
   
   
  </div>
  <!-- END CONTENT BODY --> 
 </div>
 <!-- END CONTENT --> 
 <!-- BEGIN QUICK SIDEBAR --> 
 <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 
 
 <!-- END QUICK SIDEBAR --> 
</div>
<!-- END CONTAINER --> 
<!-- BEGIN FOOTER -->
<div class="page-footer">
 <div class="page-footer-inner"> 2016 &copy; Organization </div>
 <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
</div>

        <div class="modal fade" id="confirm_delete" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModal1">Confirm Delete</h4>
                    </div>
                    <div class="modal-body"> 
                        <div class="form-group">
                            <span Style="font-style: normal;">Are You Sure To Want To Delete Selected Course ?</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <span id="response" style="color: green; display: block; font-size: 14px; float: left;" ></span>
                        <!--<button type="button" class=" btn btn green" data-dismiss="modal">NO</button>-->
<!--                        <button type="button" class="btn green" id="">YES</button>-->
                        <a href="javascript:;" id="Confirm" class="btn btn-circle btn-success">YES</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="view_user" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">User View</h4>
                    </div>
                    <div class="modal-body"> 
                        <div class="form-group">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="displaytable">
                                <thead>
                                    <tr>
                                        <td>Sr. No</td>
                                        <td>User Name</td>
                                        <td>Email Address</td>
                                    </tr>
                                </thead>
                                <tbody id="TBODY">
                                    <tr>
                                        <td>1</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
<!--                    <div class="modal-footer">
                        <button type="button" class=" btn btn green" data-dismiss="modal">OK</button>
                    </div>-->
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="view_pc" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModal">PC View</h4>
                    </div>
                    <div class="modal-body"> 
                        <div class="form-group">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="systemtable">
                                <thead>
                                    <tr>
                                        <td>Sr. No</td>
                                        <td style="width: 150px;">System Name</td>
                                        <td>System Id</td>
                                    </tr>
                                </thead>
                                <tbody id="TBody">
                                    <tr>
                                        <td>1</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
<!--                    <div class="modal-footer">
                        <button type="button" class=" btn btn green" data-dismiss="modal">OK</button>
                    </div>-->
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
<!-- END FOOTER --> 

<!-- BEGIN CORE PLUGINS --> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
<!-- END CORE PLUGINS --> 
<!-- BEGIN THEME GLOBAL SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
<!-- END THEME GLOBAL SCRIPTS --> 
<!-- BEGIN THEME LAYOUT SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="${pageContext.request.contextPath}/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS --> 

        <script>
            $(document).ready(function () {
                $('#side-menu').find('li').removeClass('active');
                $(document).find(".course").addClass('active');
                $('#sample_6').dataTable();
                $('#displaytable').dataTable();
                $('#systemtable').dataTable();
                
                
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
              var _t = $(e.target.hash).find('table.datatable');

              if(_t.length>0){
                _t.dataTable().fnDestroy();
                _t.dataTable();
              }
            });

                console.log($('.row_count').length);
                if($('.row_count').length>0){
                    $('#exportcourse').css('display','inline');
                }else{
                    $('#exportcourse').css('display','none');
                }
            });
        </script>
        <script>
                $(".checkboxes").on("click", function () {
                    var checkedData = [];
                    $("#performactions").find("input[type='checkbox'][name='courseId']:checked").each(function () {
                        checkedData.push($(this).val());   
                    });
                    if(checkedData.length === 1){
                        $("#deletecourse").show();
                    }
                    else if (checkedData.length > 1) {
                            $("#deletecourse").show(); 
                        }
                    else if (checkedData === null || checkedData.length === 0){
                            $("#deletecourse").hide();
                        }
                    });
                    
                $(".group-checkable").on("click", function () {
                        var checkedData = [];
                        var checkedboxes = [];
                        $("#performactions").find("input[type='checkbox'][name='courseId']").each(function () {
                            checkedData.push($(this).val());      
                        });
                        $("#performactions").find("input[type='checkbox'][name='courseId']:checked").each(function () {
                            checkedboxes.push($(this).val());   
                        });
                        console.log($(this).is(":checked"));
                        if($(this).is(":checked")){
                            console.log(checkedboxes);
                            if(checkedData === null || checkedData.length === 0){
                                $("#deletecourse").hide();
                            }else{
                                $("#deletecourse").show();
                            }
                        }else{
                            $("#deletecourse").hide();
                        }
                    });
        </script>
        <script>
            $("#submit").on("click", function(){
                 $("#uploadresponse").text("");
                 $("#responsemessage").text("");
                var formdata = new FormData();
                formdata.append("course", course.files[0]);

                if($("#course").val() === ''){
                    $("#responsemessage").text("Please Select a file.");
                }else{
                    var Uname = "${user.userName}".replace(" ", "");
                    $("#loading").css('display','block');
                    $.ajax({
                        url: "${pageContext.request.contextPath}/"+Uname+"/uploadcoursefile",
                        type: 'POST',
                        data:  formdata,
                        dataType: 'text',
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            if(data === "success"){
                                $("#loading").css("display","none");
                                $("#successupload").text("Course Uploaded Successfully");
                                setTimeout(function()
                                { 
                                    window.location.reload();
                                }, 1000);
                            } else if(data.indexOf("skipped") > -1){
                                $("#loading").css("display","none");
                                $("#successupload").text(data);
                                setTimeout(function()
                                { 
                                    window.location.reload();
                                }, 5000);
                                
                            } else if(data === "Course upload limit over"){
                                $("#loading").css("display","none");
                                $("#successupload").text(data);
                                setTimeout(function()
                                { 
                                    window.location.reload();
                                }, 5000);
                            }else {
                                $("#loading").css("display","none");
    //                            window.location.href = "${pageContext.request.contextPath}/${user.userName}/viewcourse";
                                $("#responsemessage").text(data);
                            }
                        }
                    });
                }
                return false;
            });
            
             $("#deletecourse").on("click", function () {
                 $("#confirm_delete").modal("show");
            });
            
            $("#Confirm").on("click", function () {
                 $("#uploadresponse").text("");
                var checkedData = [];
                $("#performactions").find("input[type='checkbox'][name='courseId']:checked").each(function () {
                    checkedData.push($(this).val());   
                });
                if (checkedData === null || checkedData.length === 0){
                    $("#uploadresponse").text("Please Select a Course");
                    $("#confirm_delete").modal("hide");
                }
                else {
                    console.log(checkedData);
                    var Uname = "${user.userName}".replace(" ", "");
                    $.get("${pageContext.request.contextPath}/"+Uname+"/deletecourse", {courseIds: checkedData},
                    function (data, status) {
                        if(data.indexOf("success") > -1){
                            var res = data.split("#")[1];
                            $("#response").text(res);
                            setTimeout( function()
                            {
                                $("#confirm_delete").modal("hide");
                                window.location.reload();
                            }, 5000);
                            
                        } else{
                            $("#response").text(data);
                        }
                    });
                }
            });
            
            $(".viewuser").on("click", function(){
             $("#uploadresponse").text("");
                var courseid = this.id;
                var Uname = "${user.userName}".replace(" ", "");
                $.post("${pageContext.request.contextPath}/"+Uname+"/coursedetails", {courseid: courseid},
                function(data, status){
                    if(data.indexOf("success") > -1){
                            $('#displaytable').dataTable().fnDestroy();
                            $('#displaytable TBODY').empty();
                            var wsHtml = '';
                            obj = JSON.parse(data.split("#")[1]);
                            var shareInfoLen = obj.length;
                            for(var i = 0; i < shareInfoLen; i++){
                               console.log( obj[i].userName +"," + obj[i].Email);
                            }
                            for (var i = 0; i < shareInfoLen; i++) {
                                wsHtml += '<tr>';
                                wsHtml += '<td>' + (i+1).valueOf() + '</td>';
                                wsHtml += '<td>' + (obj[i].userName).valueOf() + '</td>';
                                wsHtml += '<td>' + (obj[i].Email).valueOf() + '</td>';
                                wsHtml += '</tr>';
                            }
                            $('#displaytable > TBODY:last').append(wsHtml);
                            $('#displaytable').dataTable();
                            $("#view_user").modal("show");
                        } else{
                            $("#uploadresponse").text(data);
                        }
                });
            });
            $(".viewsystem").on("click", function(){
                 $("#uploadresponse").text("");
                var courseid = this.id;
                var Uname = "${user.userName}".replace(" ", "");
                $.post("${pageContext.request.contextPath}/"+Uname+"/coursedetails", {courseid: courseid},
                function(data, status){
                    if(data.indexOf("success") > -1){
                        $('#systemtable').dataTable().fnDestroy();
                        $('#systemtable TBody').empty();
                        var wsHtml = '';
                           obj = JSON.parse(data.split("#")[1]);
                           var shareInfoLen = obj.length;
                           for(var i = 0; i < shareInfoLen; i++){
                               console.log( obj[i].SystemName +"," + obj[i].SystemId);
                           }
                           for (var i = 0; i < shareInfoLen; i++) {
                                wsHtml += '<tr>';
                                wsHtml += '<td>' + (i+1).valueOf() + '</td>';
                                wsHtml += '<td>' + (obj[i].SystemName).valueOf() + '</td>';
                                wsHtml += '<td class="clearfix"><span class="text-ellips">' + (obj[i].SystemId).valueOf() + '</span></td>';
                                wsHtml += '</tr>';
                            }
                            $('#systemtable > TBody:last').append(wsHtml);
                            $('#systemtable').dataTable();
                            $("#view_pc").modal("show");
                        } else{
                            $("#uploadresponse").text(data);
                        }
                });
            });
            
            $("#exportcourse").on("click", function(){
                var Uname = "${user.userName}".replace(" ", "");
                window.location.href = "${pageContext.request.contextPath}/"+Uname+"/exportcourse";
            });
            </script>
</body>
</html>
