/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.CountryCode;
import com.coretechies.filesecuritysystem.domain.Course;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("CountryDao")
public class CountryDaoImpl implements CountryDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public List<CountryCode> getCountryCodes() {
        try {
            return entityManager.createQuery("SELECT C FROM CountryCode AS C", CountryCode.class).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public CountryCode getCountryById(Integer countryCodeId){
        try{
            TypedQuery<CountryCode> createQuery = entityManager.createQuery("SELECT C FROM CountryCode AS C WHERE C.countryCodeId=:countryCodeId", CountryCode.class);
            createQuery.setParameter("countryCodeId", countryCodeId);
            return createQuery.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
