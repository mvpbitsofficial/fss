/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.Watermark;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("WatermarkDao")
public class WatermarkDaoImpl implements WatermarkDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = false)
    public Watermark saveWatermark(Watermark watermark) {
        try {
            entityManager.persist(watermark);
            return watermark;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = false)
    public Watermark updateWatermark(Watermark watermark) {
        try {
            entityManager.merge(watermark);
            return watermark;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Watermark> getWatermarkList(Integer userId) {
        try {
            TypedQuery<Watermark> createQuery = entityManager.createQuery("SELECT W FROM Watermark AS W WHERE W.userId=:userId", Watermark.class);
            createQuery.setParameter("userId", userId);
            return createQuery.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Watermark getWatermarkByName(Integer userInfoId, String watermarkName) {
        try {
            TypedQuery<Watermark> createQuery = entityManager.createQuery("SELECT W FROM Watermark AS W WHERE W.userId=:userId AND W.watermarkName=:watermarkName", Watermark.class);
            createQuery.setParameter("userId", userInfoId);
            createQuery.setParameter("watermarkName", watermarkName);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Watermark getWatermarkByMessage(Integer userInfoId, String watermarkMessage) {
        try {
            TypedQuery<Watermark> createQuery = entityManager.createQuery("SELECT W FROM Watermark AS W WHERE W.userId=:userId AND W.watermarkMessage=:watermarkMessage", Watermark.class);
            createQuery.setParameter("userId", userInfoId);
            createQuery.setParameter("watermarkMessage", watermarkMessage);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Watermark getWatermark(Integer userInfoId, Integer watermarkId) {
        try {
            TypedQuery<Watermark> createQuery = entityManager.createQuery("SELECT W FROM Watermark AS W WHERE W.userId=:userId AND W.watermarkId=:watermarkId", Watermark.class);
            createQuery.setParameter("userId", userInfoId);
            createQuery.setParameter("watermarkId", watermarkId);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = false)
    public Boolean deletewatermark(Integer watermarkId) {
        try {
            entityManager.remove(entityManager.find(Watermark.class, watermarkId));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Watermark getWatermarkByName(Integer userInfoId, String watermarkName, Integer watermarkId) {
        try {
            TypedQuery<Watermark> createQuery = entityManager.createQuery("SELECT W FROM Watermark AS W WHERE W.userId=:userId AND W.watermarkName=:watermarkName AND W.watermarkId!=:watermarkId", Watermark.class);
            createQuery.setParameter("userId", userInfoId);
            createQuery.setParameter("watermarkName", watermarkName);
            createQuery.setParameter("watermarkId", watermarkId);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Watermark getWatermarkByMessage(Integer userInfoId, String watermarkMessage, Integer watermarkId) {
        try {
            TypedQuery<Watermark> createQuery = entityManager.createQuery("SELECT W FROM Watermark AS W WHERE W.userId=:userId AND W.watermarkMessage=:watermarkMessage AND W.watermarkId!=:watermarkId", Watermark.class);
            createQuery.setParameter("userId", userInfoId);
            createQuery.setParameter("watermarkMessage", watermarkMessage);
            createQuery.setParameter("watermarkId", watermarkId);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Watermark getDefaultWatermark(Integer userInfoId) {
        try {
            TypedQuery<Watermark> createQuery = entityManager.createQuery("SELECT W FROM Watermark AS W WHERE W.userId=:userId AND W.defaultWatermark=true", Watermark.class);
            createQuery.setParameter("userId", userInfoId);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    
    @Override
    @Transactional(readOnly = true)
    public Watermark getWatermarkForPreview(Integer watermarkId){
        try{
            System.out.println("Dao method watermark id=="+watermarkId);
            TypedQuery<Watermark> createQuery = entityManager.createQuery("SELECT W FROM Watermark AS W WHERE W.watermarkId=:watermarkid", Watermark.class);
            createQuery.setParameter("watermarkid", watermarkId);
            return createQuery.getSingleResult();
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
