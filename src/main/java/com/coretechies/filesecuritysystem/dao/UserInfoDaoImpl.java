/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.UserInfo;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.hibernate.Hibernate;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("UserInfoDao")
public class UserInfoDaoImpl implements UserInfoDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = true)
    public UserInfo authenticate(String userName, String password) {
        try {
            TypedQuery<UserInfo> createQuery = entityManager.createQuery("SELECT U FROM UserInfo AS U WHERE (U.userName=:userName OR U.emailId=:userName) AND U.password=:password", UserInfo.class);
            createQuery.setParameter("userName", userName);
            createQuery.setParameter("password", password);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional
    public UserInfo updateUser(UserInfo userInfo) {
        try {
            return entityManager.merge(userInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public UserInfo getUserInfo(String userName) {
        try {
            TypedQuery<UserInfo> createQuery = entityManager.createQuery("SELECT U FROM UserInfo AS U WHERE (U.userName=:userName OR U.emailId=:userName)", UserInfo.class);
            createQuery.setParameter("userName", userName);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public UserInfo getUserInfo(String userName, Integer userId) {
        try {
            TypedQuery<UserInfo> createQuery = entityManager.createQuery("SELECT U FROM UserInfo AS U WHERE (U.userName=:userName OR U.emailId=:userName) AND U.userInfoId!=:userId", UserInfo.class);
            createQuery.setParameter("userName", userName);
            createQuery.setParameter("userId", userId);
            return createQuery.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserInfo> getUserInfoByUserType(Integer userType) {
        try {
            TypedQuery<UserInfo> createQuery = entityManager.createQuery("SELECT U FROM UserInfo AS U WHERE U.userType=:userType", UserInfo.class);
            createQuery.setParameter("userType", userType);
            return createQuery.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public UserInfo getUserInfo(Integer userId) {
        try {
            return entityManager.find(UserInfo.class, userId);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = false)
    public UserInfo createUser(UserInfo userInfo) {
        try {
            entityManager.persist(userInfo);
            return userInfo;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = false)
    public Boolean deleteClient(Integer userId) {
        try {
            UserInfo find = entityManager.find(UserInfo.class, userId);
            entityManager.remove(find);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserInfo> getUserInfoByCreatedBy(Integer createdBy) {
        try {
            System.out.println("createdBy :" + createdBy);
            TypedQuery<UserInfo> createQuery = entityManager.createQuery("SELECT U FROM UserInfo AS U WHERE U.createdBy=:createdBy", UserInfo.class);
            createQuery.setParameter("createdBy", createdBy);
            System.out.println("createQuery : " + createQuery);
            List<UserInfo> resultList = createQuery.getResultList();
            for (UserInfo userInfo : resultList) {
                Hibernate.initialize(userInfo.getUserLicense());
                Hibernate.initialize(userInfo.getSystemInfos());
            }
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserInfo> getUserInfoByCreatedByDescendingOrder(Integer createdBy) {
        try {
            TypedQuery<UserInfo> createQuery = entityManager.createQuery("SELECT U FROM UserInfo AS U WHERE U.createdBy=:createdBy ORDER BY U.userInfoId DESC", UserInfo.class);
            createQuery.setParameter("createdBy", createdBy);
            return createQuery.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<UserInfo> getUserInfoByCreatedOn(Integer createdBy, Date fromDate, Date toDate) {
        try {
            TypedQuery<UserInfo> createQuery = entityManager.createQuery("SELECT U FROM UserInfo AS U WHERE U.createdBy=:createdBy AND U.createOn>=:startDate AND U.createOn<=:endDate", UserInfo.class);
            createQuery.setParameter("createdBy", createdBy);
            createQuery.setParameter("startDate", fromDate);
            createQuery.setParameter("endDate", toDate);
            return createQuery.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<UserInfo> chartUserData(Integer createdBy, Date date){
        try{
            TypedQuery<UserInfo> query = entityManager.createQuery("SELECT U FROM UserInfo AS U WHERE U.createdBy=:createdBy AND U.createOn=:date", UserInfo.class);
            query.setParameter("createdBy", createdBy);
            query.setParameter("date", date);
            System.out.println("date===="+date);
            System.out.println(query.getResultList());
            return query.getResultList();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    @Transactional(readOnly = true)
    public Map<Integer, Integer> showUserIdThroughCount(){
        try{
            TypedQuery<UserInfo> query = entityManager.createQuery("SELECT U FROM UserInfo AS U WHERE U.userType=:type", UserInfo.class);
            query.setParameter("type", 3);
            List<UserInfo> resultList = query.getResultList();
            Map<Integer,Integer> map = new HashMap<>();
            Integer count = 1;
            for (UserInfo userInfo : resultList) {
                map.put(userInfo.getUserInfoId(),count);
                count++;
            }
            return map;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    @Transactional(readOnly = true)
    public Map<Integer, Integer> showClientIdThroughCount(){
        try{
            TypedQuery<UserInfo> query = entityManager.createQuery("SELECT U FROM UserInfo AS U WHERE U.userType=:type", UserInfo.class);
            query.setParameter("type", 2);
            List<UserInfo> resultList = query.getResultList();
            Map<Integer,Integer> map = new HashMap<>();
            Integer count = 1;
            for (UserInfo userInfo : resultList) {
                map.put(userInfo.getUserInfoId(),count);
                count++;
            }
            return map;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public Integer showUserChartData(Date startDate, Date endDate, Integer type, Integer createdBy){
        try{
            TypedQuery<UserInfo> query = entityManager.createQuery("SELECT U FROM UserInfo AS U WHERE createOn BETWEEN :from AND :to AND userType=:type AND U.createdBy=:createdBy", UserInfo.class);
            query.setParameter("from", startDate);
            query.setParameter("to", endDate);
            query.setParameter("type", type);
            query.setParameter("createdBy", createdBy);
            List<UserInfo> resultList = query.getResultList();
            System.out.println("userInfo" +resultList);
            if(resultList != null){
                return resultList.size();
            } else{
                return 0;
            }
        } catch(Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public UserInfo getUserInfoEmailAndType(String email, Integer type) {
        try{
            TypedQuery<UserInfo> query = entityManager.createQuery("SELECT U FROM UserInfo AS U WHERE U.emailId=:email AND U.userType=:type", UserInfo.class);
            query.setParameter("email",email);
            query.setParameter("type", type);
            return query.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
        
    }
    
    @Override
    public UserInfo getUserInfoMobileAndType(String mobile, Integer type){
        try{
            TypedQuery<UserInfo> query = entityManager.createQuery("SELECT U FROM UserInfo AS U WHERE U.mobileNumber=:mobile AND U.userType=:type", UserInfo.class);
            query.setParameter("mobile", mobile);
            query.setParameter("type", type);
            return query.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
