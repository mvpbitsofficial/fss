/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.security;

/**
 *
 * @author CoreTechies M8
 */
public class InvalidInformationException extends Exception {

    private String message = null;

    public InvalidInformationException() {
        super();
    }

    public InvalidInformationException(String message) {
        super(message);
        this.message = message;
    }

    public InvalidInformationException(Throwable cause) {
        super(cause);
    }

    @Override
    public String toString() {
        return message;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
