/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.domain;

import java.io.Serializable;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author CoreTechies M8
 */
@Entity
@Table(name = "watermark")
@Access(AccessType.FIELD)
public class Watermark implements Serializable {

    @Id
    @GeneratedValue(generator = "watermark")
    @GenericGenerator(name = "watermark", strategy = "increment")
    private Integer watermarkId;

    @Column
    private String watermarkName;

    @Column
    private String watermarkMessage;
    
    @Column 
    private String watermarkContent;
    
    @Column
    private String property;

    @Column
    private Integer userId;

    @Column
    private Boolean defaultWatermark;
    
    @Column
    private Integer textPosition;
    
    @Column
    private String fontSize;
    
    @Column
    private String fontFamily;
    

    public Integer getWatermarkId() {
        return watermarkId;
    }

    public void setWatermarkId(Integer watermarkId) {
        this.watermarkId = watermarkId;
    }

    public String getWatermarkName() {
        return watermarkName;
    }

    public void setWatermarkName(String watermarkName) {
        this.watermarkName = watermarkName;
    }

    public String getWatermarkMessage() {
        return watermarkMessage;
    }

    public void setWatermarkMessage(String watermarkMessage) {
        this.watermarkMessage = watermarkMessage;
    }

    public String getWatermarkContent() {
        return watermarkContent;
    }

    public void setWatermarkContent(String watermarkContent) {
        this.watermarkContent = watermarkContent;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Boolean getDefaultWatermark() {
        return defaultWatermark;
    }

    public void setDefaultWatermark(Boolean defaultWatermark) {
        this.defaultWatermark = defaultWatermark;
    }

    public Integer getTextPosition() {
        return textPosition;
    }

    public void setTextPosition(Integer textPosition) {
        this.textPosition = textPosition;
    }

    public String getFontSize() {
        return fontSize;
    }

    public void setFontSize(String fontSize) {
        this.fontSize = fontSize;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    @Override
    public String toString() {
        return "Watermark{" + "watermarkId=" + watermarkId + ", watermarkName=" + watermarkName + ", watermarkMessage=" + watermarkMessage + ", userId=" + userId + ", defaultWatermark=" + defaultWatermark + ", textPosition=" + textPosition + ", fontSize=" + fontSize + ", fontFamily=" + fontFamily + '}';
    }
    
    
}
