/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.dao;

import com.coretechies.filesecuritysystem.domain.MailSetting;
import com.coretechies.filesecuritysystem.domain.UserInfo;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author CoreTechiesLP
 */
@Repository("MailSettingDao")
public class MailSettingDaoImpl implements MailSettingDao{

    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    @Transactional
    public MailSetting saveCourse(MailSetting mailSetting) {
        try {
            entityManager.persist(mailSetting);
            return mailSetting;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    @Transactional(readOnly = false)
    public MailSetting updateCourse(MailSetting mailSetting) {
        try {
            entityManager.merge(mailSetting);
            return mailSetting;
        } catch (Exception e) {
            return null;
        }
    }
    
    @Override
    public MailSetting getMailSettingsById(Integer userid){
        try{
            TypedQuery<MailSetting> query = entityManager.createQuery("SELECT M FROM MailSetting AS M WHERE M.userId=:userid", MailSetting.class);
            query.setParameter("userid", userid);
            return query.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
