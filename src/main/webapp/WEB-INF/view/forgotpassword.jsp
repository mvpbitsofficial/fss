<%-- 
    Document   : changepassword
    Created on : 9 Aug, 2016, 2:24:32 PM
    Author     : CoreTechies M8
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>FSS | Forgot Password</title>
        <script>var myContextPath = "${pageContext.request.contextPath}";</script>
        <script src="${pageContext.request.contextPath}/js/jquery-1.10.2.min.js"></script>
    </head>
    <body>
        <h1>Hello World!</h1>
        <form action="#">
            <div>
                <input type="text" id="newPassword" placeholder="new password"/>
                <br/>
                <label style="color: red;" id="errorMessageNewPassword"></label>
            </div>
            <div>
                <input type="text" id="confirmPassword" placeholder="confirm password"/>
                <br/>
                <label style="color: red;" id="errorMessageConfirmPassword"></label>
            </div>
            <div>
                <button type="button" id="changePassword">Submit</button>
                <br/>
                <label style="color: red;" id="errorMessageChangePassword"></label>
            </div>
        </form>
        <script>
            $(document).on("click", "#changePassword", function () {
                var newPassword = $("#newPassword").val();
                var confirmPassword = $("#confirmPassword").val();
                var flag = true;
                if (newPassword === null || newPassword === "") {
                    $("#errorMessageNewPassword").text("Please enter new password.");
                    flag = false;
                }
                if (confirmPassword === null || confirmPassword === "") {
                    $("#errorMessageConfirmPassword").text("Please enter confirm password.");
                    flag = false;
                }
                if (newPassword !== confirmPassword) {
                    $("#errorMessageConfirmPassword").text("Password and confirm password are mismatch.");
                    flag = false;
                }
                if (flag) {
                    $.post("${pageContext.request.contextPath}/forgotpassword", {newPassword: newPassword},
                            function (data, status) {
                                if (data === "success") {
                                    window.location.href = "${pageContext.request.contextPath}/";
                                } else if (data === "Please login again.") {
                                    window.location.href = "${pageContext.request.contextPath}/";
                                } else {
                                    $("#errorMessageChangePassword").text(data);
                                }
                            }
                    );
                }
            });
        </script>
    </body>
</html>
