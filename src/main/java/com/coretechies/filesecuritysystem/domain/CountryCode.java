/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coretechies.filesecuritysystem.domain;

import java.io.Serializable;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author CoreTechies M8
 */
@Entity
@Table(name = "countrycode")
@Access(AccessType.FIELD)
public class CountryCode implements Serializable {

    @Id
    @GeneratedValue(generator = "countrycode")
    @GenericGenerator(name = "countrycode", strategy = "increment")
    private Integer countryCodeId;

    @Column
    private String countryCode;

    @Column
    private String country;

    @Column
    private String mobileCode;
    
    
    public Integer getCountryCodeId() {
        return countryCodeId;
    }

    public void setCountryCodeId(Integer countryCodeId) {
        this.countryCodeId = countryCodeId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMobileCode() {
        return mobileCode;
    }

    public void setMobileCode(String mobileCode) {
        this.mobileCode = mobileCode;
    }

    @Override
    public String toString() {
        return "CountryCode{" + "countryCodeId=" + countryCodeId + ", countryCode=" + countryCode + ", country=" + country + ", mobileCode=" + mobileCode + '}';
    }

}
