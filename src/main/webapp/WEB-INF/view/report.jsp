
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>FSS | Report</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${pageContext.request.contextPath}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="${pageContext.request.contextPath}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top"> 
 <!-- BEGIN HEADER INNER -->
 <jsp:include page="topbar.jsp" />
 <!-- END HEADER INNER --> 
</div>
<!-- END HEADER --> 
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER --> 
<!-- BEGIN CONTAINER -->
<div class="page-container"> 
 <!-- BEGIN SIDEBAR -->
 <div class="page-sidebar-wrapper"> 
  <!-- BEGIN SIDEBAR --> 
  <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing --> 
  <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
  <div class="page-sidebar navbar-collapse collapse"> 
   <!-- BEGIN SIDEBAR MENU -->
   <jsp:include page="sidebar.jsp" />
   <!--  <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
     DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element 
    <li class="sidebar-toggler-wrapper hide"> 
      BEGIN SIDEBAR TOGGLER BUTTON 
     <div class="sidebar-toggler"> <span></span> </div>
      END SIDEBAR TOGGLER BUTTON  
    </li>
    <li class="nav-item  "> <a href="index.html" class="nav-link nav-toggle"> <i class="icon-home"></i> <span class="title">Dashboard</span> </a> </li>
    <li class="nav-item  "> <a href="users.html" class="nav-link nav-toggle"> <i class="icon-user "></i> <span class="title">Users</span> </a>
     
    </li>
    <li class="nav-item  "> <a href="licence.html" class="nav-link nav-toggle"> <i class="icon-key"></i> <span class="title">Licence</span>  </a>
     
    </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-note "></i> <span class="title">Applications</span></a>
     
    </li>
    <li class="nav-item  active"> <a href="report.html" class="nav-link nav-toggle"> <i class="icon-docs "></i> <span class="title">Report</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-clock "></i> <span class="title">History</span> </a> </li>
    <li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-reload "></i> <span class="title">Backup</span> </a> </li>
    <li class="nav-item  "> <a href="watermark.html" class="nav-link nav-toggle"> <i class="icon-frame "></i> <span class="title">Watermark</span> </a> </li>
    <li class="nav-item  "> <a href="settings.html" class="nav-link nav-toggle"> <i class="icon-settings "></i> <span class="title">Settings</span> </a> </li>
   </ul>--> 
   <!-- END SIDEBAR MENU --> 
   <!-- END SIDEBAR MENU --> 
  </div>
  <!-- END SIDEBAR --> 
 </div>
 <!-- END SIDEBAR --> 
 <!-- BEGIN CONTENT -->
 <div class="page-content-wrapper"> 
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content"> 
   <!-- BEGIN PAGE HEADER--> 
   
   <!-- BEGIN PAGE TITLE--> 
   <!--<h3 class="page-title"><i class="icon-docs "></i> Report
    <!-- <small>blank page layout</small>--
   </h3>--> 
   <!-- END PAGE TITLE--> 
   <!-- END PAGE HEADER-->
   
   <div class="row">
    <div class="col-md-12"> 
     <!-- BEGIN PROFILE SIDEBAR --> 
     
     <!-- END BEGIN PROFILE SIDEBAR --> 
     <!-- BEGIN PROFILE CONTENT -->
     <div class="profile-content">
      <div class="row">
       <div class="col-md-12">
        <div class="portlet light bordered ">
         <div class="portlet-title tabbable-line">
          <div class="caption caption-md" style="width:100%;"> <i class="icon-globe theme-font hide"></i> <span class="caption-subject  bold uppercase"><i class="icon-docs "></i> Report</span> </div>
          <ul class="nav nav-tabs pull-left">
           <li class="active"> <a href="#tab_1_1" data-toggle="tab">System ID</a> </li>
           <li> <a href="#tab_1_2" data-toggle="tab">License Details</a> </li>
           <li> <a href="#tab_1_3" data-toggle="tab">Alert</a> </li>
          </ul>
         </div>
         <div class="portlet-body">
          <div class="tab-content"> 
           <!-- PERSONAL INFO TAB -->
           <div class="tab-pane active " id="tab_1_1">
            <div class="row clearfix">
             <div class="clearfix col-sm-6 "  style="margin:0 auto; float:none;">
              <div class="input-group">
                <span class="input-group-addon input-circle-left">
                        <i class="fa fa-desktop"></i>
                </span>
               <input type="text" class="form-control" id="systemId" placeholder="Enter System ID">
                <span class="input-group-btn">
                <button class="btn btn-default green btn-circle-right" type="button"><i class="fa fa-upload"></i></button>
              	</span>
              </div>
              <div class="input-group-error" style=" margin-bottom:25px;">
                  <span class="error-message" id="errorMessagesystemId"></span>
              </div>
              <!-- /input-group -->
              <div class="input-group" style=" margin-bottom:25px; margin-left: 140px;"> <span class="input-group-btn input-group-btn-new">
               <button class="btn btn-circle btn-danger" type="reset" id="resetsystemId"><i class="fa fa-refresh"></i> Reset</button>
               <button class="btn btn-circle btn-success" id="getsysteminfo" type="button">Get Details</button>
               
               </span> </div>
              <div class="details-section" id="system_details" style="display: none;">
               <div class="panel panel-success">
                <div class="panel-heading">
                 <h3 class="panel-title" style="font-weight:500;">System ID Details</h3>
                </div>
                <div class="panel-body">
                 <table>
                  <tr>
                   <th>Disk Serial Number</th>
                   <td id="diskserialnumber"></td>
                  </tr>
                  <tr>
                   <th>MAC ID</th>
                   <td id="macid"></td>
                  </tr>
                 </table>
                </div>
               </div>
              </div>
             </div>
            </div>
           </div>
           <!-- END PERSONAL INFO TAB --> 
           <!-- CHANGE AVATAR TAB -->
           <div class="tab-pane" id="tab_1_2">
            <div class="row clearfix">
             <div class="clearfix col-sm-6" style="margin:0 auto; float:none;">
              <div class="input-group">
                <span class="input-group-addon input-circle-left">
                        <i class="fa fa-credit-card"></i>
                </span>
               <input type="text" class="form-control" id="licenseId" placeholder="Enter Licence" style="width: 100%;">
                <span class="input-group-btn">
                <button class="btn btn-default green btn-circle-right" type="button"><i class="fa fa-upload"></i></button>
              	</span>
               
              </div>
               <div class="input-group-error" style=" margin-bottom:25px;">
                   <span class="error-message" id="errorMessagelicenseId"></span>
               </div>
              <!-- /input-group -->
              <div class="input-group" style=" margin-bottom:25px; margin-left: 140px;"> <span class="input-group-btn input-group-btn-new">
               <button class="btn btn-circle btn-danger" type="reset" id="resetlicenseId"><i class="fa fa-refresh"></i> Reset</button>
               <button class="btn btn-circle btn-success" id="getlicenseinfo" type="button">Get Details</button>
               
               </span> </div>
            <div class="details-section" id="license_details_owner" style="display: none;">
               <div class="panel panel-success">
                <div class="panel-heading">
                 <h3 class="panel-title" style="font-weight:500;">License Details</h3>
                </div>
                <div class="panel-body">
                 <table>
                  <tr>
                   <th>User ID</th>
                   <td id="userId"></td>
                  </tr>
                  <tr>
                   <th>Name</th>
                   <td id="name"></td>
                  </tr>
                  <tr>
                   <th>Organization</th>
                   <td id="organization"></td>
                  </tr>
                  <tr>
                   <th>Validity</th>
                   <td id="validity"></td>
                  </tr>
                  <tr>
                   <th>License Limit</th>
                   <td id="licenseLimit"></td>
                  </tr>
                  <tr>
                   <th>System ID</th>
                   <td class="clearfix"><span class="text-ellips" style="width:250px;" id="systemid"></span><a href="javascript:;" class="pull-right btn btn-sm  grey-salsa btn-outline red-hover btn-circle" id="copysystemId"><i class="fa fa-copy"></i></a></td>
                  </tr>
                 </table>
                </div>
                   <span style="color: green; display: block; font-size: 14px; margin-left:15px;" class="copyresponse"></span>
               </div>  
              </div>
            <div class="details-section" id="license_details_client" style="display: none;">
               <div class="panel panel-success">
                <div class="panel-heading">
                 <h3 class="panel-title" style="font-weight:500;">License Details</h3>
                </div>
                <div class="panel-body">
                 <table>
                  <tr>
                   <th>User ID</th>
                   <td id="userId1"></td>
                  </tr>
                  <tr>
                   <th>Name</th>
                   <td id="name1"></td>
                  </tr>
                  <tr>
                   <th>Course ID</th>
                   <td id="courseid"></td>
                  </tr>
                  <tr>
                   <th>Course Name</th>
                   <td id="coursename"></td>
                  </tr>
                  <tr>
                   <th>Validity</th>
                   <td id="validity1"></td>
                  </tr>
                  <tr>
                   <th>System ID</th>
                   <td class="clearfix"><span class="text-ellips" style="width:250px;" id="systemid1"></span><a href="javascript:;" class="pull-right btn btn-sm  grey-salsa btn-outline red-hover btn-circle" id="copysystemId1"><i class="fa fa-copy"></i></a></td>
                  </tr>
                 </table>
                </div>
                   <span style="color: green; display: block; font-size: 14px; margin-left:15px;" class="copyresponse"></span>
               </div>
              </div>
             </div>
            </div>
           </div>
           <!-- END CHANGE AVATAR TAB --> 
           <!-- CHANGE PASSWORD TAB -->
           <div class="tab-pane" id="tab_1_3"> 
            <!--*********
														Alert Table
														*********-->
            <div class="portlet-body">
             <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_5">
              <thead>
               <tr>
<!--                <th class="table-checkbox"> <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                  <input type="checkbox" class="group-checkable" data-set="#sample_5 .checkboxes" />
                  <span></span> </label>
                </th>-->
                <th>Sr. No.</th>
                <th> Username </th>
                <th> Expire Date </th>
                <th> Status </th>
               </tr>
              </thead>
              <tbody id="tBody">
                  <c:forEach items="${licenseexpirealert}" var="license" varStatus="status">
                <tr class="odd gradeX">
<!--                 <td><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                   <input type="checkbox" name="licenceId" class="checkboxes"  />
                   <span></span> </label></td>-->
                <td>${status.count}</td>
                 <td>${license.userInfo.userName}</td>
                 <td><fmt:formatDate type="both" pattern="dd-MM-yyyy" value="${license.expiryDate}" /></td>
                 <td><c:choose>
                   <c:when test="${license.expiryDate.time gt today.time}"> Active </c:when>
                   <c:otherwise> Expired </c:otherwise>
                  </c:choose></td>
                </tr>
               </c:forEach>
              </tbody>
             </table>
            </div>
           </div>
           <!-- END CHANGE PASSWORD TAB --> 
          </div>
         </div>
        </div>
       </div>
      </div>
     </div>
     <!-- END PROFILE CONTENT --> 
    </div>
   </div>
  </div>
  <!-- END CONTENT BODY --> 
 </div>
 <!-- END CONTENT --> 
 <!-- BEGIN QUICK SIDEBAR --> 
 <a href="javascript:;" class="page-quick-sidebar-toggler"> <i class="icon-login"></i> </a> 
 
 <!-- END QUICK SIDEBAR --> 
</div>
<!-- END CONTAINER --> 
<!-- BEGIN FOOTER -->
<div class="page-footer">
 <div class="page-footer-inner"> 2016 &copy; Organization </div>
 <div class="scroll-to-top"> <i class="icon-arrow-up"></i> </div>
</div>
<!-- END FOOTER --> 

<!-- BEGIN CORE PLUGINS --> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script> 
<!-- END CORE PLUGINS --> 
<!-- BEGIN THEME GLOBAL SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/global/scripts/app.min.js" type="text/javascript"></script> 
<!-- END THEME GLOBAL SCRIPTS --> 
<!-- BEGIN THEME LAYOUT SCRIPTS --> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> 
<script src="${pageContext.request.contextPath}/clipboard/clipboard.min.js"></script> 
<!-- END THEME LAYOUT SCRIPTS --> 
<script>
    
                var clipboard = new Clipboard('#copysystemId', {
                text: function () {
                    var key = $("#systemid").text();
                    if(key===""){
                        
                    }else{
                        $(".copyresponse").text("System Id Copied successfully.");
                        return key;
                    }
                }
            });
            clipboard.on('success', function (e) {
                console.log(e);
            });
            clipboard.on('error', function (e) {
                console.log(e);
            });
            
            var clipboard = new Clipboard('#copysystemId1', {
                text: function () {
                    var key = $("#systemid1").text();
                    if(key===""){
                        
                    }else{
                        $(".copyresponse").text("System Id Copied successfully.");
                        return key;
                    }
                }
            });
            clipboard.on('success', function (e) {
                console.log(e);
            });
            clipboard.on('error', function (e) {
                console.log(e);
            });
            
            $("#systemId").on("change", function(){
                var systemId = $("#systemId").val();
                if (systemId === null || systemId.trim() === "") {
                    $("#errorMessagesystemId").text("Please enter System ID");
                } else {
                    $("#errorMessagesystemId").text("");
                }
            });
            
            $("#licenseId").on("change", function(){
                var licenseId = $("#licenseId").val();
                if (licenseId === null || licenseId.trim() === "") {
                    $("#errorMessagelicenseId").text("Please enter license Key");
                } else {
                    $("#errorMessagelicenseId").text("");
                }
            });
        </script> 
        <script>
            $("#getsysteminfo").on("click", function () {
                var systemId = $("#systemId").val();
                if (systemId === null || systemId.trim() === "") {
                    $("#errorMessagesystemId").text("Please enter System ID");
                } else {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getsysteminfo", {systemId: systemId},
                            function (data, status) {
                                if (data.indexOf("success") !== -1) {
                                    var jsondata = JSON.parse(data.split("#")[1]);
                                    $("#diskserialnumber").html(jsondata.serialNumber);
                                    $("#macid").html(jsondata.macId);
                                    $("#system_details").css("display", "block");
                                } else {
                                    $("#errorMessagesystemId").text(data);
                                }
                            }
                    );
                }
            });
        </script> 
<script>
            $("#getlicenseinfo").on("click", function () {
                var licenseId = $("#licenseId").val();
                if (licenseId === null || licenseId.trim() === "") {
                    $("#errorMessagelicenseId").text("Please enter license Key");
                } else {
                    var Uname = "${user.userName}".replace(" ", "");
                    $.post("${pageContext.request.contextPath}/"+Uname+"/getlicenseinfo", {licenseId: licenseId},
                            function (data, status) {
                                if (data.indexOf("success") !== -1) {
                                    var type = "${user.userType}";
                                    if(type === "1"){
                                        var jsondata = JSON.parse(data.split("#")[1]);
                                        $("#userId").html(jsondata.userId);
                                        $("#name").html(jsondata.userName);
                                        $("#organization").html(jsondata.organization);
                                        $("#validity").html(jsondata.validity);
                                        $("#licenseLimit").html(jsondata.licenseLimit);
                                        $("#systemid").html(jsondata.systemId);
                                        $("#license_details_owner").css("display", "block");
                                    } else{
                                        var jsondata = JSON.parse(data.split("#")[1]);
                                        $("#userId1").html(jsondata.userId);
                                        $("#name1").html(jsondata.userName);
                                        $("#courseid").html(jsondata.courseId);
                                        $("#coursename").html(jsondata.courseName);
                                        $("#validity1").html(jsondata.validity);
                                        $("#systemid1").html(jsondata.systemId);
                                        $("#license_details_client").css("display", "block");
                                    }
                                } else {
                                    $("#errorMessagelicenseId").text(data);
                                }
                            }
                    );
                }
            });
        </script> 
        <script>
            $(document).ready(function () {
                $('#side-menu').find('li').removeClass('active');
                $(document).find(".report").addClass('active');
            });
        </script>
        <script>
            $("#resetsystemId").on("click", function(){
                $("#systemId").val("");
                $("#system_details").css("display", "none");
                $("#errorMessagesystemId").text("");
            });
            $("#resetlicenseId").on("click", function(){
                $("#licenseId").val("");
                $("#license_details").css("display", "none");
                $("#errorMessagelicenseId").text("");
            });
        </script>
        <script>
        $( ".form-control" ).focus(function() {
            $(this).prev('.input-group-addon').removeClass().addClass('input-group-addon-focus');
            $(this).next('.input-group-addon').removeClass().addClass('input-group-addon-focus');
        });

        $( ".form-control" ).focusout(function() {
            $(this).prev('.input-group-addon-focus').removeClass().addClass('input-group-addon');
            $(this).next('.input-group-addon-focus').removeClass().addClass('input-group-addon');
        });
        </script>
</body>
</html>
